File: /home/sergio/.dotfiles/wiki/syndaemon.md
Las Change: Sep 19 2022 08:39

## Disable touchpad while typing (voidlinux)
 vim:ft=markdown
+ https://unix.stackexchange.com/a/210151/3157
+ https://ostechnix.com/disable-touchpad-while-typing-in-ubuntu-using-syndaemon/

Turns out, to enable the touchpad, one has to copy a reference Xorg config snippet:

    doas cp /usr/share/X11/xorg.conf.d/70-synaptics.conf /etc/X11/xorg.conf.d/70-synaptics.conf

    syndaemon -i 0.8 -K -R -t -d

Let us break down the above command and see what each argument does.

    -i : Defines how many seconds to wait after the last key press before enabling the touchpad. In my case, I have set 0.8 second. The default value is 2.0 seconds.
    - t : Only disable tapping and scrolling functions, not mouse movements, in response to keyboard activity.
    -K : ignore Modifier+Key combos.
    -R : Use the XRecord extension for detecting keyboard activity instead of polling the keyboard state.
    -d : Start syndaemon as a daemon in the background.

Check if all is working as you spect:

    ps aux | grep syndaemon

In addition, one can modify the file to set default options for tapping and scrolling.

So in fact the "problem" is that touchpads is nowadays handled by libinput, not by synaptics. This is why xinput still lists the device, but synclient cannot find it.

The touchpad properties can also be controlled using xinput, via xinput list-props and xinput set-prop—however, personally I found the libinput-touchpad-driver to be way less versatile than the synaptics driver, and not suitable for my purposes. Your Mileage May Vary.

If you simply want to change a specific single setting, you may want to look into xinput, before messing with your Xorg config.
