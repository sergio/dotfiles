---
file: xargs.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# xargs.md

## group output

    echo {1..9} | xargs -n3

## Convert png to jpg and vice-versa
+ https://www.tecmint.com/linux-image-conversion-tools/

```bash
    #----------- Convert PNG to JPG -----------
    ls -1 *.png | xargs -n 1 bash -c 'convert "$0" "${0%.png}.jpg"'

    #----------- Convert JPG to PNG -----------
    ls -1 *.jpg | xargs -n 1 bash -c 'convert "$0" "${0%.jpg}.png"'

    Explanation about the options used in the above command.

    -1 – flag tells ls to list one image per line.
    -n – specifies the maximum number of arguments, which is 1 for the case.
    -c – instructs bash to run the given command.
    ${0%.png}.jpg – sets the name of the new converted image, the % sign helps to remove the old file extension.


## Remove files
+ https://www.cyberciti.biz/faq/linux-unix-how-to-find-and-remove-files/

    find . -type f -name "*.err" -print0 | xargs -I {} -0 rm -v "{}"
```

## Crete html links for all pdf in the current folder
+ https://www.youtube.com/watch?v=hraHAZ1-RaM

    ls *.pdf | xargs -i echo "<a href='{}'><++></a>" > links

## Creating files from a list
+ https://www.linuxdescomplicado.com.br/2019/08/o-poder-e-a-versatilidade-do-comando-xargs.html

    cat /home/ricardo/lista.txt | xargs touch

you can also delete them

    cat /home/ricardo/lista.txt | xargs rm

## xargs with curl
+ https://www.abeautifulsite.net/downloading-a-list-of-urls-automatically

    xargs -P 4 -n 1 curl -k -O -J -L < bigfile.txt

## A better unix find with parallel processing?

xargs with the -P option (number of processes). Say I wanted to compress all
the logfiles in a directory on a 4-cpu machine:

    find . -name '*.log' -mtime +3 -print0 | xargs -0 -P 4 bzip2

You can also say -n <number> for the maximum number of work-units per process.
So say I had 2500 files and I said:

    find . -name '*.log' -mtime +3 -print0 | xargs -0 -n 500 -P 4 bzip2

This would start 4 bzip2 processes, each of which with 500 files, and then
when the first one finished another would be started for the last 500 files.

Not sure why the previous answer uses xargs and make, you have two parallel engines there!

## Copiando o mesmo arquivo para três pastas diferentes

    echo dir1 dir2 dir3 | xargs -n 1 cp -v arquivo.txt

    -n 1 – diz ao xargs para usar no máximo um argumento por linha de comando e enviar para o comando cp.
    cp – usado para copiar um arquivo.
    -v – permite mostrar os detalhes da operação de cópia.

## Consertando permissões
Usando a opção -0 do find e do xargs delimitamos os arquivos, isso
é útil para nomes de arquivos com espaços

    find . -type f -print0 | xargs -0 chmod +w

## Apagando thumbnails

    xargs rm -f <(find ~/.thumbnails -type f -ctime +3)

## Para arquivos com espaços no nome
Do manual do próprio xargs

    find /tmp -name core -type f -print0 | xargs -0 /bin/rm -f

## Testes !!!!!!!

    find -type f | xargs -i iconv -f iso-8859-1 -t utf-8 {} -o {}

## indicando um delimitador

    find /media/disk-1/mp3 -iname "*.db" | xargs -d '\n' rm -rf

## juntando linhas a cada 5 linhas
+ fonte: http://br.groups.yahoo.com/group/shell-script/message/31367

``` text
Pessoal,

tenho uma comando que gera a seguinte saida, onde a unica padronagem é uma
sequencia de 5 linhas:

0
r5
751625160
601300096
391584768
1
r5
1401393800
1121115008
621056
12
r5
1401393800
1121115008
68344320
45
r5
1401393800
1121115008
1235456
223
r5
2522508840
2242230016
388346880
```

Eu precisava gerar a saida neste formato, em 5 colunas:

``` text
0 r5 751625160 601300096 391584768
1 r5 1401393800 1121115008 621056
12 r5 1401393800 1121115008 68344320
45 r5 1401393800 1121115008 1235456
223 r5 2522508840 2242230016 388346880
Como poderei fazer?
Obrigado,
Alessandro Almeida.
```

Solução

    cat teste.txt | xargs -n5

