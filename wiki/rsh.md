---
file: rsh.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Acesso remoto
Executa comandos em computadores remotos
