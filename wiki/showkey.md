---
File: showkey.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: fev 13, 2020 - 12:26
tags: [tags]
---


#  showkey.md intro:
showkey - examine the codes sent by the keyboard

    showkey -a
