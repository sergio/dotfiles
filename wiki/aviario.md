---
File: ~/.dotfiles/wiki/aviario.md
Author: Sergio Araujo
Last Change: Sat, 20 Apr 2024 - 07:45:08
tags: [trabalho, work, galinhas]
---

#  aviario.md intro:

## links de vídeos

+ [Como começar a criação do zero](https://www.youtube.com/watch?v=9hpOofnigYI)
+ [como fazer ração](https://www.youtube.com/watch?v=CF0kCuhG_uc)
+ [moringa na ração](https://www.youtube.com/watch?v=ysNmaC6wOII)
+ [mais moringa na ração](https://www.youtube.com/watch?v=XPWMxoY-r4w)

Boas práticas na criação de galinhas caipira:

+ Não aglomerar muitos animais, se possível dividir em grupos menores
    Em caso de doenças a propagação não é tão rápida.

+ O criatório tem que ter área sombreada e área em que os animais pastam
    Com isso os mesmos podem enriquecer sua dieta com folhas de mato e
    besoursos, o que também ajuda a horta.

+ O esterco tem que ser periodicamente coletado com dois propósitos:
  1 - produção de fertilizante de alta qualidade (talvez o melhor de todos)
  2 - nas fezes das galinhas podem ficar ovos de parasitas que as atacam

+ Tem que haver bons puleiros, as galinhas gostam de dormir longe do chão

+ O uso de uma chocadeira é recomendável para garantir um índice de
    nascimentos constantes.

+ Com a orientação de um veterinário saber sobre a possibilidade de estocar
    algum medicamente para as doenças mais comuns das mesmas para que se possa
    fazer um trabalho preventivo antes que mais animais se contaminem, o gôgo
    é um exemplo. Talvez um chiqueiro menor para isolar animais com esse mal
    até que se recuperem.

+ A primeira refeição das galinhas é milho, a quantidade ideal é testada até o
    ponto em que após a primeira dieta não sobre milho no local escolhido.
    Se a área de pastejo for extensa diminui-se o dispêndio com ração.

+ Uma balança para pesagem da ração que iremos prepara
    Receita básica:

## Ração de postura

``` markdown
Ingredientes
------------
6kg de farelo de milho
2/5 kg de farelo de soja
1kg de calcário calcítico
50g de fosfato bicálcico
40g de sal mineral
```

``` markdown
Receita da EMBRAPA
-------------------
+ Milho - Fonte de energia com 10% de proteina
+ Mandioca - Fonte de energia
+ Muringa - Fonte de proteina
+ Umidade
```

+ Vou tentar implantar a muringa na fazenda com dois propósitos:
  1 - A muringa é uma planta riquíssima em nutrientes, faz-se uma farinha e
  adiciona-se na ração das galinhas.
  2 - A muringa produz flores quase o ano todo, assim o apiário existente ou
  em projeto pode se beneficiar. (Abelhas nativas seriam o ideal por não
  apresentarem nenhum risco de ferroadas)

+ A área do "pastejo" deve se possível ter árvores, pois as galinhas gostam de
 descansar nas sombras das árvores, evitando que nas horas quentes elas se
 aglomerem no galpão onde ficarão os puleiros.

 + No aproveitamento dos restos de comida deve-se evitar comidas muito velhas,
     mas em geral quase tudo se aproveita, evitando-se colocar cascas de ovos
     na comida senão as galinha aprendem a comer ovos.

+ Dos ninhos - O ideal é um ninho para cada 4 galinhas.
  são feitos de pinos.

## Remédio para o gôgo de caroço:

+ https://www.youtube.com/watch?v=5CtlCXXZGL4&t=390s

Ingredientes:

 * Sal
 * Cinza
 * Agua
