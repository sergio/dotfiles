---
file: ddrescue.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

ddrescue - data recovery tool

It is said that it is better to create an iso image from
a removable media like a flash drive.

TIP: You can use gnome-discs to easily perform a backup
     of you stick drive iso image
     #source: https://askubuntu.com/a/315041/3798

It is also used in forens activities

