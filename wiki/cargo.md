---
File: cargo.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [rust, tools, cargo, nvim]
---

#  Rust package manager

On voidlinux run:

    doas xbps-install -Sy cargo

Using cargo you can install `tree-sitter-cli`

    cargo install tree-siter-cli
    cargo install tree-sitter

