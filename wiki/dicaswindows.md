---
file: dicaswindows.md
author: Sergio Araujo
Last Change: Wed, 13 Nov 2024 - 16:13:59
tags: [windows]
---

# Arquivo: dicas windows

## Remover mensagem para ativar o windows

Abra o cmd como administrador e digite esse comando:
```sh
bcdedit -set testsigning OFF
```

Reinicie o windows

## Tiling window manager

[Glaze](https://github.com/glzr-io/glazewm)

## Modelos do word:

```text
C:\Users\<User>\AppData\Roaming\Microsoft\Modelos
```

## Atlhos e configurações úteis para o Rwindow$

## setar NVIM_APPNAME no windows

    setx NVIM_APPNAME newname
    Setx NVIM_APPNAME "lv" ; $env:NVIM_APPNAME = "lv" ; echo $env:NVIM_APPNAME



## limpar a formatação no word

Ctrl + Shift + n

## Dividir a tela em dois 

windows + ->

## virtual desktops

Virtual Desktop Management

Create a New Virtual Desktop:

    Win + Ctrl + D

Switch Between Virtual Desktops:

    Next Desktop: Win + Ctrl + Right Arrow
    Previous Desktop: Win + Ctrl + Left Arrow

Close the Current Virtual Desktop: 

    Win + Ctrl + F4

Task View (Desktop Overview)

Open Task View: Win + Tab
(This allows you to see all open windows and virtual desktops.)

## The Package Manager for Windows

+ https://chocolatey.org/

    choco install neovim
    choco install gnuwin32-coreutils.install

## alias no chocolatey

[PowerShell tools](https://github.com/PowerShell/PSReadLine)

Open your PowerShell profile in a text editor:

notepad $PROFILE

Add the alias code to this file:

function Go-Nvim {
    Set-Location -Path "$HOME\.config\nvim"
}
Set-Alias nvimhome Go-Nvim

## Change environment variable in powershell

    Setx NVIM_APPNAME "lv" ; $env:NVIM_APPNAME = "lv" ; echo $env:NVIM_APPNAME

## function to open files

```sh
function Open-FileWithFd {
    # Use `fd` to locate files, excluding .png, .jpg, and .git directory
    $file = fd -L -tf -E "*.png" -E "*.jpg" -E .git/ | fzf
    # Check if a file was selected
    if ($file) {
        nvim $file
    } else {
        Write-Output "Operation aborted."
    }
}
```

```sh
{
    "command": { "action": "runCommandLine", "commandLine": "powershell -c
Open-FileWithFd" },
    "keys": "ctrl+m"
}
```

## Scoop

A command-line installer for Windows

scoop install neovim

## get wifi password

netsh wlan show profile name="INSERT SSID HERE" key=clear

Note: if you don't know your SSID, you can list all Wi-Fi profiles you have connected in the past with following command:

netsh wlan show profile

Where router-name is the name of your wifi network

The first version of the command may dump out tens of lines of output (commonly between 30 and 40 lines).  If you want to see only the password (Key Content), you can pipe the command through findstr Key.  Note that the K in Key must be capitalized.

Example command line will therefore look like:

netsh wlan show profile name="Sales Dept Network" key=clear | findstr Key

You can leave out the "name=":

netsh wlan show profile "Sales Dept Network" key=clear | findstr Key

## Mediafire for windows 10

+ https://www.onmsft.com/news/mediafire-now-universal-windows-10-app

    Mediafire for android -> http://bit.ly/2SdDBBx

## Formatando PenDrive pelo CMD

+ http://anibolete.blogspot.com/2011/04/formatando-pendrive-pelo-cmd.html

    - Abra o prompt de comando (Iniciar >> All Programs >> Acessories >> Command Prompt).
    1. Diskpart (Uma nova janela será aberta, espere até o cursor aparecer)
    2. List Disk
    3. Select Disk 1 (substitua o 1 pelo número referente ao seu pendrive)
    4. Clean
    5. Create partition primary
    6. Active
    7. Format fs=fat32 quick
    8. Assign
    9. Exit

## Disable Autotuning (network)

+ https://www.sysprobs.com/windows-7-network-slow

Run in the cmd (as administrator) the folowing command

netsh interface tcp set global autotuning=disabled

## A good terminal emulator

+ https://github.com/cmderdev/cmder

You also should use babun "oh-my-zsh" framework on windows:
+ http://babun.github.io/

## Exibe os programas que estão abertos (possibilitando mata-los)

Ctrl + shift + Esc

Outro modo:
Abra o command e digite

tasklist.

Pegue o PID do programa e faça

taskkill /pid PID

Matar programa pelo nome:
taskkill /IM utorrent.exe /F

## Desfragmentar no linux
+ https://www.howtoforge.com/tutorial/linux-filesystem-defrag/

    sudo apt-get install e2fsprogs

To do this I use the following command:

sudo e4defrag

## Recording a desktop session

http://goo.gl/lbwM9G
On execute menu open:  psr.exe
In English "Step recorder" em portugues Gravador de passos

## Adding a new dir to windows path

first to see the path

echo %path%

setx /M PATH "%PATH%;C:\Program Files\MySQL\MySQL Server 5.5\bin"
setx /M PATH "$Env:PATH;<path to add>"

To add youtube-dl to PATH for all users, you first have to place youtube-dl.exe in a location that every user can access (e.g. C:/Program Files). Then, open cmd.exe as Administrator and paste the following command:

setx /M path "%PATH%;C:\Program Files"

## todos os atalhos de configuração em uma única pasta

1. Crie uma pasta em qualquer lugar;
2. De a essa pasta o seguinte nome (sem aspas):

    GodMode.{ED7BA470-8E54-465E-825C-99712043E01C}

3. O ícone da pasta deve mudar, indicante que o processo foi realizado com sucesso.

## Limpando o spool de de impressão

``` bat
net stop spooler
cd %systemroot%\system32\spool\PRINTERS
del /f /s *.SHD
del /f /s *.SPL
net start spooler
exit
```

## colar na linha de comando do windows

alt + espaço + e + l

## consertando pastas ocultas no pendrive

(considere neste caso o pendrive como sendo letra 'g')

attrib -h -r -s /s /d g:\*.*

```txt
Outra versão
-------------------------8<---------------------------
@ECHO OFF
ECHO Digite a letra do seu dispositivo e pressione enter.
ECHO (Verifique a letra atraves do Windows Explorer)
ECHO.
ECHO Digite somente a letra:
set /p letter=

ECHO.
ECHO %letter%: selecionado.

ECHO.
ECHO Fechando o Explorer para evitar problemas.
ECHO (Sera aberto novamente ao finalizar o processo)
ECHO Aproveite para acessar os blogs: www.thiagomedeiros.com e o www.ogestor.eti.br
ECHO.
taskkill /im explorer.exe /f
ECHO.
ECHO.
ECHO Processando arquivos, aguarde...
ECHO.
-------------------------8<---------------------------

attrib -r -a -s -h /s /d %letter%:*.*
del %letter%:*.lnk .vbs. .js .com /f /q
ECHO.
ECHO ------------CONCLUIDO!------------
ECHO.
pause
start explorer
start explorer %letter%:
taskkill /im cmd.exe /f
```

A final tip that might change your overall experience in Windows is that
you can improve the System Performance by playing with the options in
`%windir%\system32\SystemPropertiesPerformance.exe`. I disabled all of them
except for “Smooth edges of screen fonts”. Give it a try, you won’t be
disappointed.

## comandos do windows

``` markdown
    shell:appsfolder = mostra todos os apps
    control userpasswords2
    ac3filter.cpl = Abre filtro AC3 (se instalado)
    winver = Acerca do Windows (Ver a versão do Windows)
    wuaucpl.cpl = Actualizações automáticas
    appwiz.cpl = Adicionar ou remover programas
    odbccp32.cpl = Administrador da origem de dados de ODBC
    msinfo32 = Ajuda e suporte
    ntmsmgr.msc = Armazenamento amovível
    accwiz = Assistente de acessibilidade
    wiaacmgr = Assistente de câmara ou scanner
    netsetup.cpl = Assistente de configuração de rede
    ntbackup = Assistente de cópia de segurança ou restauro
    icwconn1 / inetwiz = Assistente de ligação à Internet
    migwiz = Assistente de transferência de definições e de ficheiros
    fsquirt = Assistente de transferência de ficheiros do Bluetooth
    hdwwiz.cpl = Assistente para adicionar hardware
    calc = Calculadora
    wscui.cpl = Centro de segurança do Windows
    certmgr.msc = Certificados
    telnet = Cliente Telnet
    ipconfig /flushdns = Configuração de protocolo de Internet (apagar informações de DNS )
    ipconfig /release = Configuração de protocolo de Internet (Todas as conexões )
    ipconfig /displaydns = Configuração de protocolo de Internet (ver DNS )
    ipconfig /all = Configuração de protocolo de Internet (ver tudo)
    ipconfig /setclassid = Configuração de protocolo de Internet (Modificar DHCP Class ID)
    ipconfig = Configuração do IP
    rsop.msc = Conjunto de politicas resultante (XP Prof)
    nusrmgr.cpl = Constas de utilizadores
    joy.cpl = Controladores de jogos
    secpol.msc = Definições da segurança local
    dfrg.msc = Desfragmentador do disco
    logoff = Desliga o utilizador do Windows
    eudcedit = Editor de carácter privado
    sysedit = Editor de configuração do sistema
    regedit / regedit32 = Editor de registo
    shutdown = Encerramento do Windows
    explorer = Explorador do Windows
    dxdiag = Ferramenta de diagnóstico do Direct X
    wabmig = Ferramenta de importação de livro de endereços
    mrt = Ferramenta de remoção de software malicioso Microsoft Windows
    control admintools = Ferramentas administrativas
    firewall.cpl = Firewall do Windows
    fonts = Fontes
    compmgmt.msc = Gestão de computadores
    diskmgmt.msc = Gestão de discos
    devmgmt.msc = Gestor de dispositivos
    packager = Gestor de objectos – pacote
    diskpart = Gestor de partições do disco
    taskmgr = Gestor de tarefas do Windows
    utilman = Gestor de utilitários
    verifier = Gestor de verificador de controladores
    hypertrm = HyperTerminal
    iexpress = Iexpress Wizard
    cmd -> systeminfo = Informações do systema
    control printers = Impressoras e faxes
    wmimgmt.msc = Infra-estrutura de gestão do Windows
    wupdmgr = Iniciar Windows Update
    mobsync = Itens a sincronizar
    iexplore = Internet Explorer
    tourstart = Introdução do Windows XP
    mshearts = Jogo de cartas Copas
    freecell = Jogo de cartas FreeCell
    spider = Jogo de cartas Spider Solitare
    winmine = Jogo Minesweeper
    mstsc = Ligação ao ambiente de trabalho remoto
    ncpa.cpl / control netconnections = Ligações de rede
    cleanmgr = Limpeza do disco
    cmd = Linha de comandos
    rasphone = Lista telefónica
    wab = Livro de endereços
    charmap = Mapa de caracteres
    dialer = Marcador telefónico
    access.cpl = Microsoft Access (se instalado )
    winchat = Microsoft Chat
    excel = Microsoft Excel (se instalado )
    frontpg = Microsoft Frontpage (se instalado )
    moviemk = Microsoft Movie Maker
    mspaint = Microsoft Paint
    powerpnt = Microsoft Powerpoint (se instalado )
    winword = Microsoft Word (se instalado )
    nero = Nero (se instalado)
    conf = Netmeeting
    notepad = Notepad
    nvtuicpl.cpl = Nview Desktop Manager (se instalado)
    access.cpl = Opções de acessibilidade
    control folders = Opções de pastas
    intl.cpl = Opções regionais e de idioma
    msimn = Outlook Express
    control = Painel de controlo
    directx.cpl = Painel de controlo Direct X (se instalado)
    jpicpl32.cpl = Painel de controlo Java (se instalado)
    pbrush = Paint
    ddeshare = Partilhas DDE
    printers = Pasta de impressoras
    "net share" ou compmgmt.msc graficamente = ver pastas compartilhadas no cmd
    fsmgmt.msc = Pastas partilhadas
    ntmsoprq.msc = Pedidos do operador de armazenamento amovível
    perfmon = Performance Monitor
    perfmon.msc = Performance Monitor
    telephon.cpl = Phone and Modem Options
    pinball = Pinball para Windows
    gpedit.msc = Politica de grupo (XP Prof)
    powercfg.cpl = Power Configuration
    findfast.cpl = Procura rápida (quando ligada)
    control color = Propriedade des visualização
    inetcpl.cpl = Propriedades da internet
    timedate.cpl = Propriedades de data e hora
    password.cpl = Propriedades de senhas
    mmsys.cpl = Propriedades de som e dispositivos de áudio
    control desktop/desk.cpl = Propriedades de visualização
    main.cpl / control mouse = Propriedades do rato
    sysdm.cpl = Propriedades do sistema
    control keyboard = Propriedades do teclado
    syskey = Protecção de base de dados do Windows
    Protecção de ficheiros do Windows (analisar em cada arranque) sfc /scanboot
    sfc /scanonce = Protecção de ficheiros do Windows (analisar no próximo arranque)
    sfc /scannow = Protecção de ficheiros do Windows (analisar)
    sfc /revert = Protecção de ficheiros do Windows (repor configuração de fábrica)
    QuickTime.cpl = Quicktime (se instalado)
    realplay = Real Player (se instalado)
    sticpl.cpl = Scanners e câmaras
    ciadv.msc = Serviço de indexação
    services.msc = Serviços
    dcomcnfg = Serviços componentes
    control schedtasks = Tarefas agendadas
    osk = Teclado de ecrã
    control fonts = Tipos de letra
    tweakui = Tweak UI (se instalado )
    msconfig = Utilitário de configuração do sistema
    cliconfg = Utilitário de rede do cliente de SQL Server
    sfc = Utilitário de verificação de ficheiros do sistema
    chkdsk = Utilitário de verificação do disco
    drwtsn32 = Utilitário Dr. Watson para o Windows
    lusrmgr.msc = Utilizadores e grupos locais
    sigverif = Verificação de assinatura do ficheiro
    clipbrd = Visualizador da área de armazenamento
    javaws = Visualizador de aplicações de java (se instalado)
    eventvwr.msc = Visualizador de eventos
    magnify = Windows Magnifier
    wmplayer = Windows Media Player
    msmsgs = Windows Messenger (Executar MSN)
    tourstart = Windows XP Tour Wizard (Tour Windows)
    write = Wordpad
    setx = Set variables like
```

## Windows shortcuts

Ctrl + Win + d  ................. create new virtual Desktop
Ctrl + Win + --> ................ alternate virtual Desktop
Ctrl + Win + F4 ................. close virtual Desktop
Win  + i  ....................... Open settings
Win + k  ........................ connect Tv
Win + v  ........................ clipboard
Win + Alt + r ................... record screen
Win + g ......................... see recordings
Win + l ......................... lock screen
Win + h ......................... record speech
