---
File: sxiv.md
Author: Sergio Araujo
Last Change: Wed, 16 Aug 2023 - 07:02:29
 vim:ft=markdown
tags: [img]
---

# sxiv.md intro

+ https://www.youtube.com/watch?v=GYW9i_u5PYs
+ https://hund.tty1.se/2018/04/12/sxiv-a-simple-vi-like-image-viewer.html

## It has a fork called nsxiv

+ https://github.com/nsxiv/nsxiv

nsxiv is a fork of now unmaintained sxiv with the purpose of being a drop-in
replacement of sxiv, maintaining it and adding simple, sensible features. nsxiv
is free software licensed under GPLv2 and aims to be easy to modify and
customize.

## NOTE

There is https://github.com/nsxiv/nsxiv which is a 'new' mantained version of sxiv (Worth taking a look)

## config sxiv:

    $XDG_CONFIG_HOME/sxiv/exec/key-handler

```sh
#!/bin/sh
# $XDG_CONFIG_HOME/sxiv/exec/key-handler
# Last Change: Sun, 15 May 2022 14:58

# the key handler starts with Ctrl-x

CH="/bin/wal"

while read file
do
    case "$1" in
    "c")
        cp "$file" ~/img/backgrounds/ ;;
    "w")
        $CH -i "$file" ;;
    "d")
        trash "$file" ;;
    esac
done
```

## Tricks
Read input list of images, select and then create a list of those images:

    ls * | sxiv -tio > images.txt

Select images to delete and send the list to trash-put:

    sxiv -o * | xargs trash

When viewing the images select those who will be deleted with `m`

## Shortcuts

    n ................... next image
    Space ............... next image
    p ................... previous
    m ................... mark
    f ................... toggle fullscren mode
    q ................... exit sxiv
    W ................... fit image to window

## Desktop entry (~/.local/share/applications/sxiv.desktop)

```sh
[Desktop Entry]
Type=Application
Name=sxiv
GenericName=Image Viewer
Exec=sxiv %F
MimeType=image/bmp;image/gif;image/jpeg;image/jpg;image/png;image/tiff;image/x-bmp;image/x-portable-anymap;image/x-portable-bitmap;image/x-portable-graymap;image/x-tga;image/x-xpixmap;
NoDisplay=true
Icon=sxiv
```
