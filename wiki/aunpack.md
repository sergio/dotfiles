---
Title: /home/sergio/.dotfiles/wiki/aunpack.md
Last Change: Tue, 31 Oct 2023 - 14:37:58
tags: [zip, unzip, rar, unrar, tools]
---

atool - A script for managing file archives of various types

aunpack – Decompress archives of any type

An invaluable tool to decompress zip or any other archive files. The best
feature is that it automatically detects if the archive doesn’t have a root
directory and creates one before unpacking if needed, preventing the annoyance
of extracting many files in the wrong directory. Who remembers all the arguments
needed when calling unzip or tar?

