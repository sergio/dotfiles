---
file: python.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Linguagem de progação alto nível e super fácil
Atualmente na versão 3 o python está se tornando
a linguágem mais poderosa da galáxia

## Installation tool like apt-get for python user

	python3 -m pip install --user pipx

## vintual envs course:
+ https://realpython.com/lessons/what-about-package-manager/

Install pyenv:

    curl https://pyenv.run | zsh
