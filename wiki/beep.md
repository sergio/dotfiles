---
file: beep.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Para um programa com o único intuito de provocar barulho o beep é até bastante
versátil. Veja algumas de suas opções:

    * -f: indica a frequência do barulho
    * -l: indica por quantos milisegundos o barulho vai durar
    * -r: indica quantos beeps o programa vai gerar

Exemplo: provocar 2 bipes de 2 segundos cada.

    beep -l 2000 -r 2

