---
file: linuxperformancetips.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# performance tips
+ https://itsfoss.com/speed-up-slow-wifi-connection-ubuntu/

## clear caches
One possible use of this is to test vim/neovim startup time, we need to clear caches

    echo 1 > /proc/sys/vm/drop_caches

## Close IPV6 support ∞

    echo "alias net-pf-10 off" >> /etc/modprobe.conf
    echo "alias ipv6 off" >> /etc/modprobe.conf

    disableipv6 (){
      echo "#disable ipv6" >> /etc/sysctl.conf
      echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
      echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
      echo "net.ipv6.conf.lo.disable_ipv6 = 1" >> /etc/sysctl.conf
    } && disableipv6

## Change your systemctl settings:
 [sysctl](sysctl).md

## firefox cache in RAM

criar a seguinte chave (string) através do about:config

    browser.cache.disk.parent_directory

coloque o valor para /var/shm/firefox-cache

## Disabling default ttys
+ https://alpha.de.repo.voidlinux.org/docs/print.html

Void Linux enables agetty(8) services for the ttys 1 to 6 by default.

To disable agetty services remove the service symlink and create a down file
in the agetty service directory to avoid that updates of the runit-void
package re-enable the service.

    sudo rm /var/service/agetty-tty[3-6]
    sudo touch /etc/sv/agetty-tty{3..6}/down

## Use a faster dns

    1.1.1.1

## Reduce Grub Load Time
+ https://www.fossmint.com/speed-up-ubuntu-linux/

When your laptop boots it shows an option for you to dual boot another OS or
enter recovery mode right? You typically have to wait for the default 10
seconds to pass or hit the enter button to get past that section.

You can make your machine automatically boot faster by reducing the wait time from 10 seconds. You can set this with the command below where you change

    GRUB_TIMEOUT=0

    sudo gedit /etc/default/grub
    sudo update-grub

## power management (cooling down the system for better performance)

    sudo xbps-install -Syu tlp
    sudo ln -s /etc/sv/tlp /var/service
