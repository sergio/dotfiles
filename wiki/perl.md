---
file: perl.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [dev, programming-languages, perl]
---

## instalador de pacotes "cpan"

    install Crypt::SSLeay
    install HTTP::Request::Common
    install HTTP::Cookies
    install LWP::UserAgent
    install LWP::Simple
    install HTML::Entities

    # perl rename
    cpan
    cpan1> install File::Rename

## Finding out lines that start with '#' but are not followed by
+ https://stackoverflow.com/a/6363029/2571881

    #
    !
    <space>http

    perl -ne 'print if /^#(?!(#|!| http))/' file

## Decimal para binario - decimal to binary

    $ var="304"
    $ perl -e 'printf "%b\n",'$var
    100110000

Em forma de função (usando perl)

    dec2bin(){
        var=$1
        perl -e 'printf "%b\n",'$var
    }

## regex tips:

    (?=) - Positive look ahead assertion foo(?=bar) matches foo when followed by bar
    (?!) - Negative look ahead assertion foo(?!bar) matches foo when not followed by bar
    (?<=) - Positive look behind assertion (?<=foo)bar matches bar when preceded by foo
    (?<!) - Negative look behind assertion (?<!foo)bar matches bar when NOT preceded by foo
    (?>) - Once-only subpatterns (?>\d+)bar Performance enhancing when bar not present
    (?(x)) - Conditional subpatterns
    (?(3)foo|fu)bar - Matches foo if 3rd subpattern has matched, fu if not
    (?#) - Comment (?# Pattern does x y or z)

## Printing

    perl -e "print 42"
    42

## substitution

    perl -p -i -e 's/replace this/using that/g' /all/text/files/in/*.txt

    echo 'um <em>texto<strong> com mais coisas' | perl -p -e
    's/<\/?(p|em|strong)>//g'

## Opening perl interactive shell
+ https://stackoverflow.com/a/73703/2571881

    perl -de1

## Renaming files (normalise file names)
+ https://blog.gnustavo.com/2020/11/perl-is-dead-expressive.html

Suppose there is a set of files in a directory which names consist of an
alphabetical prefix, followed by a sequence of digits and ending in the
extension .jpg. For example:

    $ ls
    a0.jpg b1.jpg c123.jpg

The challenge is to rename them so that all filenames have the same number of
digits in them. In the case above, the result should be:

    a000.jpg b001.jpg c123.jpg

    ls | perl -lpe \
    's/^([a-z]+)(\d+)\.jpg/sprintf "mv -n %s %s%03d.jpg", $&, $1, $2/e' | sh


``` perl
    foreach (<*.jpg>) {
        rename $_, sprintf("$1%03d.jpg", $2)
            if /^(.)(\d+)\.jpg$/;
    }

```

NOTE: In order to test the above command remove the `| sh`, it will just print
the result command on your screen.

But the [perl-]rename can solve this issue simply by doing:

    rename 's/(\d+)/sprintf("%03d", $1)/e' *.jpg

## Getting Mairo's flashcards

``` sh
curl -L link | grep -oP '(?<=<p><strong><em>).*(?=</em></p>$)' | sed -E 's/&nbsp;/ /g' | perl -pe 's/<\/?(em|strong)>//g' | sed 's,<br>,\n,g'
```

## convert to uppercase

    perl -pi.bak  -e "tr/[a-z]/[A-Z]/" sample.txt

