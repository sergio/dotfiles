---
File:~/.dotfiles/wiki/harpoon.md
Last Change: Wed, 10 Jan 2024 - 14:42:35
tags: [nvim, plugins]
---

https://github.com/ThePrimeagen/harpoon

Getting you where you want with the fewest keystroke

```lua
-- harpoon: https://github.com/ThePrimeagen/harpoon
return {
  'ThePrimeagen/harpoon',
  branch = 'harpoon2',
  dependencies = {
    'nvim-lua/plenary.nvim',
  },
  keys = {
    { '<Leader>gh', function()
        harpoon.ui:toggle_quick_menu( harpoon:list()) 
      end,  
      desc = 'Harpoon Menu' },

    { '<Leader>ga', function()
      harpoon:list():append() end, 
      desc = 'Harpoon Append File' },

    { '<Leader>gj', function()
      harpoon:list():next() end,
      desc = 'Harpoon jump next' },

    { '<Leader>gk', function()
      harpoon:list():prev() end,
      desc = 'Harpoon jump prev' },

  },
  config = function()
    harpoon = require('harpoon'):setup()
  end,
}
```
