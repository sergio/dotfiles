---
file: printf.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


Imprimir uma sequência de 1 a 300 usando 3 dígitos

## Bash 3

   printf "%03d\n" {1..300}

   printf "%s\n" {01..10}

   Quando usamos %s designamos string
   Quando usando %d atribuimos decimais

    for ((i=1;i-9;i++)) {
        num=$(echo "ibase=10; obase=2; $i"| bc) # convert to binary
        printf "%09d\n" $num
    }

    decToBin () {
            echo "ibase=10; obase=2; $1" | bc
    }

     printf "%09d\n" $(echo $((1 << 0)) )

