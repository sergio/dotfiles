---
file: ~/.dotfiles/wiki/cron.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [cron, admin]
---

## Introdução
+ https://crontab.guru
+ https://crontab-generator.org/
+ https://docs.voidlinux.org/config/cron.html

O cron (vem de cronologia) funciona assim

## minuto hora dia-do-mes mes dia-da-semana comando
+ https://www.cyberithub.com/solved-errors-in-crontab-file-cant-install/

``` sh
00 03 0 0 0 rm -rf /tmp/*

O comando acima limpará a pasta /tmp
todos os dias às 03:00h

52  18  1   *   *  root run-parts --report /etc/cron.montly
|   |   |   |   |   |   |
|   |   |   |   |   |   \_Comando que será executado
|   |   |   |   |   |
|   |   |   |   |   \_ UID que executará o comando
|   |   |   |   |
|   |   |   |   \_ Dia da semana (0-7)
|   |   |   |
|   |   |   \_ Mes (1-12)
|   |   |
|   |   \_ Dia do Mês (1-31)
|   |
|   \_ Hora
|
\_ Minuto
```

## See how a crontab is setted to a specific user

    sudo crontab -u sergio -l

## Run a task every 5 minutes

+ https://crontab.guru/every-5-minutes

In case of void linux I installed cronie:

    doas xbps-install -Sy cronie

``` sh
# crontab -e
# >/dev/null 2>&1 <- used to preven sending e-mail's
# in order to get your DISPLAY run the command below
# env | grep -i display
# If you do not set this variable feh will not change your wallpaper
# source: https://superuser.com/a/1122245/45032
DISPLAY=':0'
SHELL=/bin/sh
HOME=/home/sergio
PATH=/sbin:/bin:/usr/sbin:/usr/bin:$HOME/.local/bin
MAILTO=""
HOME=/home/sergio

#*/5 * * * * wal -qi /home/sergio/.dotfiles/backgrounds >/dev/null 2>&1
*/5    *    *    *    *  feh -z --bg-fill $HOME/img/backgrounds/*
#*/30    *    *    *    *  mpg123 /home/sergio/music/drink-water.mp3
#|    |    |    |    |            |
#|    |    |    |    |    Command or Script to execute
#|    |    |    |    |
#|    |    |    | Day of week(0-6)
#|    |    |    |
#|    |    |  Month(1-12)
#|    |    |
#|    |  Day of Month(1-31)
#|    |
#|   Hour(0-23)
#|
#Min(0-59)
```

## Minha tabela cron atual

```sh
# crontab -e
# vim:et:sw=4:ts=4:cms=#%s:
# Last Change: Mon, 25 Sep 2023 08:32
# >/dev/null 2>&1   <- used to preven sending e-mail's
# env | grep -i display  to get your DISPLAY variable
# otherwhise feh will not be abble to change your background
# source: https://superuser.com/a/1122245/45032
DISPLAY=':0'
SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/home/sergio/.dotfiles/algorithm/shell/bash/
XDG_PICTURES_DIR=/home/sergio/img
WALLPAPER_DIR=$XDG_PICTURES_DIR/backgrounds
WAL=/home/sergio/.local/bin/wal
MAILTO=""
#*/5 * * * * /home/sergio/.dotfiles/algorithm/shell/bash/waltheme.sh
*/5 * * * * /usr/bin/wal -qi /home/sergio/.dotfiles/backgrounds >/dev/null 2>&1
* * * * *  /home/sergio/.dotfiles/bin/checkbattery.sh >/dev/null
58 20 * * 1 /home/sergio/.dotfiles/bin/dumpceararural start >/dev/null 2>&1
5 22 * * 1 /home/sergio/.dotfiles/bin/dumpceararural stop >/dev/null 2>&1
* * * * *  /home/sergio/.dotfiles/bin/checkbattery.sh >/dev/null
```

## sempre usar caminho completo no cron

Quando referenciar scripts use o caminho completo, no cron não há como definir path

## criar um log:

```language
$ crontab -e
59 23 * * * /home/john/bin/backup.sh > /home/john/logs/backup.log 2>&1

In the above:

    > /home/john/logs/backup.log indicates that the standard output of the backup.sh script will be redirected to the backup.log file.
    2>&1 indicates that the standard error (2>) is redirected to the same file descriptor that is pointed by standard output (&1).
    So, both standard output and error will be redirected to /home/john/logs/backup.log
```

## atualizar a hora

## manter o relogio certo

    0 * * * * /usr/sbin/ntpdate -s ntp.usp.br

## minuto hora dia mˆes dia da semana comando

## Para executar o mesmo comando com um intervaldo
## de 4 horas entre cada execucao, poderiamos fazer:
## minuto hora dia mˆes dia da semana comando

    0 */4 * * * /usr/sbin/ntpdate -s ntp.usp.br

## executar um comando a cada 15 minutos

    */15 * * * * /path/to/command

## limpar thumbnails antigos
a cada trinta minutos verifica e limpa os thumbnails com mais de
três dias de acesso.

    */30 * * * * find ~/.thumbnails -type f -atime +3 | xargs rm -f

## visualizando o conteúdo da tabela cron

    crontab -l

## algumas dicas sobre o cron

Mudando o papel de parede

Apparently gsettings needs some variables to be set. Because CRON uses only a very restricted set of environment variables you must set them before your script. Use the following code in your CRON line.

    30 */2 * * * DISPLAY=:0 GSETTINGS_BACKEND=dconf /your/path/your-script.sh

## editando a tabela do cron

    crontab -e

Se você exportou as chaves ssh para outro computador pode até fazer um backup
assim:

    00 02 0 0 0 rsync -avz /backup/ root@servidor:/root/backup/

Veja como exporta a chave ssh

``` sh
ssh-keygen -b 1024 -t dsa
ssh-copy-id -i ~/.ssh/id_dsa.pub usuario@maquina_remota
```

## Referências
* http://www.devin.com.br/tlm4/s1-agendando-crontab.html
