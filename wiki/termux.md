---
file: ~/.dotfiles/wiki/termux.md
Last Change: Sun, 13 Oct 2024 - 05:56:05
tags: [termux, tools, android, admin, sysadmin]
---

# termux.md intro

+ [Access your termux from linux](https://linuxconfig.org/ssh-into-linux-your-computer-from-android-with-termux)
+ [video](https://www.youtube.com/watch?v=RxZRmKv-F94)
+ [instalando o termux](https://github.com/outragedline/neovim-termux#instalando-termux)

## Use F-droid instead of google play store

The updated termux version can only be found on the F-droid repo, read more [here](https://github.com/termux/termux-app/issues/2278)

## proot distro

+ [proot-distro how to](https://wiki.termux.com/wiki/PRoot)


An utility for managing installations of the Linux distributions in Termux. 

Install a linux distro inside your termux

``` sh
pkg in proot-distro
proot-distro install void
proot-distro login void
proot-distro list
proot-distro reset
```

## nerd fonts

+ [link](https://github.com/notflawffles/termux-nerd-installer)
+ [Mononoki font](https://www.nerdfonts.com/font-downloads) search for mononoki

## fix copy paste in neovim

just install the termux-api and remove xclip

## Check battery

``` sh
termux-battery-status
```

## Needed programs

```sh
iproute2 openssh termux-services curl wget mpv mpg123
cronie termux-api neovim python3 zsh zip unzip file fasd
trash-cli x11-repo xorg-xauth termux-api fd git
tree fzf nodejs termimage silversearcher-ag lua53 luarocks perl zsh termux-exec binutils jq
```

``` sh
pip3 install pipx
pipx install yt-dlp oscclip
```

## If you change $ZDOTDIR your zshrc must be named .zshrc even in the new path

## sudo

```sh
pkg in tsu
```

### font

+ <https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases>
+ <https://www.reddit.com/r/termux/comments/k93jly/comment/gf1rua4/>

```sh
cp font.ttf ~/.termux
termux-reload-settings
```

## font size

``` sh
ctrl-alt- +/-
```

## termux ssh ~/.config file

```sh
# This config uses the ed_25519 keys
# File: $HOME/.ssh/config
Host bitbucket.org
  AddKeysToAgent yes
  IdentityFile ~/.ssh/id_ed25519

# Allow password authentication
PasswordAuthentication yes

# Allow public key authentication
PubkeyAuthentication no

# Authorized keys file
AuthorizedKeysFile .ssh/authorized_keys

# Key exchange algorithms
KexAlgorithms curve25519-sha256,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512

# Host keys
#HostKey /etc/ssh/ssh_host_ed25519_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Hostname
#HostName localhost

# Port
Port 8022
```

## cron no termux

+ [Cron no termux](https://wiki.termux.com/wiki/Termux-services)

    pkg install cronie termux-services # restart termux session
    sv-enable crond
    crontab -e

    sv-enable sshd
    sv up sshd
    sv down sshd
    sv restart sshd

    from your (laptop):
    ssh-copy-id -p8022 -i ~/.ssh/id_rsa.pub u0_a221@192.168.0.46

So you to x11 forward install

  xorg-xauth

## Keep services alive

+ <https://wiki.termux.com/wiki/Termux:Boot>

Create a file under $HOME/.termux/boot/start-services

```sh
#!/data/data/com.termux/files/usr/bin/sh
# file: $HOME/.termux/boot/start-services
termux-wake-lock
. $PREFIX/etc/profile
```

You must disable some power saving options for the termux
and termux-boot and allow background activity on your
android settings.

## edit files over ssh

    install sshfs (on void fuse-sshfs)
    create a local mount point ~/remoteserv
    sshfs -o idmap=user <username>@<ipaddress>:/remotepath ~/remoteserv

## termux-boot

+ <https://wiki.termux.com/wiki/Termux:Boot>

    mkdir -p ~/.termux/boot/

    Put scripts you want to execute inside the ~/.termux/boot/ directory. If there are multiple files, they will be executed in a sorted order.

## As on termux keyboard we are slower typing

timeoutlen=1000
ttimeoutlen=10

It means that a mapping will wait 1 second to complete (as before) whereas a keycode will complete after only 10ms.

## send sms using termux

Add permission to termux at:

1 - Contacts
2 - SMS

```sh
termux-sms-send -n <number> <message>

termux-sms-list > contatos.txt

jq '.[] | select(.name == "Emanoel")' contatos.txt
{
    "name": "Emanoel",
    "number": "974-896-10"
}
```

## Curso de termux em vídeo

+ [No canal DK Security](https://www.youtube.com/playlist?list=PLvP6qbXrfmwglcinnyNRTWsLb57yIm2Xa)

Install:

    termux (via f-droid)

from termux install:

    pkg install termux-tools openssh mpv wget termux-api neovim python

    NOTE: you can use ->  pkg in package-name

## Fix "Repository under mantainance or down"

+ <https://www.youtube.com/watch?v=kaCkPTX3p2E>

    termux-change-repo

## set folders

    termux-setup-storage

If you have Termux:API application and termux-api package installed, you can use Android file picker to get any file from either shared or external storage by using utility termux-storage-get. Example:

  termux-storage-get filename.txt

## Generate your ssh-key on your android

  ssh-keygen -b 4096 -t rsa

Now copy your ssh public key:

  cat ~/.ssh/id_rsa.pub

Select the key, copy to the clipboard

Access your computer via termux

  ssh -p 2325 user@192.168.15.62

  nvim ~/.ssh/authorized_keys

Now paste the key

on your android start your sshd server

    sshd

    OBS: If is there another android you can start the server
    at another port like:

    sshd -p 2223

    If you have tried to start a ssh server and it did not work try

    killall sshd
    sshd -d

    OBS: it will start by default on the 8022 port

## Get your local ip

    pkg in iproute2
    alias localip='ip addr | grep -Po '\''(?<=inet)\s(?!127)[^/]+'\'

## Using ~/.ssh/config

``` sh
Host hp
    HostName 192.168.0.104
    User sergio
    Port 22

Host phone
    HostName 192.168.0.100
    User u0_a289
    Port 8022
```

## termux style

Termux:

    git clone https://github.com/adi1090x/termux-style
    cd termux-style
    ls
    chmod +x setup
    ./setup
    termux-style

## control the timeout

+ <https://bit.ly/2BUo5p9>

  vim /data/data/com.termux/files/usr/etc/ssh/sshd_config

```conf
# termux sshd_config file:
# $PREFIX/etc/ssh/sshd_config
PrintMotd no
#PasswordAuthentication yes
Subsystem sftp /data/data/com.termux/files/usr/libexec/sftp-server

# Allow password authentication
PasswordAuthentication no

# Allow public key authentication
PubkeyAuthentication yes

# Authorized keys file
AuthorizedKeysFile .ssh/authorized_keys

# Key exchange algorithms
KexAlgorithms curve25519-sha256,diffie-hellman-group14-sha256,diffie-hellman-group16-sha512

X11Forwarding yes
X11UseLocalhost yes
X11DisplayOffset 10

# Host keys
#HostKey /etc/ssh/ssh_host_ed25519_key
#HostKey /etc/ssh/ssh_host_ed25519_key

# Hostname
#HostName localhost

# Port
Port 8022
```

Increase SSH connection timeout using client side configuration

Another option is to enable ServerAliveInterval option in the client’s
$HOME/.ssh/ssh_config file. Very useful when you don’t have access to remote
servers’ sshd config file. Open the terminal application and then type the
following command:

```conf
vi ~/.ssh/config
Append/modify values as follows:

ServerAliveInterval 15
ServerAliveCountMax 3
```

In order to get your IP address:

  logcat -s 'syslog'

Find your hostname

  whoami

On your linux machine

    ssh

## Using scp

+ <https://wiki.termux.com/wiki/Remote_Access>

    scp -P 2223 aliases u0_a103@192.168.15.30:~/storage/downloads
    scp -P 8022 *.mp3 u0_184@192.168.15.2:~/storage/music

## setting termux access to /storage (external sdcard)

 termux-setup-storage

 cd /storage

## Create symlink on your [termux](termux.md) to access your sdcard

First figure out your sdcard identifier, using a sdcard adapter, mount your
sdcard in your computer and run one of these two commands:

    awk -F'[ =]' '/.dev.sdb/ {print $3}' <<<$(sudo blkid)
    grep -Po '(?<= UUID=")[^"]*' <<<$(sudo blkid /dev/sdb1)
    awk '/sdb1/ {print $11}' <<<$(ls -l /dev/disk/by-uuid)

   Then use its value on your symbolic link

## Access your termux using nautilus

    sftp://u0_a204@phone/data/data/com.termux/files/home
    sftp://u0_a179@192.168.0.100:8022/data/data/com.termux/files/home

## How to use Termux to download YouTube videos

+ <https://techwiser.com/how-to-use-termux-to-download-youtube-videos/>

    wget <http://pastebin.com/raw/W1nvzN6q> -O install.sh

    dos2unix install.sh

    chmod u+x install.sh

    ./install.sh

## pasting from system clipboard into neovim on termux

+ <https://github.com/termux/termux-packages/issues/2308>

Currently I am not using the code bellow, but I will keep it
for study.

```vim
au TextYankPost * call system('termux-clipboard-set &', @")
function Paste(p)
    let sysclip=system('termux-clipboard-get')
    if sysclip != @"
        let @"=sysclip
    endif
    return a:p
endfunction
noremap <expr> p Paste('p')
noremap <expr> P Paste('P')
```

## Listen youtube on termux

    pkg install mpv termux-api neovim python
    pip install yturl

    cat <<-'EOF' >> ~/.bashrc
    ya(){ # menemonic youtube audio
        mpv --no-video "$(yturl $(termux-clipboard-get))"
    }
    EOF

    yt

Using pure mpv

    mpv --no-video url

## short url using termux

``` sh
srl(){
    clear
    longurl=`termux-clipboard-get`
    url=`curl -s http://tinyurl.com/api-create.php?url=$longurl`
    echo $url | termux-clipboard-set
    echo "your shortened url:"
    echo $url
}
```

## comandos da api do termux

+ <https://wiki.termux.com/wiki/Termux:API>

    termux-camera-photo ...... Take a photo and save it to a file in JPEG format.

## Download videos from youtube

    wget https://pastebin.com/raw/RijZvset -O install.sh;chmod u+x install.sh;./install.sh;

## get youtube title

    #!/data/data/com.termux/files/usr/bin/bash

    URL=$(termux-clipboard-get)

    TITLE=$(youtube-dl --skip-download --get-title --no-warnings $URL | sed 2d | awk '{print tolower($0)}')

    echo "$TITLE' '$URL"  | tee termux-clipboard-set
