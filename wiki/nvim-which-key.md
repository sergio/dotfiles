---
File:~/.dotfiles/wiki/nvim-which-key.md
Last Change: Tue, 23 Jan 2024 - 17:50:52
tags: [nvim, plugins]
---

## Intro

WhichKey is a lua plugin for Neovim that displays a popup with possible key bindings of the command you started typing. Heavily inspired by the original emacs-which-key and vim-which-key.

- <https://github.com/folke/which-key.nvim>

If you type your `<leader> + key` fast enough, which depends on your
`vim.opt.timeoutlen` variable (see lua/options.lua), which-key will not show
the keybindings/shortcuts. Notice! Not every keybinding is listed on which-key,
but the config file under ./lua/user/whichkey.lua has groups like this:

## Nvim which-key: show keybindigs

+ [Oficial site](https://github.com/folke/which-key.nvim)

```lua
local wk = require("which-key")

-- Harpoon Which-key mappings
wk.register({
    -- The first key you are pressing
    h = {
        name  = "harpoon",
        -- the second key
        x = { function()
            require('harpoon.mark').add_file()
        end, "Mark file" }
    },
}, { prefix = "<leader>" })
```

Another relevant part of which-key settings:

```lua
defaults = {
      mode = { "n", "v" },
      ["g"] = { name = "+goto" },
      ["gs"] = { name = "+surround" },
      ["]"] = { name = "+next" },
      ["["] = { name = "+prev" },
      -- ["<leader><tab>"] = { name = "+tabs" },
      ["<leader>b"] = { name = "+buffer" },
      ["<leader>c"] = { name = "+code" },
      ["<leader>f"] = { name = "+file/find" },
      ["<leader>G"] = { name = "+git" },
      ["<leader>l"] = { name = "+Lazy" },
      ["<leader>n"] = { name = "+Neovim" },
      ["<leader>z"] = { name = "+Zsh" },
      ["<leader>u"] = { name = "+ui" },
      ["<leader>h"] = { name = "+Harpoon" },
      -- ["<leader>m"] = { name = "+Treesj" },
      -- ["<leader>gh"] = { name = "+hunks" },
      -- ["<leader>q"] = { name = "+quit/session" },
      -- ["<leader>s"] = { name = "+search" },
      -- ["<leader>w"] = { name = "+windows" },
      -- ["<leader>x"] = { name = "+diagnostics/quickfix" },
    },
```
