---
file: ps.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [ps, processes, cpu, memory, tools]
---

## Search for a precess by its name

    ps -fC firefox

To show just the process id PID

    ps -C firefox | awk 'NR==2 {print $1}'
    pgrep firefox

## Find Out Process Name Using PID Number
+ https://www.tecmint.com/find-process-name-pid-number-linux/

In this section, we will see how to find out a process name using its PID
number with the help of user defined format i.e comm= which means command name,
same as the process name.

    ps -p 2523 -o comm=

## Most memory consumption precesses
+ https://www.tecmint.com/find-linux-processes-memory-ram-cpu-usage/

    ps -A --sort -rss -o comm,pmem,rss | head -n 6

    ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head

## What terminal I am using?

    ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)

## listar usuario memória e cpu

    ps aux --sort=%mem,%cpu

    -t

    Select by tty.  This selects the processes associated with the
    terminals given in ttylist.

