---
file: nslookup.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

O Nslookup (Name System Look Up) é um instrumento que permite interrogar um
servidor de nomes a fim de obter as informações relativas a um domínio ou a um
hóspede e permite assim diagnosticar eventuais problemas de configuração do
DNS.

O comando nslookup foi desaprovado. Em seu lugar, um comando novo e mais
poderoso – [[dig]] (domain information groper) – deve ser usado. Em alguns
servidores Linux mais recentes o comando nslookup talvez não esteja
disponível.

nslookup www.google.com
