---
File: devuan.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [distros, linux]
---

#  devuan.md intro:
Devuan GNU+Linux is a fork of Debian without systemd that allows users to
reclaim control over their system by avoiding unnecessary entanglements and
ensuring Init Freedom.


## Checkin the integrity of images

Download the SHA256SUMS from the release archive and verify image integrity.

    user@hostname:~$ sha256sum --ignore-missing -c SHA256SUMS

    sha256sum devuan_beowulf_3.1.1_amd64_desktop-live.iso

GPG

Installation images distributed by Devuan are signed so that they can be
verified as coming from Devuan. Verifying images lets you know they have not
been altered prior to you receiving them.

Get the Devuan developers signing keys and import them to your keychain.

    user@hostname:~$ gpg --import devuan-devs.gpg
