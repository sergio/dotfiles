---
File: printenv.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: abr 20, 2020 - 08:40
tags: [tools, environment]
---

#  printenv.md intro:
+ https://bit.ly/3ammLXg

show environment settings

    printenv MANPAGER
    vim -M +MANPAGER -

Another way of seeing environment settings is using the command

    env
