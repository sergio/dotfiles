---
Filename: xcalib.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [monitor, color, icc, display, lcd, inkscape, design, gimp]
# vim:ft=markdown:et:sw=4:ts=4:cole=0:nospell:
---

xcalib-0.8_5 Tiny monitor calibration loader for X

argyllcms ICC compatible color management

ArgyllCMS is an ICC compatible colour management system, available as
Open Source under the GNU Copyleft license. It can be used freely, but
any re-distribution must comply with the GNU license conditions

It is cross platform, running on Microsoft Windows, Apple OS X and
Linux.

It is a collection of command line tools that can be used for a wide variety
of colour management purposes

## como usar o xcalib:
+ https://elias.praciano.com/2014/12/como-calibrar-e-configurar-os-ajustes-de-brilho-e-contraste-no-linux/

O xcalib é uma pequena ferramenta para calibrar e configurar o contraste, o
brilho e o gamma do seu display LCD, feito para Linux e Windows. Se você não
está satisfeito com os controles (de hardware), pode usar o xcalib para
configurar rapidamente a forma como o xorg regula os ajustes do seu display. As
opções, aqui relatadas, foram testadas em um notebook com uma placa Intel i915,
rodando o Ubuntu 14.04 LTS — não há qualquer garantia de que vá funcionar no seu
equipamento. Faça uso do conhecimento deste tutorial de forma responsável, por
sua própria conta e risco e nunca faça testes em máquinas de produção.

O xcalib pode conflitar com outras ferramentas de ajuste do vídeo, caso você
esteja usando alguma. No meu caso, desliguei temporariamente o aplicativo de
ajuste da temperatura do monitor Redshift.

    xcalib -version
    xcalib 0.8

Para resetar a configuração atual do vídeo e limpar os ajustes, use o seguinte comando:

    xcalib -c

O comando, acima, pode ser muito útil se você bagunçar muito o sistema, enquanto aprende a usar melhor o aplicativo.
Já aconteceu de ficar com a tela toda branca, mas conseguir reiniciar o perfil de cores, digitando este comando dentro de um terminal “às cegas”.

## Onde obter os perfis de cor?
+ http://wiki.nosdigitais.teia.org.br/CMYK_em_Linux
+ https://www.adobe.com/support/downloads/iccprofiles/icc_eula_win_end.html

Na pasta raiz do usuário crie uma pasta com o nome ".color" e dentro dela "icc"
Copie os arquivos .icc que estão dentro das pastas "CMYK Profiles" e "RGB Profiles" para a pasta icc

Pode também coipar os perfis de cor direto para a pasta de cores do sistema:

    doas cp * /usr/share/color/icc

Perfis de cor ARTEFIX
https://drive.google.com/drive/folders/1t2pG7WmxEwWDVGVpVEZ-8NOhN3V-NWKg

## como ajustar as cores do monitor usando o xcalib
+ https://elias.praciano.com/2014/12/como-calibrar-e-configurar-os-ajustes-de-brilho-e-contraste-no-linux/

Veja um exemplo de como invocar um perfil ICC:

    xcalib -d :0 -s 0 -v /usr/share/color/icc/colord/Bluish.icc

A opção -d :0, acima, se refere ao display ao qual será aplicado o perfil Bluish.icc e a opção -s 0 se refere à tela a que ele se aplica. A opção -v serve para exibir mais informações sobre a execução do comando.
Não fique desapontado se apenas alguns profiles ICC funcionarem no seu modelo de monitor. É assim, mesmo: nem todos funcionam.
Se você usa apenas um monitor, pode simplificar o comando:

    xcalib -v /usr/share/color/icc/colord

Como ajustar o contraste e o brilho do monitor com xcalib

As opções que se referem ao contraste e ao brilho são -co e -b. Opcionalmente, é possível usar as formas extensas -contrast e -brightness, como no exemplo abaixo:

    xcalib -v -contrast 70.0 -brightness 30.0 -alter

Os valores usados, acima, são percentuais: 70% de contraste e 30% de brilho, portanto.
A opção -alter se provou necessária. O xcalib vai pedir o arquivo ICC, se você não a usar.
Tenha cuidado com os valores usados, para não acabar com uma tela preta ou totalmente branca, o que dificulta corrigir a situação sem ter que reiniciar a máquina. Faça as alterações aos poucos.

### como ajustar individualmente as cores no vídeo

O xcalib permite ajustar especificamente o gamma, o contraste e o brilho de cada uma das cores que compõe o espectro que forma as imagens no seu display.
Para ajustar individualmente o vermelho, para obter uma sensação visual mais “quente”, use a opção -red. Veja 3 exemplos de ajuste do vermelho (red), verde (green) e azul (blue):

    xcalib -red 1.8 40 90 -alter
    xcalib -green 1.5 20 70 -alter
    xcalib -blue 1.3 20 70 -alter

## Explicação sobre prefís de cor:
+ https://cursos.alura.com.br/forum/topico-gerenciamento-de-perfis-de-cor-78009

Os perfis CMYK existem para padronizar os processos de impressão, de modo a buscar que a produção reproduza fielmente o que se vê em tela, além de melhor aproveitar as características específicas de cada substrato.

Ao escolher um determinado perfil, você está informando como será o gerenciamento e equilíbrio de cores. Cada um deles, portanto, dará instruções próprias ao maquinário no momento da impressão. Veja os exemplos a seguir:

    Coated FOGRA27 e Coated FOGRA39: utilizados em impressões de alta qualidade (em papel couchê, por exemplo), que suportam uma carga total de tinta de 350%.

    U.S. web coated (SWOP) v2: o Photoshop e demais softwares gráficos geralmente o utilizam como padrão. Ele limita a carga total de tinta em 300%.

No Brasil, os perfis mais comuns na produção são: U.S. web coated (SWOP) v2, U.S. web uncoated v2, Coated FOGRA27 e Coated FOGRA39. De qualquer maneira, caso queira ter o maior controle possível da impressão, pergunte à gráfica qual perfil ela utiliza.

Não efetuar o gerenciamento de cores significa que as especificações de cores dependerão do dispositivo. Dessa forma, o mais seguro é você utilizar um dos que citei acima e também fazer contato com a gráfica. Mas saiba que, geralmente, apenas as empresas de maior porte e capacidade técnica é que fazem esse tipo de controle; bureaus rápidos muito provavelmente não o farão.

Espero ter ajudado. Qualquer dúvida, estamos à disposição.

Abraço,

Felipe Labouriau
