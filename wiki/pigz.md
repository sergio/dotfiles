---
file: pigz.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
 vim:set ft=markdown nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
---
#  pigz.md intro:
+ https://askubuntu.com/a/288488/3798
+ https://bit.ly/3nZDwzq

pigz, which stands for parallel implementation of gzip, is a fully functional
replacement for gzip that exploits multiple processors and multiple cores to
the hilt when compressing data

+ https://stackoverflow.com/a/39904353/2571881

Mark Adler's top voted answer on the SO link that you included in your
question does provide a solution for specifying compression-level as well as
number of processors to use:

    tar cf - paths-to-archive | pigz -9 -p 32 > archive.tar.gz

    tar cfv dictation-female.tar.bz2 dictation-female --use-compress-program=lbzip2

# fast unpack:

    tar -I pigz -xf /mnt/sd/current/backup/bigbackup_web.tar.gz -C /tmp

# fast pack:

    tar -cf bigbackup.tar.gz -I pigz /opt

On zsh we can create a tarfast this way:

    (( $+commands[lbzip2] )) && alias tarfast='tar --use-compress-program=lbzip2'

And compress this way

    tarfast -cvf compress-name.tar.bz2 target-folder
