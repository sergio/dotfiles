---
File: viewnior.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [tags]
---

#  viewnior.md intro:

Viewnior is a fast and simple image viewer for GNU/Linux.

    Transparent images 

    Configure mouse actions 

    Navigation pan 

Created to be simple and elegant. It’s minimalistic interface provides more screen space for your images. Among its features are:

    Fullscreen & Slideshow
    Rotate, flip, crop, save, delete images
    Animation support
    Browse only selected images
    Navigation window
    Set image as wallpaper (Auto-detected DE, Gnome 3, Gnome 2, XFCE, MATE, Cinnamon, LXDE, FluxBox, Nitrogen, Puppylinux)
    EXIF and IPTC metadata
    Simple interface
    Configurable mouse actions

Viewnior was inspired by big projects like Eye of Gnome, because of it’s usability and richness, and by GPicView, because of it’s lightweight design and minimal interface. So here comes Viewnior – small and light, with no compromise with the quality of it’s functions. The program is made with better integration in mind (follows Gnome HIG2)

Viewnior is written in C (GTK+) and uses modified version of the GtkImageView library by Bjourn Lindqvist.

Viewnior is licensed under the terms of the GPLv3.
