Filename: cryptsetup.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [security, hack, criptography]

 vim:ft=markdown
## What cryptsetup is:
+ https://wiki.archlinux.org/title/Dm-crypt

cryptsetup é uma ferramenta da linha de comando para o dm-crypt criar, acessar e
gerenciar dispositivos criptografados. Ela foi expandida para suportar
diferentes tipos de encriptação que dependem do mapeador de dispositivos e
módulos criptografados (device-mapper and the cryptographic modules).

