---
file: hack.md
author: Sergio Araujo
Last Change: Sun, 29 Dec 2024 - 13:57:58
 vim:ft=markdown
tags: [ocr, hack, wifi, intelbras, medium]
---

## way back machine

+ [acess old web pages](http://web.archive.org/)
+ [old yahho grups](https://yahoo-geddon.tumblr.com/)

## Beautiful online ocr

+ [online ocs](https://docsumo.com/free-tools/online-ocr-scanner)

## curiosity

[youtube](https://youtu.be/eRvH0bP1QMM)

## many hack articles

+ [Hack articles](https://jameshunt.us/writings/)

## save youtube adds

+ [How to save youtube adds](https://youtu.be/476CbVEbItE)

[DreamPower](https://github.com/opendreamnet/dreampower "Create fake nudes")

DreamPower is a fork of the DeepNude algorithm that generates better fake nudes and puts at your disposal a command line interface.

## provernet

Nesse caso de Lentidão, Quedas, Oscilação ou Teste de Velocidade, peço que entre em contato na central (31) 3455-7751 ou (31) 3683-1087 ou (31) 3051-2929 próximo ao equipamento, para que possamos fazer o teste simultâneo ok?! O horário de atendimento é de 08:00 ás 21:00 de segunda a sábado, e aos domingos e feriados é de 09:00 ás 21:00.

## Useful websites

+ [10 minute mail](https://10minutemail.com/)

## hack.md

    My A3 359751082526208 / 01
    blade ->  k!cfCsdKh2z565 _#xunil864k
    fly reader -> 7mrmKqbf2qEsJo
    Lara network -> @familly150616@linux
    buy -> 疓疙疟疣疙疞畐疟疥畐疣疙疞疓疢疟疓疙疟
    brisa wifi - e5ohy6yt 

    CEP example: 31535-050
    Rua Dante Grassi 711, Rio Branco - Belo Horizonte - MG

## Build your own search engine using searXNG

+ <https://search.networkchuck.coffee/info/en/about>
+ <https://searx.space/>  (list of available services)

To add a new search engine you have to add the following string at the end of the url:

    /?q=%s
    https://search.networkchuck.coffee/?q=%s
    https://search.teamriverbubbles.com/?q=%s

## Meu A01

I have stored my IMEI in my bitwarden account:

+ [Enconte seu android](https://www.google.com/android/find)

Peça o bloqueio após abrir um boletim de ocorrência

``` txt
Claro: 1052
Oi: 1057 ou *144
TIM: 1056 ou *144
Vivo: 1058 ou *8486
Nextel: 1050 ou (11) 4004–6611
Porto Seguro Conecta: 10544 ou *333
```

## Meu Vivo

    85989699985

- vivo me liga:

Passo 1. Para mandar um torpedo, basta ligar para *9090 + DDD (se o DDD
for diferente do seu) + número Vivo do destinatário (Ex: *9090 + 31 +
99983-1616)

## modem rf301k intelbras (configurar como repetidor)

+ [Configurar o modo repetidor do modem](https://www.youtube.com/watch?v=SV-XDIfbQmM&t=32s)

## como cancelar mensagens das operadoras de celular

Passo 1. Abra o aplicativo de mensagens do seu celular e envie a palavra "sair" para sua operadora:

    TIM: 4112

    Claro: 888

    Oi: 55555

    Vivo: 457

Cancelar vivo turbo -> envia "desatirvar" para o número 9003

See more at [dicasgoogle](dicasgoogle.md)

## usefull websites

+ [bypass protected articles like medium](https://12ft.io/)

## Hide folders from your file manager withhouw renaming them

The method is quite simple. Just create a .hidden text file in your home
directory, and put the name of the files/folders you want to hide in
each separate line. So the contents of my own file at
/home/mhsabbagh/.hidden will be:

    snap
    Videos
    Music

Then close your file manager and open it again, and you’ll see the folders being hidden:

## password manager based on python "passpie"

``` sh
pip install --user passpie
```

## CLI password manager bitwarden

``` sh
sudo npm install -g @bitwarden/cli
```

## alguns torrents de conteúdo para o vim

+ [vim torrents](https://thepiratebay.org/search/vim/0/99/0)

## [Dotshare site](http://dotshare.it/)

DotShare is the attempted solution to all of the sporadic "Post your X
configs!" threads on various distro's forums. Moving all of those
threads to one central location, people can follow and get updates from
users who often post configs they like or share their tastes in setups.
In the end making it much easier to find what interests them and the new
cool things people are coming up with.

## Senha do roteador Vivo ou GVT

reddit account: Username: sergiolinux Passwd: 異疜疙疞疥疨畤畦畨疛

Solutions_Anisia → b35c81p45d tip: hideunhide

wifi Vanderilo: 03121962
wifi Manel: 08091988

fabio network VIVO-98D6 → 2cCcXpnAvd eliane network: eliane123

## A private messenger that does not need your phone number

+ [Send private messages](https://getsession.org/)

# Recover yahoo groups

+ [here](https://datahorde.org/how-to-recover-your-yahoo-groups-from-the-internet-archive/)

## Female viagra

    疜疑疔疩畝疕疢疑

## 3*new 3eg* account key

    i*hh00.com.br

    my alternative e-mail .com.br
    '畢異畣畓畤畔疜疙疞疥疨'

## zinha cpf: 727.896463-68

## milaif g 畣畨畡畦畨畠畨畡畣畡略畐畠畡畠畣畧畧

3et\*lux wifi 0916804Vg

## meu nit

     NIT: 10996013293

## Como saber meu número claro

    *510#

skype user: sergioaraujosilva passwd: sameofmylinuxmachine

    d*yd*1 -- atnovasenha chan chan chan

new google account: sergioluizaraujosilva passwd: sameofmylinuxmachine

    mypaswdpp 801189BuRNa089y8k

    airos ip: http://198.168.167.100/
    passwrd airos -->
    user = ubnt
    password =ubnt

Faster dns server

    1.0.0.1

## How to direct download dropbox links?

Let's say you have the link:

    https://www.dropbox.com/s/hyeo1ilubd6lw28/tmm-004.mp3?dl=0

Just swap zero for one and vuala, your direct download is ready!

## android

    códico para resetar a instalação do android
    *2767*3855#

    http://oitorpedoweb.oi.com.br/
    j03l3a=86093561

    -----------------------------------------------------
    senha para rede wireless da g
    ssid GVT-B1FD
    CP1116BULAY

    -----------------------------------------------------
    key hex 636f737461406d6164616e65742e636f6d2e627200
    key ascii costa@madanet.com.br

    sudo pppoeconf wlan0

## edita /etc/network/interfaces

    type key: WPA2PSK
    costa@madanet.com.br

    network name: madanet

    encryption: AES
    connection type: ESS

## some google secrets

Manipulate the google engine by using it to locate mp3 files online as well as some free software, and more! Using
Google, and some finely crafted searches we can find a lot of interesting information.

For Example we can find:

+ Passwords
+ Software / MP3′s
+ etc.

Presented below is just a sample of interesting searches that we can send to google to obtain info. After you get a
taste using some of these, try your own crafted searches to find info that you would be interested in.

Try a few of these searches:

    intitle:"Index of" passwords modified
    allinurl:auth_user_file.txt

    “access denied for user” “using password”

    “A syntax error has occurred” filetype:ihtml

    allinurl: admin mdb

    “ORA-00921: unexpected end of SQL command”

    inurl:passlist.txt

    “Index of /backup”

    “Chatologica MetaSearch” “stack tracking:”

And these:

    “parent directory ” /appz/ -xxx -html -htm -php -shtml -opendivx -md5 -md5sums
    “parent directory ” DVDRip -xxx -html -htm -php -shtml -opendivx -md5 -md5sums
    “parent directory “Xvid -xxx -html -htm -php -shtml -opendivx -md5 -md5sums
    “parent directory ” Gamez -xxx -html -htm -php -shtml -opendivx -md5 -md5sums
    “parent directory ” MP3 -xxx -html -htm -php -shtml -opendivx -md5 -md5sums
    “parent directory ” Name of Singer or album -xxx -html -htm -php -shtml -
    opendivx -md5 -md5sums

Notice that I am only changing the word after the parent directory, change it to what you want and you will get a lot of
stuff.

## method 2

put this string in google search:

    intitle:index.of mp3

You only need add the name of the song/artist/singer. Example: intitle:index.of mp3 jackson

## method 3

put this string in google search:

    inurl:microsoft filetype:iso

You can change the string to watever you want, ex. microsoft to adobe, iso to zip etc…

    “AutoCreate=TRUE password=*”

This searches the password for “Website Access Analyzer”, a Japanese software that creates webstatistics. For those who
can read Japanese, check out the author’s site at: coara.or.jp/~passy/ [coara.or.jp/~passy/]

    "http://*:*@www" domainname

This is a query to get inline passwords from search engines (not just Google), you must type in the query followed with
the the domain name without the .com or .net

     Another way is by just typing
     “http://bob:bob@www”

     “sets mode: +k”
     This search reveals channel keys (passwords) on IRC as revealed from IRC chat
     logs.

     allinurl: admin mdb
     Not all of these pages are administrator’s access databases containing
     usernames, passwords and other sensitive information, but many are!

         allinurl:auth_user_file.txt

     DCForum’s password file. This file gives a list of (crackable) passwords,
     usernames and email addresses for DCForum and for DCShop (a shopping cart
     program(!!!). Some lists are bigger than others, all are fun, and all belong to
     googledorks. =)

         intitle:"Index of" config.php

     This search brings up sites with “config.php” files. To skip the technical
     discussion, this configuration file contains both a username and a password for
     an SQL database. Most sites with forums run a PHP message base. This file gives
     you the keys to that forum, including FULL ADMIN access to the database.

     eggdrop filetype:user user

     These are eggdrop config files. Avoiding a full-blown descussion about eggdrops
     and IRC bots, suffice it to say that this file contains usernames and passwords
     for IRC users.

     Note: This is for informational purposes only. Do not use any of this
     information for illegal uses. Again, this is for demonstrational purposes.
     HacknMod is not responsible for your actions and does not promote illegal or
     malicious actvities.

## How to deal with e-mail scammers

+ <https://www.theverge.com/2017/11/10/16632724/scam-chatbot-ai-email-rescam-netsafe>

      me@rescam.org

## some cartoon sounds

    # mega command line tool
    https://mega.nz/cmd

+ <http://tinyurl.com/HBmega> .nz/#!gxJz2SZa!YqtK7EudLoNx5oIjisyr8in1c9xSmdQ6QkqPGcOTHy8
+ <http://tinyurl.com/HB1mega>
+ <http://tinyurl.com/HB2mega>
+ <http://tinyurl.com/HB3mega>
+ <http://tinyurl.com/HB4mega>
+ <http://tinyurl.com/HB5mega>

## transfer big files between two machines

+ <https://takeafile.com/>

tags: shell, bash, zsh
