---
File: epub.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: fev 18, 2021 - 14:52
tags: [tags]
---

#  epub.md intro:
+ https://garyhall.org.uk/create-ebook-command-line.html

Tips about specific epub creation

## parts of a ebook

1 - cover
2 - titlepage (folha de rosto)
3 - nav
4 - acknowledgments
4 - normal chapters
5 - credits

## epub template in Portuguese
+ https://github.com/JFTavares/ePub3-base

## read epub online
+ https://epub-reader.online/

## validate epub

Use epub checker
+ https://inclusivepublishing.org/toolbox/accessibility-checker/getting-started/#install-ace
+ http://www.mobileread.mobi/forums/showthread.php?t=294678

    doas npm install @daisy/ace -g

## Software for e-pub creation

1 - sigil -> [documentation](https://sigil-ebook.com/documentation/)
2 - calibre

# Changing the font
+ https://tex.stackexchange.com/a/234796/3528
+ https://colofao.com.br/709/sobre-fontes-em-e-books/

``` css
@font-face {
    font-family: "Linux Biolinum O";
    font-weight: 400;
    font-style: normal;
    /*font-variant: small-caps;*/
    src: url(../Fonts/LinBiolinum_R.otf);
}


/* latin-ext */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 400;
  src: url(../Fonts/Alegreya-Italic.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 400;
  src: url(../Fonts/Alegreya-Italic.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 500;
  src: url(../Fonts/Alegreya-Bold.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 700;
  src: url(../Fonts/Alegreya-BoldItalic.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 800;
  src: url(../Fonts/Alegreya-Bold.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: italic;
  font-weight: 900;
  src: url(../Fonts/Alegreya-Bold.otf);
}


/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: normal;
  font-weight: 400;
  src: url(../Fonts/Alegreya-Regular.otf);
}

/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: normal;
  font-weight: 500;
  src: url(../Fonts/Alegreya-Regular.otf);
}



/* latin */
@font-face {
  font-family: 'Alegreya';
  font-style: normal;
  font-weight: 700;
  src: url(../Fonts/Alegreya-Bold.otf);
}
```

Search fonts

    fc-list : family | cut -f1 -d"," | sort

For pdflatex

``` markdown
---
author:
- Sergio Araujo
- Thiago Avelino
fontfamily: Libertine
monofont: DejaVuSansMono
---
```

## boock cover template
+ source: booknando

``` xhtml
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html>

<html xmlns:epub="http://www.idpf.org/2007/ops" lang="en" xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title>Capa</title>
  <style>
    @page{margin-top:0;margin-bottom:0}body{margin:0;padding:0;text-align:center;oeb-column-number:1}img{max-width:100%;max-height:100%!important}figure{display:block;height:100%;page-break-inside:avoid;break-inside:avoid;text-align:center;margin:0}@supports (height:100vh){figure{height:100vh}}
  </style>
</head>

<body epub:type="cover">
 <figure id="cover">
    <img alt="capa" src="../Images/cover.png" />
  </figure>

</body>
</html>
```

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0 spell spelllang=en_us:
-->
