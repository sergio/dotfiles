---
file: redshift.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [tools, desktop, screen]
---
#  redshift.md intro:

Redshift adjusts the color temperature of your screen according to your
surroundings. This may help your eyes hurt less if you are working in front
of the screen at night. This program is inspired by
[f.lux](http://www.stereopsis.com/flux/)

+ https://unix.stackexchange.com/a/473588/3157
+ https://www.latlong.net/
+ https://github.com/jonls/redshift/blob/master/redshift.conf.sample

## Configuration

You will have to manually create the configuration directory as it's moved
in this version, though falling back in case the directory does not exist:

    mkdir ~/.config/redshift

Use your favorite text editor to create and edit the config file

    vim ~/.config/redshift/redshift.conf

Besides having .conf as extension I have to use this modeline on redshift.conf
because the comment string is ";"

    ; vim:set cms=;\ %s:filetype=redshift.conf:

Access maps.google.com right click on your localization and get your coordinates

## Reset redshift settings
+ https://askubuntu.com/a/1124208/3798

    redshift -x

In case of bspwm -> bspwmrc I normally run:

    redshift -x && redshift &

## colocar no autostart do xfce

OBS: Tem que instalar o programa  `geoclue2`

```sh
[ ! -d  ~/.config/autostart ] && mkdir -p ~/.config/autostart
ln -sfvn ~/.dotfiles/redshift/redshift.desktop ~/.config/autostart/redshift.desktop
```


