---
File:/data/data/com.termux/files/home/.dotfiles/wiki/zsh-zle.md
Last Change: Fri, Dec 2023/12/08 - 07:13:09
tags: [zsh, zle, cli]
---

## zle: zsh command line editor

+ [A Guide to the Zsh Line Editor with Examples](https://thevaluable.dev/zsh-line-editor-configuration-mouseless/)

NOTE: if you are using termux you must source your widgets file because
even when you close your termux it will keep the current section

## binding keys

The two keys CTRL and ALT are often used for keystrokes; therefore, it’s useful to know how to tell Zsh to use one or the other in our key binding:

``` sh
^ - Represent the CTRL key. For example: ^c for CTRL+c.
\e - Represent the ALT key. For example: \ec for ALT+c.
```

To clean the screen:

``` sh
bindkey '^l' clear-screen
```

## list all widgets avaiable

``` sh
zle -al
```

In practice, for you to display these sequences and use them in your bindings, you can use CTRL+v in your terminal followed by the special key you want to bind. It will output verbatim the control character sent, without interpreting it.

Show git diff:

``` sh
function _git-diff {
    zle push-input
    BUFFER="git diff"
    zle accept-line
}

zle -N _git-diff
bindkey '^Xd' _git-diff
```

To run a widget type alt-x

to see what a keybind does:

``` sh
bindkey '^[x'
```

