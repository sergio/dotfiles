# groff
+ https://www.gnu.org/software/groff/
+ https://www.youtube.com/watch?v=w8EKH_fjmXA

tags: [text, utils, doc, utils, latex]

groff (GNU roff) is a typesetting system that reads plain text input files that include formatting commands to produce output in PostScript, PDF, HTML, or DVI formats or for display to a terminal. Formatting commands can be low-level typesetting primitives, macros from a supplied package, or user-defined macros. All three approaches can be combined. 

## extension
There is a package called groffdown to compile markdown files as if they were groff files:
+ https://github.com/ChausseBenjamin/groffdown

