---
file: flatpack.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [packages, flatpack, installation]
---

## add flathub repo:
+ Loja de aplicativos flatpack: https://flathub.org/apps.html

    flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

Restart

To complete setup, restart your system. Now all you have to do is install some apps!

## anki (flashcards)

    flatpak install https://flathub.org/repo/appstream/net.ankiweb.Anki.flatpakref

## Install gimp 2.10 flatpack

    flatpak install https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref

## neovim (nvim)

    flatpak install https://flathub.org/repo/appstream/io.neovim.nvim.flatpakref

## Install podcasts (gnome)

    flatpak install flathub org.gnome.Podcasts

## Rss reader:

    flatpak install flathub org.gabmus.gfeeds

## gradio

    flatpak install --user --from
    https://repos.byteturtle.eu/gradio-master.flatpakref

## install telegram Desktop

    flatpak install --from https://flathub.org/repo/appstream/org.telegram.desktop.flatpakref

    flatpak --user update org.telegram.desktop
    flatpak --user uninstall org.telegram.desktop

## Install discord

    flatpak install --from https://flathub.org/repo/appstream/com.discordapp.Discord.flatpakref

## Shortwave radio player:

    flatpak install  de.haeckerfelix.Shortwave.flatpakref

## FontFinder
+ https://vitux.com/use-font-finder-to-install-google-fonts-on-debian-10/

    fontfinder
    https://flathub.org/apps/details/io.github.mmstick.FontFinder

    sudo flatpak install io.github.mmstick.FontFinder.flatpakref

## Given precedence for flatpak apps:

```sh
result=$(flatpak list | grep -ic 'inkscape' 2>/dev/null)

if [ "$result" -lt 1 ]; then
    inkscape
else
    flatpak run org.inkscape.Inkscape
fi
```


## mind maps
+ https://flathub.org/apps/details/com.github.phase1geo.minder
