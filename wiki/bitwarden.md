---
File: bitwarden.md
Author: Sergio Araujo
Last Change: Mon, 14 Oct 2024 - 14:56:34
vim:ft=markdown
Date: dez 16, 2020 - 10:24
tags: [firefox, addons, tools, security]
---

#  bitwarden.md intro:
- https://bitwarden.com/help/article/cli/

Secure passowrd manager

## How to install?
- https://bitwarden.com/help/article/cli/

    bw --help

    bw login [email] [password]

    bw login --apikey

This will initiate a prompt for your personal client_id and client_secret. Once your session is authenticated using these values, you can use the unlock command

## biwtwarden cli show password:

bw get password stackoverflow.com | pbcopy

bw sync

## To open the side panel type:
Como abrir a barra lateral no firefox

shift + alt + u

## Bitwarden cli
+ https://bitwarden.com/help/article/cli/#using-email-and-password

    doas npm install -g @bitwarden/cli

## save your bitwarde session to avoid typig your passowrd all the time:

```sh
# bitwarden login helper, it saves a session
function bwu(){
export BW_SESSION=$(bw unlock --raw $1)
}
```
## shortcuts

```txt
The following are the default shortcuts for Bitwarden browser extensions:

Ctrl/CMD + Shift + Y → Activate extension
Ctrl/CMD + Shift + L → Autofill, press again to cycle through matching logins
Ctrl/CMD + Shift + 9 → Generate a password and copy it to the clipboard
Ctrl/CMD + Shift + N → Lock extension
```

