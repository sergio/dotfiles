---
file: lscpu.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# lscpu.md - dom 12 nov 2017 20:36:23 -03

Gives complete infromation about processor

## How many cores do I have on my processor

    sed -n '2p' <(lscpu) | pbcopy

## tags: hardware, cpu, processors
