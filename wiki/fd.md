---
file: ~/.dotfiles/wiki/fd.md
author: Sergio Araujo
Last Change: Sat, 13 Apr 2024 - 13:50:06
tags: [search, tools, find, admin, sysadmin]
---

## Intro

NOTE: veja no final um script para compilar e instalar o fd

Technically, every fd search uses regular expressions. However, the search
pattern can be specified in a regex format. The following search looks for
entries starting with an m and containing the substring back at any other
position. For more information on the regular expression syntax, consult the
regex documentation.

-------------------------------------------------------------------------

NOTE: THE ~/.ignore FILE COULD AFFECT YOUR SEARCH (PAY ATTENTION TO THIS)
The global fd ignore file (usually $HOME/.config/fd/ignore
-------------------------------------------------------------------------

# Install the command fd and alias

A simple, fast and user-friendly alternative to 'find'

    (( $+commands[fd] )) && alias fd=/usr/bin/fd

## Reading the help

    fd -h

## find mp3 in the current folder and move to

    fd -e mp3 -d 1 | xargs mv -t flavio-jose

## play all mp3 including subfolders

    fd -e mp3 -0 | xargs -0 mpv


## type dir/file

    fd -t d name

    fd . --changed-within 20min -tf

## Simple search

fd is designed to find entries in your filesystem. The most basic search you
can perform is to run fd with a single argument: the search pattern. For
example, assume that you want to find an old script of yours (the name
included netflix):

    fd netfl
    Software/python/imdb-ratings/netflix-details.py

another example

    fd -H dicas .dotfiles

    .dotfiles/vim/wiki/DicasOpera.md
    .dotfiles/vim/wiki/dicasdebian.md
    .dotfiles/vim/wiki/dicasdobash.md
    ...

## comparing fd with find

    find -iname "*pattern*"
    fd pattern

## Regular expression search

The search pattern is treated as a regular expression. Here, we search for
entries that start with x and end with rc:

    cd /etc

    fd '^x.*rc$'

    X11/xinit/xinitrc
    X11/xinit/xserverrc

## Searching for a particular file extension

Often, we are interested in all files of a particular type. This can be done
with the -e (or --extension) option. Here, we search for all Markdown files in
the fd repository:

    cd fd
    fd -e md
    CONTRIBUTING.md
    README.md

## Parallel command execution

If the -x/--exec option is specified alongside a command template, a job pool
will be created for executing commands in parallel for each discovered path as
the input. The syntax for generating commands is similar to that of GNU
Parallel:

    {}: A placeholder token that will be replaced with the path of the search result (documents/images/party.jpg).
    {.}: Like {}, but without the file extension (documents/images/party).
    {/}: A placeholder that will be replaced by the basename of the search result (party.jpg).
    {//}: Uses the parent of the discovered path (documents/images).
    {/.}: Uses the basename, with the extension removed (party).

## Rename files

    fd -e txt -x mv {} {.}.md

## Convert all jpg files to png files

    fd -e jpg -x convert {} {.}.png

## Generate thumbnails fast

Enter the wallpapers folder

    mkdir thumbs
    fd -d1 -tf | parallel mogrify -format jpg -path thumbs -thumbnail 200x200 {}

Now creating a README.md with thumbs preview:

    fd -tf -d1  . thumbs  | awk -F/ '{print "[!["$2"]("$1"/"$2")]("$2")"}' > README.md

## Unpack all zip files (if no placeholder is given, the path is appended)

    fd -e zip -x unzip {}

## Convert all flac files into opus files

    fd -e flac -x ffmpeg -i {} -c:a libopus {.}.opus

## Count the number of lines in Rust files (the command template can be terminated with ';')

    fd -x wc -l \; -e rs

## find every shell script

    fd sh --type f

This can be abbreviated to:

    fd sh -tf

## find Markdown (extension)

    fd -e md
    fd -e md --exec wc -l

    # Find files that begin with "foo"
    fd '^foo'

    fd pattern --exec command

## search empty folders and exclude (exclude "build")

    fd -te -td -E build

## options

    -H        show hidden files
    --regex   default
    -i        ignore case (default)
    -0        --print0 (see xargs)
    f         regular files
    d         directories
    l         symlink
    x         executable
    e         empty

    You can also ignore specific files by adding -E to the command

## Follow symlinks

    -L  ................... follow symlinks

    By default, fd does not descend into symlinked
    directories. Using this flag, symbolic links are also
    traversed. The flag can be overridden with '--no-
    follow'.

## find empty directories

    fd -te -td

    fd -te -td -E build -x trash

    -te ................ type empty
    -td ................ type directory
    -E build ........... exclude build from the search
    -x --exec .......... execute command

    fd -e jpg -x convert {} {.}.png

## Useful options

    -u, --unrestricted
        Alias for '--no-ignore'. Can be repeated; '-uu' is an alias for
        '--no-ignore --hidden'.

    -0, --print0
        Separate search results by the null character
        (instead of newlines). Useful for piping results to
        xargs.

    -d, --max-depth d
        Limit directory traversal to at most d levels of
        depth. By default, there is no limit on the search depth.

Examples:

    - Only search for files:
        fd --type file …
        fd -tf …
    - Find both files and symlinks
        fd --type file --type symlink …
        fd -tf -tl …
    - Find executable files:
        fd --type executable
        fd -tx
    - Find empty files:
        fd --type empty --type file
        fd -te -tf
    - Find empty directories:
        fd --type empty --type directory
        fd -te -td

## Find files modified in the last two days

    fd --changed-within 2days
    ag.md
    awk.md
    comandoslinux.md
    dicasfirefox.md
    dicasnvim.md
    dwm.md

    fd . --changed-within=2d

    View images of the last 2 days
    sxiv $(fd --changed-within 2days)

## find with max depth

    fd -d1 -tf "i.*"
    agignore
    hidden
    inputrc

In the above search the `-d` flag determines how deep our search will be and
`-tf` means: type file. Finally the `i.*` is a regex that means any pattern that
starts with letter "i".

The following search looks for entries starting with an m and containing the
substring back at any other position. For more information on the regular
expression syntax, consult the regex documentation.

    fd '^m.*back.*$'

Typically, fd works in regexp mode and parses the search term as a regular
expression. However, adding the -g option forces fd to perform a glob-based
search. This causes it to only display entries that exactly match the search
term. In the following search, the backup directory matches, but wpbackup does
not perfectly match and is not listed.

    fd -g backup

## install fd

```sh
#!/bin/bash

# Função para verificar se o comando existe
comando_instalado() {
    command -v "$1" &>/dev/null
}

# Instalar dependências dependendo do sistema
instalar_dependencias() {
    echo "Instalando dependências..."
    
    # Verifica a distribuição e instala as dependências
    if comando_instalado "apt"; then
        sudo apt update && sudo apt install -y git curl build-essential
    elif comando_instalado "pacman"; then
        sudo pacman -Syu --noconfirm git base-devel curl
    elif comando_instalado "pkg"; then
        pkg update && pkg install -y git curl clang
    else
        echo "Sistema não suportado para instalação automática de dependências."
        exit 1
    fi
}

# Verificar se o git está instalado
if ! comando_instalado "git"; then
    echo "Git não está instalado. Instalando..."
    instalar_dependencias
fi

# Verificar se o Rust está instalado
if ! comando_instalado "cargo"; then
    echo "Rust não está instalado. Instalando..."
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
    source $HOME/.cargo/env
else
    echo "Rust já está instalado."
fi

# Clonar o repositório do fd
echo "Clonando o repositório do fd..."
git clone https://github.com/sharkdp/fd.git
cd fd

# Compilar o fd
echo "Compilando o fd..."
cargo build --release

# Verificar se a compilação foi bem-sucedida
if [ ! -f "target/release/fd" ]; then
    echo "Erro na compilação do fd. Verifique as mensagens acima."
    exit 1
fi

# Mover o binário para um diretório no PATH
echo "Instalando o fd..."
cp target/release/fd ~/.dotfiles/bin
# sudo mv target/release/fd /usr/local/bin/

# Verificar se o fd foi instalado corretamente
if comando_instalado "fd"; then
    echo "O fd foi instalado com sucesso!"
else
    echo "Erro ao instalar o fd."
    exit 1
fi

# Limpeza
cd ..
rm -rf fd

echo "Processo concluído!"

```


## References

+ [finding files with fd](https://www.linode.com/docs/guides/finding-files-with-fd-command/)

