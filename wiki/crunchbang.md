---
file: crunchbang.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Post installation script

	cb-wellcome

For crunchbang plus plus:

    cbpp-welcome

## app launcher synapse

 Is one awesome app to lauch outher apps
 easy configuration and fast work

   to add this repo do:

	sudo add-apt-repository ppa:synapse-core/ppa
	sudo apt-get update
	sudo apt-get install synapse
