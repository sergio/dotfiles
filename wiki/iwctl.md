# iwctl Commands for Managing WiFi Networks

NOTE: This program comes from iwd (i figure it out using xlocate)

`iwctl` is a command-line tool used to manage wireless networks. Here are the essential commands to list devices, scan for available networks, and connect to a WiFi network.

## 1. Start iwctl

To begin, open a terminal and start `iwctl`:

```bash
sudo iwctl
```

This enters the `iwctl` interactive shell.

## 2. List Available Devices

To view the available WiFi devices:

```bash
device list
```

This will display all wireless devices connected to your system, such as `wlan0`.

## 3. Scan for Available Networks

Replace `<device>` with the name of your wireless device (e.g., `wlan0`):

```bash
station <device> scan
```

After scanning, you can list the available networks:

```bash
station <device> get-networks
```

This will show all nearby WiFi networks, their SSIDs, signal strength, and security types.

## 4. Connect to a WiFi Network

To connect to a specific network, replace `<device>` with your wireless device and `<SSID>` with the network name:

```bash
station <device> connect <SSID>
```

If the network is password-protected, you will be prompted to enter the passphrase.

## 5. Verify the Connection

To confirm you are connected:
```bash
station <device> show
```

This will display connection details, including the connected network's SSID.

## 6. Exit iwctl

To exit the `iwctl` interactive shell, type:
```bash
exit
```

## Example Workflow

1. Start iwctl:

   ```bash
   sudo iwctl
   ```
2. List devices:

   ```bash
   device list
   ```

   Example output:

   ```
   Devices:
      wlan0  network  available
   ```

3. Scan for networks:

   ```bash
   station wlan0 scan
   station wlan0 get-networks
   ```

4. Connect to a network:

   ```bash
   station wlan0 connect MyWiFiNetwork
   ```

5. Verify connection:

   ```bash
   station wlan0 show
   ```

6. Exit iwctl:

   ```bash
   exit
   ```

These steps should allow you to manage 
WiFi connections easily using `iwctl`. 
For further information, refer to the `man` page or `help` command within the iwctl shell.
