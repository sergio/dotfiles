---
file: rm.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# rm.md

Há uma versão mais sergura do rm chamada [trash cli](trash-cli.md)

Para remover pastas deve-se usar a opção -r que indica recursividade, ou seja a
pasta e tudo que houver nas mesma.

    rm -rf pasta

## apagar arquivos que não correspondem a determinadas extensões

    shopt -s extglob

    rm !(*.foo|*.bar|*.baz)
    ls | grep -v '\.txt' | xargs rm -f

Delete all files with the exception of filename1 and filename2:

    rm -v !("filename1"|"filename2")

delete all the files except 'html' file all at once, in a smart way.

    rm -r !(*.html)

    parallel --no-notice rm -rf ::: ^(*converted)(.)

recursive remove

    parallel --no-notice  rm -f {} ::: ***/^(*-converted)(.)

## Apagar arquivos com espaço no nome (com confirmação)

    ls -Q * | xargs -p rm

A opção "-Q" do ls coloca todos os nomes entre aspas e a opção "-p"
do xargs exibe um prompt de confirmação
## remover aquivos terminados com 'ps'

    ls | grep -v ps$ | xargs rm

