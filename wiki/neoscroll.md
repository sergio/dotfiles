---
File: neoscroll.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [neovim, plugins]
---

#  neoscroll.md intro:

    use "karb94/neoscroll.nvim"

    use {
        "karb94/neoscroll.nvim",
        event = "WinScrolled",
        config = function()
            require('neoscroll').setup()
        end
    }

