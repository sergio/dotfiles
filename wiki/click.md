---
File: click.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [python, cli]
---

#  click.md intro:
+ https://click.palletsprojects.com/en/7.x/
+ https://click.palletsprojects.com/en/7.x/quickstart/#screencast-and-examples

Click is a Python package for creating beautiful command line interfaces in a composable way with as little code as necessary.

    sudo pip install click

## A little example

``` Python
import click

@click.command()
def main():
    print('I am the ad_notifier CLI')
```

