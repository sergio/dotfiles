---
File: lxdm.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [tags]
---

## lxdm - LXDE Display Manager (Login screen)
+ https://wiki.manjaro.org/index.php/LXDM_Configuration

## change background image:

    doas vim /etc/lxdm/lxdm.conf

    ## background of the greeter
    # bg=/usr/share/backgrounds/default.png

## man page
SYNOPSIS

       lxdm      [options]

DESCRIPTION

       lxdm  is  the LXDE display manager also known as login manager. It shows a graphical login
       screen for usernames and password and allows you to choose a desktop and language for  the
       session. After authenticating a user it starts a session.

       lxdm works with various desktops like LXDE, KDE and others.

OPTIONS

       -d     daemon mode

       -h     help

FILES

       /var/log/lxdm.log
              log information

       /etc/lxdm/Xsession
              script to run as user after login of user

       /etc/lxdm/default.conf
              configuration  file  for  lxdm.  It's  a  symlink  to  the  real configuration file
              (/etc/lxdm/lxdm.conf  for  standard   lxdm,   /etc/xdg/lubuntu/lxdm/lxdm.conf   for
              Lubuntu). See below for details of the configuration file.

CONFIGURATION FILE

       The  configuration  file  is  /etc/lxdm/default.conf,  which  is  a  symlink  to  the real
       configuration       file.       (/etc/lxdm/lxdm.conf       for       standard        lxdm,
       /etc/xdg/lubuntu/lxdm/lxdm.conf for Lubuntu).

       To  update  the  symlink,  type  "update-alternative  --config  lxdm.conf"  and follow the
       instructions.

       autologin

       Enable autologin if you set autologin=user, when user is your login. Disable  by  default,
       you need to uncomment the line.

       session

       Default  session  which will be started if "Default" session is provided at login. Disable
       by default, you need to uncomment the line.

       numlock

       Enable numlock support. Disable by default, you need to uncomment the line.

       arg

       Override arguments passed to the Xserver. Disable by default, you need  to  uncomment  the
       line.

       gtk_theme

       Select the gtk theme for the greeter.

       bg

       Select the path of the background of the greeter.

       bottom_pane

       Select if the bottom panel is available. 1 to TRUE, 0 to FALSE.

       lang

       Select if the language choose if available. 1 to TRUE, 0 to FALSE.

       theme

       Select the theme for the greeter. Theme are located in /usr/share/lxdm/themes/

AUTHORS

       This  manual  page  was  written  by  Trevor  Walkley <bluewater@sidux.com> for the Debian
       Project, but may be used by others. Permission  is  granted  to  copy,  distribute  and/or
       modify  this  document under the terms of the GNU General Public License, Version 2 or any
       later version published by the Free Software Foundation.

       On Debian systems, the complete  text  of  the  GNU  General  Public License can be  found
       in /usr/share/common-licenses/GPL.

                                            March 2010

