---
File: /home/sergio/.dotfiles/wiki/packer.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## Test if a plugin is loaded:
In this case LuaSnip

```lua
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

if packer_plugins['LuaSnip'] and packer_plugins['LuaSnip'].loaded then
    print('LuaSnip is loaded')
end

if packer_plugins['neovim-ayu'] and packer_plugins['neovim-ayu'].loaded then
    print('neovim-ayu loaded')
end


```

