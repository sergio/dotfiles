---
File: fzf.md
author: Sergio Araujo
Last Change: Wed, 20 Mar 2024 - 09:03:31
tags: [fuzzy, search, tools]
---

## References:

+ [fzf fuzzy file search](https://www.tecmint.com/fzf-fuzzy-file-search-from-linux-terminal/)

## fzf

Fzf is a tiny, blazing fast, general-purpose, and cross-platform command-line
fuzzy finder, that helps you to search and open files quickly in Linux and
Windows operating system. It is portable with no dependencies and has a
flexible layout with support for Vim/Neovim plugin, key bindings, and fuzzy
auto-completion.

## Multiple selection:

``` sh
xbps-query -m | fzf -m
```

Use Tab to select and unselect multiple itens then press Enter


## General use

``` sh
vim **<TAB>
```


## How to open files with Vim FZF

  ┌────────────────────────────────────┐
  │ Keybind      Action                │
  ├────────────────────────────────────┤
  │ Enter        Current Windows       │
  │ Ctrl-t       New Tab               │
  │ Ctrl-x       Horizontal Window     │
  │ Ctrl-v       Vertical Window       │
  └────────────────────────────────────┘

## Instalation

``` sh
git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
cd ~/.fzf/
./install
```

## opening files quick

``` sh
vim **<tab>
cat **<Tab>
kill -9 <Tab>
ssh **<Tab>

fzf --preview 'cat {}'
```


You can use this feature while working with environmental variables on the command-line.

``` sh
unset **<Tab>
unalias **<Tab>
export **<Tab>
```


## Ctrl-t to fill up arguments

``` sh
ls -l Ctrl-t
```

start typing filenames and hit TAB to add files to the list

## list of vim commands

``` markdown
vim commands
┌────────────────────┬────────────────────────────────────────────────────────────────┐
│   Command 		 │  List                                                          │
├────────────────────┼-───────────────────────────────────────────────────────────────┤
│   Files [PATH] 	 │  Files (similar to :FZF)                                       │
│   GFiles [OPTS] 	 │  Git files (git ls-files)                                      │
│   GFiles? 		 │  Git files (git status)                                        │
│   Buffers 		 │  Open buffers                                                  │
│   Colors 			 │  Color schemes                                                 │
│   Ag [PATTERN] 	 │  ag search result (ALT-A to select all, ALT-D to deselect all) │
│   Rg [PATTERN] 	 │  rg search result (ALT-A to select all, ALT-D to deselect all) │
│   Lines [QUERY] 	 │  Lines in loaded buffers                                       │
│   BLines [QUERY] 	 │  Lines in the current buffer                                   │
│   Tags [QUERY] 	 │  Tags in the project (ctags -R)                                │
│   BTags [QUERY] 	 │  Tags in the current buffer                                    │
│   Marks 			 │  Marks                                                         │
│   Windows 		 │  Windows                                                       │
│   Locate PATTERN 	 │  locate command output                                         │
│   History 		 │  v:oldfiles and open buffers                                   │
│   History: 		 │  Command history                                               │
│   History/ 		 │  Search history                                                │
│   Snippets 		 │  Snippets (UltiSnips)                                          │
│   Commits 		 │  Git commits (requires fugitive.vim)                           │
│   BCommits 		 │  Git commits for the current buffer                            │
│   Commands 		 │  Commands                                                      │
│   Maps 			 │  Normal mode mappings                                          │
│   Helptags 		 │  Help tags 1                                                   │
│   Filetypes 		 │  File types                                                    │
└────────────────────┴────────────────────────────────────────────────────────────────┘
```

## Useful commands

    Maps
    Commands

