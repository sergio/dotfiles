---
File: sigil.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: fev 15, 2021 - 09:54
tags: [epub, e-book, css, tools]
---

#  sigil.md intro:
+ [Video in Portuguese](https://www.youtube.com/watch?v=jgtNZvVyT5Q&t=475s)

The best epub creator for Linux

Export on Idesign > epub > epub reflowable

Every image should be places into div tag.

## Launch external xhtml editor

It is a button at the toolbar or pressing F2

## Generating internal links:
+ https://electricbookworks.github.io/ebw-training/making-ebooks/text/4-links.html

Just create:

    <a href="html-file.html">link name</a>


But what if we want to jump to a specific point in a chapter? For instance, to
a specific element like a paragraph or heading or image? We have to mark that
destination element somehow, so that we can point an href to it.

We do this by giving the element an ID. This is easy to do:

Make up an ID name (it must be unique in that HTML file), use no spaces,
avoid capital letters, and don’t start with a number, and include it as an
attribute in the element’s opening tag, like this: <p id="bobsyouruncle">

Then, when we make our link, we use that ID as the href, joined to the file
name with a hash sign:

    <a href="1-html.html#bobsyouruncle">Click here to go to the specific paragraph I'm linking to.</a>


## Shortcuts

    F3 ..................... jump to style
    Ctrl-click ............. jump to style
    Ctrl-Enter ............. create new page (page break)

## Boas práticas em css

Use 'em' ao invés de px para manter a coerência entre os objetos

## Indicar uma fonte local

``` css
@font-face {
    font-family: 'Linux Biolinum O';
    font-weight: normal;
    font-style: normal;
    font-variant: small-caps;
    src: url(../Fonts/LinBiolinum_R.otf);
}
```

## Plugins

### Docx import
+ https://www.mobileread.com/forums/showthread.php?t=273966

Is it possible to create a file that maps styles that come from word
using something like:

    p[style-name='credits'] => p.credits:fresh
    p[style-name='Block Text'] => blockquote

## Replacing everything between Tags
+ https://stackoverflow.com/a/16880892

    <footer>(.|\n)*?<\/footer>

## add the section numbers. Change the mode to Regex and use the following expressions:
+ http://www.mobileread.mobi/forums/showthread.php?t=246980

``` text
Find:<h2 id="sigil_toc_id_(\d+)">(.*?)</h2>
Replace:<h2 id="sigil_toc_id_\1">\2 \1</h2>
```

## usar títulos de seção como ids


``` sh
Abrir os arquivos para numerar
vim {003..020}*.xhtml

usar os títulos sem números em lowercase como id
silent! argdo :%s/\v(\<h2 id\=")([^"]*)("\>)(\d+\.\d+ - )([^<]*)(\<\/h2\>)/\1\L\5\3\4\5\6/g

Trocar os espaços nos ids por underscores
silent! argdo :%s/<h2 id="\zs[^"]*\ze/\=substitute(submatch(0), ' ', '_', 'g')/g
```

## Replace consecutive blank lines with one single lines

    Search this:      ^(\s*\n){2,}
    Replace by this:  \r

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0 spell spl=en:
-->
