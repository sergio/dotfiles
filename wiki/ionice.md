---
file: ionice.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# ionice.md - set or get process I/O scheduling class and priority
+ https://www.tecmint.com/delete-huge-files-in-linux/

    Last Change: mar 19 2019 08:11

## Deleting Huge Files in Linux

    ionice -c 3 rm /var/logs/syslog
    ionice -c 3 rm -rf /var/log/apache

