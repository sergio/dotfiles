---
file: mktemp.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [utils, tmp, tools]
---

## intro:
+ https://www.dicas-l.com.br/arquivo/criacao_de_arquivos_e_diretorios_temporarios_com_o_comando_mktemp.php

O comando mktemp permite criar arquivos temporários seguros

    tempdir=$(mktemp -d)
    echo $tempdir
    /tmp/tmp.nxU3HDjxpQ

    cd $(mktemp -d)

## Criando um arquivo temporário com um template

    tempfile=$(mktemp -t nome.XXXXXXXX)

O comando acima criará um arquivo temporário nomeado:

    "nome" Seguido de oito caracteres aleatórios


