---
file: html2text.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# pegando seu ip externo

		html2text http://checkip.dyndns.org | grep -i 'Current IP Address:'|cut -d' ' -f4

## veja também
[[ifconfig]]
