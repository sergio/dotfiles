---
file: xsel.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Referências: http://www.dicas-l.com.br/arquivo/copiando_textos_no_terminal.php

Opções do Xsel

    Usando a opção -i o texto copiado irá para as 3 áreas
    Usendo a opção -b o texto é copiado para o modo gráfico

---------------------------------
Colando um comando dentro do gvim
---------------------------------

    ls -l | xsel -bi

Chame o vim e use

    "+p

Em modo insert

    Ctrl-r Ctrl-o +
