# makepkg

---
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [archlinux, aur]
---

makepkg is a script to automate the building of packages. The requirements for using the script are a build-capable Unix platform and a PKGBUILD. makepkg is provided by the pacman package. 

## Installing AUR packages without AUR helpers

If you don’t want to use AUR helper, you can still install packages from AUR. You have to build them with a few commands.

As soon as you find the package you want to install on AUR page it is advised to confirm “Licence”, “Popularity”, “Last Updated”, “Dependencies” and so on as an extra quality control step.

```sh
git clone [package URL]
cd [package name]
makepkg -si
```
