---
file: getconf.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [arch, tools, admin]
---

  saber se o sistema é de 32 ou 64 bits

  getconf LONG_BIT
