---
File: regex-rename.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: mar 14, 2021 - 17:16
tags: [python, tools, regex, rename]
---

#  regex-rename.md intro:
+ https://pypi.org/project/regex-rename/

## Step 1: Testing matching pattern

Let's say you have these files:

    Staish aw+Lem+Invencible+(1).mp3
    Staish aw+Lem+Invencible+(2 ).mp3
    Staish aw+Lem+Invencible+(3).mp3

Our Regex pattern to match those files and extract number from parentheses should be like this: `.+\((\d+) ?\).+`
Let's test matching pattern: regex-rename `'.+\((\d+) ?\).+'`

The result will be:

    [2021-03-01 19:07:37] [DEBUG] matching regex pattern testing_mode=True pattern=".+\((\d+) ?\).+" replacement=None full_match=False padding=None
    [2021-03-01 19:07:37] [INFO ] matched file="Staish aw+Lem+Invencible+(1).mp3" group_1=1
    [2021-03-01 19:07:37] [INFO ] matched file="Staish aw+Lem+Invencible+(2 ).mp3" group_1=2
    [2021-03-01 19:07:37] [INFO ] matched file="Staish aw+Lem+Invencible+(3).mp3" group_1=3
    [2021-03-01 19:07:37] [DEBUG] files matched count=3

    regex-rename '.+\((\d+) ?\).+' '\1 The Invincible.mp3' --pad-to=2 --rename

## Adding leading zeros:

Let's say you have:

    file-1.txt
    file-2.txt
    file-3.txt

    regex-rename --rename '(file-)(\d+)(\.txt)' '\1\2\3' --pad-to=3

# vim: ft=markdown et sw=4 ts=4 cole=0
