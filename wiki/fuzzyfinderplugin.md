---
file: fuzzyfinderplugin.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# atalhos legais do fuzzyfind

  Ctrl-t ........... altera o modo do plugin
  Ctrl-y ........... retrocede o modo (altera)
  Ctrl-w ........... apaga o conteúdo antes do cursor
  Ctrl-s ........... pega o padrão anterior (padrão)

  Para ocultar a complementação do fuzzyfinder
  temporariamente

  Ctrl-e

  para exibir novamente

  Ctrl-x Ctrl-o

tag: vim, nvim
