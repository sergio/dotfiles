---
file: duckduckgo.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# duckduckgo search engine

## fine tune your search

Example 	            Result
cats dogs 	            Results about cats or dogs
"cats and dogs" 	    Results for exact term "cats and dogs"
cats -dogs 	            Fewer dogs in results
cats +dogs 	            More dogs in results
cats filetype:pdf 	    PDFs about cats. Supported file types: pdf, doc(x), xls(x), ppt(x), html
dogs site:example.com 	Pages about dogs from example.com
cats -site:example.com  Pages about cats, excluding example.com
intitle:dogs 	        Page title includes the word "dogs"
inurl:cats 	            Page url includes the word "cats"

## Timer and stopwatch
Make a search and click on "answer"

## qr code

    qr string to generate

## uppercase or lowercase

    "uppercase" my phrase

## cheat sheet

    neovim cheat sheet

## wheather

    wheather fortaleza

## url shortner

    shorten https://www.thewindowsclub.com/duckduckgo-search-tips-tricks

## password generator

    password 10

## html chars

    html chars
