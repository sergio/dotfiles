---
file: stackexchange.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## stackexchange.md

O Stack Exchange tem vários canais de discussão
sobre diversos assuntos: vim, bash, zsh, awk, sed
e por aí vai.

## Shortcuts

    just press 'g' and the the letter that follows

## acessar o stackoverflow via linha de comando:
+ https://ostechnix.com/search-browse-stack-overflow-website-commandline/

    pipx install so socli

## change theme:

    https://stackoverflow.com/users/preferences/

## Dark theme
+ https://meta.stackoverflow.com/questions/395949

visit your Stack Overflow profile, go to the "Edit profile and settings" tab,
scroll down to "Preferences" under "Site Settings" and find the "Theme"
section at the top of the page. You have three options - light, dark and
"system setting" which will adjust the theme based on what your system
prefers.

## list stackoverflow top awk users

    https://stackoverflow.com/tags/awk/topusers

## Atalhos de teclas nos posts

Quando necessitamos postar uma resposta os atalhos de tecla
podem ser referenciados da seguinte forma:

```markdown
<kbd>Ctrl+V</kbd><kbd>Tab</kbd>x
```

ou seja usando a marcação <kbd> Ctrl-V </kbd>

## Encurtando urls to StackExchange - shortening StackExchange urls

Urls do tipo:

    http://meta.stackexchange.com/questions/23834/official-shortened-url-service

Podem ter títulos apagados e ainda assim vão apontar pro mesmo endereço

    http://meta.stackexchange.com/q/23834/

    e a palavra question pode ser abreviada para "q"

    quando se tratar de uma resposta "answer" pode-se abreviar para "a"

## Inserting ttyrecordings at stackexchange

    [![asciicast](https://asciinema.org/a/14.png)](https://asciinema.org/a/14) https://asciinema.org/docs/embedding

## Text formating
if you type `Ctrl-k` it will format as source code

To insert

		> text here: Places all text within the paragraph enclosed in a yellow box
		>! text here: Places all within the paragraph enclosed in a yellow box that

## Users mention
If you just want to mention a user in your question, you should link to their profile.

```markdown
[@name](http://some-url)
```

## Formating your code on StackExchange
+ https://meta.stackexchange.com/questions/22186

You can simply use backticks like in any markdown code

    ```language
    your code here
    ```
