---
File: ~/.dotfiles/wiki/marcenaria.md
Last Change: Sun, 08 Sep 2024 - 13:26:25
tags: [madeira, marcenaria, tools]
---

# Serrotes japoneses

- [Ryoba duplo dente](https://www.lojaempoeirados.com.br/serrote-ryoba-japones-life-dy-250mm-duplo-dente)
- [Outros modelos](https://youtu.be/9CM_LJzz9yc?si=MrpkOwo2JgrnoayF)
- [Mão francesa](https://youtu.be/Qqa86eSl3qo?si=P5SSItm55ELNFPKW)
- [Serrote japonês não é chinez](https://youtu.be/olFAKswD64A?si=KW8aor18cfOet9Ln)
- [serrote](https://www.lojaempoeirados.com.br/serrote-ryoba-japones-life-dy-250mm-duplo-dente)

Acabei de encontrar este item incrível no AliExpress. Dá uma olhada! R$38,59
40%de desconto | 1pc ryoba japonês viu 3-edge dente puxar serras flexível flush
corte serra de mão para tenon madeira corte fino carpintaria
https://a.aliexpress.com/_mNvqg2w

## Morsa de bancada

[YouTube vídeo](https://youtu.be/gi8YEMHGZmw)
[Wooden toggle clamp](https://youtu.be/kAofBxgkizE?si=8fCgJtxMQTv1jkqB)
[Another toggle clamp](https://youtu.be/sbIUQ5Sqnqg?si=065QC6WPTc4I0xTO)
[Toggle clamp 2](https://youtu.be/L9eDfBppqHE?si=ZjXWw8gIAE6xBYsL)
[Watch this](https://youtu.be/oyL0w1aVDP8?si=CJhK80o7FSbNwYzb)
[And this](https://youtu.be/T8CtHnCZjzg?si=96gdhGyC4CeEI3D_)
[Homemade toggle clamp](https://youtu.be/LYuKDKjYEw4?si=LQgwS5nxmcTfevUm)
[Another project](https://youtu.be/wkHfMTwZmmk?si=BUQ9clCddfkU2a7b)

## Sargento de madeira

[No youtube](https://youtu.be/u_2WygXjZaA)

## Guia para furadeira

[YouTube vídeo](https://youtu.be/9gQrBysTD00?si=zdz555lpqvbQe-_T)
[Another video](https://youtu.be/O0s6Sq8mGyk)
[A melhor que achei](https://youtu.be/vuqOVGn5Eto)

## Fazendo fuso

[video no YouTube](https://youtu.be/CzbXYsKm71k)
[Video 2](https://youtu.be/IbrueIE4YRk)

## Tupia laminadora

[YouTube](https://youtu.be/sJQv7LBAvyM)

