---
file: diff.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Mostrando as diferenças entre duas árvores de diretórios

## Differences between dir1 and dir2

    diff <(cd dir1 && find | sort) <(cd dir2 && find | sort)

Outro modo de fazer o mesmo

    diff -urp /originaldirectory /modifieddirectory

    diff -Naur dir1/ dir2/

    The -u option makes the output a little easier to read.
    The -r option recurses through all subdirectories
    The -N and -a options are really only necessary if you wanted to create a patch file.

## Real life diff use:
I want to copy only the missing 'jpg' files at the ~/tmp/wallpapers

    diff -u ~/img/backgrounds ~/tmp/wallpapers

    Somente em /home/sergio/tmp/wallpapers: .git
    Somente em /home/sergio/tmp/wallpapers: .gitignore
    Somente em /home/sergio/tmp/wallpapers: README.md
    Somente em /home/sergio/tmp/wallpapers: thumbs
    Somente em /home/sergio/img/backgrounds: wallpaper-1791868.jpg
    Somente em /home/sergio/img/backgrounds: wallpaper-2099153.jpg
    Somente em /home/sergio/img/backgrounds: wallpaper-2226037.jpg

Using awk with two field separators '[: ]' I will get only the third and fifth fields to
compose a copy command:

    diff -u ~/img/backgrounds ~/tmp/wallpapers | awk -F'[: ]' '/jpg/ {print $3"/"$5}'

    /home/sergio/img/backgrounds/wallpaper-1791868.jpg
    /home/sergio/img/backgrounds/wallpaper-2099153.jpg
    /home/sergio/img/backgrounds/wallpaper-2226037.jpg

Now I only have to add some strings to my awk command and pipe the result to the shell

    "cp " ............... copy comand with space
     $3  ................ the third fild - the basename
     "/" ................ add the slash so we do not mess things up
     $5 ................. the missing file name
     " ~/tmp/wallpapers"  destination folder
     | sh ................ pipe the preceding command to the shell

    diff -u ~/img/backgrounds ~/tmp/wallpapers \
    | awk -F'[: ]' '/jpg/ {print "cp " $3"/"$5" ~/tmp/wallpapers/"}' | sh

## using nvim diff mode:

    nvim -d file1 file2
    :diffget
    :diffput
    do             same as :diffget without range
    dp             same as :diffput without range
    ]c
    [c


## I am trying to find the files in dir1 but not in dir2 only.
+ https://stackoverflow.com/a/16788549/2571881
+ https://stackoverflow.com/a/24695424/2571881

    diff -r dir1 dir2 | grep dir1 | awk '{print $4}' > difference1.txt

    comm -23 <(ls dir1 |sort) <(ls dir2|sort)

This command will give you files those are in dir1 and not in dir2.

## Usando substituição de processos

    diff <(echo hello; echo there) <(echo hello; echo world)
    2c2
    < there
    ---
    > world

## comparando o conteúdo de duas variáveis

``` bash
diff <(echo "$a") <(echo "$b")
```

## diferenças entre dois comandos

    diff <(tail -10 file1) <(tail -10 file2)

## guardando as diferenças entre dois arquivos

    diff frutas1.txt frutas2.txt > diferenca.001.patch

## aplicando as diferenças "patch"

    patch frutas1.txt < diferenca.001.patch

## sing diff to create a simple patch
+ https://linuxacademy.com/blog/linux/introduction-using-diff-and-patch/
+ http://www.thegeekstuff.com/2014/12/patch-command-examples

The most simple way of using diff is getting the differences between two
files, an original file and an updated file. You could, for example, write a
few words in a normal text file, make some modifications, and then save the
modified content to a second file. Then, you could compare these files with
diff, like this:

    diff originalfile updatedfile

Of course, replace originalfile and updatedfile with the appropiate filenames
of your case. You will most probably get an output like this:

    1c1
    < These are a few words.
    No newline at end of file
    —
    > These still are just a few words.
    No newline at end of file

Creating a patchfile

	diff originalfile updatedfile > patchfile.patch
	patch originalfile -i patchfile.patch -o updatedfile
	patch -i patchfile.patch -o updatedfile

## Other tips from LuckSmithxyz:
+ https://www.youtube.com/watch?v=-CiLU9-RAGk

< linhas que estão no arquivo mais novo
> linhas que estão no arquivo original
< mais linhas no novo arquivo

    # gera um patch
    diff -u original new > new.diff

    patch < new.diff     ........... applays diff
    patch -R < new.diff  ........... removes diff

## Referências
* http://www.commandlinefu.com/commands/browse
* http://leonardobighi.com/linux/31/usando-diff-e-patch-pra-agilizar-seu-trabalho

