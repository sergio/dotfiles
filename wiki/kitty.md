---
File: /home/sergio/.dotfiles/wiki/kitty.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## kitty terminal emulator

- https://raw.githubusercontent.com/koekeishiya/dotfiles/master/kitty/kitty.conf
- https://www.reddit.com/r/commandline/comments/jyc7wk/mastering_kitty_terminal/

## transparency

    # https://www.reddit.com/r/linux4noobs/comments/99s6tm/comment/e4qc0io/
	background_opacity         0.6
	dynamic_background_opacity yes

## theme
+ https://github.com/dexpota/kitty-themes

    THEME=https://raw.githubusercontent.com/dexpota/kitty-themes/master/themes/AtomOneLight.conf
    wget "$THEME" -P ~/.config/kitty/kitty-themes/themes
    cd .config/kitty
    ln -sfvn ./kitty-themes/themes/AtomOneLight.conf ~/.config/kitty/theme.conf


Add this line to your kitty.conf configuration file:

    include ./theme.conf

## keyboard shortcuts

	Ctrl+Shift+Enter .................. new terminal window
	Ctrl+Shift+n ...................... new os window
	Ctrl+Shift+b ...................... window backward
	ctrl+shift+c ...................... copy to clipboard     alt-c
	ctrl+shift+v ...................... paste from clipboard  alt-v
	ctrl+shift+F5 ..................... reload
	ctrl+shift+=  ..................... increase font size
	ctrl+shift+-  ..................... decrease font size
	ctrl+shift+F2 ..................... edit config file
	ctrl+shift+F5 ..................... reload kitty.conf
