---
file: lspci.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

O comando lspci lista os dispositivos como video audio etc

Combinado com o grep podemos listar apenas a marca da placa de vídeo

lspci | grep VGA
