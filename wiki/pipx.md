---
File: /home/sergio/.dotfiles/wiki/pipx.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## Tool to install python programs:
+ https://pypi.org/project/pipx/
+ https://www.mankier.com/1/pipx

Install and Run Python Applications in Isolated Environments

	python3 -m pip install --user pipx
	python3 -m pipx ensurepath

    Run an app in a temporary virtual environment:

        pipx run pycowsay moo

    Install a package in a virtual environment and add entry points to path:

        pipx install package

    List installed packages:

        pipx list

    Run an app in a temporary virtual environment with a package name different from the executable:

        pipx run --spec httpx-cli httpx http://www.github.com

Subcommands

    pipx install --> Install a package

    pipx inject  --> Install packages into an existing Virtual Environment

    pipx upgrade --> Upgrade a package

    pipx upgrade-all --> Upgrade all packages. Runs `pip install -U <pkgname>` for each package.

    pipx uninstall --> Uninstall a package

    pipx uninstall-all --> Uninstall all packages

    pipx reinstall --> Reinstall a package

    pipx reinstall-all --> Reinstall all packages

    pipx list --> List installed packages

    pipx run

    Download the latest version of a package to a temporary virtual environment, then run an app from it. Also compatible with local `__pypackages__` directory (experimental).
pipx runpip

    Run pip in an existing pipx-managed Virtual Environment

    pipx ensurepath --> Ensure directories necessary for pipx operation are in your PATH environment variable.

    pipx completions --> Print instructions on enabling shell completions for pipx

## google keep via command line (cli):

    pipx install gkeep

## vintual envs course:
+ https://realpython.com/lessons/what-about-package-manager/

Install pyenv:

    curl https://pyenv.run | zsh

