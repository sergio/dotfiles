---
File: /home/sergio/.dotfiles/wiki/fontconfig.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [fonts, utils, fc-list, fc-cache]
 vim:ft=markdown
---

## Fontconfig provides fc-list and fc-cache

## Searching for a specific ChangeFont

    fc-list | grep -i "hack"

In order to set font in this way

    st -f 'Hack-Regular:size=15'


## just show font names:

    fc-list : family
