---
file: setxkbmap.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [tools, admin, hardware, keyboard, keymappings]
---

## Using setxkbmap
+ https://wiki.archlinux.org/title/Xorg/Keyboard_configuration#Using_X_configuration_files
+ http://blog.ssokolow.com/archives/2011/12/24/getting-your-way-with-setxkbmap/

setxkbmap sets the keyboard layout for the current X session only, but can be made persistent in ~/.xinitrc or xprofile. This overrides system-wide configuration specified following #Using X configuration files.

The usage is as follows (see setxkbmap(1)):

    $ setxkbmap [-model xkb_model] [-layout xkb_layout] [-variant xkb_variant] [-option xkb_options]

To change just the layout (-layout is the default flag):

    setxkbmap xkb_layout

For multiple customizations:

    setxkbmap -model pc104 -layout cz,us -variant ,dvorak -option grp:alt_shift_toggle

    setxkbmap br

    # into ~/.xinitrc add the line bellow (it makes your keyboard settings permanent)
    setxkbmap -model abnt2 -layout br -variant abnt2

    OBS: If you are using any login manager it will not read your xinitrc, but you can
    create a symlink to ~/.profile or ~/.xprofile

    ln -sfvn ~/.xinitrc ~/.xprofile

## show setxkbmap settings

    setxkbmap -print
    setxkbmap -print -verbose 10

## other options

    setxkbmap -query | grep layout

    doas xbps-install -Sy xkb-switch

    xkb-switch -l
    xkb-switch -s pt_BR

