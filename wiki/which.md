---
file: which.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## O comando which é usado para localizar comandos

which python
/usr/bin/python

## Getting a function definition

    which mkcd

output

    mkcd () {
        mkdir -pv -p "$@" && cd $_
    }

Another option

    declare -f mkcd

    mkcd () {
        mkdir -pv -p "$@" && cd $_
    }
