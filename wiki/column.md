---
file: column.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

O comando abaixo formata a saida do arquivo data.csv com espaços uniformes

    column -t data.csv

## Align text by a specifi char
+ https://stackoverflow.com/questions/57093175/

let's say you have this:

    const service = require('./service');
    const baseService = require('./baseService');
    const config = require('../config');
    const Promise = require('bluebird');
    const errors = require('../errors');

And you want this:

    const service     = require('./service');
    const baseService = require('./baseService');
    const config      = require('../config');
    const Promise     = require('bluebird');
    const errors      = require('../errors');

Solution:

    cat file | column -t -s= -o=

[[media type="youtube" key="ZdOFU8MX0QQ" height="385" width="480"]]
[[media type="youtube" key="kVFYvyiUDT0" height="385" width="640"]]

