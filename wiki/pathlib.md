---
File: pathlib.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: mai 01, 2020 - 15:50
tags: [python, libraries]
---

#  pathlib.md intro:

## Getting the absolute path of a file or dir
+ https://bit.ly/2yI0tCb

``` python
    import pathlib
    path = pathlib.Path('.')
    full_path = path.absolute()
    my_path = full_path.as_posix()
    print(my_path)

    /home/victor

    type(my_path)
    str
```

## Getting the name and extension

``` python
from pathlib import Path
path = Path.home() / 'Downloads'

for f in path.iterdir():
    name, extension = f.stem, f.suffix
    print(f'{name} {extension}')

# file list:
flist = [p for p in pathlib.Path('.').iterdir() if p.is_file()]
```

## Creating a new directory

    from pathlib import Path

    new_dir = Path.home() / 'tmp' / 'test'

    new_dir.mkdir()

NOTE: we can add an option in order to create parents mkdir
      To delete a file we can go `f.unlink()`
      To delete a directory `folder.rmdir()`

    new_dir.mkdir(parents=True)

## Reading file contents
+ https://bit.ly/2S7v6I4

For example, we can read the contents of a text file without having to mess
with opening and closing the file:

``` python
from pathlib import Path

data_folder = Path("source_data/text_files/")

file_to_open = data_folder / "raw_data.txt"

print(file_to_open.read_text())
```

## listing directories

    from pathlib import Path

    folder = Path.home() / 'Downloads'

    [ str(x) for x in folder.iterdir() if folder.is_dir() ]

### list recursivelly

``` python
from pathlib import Path
path = Path.home() / '.dotfiles'
for x in path.rglob('*.md'):
    print(x.name)
```
