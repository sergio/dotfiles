---
file: vimeo.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---


# Downloading private Vimeo videos
+ https://mpsocial.com/t/downloading-private-vimeo-videos/21450

I prefer to do these things in Chrome but it works in every other browsers which have developer tools.

So navigate to the page where there is the video embedded.
Open Developer tools (Ctrl + shift + i; or on Windows it’s F12)
Choose Elements tab.
Press the first button which contains a cursor (“Select an element in the page to inspect it”) and click on the video itself.
This will select the video element or the thumbnail image from the HTML (DOM) code in the Developer tools.
Scroll down a little and just a few lines below the video parts you will find a script tag. Copy its content to a text editor.
You can easily find the mp4 video URL with text search in your text editor.

    search for: video/mp4
    then right after that copy the url content
