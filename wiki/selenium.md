---
file: ~/.dotfiles/wiki/selenium.md
author: Sergio Araujo
Last Change: Tue, 01 Oct 2024 - 01:45:44
abstrac: Automate your browser
---

#  selenium.md intro:


``` python
from time import sleep
from selenium import webdriver

browser = webdriver.Firefox()
browser.get('https://www.instagram.com/accounts/login/')

sleep(5)

browser.close()
```
## video sobre o selenium:

- [No youtube](https://youtu.be/8AMNaVt0z_M)
