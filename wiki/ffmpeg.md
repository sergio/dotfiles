---
file: ~/.dotfiles/wiki/ffmpeg.md
Last Change: Tue, 11 Jun 2024 - 15:19:15
tags: [tools, mp3, audio, video, convert]
---

## Referencias
+ http://pajeonline.blogspot.com/2009/05/como-converter-videos-no-linux.html
+ https://itsfoss.com/ffmpeg/

## detect your webcam:

+ https://trac.ffmpeg.org/wiki/Capture/Webcam

    v4l2-ctl --list-devices
    Laptop_Integrated_Webcam_HD: In (usb-0000:00:1d.0-1.5):
        /dev/video0
        /dev/video1
        /dev/media0

    Record webcam

    ffmpeg -f v4l2 -framerate 25 -video_size 440x280 -i /dev/video0 output.mkv

## start time to end time:
+ https://askubuntu.com/a/58707/3798

    ffmpeg -i input.mp3 -vn -acodec copy -ss 00:00:00 -to 00:01:32 output.mp3

## Normalize sound track - How can I normalize audio using ffmpeg?

+ https://superuser.com/questions/323119/

    ffmpeg -i input.wav -af "volume=5dB" output.mp3

### Increase audio volume

+ https://www.maketecheasier.com/ffmpeg-commands-media-file-conversions/

    ffmepg -i quiet-song.mp3 -af "volume=2.0" louder-song.mp3

### Increase audio of a video file

+ https://breakthebit.org/post/53570840966/how-to-increase-volume-in-a-video-without

To increase the volume of the first audio track for 10dB use:

    ffmpeg -i inputfile -vcodec copy -af "volume=10dB" outputfile

To decrease the volume of the first audio track for 5dB use:

    ffmpeg -i inputfile -vcodec copy -af "volume=-5dB" outputfile

## convert opus to mp3

    ffmpeg -i filename.opus -ab 320k newfilename.mp3

## denoise (remove noise)



## convert m4a to mp3 recursively

```bash
## /usr/bin/bash
## convert .m4a to .mp3 recursively
## Based on Glen Hewlett script:
## https://nowhereman999.wordpress.com/2011/09/11/convert-all-files-recursively-from-one-format-to-another-ie-flac-to-mp3-using-linux/
#####################################

if [ -z $1 ];then echo Give target directory; exit 0;fi
    find "$1" -depth -name '*' | while read file ; do
    directory=$(dirname "$file")
    oldfilename=$(basename "$file")
    #newfilename=$(basename "${file%.[Ff][Ll][Aa][Cc]}")
    newfilename=$(basename "${file%.[Mm]4[Aa]}")

    if [ "$oldfilename" != "$newfilename" ]; then
        ffmpeg -i "$directory/$oldfilename" -ab 320k "$directory/$newfilename.mp3" </dev/null
        rm "$directory/$oldfilename"
    fi
done
```

## oga to mp3:

    ffmpeg -i file.{oga,mp3}

## Recording your screen

+ https://trac.ffmpeg.org/wiki/Capture/Webcam
+ https://trac.ffmpeg.org/wiki/Capture/Desktop
+ https://www.youtube.com/watch?v=386Z2yeQ5fo (Luck Smith youtube)

Screencasting with ffmpeg is not a hard job. The format (-f) you need to use
is x11grab. This will capture your XSERVER. As input you’ll have to specify
your screen display number (main screen should normally be 0:0). However, this
will only capture a top-left portion of the screen. You should add a screen
size (-s). Mine is 1920×1080. The screen size should be mentioned before the
input:

    ffmpeg -f x11grab -s 1920x1080 -i :0.0 output.mp4

Press q or CTRL+C at any time to stop the screencast.

Bonus trick: You can make the size of the output file fullscreen with
inputting this after for the size (instead of 1920×1080 or any other set
resolution):

    -s $(awk '/dimensions/ {print $2}' <(xdpyinfo))

Here’s the full command:

    ffmpeg -f x11grab -s $(awk '/dimensions/ {print $2}' <(xdpyinfo)) -i :0.0 output.mp4
    ffmpeg -s $(awk '/dimensions/ {print $2}' <(xdpyinfo)) -framerate 25 -f x11grab -i :0.0 -f pulse -ac 2 -i default output.mkv

NOTE: you can use the `-f` option to automatically overwrite files with the same
name.

## Record Your Webcam

Recording input from your webcam (or another device, such an usb camera) is
even easier. In Linux, devices are stores in /dev as /dev/video0, /dev/video1,
etc.:

    ffmpeg -i /dev/video0 output.mp4

Again, q or CTRL+C to stop recording.

## Recording audio from mic and speakers from commandline

+ https://ask.fedoraproject.org/en/question/65662/

Finally I found a way to achieve what I wanted, using the PulseAudio utility "pacmd".
This tutorial help me

    pacmd list-sources|awk '/index:/ {print $0}; /name:/ {print $0}; /device\.description/ {print $0}'

Run above command to get the names of all the recording interfaces. Sample output is given below.

    index: 0
    name: <alsa_output.pci-0000_00_1b.0.analog-stereo.monitor>
        device.description = "Monitor of Built-in Audio Analog Stereo"
  * index: 1
    name: <alsa_input.pci-0000_00_1b.0.analog-stereo>
        device.description = "Built-in Audio Analog Stereo"

To record from my Speakers: use "Monitor of Built-in Audio Analog Stereo"

    ffmpeg -f pulse -i alsa_output.pci-0000_00_1b.0.analog-stereo.monitor -c:a libmp3lame output.mkv

To record from my Mic: use "Built-in Audio Analog Stereo"

    ffmpeg -f pulse -i alsa_input.pci-0000_00_1b.0.analog-stereo -c:a libmp3lame output.mkv

## substituir o audio de um video

    ffmpeg -i audio.mp3 -i video.mp4 -c copy final-video.mp4

List devices

    v4l2-ctl --list-devices

List device capabilities

    ffmpeg -f v4l2 -list_formats all -i /dev/video0

Usage example

    ffmpeg -f v4l2 -framerate 25 -video_size 640x480 -i /dev/video0 output.mkv

## remove audio from video

	ffmpeg -i grade-metalica.webm -an -vcodec copy output.webm

## merging audio and video

    ffmpeg -i audio.wav -i video.mp4 -acodec copy -vcodec copy output.mp4

## Converter ogv para flv

    ffmpeg -i input.ogv -ar 44100 -acodec libmp3lame output.flv

## convert ogg para mp3

    ffmpeg -i audio.ogg -acodec mp3 newfile.mp3

## split audio files (use mp3splt command)
+ https://askubuntu.com/a/1264853/3798

Usage using Silence mode (-s):

    mp3splt -s your.mp3

Mp3splt will try to automatically detect splitpoints with silence detection and will split all tracks found with default parameters.

Or

    mp3splt -s -p th=-50,nt=10 your.mp3

passing desired parameters, splitting 10 tracks (or less if too much) with the most probable silence points at a threshold of -50 dB

## split viedeo:

+ https://askubuntu.com/a/35645/3798

    ffmpeg -i ORIGINALFILE.mp4 -acodec copy -vcodec copy -ss START -t LENGTH OUTFILE.mp4

Where START is starting positing in seconds or in format hh:mm:ss LENGTH is the chunk length in seconds or in format hh:mm:ss

There is a python script that you can use that does this automatically(i.e. takes video file, chunk size in seconds and generates individual playable video files): https://github.com/c0decracker/video-splitter


## Convert vob to avi

    sudo apt-get install libavcodec52  libavutil49
    ffmpeg -i myVideo.vob myvideo.avi

## convert mp4 to avi:

+ https://video.stackexchange.com/questions/20399
+ https://youtu.be/tWi6QA4CBPw

    ffmpeg -i input.mp4 -c:v libx264 -c:a libmp3lame -b:a 384K output.avi

    REDUCING SIZE (COMPRESS)
    ------------------------

    ffmpeg -i inputfile.mp4 -acodec mp2 outputfile.mp4

## Convert amr audio to mp3

    ffmpeg -i audio1.amr -ar 22050 audio1.mp3

## conversao simples

    ffmpeg -i arquivo.wmv arquivo.avi

O parâmetro "-i" indica o arquivo fonte. O formato do arquivo de saída é automaticamente reconhecido pelo ffmpeg, a partir da sua extensão.

    ffmpeg -i input.flv -sameq -r ntsc outputfile.mpeg

## outro modo
fonte: http://www.quartoestudio.com/blog/ubuntu/tutorial-como-gravar-um-screencast-no-linux-usando-o-terminal-e-o-ffmpeg/

    ffmpeg -f x11grab -s 1280x1024 -r 30 -i :0.0 /tmp/screencast.mpg

## extraindo som de arquivos flash

    ffmpeg -i <filename>.flv -vn <filename>.mp3

## extract audio from video

    ffmpeg -i video.avi -f mp3 audio.mp3

## Add Poster Image to Audio

This is a great way to turn audio into video, using a single photo (such as a
album cover) for the audio. This is a very useful feature when you want to
upload audio files to sites that don’t allow anything other than video/images
(YouTube and Facebook are two examples of such sites).

Here is an example:

    ffmpeg -loop 1 -i image.jpg -i audio.wav -c:v libx264 -c:a aac -strict experimental -b:a 192k -shortest output.mp4

Just change the codecs (-c:v specifies video codecs, -c:a specifies audio codecs) and the names of your files.

Note: You don’t have to use -strict experimental if you are using a newer version (4.x).

## Comandos prontos do dicas-l

* fonte: http://www.dicas-l.com.br/dicas-l/20100312.php
