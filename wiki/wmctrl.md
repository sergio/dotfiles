---
File: wmctrl.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [wm, screen, desktop, tools]
---

#  wmctrl.md intro:
+ https://linux.die.net/man/1/wmctrl

    wmctrl - interact with a EWMH/NetWM compatible X Window Manager.

wmctrl is a command that can be used to interact with an X Window manager that is compatible with the EWMH/NetWM specification. wmctrl can query the window manager for information, and it can request that certain window management actions be taken.

wmctrl is controlled entirely by its command line arguments. The command line arguments are used to specify the action to be performed (with options that modify behavior) and any arguments that might be needed to perform the actions.

## Get the name of running window manager:

    wmctrl -m  | awk '/Name:/ {print $2}'

You can also get the content of this var: `$XDG_CURRENT_DESKTOP`

## jump to an already opened firefox window:
+ https://stackoverflow.com/a/34797384/2571881

    wmctrl -l | awk '/Firefox/ {print $1}'
    wmctrl -l | awk '/Firefox/ {print strtonum($1)}'

    wmctrl -a Firefox
