---
file: shade-nvim.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Dims your inactive windows, making it easier to see the active window at a glance.
