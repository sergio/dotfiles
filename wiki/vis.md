---
File: vis.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: fev 07, 2021 - 07:05
tags: [editor, vim, nvim, tools]
---

#  vis.md intro:
+ http://martanne.github.io/vis/man/vis.1.html

Vis aims to be a modern, legacy-free, simple yet efficient editor,
combining the strengths of both vi(m) and sam.

It extends vi's modal editing with built-in support for multiple
cursors/selections and combines it with [sam's](http://sam.cat-v.org/) [structural regular expression](http://doc.cat-v.org/bell_labs/structural_regexps/)

based [command language](http://doc.cat-v.org/bell_labs/sam_lang_tutorial/).
