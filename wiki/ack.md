---
file: ack.md
author: Sergio Araujo
Last Change: Thu, 07 Nov 2024 - 10:30:57
---

# Para baixar o comando

    curl http://betterthangrep.com/ack-standalone > ~/bin/ack && chmod 0755 !#:3
