---
File: /home/sergio/.dotfiles/wiki/streamripper.md
Last Change: Mon, 12 Dec 2022 - 19:04:05
vim:cole=0:ft=markdown:
tags: [audio, mp3, stream, radio]
---

## Record online streams

```sh
streamripper "http://stm3.xcast.com.br:8560/stream" -a -A -r -s -l 60 -u "FreeAmp/2.x" -d radio
```

a opção r permite você ouvir o que está sendo ripado
com  https://localhost:8000


     Rip from a stream:

        streamripper URL

       Rip from a stream for one hour:

        streamripper URL -l 3600

       Rip the stream, putting the mp3 files into the directory /my/music/stream1:

        streamripper URL -d /my/music/stream1 -s

       Rip the stream, creating a single file and don´t create individual tracks:

        streamripper URL -a -A

       Rip from a stream and create a relay stream at port 9000:

        streamripper URL -r 9000

       Rip from a stream, creating a relay stream at port 8000, and allowing twenty clients to
       connect:

        streamripper URL -r -R 20

