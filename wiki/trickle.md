---
file: trickle.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Limitando o apt update a 10k

    trickle -s -d11 apt-get update

## Limitando downloads no firefox a 15k

    trickle -s -d15 firefox

## Trickle with git
That is useful for download large projects

    trickle -sd 50 git clone
    trickle -sd 50 git clone https://github.com/ryanoasis/nerd-fonts.git

## Referências
* http://linuxhard.org/site/archives/1044

tags: tools, network
