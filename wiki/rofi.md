---
File: /home/sergio/.dotfiles/wiki/rofi.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [utils, rofi, bspwm, tools, twm]
---

## Intro:

+ https://github.com/maledorak/dmenu-hotkeys
+ https://github.com/svenstaro/rofi-calc
+ pipx install ddgr
+ https://github.com/gmagno/rofi-ddgr
+ https://terminalroot.com.br/2020/05/como-customizar-o-rofi-like-a-foss.html

    pipx install dmenu-hotkeys
    pipx install ddgr


##  gerar a configuração do rofi

    ln -sfvn ~/.dotfiles/rofi ~/.config

## Choose rofi theme:

    rofi-theme-selector

### Installing new rofi themes:

```sh
git clone https://github.com/lr-tech/rofi-themes-collection.git
cd rofi-themes-collection
mkdir -p ~/.local/share/rofi/themes/
```

## custom rofi (geek style)

    git clone --depth=1 https://github.com/adi1090x/rofi.git
    cd rofi
    chmod +x setup.sh
    ./setup.sh

Dependencies:

    screenshots: maim, viewnior
    battery: pkexec, acpi, powertop xfce4-power-manager-settings

## rofi-wifi-menu:
+ https://github.com/ericmurphyxyz/rofi-wifi-menu

## rofi-files
+ https://github.com/cjbassi/rofi-files

    pip install --user git+https://github.com/cjbassi/rofi-files

## Set rofi as default in clipmenu:
+ https://github.com/cdown/clipmenu

     $CM_LAUNCHER
