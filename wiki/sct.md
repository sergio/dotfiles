---
File: ~/.dotfiles/wiki/sct.md
Last Change: Wed, 18 Dec 2024 - 06:32:20
tags: [monitor, destkop, color, tool, temperature, eye]
---

## Change screen color temperature and brightness using the Xrandr extension

Esse programa está nos repostiórios do [voidlinux](voidlinux.md)

+ [link](https://github.com/mgudemann/sct)

```sh
sct 4500
```


