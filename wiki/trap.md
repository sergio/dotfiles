---
file: trap.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [bash, zsh, shell, tools]
# vim:ft=markdown:et:sw=4:ts=4:cole=0:
---

## Introdução Vc pode usar o comando trap para impedir que os
ctrl+c/ctrl+z te causem problemas.  A sintaxe do comando trap é a
seguinte:

    trap [função_a_ser_executada] número_do_sinal

Se não for informado a "função_a_ser_executada" o script não mais
reconhecerá o sinal equivalente, por exemplo se você desejar que seu
script desabilite o sinal de interrupção "^C" Ctrl+C ou SIGINT 2
utilize o comando: trap 2


## Função que remove os arquivos temporários
Script de Exemplo:

    remove_temp()
    {
        echo "Removendo arquivos temporários"
        rm -f /tmp/lixo001
        exit
    }

O comando trap deverá estar no início de seu script
quando o usuário interromper o script primeiro ele
irá executar a função remove_temp

    trap remove_temp 2

