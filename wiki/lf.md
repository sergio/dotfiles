---
File: /home/sergio/.dotfiles/wiki/lf.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [file manager, tools, admin]
 vim:ft=markdown
---

## lf command line file manager:

## config file:

    $XDG_CONFIG_HOME/lf/lfrc

## Tutorial:
+ https://github.com/gokcehan/lf/wiki/Tutorial

To read the documentation just type F1 (default keybinding)

## loading icons:
+ https://www.reddit.com/r/commandline/comments/ld8l6s

Instead of addin a backslash at each line of the icons file
we can do this instead:

```sh
# lf icons:
[ -f ~/.config/lf/icons ] && {
	LF_ICONS="$(tr '\n' ':' < ~/.config/lf/icons)" \
		&& export LF_ICONS
}
```

## image previewing:
+ https://github.com/cirala/lfimg

    git clone https://github.com/cirala/lfimg

Prerequisites

Besides lf and Überzug you will need to install the following packages:

    ffmpegthumbnailer
    ImageMagick
    poppler
    epub-thumbnailer
    wkhtmltopdf
    bat (optional - color highlight for text files)
    chafa (optional - for image preview over SSH or inside Wayland session)
    unzip (optional - for .zip and .jar files)
    7z (optional - for .7z files)
    unrar (optional - for .rar files)
    catdoc (optional - for .doc files)
    docx2txt (optional - for .docx files)
    odt2txt (optional - for .odt and *.ods files)
    gnumeric (optional - for .xls and .xlsx files)
    exiftool (optional - for music files)
    iso-info (optional - for .iso files)
    transmission (optional - for .torrent files)
    mcomix (optional - for .cbz and .cbr files)


    doas xbps-install -Su ueberzug

Installation

In the project directory you can run the following command:

make install

To install this to your system, or you can do it manually by following the guide below:

    Extract the following files: cleaner, preview to ~/.config/lf/.
    Extract lfrun to a directory that is in your $PATH variable (such as /usr/bin).
    Edit your ~/.config/lf/lfrc file and add the following lines:

set previewer ~/.config/lf/preview
set cleaner ~/.config/lf/cleaner

    In order to launch lf with image preview support from now on, you will need to use the supplied lfrun script.

I recommend that you make an alias in your shell that points to lfrun
