---
file: sleep.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


O comando sleep por padrão trabalha com segundos


sleep 5; echo "olá mundo"


Mas você pode usar horas

sleep 1h; echo "olá mundo"
