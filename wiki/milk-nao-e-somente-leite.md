---
file: milk-nao-e-somente-leite.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

source: http://www.adirferreira.com.br/milk-nao-e-somente-leite/

ADDRESS /əˈdres/ 1. to speak or write to a group of people, usually in a formal situation. 2. to deal with a problem

The president addressed the press.
We need to address the issue of homelessness before it gets out of control.

BACK /bæk/ – to give support

The organization is backed by the government.
The management has refused to back our proposals.

CARD /kɑrd/ – to ask someone for identification to prove their age, especially at a bar

Bring your ID in case you get carded.
She’s 36 years old but she looks so young that she’s always carded at the door.

DISH /dɪʃ/ – to give and say things, especially hurtful and harmful things

She agreed to dish the dirt on her ex-husband for a large fee.
He dishes out criticism very easily, but doesn’t accept any.

FASHION /ˈfæʃən/ – to create something in an improvised way, using different components

He fashioned a hat for himself from/out of newspaper.
At the last minute, I fashioned a Halloween costume from a garbage bag and some string.

HOUSE /haʊz/ – to provide a long-term space for something or someone

It will be difficult to house all the refugees.
This museum houses one of the largest collections of 19th Century American art in the world.

INCH /ɪntʃ/ – to move little by little

We are inching towards an agreement.
The woman was aware of the man inching his way toward her.

LABOR /ˈleɪbɚ/ – to work hard physically or mentally

He traveled around Europe laboring to pay his way.
I’ve been laboring over this decision for some weeks now.

MILK /mɪlk/ – to exploit a person, event or situation

The directors milked the company of several million dollars.
Now that I’m the new president of the association I’m going to milk it for all it’s worth.

NURSE /nɝːs/ – 1 to slowly bring someone back to health; 2. to rest a part of your body until it heals; 3. to take a very long time to finish a drink, especially alcohol

The woman found the abandoned baby and nursed it back to health.
I’ve been at home, nursing a bad back.
You’ve been nursing that beer for over an hour!

PICTURE /ˈpɪktʃɚ/ – to imagine something, visualize

Marge married Ken? I can’t picture them together.
Picture the scene – the crowds of people and animals, the noise, the dirt.

SHOULDER /ˈʃoʊldɚ/ – to carry most of the responsibility, if not all of it

I don’t think that John should be the one to shoulder the entire project.
Teachers cannot be expected to shoulder all the blame for poor exam results.

TABLE /ˈteɪbəl/ – to delay the discussion of a subject during a formal meeting

The decision was tabled until December.
The suggestion was tabled for discussion at a later date.

VOICE /vɔɪs/ – to vocalize feelings and opinions

Please feel free to voice your concerns.
I have voiced my objections to the plan to management.

WORD /wɝːd/– to choose the way you write or say something

I know what I want to write, I just don’t know how I should word it.
He worded the reply in such a way that he did not admit making the original error.

tags: english
