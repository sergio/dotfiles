---
File: ~/.dotfiles/wiki/ave-maria.md
Last Change: Sun, 26 Jan 2025 - 17:34:00
tags: [oracoes, espiritismo]
---

# intro

[Verdadeira ave-maria](https://youtu.be/B74vXkqxXWk?si=mUQUYCy_zjRswBof)

Ave Maria cheia de graça
O Senhor é convosco
Bendita sois vós entre as mulheres
Bendito é o fruto do vosso ventre, Jesus
Santa Maria Mãe de Deus, rogai por nós
filhas e filhos de Deus,
agora e na hora de nossa vitória
sobre o pecado a doença e a morte, Amém

