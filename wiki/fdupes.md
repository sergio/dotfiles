---
File: /home/sergio/.dotfiles/wiki/fdupes.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## Find Duplicate Files (based on size first, then MD5 hash)
+ https://askubuntu.com/a/476732/3798
+ https://linux.die.net/man/1/fdupes

	fdupes -rdN dir/

    r - recursive
    d - preserver first file, delete other dupes
    N - run silently (no prompt)

    for f in $(fdupes -f .); do gio trash $f; done

    -f ................. -f --omitfirst omit the first file in each set of matches

