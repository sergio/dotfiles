---
file: locale.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## How to Set System Locale in Linux
+ https://www.tecmint.com/set-system-locales-in-linux/

    sudo update-locale LANG=LANG=pt_BR.UTF-8 LANGUAGE

or

    sudo localectl set-locale LANG=pt_BR.UTF-8
