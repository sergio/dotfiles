---
file: case.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução

    case $var in
        foo|bar|more) ... ;;
    esac

