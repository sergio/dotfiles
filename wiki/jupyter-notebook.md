---
file: jupyter-notebook.md
author: Sergio Araujo
Last Change: Fri, 04 Oct 2024 - 18:28:24
tags: [python]
---

# Using jupiter notebook

+ https://www.dataquest.io/blog/jupyter-notebook-tips-tricks-shortcuts/
+ https://blog.dominodatalab.com/lesser-known-ways-of-using-notebooks/
+ https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook

Jupyter notebook, formerly known as the IPython notebook, is a flexible tool
that helps you create readable analyses, as you can keep code, images,
comments, formulae and plots together.

In order to start jupyter notebook just type:

    jupyter notebook (on the command line)
    and open http://localhost:8888/tree
``` markdown
    --------------------------
    Jupiter notebook Shortcuts
    --------------------------
    Enter ......... edit cell
    Esc ........... exit edit mode
    Ctrl-Enter .... run cell
    m ............. to markdown
    y ............. back to code
    A ............. insert cell above
    B ............. insert cell below
    DD ............ delete current cell

    % pastebin 'file.py' to upload code to
    pastebin and get the url returned.

    %time will time whatever you evaluate
    % lsmagic ...... show all magic commands

    To start jupyter notebook on firefox

    ipython notebook --browser=firefox
```

