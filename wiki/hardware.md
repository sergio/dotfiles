---
File: hardware.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: ago 02, 2021 - 21:28
tags: [hardware]
---

#  hardware.md intro:
Site to find out the socket of your processor, as long as you know it you can choose another processor with the same socket.

https://www.cpu-world.com/
