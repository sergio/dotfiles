---
File: ~/.dotfiles/wiki/dicasnvim.md
Last Change: Fri, 19 Jul 2024 - 18:27:14
tags: [vim, nvim, tools, editor]
---

# Useful links

- [nvim-frameworks](nvim-frameworks.md)
- [Lua Primer for Neovim](https://www.gatlin.io/blog/post/lua-primer-for-neovim)
- [defaults.nvim](https://github.com/mjlbach/defaults.nvim)
- [improving-startup-time](https://elianiva.my.id/post/improving-nvim-startup-time)
- [colorschemes on github](https://github.com/topics/neovim-colorscheme?l=lua)
- [shaunsingh nvim](https://github.com/shaunsingh/vimrc-dotfiles/tree/main/.config/nvim)
- [nvim-bqf](https://github.com/kevinhwang91/nvim-bqf)
- [neogit](https://github.com/TimUntersberger/neogit)
- [nvimdots](https://github.com/ayamir/nvimdots )
- [Nightly alongside stable](https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0)
- [cheovim](https://github.com/NTBBloodbath/cheovim)
- [doom-nvim](https://github.com/NTBBloodbath/doom-nvim)
- [Refactor nvim](https://benfrain.com/refactor-your-neovim-init-lua-single-file-to-modules-with-packer/)

- [Leiyi548 nvim](https://github.com/Leiyi548/nvim)

- [vim help](https://vim.help/table-of-contents)
- [Awesome nvim](https://github.com/rockerBOO/awesome-neovim )
- [the-best-neovim-color-schemes-in-2021](https://benfrain.com/the-best-neovim-color-schemes-in-2021/)
- [Nvimfy](https://github.com/ayesumit/Nvimfy)
- [NvChad](https://github.com/NvChad/NvChad)
- [nvim-conf](https://github.com/serhez/nvim-conf)
- [AstroVim](https://github.com/kabinspace/AstroVim)
- [cover.html](https://learnbyexample.github.io/vim_reference/cover.html)
- [vimrc voyeg3r](https://github.com/voyeg3r/dotfiles/blob/master/rcfiles/vim/vimrc)

- [term keycodes](https://bit.ly/3artfJU)
- [api manual](http://man.hubwiz.com/manual/Vim)

- [dotnvim](https://github.com/s1n7ax/dotnvim)
- [voyeg3r clone of dotnvim](https://github.com/voyeg3r/dotnvim)
- [the-builtin-functions-categorized](https://neovim.discourse.group/t/the-builtin-functions-categorized/2903)
- [init.lua single file](https://github.com/mathletedev/dotfiles/blob/main/.config/nvim/init.lua )
- [craftzdog nvim](https://github.com/craftzdog/dotfiles-public/tree/master/.config/nvim)

## multiline search/substitution

``` vim
\_s
```

Vim can search for text that spans multiple lines. For example, the search /hello\_sworld finds "hello world" in a single line, and also finds "hello" ending one line, with "world" starting the next line. In a search, \s finds space or tab, while \_s finds newline or space or tab: an underscore adds a newline to any character class.

## vim/nvim spell

- [vimspell](vimspell.md)

## open files side by site (window splited)

```sh
vim -o file1 file2
```

## LSP config using mason

- [Lsp + cmp](https://vonheikemen.github.io/devlog/tools/setup-nvim-lspconfig-plus-nvim-cmp/)

## This week in neovim

- [This week in neovim 15](https://this-week-in-neovim.org/2022/Jul/15)
- [This week in neovim 25](https://this-week-in-neovim.org/2022/Jul/25)
- [This week in neovim 01](https://this-week-in-neovim.org/2022/Aug/01)

## Get background color

- [how to get the background color in vim?](https://stackoverflow.com/a/27870856/2571881)

``` vim
:echo synIDattr(hlID("Normal"), "bg")
:let @+=synIDattr(hlID("Normal"), "bg")
```

## Enabling conceal on markdown files

Neovim with treesitter already can conceal links without using regex

``` vim
syn region markdownLink matchgroup=markdownLinkDelimiter start="(" end=")" contains=markdownUrl keepend contained conceal
syn region markdownLinkText matchgroup=markdownLinkTextDelimiter start="!\=\[\%(\%(\_[^][]\|\[\_[^][]*\]\)*]\%( \=[[(]\)\)\@=" end="\]\%( \=[[(]\)\@=" nextgroup=markdownLink,markdownId skipwhite contains=@markdownInline,markdownLineStart concealends
```

## Install nerd fonts

- [A better way to install NerdFonts](https://github.com/ronniedroid/getnf)

```sh
git clone https://github.com/ronniedroid/getnf.git
cd getnf
./install.sh
```

Now you can run

``` sh
getnf
```

There is an amazing site where you can test some proramming fonts:

- [Programming fonts](https://www.programmingfonts.org/)

### Usage

run getnf from the terminal and it will represent you with a list of NerdFonts with fzf, select the ones you want, and let it do it's work.

By default the downloaded archives are removed, But if you give getnf the -k flag, it will not remove the Archives from $HOME/Downloads/NerdFonts

## Nifty Nvim Techniques That Make My Life Easier

[Series 11](https://jdhao.github.io/2021/11/22/nifty_nvim_techniques_s11/)
[Series 10](https://jdhao.github.io/2021/06/17/nifty_nvim_techniques_s10/)
[Series 11](https://jdhao.github.io/2021/11/22/nifty_nvim_techniques_s11/)
[Series 10](https://jdhao.github.io/2021/06/17/nifty_nvim_techniques_s10/)
[Series 9](https://jdhao.github.io/2021/01/07/nifty_nvim_techniques_s9/)
[Series 8](https://jdhao.github.io/2020/11/11/nifty_nvim_techniques_s8/)
[Series 7](https://jdhao.github.io/2020/09/22/nifty_nvim_techniques_s7/)
[Series 6](https://jdhao.github.io/2019/12/21/nifty_nvim_techniques_s6/)
[Series 5](https://jdhao.github.io/2019/11/11/nifty_nvim_techniques_s5/)
[Series 4](https://jdhao.github.io/2019/09/17/nifty_nvim_techniques_s4/)
[Series 3](https://jdhao.github.io/2019/05/14/nifty_nvim_techniques_s3/)
[Series 2](https://jdhao.github.io/2019/04/17/nifty_nvim_techniques_s2/)
[Series 1](https://jdhao.github.io/2019/03/28/nifty_nvim_techniques_s1/)

- [toggle moviment](https//ddrscott.github.io/blog/2016/vim-toggle-movement/)

Pegar a coluna do último caractere da linha:

``` vim
:ec col('$')-1 ............................... last char of the line
:ec match(getline('.'), '\S')+1 ................ first non blank
```

```vim
function! ToggleHomeZero()
 let pos = getpos('.')
 execute "normal! ^"
 if pos == getpos('.')
  execute "normal! 0"
 endif
endfunction
nnoremap 0 :call ToggleHomeZero()<CR>
```

## get mode using vim.api

- [Get current mode](https://www.reddit.com/r/neovim/comments/s2v0cz)

``` vim
:lua print(vim.api.nvim_get_mode()["mode"])
local mode = vim.api.nvim_get_mode()["mode"]
```

## Opening the last edited file

``` txt
alias lvim='vim -c "normal '\''0"' <-- This alias had an issue (it opens another "empty buffer")
alias lvim="vim -c':e#<1'"

The above command "-c" says:
:e ............................. edit
# ............................. the alternative file
<1 ............................. number one
```

## Working with sessions

```vim
:mks ~/mysession.vim
vim -S mysession.vim
:source ~/mysession.vim
```

## Insert date

``` vim
:put =strftime("%c")
```

## New features of vim 0.8

1 - winbar
2 - global statusline

- [stackoverflow thead](https://stackoverflow.com/a/8426948/2571881)

```vim
:set title titlestring=%{strftime('%c',getftime(expand('%')))}
:lua print(vim.fn.strftime('%a, %b %d %Y - %H:%M', vim.fn.getftime(vim.fn.expand('%'))))
```

## Installing neovim nightly alongside stable

- [two nvim versions](https://dev.to/creativenull/installing-neovim-nightly-alongside-stable-10d0)

``` sh
git clone https://github.com/neovim/neovim.git ~/build/neovim
cd ~/build/neovim
make CMAKE_BUILD_TYPE=Release CMAKE_INSTALL_PREFIX="/bin" CMAKE_EXTRA_FLAGS="-DCMAKE_INSTALL_PREFIX=/usr/share"
sudo make install
```

## better CMAKE_BUILD_TYPE

(This is the best option because it avoids lags after quiting neovim)
Release: Full compiler optimizations and no debug information. Expect the
best performance from this build type. Often used by package maintainers.

RelWithDebInfo ("Release With Debug Info"): Enables many optimizations and
adds enough debug info so that when Neovim ever crashes, you can still get a backtrace.

```vim
sudo make install
```

# Default install location is /usr/local

``` sh
:helptags $VIMRUNTIME/doc
alias nvim8='cd ~/build/neovim; VIMRUNTIME=runtime ./build/bin/nvim'
```

## Debugging neovim

- [Debugging by exampe](https://codeinthehole.com/tips/debugging-vim-by-example/)

## How to toggle options in lua

- <https://github.com/neovim/neovim/issues/14825>

``` lua
function toggle_spelling()
vim.o.spell = not(vim.o.spell:get())
end
```

``` lua
:lua vim.opt_local.spell = not(vim.opt_local.spell:get())
:lua vim.diagnostic.disable() and :lua vim.diagnostic.enable()
```

## get filetype in lua

- [Stack exchange 7339](https://vi.stackexchange.com/a/27898/7339)

``` lua
vim.bo.filetype
```

## Getting iskeyword value

``` lvim
:let @0=execute('ec &isk')
```

## lua set local options (ftplugin)

``` lua
lua vim.opt_local.number = false
lua vim.opt_local.relativenumber = false
```

## output last change of the file

- [stackoverflow thead](https://stackoverflow.com/a/8426948/2571881)

``` vim
:strftime('%c',getftime(expand('%')))
:lua print(vim.fn.strftime('%c', vim.fn.getftime(vim.fn.expand('%'))))
:lua print(vim.fn.strftime("%a, %d %b %Y %H:%M", vim.fn.getftime(vim.fn.expand('%'))))
:lua print(vim.fn.strftime("%d %b %Y %H:%M", vim.fn.getftime(vim.fn.expand('%'))))
```

## atomic search

```vim
      lookbehind lookahead
positive  (atom)\@<= (atom)\@=
negative  (atom)\@<! (atom)\@!

In very magic mode:
      lookbehind  lookahead
positive  (atom)@<=  (atom)@=
negative  (atom)@<!  (atom)@!
```

## [What's New In Neovim 0.7 - 12](https://neovim.io/news/2022/04)

## How viminfo (shada) works?

``` sh
set viminfo=%,<800,'10,/50,:100,h,f0,n~/.vim/cache/.viminfo
" | | | | | | | + viminfo file path
" | | | | | | + file marks 0-9,A-Z 0=NOT stored
" | | | | | + disable 'hlsearch' loading viminfo
" | | | | + command-line history saved
" | | | + search history saved
" | | + files marks saved
" | + lines saved each register (old name for <, vi6.2)
" + save/restore buffer list
```

## Jump markdown headers

- [Reddit thead](https://www.reddit.com/r/vim/comments/p7xcpo/comment/h9nw69j/)
- [Stackoverflow thead](https://stackoverflow.com/a/2179779/2571881)
- [fixterms](http://www.leonerd.org.uk/hacks/fixterms/)

```vim
function! MarkdownHeaders()
  let l:filename = expand("%")
  let l:lines = getbufline('%', 0, '$')
  let l:lines = map(l:lines, {index, value -> {"lnum": index + 1, "text": value, "filename": l:filename}})
  call filter(l:lines, {_, value -> value.text =~# '^#\+ .*$'})
  call setqflist(l:lines)
  copen
endfunction
nmap <M-h> :cp<CR>
nmap <M-l> :cn<CR>
```

## Compile nvim from the source code

- [Building neovim web page](https://colleowino.github.io/building-neovim)
- [Building neovim wiki](https://github.com/neovim/neovim/wiki/Building-Neovim)

```sh
git clone https://github.com/neovim/neovim
git checkout stable
make
make test
sudo make install
```

## scroll both windows at once

- [Thead on stackoverflow](https://stackoverflow.com/a/18466534/2571881)

``` vim
:windo set scrollbind
```

## Count selected lines
- <https://stackoverflow.com/a/7262572/2571881>

``` vim
" in visual mode press:
g Ctrl-g
```

## nvim as manpage

```bash
function man () {
/bin/sh -c "unset PAGER"
nvim -c 'set ft=man bt=nofile bh=hide noswapfile' -c "Man $1" -c 'silent! only' -c 'nmap q :bd!<CR>'
}
```

```sh
/bin/sh -c "unset PAGER;col -b -x | vim -R -c 'set ft=man nomod nolist' -c 'map q :q<CR>' -c 'map <SPACE> <C-D>' -c 'map b <C-U>' -c 'nmap K :Man <C-R>=expand(\"<cword>\")<CR><CR>' -"
```

## My current configuration comes from this repo

- [Nvim from scratch](https://github.com/LunarVim/Neovim-from-scratch)
- [@chrismachine](https://github.com/LunarVim/nvim-basic-ide)

```sh
git clone https://github.com/LunarVim/Neovim-from-scratch.git
```

## Secure modelines

```lua
-+ https://github.com/numirias/security/blob/master/doc/2019-06-04_ace-vim-neovim.md#patches
-+ https://stackoverflow.com/a/41994024/2571881
if vim.fn.has('patch-8.1.1366') then
  vim.opt.modelines=5
  vim.opt.modelineexpr = false
  vim.opt.modeline = true
else
  vim.opt.nomodeline = true
end
```

Modeline issues:

# vim: set cursorline

```vim
^^^ issue: use of the word set without a trailing colon (:)
```

# vim:cursorline

```vim
^ issue: no space after the colon
```

# vim: cursorline

```vim
^ issue: no space before the word vim
```

### fix indentation (using modelines)

```vim
vim:set softtabstop=4 shiftwidth=4 tabstop=4 expandtab:
-- vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
```

## Get lua tables values

```lua
-- in case of tables (lists)
print(vim.inspect(vim.opt.listchars:get()))
-- { space = "\_", tab = ">~", }
```

```lua
lua print(vim.opt.autoindent:get())
```

## Question: How to assign "print"ed value to a variable?

- [Reddit t2gu18](https://www.reddit.com/r/neovim/comments/t2gu18)

local result = vim.fn.execute[[highlight StatusLine]]

## Check startup time

- [vim-startuptime](https://github.com/rhysd/vim-startuptime)

     nvim --startuptime startup.log -c exit && tail -5 startup.log

go install github.com/rhysd/vim-startuptime@latest
vim-startuptime

-- IMPROVE NEOVIM STARTUP
-+ <https://github.com/editorconfig/editorconfig-vim/issues/50>
vim.g.loaded_python_provier=1
vim.g.python_host_skip_check = 1
vim.g.python_host_prog='/bin/python2'
vim.g.python3_host_skip_check = 1
vim.g.python3_host_prog='/bin/python3'
vim.opt.pyxversion=3
-- if vim.fn.executable("editorconfig") then
-- vim.g.EditorConfig_exec_path = '/bin/editorconfig'
-- end
vim.g.EditorConfig_core_mode = 'external'

## How to know if a file is executable

- <https://matrix.to/#/!JAfPjWAdLCtgeCAwnS:matrix.org/$165927113651167rrstj:matrix.org?via=matrix.org&via=libera.chat&via=gitter.im>

  vim.fn.fs_stat()
  lua print(vim.loop.fs_stat(vim.fn.expand("%")))
  lua print(vim.inspect(vim.loop.fs_stat(vim.fn.expand("%"))))

  -- local mode = vim.loop.fs_stat(vim.fn.expand("%")).mode -- returns 33261
  -- local permission = string.format("%o", mode) -- convert to ocatal mode (returns: 100755)
  lua print(string.match(permission, "7", 4))

  -- Some path manipulation utilities
  local function is_dir(filename)
  local stat = vim.loop.fs_stat(filename)
  return stat and stat.type == 'directory' or false
  end

  local perm = vim.fn.getfperm(expand("%:p"))
  lua print(string.match(perm, "x", 3))

  -- para arquivos executáveis ele imprimirá: 33261 (por exemplo)
  local stat = vim.loop.fs_stat(vim.fn.expand("%"))
  print(stat.mode)

local mode = vim.loop.fs_stat(vim.fn.expand("%")).mode
local mode = string.format("%o", mode)

## Test if it is a file or dir

    local type = vim.fn.getftype(vim.fn.expand("%:p"))

## Defer some plugins on startup

- [the-illusion-of-fast-startup-times-in-vim](https://vonheikemen.github.io/devlog/tools/the-illusion-of-fast-startup-times-in-vim/)

Loggins startup:

vim --startuptime vim.log

## clear caches (still related with startup time)

Instead of restarting the system this will clear all caches

```sh
echo 1 | sudo tee /proc/sys/vm/drop_caches
```

I have found a reddit post that says zsh is the culprit for nvim slow startup time:
[help_extremely_slow_nvim_startup](https://www.reddit.com/r/neovim/comments/scjhnx/help_extremely_slow_nvim_startup/)

## Trying another nvim "distro"

```sh
git clone https://github.com/brainfucksec/neovim-lua.git
cd neovim-lua/
cp -Rv nvim ~/.config/
```

## Controling formatoptions - avoid continue commenting with o or Enter

- [SuperUser 45032](https://superuser.com/a/271024/45032)
- [StackOverflow 2571881](https://stackoverflow.com/a/16032415/2571881)

You can use the 'formatoptions' option to influence how Vim formats text.
'formatoptions' is a string that can contain any of the letters below. The
default setting is "tcq". You can separate the option letters with commas for
readability. **See fo-talbe**

letter meaning when present in 'formatoptions'

a auto-formating of paragraphs
t Auto-wrap text using textwidth
c Auto-wrap comments using textwidth, inserting the current comment leader automatically.
r Automatically insert the current comment leader after hitting <Enter> in Insert mode.
n When formatting text, recognize numbered lists.
o Automatically insert the current comment leader after hitting 'o' or 'O' in Normal mode.
q   Allow formatting of comments with "gq".

In nvim lua you can define formatoptions per filetype:

```lua
-- ~/.config/nvim/after/ftplugin/lua.lua
vim.opt_local.formatoptions:remove({ 'a', 't', 'o', '2' })
```

You could check where the option is set issuing with following commands:

```vim
:5verbose set fo?
:5verbose setl fo?
```

## Setting up lsp servers (language servers)

Use the [lsp](lsp.md)

Note: You have to add `.luarc.json` in your root dir in case of sumneko_lua

The path in my case should be:

```vim
~/build/lua-language-server/bin/lua-language-server
```

## Sort from 4th column until the end

```vim
"In this case I already have the selection `'<,'>`
:'<,'>sort /\%4v/
```

## Align by a given char

Align by equal sign

- [Stackoverflow](https://stackoverflow.com/a/51462785/2571881)

```vim
:% ! column -t -s= -o=
```

## Color picker using telescope (lua)

- [Link](https://pastebin.com/3qi2THX8)

## Avoid typing Esc twice when usig telescope

- [Esc on Telescope](https://www.reddit.com/r/neovim/comments/pzxw8h/telescope_quit_on_first_single_esc/)

## How to scroll in another window without switching to it?

- [Scroll other window](https://vi.stackexchange.com/a/26604/7339)

winnr('j') returns Window number of a window below the current one. win_getid() translates Window number to :h winid. Then win_execute() runs an arbitrary command as if that window were active (but display is not updated, and auto-commands are not triggered).

Note: win_execute() is not available in Neovim.

    call win_execute(win_getid(winnr('j')), "normal! \<C-d>")

## Getting the file name

short file name just the "tail"
expand("%:t")

    1 Ctrl-g

To insert the full path filename just press (in insert mode):

    Ctrl-r = expand("%:p")

or create an alias like this
using insert abbreviation in lua

    vim.cmd([[inoreab Fname <c-r>=expand("%:p")<cr>]])

## Getting home dir

    :lua print(vim.loop.os_homedir())

## Lua map util

```lua
-- -+ https://www.notonlycode.org/neovim-lua-config/
-- shortcut -> lhs   command -> rhs
-- lhs -> left hand side rhs -> right rand side
local function map(mode, lhs, rhs, opts)
  local options = { noremap = true, silent = true }
  if opts then
    if opts['desc'] then
      opts['desc'] = 'keymaps.lua: ' .. opts['desc']
    end
    options = vim.tbl_extend('force', options, opts)
  end
  vim.keymap.set(mode, lhs, rhs, options)
end

map("<F10>", ':echom "just a test"<cr>')
map("<F11>", "<cmd>lua require('utils').flash_cursorline()<cr>", { silent = true })

map("n", "<Down>", ":resize -5<CR>", { desc = "Resize -5" } )
map("n", "<Up>", ":resize +5<CR>", { desc = "Resize -5" } )
map("n", "<Left>", ":vertical resize -5<CR>", { desc = "Vertical Resize -5" } )
map("n", "<Right>", ":vertical resize +5<CR>", { desc = "Vertical Resize +5" } )
```

## Restore cursor position when yanking in visual mode

-- Make visual yanks place the cursor back where started
map("v", "y", "ygv<Esc>")

## Better gx mapping

map('', 'gx', '<Cmd>call jobstart(["xdg-open", expand("<cfile>")], {"detach": v:true})<CR>', {})

## Swiching buffers

Along with alternate file you can do 5<Ctrl-6> to jump to the fifth buffer.

We can also open a given buffer in an alternative window, something like:

Ctrl-w 3 Ctrl-6

I have also a mapping '<leader>b' for Telescope buffers and

<A-,> ................. BufferPrevious
<A-.> ................. BufferNext

## Nice autocommands

```lua
vim.api.nvim_create_autocmd("ModeChanged", {
  pattern = { "*:i*", "i*:*" },
  group = group,
  callback = function()
  vim.o.relativenumber = vim.v.event.new_mode:match("^i") == nil
end,
})
```

## Compile sumneko lua language server for neovim

- <https://github.com/xiyaowong/coc-sumneko-lua/issues/6>
- <https://www.chiarulli.me/Neovim/28-neovim-lua-development/>

sumneko_server (){
cd ~/.config/nvim
git clone <https://github.com/sumneko/lua-language-server>
cd lua-language-server
git submodule update --init --recursive
cd 3rd/luamake
ninja -f compile/ninja/linux.ninja
cd ../..
./3rd/luamake/luamake rebuild
} && sumneko_server

.zshenv:
[ -d ~/build/lua-language-server ] && export
PATH="$HOME/build/lua-language-server/bin:$PATH"

Note: Also add a file called `.luarc.json` to your project root dir
so the lsp will attatch to the opened buffer

## Install treesitter

doas cargo install tree-sitter-cli
cargo install tree-sitter

TODO: how to install globally

## Better gf

At the moment neovim has a pretty good gf behavior because it uses lsp or treesitter to solve the file path.

" native gf fail if the file does not exist
nnoremap gf :edit <cfile><cr>
map('n', 'gf', ':split <cfile><cr>')

gF ............. follow the file (and jumps to the line number)

gF open in the same window
<c-w>F open in a new window (Ctrl-w F)

Open files when the extension does not appear on the current file, like lua modules:

require('core.utils')

:set suffixesadd+=.lua

## Close all buffers but current one

    "%bd|e#|bd#"

## Jump to the next curly braces }

]}
]]

## Edit a new file in a vertical split window

:vsp /pat/to/file

Or using telescope press Ctrl + v to vertical split or Ctrl + x to horizontal split

## Add filename at lualine center (plugin)

```lua
-- source: https://benfrain.com/neovim-tips-and-tricks-volume-one/
lualine_c = {
   {"diagnostics", sources = {"nvim_lsp"}},
   function()
    return "%="
   end,
   "filename"
  },
```

## Compilar sumneko lua para sua arquitetura

```sh
git clone git@github.com:sumneko/lua-language-server.git #follow build instructions
git submodule update --init --recursive
$ cd 3rd/luamake && ./compile/install.sh && cd ../.. && ./3rd/luamake/luamake rebuild && cp bin/Linux/lua-language-server ~/.config/coc/extensions/coc-sumneko-lua-data/sumneko-lua-ls/extension/server/bin/Linux/
```

Sumneko root path:

```lua
-- Coloque no arquivo lspconfig.lua
local sumneko_root_path = vim.fn.stdpath('data') .. "/lsp_servers/sumneko_lua/extension/server"
local sumneko_binary = sumneko_root_path.."/bin/Linux/lua-language-server"
```

## Copy pattern to a register (copiar padrão para um registro)

```vim
:g/https:/yank A
```

## Tab completion in the command line

set wildcharm=<C-z>
cnoremap <expr> <Tab> getcmdtype() =~ '[\/?]' ? "<C-f>A<C-n>" : "<C-z>"
cnoremap / /<tab>

## Better navigation with harpoon plugin

- <https://github.com/ThePrimeagen/harpoon>

use 'nvim-lua/plenary.nvim' " don't forget to add this one if you don't have it yet! use 'nvim-lua/popup.nvim' use
'ThePrimeagen/harpoon'

To mark a file:

:lua require("harpoon.mark").add_file()

In my case I have a mpping to open the harpoon menu:

map('n','<leader>h', '<cmd>:lua require("harpoon.ui").toggle_quick_menu()<cr>')

## A working example for nvim-cmp, luasnip and friendly-snippets

- <https://github.com/mxdevmanuel/dotfiles>
- <https://github.com/L3MON4D3/LuaSnip/issues/201>
- <https://www.reddit.com/r/neovim/comments/ri2u3m/luasnip_now_has_a_snipmatestyle_loader/>
- <https://github.com/L3MON4D3/LuaSnip/blob/master/DOC.md#snipmate-snippets-loader>
- <https://sbulav.github.io/vim/neovim-setting-up-luasnip/>

See from_vscode for an explanation of opts. If opts.paths is ommited, snippets are loaded from any directory named
snippets located in the runtimepath.

require("luasnip/loaders/from_vscode").load({ paths = { "~/.config/nvim/custom_snippets" } })

As the snipmate snippet format is fundamentally the same as vscode, it can also be loaded.

require("luasnip.loaders.from_snipmate").load(opts) -- opts can be ommited

Lazy loading is also available with the snipmate-loader.

    require("luasnip.loaders.from_snipmate").lazy_load(opts) -- opts can be ommited
    require("luasnip/loaders/from_snipmate").lazy_load( {paths = { "~/.config/nvim/snippets" }})

Snipmate snippets can be loaded like this:

require("luasnip.loaders.from_snipmate").load({
paths = {
vim.fn.stdpath('config') .. '/snippets',
}
})

In my case:
require("luasnip/loaders/from_vscode").lazy_load({ paths = {"~/.config/nvim/luasnip_snippets"}})
require("luasnip/loaders/from_snipmate").lazy_load({ paths = {"~/.config/nvim/snippets"}})

In your luasnip config file:

local ls = require("luasnip")
ls.filetype*extend("all", { "*" })

### snippet converter tool

- <https://github.com/smjonas/snippet-converter.nvim>

## Insert a snippet via function

```vim
"TODO: test if it is already dinamic
:execute "normal i" . "\_skel" . "\<Tab>"
```

## Disable red underscores in markdown

- [Vim markdown issues](https://github.com/tpope/vim-markdown/issues/21)
- [Vim markdow issue 986248462](https://github.com/tpope/vim-markdown/issues/21#issuecomment-986248462)

```vim
syn match markdownError "\w\@<=\w\@="
```

Add hi link markdownError NONE to after/syntax/markdown.vim

Also change matchparen options:

- [Reddit thead](https://www.reddit.com/r/vim/comments/s7ory/comment/c4bs3v4/)

```vim
  hi MatchParen ctermfg=Yellow ctermbg=NONE cterm=bold
  hi MatchParen guifg=Yellow guibg=NONE gui=bold
```

## Put 'messages' into clipboard

- [Stackexchange discussion](https://vi.stackexchange.com/questions/31067)

```vim
:let @+=execute('messages')
```

## Set commentstring

```lua
vim.bo.commentstring = '//%s'
```

For languages that have comment symbols at the begining and at the end we can do:

```lua
vim.bo.commentstring = '<!--%s-->'
```

## Insert a range of ips using put

- [Insert ip range](https://www.commandlinefu.com/commands/view/3055/insert-ip-range-using-vim)

```vim
:for i in range(1,255) | .put='192.168.0.'.i | endfor
```

## Rename files using vim

Let's say you have these files

file-1.txt
file-2.txt
file-3.txt

You create the command (mv old new) in this case adding leading zeros to the numbers

```vim
:%s/\v(file-)(\d+)(\.txt)/\='mv '. submatch(0) . ' ' . submatch(1) . printf('%03d', submatch(2)) . submatch(3)
```

Then you call the shell:

```vim
:sil! w !bash
```

[Bulk rename](https://vim.fandom.com/wiki/Bulk_rename_files_with_Vim)

```vim
\ls | vim -
```

For example, to rename the files to lowercase:

```vim
:%s/.\*/mv -i '&' \L'&'/g
:sil! w !sh
```

An alternative method of executing the commands is:

```vim
:%!bash
```

## Substracting seconds from time

Subtrair segundos de uma data no formato hh:mm:ss

```sh
time test 00:02:01
time test 00:01:01
time test 00:04:59

:1,3s/00._\ze$/\=system('date -u -d "'.submatch(0).' +0000 -02 sec" +"%H:%M:%S"')/g
:1,3s/00._\ze$/\=system('date -u -d "'.submatch(0).' +0000 -02 sec" +"%H:%M:%S"| tr "\n" " "')/g
```

# I have a script "less2sec" that subtracts seconds from hours like: 12:03:01

# The script must be in the path and obviously be executable

```vim
:%s/[^ ]\*\ze$/\=system('less2sec ' . submatch(0))
:%s/.\*/\=system('less2sec ' . submatch(0) . ' 10')
```

## substitute quotes

- [Stackoverflow solution](https://stackoverflow.com/a/17338668/2571881)

```vim
:s/["']/\=submatch(0) == '"' ? "'" : '"'/g
```

## Adding numbers with leading zeros

Let's say you have a list like this:

Gonzaguinha*-\_Lindo_Lago_do_Amor.mp3
Gonzaguinha*-*Comecaria_tudo_outra_vez.mp3
Gonzaguinha_de_Volta_ao_Comeco.mp3
Gentileza*-\_Gonzaguinha_Ao_Vivo.mp3
Diga_la_coracao.mp3
Anunciacao.mp3
E_Vamos_A_Luta.mp3
Tiro_Ao_Alvaro.mp3
Flor_de_Lis.mp3
Aguas_De_Marco.mp3

And you want to add at the beginning of each line 01, 02, 03 etc:

```vim
let c=0 | g/^/ let c+=1 | s//\=printf('%02d', c) . '-'
```

## Make clipboard register blockwise so that you can paste as a column

```vim
:call setreg("+", getreg("+"), "b")
```

Also removing '^j' "Enter" from the clipboard register:
the ^j means enter or newline

```vim
:let @+=substitute(@+,'\n', '', 'g')
```

p pastes something after the cursor
P pastes something before the cursor
gp same as p but puts the cursor after the pasted selection
gP same as P and puts the cursor after the pasted selection

## Merge two files

- [stackoverflow 2571881>](<https://stackoverflow.com/q/72237807/2571881)

Open the two files:

```vim
vim -o file1.txt file2.txt
```

```vim
function! Join_files()
    wincmd j
    %y
    wincmd k
    call setreg('0', getreg('0'), 'b')
    normal gg
    .s/$/ /
    normal $"0p
    %s/\s\+/ /g
endfunction
```

```vim
:@0
:call Join_files()
```

## Replace mode

like insert in word text processor, just type R in normal mode

## Repository to study

```sh
git clone https://github.com/voyeg3r/vimrc-dotfiles.git
```

## Lsp language servers

c .......... ccls -> doas xbps-install -Sy ccls

## Refatorando códigos no neovim

- [Refactoring](https://stackoverflow.com/a/69482236/2571881)

```vim
:vimgrep '\v( |,)\zstest' \**/*.c :cfdo %s/\v( |,)\zstest/\U& :cfdo update
```

Scenario: I've got 4 files in my Quickfix list and I want to add a new line, "status": "not started", onto line 5 of each of them.

```vim
:cfdo :call append(4, '"status": "not started"')
```

## Creating desktop icon for neovim

```sh
  cat <<EOF > ~/.local/share/applications/nvim.desktop
  [Desktop Entry]
  Name=Neovim
  GenericName=Text Editor
  GenericName[pt]=Text Editor
  Comment=Edit text files
  TryExec=nvim
  Exec=nvim %F
  Terminal=true
  Type=Application
  Encoding=UTF-8
  Icon=nvim
  Categories=Utility;TextEditor;
  StartupNotify=false
  Keywords=Text;editor;
  MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
  EOF
```

## Getting cursor positon in lua

```lua
  local linha = vim.api.nvim_win_get_cursor(0)[1]
+- roll = linha  coll = coluna
local r,c = unpack(vim.api.nvim_win_get_cursor(0))
print(r, c)

lua vim.api.nvim_win_set_cursor({0},{15,0})
--onde zero é a janela atual, 15 é o número da linha e 0 é a coluna
```

## The jumplist issue

For jumping in the current buffer (only) prefer using `g;` and `g,`

```vim
g;
g,
```

I also have a mapping 'gl' --> '`.' mnemonic 'go last (change)'

## Running macros faster

+ [Faster macros stackexchange](https://vi.stackexchange.com/a/31888/7339)

Avoid accessing system clipboard in the macro.

Accessing external system clipboard +, \* introduces additional cost, when compared with accessing internal registers. It may even freeze the macro replaying forever (run macro on 6000 lines in my test).

```markdown
:noautocmd :norm @q to disable events temporarily during macro running. Or
:set eventignore=all before macro running, and set it back after.

:sil! argdo :noautocmd norm! @a
:sil! argdo :noa norm! @a
```

The [nvim-autopairs](https://github.com/windwp/nvim-autopairs) plugin has an option for disabling it during macro recordings: `disable_in_macro = true,`

You can store your macros as text:

```vim
:let @h="$r)yT/0f<xi[\<C-r>0](\<Esc>j"
```

## Test a new set up

- [Reddit p9z2ii](https://www.reddit.com/r/neovim/comments/p9z2ii)
- [cheovim](https://github.com/NTBBloodbath/cheovim)

```markdown
alias simple-nvim=XDG_CONFIG_HOME=~/.simple-config nvim
```

Now you can use nvim to get your regular configuration and simple-nvim to use the other configuration.

```lua
home = os.getenv('HOME')
```

## Plugins para git

[Artigo](https://dlvhdr.me/posts/how-i-use-github)

## Nvim-cmp settings

- [cmp setup](https://gist.github.com/mengwangk/324c3aed377b94bf6d0da07f53205a7a)

## Start insert at the right indenting

Just press 'S'

```lua
--@test if a string is nil or empty
M.isempty = function(s)
  return s == nil or s == ''
end

map(
  "n",
  "i",
  function()
    return Utils.is_empty_line() and 'S' or 'i'
  end,
  {
    expr = true,
    desc = "properly indent on empty line when insert",
  }
)
```

## Getting hlsearch status

```lua
local hls_status = vim.v.hlsearch
```

Specific neovim tips

```python
python3 -m pip install --upgrade pip --user
pip3 install --user pynvim
pip2 install --user pynvim
pip3 install --user jedi
```

## Framework for nvim

- [lunarvim](https://www.lunarvim.org/)

## Reselect last pasted text

- <https://stackoverflow.com/a/4317090/2571881>

nnoremap <expr> g<c-v> '`[' . getregtype()[0] . '`]'

You can also use `1v` to reselect the same amount of previous selection, very
useful for block selection mode. NOTE: You should have done some modification to be able to use 1v, blockwise or linewise.

## Preserve function in lua

- <https://bit.ly/3g6vYIW>

```lua
-+ https://bit.ly/3g6vYIW
function _G.preserve(cmd)
  cmd = string.format('keepjumps keeppatterns execute %q', cmd)
  local original_cursor = vim.fn.winsaveview()
  vim.api.nvim_command(cmd)
  vim.fn.winrestview(original_cursor)
end
preserve('%s/\\s\\+$//e')
```

### Another attempt to change the timestamp is

- <https://www.reddit.com/r/neovim/comments/u9f36k/comment/i5r2rfb/>

```vim
" https://vi.stackexchange.com/a/5962/7339
function! SubstiTute()
  let l = 1
  for line in getline(1, 7)
    call setline(l, substitute(line, "Last Change: \\zs.*", strftime("%a, %d %b %Y - %H:%M:%S"), "g"))
    let l = l + 1
  endfor
endfun
"call SubstiTute()
nmap <leader>9 call SubstiTute()<CR>

" another example
fun! SubstiTute()
  let l = 1
  let limit = line('$')
  while l < limit
    call setline(l, substitute(getline(l), 'this', 'that' , 'gc'))
    let l = l + 1
  endwhile
endfun
call SubstiTute()
```

## Get current buffer number

```vim
:lua print(vim.api.nvim_get_current_buf())
```

## Run current line

- [Reddit hscl6n1](https://www.reddit.com/r/vim/comments/s26aau/comment/hscl6n1/)

```vim
:Ctrl-r Ctrl-l
```

## wrap word in quotes

- [Stackoverflow 2571881](https://stackoverflow.com/a/20456631/2571881)

To wrap in single quotes (for example) ciw'<C-r>"'<esc> works, but repeat won't work. Try:

```vim
ciw'<C-r><C-o>"'<esc>
```

This puts the contents of the default register "literally". Now you can press .
on any word to wrap it in quotes. To learn more see :h[elp] i_ctrl-r and more
about text objects at :h text-objects

Source: [Vimcasts pasting from insert mode](http://vimcasts.org/episodes/pasting-from-insert-mode/)
## Text-objects guide

- [Article](https://thevaluable.dev/vim-create-text-objects/)

## Squeeze blank lines

```lua
M = {}

M.squeeze_blank_lines = function()
  -- references: https://vi.stackexchange.com/posts/26304/revisions
  if (vim.bo.filetype ~= 'binary') or (vim.bo.filetype ~= 'diff') then
    local old_query = vim.fn.getreg('/')  -- save search register
    M.preserve('sil! 1,.s/^\\n\\{2,}/\\r/gn') -- set current search count number
    local result = vim.fn.searchcount({maxcount = 1000, timeout = 500}).current
    local line, col = unpack(vim.api.nvim_win_get_cursor(0))
    M.preserve('sil! keepp keepj %s/^\\n\\{2,}/\\r/ge')
    M.preserve('sil! keepp keepj %s/\\v($\\n\\s*)+%$/\\r/e')
    if result > 0 then
      vim.api.nvim_win_set_cursor({0}, {(line - result), col})
    end
    vim.fn.setreg('/', old_query)      -- restore search register
  end
end

return M
```

## Converter datas

```vim
" This function requires you select the numbers
" source: https://vi.stackexchange.com/a/4699/7339
" o formato da data tem que estar ano mês dia
fun! ConvDate()
  try
    let l:a_save = @a
    norm! gv"ay
    let @a = '"' . @a . '"'
    " let var=system('~/.dotfiles/algorithm/shell/bash/convdate ' . @a)
    let timestamp=system('date +"%s" -d ' . @a)
    let new_date =system('LC_ALL=en_US;' . 'date +"%a, %b %d %Y" -d '. "@" . timestamp)
    let @a = new_date
    ec "Your new date is: " . new_date
    norm! gv"ap
    norm! kJ
  finally
    let @a = l:a_save
  endtry
endfun

" Execute this file
" https://sbulav.github.io/vim/neovim-telescope-github/
function! s:save_and_exec() abort
 if &filetype == 'vim'
  :silent! write
  :source %
 elseif &filetype == 'lua'
  :silent! write
  :lua require("plenary.reload").reload_module'%'
  :luafile %
 endif

 return
endfunction
" save and resource current file
nnoremap <leader><leader>x :call <SID>save_and_exec()<CR>
```

## Reload lua modules

```lua
+ https://www.reddit.com/r/neovim/comments/vihqql/comment/idf7b2j/
-- Make sure user modules can be reloaded when using :source
local load = function(mod)
 package.loaded[mod] = nil
 require(mod)
end

load('user.settings')
load('user.keymaps')
```

## Current ultisnips on my nvim

(as currently settled in Aug 10 2021)

pressing tab will show the snippet content
and an Enter will insert it. The <C-j> trigger also works

## Telescope nvim needs ripgrep

```vim
doas xbps-install -Sy ripgrep
```

## How to get line number using lua?

- <http://neovim.io/doc/user/api.html#nvim_win_get_cursor()>

```lua
lua print(vim.api.nvim_win_get_cursor(0)[1])

api = vim.api
function line()
  local cline = api.nvim_win_get_cursor(0)[1]
  return cline
end
```

## Avoid loosing your text adding undo points in the line

- <https://www.youtube.com/watch?v=hSHATqh8svM>

```lua
map('i', '.', '.<C-g>u')
map('i', ',', ',<C-g>u')
map('i', '?', '?<C-g>u')
map('i', '!', '!<C-g>u')
map('i', ':', ':<C-g>u')
map('i', ';', ';<C-g>u')
+ - Undo break points
local break_points = {'<Space>', '-', '_', ':', '.', '/'}
for _, char in ipairs(break_points) do
 bind('i', char, char .. '<C-g>u')
end
```

## Get filetype with lua

- [27897](https://vi.stackexchange.com/questions/27897/)

filetype is a buffer-local option, so you can access it with:

```vim
:lua print(vim.bo.filetype)
```

Get option:

- <https://vonheikemen.github.io/devlog/tools/configuring-neovim-using-lua/>

```vim
:lua print(vim.opt.lazyredraw:get())
```

## Disabling some builtin plugins

```lua
-- disable builtins plugins
local disabled_built_ins = {
  "netrw",
  "netrwPlugin",
  "netrwSettings",
  "netrwFileHandlers",
  "gzip",
  "zip",
  "zipPlugin",
  "tar",
  "tarPlugin",
  "getscript",
  "getscriptPlugin",
  "vimball",
  "vimballPlugin",
  "2html_plugin",
  "logipat",
  "rrhelper",
  "spellfile_plugin",
  "matchit"
}

for _, plugin in pairs(disabled_built_ins) do
  g["loaded_" .. plugin] = 1
end
```

## To enable spell enable 'spellfile_plugin'

- <https://github.com/mateusbraga/vim-spell-pt-br>

