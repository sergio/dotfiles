---
File: /home/sergio/.dotfiles/wiki/pywal.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [wallpapers, python, desktop]
 vim:ft=markdown
---

NOTE: After installed it will apper in the system as "wal"
The new version is "pywall"
https://github.com/dylanaraps/pywal/wiki/What's-different-in-pywal%3F

## Intro:

+ https://pypi.org/project/pywal/
+ https://www.reddit.com/r/unixporn/comments/feseh2/getting_started_with_bspwm_for_beginners/
+ https://bitbucket.org/Volteos/linux-dot-files/src/master/

## dependencies

+ imagemagick

## no archlinux

    sudo pacman -S python-pywal

Pywal is a tool that generates a color palette from the dominant colors in an
image. It then applies the colors system-wide and on-the-fly in all of your
favourite programs.

There are currently 5 supported color generation backends, each providing a
different palette of colors from each image. You're bound to find an appealing
color-scheme.

Pywal also supports predefined themes and has over 250 themes built-in. You can
also create your own theme files to share with others.

The goal of Pywal was to be as out of the way as possible. It doesn't modify
any of your existing configuration files. Instead it works around them and
provides tools to integrate your system as you see fit.

Terminal emulators and TTYs have their color-schemes updated in real-time with
no delay. With minimal configuration this functionality can be extended to
almost anything running on your system.

## Getting started:
+ https://github.com/dylanaraps/pywal/wiki/Getting-Started

Run wal and point it to either a directory (wal -i "path/to/dir") or an image
(wal -i "/path/to/img.jpg") and that's all. wal will change your wallpaper for
you and also set your terminal colors.

    wal -i ${XDG_PICTURES_DIR}/backgrounds/$(ls ${XDG_PICTURES_DIR}/backgrounds
    | shuf -n 1)

    # https://www.reddit.com/r/unixporn/comments/cbir4c
    wallpaper="$(ls -1A ~/img/backgrounds/ | shuf -n 1)"; feh --bg-center $wallpaper; wal -i $wallpaper -n

## getting help:

    wal -h
