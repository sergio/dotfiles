---
File: ~/.dotfiles/wiki/leis-cosmicas.md
Last Change: Sat, 14 Sep 2024 - 11:18:47
tags: [spiritism]
---

# Resumo das 7 Leis Cósmicas

[Link](https://youtu.be/gv-dbvXBjAs)

O vídeo apresentado explora de forma detalhada as 7 Leis Universais, conceitos que, segundo a filosofia hermética, regem toda a criação. Abaixo, um resumo conciso de cada uma:

1 - Lei da Mentalidade: Nossos pensamentos moldam nossa realidade. Uma mente positiva atrai experiências positivas, e uma mente negativa, experiências negativas.

2 - Lei da Correspondência: O que está dentro é igual ao que está fora. Nosso mundo exterior é um reflexo de nosso mundo interior.

3 - Lei da Vibração: Tudo no universo vibra em diferentes frequências. Nossas emoções e pensamentos vibram e atraem para nós o que está na mesma frequência.

4 - Lei da Polaridade: Tudo tem um oposto. A vida é um jogo de contrastes e podemos manifestar qualquer polaridade que desejamos.

5 - Lei da Causa e Efeito: Toda ação gera uma reação. Nossas escolhas e ações têm consequências.

6 - Lei do Ritmo: Tudo na vida flui em ciclos. Há momentos de crescimento e outros de declínio.

7 - Lei do Gênero: Tudo tem um princípio masculino e feminino. Essa lei se aplica tanto à criação física quanto à criação de ideias.

Em resumo: As 7 Leis Universais nos convidam a assumir o controle de nossas vidas, compreendendo que somos cocriadores de nossa realidade. Ao alinhar nossos pensamentos, emoções e ações com essas leis, podemos manifestar uma vida mais plena e feliz.

Pontos-chave abordados no vídeo:

A importância de cultivar uma mentalidade positiva.
A conexão entre o mundo interior e exterior.
O poder da vibração e como ela influencia nossas experiências.
A necessidade de agir para manifestar nossos desejos.
A importância da gratidão e da visualização.

Observação: O vídeo aprofunda cada uma dessas leis, oferecendo exemplos e insights práticos para aplicá-las no dia a dia. Para um entendimento mais completo, recomendo assistir ao vídeo completo.

Gostaria de explorar alguma dessas leis com mais profundidade? Posso fornecer mais detalhes sobre cada uma delas ou responder a perguntas específicas.





TUDO Vai Mudar Assim Que Você Aprender Isto:  As 7 Leis...O Poder Sem Limites
2024-09-11

Você já parou para pensar que tudo o que você vive, cada experiência, cada emoção, é moldado por leis que regem o universo? Imagine por um instante que existe um manual cósmico, um conjunto de princípios que, quando compreendidos, podem transformar sua realidade de forma endente.

As sete leis universais não são apenas conceitos abstratos, elas são forças invisíveis que atuam em sua vida diariamente, guiando seus pensamentos, suas ações e, consequentemente, o que você atrai para si. Mas o que aconteceria se você decidisse se alinhar com essas leis? Como sua vida mudaria? Se você pudesse acessar esse poder oculto?

Mantenha a mente aberta e sinta como essas verdades universais podem Ressoar dentro de você. Prepare-se para despertar um potencial que talvez você nem soubesse que tinha. E se eu te dissesse que a chave para a abundância, a felicidade e a realização está bem diante de você, esperando para ser descoberta? Siga comigo, porque a jornada que começamos agora pode ser a transformação que você tanto busca. Vamos explorar juntos essas leis que moldam não apenas o universo, mas também a sua própria existência. Você está pronto?

A lei da mentalidade

Sabe, eu sempre acreditei que tudo começa na cabeça da gente. É impressionante como nossos pensamentos têm um poder tão grande sobre a nossa realidade. Quando paro para pensar, percebo que a maioria das coisas que conquistei na vida foi resultado de uma mentalidade positiva. Desde que adotei essa forma de ver as coisas, minha vida começou a mudar. É como se houvesse uma conexão direta entre o que eu pensava e o que se manifestava ao meu redor. Se eu acordava pensando que o dia seria ruim, adivinha? A energia negativa se espalhava e tudo parecia dar errado. Por outro lado, quando me concentrava em pensamentos bons, em gratidão, os acontecimentos pareciam fluir de forma mais harmoniosa. É quase mágico perceber que a nossa mente é como um ímã que atrai as experiências que vivemos.

E isso não é só uma crença, é uma realidade que muitos já comprovaram. A lei da mentalidade me ensinou a importância de cuidar dos meus pensamentos. O que eu coloco na minha mente tem um impacto direto nas minhas ações e, consequentemente, nos resultados que busco. Às vezes é desafiador, claro, mas a prática constante de manter uma mentalidade positiva se torna um hábito que transforma a vida. E quem não quer uma vida cheia de boas experiências, não é mesmo?

Acredito que essa mudança mental não é apenas sobre ser otimista, mas também sobre ser consciente. É saber que temos o poder de escolher como reagir às situações. Se algo não sai como planejado, em vez de me deixar levar pelo desânimo, busco entender a lição por trás da experiência. Essa é a verdadeira essência da lei da mentalidade. Quando entendemos que tudo começa com nossos pensamentos, começamos a assumir o controle da nossa vida. Na verdade, em vez de sermos vítimas das circunstâncias, nos tornamos os criadores da nossa própria história.

A lei da correspondência

A lei da correspondência é outro conceito fascinante que tem me acompanhado. Esse princípio fala sobre a relação entre o microcosmo e o macrocosmo, como o que acontece dentro de nós reflete o que acontece fora. É como um espelho: o que vemos no mundo exterior é um reflexo do que está em nossa mente e em nosso coração.

Essa ideia me faz refletir sobre as conexões que estabeleço diariamente. Quando olho para a minha vida e para as minhas interações, percebo que muitas vezes as situações que enfrento são um reflexo das minhas próprias crenças e atitudes. Se estou em paz comigo mesmo, as relações à minha volta tendem a ser mais harmoniosas. Mas se estou em conflito interno, isso pode se manifestar em desavenças e tensões nos meus relacionamentos.

É interessante como tudo se conecta. Essa lei também me fez entender que para mudar a realidade externa, primeiro é preciso mudar a interna. Às vezes tentamos solucionar problemas superficiais, mas a verdadeira mudança começa dentro de nós. Isso exige autoconhecimento e coragem para olhar para as partes de nós que precisam de transformação. É um trabalho contínuo, mas muito recompensador.

Além disso, a lei da correspondência me ensina sobre empatia. Quando compreendo que o que os outros sentem ou fazem também é um reflexo de suas próprias experiências, consigo ser mais compreensivo. Isso nos ajuda a criar um ambiente mais positivo e solidário, onde todos podem crescer juntos.

Preste atenção: a consciência dessa lei me faz sentir parte de algo maior. Ao perceber que as minhas ações e pensamentos têm um impacto no mundo ao meu redor, sinto que estou contribuindo para uma realidade mais harmoniosa. Afinal, somos todos interconectados, e ao nos transformarmos, ajudamos a transformar o mundo.

A lei da vibração

Quando comecei a entender a lei da vibração, minha percepção sobre a vida mudou completamente. Essa lei nos ensina que tudo no universo está em constante movimento e vibra. A cada pensamento, cada emoção e cada ação que tomamos, emitimos uma frequência que pode influenciar nossas experiências.

Isso me fez perceber que a energia que eu emito tem um papel fundamental na qualidade da minha vida. A vibração não é apenas uma ideia abstrata, mas algo que podemos sentir. Quando estou em um estado elevado de energia, como alegria ou gratidão, tudo ao meu redor parece brilhar. As pessoas se tornam mais receptivas, as oportunidades aparecem e até mesmo os desafios se tornam mais fáceis de lidar. É como se eu estivesse sintonizado na frequência certa, atraindo tudo o que é positivo.

Por outro lado, quando me permito cair em vibrações negativas, como raiva ou tristeza, tudo fica mais pesado. As interações se tornam mais difíceis e a vida parece cheia de obstáculos.

Essa percepção me fez assumir a responsabilidade sobre a minha própria vibração. Agora busco constantemente elevar minha energia através de práticas como meditação, exercícios físicos e momentos de reflexão.

A lei da vibração também me ensinou a importância de estar rodeado de pessoas que vibram alto. A energia é contagiante, e estar ao lado de pessoas positivas me impulsiona a manter essa frequência elevada. É uma troca benéfica que que enriquece a vida de todos os envolvidos.

O que antes era apenas uma questão de sorte ou azar agora se torna uma escolha consciente. Preste atenção: me dei conta de que podemos transformar nossa realidade ao elevar nossa vibração. Seja por meio de pensamentos positivos, gratidão ou ações que nos façam sentir bem. Cada pequeno passo conta. A vida é uma dança constante de vibrações, e a nosso papel escolher a música que queremos tocar.

A lei da atração

A lei da atração é um dos conceitos que mais ressoam comigo. A ideia de que semelhantes atraem semelhantes é poderosa e, ao mesmo tempo, desafiadora para mim.

Essa lei é um lembrete constante de que sou responsável por aquilo que atraio para a minha vida. Quando mantenho uma mentalidade positiva e foco em meus objetivos, as oportunidades parecem surgir na naturalmente. No começo, eu era cético, pensava que era apenas uma questão de sorte. Mas com o tempo, percebi que a minha mentalidade realmente moldava minha realidade. Quando me concentro no que quero, ao invés do que não quero, as coisas começam a fluir.

Essa mudança de foco é um exercício diário, mas é incrível ver os resultados. É quase como se o universo estivesse ouvindo e respondendo aos meus desejos.

Uma das lições mais valiosas que aprendi com a lei da atração é a importância de agradecer. A gratidão cria uma vibração elevada que atrai mais coisas boas. Quando pratico a gratidão, meu dia se transforma. Até mesmo os desafios se tornam oportunidades de aprendizado, e isso muda completamente a forma como encaro a vida.

Além disso, essa lei me incentivou a visualizar meus objetivos. Criar uma imagem clara do que desejo me ajuda a manter o foco e a energia direcionada. Ao me ver alcançando minhas metas, sinto uma motivação extra para agir e continuar avançando. Esse exercício de visualização se tornou uma prática essencial para mim.

Preste atenção: a lei da atração me ensinou que somos cocriadores da nossa realidade. Quando entendemos que temos o poder de moldar nossa vida através dos nossos pensamentos e emoções, nos tornamos mais proativos. É uma jornada de autodescoberta e empoderamento, onde cada um de nós tem a capacidade de criar a vida que deseja.

A lei da ação

Quando falamos sobre a lei da ação, a primeira coisa que vem à mente é que não basta apenas sonhar ou visualizar o que queremos. É preciso agir. Eu aprendi que para que nossos desejos se tornem realidade, é necessário dar passos concretos em direção a eles. Por exemplo, se eu quero mudar de carreira, não adianta apenas desejar isso. Eu preciso buscar cursos, me conectar com



