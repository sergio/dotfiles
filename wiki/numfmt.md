---
file: numfmt.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: numfmt - Convert numbers from/to human-readable strings
---
#  numfmt.md intro:
numfmt - Convert numbers from/to human-readable strings

I have a .txt file and I need to add commas as decimal separators to improve
the readability...

``` txt
BitstreamCyberCJK;Freeware;30275;28686;v2.0 beta (1998-03-17)
Y.OzFontN;Freeware;21957;57621;v13.00 sfnt rev 5 Pen-Ji (2010-08-24)
```

should turn into this:

``` txt
BitstreamCyberCJK;Freeware;30,275;28,686;v2.0 beta (1998-03-17)
Y.OzFontN;Freeware;21,957;57,621;v13.00 sfnt rev 5 Pen-Ji (2010-08-24)
```

    numfmt -d ';' --field=3,4 --grouping < file

    -d ................. delimiter
    --field= ........... fields to apply
    --grouping ......... use delimiter according with your locale
