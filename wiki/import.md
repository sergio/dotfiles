---
file: import.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução
Para capturar a imagem de um pedaço da tela faça:

    import imagem.png
