---
File: bat.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [cat, tools]
---

## An improved version of cat
+ https://github.com/sharkdp/bat
+ https://remysharp.com/2018/08/23/cli-improved

see cofig File

    bat --config-file

