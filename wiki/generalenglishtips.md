---
file: generalenglishtips.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

accountability
encouragement
knowledge
resources
accessment
planing

Fluency traps

	T --> trying harder
	R --> resources, you do not need more resources, you need more speaking
	A --> alone, not do it alone
	P --> poor me
	S --> safe, playing safe

