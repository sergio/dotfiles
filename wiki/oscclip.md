---
Filename: oscclip.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [ssh, clipboard, python, tools, termux]
 vim:ft=markdown
---

## oscclip

oscclip is a little, zero dependency python utility which utilizes the system clipboard via OSC52 escape sequences. Using these sequences, the system clipboard is accessible via SSH as well. Terminal multiplexers, such as tmux and screen are supported.
Examples

Setting the clipboard

    $ echo "Foo" | osc-copy

Setting the clipboard and bypass terminal multiplexers

    $ echo "Foo" | osc-copy --bypass

Reading the clipboard

    $ osc-paste
    Foo
