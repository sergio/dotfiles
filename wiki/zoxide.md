---
File: ~/.dotfiles/wiki/zoxide.md
Last Change: Mon, 19 Feb 2024 - 20:42:32
tags: [tools, fzf, frexency, cd, utils]
---

# install on termux

    pkg in zoxide

now you have two commands

``` sh
z ....... non interactive
zi ...... interactive
```


