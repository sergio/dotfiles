xautomation(7) - Linux man page
Name
xautomation - control X from the command line, and find things on the screen
Description
 vim:ft=markdown
xautomation is a set of command lines programs to control X and do "visual scraping" to find things on the screen. The control interface allows mouse movement, clicking, button up/down, key up/down, etc, and uses the XTest extension so you don't have the annoying problems that xse has when apps ignore sent events. The visgrep program find images inside of images and reports the coordinates, allowing programs to find buttons, etc, on the screen to click on.

xautomation consists in the following programs:

pat2ppm
    Converts an image file from PAT to PPM format.
patextract
    Extract a part of a PNG image file.
png2pat
    Converts an image file from PNG to PAT format.
rgb2pat
    Converts an image file from 24-bit RGB to PAT format
visgrep
    Visual grep, greps for images in another image.
xte
    Generates fake input using the XTest extension.

For more information, please see the individual manpages of these programs.
See Also
pat2ppm(1), patextract(1), png2pat(1), rgb2pat(1), visgrep(1), xte(1).
Author
xautomation was written by Steve Slaven <bpk@hoopajoo.net>.

This manual page was written by Aurelien Jarno <aurel32@debian.org>, for the Debian project (but may be used by others).

