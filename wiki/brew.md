---
File: ~/.dotfiles/wiki/brew.md
Last Change: Thu, Jan 2025/01/09 - 18:38:16
tags: [packages, homebrew, brew, install]
---

# Install homebrew on linux

+ https://brew.sh/

    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

