---
file: englishtexts.md
author: Sergio Araujo
Last Change: Sat, 21 Sep 2024 - 08:52:07
tags: [english]
---

# links 

[Ai for transcription](https://TurboScribe.ai)

[fundamentals](fundamentals.md)
[towraparaundmyfinger](towraparaundmyfinger.md)
[learnlikeachildren](learnlikeachildren.md)
[quantumleapenglish](quantumleapenglish.md)
[colaboracaoemmassanainternet](colaboracaoemmassanainternet.md)
[dobblingbrainpower](dobblingbrainpower.md)
[generalenglishtips](generalenglishtips.md)
[limping](limping.md)
[ebola](ebola.md)
[story](story.md)
[plano-de-aula](plano-de-aula.md)

