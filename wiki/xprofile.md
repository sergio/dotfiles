---
File: xprofile.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 12, 2020 - 16:18
tags: [tools]
---

#  xprofile.md intro:

An xprofile file, ~/.xprofile and /etc/xprofile, allows you to execute
commands at the beginning of the X user session - before the window manager is
started.

For example, to set your keyboard settings you can add these lines on your xprofile

``` sh
# http://blog.ssokolow.com/archives/2011/12/24/getting-your-way-with-setxkbmap/
setxkbmap -model abnt2 \
    -layout br -variant abnt2 \
    -option \
    -option "lv3:ralt_switch" \
    -option "terminate:ctrl_alt_bksp" \
    -option "caps:escape" 
```

## Compatibility

The xprofile files are natively sourced by the following display managers:

    GDM - /etc/gdm/Xsession
    LightDM - /etc/lightdm/Xsession
    LXDM - /etc/lxdm/Xsession
    SDDM - /usr/share/sddm/scripts/Xsession

## Configuration

Firstly, create the file ~/.xprofile if it does not exist already. Then, simply add the commands for the programs you wish to start with the session. See below:

    ~/.xprofile
    tint2 &
    nm-applet &

