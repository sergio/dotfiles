---
File: openntpd.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [tools, network]
---

#  openntpd.md intro:
+ https://www.openntpd.org/

OpenNTPD is a Unix daemon implementing the Network Time Protocol to synchronize
the local clock of a computer system with remote NTP servers. It is also able
to act as an NTP server to NTP-compatible clients.

    doas xbps-install -Sy openntpd
    ln -s /etc/sv/openntpd /var/service/openntpd
