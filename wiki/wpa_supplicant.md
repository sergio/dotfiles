---
file: wpa_supplicant.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [wifi, net]
---


# wpa_supplicant.md

wpa_supplicant  -
Wi-Fi Protected Access client and IEEE 802.1X suppli‐ cant

    wpa_supplicant -B -i wlp7sO -c <(wpa_passphrase Solutions_Flavio flavio2015)
    sudo wpa_suplicant -B -i enp2s0 -c <(wpa_passphrase antonia dm07fs10mf17)
    dhclient wlp7s0

In order to use wpa_supplicant I have just instaled this:

    pacman -S wireless_tools networkmanager wpa_supplicant wpa_actiond netcf dialog
    pacman -S dhclient
    pacman -S wifi-menu
    pacman -S dialog
    pacman -S vim
    pacman -S nvim
    pacman -S zsh
