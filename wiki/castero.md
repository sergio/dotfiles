---
file: castero.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: This aims...
---

#  castero.md intro:
Command line podcast client

    sudo pip3 install castero

    a .................. add rss
    --import IMPORT path to OPML file of feeds to add
    --export EXPORT path to save feeds as OPML file

Commands

        h            - show this help screen
        q            - exit the client
        a            - add a feed
        d            - delete the selected feed
        r            - reload/refresh feeds
        s            - save episode for offline playback
        arrows       - navigate menus
        page up/down - scroll menus
        enter        - play selected feed/episode
        space        - add selected feed/episode to queue
        c            - clear the queue
        n            - go to the next episode in the queue
        i            - invert the order of the menu
        p or k       - pause/play the current episode
        f or l       - seek forward
        b or j       - seek backward
        ]            - increase playback speed
        [            - decrease playback speed
        1-3          - change between client layouts
