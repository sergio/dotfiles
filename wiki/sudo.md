---
file: sudo.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags [tools, admin, root, doas]
---

## Use your own vimrc when using sudo
+ https://coderwall.com/p/xvryda/use-your-own-vimrc-when-using-sudo

Preserve environment settings use "-E"

    sudo -E vim file

another great option is:

    sudoedit file

In order to allow a regular user to use sudo command do this:
edite the file /etc/sudoers

  adding this

  user ALL=(ALL) ALL

## Runing multiple commands with sudo
+ https://www.cyberciti.biz/faq/how-to-run-multiple-commands-in-sudo-under-linux-or-unix/

      sudo -- sh -c 'command1 && command2'

Understanding sudo command options

    -- : A — signals the end of options and disables further option processing for sudo command.
    sh -c : Run sh shell with given commands

## append text to a file when using sudo

    echo '192.168.1.254 router' | sudo tee -a /etc/hosts
    sudo sh -c 'echo "192.168.1.254 router" >> /etc/hosts'

