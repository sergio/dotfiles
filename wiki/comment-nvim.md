---
file: comment-nvim.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [vim, nvim, plugins]
---

## neovim comment plugin tha accepts repeat '.'
+ https://www.reddit.com/r/neovim/comments/q9ibzo
+ https://benfrain.com/refactor-your-neovim-init-lua-single-file-to-modules-with-packer/

Whit this config you can lazy load your "comment" plugin:

    -- store your config on ~/.config/nvim/lua/setup
    -- whith name comment.lua
    function get_setup(name)
    return string.format('require("setup/%s")', name)
    end

    use({
      "numToStr/Comment.nvim",
      opt = true,
      keys = { "gc", "gcc", "gbc" },
      config = get_setup("comment"),
    })

## Features

1 - Supports commentstring. Read more
2 - Prefers single-line/linewise comments
3 - Supports line (//) and block (/* */) comments
4 - Left-right (gcw gc$) and Up-Down motions (gc2j gc4k)
5 - Use with text-objects (gci{ gcat)
6 - Dot (.) repeat support for gcc, gbc and friends
7 - Supports pre and post hooks
8 - Custom language/commentstring support
9 - Ignore certain lines, powered by Lua regex

## Mappings

    Basic/Toggle mappings (config: mappings.basic)

NORMAL mode

    `gc[count]{motion}` - (Operator mode) Toggles the region using linewise comment

    `gb[count]{motion}` - (Operator mode) Toggles the region using linewise comment

    `gcc` - Toggles the current line using linewise comment

    `gbc` - Toggles the current line using blockwise comment

    `gco` - Insert comment to the next line and enters INSERT mode
    `gcO` - Insert comment to the previous line and enters INSERT mode
    `gcA` - Insert comment to end of the current line and enters INSERT mode

VISUAL mode

    `gc` - Toggles the region using linewise comment

    `gb` - Toggles the region using blockwise comment

Append comment - Pressing gcA in NORMAL mode will add the comment at the end-of-line and puts you in INSERT mode

    local rick| = "roll"

    -- Pressing gcA

    local rick = "roll" -- |

Insert comment above - Pressing gcO adds comment to the line above and puts you in INSERT mode.

    local never = "gonna"
    local give| = "you up"

    -- Pressing gcO

    local never = "gonna"
    -- |
    local give = "you up

Insert comment below - Pressing gco adds comment to the line below and puts you in INSERT mode.

    local never = "gonna"
    local let| = "you down"

    -- Pressing gco

    local never = "gonna"
    local let = "you down"
    -- |

