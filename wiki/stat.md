---
file: stat.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [system, tools]
---

## stat command:

displays file or file system status

## Get the last modification time:

    stat -c "%y" test.sh | awk -F\. '{print $1}'

    see also 'date -r'
    awk '{$5=$6="";print $1,$3,$2,$7,$4}' <(date -r stat.md)

## Preserve timestamp even when you edit files with vim:
+ https://unix.stackexchange.com/a/430288/3157

```sh
vi-preserve-time () {
    for file in "$@"; do
        local mtime=$(stat -c %y "$file")
        vi "$file"
        touch -d "$mtime" "$file"
    done
}
```

## change a string on each file without losing the timestamp:
+ https://stackoverflow.com/a/40394094/2571881

Preserve timestamp in sed command

    find dir -name '*.xml' -exec bash -c 't=$(stat -c %y "$0"); sed -i -e "s/text1/text2/g" "$0"; touch -d "$t" "$0"' {} \;

    The `ls` command gives us something like:

    Apr 26 13:07

    The `date -r file +'%F %T'` give use:
    2022-04-26 13:07:39

    whereas: (reference: https://unix.stackexchange.com/a/451556/3157)
    date -r file +'%a, %d %b %Y - %H:%M:%'   --> gives us
    Tue, 26 Apr 2022 - 13:07:39

    One can use printf-like formatting for the %y format specification directly,
    but not to modify a piece of the string in the middle:

    $ stat -c '%.19y' file
    2021-03-17 08:53:39

    This truncates the string after 19 characters, which removes the subsecond
    data, but the time zone info is also left out.

I have created a script for changing the timestamp in a wiki folder I have:

```bash
(
cd ~/.dotfiles/wiki
for file in *.md; do
    echo "Modificando o arquivo $file .."
    t=$(stat -c "%y" "$file") # original timestamp
    new_date=$(date -r "$file" +'%a, %d %b %Y - %H:%M:%S')
    sed -i "1,7s/\(Last Change: \).*/\1 $new_date/g" "$file"
    touch -d "$t" "$file"
done
)
```

## Display octal permissions of a given file

    stat -c '%a' /etc/passwd

## move specific files from subdirectories into one directory
+ https://stackoverflow.com/questions/57289018/

    find ./images -name *150x150.jpg | \
    parallel 'mv {} ./another-directory/`stat -c%i {}`_{/}'

    {/.} ............... gives us the filename with basename and no extension

In the above command `stat -c%i` gives us the inode number of the file
OBS: In order to test the command one can use --dry-run option of parallel

