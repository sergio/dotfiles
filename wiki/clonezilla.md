---
file: clonezilla.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Clonezila é um poderoso programa capaz de clonar sua instalação
Já usei uma vez instalando 40 computadores numa tarde, vale
a pena aprender a usa-lo
