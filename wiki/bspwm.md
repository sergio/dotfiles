---
file: bspwm.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [windowmanager, desktop, bspwm]
---

## bspwm is i3 like window manager
+ https://terminalroot.com.br/2021/06/como-instalar-e-configurar-o-bspwm-com-temas-para-polybar.html
+ https://bytee.net/misc/my-bswpm-sxhkd-shortcuts
+ https://bgdawes.github.io/bspwm-xfce-dotfiles/
+ https://github.com/primalkaze/bspwm-dots (nice bspwm config)
+ https://github.com/bgdawes/bspwm-xfce-dotfiles
+ https://gideonwolfe.com/tags/linux/
+ https://terminalroot.com.br/2021/08/como-deixa-sua-polybar-com-bspwm-assim.html
+ https://ricebr.github.io/Not-A-Blog//instalando-e-configurando-bspwm/
+ https://my-take-on.tech/2020/07/03/some-tricks-for-sxhkd-and-bspwm/#easily-order-windows
+ https://github.com/EndeavourOS-Community-Editions/bspwm
+ https://github.com/ricebr/Not-A-Bloat (rice guide pt-br)
+ https://github.com/ricebr/Not-A-Bloat/blob/master/ricing/README.md
+ https://github.com/AlphaTechnolog/dotfiles
+ https://github.com/Mangeshrex/dotfiles
+ https://blog.rzg.one/post/2021/05/09/floating-and-sticky-windows-in-bspwm/

## dependencies:

    sxhkd picom gimp findutils fd pywal mpd mpv neovim polybar
    nemo dunst ffmpeg scrot escrotum xbacklight xdo xprop xrdb
    xset xclip xsetroot xtitle yt-dlp sxiv st ranger xclock gpm
    clipmenu

## Increase gaps: gaps in bspwm : dynamic resizing gaps like a chad
+ https://www.youtube.com/watch?v=I-aKqH66lgM

## flash when changing focus:
+ https://github.com/fennerm/flashfocus

    pipx install flashfocus

## Scratchpad terminal:
+ https://www.reddit.com/r/bspwm/comments/85hr4c/

bspc rule:

    bspc rule -a St:scratchy sticky=on state=floating center=true rectangle=1000x625+0+0

scratchtermtoggle script:

```sh
#!/bin/bash
# source: https://www.reddit.com/r/bspwm/comments/85hr4c

id=$(xdo id -n scratchy);
if [ -z "$id" ]; then
    st -n scratchy
else
    action='hide';
    if [[ $(xprop -id $id | awk '/window state:/ {print $3}') == 'Withdrawn' ]]; then
        action='show'
    fi
    xdo $action -n scratchy
fi
```

sxhkdrc shortcut to open our scratchpad terminal:

```sh
# another scratch term
super + u
    ~/.dotfiles/bin/scratchtermtoggle
```

## dealing with hidden windows:

    bspc node -g hidden .............................................. hide window
    bspc query -N -n .hidden ......................................... list hidden windows
    bspc node $(bspc query -N -n .hidden | head -1) -g hidden=off  ... unhide hidden windows

## Open app
+ https://www.reddit.com/r/bspwm/comments/eorrhu/launching_an_application_in_the_desktop_youre_in/

Launching an application in the desktop you're in, not the one you switch to

I'm using rofi, but works the same way from the terminal as well. Launch some application with a longer start-up time, switch to another desktop, and the app appears in the desktop you just switched to. I'd like it to open up in the desktop I launched it from. Is there a way to handle this in bspwm? In i3 this could be done with i3 exec. I found this issue on bspwm repo which mentions bspc execute, but it's not a thing, and I'm not sure it ever was.

```sh
#!/bin/sh
# Open a command in current desktop

usage() {
	echo "No command specified."
}

if [ $# = 0 ]; then
	usage
	exit 1
fi

# Creates a single-shot rule that opens next application in current desktop
bspc rule -a \* -o desktop=$(bspc query -D -d) && $@
```

## Using system wide template as bspwm settings:

```sh
trash ~/.config/{bspwm,sxhkd}
install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc
```

## config:

``` sh
~/.config/bspwm/bspwmrc
```

doas xbps-install -S bspwm sxhkd feh rofi compton polybar plank xcompmgr picom fzf fzy
# mkdir ~/.config/{bspwm,sxhkd}
# cp /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm
# cp /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd

If you do not have the config files pre created

    install -Dm755 /usr/share/doc/bspwm/examples/bspwmrc ~/.config/bspwm/bspwmrc
    install -Dm644 /usr/share/doc/bspwm/examples/sxhkdrc ~/.config/sxhkd/sxhkdrc

In my case I normally do:

    ln -sfvn ~/.dotfiles/bspwm ~/.config/
    ln -sfvn ~/.dotfiles/sxhkd ~/.config

``` sh
super + enter          →   terminal
super + w              →   close window
super + [1-10]         →   Switching between virtual desktops
super + [1-10]         →   Switching between virtual desktops
super + shift + [1-10] --> move to desktop [1-10]
super + tab            →   focus last mode/desktop
super + alt + q        →   logof
super + alt + r        →   restart
super + right button   →   resize windo
super + s              →   floating window
super + t              →   toggle float window
super + esc            →   reload sxhkdrc config
super + l              →   move to the window at right
super + h              →   move to the window at left
```

## change wallpaper

``` sh
feh --bg-scale ~/img/backgrounds/radio.png
feh -z --bg-fill ~/img/backgrounds/ &
```

Now add the line below at the end of your

NOTE: for picom work with glx backend you need to run this command

    doas xbps-install -Sy libva-glx{,-devel}

``` sh
~/.config/bspwm/bspwmrc
echo '${HOME}/.fehbg' >> ~/.config/bspwm/bspwmrc
picom -b
# echo 'compton &' >> ~/.config/bspwm/bspwmrc
# echo 'plank &' >> ~/.config/bspwm/bspwmrc

rofi -show drun
rofi-theme-selector  (alt+a to save theme)
rofi -show drun -show-icons
```

NOTE: if you don't have nvidia disable vsync:
+ https://www.youtube.com/watch?v=HxbhkkfaVuo&t=479s
+ https://github.com/doubledickdude/dotfiles

    sed -i.backup '/vsync/s/true/false/' ~/.config/picom/picom.conf

On ~/.config/sxhkd/sxhkdrc:

``` markdown
# program launcher
# super + esc (reload config)
super + d
	rofi -show drun -show-icons
```

## theme settings polybar

Configure your polybar

``` sh
git clone --depth=1 https://github.com/adi1090x/polybar-themes.git

cd polybar-themes
./setup.sh
```

On your ~/.config/bspwm/bspwmrc

``` sh
echo '${HOME}/.config/polybar/launch.sh --forest &' >> ~/.config/bspwm/bspwmrc
sed -i 's/killall -q/pkill/g' ~/.config/polybar/launch.sh
```

## gtk-3.0 theme

``` sh
vim ~/.config/gtk-3.0/settings.ini
```

``` sh
[Settings]
gtk-icon-theme-name = Papirus-Grey
gtk-theme-name = Adwaita-dark
gtk-cursor-theme-name = Adwaita
gtk-application-prefer-dark-theme=1
```

See also:
+ https://unix.stackexchange.com/a/670600

    Add the following line to ~/.xprofile:
    export GTK_THEME=Adwaita:dark

    export GTK_THEME=Arc-Dark

Another way of lauching apps with a certain theme:

    GTK_THEME=Adwaita:dark gnome-calculator

## plank (dock)

``` sh
echo 'bspc rule -a Plank layer=above border=off manage=on locked=on' >> ~/.config/bspwm/bspwmrc
```
 ## bsp-layout
+ https://github.com/phenax/bsp-layout
+ https://youtu.be/4TN-tMYVpBw

    man bsp-layout

    curl https://raw.githubusercontent.com/phenax/bsp-layout/master/install.sh | bash -;

    bsp-layout set tall
    bsp-layout set tall III --master-size 0.5

    bsp-layout remove II

## Rules:
+ https://forum.endeavouros.com/t/issue-with-rules/14975/2

Getting the app name (click on the app window):

    xprop | awk '/WM_CLASS/{print $4}'

Which terminal I am using?

    ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)

Which desktop I am now?

    bspc query -D -d names

# vim:cole=0 tabstop=4 shiftwidth=4 expandtab ft=markdown:
