---
File: polybar.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [desktop, panel, polybar]
---

##  intro:
+ https://github.com/polybar
+ https://terminalroot.com.br/2021/08/como-deixa-sua-polybar-com-bspwm-assim.html

A Better WM Panel for your Linux system

Copy polybar config:

    cp /usr/share/doc/polybar/config .

## Toggle polybar
+ https://github.com/polybar/polybar/wiki/Inter-process-messaging

    polybar-msg cmd toggle

Just associate the above command in sxkbdrc

## mouse clicks on polybar
+ https://github.com/tam-carre/polywins

    Install wmctrl, it should be packaged by most distros
    Save polywins.sh, for example to ~/.config/polybar/scripts
    Make the script executable with chmod +x ~/.config/polybar/scripts/polywins.sh

## Using a preconfigurated polybar set

``` sh
git clone --depth=1 https://github.com/adi1090x/polybar-themes.git
cd polybar-themes
chmod +x setup.sh
./setup.sh
```

Another guide using the same repo:

```sh
# https://dev.to/l04db4l4nc3r/bspwm-a-bare-bones-window-manager-44di#getting-started
# clone this repo
git clone https://github.com/adi1090x/polybar-themes

# go to polybar-1 dir
cd polybar-themes/polybar-1

# copy fonts to local fonts dir (i'll put the fonts in all dirs)
cp -r fonts/* ~/.local/share/fonts

# reload font cache
fc-cache -v

# copy everything from polybar-1 to polybar config dir (backup your config first if you have)
cp -r * ~/.config/polybar

# add the following two commands to your bspwmrc (recommended)
# kill previously running instances of polybar
pkill polybar

# run launch.sh
~/.config/polybar/launch.sh
```

## Terminalroot example:
+ https://youtu.be/-iNkTSunmZ4

## show wireless icon
This is necessary because polybar gets wireless information, if it is up
how much fast is uploads and downloads etc.

    iw dev | awk '/Interface/ {print $2}'
    iw dev | awk '$1=="Interface" {print $2}'

    search for [module/netork] at:
    ~/.config/polybar/forest/modules.ini

## nice themes
+ https://github.com/google/material-design-icons

    doas xbps-install -Sy font-material-design-icons-ttf

## Alse read:
+ https://elkowar.github.io/eww/
