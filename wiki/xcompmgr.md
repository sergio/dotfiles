---
File: /home/sergio/.dotfiles/wiki/xcompmgr.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [compositor, desktop, transparency, simple terminal, tools]
 vim:ft=markdown
---

## xcompmgr:
+ https://wiki.archlinux.org/title/Xcompmgr#Installation

Xcompmgr is a simple composite manager capable of rendering drop shadows and, with the use of the transset utility, primitive window transparency. Designed solely as a proof-of-concept, Xcompmgr is a lightweight alternative to Compiz and similar composite managers.

Because it does not replace any existing window manager, it is an ideal solution for users of lightweight window managers, seeking a more elegant desktop.

## How to start applications such as xcompmgr at start up?
+ https://askubuntu.com/questions/78926/

Yes "xcompmgr" alone does nothing. You need to tell it what kind of effects you want, which is what all gibberish at the end of the command is for.

Also you must add the "&" in the end of the command to start xcompmgr in the background. Without that your startup script starts xcompmgr and then waits until it finishes (which it will never do) before continuing with your startup script.

Mine is: xcompmgr -cfF -t-9 -l-11 -r9 -o.95 -D6 &

edit: Here's an explanation for the options I'm using:

"-cfF" "c" is for soft shadows and transparency support, "f" for fade in & fade out when creating and closing windows, and "F" for fade when changing a window's transparency.

"-t-9 -l-11" shadows are offset 9 pixels from top of the window and 11 pixels from the left edge

"-r9" shadow radius is 9 pixels

"-o.95" shadow opacity is set to 0,95

"-D6" the time between each step when fading windows is set to 6 milliseconds.

"&" isn't actually xcompmgr's option, it's a built-in function in the shell itself, sending the command to run in the background.
