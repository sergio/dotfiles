---
file: mpv.md
author: Sergio Araujo
Last Change: Tue, 12 Mar 2024 - 06:35:34
 vim:ft=markdown
tags: [mp3, music, player]
---

## mpv.md
+ https://github.com/mpv-player/mpv/wiki/User-Scripts

User Scripts to add new features:

... adding functionality that is not part of the core mpv player. Most of these scripts are unofficial 3rd party scripts. Anyone can add their own script by editing this wiki. On GitHub mpv scripts are tagged as mpv-script.

+ https://github.com/yassin-l/cp-paste-url
+ https://github.com/ftk/mpv-confluence (open magnet links)
+ https://github.com/linguisticmind/mpv-scripts/tree/master/copy-subtitle
+ https://gist.github.com/avih/41acff712abd32e1f436235388c8b523
+ https://github.com/yuukidach/mpv-scripts
+ https://github.com/alifarazz/mpv-zenity-open-files
+ https://github.com/liberlanco/mpv-lang-learner
+ https://sr.ht/~guidocella/mpv-lrc (lyrics)
+ https://github.com/Kayizoku/mpv-rename
+ https://github.com/rohieb/mpv-notify
+ https://github.com/olivierlemoal/mpv-radio-title-script/
+ https://github.com/kelciour/mpv-scripts/blob/master/sub-bilingual.lua
+ https://github.com/cvzi/mpv-youtube-download

## Install flatpack version:

    Install:
    Make sure to follow the setup guide before installing

    flatpak install flathub io.mpv.Mpv

    Run:
    flatpak run io.mpv.Mpv

## config file: ~/.config/mpv/mpv.conf

A media player

## play random files just audio

    mpv --shuffle --no-video **/*.mp3

## watch youtube videos throught mpv

    mpv --ytdl url

    by default mpv cli user this option

## set window name to use in bspwm rules:

    mpv --x11-name=name

## Listen part of a video

    mpv --no-video --ab-loop-a=2:50 --ab-loop-b=5:30 youtubelink

    mpv --no-video --start=2:46 --end=5:30 youtubelink

## dump audio
+ https://bit.ly/3pnewlj
+ https://github.com/mpv-player/mpv/issues/78#issuecomment-41471243

    mpv -o matrix-dialog.mp3 --no-video --start=1:38 --end=3:27 https://youtu.be/dJTSR4i7cYc

## Baixando albuns do youtube
+ https://docsumo.com/free-tools/online-ocr-scanner OCR

I use a online OCR to get the list of musics of online albuns, in general
people put the list of musics as images on the video cover or something like
that, then I copy the times and subtract them using a script of mine:

The command bellow will subtrace two seconds of the second field (my script is
on my ~/.dotfiles/bin/less2sec)

A regex [^ ]* pega o último bloco da linha que é o tempo final (cópia do inicio
da próxima música) e subtrai 2 segundos, o seja a música corrente termina 2
segundos antes de começar a próxima.

    :%s/[^ ]*\ze$/\=system('less2sec ' . submatch(0))

O less2sec eu aplico ao segundo campo quando tá assim:

    00:04:52 00:06:09 (o útimo campo aqui tá igual ao primeiro da próxima linha)
    00:06:09

Pego o tempo de início da próxima música, copio para o final da linha
anterior e subtraio 2 segundos usando meu script less2sec


Daí eu monto a linha de download assim:

    mpv --no-video --start=00:00:00 --end=00:04:52  -o 01-Its-A-Sin.mp3 https://www.youtube.com/watch?v=JRYvfph8aGo

## função ouvir o audio de um video do youtube

listen youtube

    function mm() {
        mpv --no-video --ytdl-format=bestaudio ytdl://ytsearch10:"$@"
    }

a opção `--no-video` é auto explicativa e a opção `--ytdl` é um acrônimo que faz referência ao youtube-dl

```sh
mpv --no-video --ytdl https://www.youtube.com/watch\?v\=IpQk8qByP_4
```

## help system

    mpv --help

## In order to be able to play DVD with mpv

    mpv dvd://

## Control start point

    mpv --start=7:10 url

## Extract audio

- https://unix.stackexchange.com/a/229815/3157

  mpv videofile.avi --o=audiofile.mp3 --no-video mpv --no-video --o=paulo-sergio.mp3
  https://www.youtube.com/watch\?v\=WuetuG2DReY

    mpv --no-video --start=24:16 --end=27:44 --o=musica.mp3 link

# Extract audio from video file at a specific time. (download)

    mpv videofile.avi -o audiofileAtSpecificTime.mp3 --no-video --start=00:01:00 --end=00:03:35

    mpv --no-video --end=2:32 -o output.mp3 URI

    mpv --no-video "$(yturl <url>)"

## Save video playing in mpv
+ https://superuser.com/a/1358113/45032

    mpv --record-file="$filename" "$url"

## How to watch a live stream from the beginning in mpv

- https://haasn.xyz/posts/2016-11-04-how-to-watch-a-live-stream-from-the-beginning-in-mpv.html.mp3

by Niklas Haas on November 4, 2016

Tagged as: mpv, tips.

If you try watching a video recording on twitch etc. while it’s still ‘live’, mpv/youtube-dl will play the live video
instead of starting from the beginning.

The fix is straightforward: By appending `?t=0m` you can force it to start at the beginning:

    mpv 'https://www.twitch.tv/example/v/12345?t=0'

This is also useful for a second purpose: seeking. Normally, by trying to seek a live stream like this in mpv you will
end up buffering and downloading forever. (I’m not exactly sure what’s going on since the mpv cache is so opaque, but I
have to imagine it’s actually trying to download all the data you skipped past)

By changing it to e.g. `?t=20m` you can seek to 20 minutes in the stream.

## Shortcuts

+ https://mpv.io/manual/master/#keyboard-control
+ http://bit.ly/304RN5o

To see a list of input keys:

    mpv --input-keylist

```markdown
# ............. circle through avaiable audio tracks
up/down ....... seek 1 minute forward/downword
-> ............ seek + 10 seconds
<- ............ seek - 10 seconds (rewind)
<space> | p ... pause
q ............. stop playing and quit
Q ............. same as q but stores play position
m ............. mute sound
f ............. full screen
o ............. show progressing bar
z and Z ....... adjust subtitle delay
Ctrl+ Ctrl- ... adjust audio delay
s ............. take a screenshot
S ............. take a screenshot without subtitles
1 and 2 ....... contrast
3 and 4 ....... brightness
9 and 0 ....... increase/decrease volume
v ............. show hide sutbitles
j / J ......... Next/Previous subtitles
r / R ......... subtitles position
/ *  .......... increase decrease volume
[ ............. decrease playback speed by 10:VimwikiReturn 10%
] ............. increase playback speed by 10:VimwikiReturn 10%
backspace ..... reset playback speed to normal
< > ........... previous / next track
```

## Start with a given volume:

    volume=40         # default volume

## Baixar os 20 maiores sucessos do Pinduca no youtube

+ https://www.youtube.com/watch?v=DOAEWtiJ0EY

Tente usar --split-chapters

  OBS: cada arquivo tem o seu tempo descrito no link da página desse modo podemos colocar o tempo de início e fim de
  cada música usando: --start= e --end=

```sh
    mpv --no-video -o 01-o_rico_e_o_pobre.mp3 --start=00:00:00 --end=00:03:14 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 02-juliana.mp3 --start=00:03:16 --end=00:05:42 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 03-sinha_pureza.mp3 --start=00:05:44 --end=00:08:33 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 04-garota_do_tacaca.mp3 --start=00:08:35 --end=00:11:25 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 05-tia_luzia_tio_jose.mp3 --start=00:11:27 --end=00:14:36 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 06-bala_de_rifle.mp3 --start=00:14:38 --end=00:17:19 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 07-carimbó_do_soco.mp3 --start=00:17:21 --end=00:20:16 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 08-comadre_compadre.mp3 --start=00:20:18 --end=00:23:10 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 09-o_pinto.mp3 --start=00:23:12 --end=00:25:41 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 10-a_bailar_comantchera.mp3 --start=00:25:43 --end=00:28:58 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 11-dança_do_carimbo.mp3 --start=00:29:00 --end=00:31:52 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 12-sem_voce_nada_é_bonito.mp3 --start=00:31:54 --end=00:34:43 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 13-sirimbo_do_vovô.mp3 --start=00:34:45 --end=00:38:10 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 14-dona_mariana.mp3 --start=00:38:12 --end=00:40:59 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 15-sirimbo.mp3 --start=00:41:01 --end=00:43:34 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 16-carimbo_do_para.mp3 --start=00:43:36 --end=00:46:25 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 17-o_cacador.mp3 --start=00:46:27 --end=00:49:28 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 18-maria_judia.mp3 --start=00:49:30 --end=00:51:19 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 19-e_bahia.mp3 --start=00:51:21 --end=00:53:52 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
    mpv --no-video -o 20-meu_forrobodo.mp3 --start=00:53:52 'https://www.youtube.com/watch?v=DOAEWtiJ0EY'
```

mpv --no-video --start=00:00:00 --end=00:03:14 -o o-rico-e-o-pobre.mp3 https://www.youtube.com/watch\?v\=DOAEWtiJ0EY mpv
--no-video --start=00:03:16 --end=0 -o o-rico-e-o-pobre.mp3 https://www.youtube.com/watch\?v\=DOAEWtiJ0EY

# vim: ft=markdown et sw=4 ts=4 cole=0
