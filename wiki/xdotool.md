---
File: xdotool.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [tools, x11, mouse, screen]
---

#  xdotool.md intro:

[xdotool(it's minimal)](https://www.semicomplete.com/projects/xdotool/). You can simulate key presses, mouse movement, clicks
and more. You can even move and resize windows, modify window properties like
titles and stuff. There are examples at the bottom of the page, ways you can
utilise this tool. I personally think of using it to automate clicking on
buttons of certain GUI applications, and then filling in some details
required. Be sure to check it out, it's really awesome!

Example: focus the firefox url bar

``` sh
WID=`xdotool search "Mozilla Firefox" | head -1`
xdotool windowactivate --sync $WID
xdotool key --clearmodifiers ctrl+l
```

## paste primary selection:
+ https://askubuntu.com/a/7775/3798
+ https://askubuntu.com/a/390334/3798

    xdotool click --clearmodifiers 2

Usando o xclip -> `xclip -selection primary -o`

# As of version 2.20100623, you can do this simpler version of above:

``` sh
xdotool search "Mozilla Firefox" windowactivate --sync key --clearmodifiers ctrl+l

bash -c 'sleep .2;xdotool search "Mozilla Firefox" windowactivate --sync key --clearmodifiers ctrl+l; \
    sleep 1;xdotool key BackSpace; xdotool type "$(xclip -selection primary -o)"; \
    xdotool key Return'

```

## Detect mouse moviment:
+ https://stackoverflow.com/a/13907499/2571881

We can use the function bellow to get mouse coordinates and test if it
has moved so.

```sh
# gets mouse x coordinate
mousex (){
    echo "$(xdotool getmouselocation --shell | awk -F= 'NR==1 {print $2}')"
}

pos=$( mousex )
```

## Resize all visible gnome-terminal windows

``` sh
WIDS=`xdotool search --onlyvisible --name "gnome-terminal"`
for id in $WIDS; do
  xdotool windowsize $id 500 500
done
```

## jump to an already open firefox window:
See also [wmctrl](wmctrl.md)

    xdotool search "Mozilla Firefox" windowactivate --sync key --clearmodifiers ctrl+l

    bash -c 'xdotool search "Mozilla Firefox" windowactivate --sync key --clearmodifiers ctrl+l; xdotool key BackSpace'

    bash -c 'xdotool search "Mozilla Firefox" windowactivate --sync key --clearmodifiers ctrl+l; xdotool key BackSpace; xdotool type "$(xclip -selection primary -o)";xdotool key Return'

## Open a new terminal and type ls:

    xdotool key 'Super+t'; xdotool type "ls"; xdotool key Return

## Repeat keys:

    xdotool key --repeat 5 --delay 50 n

# As of version 2.20100623, you can do this simpler version of above:

``` sh
xdotool search --onlyvisible --classname "gnome-terminal" windowsize %@ 500
500
```

## Simulate a keystroke

    xdotool key n

