---
File: /home/sergio/.dotfiles/wiki/cava.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [music, midia, mp3, equalizer, glava, twm]
---

## Console-based Audio Visualizer for Alsa (MPD and Pulseaudio)
+ https://github.com/karlstav/cava#what-it-is
+ https://forum.maboxlinux.org/t/using-terminal-for-playing-youtube-music-and-videos/852/3

C.A.V.A. is a bar spectrum audio visualizer for the Linux terminal using ALSA, pulseaudio or fifo buffer for input.
This program is not intended for scientific use. It's written to look responsive and aesthetic when used to visualize music.

    Ctrl + seta ................. aumenta o numero de linhas do equalizador

## see also:

+ https://www.linuxuprising.com/2018/11/embed-audio-visualizer-on-your-linux.html
