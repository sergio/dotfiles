---
file: hh0.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags [hidrolise, hh0, free-energy]
---

# HHO Tips
+ http://www.youtube.com/watch?v=2i4bef33dZ8&feature=related

Nao se usa mais soda caustica amigo nem bicarbonato esquenta muito agora so usa
hidroxido de potassio formula koh a 90% facil de encontrar em lojas de produtos
quimicos com agua da chuva filtrada ou agua destilada ok boa sorte

## O tipo de aço:

Não é um aço comum, inox 316, e deve ser aço escovado, o que aumenta a produção de hidrogênio

## Quanto à detecção da quantidade de ar na admissão (sonda Efie):
+ https://www.youtube.com/watch?v=XqhSdwUOCJ8&t=6s

Efie T634 Sonda Lambda Gnv E Hho Geradores De Hidrogenio - É uma sonda que fica na saída da descarga
e envia sinais para o sistema de ingeção de modo que o carro não aumente a quantidade de combustível
por conta do gás hidrogênio que está sendo colocado no sistema.

### Existem células que geram hidrogênio puro que não precisam de Efie
+ https://www.youtube.com/watch?v=GCgIaWO6rGU
+ https://www.youtube.com/watch?v=g3LAX54Z2LY
+ https://www.youtube.com/watch?v=03ibsVB62o0

São células que tem uma montagem mais complexa mas geram hidrogênio e oxigênio separados, de modo que não
gera excedente de ar no sistema, tornando desnecessário o uso de sonda EFIE.

Usase uma membrana de 150 fios (usada em silk screen) coloca-se a membrana no meio de duas borrachas de vedação


#### Passo a Passo das Disposições dos elementos do Gerador de Hidrogênio com separação de Gases
No gerador de hidrogênio com separação de gases, temos os seguintes elementos.
+ https://geradorhidrogenio.blogspot.com/

    2 Placa de Acrílico de 10 mm
    4 Borrachas para direcionar o Oxigênio
    4 Borrachas para direcionar o Hidrogênio
    6 Borrachas para garantir a separação
    1 Placa Inox 304L para Positivo
    1 Placa Inox 304L para Negativo
    2 Placa Inox 304L para Neutro
    3 Membranas de Nylon 150 ( o mesmo utilizado para SilkScreen )

    Placa acrílico de 10 mm
    Borracha (abertura do lado do oxigênio)
    Placa Positiva
    Borracha (abertura do lado do oxigênio)
    Borracha (sem abertura)
    Membrana
    Borracha (sem abertura)
    Borracha (abertura do lado do Hidrogênio)
    Placa Neutra
    Borracha (abertura do lado do oxigênio)
    Borracha (sem abertura)
    Membrana
    Borracha (sem abertura)
    Borracha (abertura do lado do Hidrogênio)
    Placa Neutra
    Borracha (abertura do lado do oxigênio)
    Borracha (sem abertura)
    Membrana
    Borracha (sem abertura)
    Borracha (abertura do lado do Hidrogênio)
    Placa Negativa
    Borracha (abertura do lado do Hidrogênio)
    Placa acrílico de 10 mm

## PWM - controle do consumo da célula
