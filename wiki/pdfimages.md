---
file: pdfimages.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# pdfimages - dom 01 abr 2018 12:12:02 -03

## Extract images from a pdf file

    pdfimages -j file.pdf target-folder

