---
File: .dotfiles/wiki/xset.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [screen, desktop, utils, x11, screensaver, dpms]
 vim:ft=markdown
---

## intro:
+ https://sleeplessbeastie.eu/2022/02/08/how-to-use-dpms-to-turn-screen-on-or-off/

The xset command changes user settings for the X Window System. You can use xset to change the basic characteristics of your GUI, including the way your mouse and keyboard behave, and which display to use for your X session.

	xset q

Display the values of all current X Window System preferences.

	xset r rate 190 35

https://www.reddit.com/r/vim/comments/1lvg52/
(The first number is after how many ms the key will start repeating and the second number is the repetitions per second, so after 190ms of pressing 'R' the OS will put out 35 'R's a second)

## close screen

    sleep 0.5; xset dpms force standby

    xset dpms force off

## avoid turning off your screen:

    xset s off

## disable power saving mode:

    xset -dpms

