Filename: shellcheck.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [bash, zsh, shell, tools, check]

 vim:ft=markdown
## Artigo sobre o shellcheck
+ https://meleu.sh/shellcheck/

Use shellcheck e livre-se dos bugs no seu código antes mesmo de executá-lo
Com o shellcheck você vai se livrar de aborrecimentos com bugs que você jamais conseguiria prever!

    shellcheck script
