---
file: puredebatatas.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Receita de Purê de Batata da Mamãe

    Last Change: abr 03 2019 08:45

    4 batatas médias
    1 gema
    1 colher de manteiga bem cheia (não vale margarina)
    1 pitada de sal (a gosto)
    1 pires de queijo ralado (pequeno)
    1 copo de Leite pequeno (copo de geléia)
    2 colheres de creme de leite

## Modo de preparo
Cozinha a bata muito bem cozida  (tira do fogo)
descasca a bata e machuque até ficar bem machucada

coloque a batata na caçarola que vai ao fogo (novamente)
mas não acenda o fogo ainda

Adiciona a gema (sem pele) até misturar bem na batata
de modo a não ficar pedaços de ovo inteiros

Adicione o leite vagarozamente mexendo até ficar bem misturado
massa homogênea.

Coloque a caçarola no fogo,

adicione a manteiga mexendo sempre
coloque o sal

quando estiver cozido, a ponto
de mexer  e descobrir o fundo da panela

balance a ciaxa do creme de leite e adicione duas colheres
mas somente quando estiver perto de tirar do fogo.
(pode substituir com nata)

tira do fogo e adiciona o
queijo ralado e mexe.

tag: receitas
