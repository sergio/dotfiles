---
Filename: upx.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [tools, admin, compress]
 vim:ft=markdown
---

## upx:
+ https://www.hardware.com.br/dicas/comprimindo-executaveis-upx.html

O programa upx comprime executáveis em até 70%

    doas upx -9 -v /bin/nvim



# vim:cole=0:
