---
File: lua.md
Last Change: Thu, 07 Nov 2024 - 15:53:08
tags: [lua, programming]
---

## Referências:

+ https://terminalroot.com.br/lua/
+ https://www.lua.org/history.html

##  links:

+ https://www.gatlin.io/blog/post/lua-primer-for-neovim
+ https://github.com/neovim/neovim/wiki/FAQ#why-embed-lua-instead-of-x

## Webrowser in lua:

```sh
doas xbps-install -Sy luakit
```

## Print table:

```lua
for k,v in pairs(table) do
 print(v)
end
```

## laço for:

```lua
-- valor crescente
for i = 1,4 do
 print("Imprimindo "..i.."º vez!")
end

-- valor decrescente
for i = 4,1,-1 do
 print("Imprimindo "..i.."º vez!")
end
```
## add value for a variable conditionally:

```lua
opts = opts or {}
```


## lua packge management

```sh
doas xbps-install -Sy luarocks
luarocks --local install mpack
```


## função em lua para alternar aspas (toggle quote)

```vim
:s/["']/\=submatch(0) == '"' ? "'" : '"'/g
```


## get the systemm name and other environmente variables:

+ https://vi.stackexchange.com/a/11711/7339

    lua print(vim.loop.os_uname().sysname)
    lua print(vim.fn.hostname())

    let foo = exists('$FOO') ? $FOO : 'default'
    echo exists('$EXTERNAL_STORAGE') ? 'android' : 'void'

    usando lua ternary trick:
    existence = vim.fn.exists('$EXTERNAL_STORAGE') and 'android' or 'voidlinux'
    lua print(vim.fn.hostname() == "localhost" and "android" or "voidlinux")

    -- I have set --> export HOST=android on termux
    if vim.env.HOST == 'android' then
        vim.g.python3_host_prog='/data/data/com.termux/files/usr/bin/python'
    else
        vim.g.python3_host_prog='/bin/python'
    end

## pegar só o nome sem o caminho no vim:

+ https://stackoverflow.com/a/56513627/2571881

    -- remove extension
    local file = vim.api.vim_buff_get_name(0):match("[^/]*.lua$")

## string library

```lua
--Size of a string
print(string.len(sua_string))
print(#sua_string)

-- match a string
local findstring = 'helo wordl'
local found = string.match(findstring, 'world')

-- substitution
string.gsub('lua is cool', 'cool', 'awesome')
```

## Get your system platform info:

+ https://stackoverflow.com/questions/48093429

```lua
local arch
if (os.getenv"os" or ""):match"^Windows" then
   print"Your system is Windows"
   arch = os.getenv"PROCESSOR_ARCHITECTURE"
else
   print"Your system is Linux"
   arch = io.popen"uname -m":read"*a"
end
if (arch or ""):match"64" then
   print"Your system is 64-bit"
else
   print"Your system is 32-bit"
end
```

## Escape sequences

(Currently Lua has the following Escape Sequences:
```lua
    \a: Bell
    \b: Backspace
    \f: Form feed
    \n: Newline
    \r: Carriage return
    \t: Tab
    \v: Vertical tab
    \\: Backslash
    \": Double quote
    \': Single quote
    \nnn: Octal value (nnn is 3 octal digits)
    \xNN: Hex value (Lua5.2/LuaJIT, NN is two hex digits)
)
```
## os library

``` lua
os.setlocale("en_US.UTF-8")
os.setlocale("pt_BR.UTF-8")
time = os.date("%a, %d %b %Y - %H:%M")
```

Gettig timestamp

```lua
os.time(os.date("!*t"))

print(os.date("!%c",783477811))
--> Sat Oct 29 19:43:31 1994
```


## substituting text

```lua
string = "Lua Tutorial"

-- replacing strings
newstring = string.gsub(string,"Tutorial","Language")
print("The new string is "..newstring)
string.gsub('texto', '$', '.md')
```

## Try-catch i lua:

+ https://riptutorial.com/lua/example/16000/using-pcall
+ https://old.reddit.com/r/neovim/comments/u9ty5f/

pcall stands for "protected call". It is used to add error handling to functions. pcall works similar as try-catch in other languages. The advantage of pcall is that the whole execution of the script is not being interrupted if errors occur in functions called with pcall. If an error inside a function called with pcall occurs an error is thrown and the rest of the code continues execution.

Syntax:

```lua
pcall( f , arg1,···)
pcall(vim.cmd, "hi badHighlight")
pcall(function() vim.cmd [[somewrongcommand]] end)

-- Explore
pcall(function()
  require('neo-tree')
  map('n', '<leader>ef', '<cmd>Neotree focus<cr>')
  map('n', '<leader>et', '<cmd>Neotree toggle<cr>')
end)
```

# vim: cole=0:
