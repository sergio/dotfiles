---
File: ~/.dotfiles/wiki/nvim-markdown.md
last change: Thu, 18 Apr 2024 - 11:44:43
---

# Intro

Fork of vim-markdown with extra functionality.

- [Nvim markdown](https://github.com/ixru/nvim-markdown "Nvim plugin")

## Mappings

```txt
]]: Go to next header. <Plug>Markdown_MoveToNextHeader
[[: Go to previous header. Contrast with ]c. <Plug>Markdown_MoveToPreviousHeader
][: Go to next sibling header if any. <Plug>Markdown_MoveToNextSiblingHeader
[]: Go to previous sibling header if any. <Plug>Markdown_MoveToPreviousSiblingHeader
]c: Go to Current header. <Plug>Markdown_MoveToCurHeader
]u: Go to parent header (Up). <Plug>Markdown_MoveToParentHeader
Ctrl-c: Toggle checkboxes. <Plug>Markdown_Checkbox
Tab: Fold headers/lists. <Plug>Markdown_Fold
Tab: Indent new bullets, jump through empty fields in links. <Plug>Markdown_Jump
Ctrl-k: Create new links. <Plug>Markdown_CreateLink
Return: Follow links. <Plug>Markdown_FollowLink
```

Commands

The following requires `:filetype plugin on.`

    :HeaderDecrease: Decrease level of all headers in buffer: h2 to h1, h3 to h2, etc.

If range is given, only operate in the range. If an h1 would be decreased, abort. For simplicity of implementation,
Setex headers are converted to Atx.

    :HeaderIncrease: Analogous to :HeaderDecrease, but increase levels instead.

    :SetexToAtx: Convert all Setex style headers in buffer to Atx.

If a range is given, e.g. hit : from visual mode, only operate on the range.

    :Toc: create a quickfix vertical window navigable table of contents with the headers. Hit <Enter> on a line to jump to the corresponding line of the markdown file.

    :Toch: Same as :Toc but in a horizontal window.

    :Tocv: Same as :Toc but in a vertical window.

    :InsertToc: Insert table of contents at the current line.

    An optional argument can be used to specify how many levels of headers to display in the table of content, e.g., to display up to and including h3, use :InsertToc 3.

    :InsertNToc: Same as :InsertToc, but the format of h2 headers in the table of contents is a numbered list, rather than a bulleted list.
