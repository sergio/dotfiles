---
file: youtube-dl.md
author: Sergio Araujo
Last Change: Fri, 15 Mar 2024 - 19:17:40
tags: [youtube, mp3, download, tools]
---

+ https://github.com/rg3/youtube-dl/blob/master/README.md

## Download and split chapters

     yt-dlp --split-chapters -x --audio-format mp3 https://www.youtube.com/watch\?v\=DOAEWtiJ0EY

## comando para atualizar o youtube-dl?

    sudo pip3 install youtube-dl --upgrade

## youtube-dl fork
+ https://github.com/blackjack4494/yt-dlc
+ https://github.com/yt-dlp/yt-dlp
+ https://write.corbpie.com/downloading-youtube-videos-and-playlists-with-yt-dlp/

yt-dlp is a youtube-dl fork based on the now inactive youtube-dlc. The main focus of this project is adding new features and patches while also keeping up to date with the original project

config file:

    $XDG_CONFIG_HOME/yt-dlp/config

    --split-chapters for videos with internal chapters:
    --restrict-filenames flag to get a shorter title

## cowboy bebop playlist
+ https://youtube.com/playlist?list=PL65E33789AA7052BC

## How to download portion of video with youtube-dl command?
+ https://unix.stackexchange.com/a/406246/3157
+ https://unix.stackexchange.com/questions/230481/

    It follows the HH:MM:SS pattern

    youtube-dl --postprocessor-args "-ss 00:00:10 -to 00:04:56" -x --audio-format mp3 $(pbpaste)

## avoid strange characters

    --restrict-filenames

## Install youtube-dl on android
+ https://techwiser.com/how-to-use-termux-to-download-youtube-videos/

First install termux, then:

    wget http://pastebin.com/raw/W1nvzN6q -O install.sh

    dos2unix install.sh

    chmod u+x install.sh

    ./install.sh

Now, if you go to any video sites that is supported
by YouTube-dl and then tap on share button > Select termux app >
select any format by typing the number corresponding to it.
And there we go.

## Numerando um playlist durante o download
+ https://askubuntu.com/a/1081890
+ https://askubuntu.com/a/844773/3798

    youtube-dl -o '%(playlist_index)s. %(title)s.%(ext)s'

    youtube-dl -o "%(playlist_index)02d - %(title)s.%(ext)s"
                                    .
                                   /|\
                                    |

    or 3d or 4d and so on, as in C's printf:

    Using 2d, you get numbers starting from 01
    Using 3d, you get numbers starting from 001
    And so on...

    youtube-dl -cio '%(autonumber)s-%(title)s.%(ext)s'

## Controling video start point

    https://www.youtube.com/watch?v=plS_MkupCjs?t=1h25m30s

Somtimes you have to convert hours to minutes and put something like 1h(3600) + 26m(26*60)

## pegando o link do video pra assistir no mplayer

    youtube-dl -g "link"

## Baixando de uma lista
Se você tem um arquivo com as url's use:

    youtube-dl -a links.txt

## preservando o título do vídeo

    youtube-dl -t link

## Limiting bandwidth

    youtube-dl -r 30k https://www.youtube.com/watch\?v\=XnrAM9QZ90U

    youtube-dl  --rate-limit 20k

## determinando a qualidade do video a ser baixado
+ https://askubuntu.com/a/866432/3798

Pesquise neste link http://en.wikipedia.org/wiki/YouTube#Quality_and_codecs
OBS: Por padrão o youtube-dl irá baixar o video na melhor qualiade possível

	youtube-dl -f 34 "http://www.youtube.com/watch?v=2_VFKqw1q2Q"

    youtube-dl -f 'bestvideo[height<=480]+bestaudio/best[height<=480]' link
    youtube-dl -f "best[height<=480]" link

## baixando video com legenda (transcript)
Downloading video with subtitles

    youtube-dl --write-srt --sub-lang en LINK
    youtube-dl --write-srt --sub-lang --skip-download en LINK
    youtube-dl --write-srt --sub-lang --skip-download en $(pbpaste)

    youtube-dl --all-subs --skip-download LINK

Você pode criar um arquivo de configuração ...

    vim .config/youtube-dl.conf

e nele colocar as opções que desejar, caso queira ignorar
o arquivo de configuração você pode colocar a seguinte opção
na linha de comando:

    --ignore-config

## convertendo videos do youtube para mp3
Fonte:

    youtube-dl -t --extract-audio --audio-format mp3 YOUTUBE_URL_HERE
    youtube-dl -xt --audio-format mp3 https://www.youtube.com/watch?v=jwD4AEVBL6Q

OBS: The option `-t` makes youtube-dl get original title
     The option -x extracts audio

    If you want to have a cover art for the mp3 file, you can add the
    --embed-thumbnail option

## Getting a portion of a youtube video

    youtube-dl -x --postprocessor-args -ss 00:00:42.00 -t 00:02:02.00
    https://www.youtube.com/watch?v=FjE4-yT2B-4

## baixar um playlist completo
Antes de executar o comando certifique-se que a versão do youtube-dl é a mais nova

    youtube-dl -U

Agora pode executar algo como...

	youtube-dl  http://www.youtube.com/playlist?list=PLBACC77E2173B4280&feature=mh_lolz

## Download just current video from a playlist

    youtube-dl -x --audio-format mp3 --no-playlist link
    youtube-dl -x --audio-format mp3 --no-playlist $(pbpaste)

## Get playlist links

    youtube-dl -j --flat-playlist link
    youtube-dl --flat-playlist -j link | jq .

## Pegar o link de cada video de uma playlist

    youtube-dl -i --get-filename --skip-download link

## external downloader

    --external-downloader COMMAND

              Use the specified external downloader.  Currently supports
              aria2c,av‐ conv,axel,curl,ffmpeg,httpie,wget

      --external-downloader-args ARGS
              Give these arguments to the external downloader

## How many sites can I use with youtube-dl?

    youtube-dl --extractor-descriptions

tags: network, tools, download, youtube
