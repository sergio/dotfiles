File: /home/sergio/.dotfiles/wiki/nemo.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [file-manager, tools, rename]

 vim:ft=markdown
Nemo is a graphical file manager

## Bulk rename
+ https://itsfoss.com/nemo-tweaks/

Nemo also offers a bulk rename feature that many Linux users are not aware of.

What you have to do is, select the files and select rename from the right click. You’ll get different kinds of options to tweak the names of the selected group of files.
