---
file: slinux.md
author: Sergio Araujo
---
 vim:ft=markdown


# Guia de comandos linux

``` markdown
File:		 ComandosLinux.md
Last Change: 2018 jun 05 11:01
Author:		 Sergio Araujo
Site:		 http://vivaotux.blogspot.com
e-mail:      <voyeg3r ✉ gmail.com>
Twitter:	 @voyeg3r
Github:      https://github.com/voyeg3r
```


Alem dos comandos abaixo estão listados Atalhos de programas
começando pelo [[VlC]]

* [ComandoAck|ack](ComandoAck|ack.md) - Fitra textos como o grep
* [ComandoAddgroup|addgroup](ComandoAddgroup|addgroup.md) - adiciona grupos de usuários
* [ComandoAdduser|adduser](ComandoAdduser|adduser.md) - adiciona usuários ao sistema
* [ComandoAg|ag](ComandoAg|ag.md) - parecido com o grep só que mai rápido
* [ComandoAircrack-NG|aircrack-NG](ComandoAircrack-NG|aircrack-NG.md) - ferramenta hacker para redes wi-fi
* [ComandoAlias|alias](ComandoAlias|alias.md) - cria um atalho para um comando
* [ComandoAlsaconf|alsaconf](ComandoAlsaconf|alsaconf.md) - configura o som através do sistema alsa
* [ComandoApropos|apropos](ComandoApropos|apropos.md) - busca manuais
* [ComandoApt|apt](ComandoApt|apt.md) - instala programas
* [ComandoAria2c|aria2c](ComandoAria2c|aria2c.md) - programa acelerador de downloads
* [ComandoArpscan|arpscan](ComandoArpscan|arpscan.md) - diagnóstico de rede
* [ComandoArp-scan|arp-scan](ComandoArp-scan|arp-scan.md) - diganóstico de rede (como root)
* [ComandoA2ps|a2ps](ComandoA2ps|a2ps.md) - gera arquivos ps de uma entrada
* [ComandoAptitude|aptitude](ComandoAptitude|aptitude.md) - gerenciador de pacotes
* [ComandoAt|at](ComandoAt|at.md) - agenda comandos
* [ComandoAvconv|avconv](ComandoAvconv|avconv.md) - video converter
* [ComandoAwk|awk](ComandoAwk|awk.md) - manipula textos
* [ComandoAxel|axel](ComandoAxel|axel.md) - acelerador de downloads
* [ComandoBasename|basename](ComandoBasename|basename.md) - retorna o nome do arquivo sem o caminho
* [ComandoBadblocks|badblocks](ComandoBadblocks|badblocks.md) - Comando para recuperação de hds
* [ComandoBc|bc](ComandoBc|bc.md) - calculadora de linha de comando
* [ComandoBeep|beep](ComandoBeep|beep.md) - emite um som no terminal
* [ComandoBlkid|blkid](ComandoBlkid|blkid.md) - exibe o 'uuid' volume id das partições
* [ComandoCadaver|cadaver](ComandoCadaver|cadaver.md) - permite fazer downloads e uploads via webdav
* [ComandoCase|case](ComandoCase|case.md) - extrutura para escolhas múltiplas
* [ComandoCat|cat](ComandoCat|cat.md) - mostra na tela arquivos
* [ComandoCancel|cancel](ComandoCancel|cancel.md) - cancela trabalhos de impressão
* [ComandoCd|cd](ComandoCd|cd.md) - entra em diretórios
* [ComandoCdrecord|cdrecord](ComandoCdrecord|cdrecord.md) - grava cds e dvds em modo texto
* [[ComandoCdparanoia|cdparanoia]] - extrai faixas de cd
* [[ComandoCfdisk|cfdisk]] - particionador de discos
* [[ComandoChkconfig|chkconfig]] - gerenciar serviços no linux
* [[ComandoChattr|chattr]] - modifica atributos
* [[ComandoChmod|chmod]] - modifica permissões de arquivos
* [[ComandoChown|chown]] - modifica o dono de arquivos e pastas
* [[ComandoChpasswd|chpasswd]] - permite modificar senhas através de scripts
* [[ComandoChroot|chroot]] - cria uma árvore falsa de arquivos
* [[ComandoChgrp|chgrp]] - altera o grupo
* [[ComandoClear|clear]] - limpa a tela
* [[ComandoComm|comm]] - compara arquivos
* [[ComandoColumn|column]] - formata saida em colunas
* [[ComandoCron|cron]] - "crontab -e" edita a tabela de agendamento de tarefas
* [[ComandoConvert|convert]] - recodifica caracteres
* [[ComandoConvmv|convmv]] - converte nomes de arquivos para outra codificação
* [[ComandoCp|cp]] - copia arquivos e pastas
* [[ComandoCpuinfo|cpuinfo]] - retorna informações da cpu
* [[ComandoCron|cron]] - agenda comandos para datas específicas
* [[ComandoCsplit|csplit]] - divide um arquivo em seções determinadas pelo contexto
* [[ComandoCurl|curl]] - transferencia de arquivos via rede
* [[ComandoCut|cut]] - manipula campos de uma saída
* [[ComandoDate|date]] - exibe e modifica a data do sistema
* [[ComandoDd|dd]] - faz cópias perfeitas e muito mais
* [[ComandoDetox|detox]] - Limpa os nomes de arquivos
* [[ComandoDf|df]] - mostra o espaço livre no disco
* [[ComandoDhclient|dhclient]] - configura a rede com ip dinâmico DHCP
* [[ComandoDiff|diff]] - compara exibindo em destaque as diferenças
* [[ComandoDig|dig]] - obtem informações do dns
* [[ComandoDnsmasq|dnsmasq]] - cache de dns para sua estação de trabalho
* [[ComandoDosfsck|dosfsck]] - verifica integridade de sistemas de arquivos vfat
* [[ComandoDpkg|dpkg]] - instala pacotes sem rede (gera dependências
* [[ComandoDu|du]] - mostra o espaço usado no disco
* [[ComandoDmesg|dmesg]] - log da inicialização do kernel
* [[ComandoDmidecode|dmidecode]] - mostra informações sobre o hardware
* [[ComandoEcho|echo]] - mostra uma string na tela
* [[ComandoEgrep|egrep]] - grep extendido suporta expressões regulares modernas
* [[ComandoEject|eject]] - ejeta cds
* [[ComandoEnv|env]] - mostra as variáveis de ambiente
* [[ComandoEmacs|emacs]] - editor super completo e complexo - se iniciante não se aventure ComandoEspeak - pronuncia textos em ingles
* [[ComandoEthtool|ethtool]] - configura parâmetros da interface de rede
* [[ComandoEttercap|ettercap]] - sniffer de rede
* [[ComandoExpr|expr]] - manipula expressões
* [[Comandofasd|fasd]] - acelera copia e acesso a arquivos
* [[ComandoFc|fc]] - edita o último comando
* [[ComandoFdisk|fdisk]] - particionador de discos
* [[ComandoFfmpeg|ffmpeg]] - conversor de formatos de midia
* [[ComandoFfmpeg2theora|ffmpeg2theora]] - converte videos para o formato ogv
* [[ComandoFor|for]] - repete comandos determinados
* [[ComandoFile|file]] - exibe o tipo do arquivo passado
* [[ComandoFind|find]] - busca arquivos e pastas
* [[ComandoFree|free]] - informa sobre o uso da memória
* [[ComandoFuser|fuser]] - identifica qual processo está usando um recurso
* [[ComandoFzf|fzf]] command line fuzzy finder
* [[ComandoGpasswd|gpasswd]] - adiciona usuário a grupos
* [[ComandoGit|git]] - adiciona usuário a grupos
* [[ComandoGparted|gparted]] - particionador de discos
* [[ComandoGrep|grep]] - filtra textos
* [[ComandoGrin|grin]] - filtra textos (baseado em python)
* [[ComandoGetconf|getconf]] - exibi informações do sistema
* [[ComandoHalt|halt]] - desliga o sistama
* [[ComandoHostname|hostname]] - mostra o nome do computador
* [[ComandoHead|head]] - mostra as primeiras linhas de um arquivo
* [[ComandoHistory|history]] - Exibe o histórico de comandos
* [[ComandoHtml2text|html2text]] - convert html para texto
* [[ComandoHwclock|hwclock]] - ajusta o relógio da placa mãe do computador
* [[ComandoIconv|iconv]] - recodifica arquivos
* [[ComandoId|id]] - mostra a identidade numérica do usuário
* [[ComandoIf|if]] - "se" - faz testes (use em scripts)
* [[ComandoIfs|ifs]] - internal field separator
* [[ComandoIfconfig|ifconfig]] - mostra e configura endereço de rede
* IftopMon 13 Jul 2015 07:52:17 PM BRT - mostra o uso da rede por aplicações
* [[ComandoImagemagick|imagemagick]] - manipula imagens pela linha de comando
* ImportMon 13 Jul 2015 07:52:17 PM BRT - captura imgens da tela
* InstallMon 13 Jul 2015 07:52:17 PM BRT - copia arquivos e configura atributos
* [[ComandoIp|ip]] - exibe e manipula rotas
* [[ComandoIptraf|iptraf]] - monitoramento de rede
* [[ComandoIptstate|iptstate]] - analizador de tráfego de rede
* [[ComandoIwconfig|iwconfig]] - configuração de rede wireless
* [[ComandoIwlist|iwlist]] - listar as redes wireless disponíveis
* [[ComandoKill|kill]] - mata processos
* [[ComandoKillall|killall]] - mata processos
* [[ComandoLastlog|lastlog]] - mostra o último login
* [[ComandoLet|let]] - perform arithmetic operations on shell variables
* [[ComandoLess|less]] - permite paginar uma saida na tela ou paginar arquivos
* [[ComandoLame|lame]] - cria e converte mp3
* [[ComandoLocate|locate]] - localiza arquivos pelo nome
* [[ComandoLp|lp]] - para imprimir
* [[ComandoLn|ln]] - cria links
* [[ComandoLs|ls]] - lista o conteúdo de diretórios
* [[ComandoLscpu|lscpu]] - CPU architecture details
* [[ComandoLsb_release|lsb_release]] - exibe a versão do sistema
* [[ComandoLsblk|lsblk]] - exibe a identificação única dos dispositivos
* [[ComandoLsmod|lsmod]] - lista os módulos carregados do kernel
* [[ComandoLshw|lshw]] - mostra informações sobre o hardware
* [[ComandoLsof|lsof]] - mostra arquivos abertos no sistema
* [[ComandoLspci|lspci]] - mostra informações sobre os dispositivos pci
* [[ComandoLynx|lynx]] - navegador web via linha de comandos
* [[ComandoMan|man]] - exibe o manula de um comando
* [[ComandoMc|mc]] - gerenciador de arquivos por linha de comando
* [[ComandoMcedit|mcedit]] - um editor de linha de comando fácil de usar
* [[ComandoMd5sum|md5sum]] - checa a integridade de arquivos
* [[ComandoMencoder|mencoder]] - conversor de formatos de video
* [[ComandoMondo|mondo]] - ferramenta de backup do sistema
* [[ComandoMount|mount]] - monta discos permitindo o acesso aos mesmos
* [[ComandoUmount|umount]] - desmonta dispositivos
* [[ComandoMkdir|mkdir]] - cria pastas
* [[ComandoMkdosfs|mkdosfs]] - cria sistema de arquivos fat
* [[ComandoMktemp|mktemp]] - cria arquivos temporários seguros
* [[ComandoMakepasswd|makepasswd]] - cria senhas seguras
* [[ComandoMkfs|mkfs]] - cria sistema de aruqivos - formata pendrives
* [[ComandoMv|mv]] - move ou renomeia arquivos e pastas
* [[ComandoMkisofs|mkisofs]] - cria imagens iso (inclusive de pastas
* [[ComandoMplayer|mplayer]] - assistir dvds e extrair audio
* [[ComandoNano|nano]] - editor de textos de linha de comando bem fácil de usar
* [[ComandoNbtscan|nbtscan]] - exibe informações netbios
* [[ComandoNewusers|newusers]] - cria usuários à partir de uma lista
* [[ComandoNetcat|netcat]] - escreve através de conexões de rede
* [[ComandoNetstat|netstat]] - mostra informações da rede
* [[ComandoNgrep|ngrep]] - um [[grep que filtra dados na rede
* [[ComandoNl|nl]] - mostra um arquivo na tela numerado
* [[ComandoNmblookup|nmblookup]] - procura no windows netbios o nome associado a um ip
* [[ComandoNmap|nmap]] - ferramenta superpoderosa para redes
* [[ComandoNproc|nproc]] - mostra a quantidade de núcleos do seu processador
* [[ComandoNslookup|nslookup]] - usado para obter informações de um servidor dns
* [[ComandoNtpdate|ntpdate]] - atualiza o relógio via rede
* [[ComandoOggenc|oggenc]] - converter audio para ogg
* [[ComandoPacman|pacman]] - instalador de pacotes do archlinux
* [[ComandoPandoc|pandoc]] - convert multiple file formats
* [[ComandoPdfimages|pdfimages]] - extrac images from pdf files
* [[ComandoPdftk|pdftk]] - manipula pdfs de várias formas
* [[ComandoPasswd|passwd]] - modifica a senha
* [[ComandoPaste|paste]] - descricao
* [[ComandoPerl|perl]] - linguagem de programação interpretada
* [[ComandoPerlRename|perl-rename]] - renomeia arquivos facilmente
* [[ComandoPidof|pidof]] - mostra o pid de um processo
* [[ComandoPing|ping]] - testa se um host está ativo
* [[ComandoPkill|pkill]] - mata processos pelo nome
* [[ComandoPppoeconf|pppoeconf]] - configura uma conexão adsl
* [[ComandoPrintf|printf]] - usado para imprimir valores na tela
* [[ComandoPs|ps]] - mostra números dos processos
* [[ComandoPwd|pwd]] - mostra o diretório atual
* [[ComandoPwgen|pwgen]] - gerador de senhas
* [[ComandoPump|pump]] - atribui endereço ip via [[dhcp
* [[ComandoPopd|popd]] - entra no último diretório da pilha de diretórios
* [[ComandoPushd|pushd]] - coloca diretório na pilha de diretórios
* [[ComandoRandom|random]] - gerador de números aleatórios do kernel
* [[ComandoRead|read]] - lê uma entrada digitada pelo usuário
* [[ComandoRedirecionamentos|redirecionamentos]] - descricao
* [[ComandoRcp|rcp]] - cópia remota
* [[ComandoRecordmydesktop|recordmydesktop]] - grava videos com som (do desktop)
* [[ComandoRecode|recode]] - recodifica arquivos
* [[ComandoRename|rename]] - renomeia arquivos
* [[ComandoRm|rm]] - remove arquivos e pastas
* [[ComandoRmmod|rmmod]] - descarrega ou desinstala módulos do kernel
* [[ComandoRoute|route]] - controle de rotas
* [[ComandoRsh|rsh]] - executa comandos remotos - remote shell
* [[ComandoRsync|rsync]] - sincroniza dados entre hosts
* [[ComandoScreen|screen]] - abre multiplos terminais em um só
* [[ComandoSed|sed]] - editor de streams
* [[ComandoSeq|seq]] - gera uma sequencia na tela
* [[ComandoSetxkbmap|setxkbmap]] - configura o teclado nos sistemas modernos com novo xorg
* [[ComandoScp|scp]] - permite cópias entre dois hosts
* [[ComandoSftp|sftp]] - Permite cópias mais seguras que o [[scp
* [[ComandoShred|shred]] - apaga um arquivo tornando o mesmo irrecuperável
* [[ComandoSitecopy|sitecopy]] - managing a WWW site via FTP, DAV or HTTP
* [[ComandoSox|sox]] Sound eXchange, the Swiss Army knife of audio manipulation
* [[ComandoSleep|sleep]] - aguarda um tempo até a continuação de um comando
* [[ComandoShutdown|shutdown]] - desliga ou renicia o comaputador
* [[ComandoShuf|shuf]] - gera permutações randômicas
* [[ComandoSmbclient|smbclient]] - permite acesso à máquinas windows
* [[ComandoSmbmount|smbmount]] - monta compartilhamentos windows
* [[ComandoSort|sort]] - ordena linhas de um arquivo
* [[ComandoSox|sox]] - Sound eXchange, the Swiss Army knife of audio manipulation
* [[ComandoSquid|squid]] - Controla acessos
* [[ComandoSplit|split]] - divide um arquivo em pedaços
* [[ComandoSponge|sponge]] - permite redirecionar um arquivo manipulado via pipe para dentro do mesmo
* [[SysDig|sysdig]] Definitive system and process troubleshooting tool
* [[systemctrl|systemctrl]] controla serviços no systemd
* [[ComandoStat|stat]] - mostra informações detalhadas sobre arquivos e diretórios
* [[ComandoStartx|startx.md]] - inicia modo gráfico
* [[ComandoSsh|ssh]] - permite acesso remoto de forma segura
* [[ComandoSvn|svn]] - sistema de controle de versões
* [[ComandoSwapon|swapon]] - habilita memória swap
* [[ComandoSync|sync]] - sincroniza dados entre memória e discos
* [[ComandoSudo|sudo]] allow user do things as admin
* [[ComandoTail|tail]] - exibe as ultimas linhas de um arquivo
* [[ComandoTar|tar]] - empacota arquivos compactados
* [[ComandoTcpdump|tcpdump]] - captura pacotes - sniffer
* [[ComandoTcpflow|tcpflow]] - captura fluxo tcp
* [[ComandoTime|time]] - testa o tempo de execução de um comando
* [[ComandoTidy|tidy]] - indenta códigos fonte
* [[ComandoTimidity|timidity]] - programa para tocar musicas midi - linha de comando
* [[ComandoTrickle|trickle]] - limita banda de conexão
* [[ComandoTree|tree]] - exibe a listagem do diretório como uma arvore
* [[ComandoTouch|touch]] - exibe a hora de criação de arquivos
* [[ComandoTee|tee]] - descricao
* [[ComandoTr|tr]] - faz substituição de caracteres
* [[ComandoTrap|trap]] - captura sinais (veja exemplo)
* [[ComandoTty|tty]] - exibe o nome do terminal atual
* [[ComandoType|type]] - exibe onde está um comando
* [[ComandoTzselect|tzselect]] - seleciona o timezone (país)
* [[ComandoUfw|ufw]] - novo firewall do linux
* [[ComandoUname|uname]] - exibe informações sobre a máquina
* [[ComandoUmask|umask]] - define e exibe as permissões ao criar arquivos e pastas
* [[ComandoUniq|uniq]] - remove linhas duplicadas
* [[ComandoUnaccent|unaccent]] - remove acentos de nomes
* [[ComandoUsermod|usermod]] - adiciona um usuário a um grupo
* [[ComandoVol_id|vol_id]] - fornece string de identificação de dispositivos (hd)
* [[ComandoWatch|watch]] - executa um comando periodicamente exibindo a saida
* [[ComandoWhatis|whatis]] - exibe a descrição sobre um comando dado
* [[ComandoWc|wc]] - conta linhas de um arquivo
* [[ComandoWget|wget]] - baixa arquivos e sites em modo texto
* [[ComandoWhich|which]] - O comando which é usado para localizar comandos
* [[ComandoWhereis|whereis]] - indica o local de um comando
* [[ComandoWho|who]] - informa quem está logado no computador
* [[ComandoWodim|wodim]] - grava dados em discos ópticos
* [[ComandoW3m|w3m]] - navegador de linha de comandos
* [[ComandoWhoami|whoami]] - quem sou eu?
* [[ComandoVim|vim]] - editor supercompleto e complicado para iniciantes
* [[ComandoXargs|xargs]] - construir listas de parâmetros ...
* [[ComandoXclip|xclip]] - manipula o clipboard
* [[ComandoXdebconfigurator|xdebconfigurator]] - detectar opções do monitor
* [[ComandoXflux|xflux]] - controla luminosidade do monitor baseado na sua localização geográfica
* [[ComandoXinit|xinit]] - inicia seção gráfica
* [[ComandoXkill|xkill]] - permite matar processos graficamente
* [[ComandoXmessage|xmessage]] - exibe pop-up tipo net send
* [[ComandoXrandr|xrandr]]  - mostra e configura resolução de tela
* [[ComandoXsel|xsel]] - manipula o clipboard
* [[ComandoZip|zip]] - compacta arquivos
* [[ComandoZsync|zsync]] - implementação para http do protocolo rsync
* [[ComandoYoutube-dl|youtube-dl]] - baixa vídeos do youtube
* [[ComandoYaourt|yaourt]] Gerenciador de pacotes para archlinux
* [[ComandoUnzip|unzip]] - descompacta arquivos zip
