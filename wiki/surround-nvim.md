---
file: surround-nvim.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [vim, nvim, plugins]
---

## neovim plugin to change surrounding chars like ({["
+ https://github.com/ur4ltz/surround.nvim
+ https://futurile.net/2016/03/19/vim-surround-plugin-tutorial/

Normal Mode - Sandwich Mode

    Provides key mapping to add surrounding characters.( visually select then press s<char> or press sa{motion}{char})
    Provides key mapping to replace surrounding characters.( sr<from><to> )
    Provides key mapping to delete surrounding characters.( sd<char> )
    ss repeats last surround command.

Normal Mode - Surround Mode

    Provides key mapping to add surrounding characters.( visually select then press s<char> or press ys{motion}{char})
    Provides key mapping to replace surrounding characters.( cs<from><to> )
    Provides key mapping to delete surrounding characters.( ds<char> )

#### Normal Mode - Surround Mode

1. Provides key mapping to add surrounding characters.( visually select then press `s<char>` or press `ys{motion}{char}`)
2. Provides key mapping to replace surrounding characters.( `cs<from><to>` )
3. Provides key mapping to delete surrounding characters.( `ds<char>` )

#### Insert Mode

- `<c-s><char>` will insert both pairs in insert mode.
- `<c-s><char><space>` will insert both pairs in insert mode with surrounding whitespace.
- `<c-s><char><c-s>` will insert both pairs on newlines insert mode.

    ys3w" ..................... surround 3 words using "
    ds[ ....................... delete surround
    cs"' ...................... change surround
    cq (surround mode) ........ toggle quotes

### IDK I was bored

1. Cycle surrounding quotes type. (`stq`)
1. Cycle surrounding brackets type. (`stb`)
1. Use `<char> == f` for adding, replacing, deleting functions.
