---
file: viml.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

# viml
Some usefull scripts

" How to know if I am on the last buffer
function! Bye()
     if len(filter(range(1, bufnr('$')), 'buflisted(v:val)')) == 1
         :echom "You are in the last buffer!"
     else
         :bdelete
    endif
endfunction
map <silent> <F8> :call Bye()<CR>
