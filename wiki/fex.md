---
File: /home/sergio/.dotfiles/wiki/fex.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [utils, text, tools]
 vim:ft=markdown
---

Usage Examples
Simple splitting

    With input text: /usr/local/bin/firefox
    ---------------------------------------
    Command     Result
    ---------------------------------------
    fex /1      “usr”
    fex /{2:3}  “local/bin”
    fex /{1,-1} “usr/firefox”
    fex /-1     “firefox”
    fex /{:}    “/usr/local/bin/firefox/
    fex /0      “/usr/local/bin/firefox/
