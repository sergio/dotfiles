---
file: android.md
author: Sergio Araujo
Last Change: Fri, 29 Mar 2024 - 06:17:42
tags: [android, phone, termux]
---

## Lineage OS for android A01:
+ https://forum.xda-developers.com/t/rom-r-gsi-unofficial-lineageos-18-1-for-a01-a015f.4340705/

## how to install grapheneOS:
+ [youtube watch?v=ro5OsVk8OfE](https://www.youtube.com/watch?v=ro5OsVk8OfE)
+ [youtube watch?v=yBBTkxcADbs](https://www.youtube.com/watch?v=yBBTkxcADbs)
+ [youtube watch?v=hg2lHTFjLHU](https://www.youtube.com/watch?v=hg2lHTFjLHU)

## double tap to lock and unlock screen:
+ [youtube watch?v=qwM_-sBkmFU](https://www.youtube.com/watch?v=qwM_-sBkmFU)

    settings > advanced features > motions and gestures > double tab to turn (on/off) screen

## How to access your android through ssh

Install sshelp fron google app store. In nautilus type

    ssh://admin:admin@192.168.1.3:2222

## Alternatives to the playstore:
+ https://auroraoss.com/

    aurora-store

## Activate developer options (opções de desenvolvedor)

    # it works on j1mini

    About device >> software info >> Build number (tap 5 times)

## How to download youtube videos on android (termux + youtube-dl)
+ [how-to-use-termux-to-download-youtube-videos](https://techwiser.com/how-to-use-termux-to-download-youtube-videos/)

Follow the steps

1. Go to play store and download the termux app. If you tried the previous tutorial on termux, then uninstall the termux app and reinstall it.

2. Next, open the termux app and copy paste the 4 commands in the same order. Just copy paste them it will run som commands to download the library.

        wget http://pastebin.com/raw/W1nvzN6q -O install.sh
        dos2unix install.sh
        chmod u+x install.sh
        ./install.sh

### termux shortcuts

    ctrl-p ................ show last command (then press ctrl-o)
    ctrl-r ................ reverse search command
    !! .................... repeat last command
    !ec ................... repeat last command starting with ec

## How to access my Android files using Wi-Fi in Ubuntu?
+ [askubuntu 626941](https://askubuntu.com/questions/626941/)

Both my Ubuntu and Android phone are using the same Wi-Fi. How can I transfer
files between the phone and the PC? Is there any way to mount the Android file
system? I don't want to use the USB cable. Bluetooth doesn't work. So WiFi is
the only other option.

My favourite application is SSHelper. It is free on google play store and well documented.

It creates a SSH server you can access using sftp. You mount the filesystem using Nautilus.

    http://arachnoid.com/android/SSHelper/

## Acessar pastas do android pela linha de comando
Montar o dispositivo via cabo usb, em seguida

    cd /run/user/1000/gvfs

    ls (para exibir o nome do dispositivo)

No meu caso apenas tive que colocar duas contrabarras no parâmetro

    cd mtp\:host\=SAMSUNG_SAMSUNG_Android_420010d501d0b400

## como saber a versão do android

We recommended to use firmware version from your own region.
To check phone version you can follow two way :

    Tap or Type *#1234#
    Go to Setting - About Phone - see Baseband version

## onde obter os rooms modificados

	http://www.cyanogenmod.org/

## Some tips about adb:
+ https://www.fosslinux.com/25170/how-to-install-and-setup-adb-tools-on-linux.htm
+ https://www.maketecheasier.com/common-adb-commands/

    adb reboot
    adb reboot recovery
    sudo adb reboot bootloader
    adb reboot fastboot

    sudo fastboot reboot

## Espelhar o android no linux

    adb shell screenrecord --output-format=h264 - | ffplay -

## Install/Uninstall Apps

Besides from moving files back and forth, you can actually install apk files
with just a single command. To install an app you have to specify the full
path of the apk file. So, replace "path/to/file.apk" with the actual apk file
path.

    adb install "path/to/file.apk"

If you have multiple devices attached to your computer and only want to
install the apk file on just one device then use the below command. Replace
[serial-number] with the actual device serial number. You can get the device
serial number using the fourth command above.

## Copy Files from Computer to Phone
+ https://techwiser.com/list-adb-commands/

If you want to copy files from your computer to phone using adb then you can
use this command. Do forget to replace [source] and [destination] with actual
file paths.

    adb push [source] [destination]

Once you replace the above command with actual file paths, this is how it
looks like.

    adb push "E:\Video Songs\Aankhon Mein Teri - Om Shanti Om.mp4"
    "/sdcard/Downloads/video.mp4"

## Copy Files from Phone to Computer

Just like you can copy files from your computer to Android device, you can
copy files from your phone to computer. To do that simply use the below
command. Replace [source] and [destination] with actual file paths.

adb pull [source] [destination]

Once you replace the above command with actual file paths, this is how it
looks like.

adb pull "/sdcard/Downloads/video.mp4" D:\Downloads

## android tools
+ https://developer.android.com/studio/command-line/adb?hl=pt-br

Android Debug Bridge (adb)

Android Debug Bridge (adb) is a versatile command-line tool that lets you communicate with a device. The adb command facilitates a variety of device actions, such as installing and debugging apps, and it provides access to a Unix shell that you can use to run a variety of commands on a device. It is a client-server program that includes three components:

    A client, which sends commands. The client runs on your development machine. You can invoke a client from a command-line terminal by issuing an adb command.
    A daemon (adbd), which runs commands on a device. The daemon runs as a background process on each device.
    A server, which manages communication between the client and the daemon. The server runs as a background process on your development machine.

    # archlinux
    yaourt -S android-tools

    # voidlinux
    sudo xbps-install -S android-tools

Listar os dispositivos conectados via usb

Try to restart the Adb server.
+ https://askubuntu.com/a/909227/3798

    sudo adb kill-server

and then

    sudo adb start-server

then connect your device turn Debugging on and type

    adb devices

NOTE: Also removing the ~/.android folder and restarting the server worked fine!

## Reiniciar com o fastboot (veja no script de instalação) (desbloqueio do bootloader)
+ https://www.youtube.com/watch?v=9Ujuotwv1xA

    sudo adb reboot bootloader
    sudo adb start-server
    sudo adb sideload lineage-16.0-20190609-UNOFFICIAL-serranoltexx.zip
    sudo adb sideload open_gapps-arm-9.0-mini-20190706.zip

    adb kill-server; adb start-server
    sudo adb sideload lineage-14.1-20170315-UNOFFICIAL-serranoveltexx.zip
    sudo adb sideload open_gapps-arm-7.1-mini-20190706.zip

OBS: À partir desse ponto usa-se o comando `fastboot`

## reiniciar o aparelho

    sudo fastboot reboot

No motoG 8 power lite podemos desligar e ligar pressionando power+volume_up

Para pegar o código que será enviado para o site da motorola
neste link: https://motorola-global-portal.custhelp.com/app/standalone/bootloader/unlock-your-device-b

    sudo fastboot oem get_unlock_data
      ...
    (bootloader) 3A55940646931947#00000000000000
    (bootloader) 00000000585431303639000000#47B4
    (bootloader) F5FFC71EDAD55334BFDF26B46541C2F
    (bootloader) C2795#84C187060F000000000000000
    (bootloader) 0000000

Obs: O código dever ser limpo tirando (bootloader)  e espaços

Daí a motorola manda um código para o seu e-mail
e o comando final ficar mais ou menos assim:

    sudo fastboot oem unlock PT4SFSVXCDCN4HLKVUYD

No comando acima colei o meu código

## Desinstalando aplicativos pelo adb: (bloatware)
+ https://www.youtube.com/watch?v=0P9aMhaUCR8
+ https://darpan.blog/code/guide-remove-bixby-bloatware-from-samsung-galaxy-phones/
+ https://r1.community.samsung.com/t5/others/how-to-remove-samsung-bloatware-without-root/td-p/5817510
+ https://www.minitool.com/news/list-of-samsung-bloatware-safe-to-remove.html

+ Revogue e habilite novamente a depuração USB
+ pare e reinicie o adb server:  sudo adb kill-server ; sudo adb start-server
+ entre no adb shell: sudo adb shell
+ para listar os pacotes:  pm list packages | grep 'google'
+ para remover:  pm uninstall -k --user 0 com.facebook.appmanager

    OBS: remova "package:" no início dos nomes na hora de digitar o comando de desinstalar

    pm list packages | grep 'com.samsung.android'

    pm list packages | grep "bixby"

    adb shell pm uninstall -k --user 0 com.samsung.android.themestore
    adb shell pm uninstall -k --user 0 com.samsung.android.aremoji
    adb shell pm uninstall -k --user 0 com.sec.android.app.samsungapps
    adb shell pm uninstall -k --user 0 com.samsung.android.game.gamehome
    adb shell pm uninstall -k --user 0 com.samsung.android.scloud
    adb shell pm uninstall -k --user 0 com.samsung.android.emojiupdater
    adb shell pm uninstall -k --user 0 com.samsung.android.smartfitting
    adb shell pm uninstall -k --user 0 com.samsung.android.game.gametools
    adb shell pm uninstall -k --user 0 com.enhance.gameservice
    adb shell pm uninstall -k --user 0 com.samsung.android.game.gos
    #adb shell pm uninstall -k --user 0 com.monotype.android.font.foundation
    adb shell pm uninstall -k --user 0 com.sec.android.app.billing
    adb shell pm uninstall -k --user 0 com.samsung.android.wellbeing
    adb shell pm uninstall -k --user 0 com.sec.spp.push
    adb shell pm uninstall -k --user 0 com.samsung.android.keyguardwallpaperupdator
    adb shell pm uninstall -k --user 0 com.samsung.android.app.watchmanagerstub
    adb shell pm uninstall -k --user 0 com.samsung.android.fmm
    adb shell pm uninstall -k --user 0 com.samsung.android.beaconmanager
    adb shell pm uninstall -k --user 0 com.sec.android.diagmonagent
    adb shell pm uninstall -k --user 0 com.samsung.android.smartmirroring
    adb shell pm uninstall -k --user 0 com.samsung.android.kidsinstaller
    adb shell pm uninstall -k --user 0 com.samsung.android.app.omcagent
    adb shell pm uninstall -k --user 0 flipboard.boxer.app
    adb shell pm uninstall -k --user 0 com.samsung.android.app.reminder
    #adb shell pm uninstall -k --user 0 com.diotek.sec.lookup.dictionary
    adb shell pm uninstall -k --user 0 com.samsung.android.aircommandmanager
    adb shell pm uninstall -k --user 0 com.samsung.knox.securefolder
    adb shell pm uninstall -k --user 0 com.samsung.android.app.routines
    adb shell pm uninstall -k --user 0 com.samsung.android.mdecservice
    adb shell pm uninstall -k --user 0 com.samsung.android.allshare.service.fileshare
    adb shell pm uninstall -k --user 0 com.samsung.android.stickercenter
    adb shell pm uninstall -k --user 0 com.samsung.android.mateagent
    adb shell pm uninstall -k --user 0 com.samsung.android.allshare.service.mediashare
    adb shell pm uninstall -k --user 0 com.sec.android.easyMover.Agent
    adb shell pm uninstall -k --user 0 com.samsung.android.app.social
    adb shell pm uninstall -k --user 0 com.samsung.android.drivelink.stub

    adb shell pm uninstall -k --user 0 com.google.ar.core
    adb shell pm uninstall -k --user 0 com.google.android.partnersetup
    adb shell pm uninstall -k --user 0 com.android.wallpaperbackup
    #adb shell pm uninstall -k --user 0 com.android.printspooler
    adb shell pm uninstall -k --user 0 com.android.wallpaper.livepicker
    adb shell pm uninstall -k --user 0 com.android.calllogbackup
    adb shell pm uninstall -k --user 0 com.google.android.tts
    adb shell pm uninstall -k --user 0 com.google.android.feedback
    #adb shell pm uninstall -k --user 0 com.google.android.printservice.recommendation
    adb shell pm uninstall -k --user 0 com.google.android.googlequicksearchbox
    adb shell pm uninstall -k --user 0 com.google.android.setupwizard
    adb shell pm uninstall -k --user 0 com.android.bips
    #adb shell pm uninstall -k --user 0 com.android.providers.partnerbookmarks
    #adb shell pm uninstall -k --user 0 com.android.bookmarkprovider
    adb shell pm uninstall -k --user 0 com.google.android.music
    adb shell pm uninstall -k --user 0 com.google.android.videos
    adb shell pm uninstall -k --user 0 com.google.android.apps.photos

    adb shell pm uninstall -k --user 0 com.sec.android.easyonehand
    adb shell pm uninstall -k --user 0 com.linkedin.android
    adb shell pm uninstall -k --user 0 com.mygalaxy
    adb shell pm uninstall -k --user 0 com.eterno
    adb shell pm uninstall -k --user 0 com.opera.max.oem
    adb shell pm uninstall -k --user 0 com.opera.max.preinstall
    adb shell pm uninstall -k --user 0 com.osp.app.signin
    adb shell pm uninstall -k --user 0 com.aura.oobe.samsung
    adb shell pm uninstall -k --user 0 com.samsung.logwriter
    adb shell pm uninstall -k --user 0 com.samsung.android.easysetup
    adb shell pm uninstall -k --user 0 com.samsung.android.sdk.handwriting
    adb shell pm uninstall -k --user 0 com.samsung.storyservice
    adb shell pm uninstall -k --user 0 com.samsung.android.airtel.stubapp
    adb shell pm uninstall -k --user 0 com.samsung.android.smartswitchassistant
    adb shell pm uninstall -k --user 0 com.sec.android.daemonapp
    adb shell pm uninstall -k --user 0 com.samsung.android.spaymini
    adb shell pm uninstall -k --user 0 com.samsung.android.app.galaxyfinder
    adb shell pm uninstall -k --user 0 com.samsung.android.app.mirrorlink
    adb shell pm uninstall -k --user 0 com.samsung.android.app.simplesharing
    adb shell pm uninstall -k --user 0 com.sec.android.widgetapp.samsungapps
    adb shell pm uninstall -k --user 0 com.sec.android.widgetapp.webmanual
    adb shell pm uninstall -k --user 0 com.samsung.visionprovider
    adb shell pm uninstall -k --user 0 com.samsung.SMT
    adb shell pm uninstall -k --user 0 com.samsung.android.mfi
    adb shell pm uninstall -k --user 0 com.google.android.apps.tachyon
    adb shell pm uninstall -k --user 0 com.sec.android.daemonapp
    adb shell pm uninstall -k --user 0 com.sec.android.app.chromecustomizations

    adb shell pm uninstall -k --user 0 com.microsoft.skydrive
    adb shell pm uninstall -k --user 0 com.microsoft.office.officehubrow

    #adb shell pm uninstall -k --user 0 com.sec.android.app.fm

    #adb shell pm uninstall -k --user 0 com.dsi.ant.server
    #adb shell pm uninstall -k --user 0 com.dsi.ant.service.socket
    #adb shell pm uninstall -k --user 0 com.dsi.ant.sample.acquirechannels
    #adb shell pm uninstall -k --user 0 com.dsi.ant.plugins.antplus

    adb shell pm uninstall -k --user 0 com.samsung.android.samsungpass
    adb shell pm uninstall -k --user 0 com.samsung.android.samsungpassautofill
    adb shell pm uninstall -k --user 0 com.samsung.android.authfw
    adb shell pm uninstall -k --user 0 com.samsung.android.tapack.authfw
    #adb shell pm uninstall -k --user 0 com.samsung.android.mobileservice

    adb shell pm uninstall -k --user 0 com.facebook.services
    adb shell pm uninstall -k --user 0 com.facebook.system
    adb shell pm uninstall -k --user 0 com.facebook.appmanager
    adb shell pm uninstall -k --user 0 com.facebook.katana

    adb shell pm uninstall -k --user 0 com.samsung.android.app.settings.bixby
    adb shell pm uninstall -k --user 0 com.samsung.android.bixby.service
    adb shell pm uninstall -k --user 0 com.samsung.android.bixby.agent
    adb shell pm uninstall -k --user 0 com.samsung.android.bixbyvision.framework
    adb shell pm uninstall -k --user 0 com.samsung.android.bixby.wakeup
    adb shell pm uninstall -k --user 0 com.samsung.android.app.spage
    adb shell pm uninstall -k --user 0 com.samsung.android.bixby.agent.dummy
    adb shell pm uninstall -k --user 0 com.samsung.android.visionintelligence
    adb shell pm uninstall -k --user 0 com.samsung.systemui.bixby2

    adb shell pm uninstall -k --user 0 com.samsung.android.knox.analytics.uploader
    adb shell pm uninstall -k --user 0 com.samsung.android.bbc.bbcagent
    adb shell pm uninstall -k --user 0 com.samsung.android.knox.containeragent

    #adb shell pm uninstall -k --user 0 com.samsung.android.contacts
    #adb shell pm uninstall -k --user 0 com.samsung.android.messaging
    #adb shell pm uninstall -k --user 0 com.sec.android.inputmethod
    #adb shell pm uninstall -k --user 0 com.samsung.android.calendar
    #adb shell pm uninstall -k --user 0 com.sec.android.app.clockpackage
    adb shell pm uninstall -k --user 0 com.android.chrome

    adb shell pm uninstall -k --user 0 com.sec.android.app.launcher

## instalando o lineageos from recovery
Installing a custom recovery using heimdall
+ https://wiki.lineageos.org/devices/serranoltexx/install
+ https://youtu.be/A-1jAv56L1c

Samsung Galaxy S4 mini Plus LTE 64 bit GT-I9195I Stock Rom
Gapps atuais (ARM, não ARM64!)
lineage-14.1-20170315-UNOFFICIAL-serranoveltexx.zip
https://www.android-hilfe.de/forum/samsung-galaxy-s4-mini-ve-i9195i.2457/rom-gt-i9195i-7-1-lineage-14-1-unofficial.823385.html

+ https://opengapps.org/

Instalando o recovery "twrp"

    sudo heimdall flash --RECOVERY recovery.img --no-reboot

## how to fix: kernel is not seandroid enforcing

## cinogenmod alternative lineageos

## Odin alternative for linux heimdall
+ https://www.youtube.com/watch?v=Jv538rOL5p0
+ https://spoonjab.com/how-to-use-heimdall-to-flash-roms-modems/
+ https://www.androidpit.com/how-to-get-android-6-marshmallow-on-samsung-galaxy-s4
+ https://www.reddit.com/r/S7Edge/comments/79g2ie/how_to_flash_a_recovery_image_using_heimdall_on/

Device detection in Heimdall:

* First off, go in Download mode: With the Samsung Galaxy TAB turned off,
  press and hold the VOLUME Down button, and then briefly press the POWER
  button.

* Plug in connection cable to your device and USB.

* Only after that Heimdall will detect your device, if it's detectable :)

Install heimdall and then run:

    heimdall-frontend

    sudo heimdall detect
    sudo heimdall print-pit

------------------------------------------
I’m going to give you a quick step-by-step guide.

Using the GUI:

    Download and install Heimdall.

It’s open-source (as i already mentioned) and supports Windows, Mac, GNU/Linux (including Ubuntu and Fedora) and a few other operating systems, as opposed to ODIN which is proprietary and supports only Windows.

2. Reboot your phone in download (or ODIN) mode  (usually by holding volume down, home and power and by following the on-screen directions) and connect it to the computer.

Wait for the device drivers to be loaded.

3. Run Heimdall as administrator/root (sudo heimdall-frontend).

4. If it’s the first time you flash something to your phone using Heimdall, follow the following steps toobtain a PIT file; if you already have a PIT, skip them.

    A. Go to the Utilities tab.

    B. Press Detect to make sure the phone has been recognised.

    C. Click Save as under Download PIT and select a place to store the new file.

    D. Click Download.

    E. When it’s done, turn off your phone and Boot back to download mode.

5. Go to the Flash tab and Browse for the PIT you just downloaded.

6. Click Add under Partitions (Files).

7. In the Partition Name menu select RECOVERY or your phone’s equivalent (names are usually pretty clear: BOOTis usually the kernel (never touch ABOOT though unless you know what you’re doing), SYSTEM is /system, USERDATA is /data, etc.).

8. Click Browse under File and open the recovery image you downloaded. It should be in “.img” format.

9. Repeat steps 6 to 8 for any other partitions you need to flash, if any. If you’re done, click Start to start flashing

10. Enjoy!!!

## Flash 'TWRP' as the custom recovery image

    sudo fastboot flash recovery ~/Downloads/openrecovery-twrp-2.8.2.0-hammerhead.img
    sudo fastboot flash recovery twrp-2.8.6.0-titan.img

    boot in recover mode

## How to fix missing "OEM unlocking"
OBS: in older android versions ‘OEM unlocking’ option only becomes available
after 7 days of activating the device and adding a Samsung or Google account
to the device.
+ [https://www.xda-developers.com](https://www.xda-developers.com/fix-missing-oem-unlock-samsung-galaxy-s9-samsung-galaxy-s8-samsung-galaxy-note-8/)

## How block fastbook again

source: hdttp://forum.cyanogenmod.org/topic/64232-security-benefit-to-fastboot-oem-lock-after-rom-installation/

Initially, I thought to myself, "Oh, I guess this won't work." However, I
noticed at least one guide I found on "fastboot oem lock" specified getting
into bootloader via ADB, so I tried it again with the following commands:

    $ adb reboot bootloader
    $ fastboot oem lock
    ...
    (bootloader) Bootloader is locked now.
    OKAY [  1.644s]
    finished. total time: 1.644s

## Resolution
+ https://support.honeywellaidc.com/s/article/How-to-fix-adb-devices-shows-unauthorized-device

Possible solutions if "adb devices" shows a device as "unauthorized":

    adb devices
    List of devices attached
    17310D821D      unauthorized

    1 - Disconnect USB between PC and device
    2 - Stop adb server by entering "adb kill-server" in command window
    3 - On device use "Revoke USB debugging authorizations" in "Developer Options"
    4 - On PC delete "adbkey" file in user directory, for example "C:\Users\JohnDoo\.android"
    5 - Reconnect the device to the PC
    6 - Open a command window and enter "adb devices". Watch the device's screen for any Authorization message and allow the connection.

The device should now show as "device":

    C:\Users\JohnDoo>adb devices
    List of devices attached
    17310D821D      device

# vim:ft=markdown:et:sw=4:ts=4:cole=0:

