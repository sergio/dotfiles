---
file: tidy.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---



 =Usando o tidy dentro do vim=

  :!tidy -mi -css %

## Adding formatting to an xml document for easier reading

  tidy -i -xml <inputfile>

