---
File: ~/.dotfiles/wiki/xinput.md
Last Change: Mon, 19 Feb 2024 - 20:34:03
tags: [x11, xorg, touchpad, utils, tools]
---

# Intro

xinput -  Utility to configure and test X input devices

+ [xvkbd tutorial](http://xahlee.info/linux/linux_xvkbd_tutorial.html)
+ [libinput is a library to handle input devices](https://wiki.archlinux.org/title/Libinput)

In terminal, type xinput --list to list devices.

    xinput --list | grep 'id='

## No archlinux

    xorg-xinput
