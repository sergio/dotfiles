---
file: ex.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

# ex command

ex is a text editor, and the line-editor mode of vi. It is the basis of vim,
one of the most popular text editors in the world. ex was written by Bill
Joy in 1976, based on an earlier program written by Charles Haley.

"ex" stands for extended, because it was originally an extension of the simple
line editor ed. Similarly, "vi" stands for visual, because vi is the "visual"
(full-screen) editing mode, which was eventually added to ex.

OBS: neovim does not have `ex` editor but it brings `-c` option

## delete the second column with ex command

	ex +':exe ":%norm f\<Tab>dE"' -scx file

## delete until next tab
+ https://vi.stackexchange.com/a/5139/7339

	vim -c':exe ":%norm df\<Tab>"' -c ':wq' file

