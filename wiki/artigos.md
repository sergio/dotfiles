---
file: ~/.dotfiles/wiki/artigos.md
author: Sergio Araujo
Last Change: Mon, 08 Jul 2024 - 05:12:56
---

# Artigos

+ [apropagandaideologicadopig](apropagandaideologicadopig.md)
+ [vidaaposavida](vidaaposavida.md)
+ [oparadoxopython](oparadoxopython.md)
+ [pythoneocaminhodomeio](pythoneocaminhodomeio.md)
+ [dicatedtalks](dicatedtalks.md)
+ [thefreesoftwareecosystem](thefreesoftwareecosystem.md)
+ [howtomakequestionsinenglish](howtomakequestionsinenglish.md)
+ [aindustriapornografica](aindustriapornografica.md)
+ [howlearnlanguagesfast](howlearnlanguagesfast)
+ [resposta ao sergio reis](resposta-ao-sergio-reis.md)
+ [O Bom Samaritano](O-Bom-Samaritano.md)
+ [Palavras de multiplo sentido](Palavras-de-multiplo-sentido.md)
+ [milk nao e somente leite](milk-nao-e-somente-leite)
+ [useless use of cat](useless-use-of-cat)
+ [furando a bolha com rss](https://dev.to/computandoarte/utilizando-o-rss-para-furar-a-bolha-54ai)
+ [the shell power](https://dev.to/voyeg3r/the-shell-real-power-1a5j)
+ [O apagão da Internet](apagao.md)
+ [how neovim saved my marriage](nvim-marriage.md)
+ [The the real motivation behind systemd](https://unixdigest.com/articles/the-real-motivation-behind-systemd.html)

## Articles about vim

+ [introducao ao vim](introducao-ao-vim)
+ [how-to-copy-paste-on-vim](how-to-copy-paste-on-vim.md)
+ [youdontgrokvi](youdontgrokvi.md)
