---
file: kill.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# kill

    Last Change: jun 2018

## Para fechar um programa usando o comando kill fazemos assim:

    ps aux | grep firefox

O comando acima retornará (filtrando através do [[grep]]) os processos, ou seja
programas que tenham o nome firefox, pegue o número fornecido e use como abaixo

    kill -9  1945

Um modo mais prático é usar o comando [[pkill]]

    pkill firefox

ou

    pkill firefox-bin

## matando o processo do mpd

    kill -9 $(ps aux | awk '/mpd/ && !/grep/ {print $2}')

## Matando todos os processos de um usuário específico
fonte: http://www.ataliba.eti.br/node/1771

    su - username -c 'kill -9 -1'

O comando "su - username" toma a identidade do usuário indicado
a opção "-c" executa um comando "kill -9 -1" mata todos os processos possíveis

## If you just want to pause it and restart afterwards ...
  ... you can use kill with STOP or CONT signal.

+ https://unix.stackexchange.com/a/116064/3157

At first find out the processes PID with

    ps aux

Then send the signals to that PID listed to the process

    kill -STOP <PID>

    kill -CONT <PID>

 <!-- vim:ft=markdown:et:sw=4:ts=4:cole=0: -->
