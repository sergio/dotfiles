---
file: touch.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
 vim:ft=markdown
tags: [tools, admin, date]
---

# The touch command
It is used to create files and change file timestamps

    touch file-{1..20}.txt

## Alterando o timestamp usando um arquivo como referência:

Para alterar as datas de modificação e de acesso do arquivo teste para 12:31 do dia 01/05 do ano corrente, digite

    touch -t 05011231 teste

    ttime="$(date -r file.md  "+%m%d%H%M")"
    touch -t "$ttime" file.md

