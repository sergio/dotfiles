---
file: scapy.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# scapy (python lib)

Scapy is a packet manipulation tool for computer networks, written in Python
by Philippe Biondi. It can forge or decode packets, send them on the wire,
capture them, and match requests and replies. It can also handle tasks like
scanning, tracerouting, probing, unit tests, attacks, and network discovery.
