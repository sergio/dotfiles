---
file: vim-plug.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---


O plugin vim-plug cria uma pasta única onde se colocam
as pastas dos plugins por inteiro.

Esta era a aboradagem tradicional, nela cada plugin instala
um arquivo em plugin, outro em doc, outro em autoload e assim por diante

     ~/.vim
        |
        +--plugin
        |
        +--doc
        |
        +--autoload

Com o plugin vim-plug cada plugin deve ser
instalado inteiramente na pasta plugged, a única
excessão é o próprio plugin vim-plug que coloca
também um arquivo em autoload, direto na pasta .vim

     ~/.vim
        |
        +--plugged
              |
              +--plugin
              |
              +--doc
              |
              +--autoload

tags: vim, nvim
