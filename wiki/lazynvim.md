Filename: lazynvim.md
Last Change: Sat, 16 Nov 2024 - 19:49:03
tags: [nvim, plugins, plugin manager]

## Intro:
+ https://www.youtube.com/watch?v=N93cTbtLCIM
+ https://github.com/folke/lazy.nvim
+ https://www.lazyvim.org/

## Let's get started:

  mv ~/.config/nvim ~/.config/nvim.bak

  mv ~/.local/share/nvim ~/.local/share/nvim.bak
  mv ~/.local/state/nvim ~/.local/state/nvim.bak
  mv ~/.cache/nvim ~/.cache/nvim.bak

Clone the starter

  git clone https://github.com/LazyVim/starter ~/.config/nvim

Remove the .git folder, so you can add it to your own repo later

  rm -rf ~/.config/nvim/.git

Start Neovim!

  nvim --headless "+Lazy! sync" +qa

## Modularize lazy

```lua
local specs_folders = {
  { import = "user.plugins.core" },
  { import = "user.plugins.ui" },
  -- etc.
}
require("lazy").setup(specs_folders, opts)
```




