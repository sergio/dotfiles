---
File: /home/sergio/.dotfiles/wiki/awesome.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [windowmanager, desktop]
---

##  awesome window manager
+ https://awesomewm.org/apidoc/documentation/07-my-first-awesome.md.html#
+ https://www.youtube.com/watch?v=qKtit_B7Keo

    cp /etc/xdg/awesome/rc.lua ${HOME}/.config/awesome

In my case:

    ln -sfvn ~/.dotfiles/awesome ~/.config

## Scratchpad terminal:
+ https://www.youtube.com/watch?v=m6zocJ5tKxw
+ https://github.com/notnew/awesome-scratch

```lua
--TOP OF FILE
local scratch = require("awesome-scratch.scratch")
screen_width = awful.screen.focused().geometry.width
screen_height = awful.screen.focused().geometry.height

--KEYBIND
  -- open scratchpad
awful.key({ modkey }, "d", function ()
    scratch.toggle("st -n scratch", { instance = "scratch" })
  end,
    { description = "Open scratchpad terminal" , group = "sergio" }
  ),

-- RULE
    {
        rule_any = {
            instance = { "scratch" },
            class = { "scratch" },
            icon_name = { "scratchpad_urxvt" },
        },
        properties = {
            skip_taskbar = false,
            floating = true,
            ontop = false,
            minimized = true,
            sticky = true,
            width = screen_width * 0.7,
            height = screen_height * 0.75
        },
        callback = function (c)
            awful.placement.centered(c,{honor_padding = true, honor_workarea=true})
            gears.timer.delayed_call(function()
                c.urgent = false
            end)
        end
    },
```

## Lua browser:

    doas xbps-install -Sy luakit

## changing your terminal:

    ~/.config/awesome/rc.lua

Search for: terminal

    terminal = '/bin/st -g 90x20 -f "Iosevka Mayukai Original:size=15:antialias=true:autohint=true"'

change terminal like this:

```lua
terminal = "st" or "xterm"
```

## Remove title bars:
+ https://stackoverflow.com/a/42728195/2571881

Change `titlebars_enabled = true` to `false`

```lua
-- Add titlebars to normal clients and dialogs
{ rule_any = {type = { "normal", "dialog" }
    }, properties = { titlebars_enabled = false }
},
```

## change wallpaper

    cp /usr/share/awesome/themes/default/theme.lua ~/.config/awesome
    now edit: vim ~/.config/awesome/theme.lua

## shortcuts

    super + r .............. run (dmenu in my case)
    Ctrl + super + r ....... reload awesome
    super + right arrow .... next desktop
    super + s .............. show keybindings
    super + j .............. focus next by index
    super + Shift + j ...... swap with next client by index
    super + Shift + h ...... horizontal layout
    super + Shift + l ...... vertical layout

## useful snippets

I think maybe I will change this to Ctrl + Alt + j

```lua
-- Non-empty tag browsing CTRL+ALT j/k --> local lain = require("lain")
awful.key({ modkey, "Control" }, "j", function () lain.util.tag_view_nonempty(-1) end,
            {description = "view  previous nonempty", group = "tag"}),
awful.key({ modkey, "Control" }, "k", function () lain.util.tag_view_nonempty(1) end,
            {description = "view  previous nonempty", group = "tag"}),
```

### Setting shortcuts:

    awful.key({modkey, }, "b", awful.spawn("firefox"))

## layouts plugin:
+ https://github.com/lcpz/lain

## changing themes:
+ https://fontawesome.com/versions
+ https://github.com/lcpz/awesome-freedesktop

    lxappearance qt5ct numlockx font-awesome6 slock volumeicon

## Tools:

    doas xbps-install -Sy galculator-gtk3 pulsemixer

