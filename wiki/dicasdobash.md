---
File: ~/.dotfiles/wiki/dicasdobash.md
Last Change: Thu, 01 Aug 2024 - 13:56:09
tags: [bash, shell]
---

# Dicas do bash

Dicas do bash

## Economize tempo ao buscar erros em shell

- [Check your shell script here](https://www.shellcheck.net/)
- [Como detectar precisamente onde seu script está quebrando](https://meleu.sh/trap-err/#n%c3%a3o-quero-ler-tudo-isso-me-diz-logo-o-que-tenho-que-fazer)

``` sh
set -Eeuo pipefail
trap 'echo "${BASH_SOURCE}:${LINENO}:${FUNCNAME:-}"' ERR
```

## Dica sobre dotfiles

[Dica do meleu](https://meleu.sh/dotfiles/)

## Expansão de parâmetros:

Contar caracteres:

```sh
cad=onça
echo ${#cad}

: ${#cad}
```

OBS: o dois pontos é um comando que não faz nada

## Declarando arrays em shell script:

``` sh
declare -A
modified=$(git diff --name-only)

for f in "${modified[@]}"; do
    echo "file: $f"
done
```

## Tell what terminal I am using:

``` sh
ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)
```

## calcular o fatorial em bash:

```sh
seq -s* 6 | bc
```

## How can I check if a program exists from a Bash script?

+ [Check program existence](https://stackoverflow.com/a/677212/2571881)

```sh
if ! command -v <the_command> &> /dev/null
then
    echo "<the_command> could not be found"
    exit
fi
```

no zshell dá pra fazer:

``` sh
(( ! $+commands[xclip] && $+commands[termux-clipboard-get] )) && {
  alias pbpaste='termux-clipboard-get'
  alias pbcopy='termux-clipboard-set'
}
```

## bash language server LSP

``` sh
npm i --location=global bash-language-server
```

## [show color in the terminal](https://www.unixtutorial.org/how-to-show-colour-numbers-in-unix-terminal)

```bash
#!/bin/bash
for COLOR in {1..255}; do
    echo -en "\e[38;5;${COLOR}m"
    echo -n "${COLOR} "
done
```

## here-document <<-EOF, here strigs <<<$

Instead of creating a document we can create a here-document (on demand)

```bash
grep -P '(?<!big)\s\w+' <<-EOF
big house
small house
big car
small car
EOF
```

With here strings we can do:

``` sh
grep pattern <<<$var
```

## Put the output of a here document command to a variable:

``` sh
foo=$(sed 's/-/_/g' << EOF
    1-2
    3-4
    EOF
    )
```

An example of a here-string

```bash
wc -w <<< "This is a test."
```

##  `!*` Tells that you want all of the *arguments* from the previous command to be repeated in the current command
+ https://www.commandlinefu.com/commands/view/24448/

``` bash
touch file{1,2,3}; chmod 777 !*
```

If you forget to use `-r` run this:

``` sh
rm -r !!:*
!! -r
```

Substitute google from yahoo

``` sh
^google^yahoo
```

## Get the pid of the last executed command

    echo $!

## Being more concise
Avoid cat when possible

    wc -l /etc/passwd

tr and many other commands accept files

    tr X Y < FILE

## mapfile - create a vector without any loop
+ http://www.dicas-l.com.br/cantinhodoshell/cantinhodoshell_20100121.php

``` sh
cat frutas
abacate
maçã
morango
pera
tangerina
uva

mapfile vet < frutas  # Mandando frutas para vetor vet
echo ${vet[@]}        # Listando todos elementos de vet
abacate maçã morango pera tangerina uva
```

## bash onliners

	http://www.bashoneliners.com/?page=1

## Transpose words

	Ctrl-t

## Substituição de processos - process substitution

+ [substituicao-de-processos-no-bash](https://giovannireisnunes.wordpress.com/2017/10/20/substituicao-de-processos-no-bash/)

    `vim <( lspci )`

Utilizando a substituição de processos para baixar um arquivo via wget e ao mesmo tempo compactá-lo com o gzip.

``` sh
    wget http://ftp.ubuntu.com/ubuntu/dists/yakkety/Release --output-document=>( gzip -c -9 > Release.gz )

    export host='http://ftp.ubuntu.com/ubuntu/dists';\
    vimdiff <( curl ${host}/yakkety/Release ) \
    <( curl ${host}/zesty/Release )
```

O interessante deste exemplo, além de dispensar a criação de arquivos temporários, está no fato de que os arquivos são baixados simultaneamente
já que cada curl roda em uma subshell.

Outra situação comum é quado precisamos consultar certa manpage em busca de um padrão, por exemplo; se queremos saber como impor um limite de download ao
wget podemos consultar a manpage assim:

    grep limit <(man wget)

## sum array

+ [article](https://dev.to/proteusiq/to-code-and-beyond-a-neverland-adventure-in-bash-lua-python-and-rust-1jon)

try using awk

``` sh
seq 9 | awk '{($1 % 2 ? sum+=$1 : 0)} END { print sum}'

seq -s+ 1 2 9 | bc

separate odd and even numbers
awk '{ print > (NR % 2 ? "odd.txt" : "even.txt") }' a.txt
```

## calculadora na linha de comando

    echo $[31*2]

## Writing your first simple bash completion code

+ https://www.cyberciti.biz/faq/add-bash-auto-completion-in-ubuntu-linux/
+ https://iridakos.com/tutorials/2018/03/01/bash-programmable-completion-tutorial.html
+ https://github.com/iridakos/bash-completion-tutorial

Say you always want to check an IP address of three domain
names using the host command. So type the following at the shell prompt:

    complete -W 'google.com cyberciti.biz nixcraft.com' host

Now type host and press the [TAB] key to use any one of the
domain name for host command:

    host [TAB]

## How display all possible commands avaiable

source: forun ubuntu https://goo.gl/cJDkp4

``` sh
whatis `compgen -c` | less

This will list all commands and a simple description of each command.

If you want to save the list you can redirect the result into an output file

whatis `compgen -c` > listOfCommands.txt
```

source (stack overflow): https://goo.gl/PYqI2n

    compgen -c will list all the commands you could run.
    compgen -a will list all the aliases you could run.
    compgen -b will list all the built-ins you could run.
    compgen -k will list all the keywords you could run.
    compgen -A function will list all the functions you could run.
    compgen -A function -abck will list all the above in one go.

## show function definition

``` sh
type foobar

# to se a function definition
declare -f foobar
```

## how to Renaming files in a folder to sequential numbers
+ source: http://stackoverflow.com/questions/3211595/

``` sh
a=1
for i in *.jpg; do
    new=$(printf "%04d.jpg" "$a") #04 pad to length of 4
    mv -- "$i" "$new"
    let a=a+1
done
```

another option in one line:

    count='1'; for i in *.jpg; do mv $i $(printf '%04d'.jpg $count); (( count++ )); done

## Generating a range of numbers

``` sh
echo {1..10}
```

## how get pipestatus

How to get exit status of process that's piped to another

The syntax is:

``` sh
command1 | command2
echo "${PIPESTATUS[@]}"

OR

command1 | command2
echo "${PIPESTATUS[0]} ${PIPESTATUS[1]}"
```

## catching file name and file extensions

``` sh
filename=$(basename "$fullfile")
extension="${filename##*.}"
filename="${filename%.*}"
```

## Inserting DOS carriage return

``` sh
simple press Ctrl-v Ctrl-m
```

## Bash: Read a file into a variable

``` sh
value=$(<config.txt)
```

## pegando os links mp3 de um html

``` sh
grep -o 'http://[^"]*'  < test.html  | grep mp3
```

## como criar um array no bash

``` sh
var=($(uname -a))
echo ${var[0]}
```

##  imprimindo os últimos três elementos de um array

``` sh
echo "${SERVERS[@]:(-3)}"
```

## Abrindo a pasta atual no gerenciador de arquivos

``` sh
xdg-open .
```

## Dicas do Júlio César Neves

``` sh
read -p "Deseja continuar (s/n)? "

[[ ${REPLY^} == N ]] && exit

## Para capturar qualquer tecla, tipo "pressione qualquer tecla pra continuar faça:"

    read -n 1 -s -p "Press any key to continue"
```

``` sh
function ask()
{
    echo -n "$@" '[y/n] ' ; read ans
    case "$ans" in
        y*|Y*) return 0 ;;
        *) return 1 ;;
    esac
}
```

Veja também InputRc

Para acessar rapidamente um comando recem digitado basta
pressionar Ctrl-r e digitar parte do comando, o bash achará
o comando, basta então pressionar enter.

```
^176^160 ........ troca 176 por 160 no comando anterior
```

## Para fechar o terminal direto, mesmo que exista texto digitado

```
Ctrl-Shift-q
```

## Abrir o terminal via teclado

```
Ctrl-alt-t
```

## function to get a video link from a playlist

```bash
gonly () {
        xclip -i -selection clipboard -o | awk -F'&' '{print $1}' | xclip -selection clipboard
        echo $(xclip -selection clipboard -o)
}

## função para testar se o valor passado é inteiro

``` sh
## /bin/sh
## Check if input is integer or not

${1:? Usage: No input, exiting..}

result=$(echo $1 | egrep ^[[:digit:]]+$)

if [ "$result" = "" ] ; then
    echo "Not Integer"
else
    echo "Integer"
fi
```
