---
File: /home/sergio/.dotfiles/wiki/glxinfo.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [screen, monitor, video, tools]
 vim:ft=markdown
---

glxinfo is a command-line tool that can help you diagnose problems with your 3D acceleration setup. It can also provide additional helpful information for developers. Use man glxinfo to get an overview of its options.

    glxinfo -B
