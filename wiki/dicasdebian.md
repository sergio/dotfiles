---
file: dicasdebian.md
author: Sergio Araujo
Last Change: Sun, 16 Jun 2024 - 19:56:27
---

# Converting your debian to sid

+ http://www.binarytides.com/enable-testing-repo-debian/
+ https://www.pcsuggest.com/using-debian-as-rolling-release-distribution/
+ https://www.digitalocean.com/community/tutorials/upgrading-debian-to-unstable
+ https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/i386/iso-hybrid/
+ https://debgen.simplylinux.ch/

```
sudo vi /etc/apt/preferences.d/my_preferences
Package: *
Pin: release a=stable
Pin-Priority: 700

Package: *
Pin: release a=testing
Pin-Priority: 650

Package: *
Pin: release a=unstable
Pin-Priority: 600

sudo apt-get clean
```

## How to install the silversearcher "ag" on debian

    sudo apt install -y silversearcher-ag

## Install conky on debian

    sudo apt install -y conky-all

## Debian buster tips

+ http://tinyurl.com/y5qcyjbf

AppArmor is a new line of defense against malicious software.  The release
notes indicate it's now enabled by default in buster.  For desktops, I
recommend installing apparmor-profiles-extra apparmor-notify.  The latter
will provide an immediate GUI indication when something is blocked by
AppArmor, so you can diagnose strange behavior.  You may also need to add
userself to the adm group with adduser username adm.

Programming fonts

    sudo apt install -y fonts-firacode
    sudo apt install -y fonts-noto-mono fonts-inconsolata
    sudo apt install -y fonts-fantasque-sans

## Install more fonts with Typecatcher

+ [Install google fonts](http://ubuntuhandbook.org/index.php/2020/07/install-google-web-fonts-ubuntu-20-04/)

Typecatcher is an gui tool that downloads google fonts and installs them on your system. It can be installed with apt-get

    sudo apt-get install typecatcher

Google fonts contain lots and lots of fonts of various designs. They are getting very popular on websites since they look much better than older fonts.

Some of the popular fonts are Lato, Open Sans, Merriweather Sans, Roboto etc. Install the ones that you like.

## How To Install A Debian Package On Any Linux Distribution

+ https://www.addictivetips.com/ubuntu-linux-tips/install-a-debian-package-on-any-linux-distribution/

    ar xv package.deb

All three items should now be inside ~/deb-extracted. Use the rm command to
remove "debian-binary". It's not necessary, as we are not using Debian Linux.

From here, we'll need to extract the file data from data.tar.xz. It contains
everything required to run the program you downloaded as a program on Linux.
Extract it to the folder with tar.

    tar -xvf data.tar.xz

Extracting the data archive will output 3 folders. The folders are "opt",
"usr" and "etc".

Using rm -rf, delete the etc folder. Items in this folder aren't needed, as it
is a Debian update job to check for updates.

Note: Don't a / in front of the command below. You might accidentally delete
/etc/, and not the etc folder extracted in ~/deb-extracted.

    rm -rf etc

Next, move the files inside of usr and opt files to where they belong on the
PC. For example, to install Google Chrome on a non-Debian Linux distribution,
you'd move the files to where they belong, manually:

    cd opt
    sudo -s
    mv google /opt/
    ln -snf /opt/google/google-chrome /usr/bin/
    cd .. share
    mv -f * /usr/share/

The above example shows exactly what to do with extracted files from
data.tar.xz. Obviously, other Debian packages might have contents inside the
extracted folder that are different from the ones you see in this tutorial.
The idea is to look at the folder names inside of a data.tar.xz archive, and
pay attention to the names. The folders inside have the same names as folders
on your Linux PC's filesystem, and the items inside go to those locations.

## Install softwares and upgrade the system

The system upgrade is recommended at runlevel 3, simply without any GUI program
running. To do so, logout from the system, press Alt + Ctrl + F2 to open up a
virtual tty, then login with your username and password. If you are using a
graphical login manager then stop it,

    sudo service sddm stop
    sudo service gdm stop
    sudo service xdm stop

## limiting apt-get speed

    sudo apt-get -o Acquire::http::Dl-Limit=25 install -y package

## Then run

    sudo apt-get dist-upgrade

## Debian unstable repositary (sid)
deb http://ftp.us.debian.org/debian/ sid main contrib non-free deb
http://ftp.us.debian.org/debian/ unstable main contrib non-free

## Cleaning apt cache
+ https://askubuntu.com/a/285692

    `sudo apt-get clean`

## Install some metapackages
Run as root

    tasksel

## Install numix-circle

+ https://www.youtube.com/watch?v=1G4XGNHMpNM
+ https://gist.github.com/voyeg3r/fd8c3989d6f5c690e21e1474b950741c

    sudo apt install git docky -y

installnumix (){
    [ -d ~/.icons ] || mkdir ~/.icons
    [ -d ~/.themes ] || mkdir ~/.themes
    cd ~/.themes
    git clone https://github.com/numixproject/numix-gtk-theme.git
    cd ~/.icons
    git clone --depth 1 https://github.com/numixproject/numix-icon-theme.git
    git clone --depth 1 https://github.com/numixproject/numix-icon-theme-circle.git
    cd numix-icon-theme && cp -R * ..
    cp numix-icon-theme-circle && cp -R * ..
    gsettings set org.gnome.desktop.interface gtk-theme "Numix"
    gsettings set org.gnome.desktop.wm.preferences theme "Numix"
    gsettings set org.gnome.desktop.interface icon-theme "Numix-Circle"
} && installnumix

## O que fazer ap�s instalar o debian
+ https://www.youtube.com/watch?v=mifEI1pBx4s
+ https://debgen.simplylinux.ch/

## Install shellcheck (verify shell script code)
+ https://github.com/koalaman/shellcheck#installing

    apt-get install shellcheck

