---
File: /home/sergio/.dotfiles/wiki/neofetch.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [utils, desktop, customization]
 vim:ft=markdown
---

## What is neofetch?
Neofetch is a super-convenient command-line utility used to fetch system
information within a few seconds. It is cross-platform, open-source, and it
displays everything ranging from your system’s uptime to the Linux kernel
version. This guide will explain the working of Neofetch, its features, and the
method of installing it.

## How to custumize neofetch?
+ https://github.com/dylanaraps/neofetch/wiki/Customizing-Info
+ https://github.com/AlphaTechnolog/dotfiles/blob/main/.config/neofetch/config.conf

## See also:
+ ufetch
+ https://gitlab.com/jschx/ufetch/-/blob/master/ufetch-void
