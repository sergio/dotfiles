---
file: sysctl.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# sysctl - controls kernel parameters
+ https://www.tecmint.com/change-modify-linux-kernel-runtime-parameters/
+ https://www.picoloto.com.br/linux/otimizando_swap_memoria_ram.php
+ https://mythreed.com/posts/2017-10-26-tweaking-void-pt2/
+ https://wiki.mikejung.biz/Sysctl_tweaks

These settings are tested on my 4GB old machine runing voidlinux
and were copyied from this link: https://mythreed.com/posts/2017-10-26-tweaking-void-pt2/

## How much swapness is my computer consuming?

    sudo sysctl -a|grep swappiness

## How to view current sysctl settings

To view the current sysctl settings you can run the command below which will
list all of the settings currently applied to the server.

    sudo sysctl -a

If you want to make a backup of the sysctl settings before you begin tweaking
and changing stuff, run this command, which will output all current sysctl
settings, then send the output to a file called sysctl.bak. I always like to
do this right away so that I can always revert settings back to the default if
needed.

    sysctl -a > /$location_to_save/sysctl.bak

## How to change the swappiness config?

    sudo mkdir /etc/sysctl.d/
    sudo vim /etc/sysctl.d/50-tweaks.conf

in that file place the following

    vm.swappiness = 5
    vm.vfs_cache_pressure = 50

    vm.dirty_background_ratio = 25
    vm.dirty_background_bytes = 0
    vm.dirty_ratio = 75
    vm.dirty_bytes = 0
    vm.dirty_expire_centisecs = 6000
    vm.dirty_writeback_centisecs = 2000

Then run following command to apply the changes to the running configuration.

    sudo sysctl -p

