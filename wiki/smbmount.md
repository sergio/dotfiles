---
file: smbmount.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---



smbmount //<ip>/<resource> <local_mount_point>
