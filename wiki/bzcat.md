---
file: bzcat.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# bzcat.md

		bzcat - decompresses files to stdout

## Example
+ https://stackoverflow.com/a/47226061/2571881

		for i in *; do bzcat "$i" | head -n10; done

