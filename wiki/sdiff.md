---
file: sdiff.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# sdiff.md
+ https://www.computerhope.com/unix/usdiff.htm

Compare two files side-by-side, optionally merge them interactively, and output the results.

## breaf explanation

    sdiff -o merged.file left.file right.file

After runing the above command the prompt `%` will appear
if we type `l` sdiff will use the left file and if we use `r`
sdiff will use the right version

the symbol `>` indicats that the right file has lines that are
not present in the left one version, so if we want to keep them
we must press `r`.

## Manually merge two files using sdiff

I'd like to merge two files by doing the following:

    Output the diff of the two files into a temp file and
    Manually select the lines I want to copy/save.

The problem here is that diff -u only gives me a file lines of context, while I want to output the entire file in a unified format.

Is there any way diff can do this?

### solution

One option that might fit the bill for you,

sdiff : side-by-side diff of files.

    sdiff -o merged.file left.file right.file

Once there, it will prompt you with what lines you want to keep from which file. Hit ? and then enter for a little help. Also man sdiff with the detailed goods.

(In my distro, these come packaged in the "diffutils" package [fedora,centos])

If you need to automate the process, you might want to try the util merge, which will mark conflicts in the files. However, that might put you back at square one.
