---
file: livestreamer.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Livestreamer
Livestreamer is a command-line utility that pipes video streams from various
services into a video player, such as VLC. The main purpose of Livestreamer
is to allow the user to avoid buggy and CPU heavy flash plugins but still be
able to enjoy various streamed content. There is also an API available for
developers who want access to the video stream data.

## Instalation

    sudo pip install -U livestreamer

## Como assistir aos vídeos do Twitch sem engasgos com o Livestreamer
+ https://www.tecmundo.com.br/tutorial/82183-assistir-videos-twitch-engasgos-livestreamer-video.htm
+ https://github.com/bastimeyer/livestreamer-twitch-gui/releases

A GUI para o Livestreamer pode ser baixada no link acima, para rodar
descompacte o arquivo para sua arquitetura e rode o script `start.sh`

