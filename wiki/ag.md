---
file: ag.md - the silver searcher
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# ag - The SilverSearcher

## instalation 
On artix linux i compiled the code and did a sudo make install

A code-searching tool similar to ack, but faster. http://geoff.greer.fm/ag/
Recursively search for PATTERN in PATH. Like grep or ack, but faster.

    ag [file-type] [options] PATTERN [PATH]

    # Searches for sergio on python files
    ag --python sergio

    ag --list-file-types

Considering the above example you can see that the only thing
that you must use when using 'ag' command is its own name and
one pattern to find, because always you have brackets options
between brackets this means that you can ignore things between
them

In my case I've tried to use 'ag' as search tool for ctrlp
plugin in vim and always it seems to find mp3 files and I am
trying to use .agignore file

OBS: The "agignore" file will be replaced by ~/.ignore in the near future

    alias ag='ag --path-to-ignore ~/.ignore'

## localizar links markdown sem extensão ".md"

O uso da opção `-l` do ag faz ele exibier somente os nomes

    ``` ag
    ag -l '\[[^]]*\]\([^):.]*\)' .
    ```

## Exibir nomes de arquivos que contém certa palavra

    ag 'word' . -il

To print the filenames matching PATTERN and the number of matches in each
file, other than the number of matching lines, use the -c switch as shown.

    ag -lc root ./bin/

    /etc/passwd:3

Se eu digitar apenas um termo de busca ele vai mostrar os contextos
com a palavra buscada em destaque

       -A --after [LINES]
              Print lines after match. If not provided, LINES defaults to 2.

       -B --before [LINES]
              Print lines before match. If not provided, LINES defaults to 2.

       -c --count
              Only print the number of matches in each file. Note: This is the
              number of matches, not the number of matching lines. Pipe output
              to wc -l if you want the number of matching lines.

       -C --context [LINES]
              Print lines before and after matches. Default is 2.

       -g PATTERN
              Print filenames matching PATTERN.

       -G --file-search-regex PATTERN
              Only search files whose names match PATTERN.

       --hidden
              Search hidden files. This option obeys ignored files.

       --[no]numbers
              Print line numbers. Default is to omit line numbers when search‐
              ing streams.

       -o --only-matching
              Print only the matching part of the lines.

       -v --invert-match
              Match every line not containing the specified pattern.


