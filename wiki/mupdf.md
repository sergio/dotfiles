---
file: mupdf.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# MuPDF is a lightweight PDF viewer written in portable C

tags: pdf, tools
