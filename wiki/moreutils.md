---
File: ~/.dotfiles/wiki/moreutils.md
Last Change: Mon, 12 Feb 2024 - 08:42:47
tags: [ ]
---

## intro

+ [Reference for more](https://ostechnix.com/moreutils-collection-useful-unix-utilities/)

A Collection Of More Useful Unix Utilities
List Of Available Utilities In Moreutils

As of writing this guide, Moreutils provides the following utilities:

    chronic - Runs a command quietly unless it fails.
    combine - Combine the lines in two files using boolean operations.
    errno - Look up errno names and descriptions.
    ifdata - Get network interface info without parsing ifconfig output.
    ifne - Run a program if the standard input is not empty.
    isutf8 - Check if a file or standard input is utf-8.
    lckdo - Execute a program with a lock held.
    mispipe - Pipe two commands, returning the exit status of the first.
    parallel - Run multiple jobs at once.
    pee - tee standard input to pipes.
    sponge - Soak up standard input and write to a file.
    ts - timestamp standard input.
    vidir - Edit a directory in your text editor.
    vipe - Insert a text editor into a pipe.
    zrun - Automatically uncompress arguments to command.
