---
-- File: /home/sergio/.dotfiles/wiki/zzz.md
-- Last Change: Mon, 19 Feb 2024 - 14:05:58
-- tags: [suspend, zzz, slock, x11, screen, lockscreen, lock]
-- vim:ft=markdown:
---

zzz, ZZZ – suspend or hibernate your computer


## Lock screen on suspend:
+ https://unix.stackexchange.com/a/606178/3157

```sh
#!/bin/sh
# File: /etc/zzz.d/suspend/01

doas -u sergio slock &
sleep 2
```

