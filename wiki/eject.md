---
file: eject.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Remontar pendrive sem retira-lo

eject /dev/sdb; sleep 1; eject -t /dev/sdb

## abrir bandeja de cd

eject

## fechar bandeja de cd

eject -t

