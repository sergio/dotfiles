---
file: readlink.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [ln, link, symlink]
---
#  readlink.md intro:

readlink - print resolved symbolic links or canonical file names

example:

    readlink ~/.config/nvim

It shows ~/.config/nvim (as an exemple)
