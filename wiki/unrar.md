---
File: unrar.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: mar 08, 2021 - 15:25
tags: [tools, rar, unrar]
---

#  unrar.md intro:
+ https://www.wikihow.com/Unrar-Files-in-Linux

## How to install unrar on Linux

    Type "cd /tmp".
    Type "wget http://www.rarlab.com/rar/rarlinux-3.9.1.tar.gz ".
    Unrar the file with the following command: "tar -zxvf rarlinux-3.9.1.tar.gz".

    Enter "cd rar".
    Type "./unrar".

    cp rar unrar /usr/local/bin
    The unrar command is now available for use in your Linux installation

## using unrar

    unrar x file.rar
