Filename: typography.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [ttf, otf, fonts, typography]

 vim:ft=markdown
## Sites an tools to help you choose a better font:
+ https://wordmark.it/

## Beautiful fonts:
+ https://fonts.google.com/specimen/Acme
+ https://fonts.google.com/specimen/Alegreya
+ https://fonts.google.com/specimen/Style+Script
+ https://fonts.google.com/specimen/Suez+One
+ https://fonts.google.com/specimen/Great+Vibe
+ https://fonts.google.com/specimen/Concert+One
+ https://fonts.google.com/specimen/Bebas+Neue


