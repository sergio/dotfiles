Filename: curso-nvim.md
Last Change: Thu, 01 Dec 2022 - 09:06:06
tags: [nvim, vim, curso, course]

## Roteiro para o curso em vídeo do nvim:

+ Instalando um nvim pronto (e usar como modelo)
+ Movimentando-se rapidamente no arquivo corrente e entre arquivos

    f(char) t(char) {}
    Ctrl+o
    gi
    `.
    zz, zt, zb
    Ctrl+d, Ctrl-u

+ Mapear teclas para ações comuns
+ Básico: dot repeat, aliases para abrir o vim,
+ Autocomandos e ftplugins

+ Abrindo arquivos rapidamente usando aliases e funções do shell (fzf)

   funções "old, vif, nf"
   abrindo o conteúdo do clipboard no nvim
   abrindo pipe do shell no nvim


+ Undo, undo break, undofile undolist, backup no github

    -- Break undo chain on punctuation so we can
    -- use 'u' to undo sections of an edit
    local break_points = {',', '.', '!', '?', ';', '(', ':', '[', '{', '/'}
    for _, char in ipairs(break_points) do
    map('i', char, char .. "<C-g>u", { noremap = true })
    end

   :earlier 10m
   :earlier 2h
   :earlier 2d

