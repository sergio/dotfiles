---
file: inkscape.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [svg, verctor, img, design, icc]
---

## Install inkscape flatpak
+ https://flathub.org/apps/details/org.inkscape.Inkscape

    flatpak install flathub org.inkscape.Inkscape
    flatpak run org.inkscape.Inkscape

    cd ~/.local/share/applications

```sh
# Create your flatpack icon (the dash at <<-EOF will supress tabs)
cat <<-EOF >> ~/.local/share/applications/org.inkscape.Inkscape.desktop
    [Desktop Entry]
    Type=Application
    Version=1.2
    Name=Inkscape
    Comment=Create and edit Scalable Vector Graphics images
    Icon=org.inkscape.Inkscape
    Exec=flatpak run org.inkscape.Inkscape
    Terminal=false
    StartupNotify=true
    Categories=Graphics;VectorGraphics;GTK;
EOF
```

Para rodar a versão flatpack:

```
#!/bin/env bash
# File: /home/sergio/.local/bin/inkscape
# Last Change: Mon, 10 Oct 2022 14:34:27
# Purpuse: Give precedence to inkscape flatpack
# Author: Sérgio Araújo

# Para esse script funcionar ele tem que estar numa pasta
# do path que seja lida antes de qualquer comando inkscape
# tipo -> PATH="~/.local/bin:$PATH"
#
# seguindo o exemplo acima a sua pasta "~/.local/bin" será lida primeiro
# e o teste abaixo vai abrir o flatpak caso esteja instalado
#
# Provavelmente a sua pasta ~/.local/bin já é lida primeiro,
# pra saber rode esse comando:
# tr ':' '\n' <<< $PATH

result=$(flatpak list | grep -ic 'inkscape' 2>/dev/null)

if [ "$result" -lt 1 ]; then
    inkscape
else
    flatpak run org.inkscape.Inkscape
fi
```


## color profile:
+ https://wiki.archlinux.org/title/ICC_profiles#Loading_ICC_profiles
+ https://www.youtube.com/watch?v=bEN4zuqOJzY&list=PLC_abLAOqeVE3r2LkkgZKQJ98LQL3z0Eg

## Reenable welcome screen:
+ https://inkscape.org/forums/beyond/how-do-i-turn-the-welcome-screen-back-on/

    Go->Preferences->Interface->Windows->>Miscellaneous: Show Welcome dialog=check

## videos pra baixar
+ Fechamento de arquivo para gráfica: https://www.youtube.com/watch?v=2irQptMCm_c (pdf/x-1a)

[advanced technics](https://design.tutsplus.com/courses/advanced-techniques-in-inkscape)
Inkscape Explained: Path Functions
https://www.youtube.com/watch?v=R8lE2wyfSYY

Tiled clones ->
https://www.youtube.com/watch?v=YZLwVpeu2-g

Every time you create radial tiled clones define:
1 - Move the rotation center off the object
2 - Shift/deslocamento = -100
3 - Rotation/rotação = número de objetos dividido por 360

In order to calculate the angle you can:

    echo $(360/16) | pbcopy

## Tamanho para cartão de visitas:

    91 x 51 mm
    80 x 50 mm  Padrão Internacional

## Shortcuts reference:
+ https://inkscape.org/doc/keys.html

    % ............................ toggle snap

    nodes
    Shift + c ................... make cusp
    Shift + s ................... make smooth
    Shift + y ................... make symetric

    Ctrl + r .................... toggles rules

    Ctrl + Alt + h .............. align by vertical axis
    Ctrl + Alt + t .............. align by horizontal axis
    Ctrl + Alt + 5 .............. align by vertical and horizontal axis

## Select objects (special way):

    Alt + selection tool (to select by touching any objects region)

## shades generator (gradientes)
+ https://noeldelgado.github.io/shadowlord/#ff5233

## Canvas rotate (rotação da página)

    Ctrl+Shift + Scroll

## inkscape 1.0 beta
+ https://inkscape.org/release/1.0beta2/platforms/

    https://inkscape.org/release/inkscape-1.0beta2/gnulinux/appimage/dl/

Compiling the new version

    https://inkscape.org/develop/getting-started/#building-inkscape-linux

## Deleting all guides

    Edit > delete all guides

    Editar > Eliminar todas as guias

## Changing nodes

    Shift + u   ......... make curve
    Shift + l   ......... make line
    Shift + b  .......... break node

## Efeitos em caminhos
+ https://www.youtube.com/watch?v=jqP-NkSeUSc&t=313s

Para criar um efeito de corda crie uma figura que tem duas
extremidades parecidas com a ponta de um facão. (machete)

## Define half size of an object by dividing with slash

When you type the size of the object you can put something like

    90/2  on the size dialog

## Yin Yang logo
+ https://www.youtube.com/watch?v=5ML4MVhvrv4

    Firts combine, then divide

    Another way: Create the intermediate circles, apply union to them,
    select the united form and the big circle and apply division

    OBS: to create a circle with the half of the size use
         Ctrl + Shift + M (transformation)

## Good fonts for desingners

    MiriamLibre https://fonts.google.com/specimen/Miriam+Libre
    http://typeof.net/Iosevka/

## Shortucts list
+ https://inkscape.org/en/doc/keys046.html

    % .................... togle snapping
    Shift - g ............ convert object to guides
    Alt-Click + drag ..... draw a line to select multiple objects

## Import Adobe Ilustrator
+ https://www.youtube.com/watch?v=Hwilw97pR6k

Just change the extension to pdf and vice-versa

## Inkscape open symbols
+ https://github.com/Xaviju/inkscape-open-symbols

## New icon set
+ https://logosbynick.com/new-icons-for-inkscape/
+ http://logosbynick.com/wp-content/uploads/2016/04/Version-92.zip

## How to make inskscape go dark (theme)
+ https://logosbynick.com/how-to-make-inkscape-go-dark/

## How to use templates on inkscape

Put your template files at:

    ~/.config/inkscape/templates

After that you can do:  File > New from template

Tags: inkscape, design
