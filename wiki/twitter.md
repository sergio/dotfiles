---
file: twitter.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# twitter
How to perform advanced search on twitter

    https://twitter.com/search-advanced
