---
file: shuf.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## install

    from GNU coreutils

## Mostrar uma linha aleatória de um arquivo

    shuf -n1 file.txt

    mplayer $(ls -a ~/Music/**/*.mp3 | shuf)
    mpg123 $(ls **/*.mp3 | shuf -n 1)
    ls **/*.mp3 | shuf -n 1 | sed 's/.*/"&"/g' | xargs mpg123

    gsettings set org.gnome.desktop.background picture-uri file://`ls ~/img/new-wallpapers/* | shuf -n1`

    ls ~/pictures/new-wallpapers | shuf -n1

Lembre que o `mpg123` tem a opção `-Z` para gerar aleatoriedade!

## Editing a random file

    vim $(ls -1 | shuf -n1)

## Generating random numbers in a range
+ https://stackoverflow.com/a/2556282/2571881
+ https://unix.stackexchange.com/a/140751/3157

    shuf -i 2000-65000 -n 1
    shuf -i 1-100 -n 1
    shuf -i 1-4500 -n 1

Another option is using:

    echo $((RANDOM % 60))

