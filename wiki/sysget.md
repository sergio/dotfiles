---
file: sysget.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# sysget � A Front-end for Every Package Manager in Linux
+ https://www.tecmint.com/sysget-front-end-package-manager-in-linux/

## Instalation
    sudo wget -O /usr/local/bin/sysget https://github.com/emilengler/sysget/releases/download/v1.2.1/sysget
    sudo mkdir -p /usr/local/share/sysget
    sudo chmod a+x /usr/local/bin/sysget

## How to use it

    sudo sysget
    sudo sysget update
    sudo sysget install <package name>

To remove a package:

$ sudo sysget remove package

To run an update:

    sudo sysget update

To upgrade your system:

    sudo sysget upgrade

Upgrade specific package with:

    sudo sysget upgrade <package name>

To remove orphans:

    sudo sysget autoremove

Clean package manager cache:

    sudo sysget clean
