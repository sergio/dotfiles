---
file: unzip.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [zip, tools, unzip]
---

# unzip / zip command - sex 11 mai 2018 19:14:56 -03

    unzip file.zip

    unzip -l file.zip  # lista o conte�do

## fix strange chars (non-utf8)
+ https://unix.stackexchange.com/a/429559/3157

    7z x file.zip

## Create a directory for each zip-file and unzip it in that dir:

    parallel 'mkdir {.}; cd {.}; unzip ../{}' ::: *.zip

## Compress a folder
+ https://www.tecmint.com/unzip-extract-zip-files-to-specific-directory-in-linux/

    zip -r name.zip folder-to-zip

## Extract Zip File to Specific or Different Directory
+ https://www.tecmint.com/unzip-extract-zip-files-to-specific-directory-in-linux/

    mkdir -p /tmp/unziped
    unzip tecmint_files.zip -d /tmp/unziped
    ls -l /tmp/unziped/

## descompactar em um destino

    unzip file.zip -d /caminho/para/o/mesmo/

## compress a single folder

    zip -r zip-name.zip folder-to-zip

## How to compress multiple folders, each into its own zip archive?
+ https://stackoverflow.com/a/20452562/2571881

    for i in */; do zip -r "${i%/}.zip" "$i"; done

## uncompress all zip files creating a directory for eacho one of them

When processing files removing the file extension using {.} is often useful.
Create a directory for each zip-file and unzip it in that dir:

    parallel 'mkdir {.}; cd {.}; unzip ../{}' ::: *.zip

