---
file: tcpdump.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Introdução
+ https://www.slashroot.in/packet-capturing-tcpdump-command-linux

Opções do tcpdump

    -n option in tcpdump

if you do not use tcpdump with -n option, all the sender and destination host
address will be in "name" format, which means all ip's will be displayed with
hostnames.

Using -n option with tcpdump will disable name lookup. This will display all
the output in sender and reciever's IP address format.

    -c option in tcpdump

by using -c option you can specify the number of packets

    -s option in tcpdump

as mentioned earlier by default tcpdump only captures the firs 96bytes of a
packet. But suppose you need to capture packets in its full size then you need
to pass the size option -s with its argument.

You can either use -s0 option to capture the whole packet or use number of
bytes with -s argument.

    -vvv option for more verbose output in tcpdump

-w option used in tcpdump

using this -w option we can capture the output and save all the output to a
specified file.

-r option used in tcpdump

in order to read the file we just captured we  need to use -r option with
tcpdump command and passing filename as the argument to the command.

    sudo tcpdump -n -c <number of packages>
    -s0 -i <interface> -s <port> -w saved-package.cap

Filtering protocols using tcpdump command

    tcpdump -i eth0 tcp

### To get network interfaces name

    sudo tshark -D
    sudo tcpdump -D

Capturing with “tcpdump” for viewing with Wireshark It’s often more useful to
capture packets using tcpdump rather than wireshark. For example, you might
want to do a remote capture and either don’t have GUI access or don’t have
Wireshark installed on the remote machine.

Older versions of tcpdump truncate packets to 68 or 96 bytes. If this is the
case, use -s to capture full-sized packets:

    tcpdump -i <interface> -s 65535 -w <some-file>

You will have to specify the correct interface and the name of a file to save
into. In addition, you will have to terminate the capture with ^C when you
believe you have captured enough packets.

fonte: http://www.dicas-l.com.br/dicas-l/20081014.php

pegar somente os pacotes que estão sendo enviados ou recebidos para
www.gooogle.com.br. Para isso basta utilizar a expressão "host" seguido do
"nome do host" que deseja filtrar.

    tcpdump -i eth0 host www.google.com.br

Mas eu queria somente o acesso via HTTP (porta 80).  Sem problemas basta usar
a expressão "host" em conjunto com "port" utilizando o operador "and", veja
como:

    tcpdump -i eth0 -n host 192.168.0.1 and port 80

## tcpdump: Monitorando conexões Autor: Leandro Nascimento Souza
<leandro@minimedia.com.br> Data: 11/04/2007 **tcpdump: Monitorando conexões**

O tcpdump é um dos mais, se não o mais "famoso" sniffer para sistemas
GNU/[[http://www.vivaolinux.com.br/linux/|Linux]]. Com ele podemos realizar
análises de redes e solucionar problemas. Sua utilização é simples e sem
mistérios, bastando apenas ter os conhecimentos básicos de redes TCP/IP. Esta
dica é apenas uma introdução deste sniffer, maiores informações e documentação
à seu respeito podem ser encontradas em seu site oficial: *
[[http://www.tcpdump.org/|http://www.tcpdump.org]]

A instalação do tcpdump em sistemas Debian é super simples, bastando executar
o comando abaixo como super usuário (root):

    **# apt-get install tcpdump**

Para iniciarmos a utilização do tcpdump precisamos especificar a interface de
rede que queremos analisar com o parâmetro -i seguido da interface desejada,
por exemplo, se quisermos analisar todo o tráfego que passa pela interface
eth0, executaríamos a seguinte linha de comando:

    tcpdump -i eth0

Conexões de origem podem ser monitoradas utilizando o parâmetro src host, um
exemplo simples seria monitorarmos o tráfego que vem de 192.168.0.9 para nosso
computador, com o ip 192.168.0.2. A linha de comando ficaria da seguinte
forma:

    tcpdump -i eth0 src host 192.168.0.9

Se quisermos monitorar as conexões especificando um host de destino,
poderíamos fazê-lo com o parâmetro dst host, o exemplo abaixo mostra todo o
tráfego do host 192.168.0.2 com 192.168.0.1, no caso, 192.168.0.1 é nosso
gateway.

    tcpdump -i eth0 dst host 192.168.0.1

Com tcpdump também podemos especificar exceções com o parâmetro not host, por
exemplo, em nosso servidor queremos ver todo o tráfego que se passa em sua
interface, exceto o de 192.168.0.8, faríamos da seguinte forma:

    tcpdump -i eth0 not host 192.168.0.9

No tcpdump podemos também especificar portas de origem e destino com os
comandos src port e dst port, um exemplo seria monitorarmos o tráfego
destinado à porta 80 (http), para isso utilizaríamos a linha de comandos
abaixo e navegaríamos em um site qualquer:

    tcpdump -i eth0 dst port 80

Para verificarmos o tráfego da porta de origem 32881 por exemplo, faríamos da
seguinte forma:

    tcpdump -i eth0 src port 32881

Muitas opções avançadas podem ser obtidas com o tcpdump, essas são algumas
opções básicas, porém fundamentais para quem quer aprender sobre sniffers.
http://www.vivaolinux.com.br/dica/tcpdump-Monitorando-conexoes

