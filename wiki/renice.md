---
file: renice.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# renice - alter priority of running processes
+ https://www.lifewire.com/uses-of-commands-nice-renice-2201087
+ veja também [nice](nice)

    Last Change: mar 19 2019 08:55

## Como usar o comando?

    renice -5 3622

Mas que raios são esses números?

Simples -5 é o valor atribuído para a prioridade do processo firefox-bin, a
lógica é quanto menor o valor maior a prioridade, os valores podem ser
alterados em um intervalo de -20 a 20, caso execute o comando em um terminal
logado como root, caso contrário o intervalo é de 0 a 20. O 3622 é o PID do
firefox-bin obtido no primeiro comando.

    renice -10 -u root

Altera a prioridade de todos os processos cujo dono é o root.

    sudo renice -5 $(pgrep firefox | head -1)
