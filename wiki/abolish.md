---
file: abolish.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags [vim, plugins]
---

# vim abolish plugin

## O plugin abolish permite abreviações mais complexas como:

  :Abolish! {col,hon}our{,s,ed,ing} {}or{}

  Usando a exclamação o plugin já adiciona a abreviação no arquivo
  necessário ao carregamento da mesma

  Changing variables  - you can use cr followed by:

  c: camelCase
  m: MixedCase
  _: snake_case
  s: snake_case
  u: SNAKE_UPPERCASE
  U: SNAKE_UPPERCASE
  -: dash-case (not usually reversible; see |abolish-coercion-reversible|)
  k: kebab-case (not usually reversible; see |abolish-coercion-reversible|)
  .: dot.case (not usually reversible; see |abolish-coercion-reversible|)

