---
File: css.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: fev 17, 2021 - 15:09
tags: [tags]
---

## My epub.css backup
+ https://bitbucket.org/sergio/workspace/snippets/zXya7x

## em in pixels

    1em = 16px

## zebra striping a table
+ https://www.w3schools.com/howto/howto_css_table_zebra.asp

## Creating a description list

We have three elements:

1 - dl -> description list tags
2 - dt -> description list title
3 - dd -> description definition

``` css
dl {
    margin-top: 2em;
    margin-left: 0;
    padding-right: 2em;
    line-height: 1.5;
}

dt {
    clear: left;
    float: left;
    margin: 0;
    padding-right: 0.5em;
    font-weight: 800;
}

dd {
    padding-top: 0;
    margin-bottom: 1em;
}
```

## Conditional styling.
+ https://www.xspdf.com/resolution/54253308.html
To change a span class .inline-help that occurs inside a div class tip you can do:

``` css
div.tip >  spam.inline-code {
background-color: #d9caff;
}
```

## First letter
+ https://8thlight.com/blog/chris-peak/2012/12/27/css-typographic-flourishes.html

``` css
p::first-letter {
    display: block;
    float: left;
    font-size: 150%;
    font-weight: bold;
    line-height: 100%;
    margin: 0.1em 0.1em -0.2em 0;
    color: #444;
}

p.capitular::first-letter {
    font-size: 300%;
    float: left;
    line-height: 1em;
    margin-bottom: -0.1em;
    margin-right: 0.1em;
    color: #444;
}
```

## image align
+ https://comopublicarebooksnaamazon.com/2015/10/epub-como-flutuar-uma-imagem/

## css latex like
+ https://latex.now.sh/style.css
+ https://latex.now.sh/#getting-started

``` css
/* table section */

table {
   width: 90%;
   border-collapse: collapse;
   margin-left: 5%;
   font-family: "Linux Biolinum O", "Palatino", "sans-serif";
   font-size: 3em;
}

tr:nth-child(even) {
    background-color: #f2f2f2;
}

.styled-table thead tr {
    background-color: #888;
    color: #ffffff;
    text-align: left;
}

.styled-table th,
.styled-table td {
    padding: 0.1em 0.8em;
    text-align: left;
    padding-top: 0.65em;
    padding-bottom: 0.65em;
}

tbody tr:last-of-type {
    border-bottom: 0.1em solid  #777;
}

caption {
		margin-bottom: 0.6em;
      font-variant: small-caps;
  		font-weight: bold;
      color: #444;
}
```

## shaded inline code

``` css
/* for inline code */
.shaded {
   font-family: Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace;
    border-radius: 5px;
    background-color: #eee;
    padding-left: 6px;
    padding-right: 6px;
}
```

##  stackoverflow like <kbd> ctrl </kbd>

``` css
/* https://github.com/benweet/stackedit/issues/212 */
kbd {
    padding: 0.1em 0.6em;
    border: 1px solid #ccc;
    font-size: 11px;
    font-family: Arial,Helvetica,sans-serif;
    background-color: #f7f7f7;
    color: #333;
    -moz-box-shadow: 0 1px 0px rgba(0, 0, 0, 0.2),0 0 0 2px #ffffff inset;
    -webkit-box-shadow: 0 1px 0px rgba(0, 0, 0, 0.2),0 0 0 2px #ffffff inset;
    box-shadow: 0 1px 0px rgba(0, 0, 0, 0.2),0 0 0 2px #ffffff inset;
    -moz-border-radius: 3px;
    -webkit-border-radius: 3px;
    border-radius: 3px;
    display: inline-block;
    margin: 0 0.1em;
    text-shadow: 0 1px 0 #fff;
    line-height: 1.4;
    white-space: nowrap;
}
```

## Fontawesome icons
+ https://www.w3resource.com/icon/font-awesome/web-application-icons/warning.php

``` css
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
</head>
<body>
<i class="fa fa-warning"> Take Care</i>
```

``` css
.tip {
    padding: 1em;
    margin: 1em 0;
    padding-left: 100px;
    background-size: 40px;
    background-repeat: no-repeat;
    background-position: 15px center;
    min-height: 80px;
    color: #000000;
    /*background-color: #bed3ec;*/
    border: solid 2px #606060;
    border-radius: 6px;
    background-image: url("img/tip.png");
}
```

``` css
.warning {
    padding: 1em;
    margin: 1em 0;
    padding-left: 100px;
    background-size: 40px;
    background-repeat: no-repeat;
    background-position: 15px center;
    min-height: 80px;
    color: #000000;
    /*background-color: #bed3ec;*/
    border: solid 2px #606060;
    border-radius: 6px;
    background-image: url("img/warning.png");
}
```

## how to formate pre code
+ http://web.simmons.edu/~grabiner/comm244/weekfour/code-test.html

``` css
.code {
    font-family: Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace;
    line-height: 0.8em;
    border-radius: 10px;
    border: 4px solid #aaa;
    padding-left: 0.8em;
    padding-top: 0;
    padding-bottom: 1em;
    background-color: #eee;
    font-size: 0.7em;
    display: block;
    white-space: pre;
    line-height: 1.3em;;
    margin-top: 0.5em;
    margin-bottom: 0.5em;
    color: #111;
}
```

``` css
.shaded {
    font-family: Consolas, "Andale Mono WT", "Andale Mono", "Lucida Console", "Lucida Sans Typewriter", "DejaVu Sans Mono", "Bitstream Vera Sans Mono", "Liberation Mono", "Nimbus Mono L", Monaco, "Courier New", Courier, monospace;   	line-height: 0.8em;
    font-size: 0.8em;
    border-radius: 8px;
    background-color: #eee;
    padding-left: 0.3em;
    padding-top: 0.2em;
    padding-bottom: 0.2em;
    padding-right: 0.3em;
    display: inline;
    line-height: 1.4em;
    color: #444;
}
```

## In the sigil link a new font

``` css
@font-face {
    font-family: 'Linux Biolinum O';
    font-weight: normal;
    font-style: normal;
    font-variant: small-caps;
    src: url(../Fonts/LinBiolinum_R.otf);
}
```

## viewport-height
+ https://www.sitepoint.com/css-viewport-units-quick-start/

There are four viewport-based units in CSS. These are vh, vw, vmin and vmax.

* Viewport Height (vh). This unit is based on the height of the viewport. A value of 1vh is equal to 1% of the viewport height.

* Viewport Width (vw). This unit is based on the width of the viewport. A value of 1vw is equal to 1% of the viewport width.

* Viewport Minimum (vmin). This unit is based on the smaller dimension of the viewport. If the viewport height is smaller than the width, the value of 1vmin will be equal to 1% of the viewport height. Similarly, if the viewport width is smaller than the height, the value of 1vmin will be equal to 1% of the viewport width.

* Viewport Maximum (vmax). This unit is based on the larger dimension of the viewport. If the viewport height is larger than the width, the value of 1vmax will be equal to 1% of viewport height. Similarly, if the viewport width is larger than the height, the value of 1vmax will be equal to 1% of hte viewport width.

## Adding a string with line break before each chapter
Using counter in css

``` css
/* "Chapter: \n before each chapter https://stackoverflow.com/a/17047836/2571881 */
h1::before {
   content: "Chapter \A ";
   white-space: pre; /* or pre-wrap */
   line-height: 2em;
}
```

## Alternative Fonts

``` css
@font-face {
    font-family: 'Linux Biolinum O';
    font-weight: normal;
    font-style: normal;
    font-variant: small-caps;
    src: url(../Fonts/LinBiolinum_R.otf);
}

font-family: "Linux Biolinum O", "Palatino", "sans-serif";
```

## first-letter

``` css
p::first-letter {
     font-size: 2em;
    initial-letter: 3 2;
    -webkit-initial-letter: 3 2;
    color: #c69c6d;
   margin: 0.1em.2em 0 0;
}

```

## avoid hifenation

    -webkit-hyphens: none;

``` css
element {
adobe-hyphenate: none;
-webkit-hyphens: none;
-moz-hyphens: none;
-ms-hyphens: none;
-epub-hyphens: none;
hyphens: none;
}
```

## improve legibility
+ https://friendsofepub.github.io/eBookTricks/

``` css
body {
font-kerning: normal;
font-variant: common-ligatures oldstyle-nums proportional-nums;
font-feature-settings: "kern", "liga", "clig", "onum", "pnum";
}

h1, h2, h3 {
font-variant: common-ligatures lining-nums proportional-nums;
font-feature-settings: "kern", "liga", "clig", "lnum", "pnum";
}

table {
font-variant-numeric: lining-nums tabular-nums;
font-feature-settings: "kern", "lnum", "tnum";
}

code {
font-variant: no-common-ligatures lining-nums;
font-feature-settings: "kern" 0, "liga" 0, "clig" 0, "lnum";
}

.fraction {
font-variant-numeric: diagonal-fractions;
font-feature-settings: "frac";
}

a.text-color {
  color: inherit;
  -webkit-text-fill-color: inherit;
}
```

## Blockquote

``` css
/* https://tympanus.net/codrops/css_reference/before/ */
blockquote::before {
    content: "\201C";
    /* style the quote */
    color: #444;
    font-size: 4em;
    position: relative;
    top: 20px;
}

blockquote {
     font-family: "Linux Biolinum O";
     font-size: 2em;
     line-height: 1.2em;
     padding-left: 1em;
}
```

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0:
<--
