---
file: cancel.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

    lpq ................... lista trabalhos de impressão
    cancel -u joao ........ aborta impressões do usuário joao
    cancel -a ............. cancela todos os trabalhos de impressão

fonte: http://www.go2linux.org/how-to-clean-cancel-printer-queue-spool-spooler-linux
