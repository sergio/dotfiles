Filename: mlocate.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [locate, find, mlocate, search, packages]

 vim:ft=markdown
## The fastest way to find files by filename — mlocate (locate commands)
+ https://thucnc.medium.com/the-fastest-way-to-find-files-by-filename-mlocate-locate-commands-55bf40b297ab

### Using locate & updatedb to search file

Update the search database
Note: You don’t need to execute this command every time you want to search for file. Just run it something like every 1 month or when you cannot find something.

On the first run:

    doas updatedb

Finding files by filename (and path)
Just use simple syntax:

    locate {file-name-or-path}


More advanced syntax

Limit the number of results to 15:

    locate -n 15 ghost

Using wildcast:

    locate -n 20 "summer*.html"

Ignoring case:

    locate -n 20 -i "sUMmer*.Html"

Even with regular expression (most advanced):
Locate all file in my home folder with the text ssh in file path:

    locate -r "^/home/thuc/.*ssh.*$"


