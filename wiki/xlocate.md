---
title: xlocate.md
author: Sérgio Araújo
date: Tue, 27 Sep 2022 12:06:13
 vim:ft=markdown
tags: [admin, tools, xtools, packages, find, locate]
---

# xlocate intro:

How many times have you ever searched for a package on your repositories and
you could not find where it is located. Well, sometimes your desired package
is actually in your repos but it happens to be part of a "metapackage".

## What is a metapackage:

Metapackages describe a set of packages that are used together. Metapackages are
referenced just like any other NuGet package. By referencing a metapackage, you
have, in effect, added a reference to each of its dependent packages.

NuGet (pronounced "New Get") is a package manager designed to enable developers to share reusable code. It is a software as a service solution whose client app is free and open-source. The Outercurve Foundation initially created it under the name NuPack.

## xlocate (from xtools package)
+ https://www.migo.info/xlocate/readme_en.php

What it is for?
XLocate is a file system index management tool and a front-end to the most popular GNU-Linux fast search utilities. XLocate allows you to maintain a collection of databases for shelved or offsite CD/DVD/Blu-ray collections, memory sticks, SD cards and other memory cards, fixed and removable hard disks, network places,

## Update xlocate database

After installing xtools run:

    xlocate -S

Then you can find what package supports "pidof" for example:

## Basic usage:
pidof is in the procps-ng package. To search for a package which includes a given binary,
you can use the xlocate tool from xtools.

    ~ % xlocate /usr/bin/pkill
    procps-ng-3.3.17_2	/usr/bin/pkill


pidof is in the procps-ng package. To search for a package which includes a given binary, you can use the xlocate tool from xtools.

~ % xlocate /usr/bin/pkill
procps-ng-3.3.17_2	/usr/bin/pkill
