---
file: xinit.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## iniciar seção gráfica remota

    xinit -- :1

    ssh -X john@otherbox gnome-session

## Mudar parâmetros do teclado
+ http://blog.ssokolow.com/archives/2011/12/24/getting-your-way-with-setxkbmap/

``` sh
xrdb -merge ~/.Xresources

setxkbmap -model abnt2 \
    -layout br -variant abnt2 \
    -option \
    -option "lv3:ralt_switch" \
    -option "terminate:ctrl_alt_bksp" \
    -option "caps:escape" 
```

OBS: se sua sessão gráfica inicia via lightdm ou qualquer outro login 
manager o xinitrc não será lido, neste caso faça um link simbólico para 
o arquivo ~/.xprofile

    ln -svfn ~/.dotfiles/xinitrc ~/.xprofile

