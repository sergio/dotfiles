---
file: expr.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

O comando **expr** pode ser usado para fazer cálculos

expr 33 + 1220
1253

Para fazer uma multiplicação protegemos o asterisco (sinal de multiplicação)
com uma barra invertida

expr 5 \* 10
50

fonte: http://www.vivaolinux.com.br/dica/Utilizando-o-comando-expr

