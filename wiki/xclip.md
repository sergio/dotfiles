---
file: xclip.md
author: Sergio Araujo
Last Change: Tue, 30 Apr 2024 - 14:49:40
 vim:ft=markdown
tags: [tools, linux, clipboard, utils]
---

## Alternative to xclip for wayland:
+ https://github.com/bugaevc/wl-clipboard (wayland)

## dmenu clipboard:

+ https://github.com/cdown/clipmenu

## coloque este alias no seu ~/.bashrc

	alias xclip='xclip -selection c'

Agora pode fazer algo do tipo

	ls -1 | xclip
    # ... e colar no navegador e outros programas

    alias pbcopy='xclip -selection clipboard'
    alias pbpaste='xclip -selection clipboard -o'

## add something to your primary selection

 echo 'test primary selection xclip' | xclip -selection primary

## Tip on using above aliases
 If you want to paste on your terminal a long string you can do something like this:

    vlc $(pbpaste)

instead of worrying about using middle mouse button or even keyboard shortcuts

## Adding ssh key to your github account

    $ sudo pacman -S xclip
    # Downloads and installs xclip. If you don't have `pacman`, you might need to use another installer (like `yum`)

    $ xclip -sel clip < ~/.ssh/id_rsa.pub
    # Copies the contents of the id_rsa.pub file to your clipbo

## In order to copy the result of a command and also pipe it we can:

    echo "your string" | tee >(xclip -i -selection clipboard)

## copy a whole file to the clipboard:

    xclip-copyfile /etc/hosts

## copy image to the clipboard:

    xclip -selection clipboard -target image/png -i < ~/img/screenshots/last-selection-screenshot.png

Copy the contents of a PNG into the system clipboard (can be pasted in other programs correctly):

    xclip -sel clip -t image/png input_file.png

