---
Filename: pacmd.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

Reconfigure a PulseAudio sound server during
   runtime

## You can list the available sink names with

    pacmd list-sinks | grep name:
    name: <alsa_output.pci-0000_00_1b.0.analog-stereo>

## Listar nome e id:

    pacmd list-sink-inputs |
    tr '\n' '\r' |
    perl -pe 's/.*? *index: ([0-9]+).+?application\.name = "([^\r]+)"\r.+?(?=index:|$)/\2:\1\r/g' |
    tr '\r' '\n'

    str="$(pacmd list-sink-inputs)"
    paste -d\| \
    <(printf '%s' "$str" | grep 'application.process.binary' | cut -d'"' -f 2) \
    <(printf '%s' "$str" | grep 'index: ' | rev | cut -d' ' -f 1 | rev)

## Script para mudar o volume pelo ID

    #!/bin/bash

    inc() {
    playback_input=$(pactl list sink-inputs short | awk '{print $1}' | head -1)
    pactl set-sink-input-volume "$playback_input" +5%
    }

    dec() {
    playback_input=$(pactl list sink-inputs short | awk '{print $1}' | head -1)
    pactl set-sink-input-volume "$playback_input" -5%
    }

    mute() {
    playback_input=$(pactl list sink-inputs short | awk '{print $1}' | head -1)
    pactl set-sink-input-mute "$playback_input" toggle
    }
