---
file: umount.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [mount, usb, disk]
---

## Desmonta dispositivo ocupado

    umount -l /media/smb

## pode também usar fuser

    fuser -mkv /media/dispositivo

## See also: [bashmount](bashmount.md)
