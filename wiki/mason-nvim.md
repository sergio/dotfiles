---
Filename: mason-nvim.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [nvim, plugins, lsp]
 vim:ft=markdown
---
Reference:
+ https://github.com/Blovio/NeoVim_Essentials

    -- mason --> mason-lspconfig --> lspconfig
    -- must be setup in this order
    require "plugins/configs/mason"
    require "plugins/configs/mason-lspconfig"
    require "plugins/configs/lspconfig"
