---
File: ipython.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: mai 04, 2020 - 20:08
tags: [python, tools]
---

#  ipython.md intro:
+ https://ipython.readthedocs.io/en/stable/

Interactive python shell

    %quickref ............ shows whole reference on it
    %recall .............. reruns a particular command
    %edit ................ edit the command
    !ping www.google.com . run a shell cmd
