---
file: freenx.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# What is FreeNX
FreeNX is a system that allows you to access your desktop from another machine over the Internet. You can use this to login graphically to your desktop from a remote location.
