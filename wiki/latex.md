---
file: latex.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [latex, tex]
---

# References
+ https://r2src.github.io/top10fonts/

## Image sizes based on the page widht

     \includegraphics[width=0.5\textwidth]{file}

## overleaf ebook template
+ https://pt.overleaf.com/latex/templates/latex-epub-slash-ebook-template/csjgmvzppmcr

``` tex
\documentclass[ebook,12pt,oneside,openany]{memoir}
\usepackage[utf8x]{inputenc}
\usepackage[english]{babel}
\usepackage{url}

% for placeholder text
\usepackage{lipsum}

\title{\LaTeX{} ePub Template}
\author{A Author}

\begin{document}
\maketitle

Once upon a time\ldots This document shows how you can get ePub-like formatting in \LaTeX{} with the \verb|memoir| document class. You can't yet export directly to ePub from writeLaTeX, but you can download the source and run it through a format conversion tool, such as \verb|htlatex| to get HTML, and then go from HTML to ePub with a tool like Sigil or Calibre. See \url{http://tex.stackexchange.com/questions/16569} for more advice. And they lived happily ever after.

\lipsum

\end{document}
```

## Italic quotes

``` tex
% https://grantingram.wordpress.com/2007/07/30/newcommand-and-italic-quotes-in-latex/
\newenvironment{italicquotes}
{\begin{quote}\itshape}
{\end{quote}}
```




## Using rgb colors in hypersetup
+ https://latexcolor.com/
+ https://texblog.org/2015/05/20/using-colors-in-a-latex-document/

    urlcolor=[rgb]{0.5 0.0 0.13}

## Install latex packages at home directory
+ https://blog.intgckts.com/installing-latex-packages-in-local-or-home-directory/

Create a personal texmf tree in home directory(Only for the first time when installing a package),

    cd ~
    mkdir -p texmf/tex/latex/

    Download the package you want to install, from CTAN. Let’s say it is CirciTikz.
    Unzip the package now.

    gunzip circuitikz.zip

Check for .sty and other files in the extracted ‘circuitikz’ folder.
Move the unzipped ‘circuitikz’ directory to ~/texmf/tex/latex/

    mv circuitikz ~/texmf/tex/latex/

    Now, make LaTeX to recognize the new package:

    texhash ~/texmf

## Latex packages description

+ blindtext generate paragraphes, even whole documents with sections

## \FloatBarrier (capitalization important)
+ https://bit.ly/33QoROx
+ https://robjhyndman.com/hyndsight/latex-floats/

that basically says, any floats that were included above this point in the
code cannot appear after this line.

## Adding special keys

use the package menukeys

``` tex
You can visualize paths \directory{/home/moose/Desktop/manual.tex}
or menus \menu{View > Highlight Mode > Markup > LaTeX} or key
press combinations: \keys{\ctrl + \shift + F} is for formatting
in Eclipse.

You can also visualize \keys{\tab}, \keys{\capslock}, \keys{\Space},
\keys{\arrowkeyup} and many more.
```

## Include chapter

    \include{cha1}

## Avoid page numbering
+ https://tex.stackexchange.com/a/192158/3528

    \thispagestyle{empty}

## changing font size
+ https://tex.stackexchange.com/a/2503/3528

I also needed to have the fix-cm package loaded

    %% Define a HUGE
    \newcommand\HUGE{\@setfontsize\Huge{38}{47}}

    ---
    documentclass: extarticle
    fontsize: 17pt
    mainfont: fbb
    ---

### Changing verbatim font size
+ https://tex.stackexchange.com/questions/141100/

    \makeatletter
    \renewcommand\verbatim@font{\normalfont\small\ttfamily}
    \makeatother

## \LaTeX\ classes

    scrbook ....................... alternative to book (koma-script)

## how to use minted package to show codes in LaTeX
+ https://alipourmousavi.com/blog/index.php/2018/02/08/using-minted-package-in-latex-to-format-codes/
+ https://tex.stackexchange.com/questions/452150/

    pdflatex -shell-scape -synctex=1 -interaction=nonstopmode test-minted.tex

## how to reference paragraph
+ https://tex.stackexchange.com/a/7628/3528

``` tex
\documentclass[10pt,twocolumn]{article}
\setcounter{secnumdepth}{6}
\begin{document}
\paragraph{blub}\label{para}
abc paragraph \ref{para}
\end{document}
```

## Installation
+ https://texwiki.texjp.org/?Linux#texliveinstall
+ http://www.tug.org/TUGboat/tb32-1/tb100gregorio.pdf

    #wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
    wget http://mirrors.concertpass.com/tex-archive/systems/texlive/tlnet/install-tl-unx.tar.gz
    tar xvf install-tl-unx.tar.gz
    cd install-tl*
    sudo ./install-tl --repository http://mirror.ctan.org/systems/texlive/tlnet/

    adding path
    sudo /usr/local/texlive/????/bin/*/tlmgr path add

## throught tlmgr install full package list

    sudo tlmgr install scheme-full

## Installing LaTeX on debian or ubuntu

    sudo apt-get install texlive-full

## sparing an almost empty page

Insert this command right after the \subsection line:

    \enlargethispage{\baselineskip}

##  acknowledgement and abstract in roman numbering
+ https://tex.stackexchange.com/a/288220/3528

``` tex
\documentclass{article}
\begin{document}

\pagenumbering{roman}
\section*{Abstract}
\addcontentsline{toc}{section}{Abstract}
\clearpage

\section*{Acknowledgments}
\addcontentsline{toc}{section}{\protect\numberline{}Acknowledgements}
\clearpage

\tableofcontents
\clearpage

\pagenumbering{arabic}
\section{Introduction}

\end{document}
```

## convert tex to epub3
+ https://tex.stackexchange.com/a/397344/3528

    tex4ebook -f epub3 filename.tex

## instalando novos pacotes
Antes de mais nada atualize o instalador

    sudo tlmgr update --self

    sudo tlmgr install packagename

    sudo tlmgr install <package1> <package2> ...
    sudo tlmgr remove <package1> <package2> ...

    sudo tlmgr install fbb
    sudo tlmgr install CJKmainfont
    sudo tlmgr install inconsolata
    sudo tlmgr install merriweather
    sudo tlmgr install bookman
    sudo tlmgr install paratype
    sudo tlmgr install collection-fontsrecommended
    sudo tlmgr install lipsum
    sudo tlmgr install csquotes
    sudo tlmgr install lastpage
    sudo tlmgr install tcolorbox
    sudo tlmgr install environ
    sudo tlmgr install trimspaces
    sudo tlmgr install sectsty
    sudo tlmgr install ly1
    sudo tlmgr install ebgaramond
    sudo tlmgr install fourier heuristica t2
    sudo tlmgr install classicthesis bera
    sudo tlmgr install bera
    sudo tlmgr install mparhack
    sudo tlmgr install tocloft
    sudo tlmgr install fontaxes
    sudo tlmgr install mweights
    sudo tlmgr install framed
    sudo tlmgr install menukeys
    sudo tlmgr install xstring
    sudo tlmgr install adjustbox
    sudo tlmgr install collectbox
    sudo tlmgr install relsize
    sudo tlmgr install catoptions

## generate random paragraphs (random text)

    \usepackage{blindtext}

In the document type

    \blindtext

## pargraphs indentation

    \setlength{\parindent}{4em}
    \setlength{\parskip}{1em}

## encode for garamond font
I was getting a error about `ly1enc` and the soulution was

    sudo tlmgr install ly1

## Install garamond fonts and other nonfree fonts
+ http://tug.org/fonts/getnonfreefonts/

## latex documetatio:

    sudo tlmgr install texdoc
    texdoc memoir
    texdoc -l memoir   ............ shows all help options

## Veja também [[LatexExemplos]]

    pdflatex documment.tex

## Como exibir teclas de atalho corretamente no LaTeX
+ https://tex.stackexchange.com/questions/5226/keyboard-font-for-latex

## Format source code on LaTeX
Read about package listings

## Other tableofcontents formating
+ https://tex.stackexchange.com/a/35835/3528

## hide links on tableofcontents
+ https://tex.stackexchange.com/a/88416/3528

    {\hypersetup{hidelinks}
    \tableofcontents
    }

## monospaced scaled font

    \usepackage[scaled=0.9]{DejaVuSansMono}

## verbation with background background

``` tex
%% my own definition of verbatim gray backgroundcolor
%% https://latexcolor.com/
\definecolor{verbgray}{gray}{0.96}

\lstnewenvironment{code}{%
  \lstset{backgroundcolor=\color{verbgray},
  frame=single,
  framerule=0pt,
      basicstyle=\ttfamily,
  columns=fullflexible}}{}
%% END of my own definition of verbatim gray backgroundcolor
```

## Adding tables with alternated colors
+ https://texblog.org/2011/09/02/coloring-every-alternate-table-row/

``` markdown
\documentclass[11pt]{article}
\usepackage[table]{xcolor}
\definecolor{lightgray}{gray}{0.9}
\begin{document}
\begin{table}[ht]
\caption{default}
\begin{center}
\rowcolors{1}{}{lightgray}
\begin{tabular}{r|rrrrr}
  \hline
 & 1 & 2 & 3 & 4 & 5 \\
  \hline
1 & 2.36 & 1.08 & -0.49 & -0.82 & -0.65 \\
  2 & -0.68 & -1.13 & -0.42 & -0.72 & 1.51 \\
  3 & -1.00 & 0.02 & -0.54 & 0.31 & 1.28 \\
  4 & -0.99 & -0.54 & 0.97 & -1.12 & 0.59 \\
  5 & -2.35 & -0.29 & -0.53 & 0.30 & -0.30 \\
  6 & -0.10 & 0.06 & -0.85 & 0.10 & -0.60 \\
  7 & 1.28 & -0.46 & 1.33 & -0.66 & -1.80 \\
  8 & 0.80 & 0.46 & 1.37 & 1.73 & 1.93 \\
  9 & -0.75 & 0.28 & 0.51 & 0.19 & 0.58 \\
  10 & -1.64 & -0.12 & -1.17 & -0.10 & -0.04 \\
   \hline
\end{tabular}
\end{center}
\end{table}
\end{document}
```

## Have you heard about synclatex
+ https://tex.stackexchange.com/a/118491/3528

SyncTeX is a utility written by Jérôme Laurens which enables synchronization
between your source document and the PDF output. If your editor/viewer
supports it, then you can click in your source and jump to the equivalent
place in the PDF or click in the PDF and it will jump to the appropriate place
in your source document.

In order to make synctex work on gnome-shell install:

    texlive-extra-utils gedit-plugins

## vim plugin to deal with synctex

    https://github.com/coot/atp_vim

## como instalar o latex

A COMMUNITY WIKI ANSWER, BASED ON THE OP DAVID'S COMMENTS.

The solution, which worked for the OP and is recommended by him: don't use
(deinstall) the older Ubuntu/Debian repository version and install an
up-to-date version of TeX Live, which also works on Ubuntu/Debian. Additionally
is has the advantage that you can use the TeX Live Manager tlmgr for installing
the most recent updates.

Source for download and installation: http://www.tug.org/texlive/

	``` sh
	wget -c http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz
	```

## tlmgr - TeX LIVE PACKAGE MANAGER

tlmgr is the name of the package and configuration manager included in TeX
Live. It operates completely separately from any package manager the operating
system may provide. The full documentation for tlmgr is available (updated nightly),
or you can run

    tlmgr --help

If you installed MacTeX, precede the commands below with sudo, since MacTeX installs as root.
Getting updates

Occasionally we release new versions of tlmgr. Your installed tlmgr will notice
and ask you to update it before updating anything else. The command to do this
is:

    tlmgr update --self

After any updates of tlmgr, you can then run, for instance, tlmgr update --all,
which will update (including additions and removals) all other packages in your
installation that have been changed on the server. Authors frequently release
new versions of their work to CTAN, which we propagate into TeX Live.

We maintain a separate history of tlmgr changes.
Disaster recovery

You may end up in a situation where tlmgr itself does not run due to Perl
errors, failed updates, or some other reason. If this happens, the simplest way
forward is to download and run update-tlmgr-latest.sh (below) (Unix) or
update-tlmgr-latest.exe (Windows). These are self-extracting archives which
include all the infrastructure files. Running them should restore tlmgr to
a coherent state.   http://mirror.ctan.org/systems/texlive/tlnet/update-tlmgr-latest.sh

Before running them, be sure your PATH is set to use the current TeX Live bin
directory; the archives use kpsewhich from PATH to figure out where to unpack
themselves.  Test versions

If you like living on the bleeding edge (and we greatly appreciate any such
testing), you can get the latest tlmgr from a separate repository named
‘tlcritical’. This directory is updated nightly from the development sources.
The TeX Live Utility on MacOSX has a convenient way to try this. Or you can do
it on any platform using tlmgr: the following command will show you what will
be updated, along with commentary that updates to tlmgr itself are present
(ignore the urls in that commentary).

    tlmgr --repository=ftp://tug.org/texlive/tlcritical update --list

To actually do the update, use --self instead of --list, but don't forget to
keep the explicit --repository.

If you have trouble connecting to tug.org via ftp, you can also get tlcritical
via rsync. First copy it into its own separate directory on your machine, and
then tell tlmgr to use that directory, like this:

    rsync rsync://tug.org/tlcritical/ /new/local/dir
    tlmgr --repository=/new/local/dir update --list

Another form of testing is the pretest which we do for a time before each official release.

Please email bug reports or other comments to the tex-live mailing list.

## After install wellcome

See
     /usr/local/texlive/2012/index.html
     for links to documentation.  The TeX Live web site
     contains updates and corrections: http://tug.org/texlive.

     TeX Live is a joint project of the TeX user groups around the world;
     please consider supporting it by joining the group best for you. The
     list of user groups is on the web at http://tug.org/usergroups.html.

     Add /usr/local/texlive/2012/texmf/doc/man to MANPATH, if not dynamically determined.
     Add /usr/local/texlive/2012/texmf/doc/info to INFOPATH.

     Most importantly, add /usr/local/texlive/2012/bin/i386-linux
     to your PATH for current and future sessions.

### Welcome to TeX Live!

 Summary of warning messages during installation:
   Partial download of http://linorg.usp.br/CTAN/systems/texlive/tlnet/archive/minitoc.doc.tar.xz found, removing it.

		Logfile: /usr/local/texlive/2012/install-tl.log

You can't use tlmgr (or its front-end TeX Live Utility). There is a script
called getnonfreefonts at http://www.tug.org/fonts/getnonfreefonts/ that you
have to download with the Terminal command

    curl -O http://tug.org/fonts/getnonfreefonts/install-getnonfreefonts

and then install with

    sudo texlua install-getnonfreefonts

Then you can get direction on its use by

    getnonfreefonts --help

For example, in order to install URW Garamond for all users you do

    sudo getnonfreefonts-sys garamond

Installing fonts for one user only is not recommended.

 vim: ft=markdown et sw=4 ts=4 cole=0
