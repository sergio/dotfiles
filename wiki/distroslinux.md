---
file: ~/.dotfiles/wiki/distroslinux.md
author: Sergio Araujo
Last Change: Sun, 23 Jun 2024 - 12:16:53
tags: [distros, linux, iso]
---

# Distros linux leves
+ [Burn iso easily](https://www.balena.io/etcher/)
+ https://www.ventoy.net/en/index.html -> gpl3
+ https://www.youtube.com/watch?v=7NTIbFI2ths axyl

## Non systemd distros:

+ https://github.com/axyl-os/axyl-iso (dwm keys: https://github.com/axyl-os/axyl-iso/blob/master/keybindings.md#dwmkeys)
+ https://loc-os.sourceforge.io/
+ https://nosystemd.org/
+ http://tinyurl.com/y6ykpllr
+ https://www.youtube.com/watch?v=U4nFf_UeeWE (alpine Linux)
+ [voidlinux](voidlinux.md)
+ [artix](https://artixlinux.org/) [artix](artix.md)
+ https://www.slant.co/topics/18348/~linux-distros-that-don-t-use-systemd
+ instalando o alpine linux [youtube](https://www.youtube.com/watch?v=p2OeunawIP0) [alpine](alpine.md)
+ [porteus](http://www.porteus.org/) slackware based

+ [adele](https://www.adelielinux.org/about.html)
+ [Archlinux 32bits](https://bbs.archlinux32.org/)
+ Archlinux + gnome-shell direto da Índia http://www.magpieos.net/
+ Archlinux GNU: https://www.parabola.nu/
+ Archlinux: https://www.archlinux.org/download/  https://mirror.archlinux32.org/archisos/
+ Hash Linux (pre configured i3 arch based linux) https://itsfoss.com/hash-linux-review/
+ [guix](guix-linux.md) https://www.gnu.org/software/guix/
+ [arcolinux](https://arcolinux.com/)
+ Blackarch: https://blackarch.org/index.html
+ Clear linux: https://clearlinux.org/
+ CrunchBang: https://www.bunsenlabs.org/ bunselabs
+ Debian: (mini-iso) http://ftp.debian.org/debian/dists/sid/main/installer-i386/current/images/netboot/mini.iso
+ [Deepin](deepin): https://www.deepin.org/en/
+ [Devuan](devuan.md) https://devuan.org/ no systemd
+ [artixlinux](https://artixlinux.org) Simple. Fast. Systemd-free
+ [EndevourOS based on Antergos](https://itsfoss.com/endeavouros/)
+ [EndevourOS](https://endeavouros.com/)
+ EndlessOS: https://endlessos.com/pt-br/home/
+ [Fedora](fedora)
+ FerenOS: https://ferenos.weebly.com/
+ Gentoo: https://www.gentoo.org/downloads/ [gentoo](gentoo.md)
+ [CloverOS](https://cloveros.org/) based on gentoo with no systemd
+ GNU Distros list: https://www.gnu.org/distros/free-distros.en.html
+ GNU gNewSense: http://www.gnewsense.org/Main/Download
+ https://ungleich.ch/en-us/cms/blog/2019/05/20/linux-distros-without-systemd/
+ http://www.nutyx.org/en/
+ [Hyperbola](https://www.hyperbola.info/) interview: https://itsfoss.com/hyperbola-linux-bsd/ gnu
+ [Kali Linux](https://www.kali.org/downloads/)
+ [Parabola](parabola) gnu archlinux: https://www.parabola.nu/ (it has 32bit version and no systemd)
+ Point Linux: http://pointlinux.org/
+ PopOS: https://system76.com/pop
+ Proteus Linux: http://www.porteus.org/
+ [RebornOS - renasce o Antergos](https://rebornos.org/)
+ Revenge OS: https://sourceforge.net/projects/obrevenge/files/
+ Sabayon gentoo based: https://www.sabayon.org/
+ [MocaccinoOS](mocaccinoOS.md) based on sabayon https://www.mocaccino.org/ [video sobre](https://youtu.be/VQ3XKz3SO90)
+ Sabayon linux: https://www.sabayon.org/
+ Slax :https://www.slax.org/
+ Sparklinux 32bits: https://sparkylinux.org/about/
+ Suse Linux: https://en.opensuse.org/openSUSE:Tumbleweed_installation
+ [voidlinux](voidlinux) https://voidlinux.org/

## Ubuntu minimal install with gnome vanilla
+ https://help.ubuntu.com/community/Installation/MinimalCD

Após a instalação do sistema, instale o ambiente gnome com o comando:

		sudo apt install gnome-session gdm3 nautilus gnome-terminal gnome-software gedit firefox

## voidlinux → https://www.voidlinux.eu/

NOTE: It has not systemd unless you try really hard to install it.

Void is a general purpose operating system, based on the monolithic Linux®
kernel. Its package system allows you to quickly install, update and remove
software; software is provided in binary packages or can be built directly
from sources with the help of the XBPS source packages collection.

It is available for the Intel x86®, ARM® and MIPS® processor architectures;
Software packages can be built natively or cross compiling through the XBPS
source packages collection.

## Arch based linux distros

+ [Archcraft](https://archcraft-os.github.io/)
+ [Download artcraft](https://archcraft.io/download.html#second)

# vim: ft=markdown et sw=4 ts=4 cole=0
