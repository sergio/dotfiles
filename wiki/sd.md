---
File: sd.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: ago 05, 2020 - 16:32
tags: [tools, sed]
---

#  sd.md intro:
+ https://github.com/chmln/sd

stagsd is an intuitive find & replace command-line tool, it is an alternative
to sed, the built-in command-line program in Unix/Linux for parsing and
transforming text. sd has simpler syntax for replacing all occurrences and it
uses the convenient regex syntax that you already know from JavaScript and
Python. sd is also 2x-11x faster than sed.

sed is a programmable text editor, with search and replace being a common use
case. In that light, sd is more like tr, but on steroids.


