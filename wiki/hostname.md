---
file: hostname.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# retorna o nome da maquina atual

    echo "o nome do computador é `hostname`"

## retornar o IP (show locall ip address)

    hostname --ip-addresses

Command alteranativo

    ip addr show wlan0

## Changing hostiname without resarting the system
+ https://askubuntu.com/questions/87665/how-do-i-change-the-hostname-without-a-restart

    sudo hostname newname

	hostnamectl set-hostname 'new-hostname'
	source: https://askubuntu.com/a/704208
