---
file: trash-cli.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [utils, trash, gio, trash-cli]
---

## Most linux sistems bring "gio" as sucessor of gvfsd-trash, so:
We can define an alias so we can use trash normally

    (( $#commands[(I)(trash|gio)] == 2 )) && alias trash='gio trash'

It’s important to remember that gio trash sends files to ~/.local/share/Trash/files instead of ~/.trash, used on cPanel managed servers.

The .local/share/Trash directory includes three directories:

    info stores deleted files’ original location and deletion time
    files stores deleted files until it’s emptied
    expunged may temporarily hold recently emptied trash

    gio trash --empty
    gio list trash://
    trash file

    trash-restore (it will ask which file to restore)

    gio trash --list

You can also permanently remove files with the gio remove command:

    gio remove [FILE1] [FILE2]

# trash-cli.md
+ https://github.com/andreafrancia/trash-cli

    pip install trash-cli

Command line interface to the freedesktop.org trashcan.

Add alias `rm=trash` to your `.zshrc/.bashrc` to reduce typing & safely trash
files: `rm unicorn.png`.

## Usage
Trash a file:

    trash-put foo

## Trash files location

    $HOME/.local/share/Trash

## List trashed files:

``` markdown
$ trash-list
2008-06-01 10:30:48 /home/andrea/bar
2008-06-02 21:50:41 /home/andrea/bar
2008-06-23 21:50:49 /home/andrea/foo
```

## Search for a file in the trashcan:

``` markdown
$ trash-list | grep foo
2007-08-30 12:36:00 /home/andrea/foo
2007-08-30 12:39:41 /home/andrea/foo
```

## Restore a trashed file:
``` markdown
$ trash-restore
0 2007-08-30 12:36:00 /home/andrea/foo
1 2007-08-30 12:39:41 /home/andrea/bar
2 2007-08-30 12:39:41 /home/andrea/bar2
3 2007-08-30 12:39:41 /home/andrea/foo2
4 2007-08-30 12:39:41 /home/andrea/foo
What file to restore [0..4]: 4
$ ls foo
foo
```

## Remove all files from the trashcan:

    trash-empty

## Remove only the files that have been deleted more than <days> ago:

    trash-empty <days>
    trash-empty 10

## Example:

``` markdown
$ date
Tue Feb 19 20:26:52 CET 2008
$ trash-list
2008-02-19 20:11:34 /home/einar/today
2008-02-18 20:11:34 /home/einar/yesterday
2008-02-10 20:11:34 /home/einar/last_week
$ trash-empty 7
$ trash-list
2008-02-19 20:11:34 /home/einar/today
2008-02-18 20:11:34 /home/einar/yesterday
$ trash-empty 1
$ trash-list
2008-02-19 20:11:34 /home/einar/today
```

## Remove only files matching a pattern:

    trash-rm \*.o

