---
File: /home/sergio/.dotfiles/wiki/shift.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [shift, shell]
 vim:ft=markdown
---

Shift is a builtin command in bash which after getting executed, shifts/move the command line arguments to one position left. The first argument is lost after using shift command. This command takes only one integer as an argument. This command is useful when you want to get rid of the command line arguments which are not needed after parsing them.

