---
Filename: xfd.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [fonts, desktop]
 vim:ft=markdown
---

## xfd Display all the characters in an X font
+ https://www.reddit.com/r/suckless/comments/pob2s6/comment/hcx2t0n/

    xfd -fa "IcoMoon Free"
