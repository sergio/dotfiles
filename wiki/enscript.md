Filename: enscript.md
Last Change: Wed, 21 Dec 2022 - 07:55:52
tags: [pdf, toos, postscript]

## Intro:

enscript Converts text to Postscript, HTML or RTF with syntax highlighting

## Convert txt to pdf:

    enscript my_text_file.txt -o - | ps2pdf - output.pdf

