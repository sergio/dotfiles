---
File: ~/.dotfiles/wiki/scoop.md
Last Change: Thu, Nov 2024/11/28 - 16:08:08
tags: [windows, linux, toools]
---

#  Intro

Scoop is a command-line package manager for Microsoft Windows that allows users
to install, update, and remove software packages: 

Features
Scoop automates many tasks, including: 

Downloading and installing programs and their dependencies 

Eliminating permission popup windows 
Hiding GUI wizard-style installers 
Preventing PATH pollution 

Avoiding unexpected side effects from installing and uninstalling programs 
Performing all the extra setup steps to get a working program 

Buckets

Scoop organizes apps into collections called buckets, which are Git repositories
that contain JSON app manifests. The main bucket is bundled with Scoop and is
always available as the primary source for installing apps. Online repository
Scoop's online repository at http://scoop.sh has a large selection of popular
command-line and graphical software

## Add pbcopy and pbpaste to wsl
+ [Reference](https://superuser.com/a/1736148/45032)

```she
$ scoop bucket add extras
$ scoop install pasteboard
```


