Filename: xz.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [compress, uncompress, unzip, zip]
 vim:ft=markdown


## uncompress tar.xz
+ https://www.cyberciti.biz/faq/how-to-extract-tar-xz-files-in-linux-and-unzip-all-files/

    unxz archive.xz
    tar --xz -xf archive.txz
