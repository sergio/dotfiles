---
File: /home/sergio/.dotfiles/wiki/clipmenu.md
Last Change: Sun, 09 Jun 2024 - 19:56:40
tags: [xclip, xsel, clipboard, x11, tools, utils]
---

## Intro:
https://www.youtube.com/watch?v=nfPSpkqv0UM

## Set the environment variables:

You have also to have "clipmenud" running, in my case (bspwm)

    pgrep -x clipmenud > /dev/null || clipmenud &

```sh
# clipmenu options
(( $+commands[clipmenu] )) && {
    export CM_LAUNCHER=rofi
    export CM_DIR="${XDG_RUNTIME_DIR-"${TMPDIR-/tmp}"}"
    export CM_MAX_CLIPS=1000
    export CM_SELECTIONS="clipboard primary"
}
```


