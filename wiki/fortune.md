---
Last Change: Mon, 19 Feb 2024 - 14:05:58
Filename: fortune.md
---

https://www.shlomifish.org/humour/fortunes/
https://metacpan.org/dist/PerlPowerTools/view/bin/fortune

## print a random, hopefully interesting, adage

algumas opções que podem ser utilizadas com o Fortune:

``` txt
'–a' ....... all sources
'–n 100' ... number of chars
'–o' ....... offensive citatios
'–s' ....... only phrases
```

+ [strfile repo](https://github.com/HubTou/strfile)
+ [my fork](https://github.com/voyeg3r/strfile)

O pacote fortune está instalado nos dotfiles, na pasta bin, os programa
strfile pode ser instalado com:

```sh
pip install pnu-strfile
```

## How to prepare you quotes files

+ [how to](https://askubuntu.com/a/1022119)

To get you own custom fortune cookies first create a file and add the texts hat you want to be shown as a fortune.
In your text editor paste the text (string) you want to be show and between each string to be displayed there has to be a %

assuming we are creating a quotes fortune,with filename quotes

sudo nano quotes

paste in your text and edit like this

``` txt
Limits exist only in your mind!
%
"Maybe this world is another planet’s hell."
— Aldous Huxley
%
"Choices are the hinges of destiny." —Edwin Markham
%
“Sometimes it’s the smallest decisions that can 
change your life forever." —Keri Russell
%
“We are the creative force of our life, and through our 
own decisions rather than our conditions, if we carefully
learn to do certain things, we can accomplish
those goals.” —Stephen Covey
```

save the file and create a dat file:

    strfile -c % quotes quotes.dat

This creates a .dat file for your cookie file, which contains a header structure and a table of file offsets for each group of lines. This allows random access of the strings.

to see your fortune, do:

    fortune quotes

To make your cookies appear as a standard fortune cookie when you simply type fortune, add quotes and quotes.dat to `/usr/share/games/fortunes/`
If you want to add or subtract any string from your custom fortune cookie file,you will need to to run the `strfile -c % quotes quotes.dat` to create a new dat file again

    fortune -h
    fortune -sa ~/.dotfiles/fortunes

