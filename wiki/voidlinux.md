---
File: voidlinux.md
Last Change: Sat, 18 Jan 2025 - 00:05:15
tags: [distros, linux, voidlinux]
---

# References

+ [Post install](https://kennydodrill.net/blog/stable-void-linux-setup-guide/#post-installation)
+ [Nice themes](https://github.com/addy-dclxvi/almighty-dotfiles)
+ [Notes on setting up a comfortable desktop](https://kkga.me/notes/void-linux/)
+ [User services with runit](https://www.jonatkinson.co.uk/posts/user-services-with-runit-on-void-linux/)
+ [runit-user-session](https://github.com/AFriemann/runit-user-session)
+ [After install](https://gist.github.com/bastomiadi/abf27618341fc561735adfb17e586916)
+ [A lot of scripts](https://github.com/jdpedersen1?tab=repositories)

## Basic void installation

+ [Basic void installation](https://www.youtube.com/watch?v=Qdkng68598o)

## update system

NOTE: If you do not have doas install opendoas and set it

```sh
chsh -s $(which zsh) $(whoami)
doas xbps-install -Sy zsh-completions net-tools bind-utils dos2unix git curl wget
doas xbps-install -Sy netcat

doas xbps-install -Sy void-repo{,-nonfree}
doas xbps-install -Suv
```

## Better font rendering for firefox

+ [More at archlinux wiki](https://wiki.archlinux.org/title/Font_configuration)

```sh
sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/ 
sudo xbps-reconfigure -f fontconfig 
```

## Ferramenta para criar pendrive bootavel (iso)

+ [index.html](https://www.ventoy.net/en/index.html)
+ [VENTOY CRIE PENDRIVE MULTBOOT ](https://www.youtube.com/watch?v=NOZmSTT7jKM)
+ [Como fazer Pen Drive Bootável de Linux e Windows - Ventoy](https://youtu.be/11CkqZQ3scE) (video do diolinux)

Create usb stick iso loader (livecd) - read [ventoy](ventoy.md)

## Adicionar novos repositórios

```sh
sudo xbps-install -Sy void-repo{,-nonfree}
```

## Essential tools

```sh
sudo xbps-install -Sy git curl wget xclip neovim fzf zoxide aria2 zsh{,-completions}
chsh -s $(which zsh) $(whoami)
```

## Compression tools

```sh
compression_tools() {
echo "Installing compression tools..."
sudo xbps-install -Sy zip unzip unrar xarchiver file-roller pigz lbzip2 pbzip2 xz p7zip p7zip-unrar 7zip 7zip-unrar lzip lzop zstd lrzip lz4 atool unace unar
}
```

## Como ligar o LED do teclado no Linux

source: https://wendellast2a.medium.com/como-ligar-o-led-do-teclado-no-linux-0d3acd66d053

```sh
sudo xbps-install brightnessctl

brightnessctl -l
```
Uma vez que você tenha identificado o número do input do Scroll Lock, utilize o comando 
abaixo para ajustar o brilho. Substitua [2] pelo número do input que você identificou

```sh
brightnessctl --device='input2::scrolllock' set 1
```

## install packages from source xbps-src:

+ https://github.com/void-linux/void-packages#chroot-methods
+ https://github.com/void-linux/void-packages

```sh
git clone https://github.com/void-linux/void-packages.git
cd void-packages
./xbps-src binary-bootstrap
```

  echo XBPS_ALLOW_RESTRICTED=yes >> etc/conf

Build a package by specifying the pkg target and the package name:

  ./xbps-src pkg <package_name>

## repositorio extra para anydesk:

  Tem no nosso mirror, em extras:
  repository=http://void.chililinux.com/voidlinux/extras

## luakit:

[Luakit](luakit.md) is a web browser aimed at advanced users, developers,
users who want to better fix Vim/Neovim commands.

## primeiro atualize tudo:

     sudo xbps-install -u xbps && sudo xbps-install -Su
     sudo xbps-install -Sy bash-completion

## Setting aliases for fast package manangement:

```sh
alias upgrade='(){echo "Running -> doas xbps-install -Suv";doas xbps-install -Suv;}'
alias pu='(){echo "(update) Running -> doas xbps-install -Suv";doas xbps-install -Suv;}'
alias pi='doas xbps-install -Sy'
alias pq='doas xbps-query -Rs'
alias pr='doas xbps-remove -R'
```

pi git zsh && chsh -s $(which zsh) $(whoami)
pi dunst # notification system
pi zoxide

```bash
setrepo(){
    git clone --recursive -j8 git@bitbucket.org:sergio/dotfiles.git .dotfiles
    cd ~/.dotfiles && git fetch --recurse-submodules --jobs=8
    ssh -T git@github.com

    var=`echo 3gmail.com@ | sed 's,\(^3\)\(gmail\.com\)\(\@\),voyeg\1r\3\2,g'`
    git config --global user.name voyeg3r
    git config --global merge.tool vimdiff
    git config --global user.email ${var}
    git config --global alias.last 'log -1 HEAD'
    git config --global credential.helper cache ssh://git@bitbucket.org/sergio/dotfaster.git/wiki
    git config --global push.default simple
    git config --global credential.helper 'cache --timeout=3600'
    git config --global alias.hist 'log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short'
    git config --global core.autocrlf input
    git config --global core.eol lf
    git config --global core.pager less -FRSX
    git config --global init.defaultbranch main
    git config --global color.branch auto
    git config --global color.diff auto
    git config --global color.interactive auto
    git config --global color.status auto
    git config --global color.ui auto

    ln -sfvn ~/.dotfiles/zsh/zshenv ~/.zshenv
    ln -sfvn ~/.dotfiles/bash/bashrc ~/.bashrc
    ln -sfvn ~/.dotfiles/bash/inputrc ~/.inputrc
    ln -sfvn ~/.dotfiles/bash/bash_profile ~/.bash_profile
    ln -sfvn ~/.dotfiles/xinitrc ~/.xinitrc
    ln -sfvn ~/.dotfiles/rofi ~/.config
    ln -sfvn ~/.dotfiles/polybar ~/.config
    ln -sfvn ~/.dotfiles/bspwm ~/.config
    ln -sfvn ~/.dotfiles/sxhkd ~/.config
    ln -sfvn ~/.dotfiles/Xresources ~/.Xresources
    ln -sfvn ~/.dotfiles/detox/detoxrc ~/.detoxrc
    ln -sfvn /home/sergio/.dotfiles/mpv/mpv.conf ~/.config/mpv/mpv.conf
    ln -sfvn ~/.dotfiles/inkscape ~/.config/inkscape/templates
    ln -sfvn ~/.dotfiles/agignore ~/.agignore
    ln -sfvn ~/.dotfiles/gemrc ~/.gemrc
    ln -sfvn ~/.dotfiles/curl/curlrc ~/.curlrc
    ln -sfvn ~/.dotfiles/fontconfig ~/.config/fontconfig
    ln -sfvn ~/.dotfiles/bcrc ~/.config/bcrc
    ln -sfvn ~/.dotfiles/aria2 ~/.config/aria2
    [ -d ~/.config/youtube-dl ] || mkdir -p ~/.config/youtube-dl
    ln -svfn ~/.dotfiles/youtube-dl.config ~/.config/youtube-dl/config
    ln -sfvn ~/.dotfiles/yt-dlp ~/.config
    ln -sfvn ~/.dotfiles/backgrounds $"{XDG_PICTURES_DIR:-~/img}"/backgrounds
    ln -sfvn ~/.dotfiles/awesome ~/.config

    [[ ! -d ~/.fonts ]] && {
      unzip ~/.dotfiles/fonts.zip -d ~/.fonts
      fc-cache -fv
    }

} && clonerepo
```

## zip and unzip

```sh
doas xbps-install -Sy \ zip unzip unrar \ xarchiver file-roller \ pigz lbzip2 pbzip2 xz \
p7zip p7zip-unrar 7zip 7zip-unrar \ lzip lzop zstd lrzip lz4 \
atool unace unar

(
cd /usr/local/bin || { echo "Failed to cd to /usr/local/bin"; exit 1; }

linktools() {
    # lbzip2 (parallel bzip2)
    doas ln -sf /usr/bin/lbzip2 bzip2
    doas ln -sf /usr/bin/lbzip2 bunzip2
    doas ln -sf /usr/bin/lbzip2 bzcat

    # pigz (parallel gzip)
    doas ln -sf /usr/bin/pigz gzip
    doas ln -sf /usr/bin/pigz gunzip
    doas ln -sf /usr/bin/pigz gzcat
    doas ln -sf /usr/bin/pigz zcat

    # xz (optional, if you want to replace xz with a parallel version in the future)
    # doas ln -sf /usr/bin/pixz xz

    # zstd (modern compression tool)
    doas ln -sf /usr/bin/zstd zstdcat
    doas ln -sf /usr/bin/unzstd zstdcat

    # lz4 (fast compression tool)
    doas ln -sf /usr/bin/lz4 lz4cat
    doas ln -sf /usr/bin/unlz4 lz4cat

    echo "Symbolic links created successfully."
}

linktools
)
```

## comprimir executáveis

    doas xbps-install -Sy upx

## search and fuzzy search

    doas xbps-install -Sy the_silver_searcher fzf fd mlocate

## Screen capture:

screenkey shows each key you press when recording your screen

    doas xbps-install -Sy screenkey
    doas xbps-install -Sy ttyrec

## zram (memory optimization):

+ https://github.com/atweiden/zramen

  git clone https://github.com/atweiden/zramen.git
  cd zramem
  doas install -Dm 755 zramen -t /usr/bin
  doas cp -a sv/zramen /etc/sv
  doas ln -s /etc/sv/zramen /var/service

  doas xpbs-install -Sy preload

## Temas de cursor

    doas xbps-install -s xcursor-themes

## lock screen

+ https://dev.to/ccoveille/lock-screen-notifier-4o1h

  doas xbps-install -S xsecurelock betterlockscreen slock xautolock

## gnu stow

    doas xbps-install -S stow

## Record audio

+ https://askubuntu.com/a/850174/3798

Install the required packages:

    doas xbps-install pulseaudio{,-utils} opus-tools
    doas ln -s /etc/sv/pulseaudio /var/service

Edit your .bashrc file:

    nano ~/.bashrc

Copy the line below and append it to the end of your ~.bashrc:

    alias recaudio="parec --monitor-stream="$(pacmd list-sink-inputs | awk '$1 == "index:" {print $2}')" | opusenc --raw - $(xdg-user-dir MUSIC)/recording-$(date +"%F_%H-%M-%S").opus"

Save the file by pressing Ctrl+X (to close nano), then Y (for "Yes") and Enter (to confirm the file name).

Now load the new ~/.bashrc (or simply open a new terminal):

    source $HOME/.bashrc

Then try, just type

    recaudio

It will create a file in your Music home folder called recording-[DATE].opus.

## Install and Run Python Applications in Isolated Environments

    doas xbps-install -Sy python3-pip
    python3 -m pip install --user pipx
    python3 -m pipx ensurepath

    Install "(lookatme)[lookatme.md]" presentation tool
    pipx install lookatme

## blur wallpaper

+ https://www.reddit.com/r/unixporn/comments/arlsw9

  pipx install blurwal

## Enable bash completion on root user

+ https://www.youtube.com/watch?v=Z15vjwmHvCU

Change shell to bash and enable bash-completion

    doas su -
    chsh
    doas xbps-install -S bash-completion

The root user in voidlinux uses `/bin/sh`, so we must change it to `/bin/bash` Just run: `chsh`

## enable your keyboard settings

xfce4 on voidlinux uses lxdm as login manager in the file `vim /etc/lxdm/Xsession` we can see:

    [ -f /etc/profile ] && . /etc/profile
    [ -f ~/.profile ] && . ~/.profile
    [ -f /etc/xprofile ] && . /etc/xprofile
    [ -f ~/.xprofile ] && . ~/.xprofile

Which means if we want to set our 'sextxkbmap' we can make a symlink to ~/.xprofile or ~/.profile, something like:

    ln -sfvn ~/.dotfiles/xinitrc ~/.xprofile

There is one caveat: At the end of my xinitrc I have `exec bspwm`, the
solution was creating an if statement:

    if [ $XDG_CURRENT_DESKTOP == "bspwm" ]; then
        exec bspwm
    fi

In our ~/.xinitrc we can use something like:

    # http://blog.ssokolow.com/archives/2011/12/24/getting-your-way-with-setxkbmap/
    setxkbmap -model abnt2 \
        -layout br -variant abnt2 \
        -option \
        -option "lv3:ralt_switch" \
        -option "terminate:ctrl_alt_bksp" \
        -option "caps:escape"

setxkbmap -model abnt2 -layout br -variant abnt2 &

## Gerenciador de pacotes gráficos para o xbps:

    doas xbps-install -S octoxbps

## bspwm

    doas xbps-install -Sy bspwm polybar rofi{,-calc} feh wmctrl xdo xprop slop xtermcontrol xwininfo xdotool tabbed

## adding nonfree and other repositories:

    doas xbps-install -Sy void-repo-nonfree

## changing mirrors

+ https://www.youtube.com/watch?v=0E4-Lx1kML4
+ https://docs.voidlinux.org/xbps/repositories/mirrors/changing.html
+ https://docs.voidlinux.org/xbps/repositories/mirrors/index.html

Each repository has a file defining the URL for the mirror used. For official repositories, these files are installed by the package manager in /usr/share/xbps.d, but if duplicate files are found in /etc/xbps.d, those values are used instead.

To modify mirror URLs cleanly, copy all the repository configuration files to /etc/xbps.d and change the URLs in each copied repository file.

```sh
# make sure you have opendoas installed, otherwise use sudo
setvoidrepo(){
  newrepo="https://voidlinux.com.br/repo/" # Ouro Preto Brazil
  [ ! -d /etc/xbps.d ] && doas mkdir -p /etc/xbps.d
  doas cp -f /usr/share/xbps.d/*-repository-*.conf /etc/xbps.d/
  doas sed -i "s,https.*org/,${newrepo},g" /etc/xbps.d/*-repository-*.conf
} && setvoidrepo()
```

After changing the URLs, you must synchronize xbps with the new mirrors:

    doas xbps-install -S

You should see the new repository URLs while synchronizing. You can also use xbps-query to verify the repository URLs, but only after they have been synchronized:

    $ xbps-query -L
    9970 https://alpha.de.repo.voidlinux.org/current (RSA signed)
    27 https://alpha.de.repo.voidlinux.org/current/multilib/nonfree (RSA signed)
    4230 https://alpha.de.repo.voidlinux.org/current/multilib (RSA signed)
    47 https://alpha.de.repo.voidlinux.org/current/nonfree (RSA signed)
    5368 https://alpha.de.repo.voidlinux.org/current/debug (RSA signed)

Remember that repositories added afterwards will also need to be changed, or they will use the default mirror.

## xfce no sound:

+ https://www.reddit.com/r/voidlinux/comments/lp5w87/how_i_got_pipewire_working_on_void_as_a_pulse/

  doas xbps-install -Sy pulsemixer pamixer

I using pipewire (pulseaudio)

doas xbps-install -Sy pulsemixer pipewire wireplumber alsa-pipewire pamixer pulseaudio alsa-utils alsa-plugins-pulseaudio

## pipewire instalation

```sh
 sudo xbps-install -S pipewire libspa-bluetooth
 # add service pipewire
sudo ln -s /etc/sv/pipewire /var/service/
 # add service pipewire-pulse
sudo ln -s /etc/sv/pipewire-pulse /var/service/
 # add user group
sudo usermod -aG _pipewire,pulse,pulse-access $USER
 #logout your session then login again
 #check your pipewire is activated using pactl
pactl info
```

## command line music player

    doas xbps-install -Sy clyrics cmus cmus-cdio cmus-faad cmus-ffmpeg cmus-flac cmus-jack cmus-libao cmus-modplug cmus-mpc cmus-opus cmus-oss cmus-pulseaudio cmus-sndio cmus-wavpack cmusfm

## New and beautiful start apps menu

    doas xbps-install xfce4-whiskermenu-plugin

## Converting deb packages to xbps

+ https://edpsblog.wordpress.com/2020/11/19/dica-convertendo-pacotes-deb-para-xbps/

Começaremos por baixar o utilitário xdeb, que será o reponsável pela conversão dos pacotes:

```markdown
wget -nv https://github.com/toluschr/xdeb/releases/download/1.2/xdeb chmod +x xdeb mv -v xdeb bin/

# ou

sudo mv -v xdeb /usr/local/bin/
sudo xbps-install -S binutils
```

## A safier sudo

+ https://github.com/slicer69/doas [doas](doas.md)

```markdown
opendoas-6.8.1_1 Portable OpenBSD doas to execute commands as another user
```

## website: https://voidlinux.org/

+ https://docs.voidlinux.org/
- xbps → https://voidlinux.org/usage/xbps/#xbps-install1
- void forun → https://forum.voidlinux.org/
+ https://voidlinux.org/usage/runit/

## lua packge management

    # also needed for neovim
    doas xbps-install -Sy luarocks ninja StyLua
    doas npm install --location=global neovim

## flash plugin

    doas xbps-install -Sy freshplayerplugin

## voidrice (larbs)

+ https://larbs.xyz/

  curl -LO larbs.xyz/larbs.sh bash larbs.sh

## docker -> see [docker](docker.md)

## tldr commands (you need to install nodejs)

    doas npm install --location=global tldr
    doas xbps-install -Sy cheat

## checking logs and listing recent installed packages

+ http://bit.ly/38tdsDZ

xbps logs to syslog. You need to enable a syslog daemon like socklog:

+ https://docs.voidlinux.org/config/services/logging.html.

If socklog-void is installed and enabled, xbps logs end up in /var/log/socklog/xbps/

## Install comic books reader

    doas xbps-install -S mcomix

## Installing vis editor

    doas xbps-install -S vis

## installing exa (replacement for ls)

+ http://bit.ly/2W4MdwU

  (( $+commands[exa] )) && alias ls=exa

## cron

See also ~/.dotfiles/algorithm/shell/bash/randwallpaper.sh

    doas xbps-install -Sy cronie at
    ln -s /etc/sv/crond /var/service/

To change your wallpaper automatically visit [cron](cron.md)

## install emacs

    doas xbps-install -Sy emacs

## problemas com opengl: (Erro ao iniciar Steam no Void Linux [Resolvido])

+ https://bit.ly/3Sx2pSr

  "OpenGL GLX extension not supported by display".

  doas xbps-install -Sy intel-video-accel mesa-dri

## Installing neovim (nvim)

+ https://github.com/xajler/voidlinux-i3

Install patched fonts

    Download this font pack:
    https://www.mediafire.com/file/ig6b0637h91f23g/fonts.zip/file

```bash
installneovim(){
	echo "setxkbmap -model abnt2 -layout br" >> ~/.bashrc
    echo "export PATH=$HOME/.local/bin:$PATH" >> ~/.bashrc
    doas xbps-install -Sy xclip  dunst picom libva-glx mesa-intel-dri xcompmgr scrot escrotum fzf fzy git lazygit bash-completion glow ripgrep
    doas xbps-install -Sy neovim{,-remote} python3-neovim python-neovim cargo
	doas xbps-install -Sy python3-pip python-pip base-devel python3-devel python-devel
    doas xbps-install -S python3-distutils-extra # needed for ulaucher
	doas xbps-install -Sy libressl{,-devel} libtls19
    doas xbps-install -Sy gcc lua-devel
	pip3 install --user pynvim
    pip2 install --user pynvim
    doas cargo install tree-sitter-cli
    doas npm i --location=global neovim #node support
    #doas xbps-install -y yarn
    #yarn add prettier prettier-eslint prettier-eslint-cli -D
    doas npm install --location=global prettier
    doas xbps-install -S black editorconfig
    echo '"@dungsil/prettier-config"' > .prettierrc
} && installneovim
```

You can also install [fontpreview](https://github.com/oliverlew/fontpreview-ueberzug)

## Use devour

+ https://github.com/salman-abedin/devour
  Devour hides your current window before launching an external program and unhides it after quitting.

  git clone https://github.com/salman-abedin/devour.git && cd devour && sudo make install

## para o pyenv

    doas xbps-install -Sy base-devel bzip2-devel openssl openssl-devel readline readline-devel sqlite-devel xz zlib zlib-devel

    $ curl https://pyenv.run | bash
    $ curl https://pyenv.run | zsh

To list the available versions:

    pyenv install -l

## wallpaper collection

+ https://wallpaperscraft.com/catalog/art
+ https://www.artstation.com/aenamiart
+ https://github.com/dave-cao/Bash_Scripts

  git clone https://github.com/linuxdotexe/nordic-wallpapers

## ranger dependencies (file explorer):

Needed to use pdftoppm

    xi poppler{,-utils,-devel}

## manipulando pdf

+ https://www.devmedia.com.br/manipulando-pdf-com-ghostscript/34424

  pi ghostscript

## Install ulauncher a fuzzy finder app finder

    doas xbps-install -Sy ulauncher

## listing installed python packages throught pip

```markdown
pip3 list -o --format columns

Package Version Latest Type
+--

anytree 2.6.0 2.7.3 wheel lxml 4.4.1 4.4.2 wheel
```

## Void linux XBPS broke: certificate verification failed

```sh
Updating (some url) ...
Certificate verification failed for (some other url)
SSL_connect returned 1
ERROR: failed to fetch file (url of first line): Operation not permitted.
```

solution:

```sh
export SSL_NO_VERIFY_PEER=1
xbps-install ...
```

## Installing old voidlinux due boot error in the new version

I have copied the `/usr/share/xbps.d` from other machine to my `/etc/xbps.d`

Then I ran the bellow command in order to update my system:

    doas xbps-install -Su

## Adding a new user

    # useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input voiduser

## Install txt2tags

    pip install --user txt2tags

## install gnome desktop

+ https://www.reddit.com/r/voidlinux/comments/elwur3/ (fix login issue)

  doas xbps-install -Sy gnome{,-apps} avahi cups
  doas ln -s /etc/sv/dbus /var/service
  xbps-install -Sy gnome-apps

  ln -s /etc/sv/NetworkManager /var/service # Make sure other Networkmanagers are disabled!
  ln -s /etc/sv/avahi-daemon /var/service # zeroconf support
  ln -s /etc/sv/bluetoothd /var/service # Bluetooth Support
  ln -s /etc/sv/cupsd /var/service # Printer support
  ln -s /etc/sv/gdm /var/service # Make sure other display managers such as LXDM are disabled

## Installing ghome-shell addons

    doas xbps-install -Sy chrome-gnome-shell

## Castero command line podcast client radio client

+ https://www.youtube.com/watch?v=N_Wb-PQpjek

  doas xbps-install -Sy libressl-devel doas pip3 install castero gradio

  pip3 install --user pyradio pyradio --play

## Install qtile desktop on voidlinux

Install openbox:
+ https://terminalroot.com.br/2021/12/os-12-melhores-temas-para-seu-tint2.html

  sudo xbps-install -Sy openbox tint2

```markdown
sudo cat <<-EOF >> /usr/share/xsessions/qtile.desktop
[Desktop Entry]
Encoding=UTF-8
Name=Openbox
Comment=Log in using the Openbox window manager (without a session manager)
Exec=/usr/bin/qtile
TryExec=/usr/bin/qtile Icon=openbox.png
Type=XSession
EOF
```

## missing ifconfig install net-tools

  doas xbps-install -Sy net-tools bind-utils lsof

## configuração de teclado

+ https://www.vivaolinux.com.br/dica/Problemas-com-acentuacao-teclado-desconfigurado-Ubuntu

  setxkbmap -model abnt2 -layout br
  setxkbmap -model abnt2 -layout br -variant abnt2

De forma permanente (esta solução peguei no canal do telegram voidlinux-br):

```sh
sudo nvim /etc/X11/xorg.conf.d/00-keyboard.conf

Section "InputClass"
        Identifier "keyboard default"
        MatchIsKeyboard "yes"
        Option   "XkbLayout" "br"
        Option   "XkbVariant" "abnt2"
        Option "AutoRepeat" "delay rate"
EndSection
```

## update issue fixing

+ https://inbox.vuxu.org/voidlinux/47fd85ef-74cd-448b-9240-218936050ddd@googlegroups.com/

When I try to update my system with doas xbps-install -Syu I get this error: Transaction aborted due to unresolved
shlibs.

### Solution

The xbps 0.44 shlib code skips packages that have the "transaction" obj set to "remove". If you've been using xbps
for a while and never ran xbps-pkgdb(8) there could be some packages with that obsolete obj.

Solution:

    for f in $(xbps-query -s '' -p transaction|cut -d ':' -f1); do doas xbps-pkgdb $f; done

## touch to click on touchpad

+ https://askubuntu.com/a/700018/3798

  gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true

My new solution to this issue

    synclient TapButton1=1 TapButton2=3 TapButton3=2

```markdown
Then create the autostart script: `~/bin/autostart` with your bash contents:

    #!/bin/bash
    # Execute bash script below
    synclient TapButton1=1 TapButton2=3 TapButton3=2

Make sure that `~/bin/autostart` is executable

In my case I wanted to fix a touchpad feature called tap to click so I have created the following shortcut:

cat <<-EOF > ~/.config/autostart/autostart.desktop

# ~/.config/autostart/autostart.desktop

[Desktop Entry]
Type=Application
Terminal=true
Name=fix-tap-to-click
Icon=/home/sergio/img/icons/terminal.svg
Exec=/home/sergio/.dotfiles/bin/autostart
EOF
```

Tap to click using xorg.conf.d:

```sh
# /etc/X11/xorg.conf.d/30-touchpad.conf
# Touchpad configuration

Section "InputClass"
    Identifier "touchpad catchall"
    Driver "libinput"
    Option "Tapping" "on"
EndSection
```

## developer tools (dev tools)

+ https://www.reddit.com/r/voidlinux/comments/drvlui/comment/f6lcblg/

  doas xbps-install -Sy cmake base-devel gcc

## Windowmaker clock

    doas xbps-install -Sy wmclock

## grammar and spell checker

+ https://languagetool.org/download/

* https://github.com/dpelle/vim-LanguageTool

  xbps-query -Rs LanguageTool

## RSS reader - feeds

    flatpack install https://dl.flathub.org/repo/appstream/org.gabmus.gfeeds.flatpakref

## set the clock automatically via ntpd

+ https://wiki.voidlinux.org/NTP#Installation

  doas xbps-install -S chronyd
  doas ln -s /etc/sv/chronyd /var/service/

When you have shlibs (Unresolved shlibs, cannot update) error try:

    for f in $(xbps-query -s '' -p transaction|cut -d ':' -f1); do doas xbps-pkgdb $f; done

## e-book reader

    doas xbps-install -Syu bookworm
    doas xbps-install -S foliate

    # e-book converter (veja também o visualizador evince)
    doas xbps-install -S calibre

    doas xbps-install -Sy zathura zathura-djvu zathura-pdf-mupdf zathura-ps

## R programming Language

    doas xbps-install -Sy R rstudio

## pgp

    doas xbps-install -S seahorse{,-nautilus}

## nautilus fork

+ https://crunchbang.org/forums/viewtopic.php?id=30373

  alias pi='doas xbps-install -Sy'
  pi nemo{,-extensions,-fileroller,-preview,-terminal} thunar{,-volman}

## install emacs

    doas xbps-install -Sy emacs{,-gtk3}

## Set date and time automatically

    doas xbps-install -Sy ntp

## Install gnu icecat

- - https://ftp.gnu.org/gnu/gnuzilla/60.7.0/ (gnu icecat)

## Remove packages

    doas xbps-remove firefox-esr

and add your brand new firefox

    doas xbps-install -Sy firefox firejail xpra surf tabbed

    # lite browser based on vim shortcuts
    doas xbps-install -Sy qutebrowser otter-browser

# Surf browser

    doas xbps-install -Sy gdk-pixbuf-{devel,xlib} webkit2gtk-devel surf ccls

## albert launcher

    doas xbps-install -Sy albert

## install and set locate

    doas xbps-install -Sy mlocate
    doas xbps-alternatives -s mlocate -g locate

## install git and clone your repo

    doas xbps-install git zsh shfmt xclip maim trash-cli xkill
    doas xbps-install mercurial

## set your ssh key

    zsh
    ssh-keygen

    eval `ssh-agent`
    ssh-add ~/.ssh/id_rsa
    alias pbcopy='xclip -selection clipboard'
    pbcopy < ~/.ssh/id_rsa.pub

    cat ~/.ssh/id_rsa.pub | ssh username@server.address.com 'cat >>
    ~/.ssh/authorized_keys'

## gtk and icon themes

    # pi is an alias for xbps-install
    pi greybird-themes numix-themes arc-theme && ln -sfvn ~/.dotfiles/gtk-3.0 ~/.config

### numix-circle icon theme

```bash
installnumix (){
    [ -d ~/.icons ] || mkdir ~/.icons
    [ -d ~/.themes ] || mkdir ~/.themes
    cd ~/.themes
    git clone https://github.com/numixproject/numix-gtk-theme.git
    cd ~/.icons
    git clone --depth 1 https://github.com/numixproject/numix-icon-theme.git
    git clone --depth 1 https://github.com/numixproject/numix-icon-theme-circle.git
    cp -R numix-icon-theme/* .
    cp -R numix-icon-theme-circle/* .
    gsettings set org.gnome.desktop.interface gtk-theme "Numix"
    gsettings set org.gnome.desktop.wm.preferences theme "Numix"
    gsettings set org.gnome.desktop.interface icon-theme "Numix-Circle"
    cd
} && installnumix
```

## polari irc client

    doas xbps-install -Sy polari
    doas xbps-install -Syu weechat

## command linux presentation tool

    doas xbps-install -Syu mdp

## xdg-utils (associação de arquivos)

+ https://unix.stackexchange.com/questions/157355

* https://unix.stackexchange.com/questions/36380
* https://unix.stackexchange.com/a/333270/3157
* https://askubuntu.com/a/289339/3798

Tools to assist applications with various desktop integration tasks

    doas xbps-install xdg-utils xdg-user-dirs xdg-user-dirs-gtk
    doas xbps-install -Sy mupdf mupdf-tools sxhkd # sxhkd manages keybindings
    doas xbps-install -Sy moreutils  # including vipe (use editor in pipes)
    xdg-user-dirs-update

    xdg-mime query filetype test.pdf
    application/pdf

    xdg-mime query default application/pdf
    evince.desktop

    xdg-open test.pdf
    gnome-open test.pdf

    # define nautilus to open directory (associação de arquivos)

    doas xbps-install -Sy perl-File-MimeInfo
    mimetype img.jpg

    xdg-mime default nautilus.desktop inode/directory

    NOTE: the solution that worked for me was this one:

    doas nvim /usr/share/applications/mimeinfo.cache
    change inode/directory

    ~/.local/share/applications/defaults.list
    [Default Applications]
    application/pdf=xpdf.desktop

## Services control

Services are enabled by simply linking them into the /var/service service directory.

    doas ln -s /etc/sv/<service-name> /var/service/

To disable them again you just remove the link.

    doas rm /var/service/<service name>

Example:

    doas ln -s /etc/sv/gpsd /var/service

### Service Status

To see the status of a supervised service use sv s <service_name>

for example,

    # sv s sshd

returns

    run: sshd: (pid 42) 1587s

To see the status of all services, use sv s /var/service/\*. Stop/Start/Restart

### Start a service

    # sv u sshd

### Stop a service

    # sv d sshd

### Restart a service

    # sv t sshd
    doas sv t NetworkManager

Each of these is a shortcut, for ‘up’, ‘down’, and ‘terminate’, respectively. Only the first letter of each word is
recognized (see sv(8)).

More verbose forms of the above

    ## sv start sshd
    ## sv stop sshd
    ## sv restart sshd

Each of these will also return the status of the service upon exit. Enabling a service

void-provided service directories live in /etc/sv. To enable a service in the current runlevel, create a symlink from it
to /var/service.

    # ln -s /etc/sv/sshd /var/service/

Once a service is linked it will always start on boot and restart if it stops (unless administratively downed).
Disabling a service

To disable a service in the current runlevel remove the symlink to its service directory from /var/service.

    # rm /var/service/sshd

Removing the symlink will also stop the service.

## Beautiful login manager: [sddm](sddm.md)

+ https://www.youtube.com/watch?v=2p7FINJSlAk

  # Note pi is an alias for doas xbps-install -Sy (opendoas)

  # download sugar-dark theme:

  # https://www.opendesktop.org/p/1272122 (sugar-dark)

  # https://store.kde.org/p/1312658/ (sugar-candy)

  doas tar -xzvf ${XDG_DOWNLOAD_DIR}/sugar-dark.tar.gz -C /usr/share/sddm/themes

  [ -f /etc/sddm.conf.d/sddm.conf ] || doas mkdir -p /etc/sddm.conf.d
  doas wget -c https://pastebin.com/raw/weygFJjX -O /etc/sddm.conf.d/sddm.conf

  pi sddm qt5{,-quickcontrols2,-svg,-graphicaleffects}
  rm -f /var/service/lxdm
  doas ln -s /etc/sv/sddm /var/service

## Samba no voidlinux:

+ https://youtu.be/G5_Yiqpvy8U
+ https://wiki.samba.org/index.php/Setting_up_Samba_as_a_Standalone_Server#Creating_a_Basic_guest_only_smb.conf_File

Pra habilitar logs no Void, instale o pacote "socklog-void" e depois habilite os serviços "socklog-unix" e "nanoklogd". Feito isso, os logs do sistema ficarão em "/var/log/socklog/".

    doas xbps-install -Sy  samba socklog-void

    smbd (serviço)
    doas ln -s /etc/sv/smbd /var/service/

    # isso funcionou para permitir acess convidado
    doas chown -R nobody:nogroup /home/share

    sudo groupadd --system smbgroup
    sudo useradd --system --no-create-home --group smbgroup -s /bin/false smbuser

    sudo chown -R smbuser:smbgroup /home/share
    sudo chmod -R g+w /home/share

## Desktop brightness control

    doas xbps-install -Sy redshift redshift-gtk gpsd

## Install plotinus

+ https://github.com/p-e-w/plotinus A searchable command palette in every modern GTK+ application

## haskel development:

+ https://dev.to/waynevanson/haskell-development-on-voidlinux-a-match-in-heaven-and-hell-2hna

DO install the following.

    stack
    ncurses-libtinfo-devel

stack is the tool of choice for managing haskell projects.
ncurses-libtinfo-devel provides the file /lib/libtinfo.so and related symbolic links. Without it, stack doesn't know what the architecture of your machine is and cannot build GHC.

    doas xbps-install -Sy stack ncurses-libtinfo-devel

## gtk+3 devel

Needed to build some very useful packages like: https://github.com/p-e-w/plotinus

    doas xbps-install -Syu gtk+3-devel

## command line translator

    doas xbps-install -Sy translate-shell

## python3 devel and other python stuff

Installing neovim with the default package manager solved some issues I had

    doas xbps-install -Sy python3-neovim neovim-remote python-neovim python3-neovim

    # Using the flatpak
    flatpak install --from https://flathub.org/repo/appstream/com.discordapp.Discord.flatpakref

```bash
pythontools (){
    doas xbps-install -Sy python3-pip python-pip python3-pipx regex-rename pywal ddgr so xq
    doas pip install pipenv
    doas xbps-install -Sy python3-devel python-devel base-devel
    doas pip3 install pydub
    doas pip3 install pyperclip
    doas pip3 install youtube_dl
    doas xbps-install -Sy python-BeautifulSoup4
    doas xbps-install -Sy python3-requests
    doas xbps-install -Sy python3-pafy html2text
} && pythontools
```

## Auto mount usb disks:

doas xbps-install -Sy udiskie

## change screen backlight level

    doas xbps-install -S xbacklight

## Change your colorscheme based in your wallpaper:

    pipx install pywal

## fondo (wallpaper finder)

- [homepage](https://github.com/calo001/fondo)

  flatpak install --from https://dl.flathub.org/repo/appstream/com.github.calo001.fondo.flatpakref

## install prettier (code formating tool)

+ https://prettier.io/docs/en/install.html

  doas npm install --location=global prettier

## in order to profile vim you need

    doas xbps-install -Sy python3.4-matplotlib python-matplotlib

## detox and other tools

    https://github.com/chneukirchen/xtools

    doas xbps-install -Suy detox aria2 lsof cadaver perl-rename xset
    xkblayout-state setxkbmap
    doas xbps-install -Suy trash-cli (I am aliasing "gio trash")
    doas xbps-install -Suy curl tree nmap w3m urlview
    doas xbps-install -Suy at
    doas xbps-install -Suy whois
    doas xbps-install -Suy pandoc poppler-utils util-linux
    doas xbps-install -Syu python3-Sphinx
    doas xbps-install -Suy aria2
    doas xbps-install -Suy transmission-gtk transmission-remote-cli transmission-remote-gtk
    doas xbps-install -Syu deluge{,-gtk}
    doas xbps-install -Suy gnome-font-viewer
    doas xbps-install -Suy gnome-shell-mousewheel-zoom

    # jq-1.6_1 ......... Command-line JSON processor
    doas xbps-install -Sy jq

## android-tools

- adb para voidlinux

  doas xbps-install -Sy android-tools

## Searching for packages

    doas xbps-query -Rs silver

    doas xbps-install -Sy the_silver_searcher ripgrep ack
    doas xbps-install -Sy parallel lynx
    doas xbps-install -Sy flatpak
    doas xbps-install -Sy bitlbee bitlbee-discord
    doas xbps-install -Sy gnome-disk-utility
    doas xbps-install -Sy ethtool

Using regex

    xbps-query --regex -Rs '^linux[0-9.]+-[0-9._]+'

Also install void package managemet vpm

    doas xbps-install -Sy vpm

## voidlinux service manager

    cd ~/bin
    curl -O https://raw.githubusercontent.com/bahamas10/vsv/master/vsv
    chmod ~/vsv
    vsv -h  (shouw help)

    doas vsv - show all services
    doas vsv status - same as above
    doas vsv stop <svc> - stop a service
    doas vsv start <svc> - start a service
    doas vsv restart <svc> - restart a service
    doas vsv hup <svc> - refresh a service (SIGHUP)

## Telegram desktop

+ https://www.edivaldobrito.com.br/telegram-desktop-no-linux/

  flatpak install --from https://flathub.org/repo/appstream/org.telegram.desktop.flatpakref flatpak uninstall anki

Para atualizar o programa, quando uma nova versão estiver disponível, execute o comando:

    flatpak --user update org.telegram.desktop

E se precisar desinstalar o programa, execute o seguinte comando no terminal:

    flatpak --user uninstall org.telegram.desktop

## FontFinder

+ https://vitux.com/use-font-finder-to-install-google-fonts-on-debian-10/

  fontfinder https://flathub.org/apps/details/io.github.mmstick.FontFinder

## install killall

+ https://www.reddit.com/r/voidlinux/comments/fh9ou9/comment/fk9seo3/

  doas xbps-install -Sy psmisc

## color selector

    doas xbps-install -Sy gcolor2

## latex support

    doas xbps-install -Sy gummi # The simple LaTeX editor (with preview)
    doas xbps-install -Sy texlive

You can see all packages at `/opt`. The installer is there

    find /opt -iname install-tl

## wireshark

    doas xbps-install -Sy tcpdump wireshark wireshark-gtk
    doas pip3 install scapy

## python regex-rename

    pip3 install --user regex-rename
    pip3 install --user py-rename

## Nice python shlibs

+ https://www.youtube.com/watch?v=WnV553cjbo0&t=1354s

  pip install --upgrade --user httpx pip install --upgrade --user fire

  # parsel is an web scraper like beautifulsoup

  pip install --upgrade --user parsel

  # password manager

  pip install --user pyvault

  # i3-gaps (i3wm) bspwm

  doas xbps-install -Sy i3-gaps feh sxiv i3{status,blocks} dunst mpd mpc ncmpcpp cava cli-visualizer st polybar xrandr tint2{,-conf}

  doas xbps-install -Sy mopidy{,-{local,mpd,multisonic,podcast,spotify}}

  doas xbps-install -Sy ffmpeg
  doas xbps-install -Sy freshplayerplugin

## solving bash completion as root

In voidlinux the root user has /bin/sh as its login shell, as soon as you call bash it will read your ~/.bashrc

## players

    doas xbps-install -Sy vlc mpv zenity yad socat mediainfo{,-cli} mcop gnome-mpv mpg123 audacious{,-plugins} soundconverter
    doas xbps-install -Sy rtmpdump
    doas pip install livestreamer

## power management (cooling down the system for better performance)

    doas xbps-install -Syu tlp
    doas ln -s /etc/sv/tlp /var/service

## Disabling default ttys

+ https://alpha.de.repo.voidlinux.org/docs/print.html

Void Linux enables agetty(8) services for the ttys 1 to 6 by default.

To disable agetty services remove the service symlink and create a down file in the agetty service directory to avoid
that updates of the runit-void package re-enable the service.

    doas rm /var/service/agetty-tty{3..6}
    doas touch /etc/sv/agetty-tty{3..6}/down

## Controling your kernel parameters (memory use etc)

see [[sysctl]] for changing for example, how frequently your system will use the swap memory

## Terminal transparency

    doas xbps-install -Sy pantheon-terminal
    doas xbps-install -Sy deepin-terminal
    doas xbps-install -Syu terminology

    # faster terminal emulator
    doas xbps-install -Su alacritty

    # some experiments with a different terminal
    doas xbps-install -Su kitty

    # super fast terminal
    # see also: https://github.com/jwilm/alacritty/issues/1071
    doas xbps-install -Sy alacritty

    # terminal trasnparency
    gsettings set io.elementary.terminal.settings background 'rgba(37, 46, 50, 0.95)'

    doas xbps-install -Su ImageMagick ueberzug inkscape gimp feh exiftool colord
    doas xbps-install -Sy anki
    doas xbps-install -Sy pandoc

    # color management
    doas ln -s /etc/sv/colord /var/service

## go language

...Also install html-tool

    doas xbps-install -Sy go
    go get -u github.com/tomnomnom/hacks/html-tool

## google-drive client cli

    doas xbps-install -Sy drive

## fonts

+ https://www.creativebloq.com/graphic-design-tips/best-free-fonts-for-designers-1233380

* MiriamLibre https://fonts.google.com/specimen/Miriam+Libre
* Monaco → https://github.com/todylu/monaco.ttf

```sh
doas xbps-install -Sy font-awesome font-{iosevka,firacode,inconsolata-otf,ibm-type1,hack-ttf,adobe-source-code-pro,hermit-otf,fantasque-sans-ttf,tamsyn} font-firacode font-fira-otf libreoffice-fonts fontmanager abiword && fc-cache fv
doas xbps-install -Sy font-ibm-plex-{otf,ttf}

# useful for polybar themes
doas xbps-install -Sy font-material-design-icons-ttf
```

### Install font hack

    curl -L -O https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.tar.gz
    tar -xzvf Hack-v3.003-ttf.tar.gz
    [ -d "~/.local/share/fonts" ] || mkdir -p ~/.local/share/fonts
    mv ttf/Hack-Regular.ttf ~/.local/share/fonts/Hack-Regular.ttf
    mv ttf/Hack-Italic.ttf ~/.local/share/fonts/Hack-Italic.ttf
    mv ttf/Hack-Bold.ttf ~/.local/share/fonts/Hack-Bold.ttf
    mv ttf/Hack-BoldItalic.ttf ~/.local/share/fonts/Hack-BoldItalic.ttf

    rmdir ttf/
    rm ./Hack-v3.003-ttf.tar.gz

### Terminal font:

+ https://www.reddit.com/r/voidlinux/comments/c5raru/console_fonts/

  Install terminus-font
  sudo vim /etc/rc.config

  FONT="ter-d22b"

## Neofetch console:

+ https://linuxhint.com/what-is-neofetch-for-linux/ tags: [info, tools, admin, sysadmin]

Neofetch is a super-convenient command-line utility used to fetch system
information within a few seconds. It is cross-platform, open-source, and it
displays everything ranging from your system’s uptime to the Linux kernel
version. This guide will explain the working of Neofetch, its features, and
the method of installing it.

    doas xbps-install -Sy neofetch

Now I am using pfetch:

    git clone https://github.com/dylanaraps/pfetch.git
    sudo make install

## alsautils

    doas xbps-install -Sy alsa-utils pulseaudio{,-utils,equalizer-ladspa} ConsoleKit2
    doas ln -s /etc/sv/alsa /var/service

## youtube-dl

    doas xbps-install -Sy youtube-dl yturl youtube-viewer gtk-youtube-viewer

    # https://github.com/yt-dlp/yt-dlp#installation
    # https://stackoverflow.com/a/41310760/2571881
    pi libffi-devel
    pip install --user --upgrade yt-dlp

    xbps-install -S persepolis

## torrent client

    doas xbps-install -S deluge{,-gtk}

## How do I remove old kernels?

Void Linux provides the vkpurge tool for the sole purpose of listing and removing old kernels. To list the old kernels
in your system:

    vkpurge list

To remove a specific version:

    # vkpurge rm 3.8.2_1

To remove all kernels except the latest one:

    # vkpurge rm all

