---
File: ~/.dotfiles/wiki/compras.md
Last Change: Sun, 03 Nov 2024 - 05:33:48
tags: [notes, compras]
---

# Compras

- [ ] Anti-ferrugem
- [ ] Chave 8
- [ ] Goma molhada e seca
- [ ] Papel toalha
- [ ] Manteiga
- [ ] Café
- [ ] Açúcar demerara
- [x] Ovos
- [ ] Linguiça
- [ ] Bisteca
- [ ] Banha
- [x] Gasolina
- [ ] Banana, manga ou maçã
- [ ] Pano de chão
- [ ] Panela com tampa de vidro
- [ ] Peneira de aço
- [ ] Leiteira


