---
File: ~/.dotfiles/wiki/waybar.md
Last Change: Sat, Dec 2024/12/28 - 10:30:33
tags: [waybar, hyprland, voidlinux]
---

# Dicas do waybar

- Referência: https://www.reddit.com/r/voidlinux/comments/1deabrg/comment/l8p3zwk

## What is

Waybar is a highly customizable status bar for Wayland and X11 window managers, designed to display system information like CPU usage, memory, network status, and more. It supports modules and plugins for extended functionality and integrates seamlessly with environments like Sway and River. Its configuration is JSON-based, allowing users to tailor its appearance and behavior to their preferences.


## Startup error

Sugiro iniciar o hyprland pelo console/tty com dbus-run-session -- hyprland .

Confira esse tópico, apesar de envolver outro programa, a raiz do problema é a mesma (o programa dbus-daemon não rodando/o gerenciador de login não fornecendo as informações para se conectar com ele).

 Also make sure pipewire is running with the pulseaudio interface modify the .desktop to auto run pipwire or your hypr confg with exec-once

Reference for pipewire: https://docs.voidlinux.org/config/media/pipewire.html 
