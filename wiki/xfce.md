---
File: xfce.md
Author: Sergio Araujo
Last Change: Fri, 20 Dec 2024 - 10:00:05
 vim:ft=markdown
tags: [desktop, linux]
---

##  xfce.md intro:

XFCE is a free and open source desktop environment

## xfce-goodies

Common Packages in xfce4-goodies:

xfce4-taskmanager - A lightweight task manager for XFCE.
xfce4-screenshooter - A tool for taking screenshots.
xfce4-weather-plugin - A weather information plugin for the XFCE panel.
xfce4-clipman-plugin - A clipboard manager for the XFCE panel.
xfce4-dict - A dictionary application.
xfce4-notifyd - A simple, lightweight notification daemon.
xfce4-panel-profiles - A tool to save and restore XFCE panel layouts.
xfce4-sensors-plugin - A plugin to monitor hardware sensors.
xfce4-battery-plugin - A battery status indicator for the panel.
xfce4-xkb-plugin - A keyboard layout switcher plugin.
xfce4-cpugraph-plugin - A graphical CPU load monitor.
xfce4-diskperf-plugin - A plugin to monitor disk performance.
xfce4-fsguard-plugin - Monitors file system usage.
xfce4-genmon-plugin - A plugin to display generic monitors.
xfce4-mount-plugin - A plugin to mount and unmount devices.
xfce4-netload-plugin - A plugin to monitor network load.
xfce4-places-plugin - Provides quick access to your favorite directories.
xfce4-power-manager - Manages power settings and display brightness.
xfce4-timer-plugin - A simple timer for the panel.
xfce4-wavelan-plugin - Monitors wireless network status.
ristretto - A lightweight image viewer.
parole - A media player for XFCE.
mousepad - A simple text editor for XFCE.
thunar-archive-plugin - Integrates archive management into Thunar.
thunar-media-tags-plugin - Adds support for media tags in Thunar.
xfburn - A lightweight CD/DVD burning application.

## exo-open

xfce4 tool to open preferred apps

## acessando via ssh usando o thunar (gerenciador de arquivos)
Pode digitar ssh que o próprio tunar coloca sftp na barra de endereços

``` sh
sftp://sergio@192.168.0.104/home/sergio/tmp/
```

## install new themes

Install ocs-url: An install helper program for items served
via OpenCollaborationServices ocs://, in case of voidlinux just type:

    sudo xbps-install -Sy ocs-url

+ [Papirus Grey](https://www.xfce-look.org/p/1576871)"
+ [sunrise](https://www.xfce-look.org/p/1258305)

## xfce no sound:

    doas xbps-install -Sy alsa-utils
    alsamixer

## change your dock
Plank is a elegant, simple and clean dock

    doas xbps-install -Sy plank

If you start 'plank' with `--preferences` it will show a panel where 
you can choose where and how it will appear on your desktop.

## New and beautiful star apps menu
+ https://www.pragmaticlinux.com/2021/03/install-and-configure-the-whisker-menu-as-your-xfce-start-menu/

    doas xbps-install xfce4-whiskermenu-plugin

    Right click (panel) > panel preferences > add a new item

## take screenshots with xfce4

    doas xbps-install -Sy xfce4-screenshooter

## Enabling transparency
+ https://askubuntu.com/a/100376/3798

Xfwm4-Composite-Editor is a tool that enables easy access to various settings
pertaining to the Xfce window manager in a graphical application with a simple
interface.

Open: 

    xfwm4-tweaks-settings > compositor > enable screen compositor

