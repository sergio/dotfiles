---
File: googlesearch.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: jul 26, 2020 - 16:15
tags: [python]
---

#  googlesearch.md intro:
+ https://www.instagram.com/p/CDHAZtAA8M_/?utm_source=ig_web_copy_link

``` python
>>> from googlesearch import search
>>> query = "vim"
>>> for i in search(query, tld="com", num=10, stop=10, pause=2):
...     print(i)
...
https://www.vim.org/
https://www.vim.org/download.php
https://www.vim.org/about.php
https://www.vim.org/sponsor/
https://www.vim.org/vim-8.2-released.php
https://commons.wikimedia.org/wiki/File:Vimlogo.svg
https://en.wikipedia.org/wiki/Vim_(text_editor)
https://www.vim.org/download.php#unix
https://www.vim.org/download.php#amiga
https://www.vim.org/download.php#os2
```
