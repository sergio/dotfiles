---
File: python-libraries.md
Author: Sergio Araujo
Last Change: Fri, 04 Oct 2024 - 18:57:28
tags: [python]
---

#  python-libraries.md intro:

+ [pathlib](pathlib.md) - manipula paths do sistema
+ [python-magic](python-magic.md) - determina tipo mime
+ [click](click.md)   - criação de interfaces CLI
+ random  - geração de números randômicos
+ [google](googlesearch.md)   pip install --user google
+ [regex-rename](regex-rename.md) pip3 install --user regex-rename
+ [py-rename](py-rename.md)
+ [openpyxl](openpyxl.md)

# vim: ft=markdown et sw=4 ts=4 cole=0
