---
Filename: lxappearance.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [desktop, gtk, theme]
 vim:ft=markdown
---

lxappearance - GTK+ theme switcher

LXAppearance is part of LXDE project.
It's a desktop-independent theme switcher for GTK+.
