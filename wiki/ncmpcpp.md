---
/home/sergio/.dotfiles/wiki/ncmpcpp.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## Shortcuts
# % ! column -t -s= -o=
+ https://pkgbuild.com/~jelle/ncmpcpp/

    Table below serve as a quickstart cheatsheet on navigating ncmpcpp.
    Keyboard Key	Function

    Down j	   : Move Cursor down
    Page Up	   : Page up
    Page Down  : Page down
    Home	   : Home
    End	       : End
    Tab	       : Switch between playlist and browser
    1 F1	   : Help screen
    2 F2	   : Playlist screen
    3 F3	   : Browse screen
    4 F4	   : Search engine
    5 F5	   : Media library
    6 F6	   : Playlist editor
    7 F7	   : Tag editor
    0 F10	   : Clock screen
    >          : Next track
    <          : Previous track
    p          : Play/Pause
    +          : Increase volume 2%
    -          : Decrease volume 2%


    a ................ add current url/file/directory to playlist
    A ................ Add selected items to playlist/m3u
