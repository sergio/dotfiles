---
File: ~/.dotfiles/wiki/nvim.md
author: Sérgio Araujo
Last Change: Sun, 17 Nov 2024 - 18:40:49
tags: [vim, nvim, neovim, diff, diffget]
---

# Neovim configs in dotfile

[Full neivim setup 2024](https://youtu.be/KYDG3AHgYEs?si=NVqfYAVd36f3XJIT)
[Link at dotfyle](https://dotfyle.com/neovim/configurations/top)
[Lazyvim for ambitious developers](https://lazyvim-ambitious-devs.phillips.codes/course/)
[Chapter 1](https://lazyvim-ambitious-devs.phillips.codes/course/chapter-1)

true## another config to stud0

[here](https://dev.lugh.ch/git/weeheavy/neovim)
[Base template](https://github.com/VonHeikemen/lazy-template)

## To fix marksman install download the lsp server from here (on termux)

https://github.com/artempyanykh/marksman/releases for termux the version is:
https://github.com/artempyanykh/marksman/releases/download/2024-11-20/marksman-linux-arm64

You must place the server at: 

~/.local/nvim/mason/packages/marksman
and recreate the symbolic link in the bin folder pointing to this file

## Instalking lua-language-server on termux

```sh
pkg in lua-language-server
```

solution for termux

```sh
Install lua-language-server with your package manager:

    sudo pacman -S lua-language-server

Find where it is installed:

    ⚡➜ ~ which lua-language-server
    /data/data/com.termux/files/usr/bin/lua-language-server

Make a symbolic link to where Mason would usually install it:

    mkdir -p ~/.local/share/nvim/mason/packages/lua-language-server/libexec
    ln -s /data/data/com.termux/files/usr/share/lua-language-server
~/.local/share/nvim/mason/packages/lua-language-server/libexec

Create the wrapper script for starting lua-language-server (please fill in your
username):

    vim
~/.local/share/nvim/mason/packages/lua-language-server/lua-language-server

    #!/usr/bin/env bash
    exec
"/home/your-username/.local/share/nvim/mason/packages/lua-language-server/libexec/bin/lua-language-server"
"$@"

    chmod +x
~/.local/share/nvim/mason/packages/lua-language-server/lua-language-server

```

## Installing stylua 

```sh
cargo install stylua --features luajit 
```


## Reordering numbers

```vim
:let c=0 | 14,40g/^\d\+\ze/ let c+=1 | s//\=printf('%02d', c)
```

## Auto close neotree

```lua
require("neo-tree").setup({
    event_handlers = {

        {
            event = "file_open_requested",
            handler = function()
                -- auto close
                -- vimc.cmd("Neotree close")
                -- OR
                require("neo-tree.command").execute({ action = "close"
                })
            end
        },

    }
})
```

## Configure spell

```vim
setlocal spell spelllang=pt_br
```

Now you can:

```txt
z= ....... show suggestions
zg ....... add word as good one
1z= ...... accept first suggestion
```

## Installing lazyvim

In my case I have a separated nvim configuration

```sh
alias lv='(){(export NVIM_APPNAME=lv;export MYVIMRC=~/.config/lv/init.lua;nvim "$@")}'
\rm -rf ~/.config/lv
\rm -rf ~/.local/state/lv
\rm -rf ~/.local/share/lv
\rm -rf ~/.cache/lv

git clone https://github.com/LazyVim/starter ~/.config/lv
lv --headless '+Lazy! sync' +qa
```

## List your plugins

```sh
grep -A 1 return ~/.config/nvim/lua/plugins/* \
| grep -v -e -- -e return \
| awk '{print $NF}' \
| tr -d "\"',{}" \
| sort -u
```

## telescope nvim path_display

```lua
defaults = {
    theme = 'tokyonight',
    -- path_display = { 'smart' },
    -- path_display = filenameFirst,
    path_display = {
        filename_first = {
            reverse_directories = false,
        },
    },
    file_ignore_patterns = { '.git/', 'node_modules' },
    layout_strategy = 'vertical',
    layout_config = { prompt_position = 'top' },
    sorting_strategy = 'ascending',
    mappings = {
        i = {
            ['<Esc>'] = 'close',
            ['<C-c>'] = false,
            ['<C-k>'] = 'move_selection_previous',
            ['<C-j>'] = 'move_selection_next',
        },
    },
    layout_config = {
        horizontal = {
            preview_cutoff = 0,
        },
    },
},
```

## Homebrew on linux

[link here](https://docs.brew.sh/Homebrew-on-Linux)

- Daily tips for learning vim

- [link here](https://countvajhula.com/2021/01/21/vim-tip-of-the-day-a-series/)

## Implement this

[vim-cool](https://www.reddit.com/r/neovim/comments/1ct2w2h/lua_adaptation_of_vimcool_auto_nohlsearch/)

## kickstart modular

- [lik here](https://github.com/dam9000/kickstart-modular.nvim.git)
- [Link about modular](https://blog.epheme.re/software/nvim-kickstart.html)

## Good neovim themes

- [vim colorschemes](https://vimcolorschemes.com/top)
- -- Transparent Background
vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
vim.api.nvim_set_hl(0, "EndOfBuffer", { bg = "none" })
vim.api.nvim_set_hl(0, "SignColumn", { bg = "none" })
-- Set the selection background color
-- vim.api.nvim_set_hl(0, "Visual", { bg = "#37444f" })
-- Colour Line Number
vim.api.nvim_set_hl(0, "LineNr", { bg = 'none', fg = "#889299" })
-- Replace tildes at the end of the buffer
-- vim.opt.fillchars = { eob = " " }

-

NOTE: We have a virtualenv alias called myenv

## Startup time (performance)

- [articke](https://ntbbloodbath.github.io/posts/optimizing-neovim-startuptime/)

## neovim regexes

```lua
-- Function to create lists in markdown
function matchline()
    local pattern = '(^%s*[+-%*]%s*)(%[[x ]%])(.*$)'
    local pattern2 = '(^%s*[+-%*]%s*)?:(?!%[.%])(.*$)'
    local line = vim.api.nvim_get_current_line()
    if string.match(line, pattern) then
        print("patrern one matches")
    end

    if string:match(line, pattern2) then
        print("pattern two matches")
    end
end
```

## install moreutils

I gives us nice tools

## Dealing with marks

[Reference](https://vi.stackexchange.com/a/31307/7339)

```lua
local r, _ = unpack(vim.api.nvim_buf_get_mark(0, "a"))
local mark_a = vim.api.nvim_buf_get_mark(0, "a")[1] or a
```

## Also install luacheck

- [Read guide here](https://github.com/nanotee/nvim-lua-guide#luacheck)

```sh
luarocks install luacheck
luarocks install lanes
```

You can get luacheck to recognize the vim global by putting this configuration in ~/.luacheckrc (or $XDG_CONFIG_HOME/luacheck/.luacheckrc):

```lua
globals = {
    "vim",
}
```

The Alloyed/lua-lsp language server uses luacheck to provide linting and reads the same file. For more information on how to configure luacheck, please refer to its documentation

## Git commit command

```vim
nnoremap gc :execute '!git commit -m "'.input('Enter message: ').'"'<cr>
```

## shell alias to edit last file

```vim
alias lvim="vim -c':e#<1'"
```

## First lazyvim load

```sh
nvim --headless '+Lazy! sync' +qa
```

## debug mappings

```vim
echo maparg('<m-p>', 'n')
```

## Get first non blank

```lua
-- https://www.reddit.com/r/neovim/comments/voa124/comment/ied5nsd/?context=3
local var = vim.fn.getline '.':find '%S'
```

## neovim starter kit

[link](https://github.com/bcampolo/nvim-starter-kit)
[another great config](https://youtu.be/zHTeCSVAFNY?si=TA_zexu6abxOuwO_)

## autocmd group Utility

```lua
local function augroup(name)
    return vim.api.nvim_create_augroup("lazyvim_" .. name, { clear = true })
end
```

## 5 smart mini snippets (article)

- [aerticle](https://medium.com/scoro-engineering/5-smart-mini-snippets-for-making-text-editing-more-fun-in-neovim-b55ffb96325a)

## Snippets collection

- [link here](https://github.com/TwIStOy/luasnip-snippets)
- [Native snippets](https://www.reddit.com/r/neovim/comments/1cxfhom/builtin_snippets_so_good_i_removed_luasnip/)

## get current treesitter normal_mode

lua print(vim.treesitter.get_node():type())

## This week in neovim

[link for: this week in neovim](https://dotfyle.com/this-week-in-neovim)
[trending on neovim](https://dotfyle.com/neovim/plugins/trending)
[trending configs](https://dotfyle.com/neovim/configurations/top)

## undestand nvim series

- [video 1](https://youtu.be/87AXw9Quy9U?si=lO0IQ9qCJ0fPjcQQ)
- [video 4](https://youtu.be/kYXcxJxJVxQ?si=uiSyi13axT0EJiOT)

## Telescope live grep open qickfix

To open the quickfix window press

```txt
ctrl-q
```

## Install nerd fonts

- [A better way to install NerdFonts](https://github.com/ronniedroid/getnf)

```sh
git clone https://github.com/ronniedroid/getnf.git
cd getnf
./install.sh
```

Now you can run

```sh
getnf
```

There is an amazing site where you can test some proramming fonts:

- [Programming fonts](https://www.programmingfonts.org/)

Estou usando a fonte [Mononoki](https://github.com/madmalik/mononoki)

## Add blank line after md headings

```txt
:%s/^#.*\n\zs\v(^$)@!/\r&

/^#.*\n\zs\v(^$)@!

/ ..... inicio de busca
^ ..... começo de lina
.* .... seguido de qualquer coisa
\n .... seguido de quebra de linha
\zs ... cosidere á partir daqui
\v .... very magic para eviar escapes
( ..... inicia um grupo regex
^$ .... começo de linha colado no final de linha
@! .... nega o grupo
```

## Convert hour + minutes + seconds to seconds:

- https://stackoverflow.com/a/2181764/2571881

## Convert hour + minutes + seconds to seconds:

- https://stackoverflow.com/a/2181764/2571881
markdown

## good nvim model

- [it uses lazy.nvim](https://codeberg.org/j4de/nvim)

## Show current leader key

```vim
:let mapleader
```

## make lunarvim mappings not silent (avoid double tapping a keybind like ç or /)

```vim
lvim.keys.normal_mode["ç"] = { ":", silent = false }
```

## 0 to LSP

- [ThePriagen youtube](https://www.youtube.com/watch?v=w7i4amO_zaE)
- [lsp-zero](https://lsp-zero.netlify.app/)

## nvim.md intro

hyperextensible Vim-based text editor

## Conceal markdown links

In your markdown ftplugin.lua add:

```vim
vim.wo.conceallevel = 2
```

In your commands.lua add:

```vim
vim.cmd([[syn region markdownLink matchgroup=markdownLinkDelimiter start="("
end=")" contains=markdownUrl keepend contained conceal]])
vim.cmd([[syn region markdownLinkText matchgroup=markdownLinkTextDelimiter
start="!\=\[\%(\%(\_[^][]\|\[\_[^][]*\]\)*]\%( \=[[(]\)\)\@=" end="\]\%(
\=[[(]\)\@=" nextgroup=markdownLink,markdownId skipwhite
contains=@markdownInline,markdownLineStart concealends]])
```

## Insert a range of ips using put

- [insert-ip-range-using-vim](https://www.commandlinefu.com/commands/view/3055/insert-ip-range-using-vim)

```vim
:for i in range(1,255) | .put='192.168.0.'.i | endfor
```

## Função que usa uma protected call

```lua
M.secure_call = function(module)
    local status, module = pcall(require, "" .. module)
    if not status then
        vim.notify("Unable to load " .. module)
        return
    end
    return module
end
```

## File explorer

- source: dotfiles do Gabriel Binci

```lua
vim.keymap.set("n", "<leader>e", function()
    local nt_status, _ = pcall(require, "nvim-tree")
    if not nt_status then
        return ":Ex<CR>"
    else
        return ":NvimTreeToggle<CR>"
    end
end,
    { desc = "File explorer" , expr = true, replace_keycodes = true}
)
```

## Buffer explorer

Mapeamento condicional

```lua
vim.keymap.set("n", "<leader>b", function()
    -- local status_ok, _ = pcall(require, "buffer_manager")
    local status_ok, buffer_manager = pcall(require, "buffer_manager")
    if not status_ok then
        return ':ls<CR>:b<space>'
    else
        return ":lua buffer_manager.ui.toggle_quick_menu()<cr>"
    end
end,
    { desc = "List opened buffers" , expr = true, replace_keycodes = true}
)
```

## Pegando linhas ímpares e pares com o vim/nvim

```viml
:%s/.*/\=submatch(0) . (line('.')%2==0?" impar":" par")
```

Estamos usando um operador ternário que testa o resto da divião
do número da linha atual por 2

```
\= .................. registro de expressões
submatch(0) ......... corresponde ao padrão casado na busca
.  .................. concatena
line('.') ........... pega o valor da linha corrente
% ................... neste caso resto da divisão
```

## netrw

```vim
% .............. create new file
d .............. create new directory
```

## Using neovim as a diff tool

- <https://www.baeldung.com/linux/vim-diff-tool>
- <https://neovim.discourse.group/t/how-to-compare-two-files/3285/>
- <https://www.youtube.com/watch?v=2KCsEnBKrzM>

  nvim -d file1 file2
  ]c .................... jump to the next diff
  :diffget ............... aplica a diferença do arquivo alternativo no arquivo corrente
  :diffput ............... aplica a diferença do arquivo corrent no arquivo alternativo

If the two files are in two splits, try

:windo diffthis.

Otherwise, try

:diffsplit.

## sites to learn Vim

1 - <https://www.vim.so/>

## archlinux install

To install neovim with full python3 and python2 support run:

sudo pacman -S xclip
sudo pacman -S python-pip
sudo pacman -S python2-pip
sudo pip3 install neovim
sudo pip3 install neovim-remote

It is recommended to use the --user flag when installing the neovim package:

## lookbehind and lookahead regexes

```viml
lookbehind  lookahead
positive  (atom)\@<=  (atom)\@=
negative  (atom)\@<!  (atom)\@!

In very magic mode:
lookbehind  lookahead
positive  (atom)@<=  (atom)@=
negative  (atom)@<!  (atom)@!
```

## jumping faster

```viml
]] Jump forward to begin of next toplevel
[[ Jump backwards to begin of current toplevel (if already there, previous toplevel)
]m Jump forward to begin of next method/scope
[m Jump backwords to begin of previous method/scope

][ Jump forward to end of current toplevel
[] Jump backward to end of previous of toplevel
]M Jump forward to end of current method/scope
[M Jump backward to end of previous method/scope
```

## Insert semicolon and go to the next line or go to the next line

- <https://stackoverflow.com/questions/66397197>

  nmap <silent> <expr> <Leader>c strpart(getline('.'), col('$')-2, 1) == ";" ? "j" : "A;<Esc><Home>j"

## User install

The Python package neovim was renamed to pynvim.

sudo pip2 install pynvim
sudo pip3 install pynvim

The --user flag installs neovim in a directory within your home directory.
The limitation with this is that every user account on the system will need
to perform this step. You can confirm the location and that your
environment permits user packages by running: python -m site

## ToggleMovement

```viml
" Make some command toggle for more speed!
" https://ddrscott.github.io/blog/2016/vim-toggle-movement/
function! ToggleMovement(firstOp, secondOp, ...) abort
let g:toggle_movement = get(g:, "toggle_movement", [1, a:firstOp])
if g:toggle_movement[1] != a:firstOp
let g:toggle_movement = [1, a:firstOp]
endif

let try_num = 1
while try_num <= a:0 + 2
let try_num = try_num + 1
if g:toggle_movement[0] == 1
let l:command = a:firstOp
elseif g:toggle_movement[0] == 2
let l:command = a:secondOp
elseif a:0 == 1 && g:toggle_movement[0] == 3
let l:command = a:1
else
echom "Something went wrong... (g:toggle_movement = " . string(g:toggle_movement) . ")"
return
endif

if (a:0 == 0 && g:toggle_movement[0] == 2) || (a:0 == 1 && g:toggle_movement[0] == 3)
let g:toggle_movement[0] = 1
else
let g:toggle_movement[0] = g:toggle_movement[0] + 1
endif

let pos = getpos('.')
execute "normal! " . l:command
if pos != getpos('.')
break
endif
endwhile
endfunction
nnoremap <silent> ^ :call ToggleMovement('^', '$' ,'0')<cr>
nnoremap <silent> 0 :call ToggleMovement('0' ,'^', '$')<cr>
nnoremap <silent> $ :call ToggleMovement('$', '0', '^')<cr>
nnoremap <silent> ; :call ToggleMovement(';', ',')<cr>
nnoremap <silent> , :call ToggleMovement(',', ';')<cr>
nnoremap <silent> L :call ToggleMovement('L', 'M', 'H')<cr>
nnoremap <silent> M :call ToggleMovement('M', 'H', 'L')<cr>
nnoremap <silent> H :call ToggleMovement('H', 'L', 'M')<cr>
nnoremap <silent> G :call ToggleMovement('G', 'gg')<cr>
nnoremap <silent> gg :call ToggleMovement('gg', 'G')<cr>
```

## Installing via flatpack

sudo flatpak install flathub io.neovim.nvim
flatpak run io.neovim.nvim

## neovim --servername support

- [Article](http://www.davekuhlman.org/nvim-remote-control.html)

  " using vim-plug install neovim-remote
  Plug 'mhinz/neovim-remote'

  sudo pip3 install neovim-remote

  NVIM_LISTEN_ADDRESS=/tmp/nvimsocket nvr file1 file2
  nvr --servername /tmp/nvimsocket file1 file2

  nvr --remote file1 file2
  nvr --remote file1 $MYVIMRC

  nvr zsync.md
  nvr --remote test.txt && exit

# Send keys to the current buffer

nvr --remote-send 'iabc<esc>'

# Enter insert mode, insert 'abc', and go back to normal mode again

# Evaluate any VimL expression, e.g. get the current buffer

nvr --remote-expr 'bufname("")'
README.md

Open files always in the same nvim no matter which terminal you're in.

If you just run "nvr -s", a new nvim process will start and set its address
to /tmp/nvimsocket automatically.

Now, no matter in which terminal you are, nvr file will always work on
that nvim process. That is akin to emacsclient from Emacs.

## Usando nvr em conjunto com o fasd

Defina um alias para user o comando 'v'

```markdown
alias v='f -e nvr -s'
nvr `f dicasvim`
```

NOTE: $MYVIMRC is settled on my .zshenv

## Instant markdow preview

- Plug 'suan/vim-instant-markdown', {'for': 'markdown'}

  [sudo] npm -g install instant-markdown-d

## open nvim from st (simple terminal)

st -e nvim

## The emmet plugin for html

- <https://medium.com/vim-drops/be-a-html-ninja-with-emmet-for-vim-feee15447ef1>

  table>tr*2>td*5

## inserting special chars in html

- <https://vim.fandom.com/wiki/HTML_entities>

```vim
" Escape/unescape & < > HTML entities in range (default current line).
function! HtmlEntities(line1, line2, action)
let search = @/
let range = 'silent ' . a:line1 . ',' . a:line2
if a:action == 0  " must convert &amp; last
execute range . 'sno/&lt;/</eg'
execute range . 'sno/&gt;/>/eg'
execute range . 'sno/&amp;/&/eg'
else              " must convert & first
execute range . 'sno/&/&amp;/eg'
execute range . 'sno/</&lt;/eg'
execute range . 'sno/>/&gt;/eg'
endif
nohl
let @/ = search
endfunction
command! -range -nargs=1 Entities call HtmlEntities(<line1>, <line2>, <args>)
noremap <silent> <Leader>h :Entities 0<CR>
noremap <silent> <Leader>H :Entities 1<CR>

" Unescape lines 10 to 20 inclusive.
:10,20Entities 0

" Escape all lines.
:%Entities 1
```

## Find the last occurrence of one word

:1?xxx

or

:\vxxx\ze((xxx)@!\_.)*%$

## search a pattern only in a line range

- <https://jdhao.github.io/2019/11/11/nifty_nvim_techniques_s5/>

  /\%>10l\%<20lsearch

## Creating icon for nvim

- <https://paulherron.com/blog/open_file_current_neovim_window>

```sh
[Desktop Entry]
Name=Neovim
GenericName=Text Editor
TryExec=nvim
Exec=nvim %F
Terminal=true
Type=Application
Keywords=Text;editor;
Icon=nvim
Categories=Utility;TextEditor;
StartupNotify=false
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
```

## increasing numbers (disable no octal)

If you don't want to increment numbers in octal notation,

:set nrformats-=octal.

<!--
vim: ft=markdown et sw=4 ts=4 cole=0 spell:
-->
