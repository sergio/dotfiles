Filename: upower.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [power, energy, battery, admin, tools, acpi]

 vim:ft=markdown
UPower is an abstraction for enumerating power devices, listening to device events and querying history and statistics. Any application or service on the system can access the org.freedesktop.UPower service via the system message bus.

UPower was once called DeviceKit-power.

