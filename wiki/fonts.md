---
File: ~/.dotfiles/wiki/fonts.md
Last Change: Fri, 08 Nov 2024 - 18:16:40
tags: [fonts]
---

# intro

## Fontes Tipográficas

Fontes legais como:

[Quicksand](https://fonts.google.com/specimen/Quicksand) que equivale à fonte “Arial Rounded MT” do windows

## Fontes monoespaçadas

A fonte [mononoki](https://github.com/ryanoasis/nerd-fonts/releases/download/v3.2.1/Mononoki.zip) é uma ótima fonte monoespaçada
