---
File: feh.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [img, wallpaper]
---

##  Command line image viewever
+ http://manpages.ubuntu.com/manpages/xenial/man1/feh.1.html

    feh --randomize --bg-fill ~/.wallpaper/*
    feh -z --bg-fill ${XDG_PICTURES_DIR}/backgrounds

The above command tells feh to randomize the list of files in the
~/.wallpaper/ directory and set the backgrounds for all available desktops to
whichever images are at the front of the randomized list (one unique image for
each desktop).

    ⟨CTRL+delete⟩ [delete]
                Remove current file from filelist and delete it

    feh -t -Sfilename -E 128 -y 128 -W 1024 /opt/images
                Show 128x128 pixel thumbnails, limit window width to 1024 pixels.

    feh -FD5 -Sname ~/img/backgrounds/
    Starts a slideshow (5sec) for each image

## cronjob

```sh
*/15 * * * * DISPLAY=:0 feh -z --bg-fill ~/.dotfiles/backgrounds
```


## shortcuts

    -d ............ show filename on the top left
    d ............. toggle filename
    f ............. toggle fullscreen
    m ............. toggle menu
    n ............. show next img
    p ............. show preview img
    q ............. quit feh
    s ............. save (see --output-dir)
    z ............. jump random
    Home .......... jump to the first img
    End ........... jump to the last img
    Delete ........ remove file from the filelist
    Ctrl + Delete   remove file from the list and deletes it

