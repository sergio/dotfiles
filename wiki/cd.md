---
file: cd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# cd - mudar de diretórios

    cd -    ............ voltar ao último diretório
    cd ..   ............ subir um nível
	cd      ............ quando digitado sem argumentos volta ao $HOME

Pra quem usa o `zsh` podemos fazer coisas assim:

		cd =python3(:h)
