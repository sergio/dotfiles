---
file: oracoes.md
author: Sergio Araujo
Last Change: Thu, 26 Sep 2024 - 14:30:50
---

## Orações.md

- [Agradeço Senhor](Agradeco-Senhor.md)
- [Oração do amanhecer](Oracao-do-amanhecer.md)
- [Conversa com Nossa Senhora](conversa-com-nossa-senhora.md)
- [Minutos de Sabedoria](minutos.md)
- [Proteção contra perigos](oracao-sao-bento.md)
- [pra quebranto](oracao-quebranto.md)
- [Cobreiro](cobreiro.md)
- [Códigos Grabovoi](grabovoi.md)
- [Pai Nosso](pai-nosso.md)
- [Oração de São Bento](sao-bento.md)
