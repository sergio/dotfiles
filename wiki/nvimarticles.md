---
Filename: nvimarticles.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [nvim, articles]
 vim:ft=markdown
---

## Dot-Repeat in Vim and Neovim
+ https://www.vikasraj.dev/blog/vim-dot-repeat
