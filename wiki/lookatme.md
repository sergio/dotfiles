---
 Filename: ~/.dotfiles/wiki/lookatme.md
 Last Change: Fri, 19 Apr 2024 - 15:01:17
 tags: [spreedsheet, tools, ppt, nvim, markdown]
 vim:ft=markdown
---

# lookatme is an interactive, extensible, terminal-based markdown presentation tool

+ https://github.com/d0c-s4vage/lookatme
+ https://github.com/waylonwalker/lookatme
+ https://waylonwalker.com/lookatme-styles/

## how to install

```sh
pip install --upgrade lookatme
lookatme --tutorial
```

