---
file: ~/.dotfiles/wiki/nmcli.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [wifi, network]
---

## nmcli

- Referência: https://leandrodotta.home.blog/2019/12/09/conectar-a-rede-wi-fi-no-linux-com-nmcli-network-manager/

nmcli - command-line tool for controlling NetworkManager

    nmcli device wifi connect <ssid> password <password>

Onde, <ssid> é o nome (SSID) da rede Wi-Fi para se conectar, e, <password> é a senha da rede.

## List configured connections

    nmcli c
    nmcli connection show

## activate a given wifi network

    nmcli con up <connection name>

## list devices

    nmcli d

## Desliga o wifi:

    nmcli radio wifi off

## cli interface for NetworkManager

    nm-connection-editor


