---
File: nautilus.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: jun 10, 2021 - 19:23
tags: [tools]
---

## ssh access (também é possível acessar via ssh usando o thunar - xfce4)

    on thunar:
    sftp://u0_a221@192.168.3.178:8022/data/data/com.termux/files/home
    sftp://u0_a232@192.168.0.40:8022/data/data/com.termux/files/home

       Android user   ip        port             path
              |       |           |                |
    sftp://u0_a221@192.168.3.178:8022/data/data/com.termux/files/home
    sftp://u0_a179@192.168.0.100:8022/data/data/com.termux/files/home
    sftp://u0_a289@192.168.0.101:8022/data/data/com.termux/files/home

    on your termux type:

    sshd -d

## Hide folders from your file manager withhout renaming them
+ https://fosspost.org/tutorials/hide-files-folders-linux-file-manager

The method is quite simple. Just create a .hidden text file in your home
directory, and put the name of the files/folders you want to hide in each
separate line. So the contents of my own file at /home/mhsabbagh/.hidden will
be:

    snap
    Videos
    Music

Then close your file manager and open it again, and you’ll see the folders being hidden:

## Access 4shared via webdav

    davs://webdav.4shared.com
