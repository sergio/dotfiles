---
file: makepasswd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Gerador de senhas do linux
mkpasswd - generate new password, optionally apply it to a user
