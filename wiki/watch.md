---
file: watch.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# exibindo a hora a cada dois segundos

    watch 'date +%H:%M:%S'

Uma simplificação do comando acima

    watch 'date +%T'

A cada dois segundos mostra o tamanho da pasta atual

    watch -n 2 "du -sh | awk '{print $1}'"

## Verificando o status da bateria do notebook via terminal
+ Necessário o comando acpi instalado

    watch --interval=5 acpi -V

## Acompanhando logs

    watch -n60 du /var/log/messages

## Exibindo a hora segundo a segundo

    watch -n 1 -t 'date +%T'

## a cada 5 segundos exibindo a memória livre

    watch -n 5 'df -m'

## monitorando alterações em um diretório

    watch -d ls -l

A opção -d é que habilita o recurso de destacar
as diferenças entre cada atualização do watch
## monitorando conecções estabelecidas

    watch -n 1 'netstat -tpanl | grep ESTABELECIDA'

## monitorando uma pasta em que estão sendo gravados dados

    watch -n 10 'du -sh ./'

## ocultando o cabeçalho do comando watch
Use a opção -t

## Definindo um tempo menor para o comando watch

Quicker Intervals for watch command

A friend reminded me the other day, that in Linux it’s actually possible to
make watch command wait a lot less between re-runs of the commands you pass to
it. You can go as far as making watch re-run every 0.1s, but even 0.2s is
going to be 10x better than default - so the counters in my example will now
be updating almost instantly.

Just use the -n parameter to specify new time interval:

    watch -n0.2 ifconfig wlp2s0
