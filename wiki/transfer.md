---
Filename: transfer.md
Last Change: Mon, 19 Feb 2024 - 20:26:39
tags: [bash, shell, zsh, transfer, share]
---

## Transfer:  Easy file sharing from the command line

+ [transfer.sh](https://transfer.sh/)

    `curl --upload-file ./hello.txt https://transfer.sh/hello.txt`

## Using the shell function

    transfer hello.txt
    ############# 100.0% https://transfer.sh/F3QDk4/hello.txt

## see also shellcheck

NOTE: it also trnsfers zip files
