---
File: mediafire.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 20:56:43
tags: [filestorage, download, storage, mediafire, tools]
---

# How to bulk download from mediafire

+ [How scrape mediafire](https://datahorde.org/how-to-archive-or-scrape-mediafire-files-using-mf-dl/)
+ [Mediafire downloader](https://gitgud.io/Pyxia/mf-dl/)

## Installation

mf-dl follows the usual steps for setting up a python tool:

Install Python 3 if you don’t already have it on your
device. Clone the mf-dl repository from
<https://gitgud.io/Pyxia/mf-dl.git> using a git client.
Alternatively download the repo and unzip it. Using a
terminal, cd into the mf-dldirectory and run

    python3 -m pip install -r requirements.txt.

Downloading Files with mfdl.py

mfdl.py is a bulk-downloader fr MediaFire links. You may
have found these links yourself, copied them from your
bookmarks or possibly scraped them beforehand. At any rate,
mfdl.py will download the contents and metadata for a list
of links that have already been collected.

The input is a sequence of links and can be any file
separated by spaces, new-lines or commas. Ideally, you might
want to use a spreadsheet-friendly CSV file. For this
tutorial, copy the table below into Excel, or another
spreadsheet editor, and save it aslinks.csv.

To generate a list of files I've used:

    OBS: The real list was changed to avoid blocking
    plowlist -R https://www.mediafire.com/folder/jmfglhxa3xh91/wallpapers > lista.txt

    I have also removed a '?' symbol from the links and combined the full url
    using a vim macro.

    ...And used a bulkrename on "ranger file manager" like this:
    :let c=0 | g/^.*\ze\./ let c+=1 | s//\='file-' . printf('%03d', c)

Next we will need an output directory to save mf-dl’s grabs.
mf-dl does not have permission to create new directories, so
you will have to create a new folder if the destination
doesn’t already exist. For demonstration’s sake we will
create/output directory under mf-dl.

## meus papeis de parede

+ [Link](https://www.mediafire.com/file/jmvitlh4d56eyu0/wallpapers.zip/file)
