---
file: exiftool.md
author: Sergio Araujo
Last Change: Wed, 27 Mar 2024 - 00:03:44
---

# exiftool - edit images and images metadata

+ [pyExifToolGUI](https://hvdwolf.github.io/pyExifToolGUI/)

exiftool - Read and write meta information in files

## Pegar a data de criação de uma foto

+ [datas-fotos-e-shell](https://aurelio.net/blog/2010/05/15/datas-fotos-e-shell/)

```bash
exiftool img.jpg | grep 'Create Date' | head -1 | cut -d: -f2-
```

Para modificar a data usamos o comando touch (usando a opção -t)

```bash
exiftool NF | grep 'Create Date' | head -1 | cut -d: -f2- | tr -d ': ' | sed 's/./&./12'
```

```bash
for i in *; do
echo touch -t $(exiftool "$i" | grep 'Create Date' | head -1 | cut -d: -f2- | tr -d ': ' | sed 's/./&./12'\n) "$i"
done
```

## Change pdf title

+ [stackexchange 303151](https://apple.stackexchange.com/a/274742/303151)

```bash
exiftool -Title="New Title" /path/to/filename.pdf
```

## Substitui palavras-chave existentes por duas novas palavras chaves

```bash
exiftool -keywords=palavra1 -keywords=palavra2 foto.jpg
```

## Remove all metadata from a file

```bash
exiftool -all= -overwrite_original "file name.extension"
```

## Remove all metadata from the current directory

```bash
exiftool -all= -overwrite_original .
```

## Remove all metadata from all png files in the working directory

```bash
exiftool -all= -overwrite_original -ext png .
```
