Filename: lunarvim.md
Last Change: Sat, 11 Mar 2023 - 08:43:00
tags: [lunarvim, nvim, lua]

## Define your leader key:

  lvim.leader = ','

## make lunarvim mappings not silent (avoid double tapping a keybind like ç or /)

  lvim.keys.normal_mode["ç"] = { ":", silent = false }

  -- undo break
  local break_points = {',', '.', '!', '?', ';', '(', ':', '[', '{', '/'}
  for _, char in ipairs(break_points) do
  lvim.keys.insert_mode[ char ] = char .. "<C-g>u"
  end

  -- hapoon
  lvim.keys.normal_mode["<leader>a"] = ":lua require('harpoon.ui').toggle_quick_menu()<CR>"
  lvim.keys.normal_mode["<leader>m"] = ":lua require('harpoon.mark').add_file(vim.fn.expand('%:p'))<CR>"

  -- line text objects
  lvim.keys.visual_block_mode["al"] = ":<C-u>norm! 0v$<CR>"
  lvim.keys.visual_block_mode["il"] = ":<C-u>norm! _vg_<CR>"

  lvim.keys.visual_mode['<leader>y'] = '"+y:echo "Selection copied to +"<CR>'
  lvim.keys.visual_mode['<leader>Y'] = '"*y:echo "Selection copied to *"<CR>'
  lvim.keys.normal_mode['<leader>p'] = '"+gP'
  lvim.keys.normal_mode['<leader>P'] = '"*gP'

  lvim.keys.normal_mode["gl"] = { "`.zz", desc = 'jump to the last change in the file' }
  lvim.keys.normal_mode["<leader>i"] = { "`^zz" , desc = 'jump to the last insert location but in normal mode' }


  -- map("v", "<LeftRelease>", '"*y' , {silent = true})
  lvim.keys.visual_mode['<LeftRelease>'] = { '"*y', silent = true, desc = "copy selected text to the clipboard"}

