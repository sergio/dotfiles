---
file: dicaslinux.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [linux]
---

## Linux-libre
+ https://en.wikipedia.org/wiki/Linux-libre

Linux-libre is a modified version of the Linux kernel that contains no binary blobs, obfuscated code, or code released under proprietary licenses

## What my display manager is?
+ https://unix.stackexchange.com/questions/20370/

    echo $DESKTOP_SESSION

## What terminal I am using?

    ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)

## Disable secure boot via linux

    systemctl reboot --firmware-setup

## Set keyboard
+ https://edpsblog.wordpress.com/2017/05/16/pequenas-dicas-para-sabayon-e-outros-linux/#more-24739

    setxkbmap -model abnt2 -layout br -variant abnt2

## sysget - A Front-end for Every Package Manager in Linux (package manager)
+ https://www.tecmint.com/sysget-front-end-package-manager-in-linux/

## Instalation

    sudo wget -O /usr/local/bin/sysget https://github.com/emilengler/sysget/releases/download/v1.2.1/sysget
    sudo mkdir -p /usr/local/share/sysget
    sudo chmod a+x /usr/local/bin/sysget

## castero podcast client writen in python

	sudo pip3 install castero

	the x-files podcast http://lsgonxfiles.libsyn.com/rss

## Display mono fonts for your terminal

    fc-list | grep -i mono | less

you can also recreate your fonts cache after instaling any font

    fc-cache -fv

## get wifi info

    sudo lshw -C network

## Restart NetworkManager

    sudo service NetworkManager restart

## Speed Up Slow WiFi Connection In Ubuntu 14.04
+ https://itsfoss.com/speed-up-slow-wifi-connection-ubuntu/

## Solution 1: For Slow WiFi in Atheros Wireless Network Adapters

First, you need to find your wireless network adapter. You can do so by using
lshw -C network command in terminal. If your adapter manufacturer is Atheros,
this solution should work for you.

In order to know if you are using atheros just:

    lsmod | grep ath9k

Open a terminal (Ctrl+Alt+T in Ubuntu) and use the following commands one by one:

    sudo su
    echo "options ath9k nohwcrypt=1" >> /etc/modprobe.d/ath9k.conf

## Quick access to the ascii table

		man ascii

## Enabling full-duplex on your Netcard
+ https://www.cyberciti.biz/faq/howto-setup-linux-lan-card-find-out-full-duplex-half-speed-or-mode/

		sudo pacman -S net-tools

## Consertando o /etc/hosts

    [ "`awk 'NR==1 {print $NF}' /etc/hosts`" = "`hostname`" ] && echo '/etc/hosts já está ok!' || \
    sed -i.backup -r "1s/(.*)/\1 `hostname`/g" /etc/hosts

## Colocando a iso do crunchbang em um pendrive
source: http://crunchbang.org/forums/viewtopic.php?id=23267

WARNING, the following instructions will destroy any existing data on your USB stick.

Determine what device your USB is.  With your USB plugged in run:

	sudo ls -l /dev/disk/by-id/*usb*

This should produce output along the lines of:

    lrwxrwxrwx 1 root root  9 2010-03-15 22:54 /dev/disk/by-id/usb-_USB_DISK_2.0_077508380189-0:0 -> ../../sdb
    lrwxrwxrwx 1 root root 10 2010-03-15 22:54 /dev/disk/by-id/usb-_USB_DISK_2.0_077508380189-0:0-part1 -> ../../sdb1

Now cd to where your *.iso is

	cd ~/downloads

ets say the iso is named mini.iso and your USB device is sdb

Example

	sudo dd if=mini.iso of=/dev/sdb bs=4M; sync

## Ferramenta para criar pendrive bootavel
+ https://www.ventoy.net/en/index.html
+ https://www.youtube.com/watch?v=NOZmSTT7jKM

Ventoy is an open source tool to create bootable USB drive for ISO/WIM/IMG/VHD(x)/EFI files.
start to use ventoy -> https://www.ventoy.net/en/doc_start.html

