---
File: /home/sergio/.dotfiles/wiki/pulseaudio.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

## Dicas de configuração do pulseaudio:
+ https://www.hardware.com.br/comunidade/pulseaudio-audio/1150443/
+ https://www.edivaldobrito.com.br/como-melhorar-o-audio-do-pc/

    sudo apt-get install pulseaudio{,-utils,equalizer-ladspa} opus-tools
    doas ln -s /etc/sv/pulseaudio /var/service

## Supressão de ruido no pulseaudio:
+ https://www.youtube.com/watch?v=9eISgVV5T7M
+ https://www.vivaolinux.com.br/dica/Habilitando-supressao-de-ruido-no-PulseAudio

```sh
sudo vim /etc/pulse/default.pa

# Colar esse código ao final do arquivo:

load-module module-echo-cancel aec_method=webrtc sink_properties=device.description="Noise_Reduction" aec_args="analog_gain_control=0\ digital_gain_control=0"
```


