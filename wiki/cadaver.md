---
file: cadaver.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# cadaver http://webdav.4shared.com/

    cadaver - A command-line WebDAV client for Unix.

    digite seu usuário
    digite sua senha

    cd /diretorio
    mget *.ogv

No gnome shell pode-se acessar usando:

    davs://webdav.4shared.com

