---
File: /home/sergio/.dotfiles/wiki/patch.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
+ https://linuxhint.com/run-patch-command-in-linux/
+ https://www.maketecheasier.com/useful-patches-improve-dwm/
---

patch - apply a diff file to an original
 vim:ft=markdown

    patch -Np1 < patches/patch-name

## How to remove a patch:

    patch -R < /patches/patch-name
