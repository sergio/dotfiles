---
File: idph.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 21, 2020 - 20:47
tags: [english, valter herman, brain]
---

#  idph.md intro:

## Vídeos no youtube
+ [Caos no aprendizado de idiomas](http://bit.ly/37gI7Up)
+ [Desenvolvendo o ouvido](http://bit.ly/39oMgXy)
+ [Deu branco](http://bit.ly/37mnh5X)
+ [Perfeccionismo no aprendizado](http://bit.ly/2UNjrQF)
