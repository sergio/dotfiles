---
File: molho-do-sergio.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 22, 2020 - 05:53
tags: [receitas]
---

#  molho-do-sergio.md intro:

Ingredientes:

    8 Tomates bem maduros
    1 Pimentão
    1 Cebola grande
    3 Dentes de alho
    1 Molho de coentro
    1 Molho de cebolinha
    1 Colher de manteiga de verdade (não vale margarina) (pode substituir com banha)
    Sal a gosto
    Corante (colorau) a gosto
    (pimenta opcional, só pra quem gosta)

MODO DE PREPARO:

Todos os ingredientes devem estar preparados para facilitar a receita,
por exemplo: A cebola o pimentão e o cheiro verde já devem estar picados
na hora de ascender o fogo.

Os tomates podem ser cotados mas nunca passados no liquidificador, caso
contrário o sabor vai ficar aguado, basicamente os tomates serao cozidos num
refogadão.

VAMOS COMECAR:

Coloque a manteiga no fundo do panela,
adicione o alho amassado (aguarde um poucoo)
adicione o pimentão
adicione a cebola (tampando a panela a cebola solta mais água)
coloque o um pouco do cheiro verde

Se o refogado estiver muito seco pode colocar um tiquinho de água ou uns
pedaços do tomate já cortado.

Quando você perceber que o refogado já está dourando adicioe os tomates e dê
uma mexida. De vez em quando mexa para ver se o tomate não está grudando,
mas geralmente à medida que ele cozinha ele vai soltando bastante água.

Quando os tomates estiverem bem moles adicione o restante do cheiro verde.
O ponto é parecido com o de doce, ao mexer a colher você começa a ver o fundo
da panela.

DICAS:

Pra quem gosta de pimenta eu aconselho pilar pimenta do reino na hora e
colocar já no prato, o aroma e o sabor ficam melhores.

Outra dica: Não misture esse molho ao macarrão, coloque o macarrão no prato
e com uma concha coloque o molho sobre o macarrão já no prato.

Para dar mais sabor adquira Queijo parmesão (daqueles de saquinho)
e pode usar orégano, também colocando no prato.

Pode também colocar orégano na hora de servir (sobre o prato)

