---
file: regex.md
author: Sergio Araujo
Last Change: Mon, 23 Dec 2024 - 10:43:19
tags: [regex]
---

# This page has some tips about Regular Expressions
* [regex - best practices](http://www.softpanorama.org/Scripting/Perlbook/Ch05/best_practices.shtml)
* [vim regexes are awesome](vim-regexes-are-awesome.md)
* [another vim regexes article](http://andrewradev.com/2011/05/08/vim-regexes/)
* [vim regexes](vim-regexes.md)

## See also [dicasvim](dicasvim.md) to learn about regex on vim

## Validate hour HH:MM:SS 24-hour format with leading 0
+ https://stackoverflow.com/a/51177696/2571881

    (?:[01]\d|2[0-3]):(?:[0-5]\d):(?:[0-5]\d)
