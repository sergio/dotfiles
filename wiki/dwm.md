---
File: /home/sergio/.dotfiles/wiki/dwm.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [desktop managers, dwm, dec2bin, bc]
---

## dwm links:

+ https://www.youtube.com/watch?v=7pfxHoAnCkE (distrotub video)
+ https://blog.juancastro.xyz/en/posts/dwm/ (great article)
+ https://github.com/skuzzymiglet/dwm/blob/master/config.h
+ https://github.com/tallguyjenks/dwm/blob/main/config.h


## dependencies

```sh
# Install core dependencies
sudo xbps-install -Sy base-devel libX11-devel libXft-devel libXinerama-devel freetype-devel fontconfig-devel

# Install common programs
sudo xbps-install -Sy dmenu slock xsetroot xrandr xrdb feh scrot picom nitrogen alsa-utils pulseaudio blueman udiskie lxappearance rofi xfce4-power-manager brightnessctl playerctl htop ranger neovim git curl wget xdotool xclip sxiv dunst cronie xset xautolock redshift
```

The dwm and st theirselves i install modified versions


##  Existe uma alternativa ao dwm voltada para wayland [dwl](dwl.md)

Para que o script de autostart carregue crie uma pasta
~/.dwm contendo o script de autostart

## git repository
Consider also using https://larbs.xyz/

    git clone git://git.suckless.org/dwm

Currently I am using the distrotube build with some changes.
I am also testing the flexypatch build:

+ https://github.com/bakkeby/dwm-flexipatch

## Some programs we need:
+ https://github.com/CameronNemo/brillo

    brillo sxiv dunst

## Jump to the next active window when I close the last window

**Every time I close the last window in a certain workspace the dwm jumps to the next active window.**

I asked to our beloved bard.google.com:
dwm linux - when close last window jump to the next active workspace

**and he said:**
To make dwm jump to the next active workspace when you close the last window in the current workspace, you can add the following line to your config.h file:

    #define SWAPWORKSPACESONLASTWINDOWCLOSE 1

Once you have added this line, recompile and install dwm. Now, whenever you close the last window in a workspace, dwm will automatically switch to the next active workspace.

## rules
These rules ensures each new program will open on its window

```c
static const Rule rules[] = {
    /* xprop(1):
     *  WM_CLASS(STRING) = instance, class
     *  WM_NAME(STRING) = title
     */

    /* class              instance    title       tags mask     switchtotag    isfloating   monitor */
    { "Gimp",             NULL,       NULL,       0,            0,             1,           -1 },
    { "Firefox",          NULL,       NULL,       2 << 0,       1,             0,           -1 },
    { "TelegramDesktop",  NULL,       NULL,       1 << 4,       1,             0,           -1 },
    { "ranger",           NULL,       NULL,       4 << 0,       1,             0,           -1 },
    { "pulse",            NULL,       NULL,       0,            0,             1,           -1 },
    { "St",               NULL,       NULL,       1 << 0,       1,             0,           -1 },
    { "term2",            NULL,       NULL,       0,            0,             0,           -1 },
    { "htop",             NULL,       NULL,       0,            0,             1,           -1 },
    { "scratchpad",       NULL,       NULL,       0,            0,             1,           -1 },
    { "scratchy",         NULL,       NULL,       0,            0,             1,           -1 },
    { "nvim",             NULL,       NULL,       1 << 8,       1,             0,           -1 },
    { "newsboat",         NULL,       NULL,       1 << 5,       1,             1,           -1 },
    { "Nemo",             NULL,       NULL,       1 << 7,       1,             0,           -1 },
    { "Firefox",          "Places",   "Library",  0,            0,             1,           -1 },
};
```

## ideas to improve dwm:

Map Super + q to close applications, it is more ernonomic

## patches:

    dwm-alwayscenter-20200625-f04cac6.diff
    dwm-attachbottom-6.2.diff
    dwm-autostart-20210120-cb3f58a.diff
    dwm-cfacts-20200913-61bb8b2.diff
    dwm-pertag-20200914-61bb8b2.diff
    dwm-rotatestack-20161021-ab9571b.diff
    dwm-scratchpad-6.2.diff
    dwm-switchtotag-6.2.diff
    dwm-uselessgap-20211119-58414bee958f2.diff
    dwm-tagshift-6.3.diff

## Rules

    echo $((1<<8)) | dec2bin | paddy 9
    100000000

    1<<8  = 100000000 ................ first tag
    1<<7  = 010000000 ................ second
    1<<6  = 001000000 ................ third tag
    1<<5  = 000100000 ................ fourth tag
    1<<4  = 000010000 ................ fifth tag
    1<<3  = 000001000 ................ sixth tag

## Swich to the last active tag

    super + alt + j
	{1, {{MODKEY|Mod1Mask, XK_j}},		  view,           {0} },


## Add dwm in your login manager:
+ https://www.reddit.com/r/suckless/comments/jj61py/

    sudo cat <<-EOF > /usr/share/xsessions/dwm.desktop
    [Desktop Entry]
    Encoding=UTF-8
    Name=dwm
    Comment=Dynamic window manager
    Exec=dwm
    Icon=dwm
    Type=XSession
    EOF

    doas chmod 644 /usr/share/xsessions/dwm.desktop

## dwmblocks - dwm status bar
+ https://www.youtube.com/watch?v=h2op4QqIuHk
+ https://www.youtube.com/watch?v=NNwcCPVwr2A
+ https://github.com/gerardet46/dwmblocks

## Paste primary selection on mouse click:

    { ClkClientWin,   ControlMask|Mod1Mask,  Button1,   spawn,    SHCMD("xdotool click --window $(xdotool getactivewindow) --clearmodifiers 2") },

## Default keybindings:

      MODKEY + Shift + Space .............. toggle fullscript
      MODKEY + leftClick .................. move window
      MODKEY + p .......................... launch dmenu

## how to configure dwm:
+ https://dev.to/knassar702/the-best-way-to-configure-your-dwm-5efn
+ https://ratfactor.com/dwm2

## Luck Smith dwm:

    Mod + F1  ................ display help
    Super + F ................ toggle full screen
    q ........................ to quit

    Notice also the case sensitivity of the shortcuts* , Be sure you play around with these. Be
flexible with the basic commands and the system will grow on you quick.

    • Mod+Enter – Spawn terminal (the default terminal is st; run man st for more.)
    • Mod+q – Close window
    • Mod+d – dmenu (For running commands or programs without shortcuts)
    • Mod+j/k – Cycle thru windows by their stack order
    • Mod+Space – Make selected window the master (or switch master with 2nd)
    • Mod+h/l – Change width of master window
    • Mod+z/x – Increase/decrease gaps (may also hold Mod and scroll mouse)
    • Mod+a – Toggle gaps
    • Mod+A – Gaps return to default values (may also hold Mod and middle click)
    • Mod+Shift+Space – Make a window float (move and resize with Mod+left/right click).

• Mod+s – Make/unmake a window “sticky” (follows you from tag to tag)
• Mod+b – Toggle statusbar (may also middle click on desktop)
• Mod+v – Jump to master windo

    Basic Programs
    • Mod+r – lf (file browser/manager)
    • Mod+R – htop (task manager, system monitor that R*dditors use to look cool)
    • Mod+e – neomutt (email) – Must be first configured by running mw add.
    • Mod+E – abook (contacts, addressbook, emails)
    • Mod+m – ncmpcpp (music player)
    • Mod+w – Web browser (Brave by default)
    • Mod+W – nmtui (for connecting to wireless internet)
    • Mod+n – vimwiki (for notes)
    • Mod+N – newsboat (RSS feed reader)
    • Mod+F4 – pulsemixer (audio system control)
    • Mod+Shift+Enter – Show/hide dropdown terminal
    • Mod+’ – Show/hide dropdown calculator
    • Mod+D – passmenu (password manager)
    Tags/Workspaces

    Tags/Workspaces
    There are nine tags, active tags are highlighted in the top left.
    • Mod+(Number) – Go to that number tag
    • Mod+Shift+(Number) – Send window to that tag
    • Mod+Tab – Go to previous tag (may also use \ for Tab)
    • Mod+g – Go to left tag (hold shift to send window there)
    • Mod+; – Go to right tag (hold shift to send window there)

    Audio
    I use ncmpcpp as a music player, which is a front end for mpd.
    • Mod+m – ncmpcpp, the music player
    • Mod+. – Next track
    • Mod+, – Previous track
    • Mod+< – Restart track
    • Mod+> – Toggle playlist looping
    • Mod+p – Toggle pause
    • Mod+p – Force pause music player daemon and all mpv videos
    • Mod+M – Mute all audio
    • Mod+- – Decrease volume (holding shift increases amount)
    • Mod++ – Increase volume (holding shift increases amount)
    • Mod+[ – Back 10 seconds (holding shift moves by one minute)
    • Mod+] – Forward 10 seconds (holding shift moves by one minute)
    • Mod+F4 – pulsemixer (general audio/volume sink/source control)
    Recording

    Recording
    I use maim and ffmpeg to make different recordings of the desktop and audio. All of these
    recording shortcuts will output into ˜, and will not overwrite previous recordings as their names ## My personal keymaps for dwm
OBS: My MODKEY is the windows key

    Shift + Alt + j ........................ decrease window
    Shift + Alt + k   ...................... increase window
    Windows + Alt + q ...................... exit
    Windows + Ctrl + Shift + q ............. restart (it needs a patch)
    Windows + Shift + j .................... move window clockwise
    Windows + Shift + h .................... decrese window
    Windows + Shift + l .................... increase window
    Windows + m ............................ monocle mode
    Windows + t ............................ tiling mode
    Windows + j ............................ next window
    Windows + k ............................ previou window
    Windows + l/h .......................... decrease window
    Windows + i ............................ change layout horizontal (add to master)
    Windows + d ............................ change layout vertical (remove to master)
    Windows + v  ........................... nvim
    Control + Alt + i ...................... img  command
    Windows + Shift + i .................... inkscape
    Windows + Shift + 1-9 .................. move app to another workspace
    Windows + 0 ............................ view all windows on the screen
    Windows + 1 ............................ view windows in their original screen

     ColtrolMas = control
     Mod1Mask   = alt
     XK_i       = i
     MODKEY     = windows

## Meus atalhos do dwm

Este arquivo contém os atalhos configurados no DWM, organizados por categorias para facilitar a consulta.

---

## **Atalhos Gerais**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + p`              | Abrir dmenu                               |
| `MODKEY + Return`         | Abrir terminal (st)                       |
| `Control + F7`            | Abrir terminal secundário (term2)         |
| `MODKEY + w`              | Abrir Firefox                             |
| `MODKEY + n`              | Abrir Nemo (gerenciador de arquivos)      |
| `MODKEY + Shift + n`      | Abrir Newsboat                            |
| `MODKEY + Shift + t`      | Abrir htop                                |
| `MODKEY + r`              | Abrir Ranger                              |
| `Control + F8`            | Abrir Ranger2                             |
| `MODKEY + Shift + u`      | Alternar scratchpad                       |
| `MODKEY + b`              | Alternar visibilidade da barra            |
| `MODKEY + q`              | Fechar janela                             |
| `MODKEY + Shift + c`      | Fechar janela                             |
| `MODKEY + Shift + q`      | Sair do DWM                               |
| `MODKEY + Control + Shift + q` | Sair do DWM (forçado)                |

---

## **Gerenciamento de Janelas**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + j`              | Focar próxima janela                     |
| `MODKEY + k`              | Focar janela anterior                    |
| `MODKEY + Tab`            | Alternar entre janelas                   |
| `MODKEY + space`          | Alternar layout                          |
| `MODKEY + Shift + space`  | Alternar entre flutuante e tiling        |
| `MODKEY + h`              | Diminuir tamanho da área mestre          |
| `MODKEY + l`              | Aumentar tamanho da área mestre          |
| `MODKEY + Shift + h`      | Aumentar fator de crescimento da janela  |
| `MODKEY + Shift + l`      | Diminuir fator de crescimento da janela  |
| `MODKEY + Shift + Return` | Zoom na janela                           |
| `MODKEY + t`              | Layout Tile                              |
| `MODKEY + f`              | Layout Flutuante                         |
| `MODKEY + m`              | Layout Monocle                           |
| `MODKEY + g`              | Layout Grid                              |

---

## **Gerenciamento de Tags**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + 1-9`            | Mudar para a tag 1-9                     |
| `MODKEY + Shift + 1-9`    | Mover janela para a tag 1-9              |
| `MODKEY + 0`              | Visualizar todas as tags                 |
| `MODKEY + Shift + 0`      | Mover janela para todas as tags          |
| `MODKEY + ,`              | Focar monitor anterior                   |
| `MODKEY + .`              | Focar próximo monitor                    |
| `MODKEY + Shift + ,`      | Mover janela para o monitor anterior     |
| `MODKEY + Shift + .`      | Mover janela para o próximo monitor      |

---

## **Volume e Brilho**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `XF86XK_AudioRaiseVolume` | Aumentar volume                          |
| `XF86XK_AudioLowerVolume` | Diminuir volume                          |
| `XF86XK_AudioMute`        | Mutar volume                             |
| `MODKEY + F11`            | Diminuir volume                          |
| `MODKEY + F12`            | Aumentar volume                          |
| `MODKEY + Shift + F12`    | Mutar volume                             |
| `XF86XK_MonBrightnessUp`  | Aumentar brilho                          |
| `XF86XK_MonBrightnessDown`| Diminuir brilho                          |

---

## **Outros Atalhos**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + x`              | Abrir menu de energia                    |
| `MODKEY + Print`          | Captura de tela                          |
| `Control + Print`         | Captura de tela em tela cheia            |
| `MODKEY + Shift + i`      | Abrir Inkscape                           |
| `MODKEY + v`              | Abrir Neovim                             |
| `MODKEY + Shift + o`      | Resetar fator de crescimento da janela   |
| `MODKEY + u`              | Focar janela urgente                     |
| `Control + Mod1 + Escape` | Abrir xkill                              |
| `Control + F12`           | Desligar tela                            |
| `MODKEY + F4`             | Alternar screenkey                       |

---

## **Rofi e Menus**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `Control + Mod1 + r`      | Abrir Rofi (launcher)                    |
| `Control + Mod1 + f`      | Abrir Rofi (navegador de arquivos)       |
| `Control + Mod1 + c`      | Abrir clipmenu                           |
| `Control + Mod1 + b`      | Abrir menu de fundo (feh)                |
| `Control + Mod1 + p`      | Abrir menu de energia (Rofi)             |
| `Control + Mod1 + t`      | Abrir Telegram                           |
| `Control + Mod1 + e`      | Editar wiki                              |
| `Control + Mod1 + d`      | Abrir DRadio                             |

---

## **Comandos Personalizados**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + Shift + j`      | Rotacionar pilha de janelas (+1)         |
| `MODKEY + Shift + k`      | Rotacionar pilha de janelas (-1)         |
| `MODKEY + Mod1 + k`       | Matar processo via Rofi                  |
| `MODKEY + Mod1 + m`       | Tocar MPV                                |
| `MODKEY + Mod1 + c`       | Abrir calendário                         |
| `MODKEY + Mod1 + b`       | Abrir menu de bateria                    |
| `MODKEY + Mod1 + d`       | Fechar notificação (Dunst)               |

---

## **Teclas de Função**

| Tecla                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + F2`             | Iniciar gravação de tela                 |
| `MODKEY + F3`             | Parar gravação de tela                   |
| `MODKEY + Control + F6`   | Iniciar dumpceararural                   |
| `MODKEY + Control + F7`   | Parar dumpceararural                     |

---

## **Botões do Mouse**

| Botão                     | Ação                                      |
|---------------------------|-------------------------------------------|
| `MODKEY + Botão1`         | Mover janela                              |
| `MODKEY + Botão2`         | Alternar flutuante                        |
| `MODKEY + Botão3`         | Redimensionar janela                      |
| `ClkTagBar + Botão1`      | Visualizar tag                            |
| `ClkTagBar + Botão3`      | Alternar visualização da tag              |
| `ClkLtSymbol + Botão1`    | Alternar layout                           |
| `ClkLtSymbol + Botão3`    | Definir layout Monocle                    |

---

### **Observações:**
- `MODKEY` é geralmente a tecla `Super` (Windows) ou `Alt`.
- `Control + Mod1` é `Control + Alt`.
- `Shift` é a tecla de shift.

