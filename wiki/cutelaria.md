
# Dicas de cutelaria

+ [benchmade folding knife frame](https://youtu.be/lOjJ57Hra0w?t=451)

## Sobre a licença da benchmade

The **Axis Lock** mechanism, initially patented by Benchmade, was covered under a U.S. patent (US Patent No. 5,737,841), which was granted in 1998. However, patents typically last 20 years from the date of filing, meaning the **U.S. patent expired in 2018**.

Regarding international patents, the protection would depend on whether Benchmade filed for patent protection in other countries and regions using the **Patent Cooperation Treaty (PCT)** or directly in each country.

1. **Regions where the Axis Lock patent may have been active**:
   - Patents are generally enforceable in the countries where the patent holder files for them. While the Axis Lock patent expired in the U.S., it may have been protected in other regions if Benchmade applied for international patents. 
   - Common regions that companies may seek protection include **Europe, Japan, Canada, China, and Australia**.

2. **Brazil**:
   - To check if the Axis Lock is still protected in Brazil, you'd need to investigate whether Benchmade filed for patent protection there. This can be done by searching through the **Brazilian National Institute of Industrial Property (INPI)** database.
   - However, if Benchmade did file for a patent in Brazil in the same timeframe, it would likely have expired around 2018, as patents also last for 20 years in Brazil (similar to U.S. law).

### Key Steps to Check:

- **Check INPI Database**: You can perform a search on Brazil's INPI website to see if a patent for the Axis Lock mechanism exists and whether it's still active.
- **Patent Expiry**: If the patent was filed around the same time as the U.S. patent, it would likely be expired in Brazil too.

In summary, the **Axis Lock patent is likely expired** in most regions, including Brazil, but you should verify through local patent offices or consult a legal expert to be certain.
