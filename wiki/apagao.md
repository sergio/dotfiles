---
File: ~/.dotfiles/wiki/apagao.md
Last Change: Wed, 31 Jan 2024 - 02:25:11
tags: [artigos, espiritualidade]
---

# A lógica por tráz

A tecnologia, a dispeito do seu largo uso em favor das mais nobres
causas humanas, tem sido instrumento de enganação e opressão sistemáticos. Recentemente duas guerras, as que mais chamam a atenção, e não por acaso com o objetivo de manter o império da guerra e dos combustíveis fósseis,  se aprofundam sem o menor pudor.

Se há um momento na história humana, em que uma intervenção divina seria providencial, podemos dizer com absoluta segurança que esse é um desses momentos. Ocorre que, deliberadamente, forçar o planeta a uma ecatombe parece ser uma escolha da OTAN, nas duas frentes mais belicosas.

Por tudo isso, creio, estamos muitíssimo próximos dos dias de escuridão, em que a tecnologia, será momentaneamente suprimida pela espiritualidade maior a fim de reestabelecer a verdade. Exultemos, desconforto haverá, mas esse momento é também um momento de um grande degredo, em que bilhões de seres, os mais relutantes frente às reformas morais íntimas deixarão a terra, e para muitos já será perceptível, uma mudança na aura do planeta.
