---
file: popd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---



acessa o último diretório colocado na pilha de acesso através
do comando [[pushd]].
