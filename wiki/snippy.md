---
Filename: snippy.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [nvim, snippets]
 vim:ft=markdown
---

## A snippets plugin for Neovim 0.5.0+ written in Lua.
+ https://github.com/dcampos/nvim-snippy

See also https://github.com/dcampos/cmp-snippy
nvim-snippy completion source for nvim-cmp.
