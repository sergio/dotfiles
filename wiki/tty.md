---
file: tty.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Ao digitar o comando tty num terminal ele gera uma saída como esta

    /dev/pts/0

