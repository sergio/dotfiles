---
file: ffmpeg2theora.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Converter video flv para ogv

ffmpeg2theora -V 200 -A 64 test.flv

## Converter ogv para flv

ffmpeg -i input.ogv -ar 44100 -acodec libmp3lame output.flv

