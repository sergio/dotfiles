---
author: Sergio Araujo
 - 13:14
Last Change: Mon, 19 Feb 2024 - 14:05:58

# Referências:
+ http://www.emacswiki.org/emacs-pt/NovatoNoEmacs
+ https://blog.aaronbieber.com/2015/07/05/a-gentle-introduction-to-emacs-configuration.html
+ http://www.jesshamrick.com/2012/09/10/absolute-beginners-guide-to-emacs/
+ https://www.emacswiki.org/emacs/Evil
+ https://ogbe.net/emacsconfig.html
+ https://www.youtube.com/watch?v=Iagbv974GlQ
+ https://www.youtube.com/watch?v=JWD1Fpdd4Pc
+ https://ambrevar.xyz/emacs-everywhere/
+ http://pragmaticemacs.com/installing-and-setting-up-emacs/
+ video on youtube: https://www.youtube.com/watch?v=74zOY-vgkyw
+ https://codesource.io/top-50-emacs-commands-and-emacs-shortcuts/
+ https://emacsredux.com/blog/2013/04/28/execute-commands-ninja-style-with-key-chord-mode

## Emacs stuf I think are better than in vim

1 - Toggling indetation positions of the cursor with C-i

## opening file under cursor

Just type C-x C-f as if you were opening any other file

## Increment numbers like in vim

```elisp
;; Increase numbers like in vim
;; https://emacs.stackexchange.com/a/41920/18789
(defun increment-number-at-point ()
  (interactive)
  (let ((old-point (point)))
    (unwind-protect
        (progn
          (skip-chars-backward "0-9")
          (or (looking-at "[0-9]+")
              (error "No number at point"))
          (replace-match (number-to-string (1+ (string-to-number (match-string 0))))))
      (goto-char old-point))))

(global-set-key (kbd "C-x C-a") 'increment-number-at-point)
```

## mark current word (select current word)

    Just use -> M-j

You can also use M-b or M-f to extend your selection in oposite direction
It is also possible to jump to the oposite site of your selection (as you do
in vim with 'o') just use C-x C-x.

## setting window size

    ;; window size
    ;; https://www.emacswiki.org/emacs/FrameSize
    (when window-system (set-frame-size (selected-frame) 80 24))

## squeeze blank lines

```elisp
;; http://ergoemacs.org/emacs/elisp_compact_empty_lines.html
(defun xah-clean-whitespace ()
  "Delete trailing whitespace, and replace repeated blank lines to just 1.
Only space and tab is considered whitespace here.
Works on whole buffer or text selection, respects `narrow-to-region'.

URL `http://ergoemacs.org/emacs/elisp_compact_empty_lines.html'
Version 2017-09-22 2020-09-08"
  (interactive)
  (let ($begin $end)
    (if (use-region-p)
        (setq $begin (region-beginning) $end (region-end))
      (setq $begin (point-min) $end (point-max)))
    (save-excursion
      (save-restriction
        (narrow-to-region $begin $end)
        (progn
          (goto-char (point-min))
          (while (re-search-forward "[ \t]+\n" nil "move")
            (replace-match "\n")))
        (progn
          (goto-char (point-min))
          (while (re-search-forward "\n\n\n+" nil "move")
            (replace-match "\n\n")))
        (progn
          (goto-char (point-max))
          (while (equal (char-before) 32) ; char 32 is space
            (delete-char -1))))
      (message "white space cleaned"))))

;; adding hook to call xah-clean-whitespace
(add-hook 'before-save-hook 'xah-clean-whitespace)
(global-set-key (kbd "s-d") 'xah-clean-whitespace)
```

## delete current paragraph

```lisp
;; http://ergoemacs.org/emacs/emacs_delete_block.html
(defun xah-delete-current-text-block ()
  "Delete the current text block or selection, and copy to `kill-ring'.
A “block” is text between blank lines.

URL `http://ergoemacs.org/emacs/emacs_delete_block.html'
Version 2017-07-09"
  (interactive)
  (let ($p1 $p2)
    (if (use-region-p)
        (setq $p1 (region-beginning) $p2 (region-end))
      (progn
        (if (re-search-backward "\n[ \t]*\n+" nil "move")
            (progn (re-search-forward "\n[ \t]*\n+")
                   (setq $p1 (point)))
          (setq $p1 (point)))
        (re-search-forward "\n[ \t]*\n" nil "move")
        (setq $p2 (point))))
    (kill-region $p1 $p2)))

(global-set-key (kbd "C-x p") 'xah-delete-current-text-block)
```

## Registers in Emacs
+ https://www.gnu.org/software/emacs/manual/html_mono/emacs.html#Registers

A register can store a position, a piece of text, a rectangle, a
number, a window configuration, or a file name, but only one thing at
any given time. Whatever you store in a register remains there until
you store something else in that register. To see what register r
contains, use M-x view-register:

## plugins:

1 - avy (jump easily on emacs) https://github.com/abo-abo/avy

;; zap-to-char

	M-z ........... similar to vim df'char

## Using the key-ring

After copying things around when you want to paste something
you can type C-y then M-y, this will circle throught all kill ring

## Eval function

    M-:

## function to open init.el

   (global-set-key (kbd "<f6>") (lambda() (interactive)(find-file "~/.emacs")))

## Indent current line and deindent

	C-i

```lisp
(defun my-indent-region (N)
  (interactive "p")
  (if (use-region-p)
      (progn (indent-rigidly (region-beginning) (region-end) (* N 4))
             (setq deactivate-mark nil))
    (self-insert-command N)))

(defun my-unindent-region (N)
  (interactive "p")
  (if (use-region-p)
      (progn (indent-rigidly (region-beginning) (region-end) (* N -4))
             (setq deactivate-mark nil))
    (self-insert-command N)))

(global-set-key (kbd "C-<") 'my-indent-region)
(global-set-key (kbd "C->") 'my-unindent-region)
```

## Using help:

    ctrl-h ?

or if you know what key under help you should use you can type, for example:

    ctrl-h f  (to get description of any given function)

## mapping for next and previous paragraphs

``` lisp
;; Move up/down paragraph
(global-set-key (kbd "M-n") #'forward-paragraph)
(global-set-key (kbd "M-p") #'backward-paragraph)
```

## restore cursor position

``` lisp
;; https://emacs.stackexchange.com/a/12712/18789
(use-package saveplace
  :init (save-place-mode))
```

## function to mark paragraph

``` lisp
(defun dg-mark-paragraph ()
  (interactive)
  (mark-paragraph)
  (goto-char (region-beginning))
  (when (= (string-match paragraph-separate (thing-at-point 'line)) 0)
    (forward-line)))
```

## newline

Emacs has a function called 'newline'

## kill current line (decrease) (delete current line)

    C-S-backspace   # Ctrl-Shift-Backspace

## Retangular modifications like in vim C-v

Hit c-return to start a block selection and use this:

http://ergoemacs.org/emacs/emacs_string-rectangle_ascii-art.html

## show filename:
+ https://stackoverflow.com/a/3669681/2571881

``` lisp
(setq frame-title-format
        (list (format "%s %%S: %%j " (system-name))
        '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))
```

You can also do something like this :

``` lisp
(defun show-file-name ()
    "Show the full path file name in the minibuffer."
    (interactive)
    (message (buffer-file-name)))

(global-set-key [C-f1] 'show-file-name) ; Or any other key you want
```

## ido-mode
+ https://masteringemacs.org/article/introduction-to-ido-mode

“Interactively DO things”

There are many ways of improving your productivity when you use Emacs, and Ido
(or “Interactively DO things”) is one of those packages that you enable and
then never, ever turn off again. It’s simply that useful. By super-charging
Emacs’s completion engine and improving the speed at which you open files and
buffers, you will significantly cut down on the time spent doing these menial
tasks.

Most ido commands are named “ido-xxxxx”; so find-file becomes ido-find-file, and so on.

## start

    emacs -Q ..................... start without opening screen

    Abrir arquivo ................ C-x C-f
    Fechar arquivo ............... C-x k
    Abrir arquivo alternativo..... C-x C-v
    Abrir a ajuda do emacs ....... C-h r

    Para fechar ............ C-x c
    para salvar ............ C-x s
    para salvar como ....... C-x C-w

    Command line like vim :  M-x

## Start Emacs

   	start emacs in text terminal

	emacs --no-window-system

    ;; setting a different emacs config file
    emacs -q -l ~/.emacs2

## or to open it on the terminal emulator

    emacs -nw

Carregar um tema escuro

    M-x load-theme
    M-x load-theme RET

    manojo<tab>

    Customise-theme ......... M-x customize-th <tab>

    next line ............. C-n
    previous line ......... C-p

## beginning of line or first non blank

    (defun smarter-move-beginning-of-line (arg)
    "Move point back to indentation of beginning of line.

    Move point to the first non-whitespace character on this line.
    If point is already there, move to the beginning of the line.
    Effectively toggle between the first non-whitespace character and
    the beginning of the line.

    If ARG is not nil or 1, move forward ARG - 1 lines first.  If
    point reaches the beginning or end of the buffer, stop there."
    (interactive "^p")
    (setq arg (or arg 1))

    ;; Move lines first
    (when (/= arg 1)
        (let ((line-move-visual nil))
        (forward-line (1- arg))))

    (let ((orig-point (point)))
        (back-to-indentation)
        (when (= orig-point (point))
        (move-beginning-of-line 1))))

    ;; remap C-a to `smarter-move-beginning-of-line'

## Function to select current line

```lisp
;; https://emacs.stackexchange.com/a/59303/18789
(defun mark-whole-line ()
  (beginning-of-line)
  (set-mark-command nil)
  (end-of-line))
```

## Reselect region

```elisp
(defun reselect-last-region ()
  (interactive)
  (let ((start (mark t))
        (end (point)))
    (goto-char start)
    (call-interactively' set-mark-command)
    (goto-char end)))
```

## to comment or uncomment a block

   M-h .............. select paragraph
   M-; .............. toggles comments

## enable relative line number

    (setq display-line-numbers 'relative)

## Setting font

    M-x menu-set-font

    ;; font setting
    (set-default-font "FuraMono Nerd Font 14" nil t)

## Increase font size

    C-x C-+

## god-mode

``` lisp
;; https://github.com/emacsorphanage/god-mode
(use-package god-mode
  :ensure t
  :bind ("<escape>" . #'god-local-mode))
```

## Disable welcome screen

    (setq inhibit-startup-screen t)

In order to evaluate this expression right away just go to
the end of the expression and press

    C-x C-e

## copy from clipboard
+ https://stackoverflow.com/a/9986416/2571881

    M-x clipboard-yank .... paste from clipboard
    C-y ................... paste from clipboard
    Shift-Del ............. cut
    Control-Insert ........ copy
    Shift-Insert .......... paste

c-space ..............  start selection
c-e ..................  til the end of line
M-w  .................. copy

## open a new line below
+ https://stackoverflow.com/questions/5898448/

c-e c-o

or using a function

``` lisp
;; Insert new line above current line
;; and move cursor to previous line (newly inserted line)
;; it will also indent newline
;; TODO: right now I am unable to goto previous line, FIXIT
(global-set-key (kbd "<C-S-return>") (lambda ()
(interactive)
(beginning-of-line)
(newline-and-indent)
(previous-line)))
```

``` lisp
;; https://bit.ly/3s3Wx5z
(defun insert-new-line-below ()
  "Add a new line below the current line"
  (interactive)
  (let ((oldpos (point)))
    (end-of-line)
    (newline-and-indent)))

(global-set-key (kbd "C-o") 'insert-new-line-below)
```

## jump to the previous buffer

``` lisp
(defun er-switch-to-previous-buffer ()
  "Switch to previously open buffer.
Repeated invocations toggle between the two most recently open buffers."
  (interactive)
  (switch-to-buffer (other-buffer (current-buffer) 1)))

(global-set-key (kbd "C-c b") #'er-switch-to-previous-buffer)

```

## Copy line

C-S-Backspace C-y

‘C-a C-SPC C-e M-w’ copies the current line without the newline.
‘C-a C-SPC C-n M-w’ copies the current line, including the newline.
‘C-a C-k C-k C-y’ kills the line with newline and yanks it back, in effect saving it to the kill ring.
‘C-a C-k C-y’ kills and yanks back the line without newline if kill-whole-line is nil, or including the newline otherwise.
‘C-S-backspace C-y’ Kills an entire line at once (kill-whole-line) and yanks it back.

## Creating new buffer

C-x k ............ kill current buffer (discard)
C-x b ............ create new buffer

    (defun new-empty-buffer ()
      "Creates a new buffer in current window"
      (interactive)
      (let ((-buf (generate-new-buffer "untitled")))
        (switch-to-buffer -buf)
        (funcall initial-major-mode)
        (setq buffer-offer-save t)))

	(global-set-key (kbd "<f2>") 'new-empty-buffer)

## Install new packages

```elisp
;; ================== add melpa repo ==================================
;; add to the end of your ~/.emacs
(add-to-list 'package-archives
'("marmalade" . "http://marmalade-repo.org/packages/") t)
```

So you can run

```elisp
M-x list-packages
M-x package-refresh-contents
```

to list all packages which will automatically refresh the archive contents.
+ https://www.emacswiki.org/emacs/Evil
+ http://www.makble.com/how-to-toggle-evil-mode-in-emacs

M-x `package-install` `<RET>` evil `<RET>`

(require 'evil)
(evil-mode 1)

If you are in evil mode press `Ctrl-z` to toggle to Emacs mode

## better goto line (go to line)

    ;; better goto line
    (global-set-key (kbd "C-M-g")   'goto-line)

## ;; Emacs - highlighting the current word

```elisp
;; http://chopmo.dk/2016/10/27/emacs-highlighting-current-word.html
;; Emacs - highlighting the current word
(require 'hi-lock)
(defun jpt-toggle-mark-word-at-point ()
  (interactive)
  (if hi-lock-interactive-patterns
      (unhighlight-regexp (car (car hi-lock-interactive-patterns)))
    (highlight-symbol-at-point)))
```

## Open customize options

M-x customize

## make esc exit prompts

```elisp
;; Make ESC quit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)
```

## delete previous word

     M-backspace .... deletes previous word
     M-d ............ deletes word ahead

## Selections

	Select whole file ........... C-x h
    Select more lines ........... C-Spacebar (C-n or C-p)

## single lines only

    ;; select the buffer and run M-x single-lines-only
    (defun single-lines-only ()
    "replace multiple blank lines with a single one"
    (interactive)
    (goto-char (point-min))
    (while (re-search-forward "\\(^\\s-*$\\)\n" nil t)
        (replace-match "\n")
        (forward-char 1)))

## almost equivalente to zz zt zb

    M-r

## set use-package

``` lisp
(require 'package)

(add-to-list 'package-archives
'("melpa" . "http://melpa.org/packages/") t)

(package-initialize)
(unless (package-installed-p 'use-package)
(package-refresh-contents)
(package-install 'use-package))
```

## auto-complete

    (use-package auto-complete
    :ensure t
    :init
    (progn
        (ac-config-default)
        (global-auto-complete-mode t)))

## jumps

    M-m .................... first non-blank
    M-< .................... start of the file
    M-> .................... end of the file
    C-u C-SPC .............. jumps back like vim's C-o
    M-a .................... previous paragraph
    M-e .................... next paragraph
    M-v .................... Pular para a janela anterior
    C-v .................... Pular para a próxima janela
    C-x C-v  ............... Abrir arquivo alternativo
    C-u C-space ............ last position

## Setting marks to delete regions
+ https://stackoverflow.com/a/15998341/2571881

    C-<SPC> ....... emacs will show MARK SET
    move arround
    c-w ........... deletes the whole region

    For example to delete until the beginning of the file

    C-<SPC> ................ set mark
    Esc< ................... beginning of the buffer
    C-w .................... kill region

## Some similar features of vim/emacs

    k .............. ^p
    j .............. ^n
    zz  ............ ^l

    Actually ^l in Emacs does more, it alternates between top, middle and
    botton screen

## Incremental Search

    C-s
    C-s C-s ....... repeat last search
    C-g ........... cancel and go back where you started

          To continue searching just press c-s again

	  C-r ............. search backwards

## Transpose chars
+ http://pragmaticemacs.com/emacs/transpose-characters/

That's a native emacs feature (just in case...)

    ;; adjust transpose-chars to switch previous two characters
    (global-set-key (kbd "C-t")
                    (lambda () (interactive)
                      (backward-char)
                      (transpose-chars 1)))

## Inserting new line

I am using prelude, and S-RET is equivalent to vi's o and C-S-RET is equivalent to vi's O.

I recommend adding this to your .emacs, as it makes C-n insert newlines if
the point is at the end of the buffer. Useful, as it means you won’t have to
reach for the return key to add newlines!

    (setq next-line-add-newlines t)

## kill current buffer

	;; kill current buffer instead of asking what buffer to kill
	(global-set-key (kbd "C-x k") 'kill-this-buffer)

## open directory

	c-x c-d

## Opening recent files

To enable ‘recentf-mode’, put this in your `~/.emacs’:

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)

## Split screen

    c-x 2

In order to jump to another window

    c-x o

To close the second window just type

    c-x 1

To close the current window press

    c-x 0

## jumping to the end/beginning of file

    Esc-< ............................ vim gg
    M-< .............................. vim gg
    M-> .............................. vim G

    paragraph   C-↑
    M-e ........ end of paragraph
    M-a ........ beginning of paragraph
    C-e ........ end of line
    C-a ........ start of line

## Jumping to the last postion
+ https://superuser.com/a/244291/45032

    usually bound to C-x C-@ and C-x C-SPC.

    C-x C-x

It will also highlight everything between your previous cursor location and
the new location (if you want to get rid of the highlighting, just `C-g`)

## kill current buffer

    C-x k

## shortcut to save current file

    (global-set-key [f9] 'save-buffer) ; Or any other key you want

<remap> <self-insert-command>	Custom-no-edit

M-TAB		widget-backward


Global Bindings:
key             binding
---             -------

 ..  	self-insert-command
  ..  	self-insert-command

C-@		set-mark-command
C-a		move-beginning-of-line
C-b		backward-char
C-c		mode-specific-command-prefix
C-d		delete-char
C-e		move-end-of-line
C-f		forward-char
C-g		keyboard-quit
C-h		help-command
C-j		newline-and-indent
C-k		kill-line
C-0 C-k         kill-line until the beginning
C-l		recenter-top-bottom
C-n		next-line
C-o		open-line
C-p		previous-line
C-q		quoted-insert
C-r		isearch-backward
C-s		isearch-forward
C-t		transpose-chars
C-u		universal-argument
C-v		scroll-up
C-w		kill-region
C-x		Control-X-prefix
C-y		yank
C-z		suspend-frame
ESC		ESC-prefix
C-\		toggle-input-method
C-]		abort-recursive-edit
C-_		undo
C-SPC		set-mark-command
C--		negative-argument
C-/		undo
C-0 .. C-9	digit-argument
<C-M-down>	down-list
<C-M-end>	end-of-defun
<C-M-home>	beginning-of-defun
<C-M-left>	backward-sexp
<C-M-right>	forward-sexp
<C-M-up>	backward-up-list
<C-S-backspace>			kill-whole-line
<C-backspace>			backward-kill-word
<C-delete>	kill-word
<C-down>	forward-paragraph
<C-down-mouse-1>		mouse-buffer-menu
<C-down-mouse-2>		facemenu-menu
<C-drag-n-drop>			w32-drag-n-drop-other-frame
<C-end>		end-of-buffer
<C-home>	beginning-of-buffer
<C-insert>	kill-ring-save
<C-insertchar>	kill-ring-save
<C-left>	backward-word
<C-next>	scroll-left
<C-prior>	scroll-right
<C-right>	forward-word
<C-up>		backward-paragraph
<C-wheel-down>	mwheel-scroll
<C-wheel-up>	mwheel-scroll
<M-begin>	beginning-of-buffer-other-window
<M-down-mouse-1>		mouse-drag-secondary
<M-drag-mouse-1>		mouse-set-secondary
<M-end>		end-of-buffer-other-window
<M-home>	beginning-of-buffer-other-window
<M-left>	backward-word
<M-mouse-1>	mouse-start-secondary
<M-mouse-2>	mouse-yank-secondary
<M-mouse-3>	mouse-secondary-save-then-kill
<M-next>	scroll-other-window
<M-prior>	scroll-other-window-down
<M-right>	forward-word
<S-delete>	kill-region
<S-down-mouse-1>		mouse-appearance-menu
<S-insert>	yank
<S-insertchar>	yank
<S-wheel-down>	mwheel-scroll
<S-wheel-up>	mwheel-scroll
<again>		repeat-complex-command
<begin>		beginning-of-buffer
<compose-last-chars>		compose-last-chars
<copy>		clipboard-kill-ring-save
<cut>		clipboard-kill-region
<delete-frame>	handle-delete-frame
<deletechar>	delete-char
<deleteline>	kill-line
<double-mouse-1>		mouse-set-point
<down>		next-line
<drag-mouse-1>	mouse-set-region
<drag-n-drop>	w32-drag-n-drop
<end>		move-end-of-line
<execute>	execute-extended-command
<f1>		help-command
<f10>		menu-bar-open
<f16>		clipboard-kill-ring-save
<f18>		clipboard-yank
<f2>		2C-command
<f20>		clipboard-kill-region
<f3>		kmacro-start-macro-or-insert-counter
<f4>		kmacro-end-or-call-macro
<find>		search-forward
<header-line>	Prefix Command
<help>		help-command
<home>		move-beginning-of-line
<iconify-frame>			ignore-event
<insert>	overwrite-mode
<insertchar>	overwrite-mode
<insertline>	open-line
<language-change>		ignore
<left>		backward-char
<left-fringe>	Prefix Command
<lwindow>	ignore
<make-frame-visible>		ignore-event
<menu>		execute-extended-command
<mode-line>	Prefix Command
<mouse-1>	mouse-set-point
<mouse-2>	mouse-yank-at-click
<mouse-3>	mouse-save-then-kill
<mouse-movement>		ignore
<next>		scroll-up
<open>		find-file
<paste>		clipboard-yank
<prior>		scroll-down
<redo>		repeat-complex-command
<right>		forward-char
<right-fringe>	Prefix Command
<rwindow>	ignore
<select-window>			handle-select-window
<switch-frame>			handle-switch-frame
<triple-mouse-1>		mouse-set-point
<undo>		undo
<up>		previous-line
<vertical-line>			Prefix Command
<vertical-scroll-bar>		Prefix Command
<wheel-down>			mwheel-scroll
<wheel-up>	mwheel-scroll

C-h C-a		about-emacs
C-h C-c		describe-copying
C-h C-d		view-emacs-debugging
C-h C-e		view-external-packages
C-h C-f		view-emacs-FAQ
C-h C-h		help-for-help
C-h RET		view-order-manuals
C-h C-n		view-emacs-news
C-h C-o		describe-distribution
C-h C-p		view-emacs-problems
C-h C-t		view-emacs-todo
C-h C-w		describe-no-warranty
C-h C-\		describe-input-method
C-h .		display-local-help
C-h 4		Prefix Command
C-h ?		help-for-help
C-h C		describe-coding-system
C-h F		Info-goto-emacs-command-node
C-h I		describe-input-method
C-h K		Info-goto-emacs-key-command-node
C-h L		describe-language-environment
C-h S		info-lookup-symbol
C-h a		apropos-command
C-h b		describe-bindings
C-h c		describe-key-briefly
C-h d		apropos-documentation
C-h e		view-echo-area-messages
C-h f		describe-function
C-h g		describe-gnu-project
C-h h		view-hello-file
C-h i		info
C-h k		describe-key
C-h l		view-lossage
C-h m		describe-mode
C-h n		view-emacs-news
C-h p		finder-by-keyword
C-h q		help-quit
C-h r		info-emacs-manual
C-h s		describe-syntax
C-h t		help-with-tutorial
C-h v		describe-variable
C-h w		where-is
C-h <f1>	help-for-help
C-h <help>	help-for-help

C-x C-@		pop-global-mark
C-x C-b		list-buffers
C-x C-c		save-buffers-kill-terminal
C-x C-d		list-directory
C-x C-e		eval-last-sexp
C-x C-f		find-file
C-x TAB		indent-rigidly
C-x C-k		kmacro-keymap
C-x C-l		downcase-region
C-x RET		Prefix Command
C-x C-n		set-goal-column
C-x C-o		delete-blank-lines
C-x C-p		mark-page
C-x C-q		toggle-read-only
C-x C-r		find-file-read-only
C-x C-t		transpose-lines
C-x C-u		upcase-region
C-x C-v		find-alternate-file
C-x C-w		write-file
C-x C-x		exchange-point-and-mark
C-x C-z		suspend-frame
C-x ESC		Prefix Command
C-x $		set-selective-display
C-x '		expand-abbrev
C-x (		kmacro-start-macro
C-x )		kmacro-end-macro
C-x *		calc-dispatch
C-x +		balance-windows
C-x -		shrink-window-if-larger-than-buffer
C-x .		set-fill-prefix
C-x 0		delete-window
C-x 1		delete-other-windows
C-x 2		split-window-vertically
C-x 3		split-window-horizontally
C-x 4		ctl-x-4-prefix
C-x 5		ctl-x-5-prefix
C-x 6		2C-command
C-x 8		Prefix Command
C-x ;		comment-set-column
C-x <		scroll-left
C-x =		what-cursor-position
C-x >		scroll-right
C-x [		backward-page
C-x ]		forward-page
C-x ^		enlarge-window
C-x `		next-error
C-x a		Prefix Command
C-x b		switch-to-buffer
C-x d		dired
C-x e		kmacro-end-and-call-macro
C-x f		set-fill-column
C-x h		mark-whole-buffer
C-x i		insert-file
C-x k		kill-buffer
C-x l		count-lines-page
C-x m		compose-mail
C-x n		Prefix Command
C-x o		other-window
C-x q		kbd-macro-query
C-x r		Prefix Command
C-x s		save-some-buffers
C-x u		advertised-undo
C-x v		vc-prefix-map
C-x z		repeat
C-x {		shrink-window-horizontally
C-x }		enlarge-window-horizontally
C-x DEL		backward-kill-sentence
C-x C-SPC	pop-global-mark
C-x C-+		text-scale-adjust
C-x C--		text-scale-adjust
C-x C-0		text-scale-adjust
C-x C-=		text-scale-adjust
C-x <C-left>	previous-buffer
C-x <C-right>	next-buffer
C-x <left>	previous-buffer
C-x <right>	next-buffer

M-0 .. M-9	digit-argument

C-M-@		mark-sexp
C-M-a		beginning-of-defun
C-M-b		backward-sexp
C-M-c		exit-recursive-edit
C-M-d		down-list
C-M-e		end-of-defun
C-M-f		forward-sexp
C-M-h		mark-defun
C-M-j		indent-new-comment-line
C-M-k		kill-sexp
C-M-l		reposition-window
C-M-n		forward-list
C-M-o		split-line
C-M-p		backward-list
C-M-r		isearch-backward-regexp
C-M-s		isearch-forward-regexp
C-M-t		transpose-sexps
C-M-u		backward-up-list
C-M-v		scroll-other-window
C-M-w		append-next-kill
ESC ESC		Prefix Command
C-M-\		indent-region
M-SPC		just-one-space
M-!		    shell-command
M-$		    ispell-word
M-%		    query-replace
M-'		    abbrev-prefix-mark
M-(		    insert-parentheses
M-)		    move-past-close-and-reindent
M-*		    pop-tag-mark
M-,		    tags-loop-continue
M--		    negative-argument
M-.		    find-tag
M-/		    dabbrev-expand
M-:		    eval-expression
M-;		    comment-dwim
M-<		    beginning-of-buffer
M-=		    count-lines-region
M->		    end-of-buffer
M-@		    mark-word
M-\		    delete-horizontal-space
M-^		    delete-indentation
M-`		    tmm-menubar
M-a		    backward-sentence
M-b		    backward-word
M-c		    capitalize-word
M-d		    kill-word
M-e		    forward-sentence
M-f		    forward-word
C-3 M-f     forward three words
C-u 3 M-f   forward three words
M-g		    Prefix Command
M-h		    mark-paragraph
M-i		    tab-to-tab-stop
M-j		    indent-new-comment-line
M-k		    kill-sentence
M-l		    downcase-word
M-m		    back-to-indentation
M-o		    facemenu-keymap
M-q		    fill-paragraph
M-r		    move-to-window-line
M-s		    Prefix Command
M-t		    transpose-words
M-u		    upcase-word
M-v		    scroll-down
M-w		    kill-ring-save
M-x		    execute-extended-command
M-y		    yank-pop
M-z		    zap-to-char
M-{		    backward-paragraph
M-|		    shell-command-on-region
M-}		    forward-paragraph
M-~		    not-modified
M-DEL		backward-kill-word
C-M-S-v		scroll-other-window-down
C-M-SPC		mark-sexp
C-M-%		query-replace-regexp
C-M--		negative-argument
C-M-.		find-tag-regexp
C-M-/		dabbrev-completion
C-M-0 .. C-M-9	digit-argument
ESC <C-backspace>		backward-kill-sexp
ESC <C-delete>			backward-kill-sexp
ESC <C-down>			down-list
ESC <C-end>			end-of-defun
ESC <C-home>			beginning-of-defun
ESC <C-left>			backward-sexp
ESC <C-right>			forward-sexp
ESC <C-up>	backward-up-list
ESC <begin>	beginning-of-buffer-other-window
ESC <end>	end-of-buffer-other-window
ESC <home>	beginning-of-buffer-other-window
ESC <left>	backward-word
ESC <next>	scroll-other-window
ESC <prior>	scroll-other-window-down
ESC <right>	forward-word

M-s h		Prefix Command
M-s o		occur
M-s w		isearch-forward-word

M-o ESC		Prefix Command
M-o b		facemenu-set-bold
M-o d		facemenu-set-default
M-o i		facemenu-set-italic
M-o l		facemenu-set-bold-italic
M-o o		facemenu-set-face
M-o u		facemenu-set-underline

M-g ESC		Prefix Command
M-g g		goto-line
M-g n		next-error
M-g p		previous-error

M-ESC ESC	keyboard-escape-quit
M-ESC :		eval-expression

<vertical-line> <C-mouse-2>	mouse-split-window-vertically
<vertical-line> <down-mouse-1>	mouse-drag-vertical-line
<vertical-line> <mouse-1>	mouse-select-window

<vertical-scroll-bar> <C-mouse-2>
				mouse-split-window-vertically
<vertical-scroll-bar> <mouse-1>
				scroll-bar-toolkit-scroll

<header-line> <down-mouse-1>	mouse-drag-header-line
<header-line> <mouse-1>		mouse-select-window

<mode-line> <C-mouse-2>		mouse-split-window-horizontally
<mode-line> <down-mouse-1>	mouse-drag-mode-line
<mode-line> <drag-mouse-1>	mouse-select-window
<mode-line> <mouse-1>		mouse-select-window
<mode-line> <mouse-2>		mouse-delete-other-windows
<mode-line> <mouse-3>		mouse-delete-window

<right-fringe> <mouse-1>	mouse-set-point
<right-fringe> <mouse-2>	mouse-yank-at-click
<right-fringe> <mouse-3>	mouse-save-then-kill

<left-fringe> <mouse-1>		mouse-set-point
<left-fringe> <mouse-2>		mouse-yank-at-click
<left-fringe> <mouse-3>		mouse-save-then-kill

<C-down-mouse-2> <bg>		facemenu-background-menu
<C-down-mouse-2> <dc>		list-colors-display
<C-down-mouse-2> <df>		list-faces-display
<C-down-mouse-2> <dp>		describe-text-properties
<C-down-mouse-2> <fc>		facemenu-face-menu
<C-down-mouse-2> <fg>		facemenu-foreground-menu
<C-down-mouse-2> <in>		facemenu-indentation-menu
<C-down-mouse-2> <ju>		facemenu-justification-menu
<C-down-mouse-2> <ra>		facemenu-remove-all
<C-down-mouse-2> <rm>		facemenu-remove-face-props
<C-down-mouse-2> <sp>		facemenu-special-menu

<f1> C-a	about-emacs
<f1> C-c	describe-copying
<f1> C-d	view-emacs-debugging
<f1> C-e	view-external-packages
<f1> C-f	view-emacs-FAQ
<f1> C-h	help-for-help
<f1> RET	view-order-manuals
<f1> C-n	view-emacs-news
<f1> C-o	describe-distribution
<f1> C-p	view-emacs-problems
<f1> C-t	view-emacs-todo
<f1> C-w	describe-no-warranty
<f1> C-\	describe-input-method
<f1> .		display-local-help
<f1> 4		Prefix Command
<f1> ?		help-for-help
<f1> C		describe-coding-system
<f1> F		Info-goto-emacs-command-node
<f1> I		describe-input-method
<f1> K		Info-goto-emacs-key-command-node
<f1> L		describe-language-environment
<f1> S		info-lookup-symbol
<f1> a		apropos-command
<f1> b		describe-bindings
<f1> c		describe-key-briefly
<f1> d		apropos-documentation
<f1> e		view-echo-area-messages
<f1> f		describe-function
<f1> g		describe-gnu-project
<f1> h		view-hello-file
<f1> i		info
<f1> k		describe-key
<f1> l		view-lossage
<f1> m		describe-mode
<f1> n		view-emacs-news
<f1> p		finder-by-keyword
<f1> q		help-quit
<f1> r		info-emacs-manual
<f1> s		describe-syntax
<f1> t		help-with-tutorial
<f1> v		describe-variable
<f1> w		where-is
<f1> <f1>	help-for-help
<f1> <help>	help-for-help

<help> C-a	about-emacs
<help> C-c	describe-copying
<help> C-d	view-emacs-debugging
<help> C-e	view-external-packages
<help> C-f	view-emacs-FAQ
<help> C-h	help-for-help
<help> RET	view-order-manuals
<help> C-n	view-emacs-news
<help> C-o	describe-distribution
<help> C-p	view-emacs-problems
<help> C-t	view-emacs-todo
<help> C-w	describe-no-warranty
<help> C-\	describe-input-method
<help> .	display-local-help
<help> 4	Prefix Command
<help> ?	help-for-help
<help> C	describe-coding-system
<help> F	Info-goto-emacs-command-node
<help> I	describe-input-method
<help> K	Info-goto-emacs-key-command-node
<help> L	describe-language-environment
<help> S	info-lookup-symbol
<help> a	apropos-command
<help> b	describe-bindings
<help> c	describe-key-briefly
<help> d	apropos-documentation
<help> e	view-echo-area-messages
<help> f	describe-function
<help> g	describe-gnu-project
<help> h	view-hello-file
<help> i	info
<help> k	describe-key
<help> l	view-lossage
<help> m	describe-mode
<help> n	view-emacs-news
<help> p	finder-by-keyword
<help> q	help-quit
<help> r	info-emacs-manual
<help> s	describe-syntax
<help> t	help-with-tutorial
<help> v	describe-variable
<help> w	where-is
<help> <f1>	help-for-help
<help> <help>	help-for-help

C-h 4 i		info-other-window

C-x RET C-\	set-input-method
C-x RET F	set-file-name-coding-system
C-x RET X	set-next-selection-coding-system
C-x RET c	universal-coding-system-argument
C-x RET f	set-buffer-file-coding-system
C-x RET k	set-keyboard-coding-system
C-x RET l	set-language-environment
C-x RET p	set-buffer-process-coding-system
C-x RET r	revert-buffer-with-coding-system
C-x RET t	set-terminal-coding-system
C-x RET x	set-selection-coding-system

C-x ESC ESC	repeat-complex-command
C-x M-:		repeat-complex-command

C-x 4 C-f	find-file-other-window
C-x 4 C-o	display-buffer
C-x 4 .		find-tag-other-window
C-x 4 0		kill-buffer-and-window
C-x 4 a		add-change-log-entry-other-window
C-x 4 b		switch-to-buffer-other-window
C-x 4 c		clone-indirect-buffer-other-window
C-x 4 d		dired-other-window
C-x 4 f		find-file-other-window
C-x 4 m		compose-mail-other-window
C-x 4 r		find-file-read-only-other-window

C-x 5 C-f	find-file-other-frame
C-x 5 C-o	display-buffer-other-frame
C-x 5 .		find-tag-other-frame
C-x 5 0		delete-frame
C-x 5 1		delete-other-frames
C-x 5 2		make-frame-command
C-x 5 b		switch-to-buffer-other-frame
C-x 5 d		dired-other-frame
C-x 5 f		find-file-other-frame
C-x 5 m		compose-mail-other-frame
C-x 5 o		other-frame
C-x 5 r		find-file-read-only-other-frame

C-x 8 RET	ucs-insert

C-x a C-a	add-mode-abbrev
C-x a '		expand-abbrev
C-x a +		add-mode-abbrev
C-x a -		inverse-add-global-abbrev
C-x a e		expand-abbrev
C-x a g		add-global-abbrev
C-x a i		Prefix Command
C-x a l		add-mode-abbrev
C-x a n		expand-jump-to-next-slot
C-x a p		expand-jump-to-previous-slot

C-x n d		narrow-to-defun
C-x n n		narrow-to-region
C-x n p		narrow-to-page
C-x n w		widen

C-x r C-@	point-to-register
C-x r SPC	point-to-register
C-x r +		increment-register
C-x r b		bookmark-jump
C-x r c		clear-rectangle
C-x r d		delete-rectangle
C-x r f		frame-configuration-to-register
C-x r g		insert-register
C-x r i		insert-register
C-x r j		jump-to-register
C-x r k		kill-rectangle
C-x r l		bookmark-bmenu-list
C-x r m		bookmark-set
C-x r n		number-to-register
C-x r o		open-rectangle
C-x r r		copy-rectangle-to-register
C-x r s		copy-to-register
C-x r t		string-rectangle
C-x r w		window-configuration-to-register
C-x r x		copy-to-register
C-x r y		yank-rectangle
C-x r C-SPC	point-to-register

C-x v +		vc-update
C-x v =		vc-diff
C-x v a		vc-update-change-log
C-x v b		vc-switch-backend
C-x v c		vc-rollback
C-x v d		vc-dir
C-x v g		vc-annotate
C-x v h		vc-insert-headers
C-x v i		vc-register
C-x v l		vc-print-log
C-x v m		vc-merge
C-x v r		vc-retrieve-tag
C-x v s		vc-create-tag
C-x v u		vc-revert
C-x v v		vc-next-action
C-x v ~		vc-revision-other-window

M-s h f		hi-lock-find-patterns
M-s h l		highlight-lines-matching-regexp
M-s h p		highlight-phrase
M-s h r		highlight-regexp
M-s h u		unhighlight-regexp
M-s h w		hi-lock-write-interactive-patterns

M-o M-S		center-paragraph
M-o M-o		font-lock-fontify-block
M-o M-s		center-line

M-g M-g		goto-line
M-g M-n		next-error
M-g M-p		previous-error

<C-down-mouse-2> <fc> b		facemenu-set-bold
<C-down-mouse-2> <fc> d		facemenu-set-default
<C-down-mouse-2> <fc> i		facemenu-set-italic
<C-down-mouse-2> <fc> l		facemenu-set-bold-italic
<C-down-mouse-2> <fc> o		facemenu-set-face
<C-down-mouse-2> <fc> u		facemenu-set-underline

<C-down-mouse-2> <fg> o		facemenu-set-foreground

<C-down-mouse-2> <bg> o		facemenu-set-background

<C-down-mouse-2> <sp> r		facemenu-set-read-only
<C-down-mouse-2> <sp> s		facemenu-remove-special
<C-down-mouse-2> <sp> t		facemenu-set-intangible
<C-down-mouse-2> <sp> v		facemenu-set-invisible

<C-down-mouse-2> <ju> b		set-justification-full
<C-down-mouse-2> <ju> c		set-justification-center
<C-down-mouse-2> <ju> l		set-justification-left
<C-down-mouse-2> <ju> r		set-justification-right
<C-down-mouse-2> <ju> u		set-justification-none

<C-down-mouse-2> <in> <decrease-left-margin>
				decrease-left-margin
<C-down-mouse-2> <in> <decrease-right-margin>
				decrease-right-margin
<C-down-mouse-2> <in> <increase-left-margin>
				increase-left-margin
<C-down-mouse-2> <in> <increase-right-margin>
				increase-right-margin

<f1> 4 i	info-other-window

<help> 4 i	info-other-window

C-x a i g	inverse-add-global-abbrev
C-x a i l	inverse-add-mode-abbrev


Function key map translations:
key             binding
---             -------

C-x		Prefix Command
<C-S-kp-1>	<C-S-end>
<C-S-kp-2>	<C-S-down>
<C-S-kp-3>	<C-S-next>
<C-S-kp-4>	<C-S-left>
<C-S-kp-6>	<C-S-right>
<C-S-kp-7>	<C-S-home>
<C-S-kp-8>	<C-S-up>
<C-S-kp-9>	<C-S-prior>
<C-S-kp-down>	<C-S-down>
<C-S-kp-end>	<C-S-end>
<C-S-kp-home>	<C-S-home>
<C-S-kp-left>	<C-S-left>
<C-S-kp-next>	<C-S-next>
<C-S-kp-prior>	<C-S-prior>
<C-S-kp-right>	<C-S-right>
<C-S-kp-up>	<C-S-up>
<M-backspace>	M-DEL
<M-clear>	C-M-l
<M-delete>	M-DEL
<M-escape>	M-ESC
<M-kp-next>	<M-next>
<M-linefeed>	C-M-j
<M-return>	M-RET
<M-tab>		M-TAB
<S-iso-lefttab>	<backtab>
<S-kp-down>	<S-down>
<S-kp-end>	<S-end>
<S-kp-home>	<S-home>
<S-kp-left>	<S-left>
<S-kp-next>	<S-next>
<S-kp-prior>	<S-prior>
<S-kp-right>	<S-right>
<S-kp-up>	<S-up>
<S-tab>		<backtab>
<backspace>	DEL
<clear>		C-l
<delete>	C-d
<escape>	ESC
<iso-lefttab>	<backtab>
<kp-0>		0
<kp-1>		1
<kp-2>		2
<kp-3>		3
<kp-4>		4
<kp-5>		5
<kp-6>		6
<kp-7>		7
<kp-8>		8
<kp-9>		9
<kp-add>	+
<kp-begin>	<begin>
<kp-decimal>	.
<kp-delete>	C-d
<kp-divide>	/
<kp-down>	<down>
<kp-end>	<end>
<kp-enter>	RET
<kp-equal>	=
<kp-home>	<home>
<kp-insert>	<insert>
<kp-left>	<left>
<kp-multiply>	*
<kp-next>	<next>
<kp-prior>	<prior>
<kp-right>	<right>
<kp-separator>	,
<kp-space>	SPC
<kp-subtract>	-
<kp-tab>	TAB
<kp-up>		<up>
<linefeed>	C-j
<return>	RET
<tab>		TAB

C-x @		Prefix Command

C-x @ S		event-apply-shift-modifier
C-x @ a		event-apply-alt-modifier
C-x @ c		event-apply-control-modifier
C-x @ h		event-apply-hyper-modifier
C-x @ m		event-apply-meta-modifier
C-x @ s		event-apply-super-modifier


Input decoding map translations:
key             binding
---             -------

=======================================

Key translations Starting With C-x:
key             binding
---             -------

C-x 8		iso-transl-ctl-x-8-map


Global Bindings Starting With C-x:
key             binding
---             -------

C-x C-@		pop-global-mark
C-x C-b		list-buffers
C-x C-c		save-buffers-kill-terminal
C-x C-d		list-directory
C-x C-e		eval-last-sexp
C-x C-f		find-file
C-x TAB		indent-rigidly
C-x C-k		kmacro-keymap
C-x C-l		downcase-region
C-x RET		Prefix Command
C-x C-n		set-goal-column
C-x C-o		delete-blank-lines
C-x C-p		mark-page
C-x C-q		toggle-read-only
C-x C-r		find-file-read-only
C-x C-s		save-buffer
C-x C-t		transpose-lines
C-x C-u		upcase-region
C-x C-v		find-alternate-file
C-x C-w		write-file
C-x C-x		exchange-point-and-mark
C-x C-z		suspend-frame
C-x ESC		Prefix Command
C-x $		set-selective-display
C-x '		expand-abbrev
C-x (		kmacro-start-macro
C-x )		kmacro-end-macro
C-x *		calc-dispatch
C-x +		balance-windows
C-x -		shrink-window-if-larger-than-buffer
C-x .		set-fill-prefix
C-x 0		delete-window
C-x 1		delete-other-windows
C-x 2		split-window-vertically
C-x 3		split-window-horizontally
C-x 4		ctl-x-4-prefix
C-x 5		ctl-x-5-prefix
C-x 6		2C-command
C-x 8		Prefix Command
C-x ;		comment-set-column
C-x <		scroll-left
C-x =		what-cursor-position
C-x >		scroll-right
C-x [		backward-page
C-x ]		forward-page
C-x ^		enlarge-window
C-x `		next-error
C-x a		Prefix Command
C-x b		switch-to-buffer
C-x d		dired
C-x e		kmacro-end-and-call-macro
C-x f		set-fill-column
C-x h		mark-whole-buffer
C-x i		insert-file
C-x k		kill-buffer
C-x l		count-lines-page
C-x m		compose-mail
C-x n		Prefix Command
C-x o		other-window
C-x q		kbd-macro-query
C-x r		Prefix Command
C-x s		save-some-buffers
C-x u		advertised-undo
C-x v		vc-prefix-map
C-x z		repeat
C-x {		shrink-window-horizontally
C-x }		enlarge-window-horizontally
C-x DEL		backward-kill-sentence
C-x C-SPC	pop-global-mark
C-x C-+		text-scale-adjust
C-x C--		text-scale-adjust
C-x C-0		text-scale-adjust
C-x C-=		text-scale-adjust
C-x <C-left>	previous-buffer
C-x <C-right>	next-buffer
C-x <left>	previous-buffer
C-x <right>	next-buffer

C-x RET C-\	set-input-method
C-x RET F	set-file-name-coding-system
C-x RET X	set-next-selection-coding-system
C-x RET c	universal-coding-system-argument
C-x RET f	set-buffer-file-coding-system
C-x RET k	set-keyboard-coding-system
C-x RET l	set-language-environment
C-x RET p	set-buffer-process-coding-system
C-x RET r	revert-buffer-with-coding-system
C-x RET t	set-terminal-coding-system
C-x RET x	set-selection-coding-system

C-x ESC ESC	repeat-complex-command
C-x M-:		repeat-complex-command

C-x 4 C-f	find-file-other-window
C-x 4 C-o	display-buffer
C-x 4 .		find-tag-other-window
C-x 4 0		kill-buffer-and-window
C-x 4 a		add-change-log-entry-other-window
C-x 4 b		switch-to-buffer-other-window
C-x 4 c		clone-indirect-buffer-other-window
C-x 4 d		dired-other-window
C-x 4 f		find-file-other-window
C-x 4 m		compose-mail-other-window
C-x 4 r		find-file-read-only-other-window

C-x 5 C-f	find-file-other-frame
C-x 5 C-o	display-buffer-other-frame
C-x 5 .		find-tag-other-frame
C-x 5 0		delete-frame
C-x 5 1		delete-other-frames
C-x 5 2		make-frame-command
C-x 5 b		switch-to-buffer-other-frame
C-x 5 d		dired-other-frame
C-x 5 f		find-file-other-frame
C-x 5 m		compose-mail-other-frame
C-x 5 o		other-frame
C-x 5 r		find-file-read-only-other-frame

C-x 8 RET	ucs-insert

C-x a C-a	add-mode-abbrev
C-x a '		expand-abbrev
C-x a +		add-mode-abbrev
C-x a -		inverse-add-global-abbrev
C-x a e		expand-abbrev
C-x a g		add-global-abbrev
C-x a i		Prefix Command
C-x a l		add-mode-abbrev
C-x a n		expand-jump-to-next-slot
C-x a p		expand-jump-to-previous-slot

C-x n d		narrow-to-defun
C-x n n		narrow-to-region
C-x n p		narrow-to-page
C-x n w		widen

C-x r C-@	point-to-register
C-x r SPC	point-to-register
C-x r +		increment-register
C-x r b		bookmark-jump
C-x r c		clear-rectangle
C-x r d		delete-rectangle
C-x r f		frame-configuration-to-register
C-x r g		insert-register
C-x r i		insert-register
C-x r j		jump-to-register
C-x r k		kill-rectangle
C-x r l		bookmark-bmenu-list
C-x r m		bookmark-set
C-x r n		number-to-register
C-x r o		open-rectangle
C-x r r		copy-rectangle-to-register
C-x r s		copy-to-register
C-x r t		string-rectangle
C-x r w		window-configuration-to-register
C-x r x		copy-to-register
C-x r y		yank-rectangle
C-x r C-SPC	point-to-register

C-x v +		vc-update
C-x v =		vc-diff
C-x v a		vc-update-change-log
C-x v b		vc-switch-backend
C-x v c		vc-rollback
C-x v d		vc-dir
C-x v g		vc-annotate
C-x v h		vc-insert-headers
C-x v i		vc-register
C-x v l		vc-print-log
C-x v m		vc-merge
C-x v r		vc-retrieve-tag
C-x v s		vc-create-tag
C-x v u		vc-revert
C-x v v		vc-next-action
C-x v ~		vc-revision-other-window

C-x a i g	inverse-add-global-abbrev
C-x a i l	inverse-add-mode-abbrev


Function key map translations Starting With C-x:
key             binding
---             -------

C-x @		Prefix Command

C-x @ S		event-apply-shift-modifier
C-x @ a		event-apply-alt-modifier
C-x @ c		event-apply-control-modifier
C-x @ h		event-apply-hyper-modifier
C-x @ m		event-apply-meta-modifier
C-x @ s		event-apply-super-modifier

* http://code.google.com/p/yasnippet/

  M-q ........... wrapping text

If you type a lot of words into Emacs, you will notice that it does
not automatically wrap the text. This can be very annoying both for
moving the point around if you are typing full paragraphs, because
Emacs will treat the paragraph as a single line, and for readability.

## Macros

You can do it with keyboard macros: f3 bla M-5 f4.

    F3 means "start recording"
    then you insert bla
    M-5 means "5 times"
    F4 means finish

Alternate to M-5

	  C-x ( ............... inicia macro
	  C-x ) ............... finaliza macro
	  C-x e ............... executa macro

	  C-u 5 ............... quantificador
	  C-u 5 C-x e ......... 5 vezes a macro

## Reload Init File

To reload init file:

   	 Alt+x eval-buffer

or

    Alt+x load-file

You can also just move the point to the end of any sexp and press `C-xC-e` to execute just that sexp. Usually it's not necessary to reload the whole file if you're just changing a line or two.

    C-x C-e ;; current line
    M-x eval-region ;; region
    M-x eval-buffer ;; whole buffer
    M-x load-file ~/.emacs.d/init.el

    ;; ========== reload init file ==================================
    (defun reload-init-file ()
    (interactive)
    (load-file "~/.emacs"))

    (global-set-key (kbd "C-c C-l") 'reload-init-file)    ; Reload .emacs file

## Setting the mark and copying
+ https://bit.ly/30MkvpK

We can set mark C-Space and continue typing and
at the end we can cut C-w or copy M-w

C-SPC C-SPC activates mark without activating a region
I can create as many marks as I need and then pop up them with
C-u C-SPC

## jumping to the other side of the selection

In vim we use 'o' to jump in selections, in Emacs just type
C-x C-x

## expand-region

So first invocation selects a word, and then the next invocation
expands the region to a symbol, further to a string, further to the
string wrapped in quotes and so on to encompass the whole line and
finally the whole buffer.

## Repeat last insert

    C-x z

The above shortcut will repeat last insert
The above shortcut will repeat last insert

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0
-->
