---
file: introducao-ao-vim.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
abstrac: This aims...
---

#  Introdução

O editor de textos vim é muito controverso, devido sua extensibilidade, ou
seja, sua capacidade de receber novos recursos através de configurações,
plugins, ou mesmo pela infinidade de atalhos que sua natureza modal
possibilita.

Devemos ter em mente o seguinte: A facilidade de uso de uma
ferramenta é inversamente proporcional à sua quantidade de recursos, o bloco
de notas do Windows é extremamente fácil de usar, no entanto sua capacidade
como editor é sofrível. Digamos que o vim está no extremo oposto, mesmo que
você gaste sua vida inteira estudando ele sempre haverá algo novo a aprender.

## Introdução aos registros do vim

No vim o ato de apagar algo alimenta o registro padrão e o registro
de deleão número 1, se houver algo no registro 1 seu conteúdo será movido
para o registro 2 e assim sucessivamente até o nono registro.

Portanto se eu apagar algo em modo normal eu posso recuperar esse conteúdo
pressionando 'u' em modo normal (desfazer), pressionando 'p' colando o que
foi deletado ou utilizando o registro '1'  "1p.

Observe o gráfico abaixo sobre as ações e apagar e copiar, ele mostra
como apagar e copiar geram conteúdos nos registros:

    +---------+
    | Deleção | ---------------+---------------------------+
    +---------+                |                           |
                               |                  +-------------------+
                               |                  |  Registros 1 a 9  |
                               |                  +-------------------+
                    +-------------------+
                    | Registro Padrão " |
                    +-------------------+
                               |                  +----------------------+
                               |                  |  Registro de cópia 0 |
                               |                  +----------------------+
    +---------+                |                           |
    | Cópia   | ---------------+---------------------------+
    +---------+

Note que tanto quando deletamos algo como quando copiamos, o registro padrão
é alimentado, e tanto a cópia quanto a deleção tem seus registros próprios.
Talvez a diferença mais perceptível é que o registro de cópia só tem um espaço
na memória.

Se você tiver as seguintes linhas

    linha 1 copiada
    linha 2 deletada
    linha 3 deletada

executar uma cópia na primeira linha com 'yy'
e deletar as duas subsequentes com 'dd'...

E em seguida executar o comando:

    :reg 0 " 1 2

O resultado será:

    --- Registradores ---
    ""   linha 3 deletada
    "0   linha 1 copiada
    "1   linha 3 deletada
    "2   linha 2 deletada

Tanto no caso da cópia quando no caso da deleção, é implícito o uso do
registro padrão, caso queiramos indicar outro registro basta preceder-mos a
ação com o caractere correspondente.

Por exemplo, para apagar a linha atual para o registro 'a' usamos "add

## A área de transferência
Para saber se o seu vim tem suporte a "Área de Transferência" (clipboard),
execute o seguinte comando:

    :echo has('clipboard')

Se a saída desse comando for 1 o seu vim foi compilado com suporte a
clipboard, no caso do nvim você tem que instalar algum gerenciador de
clipboard como o xclip.

### Clipboard e seleção primária

O clipboard é um espaço de memória comum à maioria dos programas, é aquele
usado quando pressionamos a combinação de teclas "Ctrl-c" (que normalmente não
funciona no vim).

No vim o clipboard é referenciado pelo caractere +

No caso do vim podemos copiar para o clipbaord (em modo normal) com o comando
`"+yy`. No caso acima estamos copiando a linha inteira 'yy'. Se queremos
copiar as próximas duas palavras para o clipboard usamos o comando `"+y2w`.
Para copiar o arquivo inteiro para o clipboard podemos usar o comando `:%+y`.

A seleção primária é um outro espaço de memória comun aos usuários do
gerenciador gráfico x-window do Linux, ou seja, nem todo linux vai

