---
file: cal.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# cal.md

Comando calendario

## Exibir o calendário do mês corrente

		cal -m -1

Por padrão cal exibe o mês corrente
