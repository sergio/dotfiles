---
file: fzy.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# fzy.md
+ https://github.com/jhawthorn/fzy

fzy is a fat, simple fuzzy text selector for terminal with advanced
scorring algorithm.

   ls -l $(find -type f | fzy)

   nvim $(ag --hidden . -l -g '' | fzy)
