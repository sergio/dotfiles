---
file: let.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# O comando let e usado para atribuir variaveis

    Last Change: mar 29 2019 20:41

    let contador++
    contador=$((contador+1))

let commands is used to perform arithmetic operations on shell variables.

$ cat arith.sh
## /bin/bash

    let arg1=12
    let arg2=11

    let add=$arg1+$arg2
    let sub=$arg1-$arg2
    let mul=$arg1*$arg2
    let div=$arg1/$arg2
    echo $add $sub $mul $div

    $ ./arith.sh
    23 1 132 1
