---
file: bc.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Introdução

Para usar escala de dois dígitos

    [ ! -f ~/.bcrc ] || touch ~/.bcrc && export BC_ENV_ARGS=$HOME/.bcrc || echo scale=2 >> ~/.bcrc

    seq 1 100 | paste -sd+ | bc

Same result using awk

    seq 1 100 | awk '{sum+=$1} END {print sum}'

### Como fazer o cálculo no linux?

    echo 'scale=3; 36/46' | bc

## Fatorial

    fac() { (echo 1; seq $1) | paste -s -d\* | bc; }

## somando os numeros de 1 a 100

    seq -s+ 100 | bc

## Calculos com números quebrados

    echo 'scale=2; 5/2' | bc

## Usando Here strings
Neste caso não temos que usar echo

    bc <<< 'scale=2; 2 / 5'

OBS: se eu tenho o meu "bcrc", ou seja, arquivo de configuração pronto
bast usar:

    bc <<< 5/3

sem uso de aspas e scale

## Decimal para hexadecimal

    echo "obase=16; ibase=10; 56" | bc

## binário para decimal

    echo "obase=10;ibase=2;$bin" | bc # 100 (binary to decimal)

## decimal para binário:
+ /home/sergio/.dotfiles/zsh/autoloaded/dec2bin

    echo "ibase=10;obase=2;$1" | bc

## Somando valores com o 'paste' e o 'bc'
Fonte: http://br.groups.yahoo.com/group/shell-script - Mensagem 19183
arquivo valores.txt contem

    cat valores.txt
    23
    21
    90
    32

o comando deve me retornar a soma de todos estes numeros, alguem pode
me dar um help?

     paste -sd+ valores.txt | bc
     166

A opção -s transforma coluna em linha, a opçao -d indica o
delimitador e bc é uma calculadora. Veja só o paste:

     $ paste -sd+ valores.txt
     23+21+90+32

O mesmo resultado pode ser obtido usando-se o awk:

    awk '{var+=$1} END {print var}' valores.txt

## Retornando apenas a saída

    echo "23 - 22" | bc -l -q

## Função para converter de decimal para binário

    function decToBin { echo "ibase=10; obase=2; $1" | bc; }

## Comparando valores
Fonte: [Lista brasileira de shell script](http://br.groups.yahoo.com/group/shell-script/message/30666|lista)

    if (( ! $(echo 2.0 == 1.006 | bc -l) )); then
        echo isso é falso
    fi
