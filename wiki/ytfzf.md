---
File: /home/sergio/.dotfiles/wiki/ytfzf.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [youtube, fzf]
 vim:ft=markdown
---

## site:
+ https://github.com/pystardust/ytfzf
+ https://www.youtube.com/channel/UC3yaWWA9FF9O

A POSIX script that helps you find Youtube videos (without API) and
opens/downloads them using mpv/youtube-dl
