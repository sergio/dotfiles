---
file: mvconv.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# convert filename charset

    convmv -r -f windows-1252 -t UTF-8 .
