File: /home/sergio/.dotfiles/wiki/xev.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [cli, tools, keyboard]

 vim:ft=markdown
xev # This X program is useful for seeing the keycodes and symbols generated by your keyboard. Can remap using xmodmap. For instance, I've changed my caps lock key into an escape key.
