Filename: dbus.md
Last Change: Tue, 10 Jan 2023 - 08:49:38
tags: [services, dbus, dunst, notifications]

## Intro:
+ https://askubuntu.com/a/845191/3798

The standard systemwide and per-session message bus setups are configured in the files:

    /usr/share/dbus-1/system.conf

and

    /usr/share/dbus-1/session.conf.

These files normally a system-local.conf or session-local.conf in /etc/dbus-1; you can put local overrides in those files to avoid modifying the primary configuration files.

The configuration file is an XML document. It must have the following doctype declaration:

I believe you need to modify /usr/share/dbus-1/session.conf which is what /etc/dbus-1 in ubuntu is linked to.

Note: Limits are normally only of interest on the systemwide bus, not the user session buses but I think you can use them on the session bus.

The element defines a security policy to be applied to a particular set of connections to the bus. A policy is made up of and elements. Policies are normally used with the systemwide bus; they are analogous to a firewall in that they allow expected traffic and prevent unexpected traffic.

Policies applied later will override those applied earlier, when the policies overlap. Multiple policies with the same user/group/context are applied in the order they appear in the config file.

You can likely just tack something like this on the end of /usr/share/dbus-1/session.conf prior to the </busconfig> line which would allow access for those in the lp group and deny it for everyone else. Of course you'll need to modify this to match your environment and your needs.

<policy group="lp">
    <allow send_destination="org.bluez"/>
    <allow send_destination="org.bluez.obex"/>
  </policy>

  <policy context="default">
    <deny send_destination="org.bluez"/>
    <deny send_destination="org.bluez.obex"/>
  </policy>

Sources:

+ https://github.com/netblue30/firejail/issues/796
+ http://www.linuxfromscratch.org/blfs/view/svn/general/dbus.html
+ https://dbus.freedesktop.org/doc/dbus-daemon.1.html
+ https://github.com/ghent360/bluez/blob/master/src/bluetooth.conf
