---
Filename: ~/.dotfiles/wiki/bolo-de-milho.md
Last Change: Sun, 05 May 2024 - 12:55:22
---

    4 ovos
    2 chicaras de açúcar
    1 lata de milho verde
    1 chícara de óleo
    1 pitada de sal

    Bate por 2 minutos

    Despeja numa vazilha
    Mistura
    2 chicaras de flocão
    1 de farinha de trigo
    1 colher de royal
