---
file: ciclismo.md
author: Sergio Araujo
Last Change: Tue, 12 Nov 2024 - 20:06:51
 vim:ft=markdown
tags: [bicicle, diy]
---

## Motor de esteira para bicicleta elétrica:
+ https://www.youtube.com/watch?v=DPfuHh3scK0
+ https://www.youtube.com/watch?v=nX2mKVTZoF4
+ https://youtu.be/dPmqd5l3IGg

Usando 2 baterias de skate elétrico

OBS: Motor de esteira refrigera com alto giro, se baixar o giro ele pode queimar

Controle de potência: https://www.youtube.com/watch?v=FSjRm7Hoa-s
Usa-se PWM (mais eficiente controle de potência)

Em outro vídeo vemos uma ponte retiricadora de 25 ampreres sendo usada:
+ https://www.youtube.com/watch?v=pim5qKXCjXM

Como testar o motor de esteira ligando em uma fonte de computador ATX:
+ https://www.youtube.com/watch?v=zy7kwKCXvIk

## Calculo de polias:
+ https://www.youtube.com/watch?v=n6O5fuXcbVI

    (expessura do eixo / polia ) * rotação

### Ligando e controlando velocidade de um motor de esteira- Parte 1
https://www.youtube.com/watch?v=fCcEA5g3zI8

## Resolução do contran sobre bicicletas elétricas (excepcionalidade)

Fica excepcionalizada da equiparação prevista no caput deste artigo a bicicleta dotada originalmente de motor elétrico auxiliar, bem como aquela que tiver o dispositivo motriz agregado posteriormente à sua estrutura, sendo permitida a sua circulação em ciclovias e ciclo faixas, atendidas as seguintes condições:

I – com potência nominal máxima de até 350 watts;

II – velocidade máxima de 25 km/h;

III – serem dotadas de sistema que garanta o funcionamento do motor somente quando o condutor pedalar;

IV – não dispor de acelerador ou de qualquer outro dispositivo de variação manual de potência;

V – estarem dotadas de:

a) indicador de velocidade;

b) campainha;

c) sinalização noturna dianteira, traseira e lateral;

d) espelhos retrovisores em ambos os lados;

e) pneus em condições mínimas de segurança.

VI – uso obrigatório de capacete de ciclista.

# Dicas sobre ciclismo

K7 - mecanismo melhor que catrata pois o conjunto é separado das engrenagens
Free Hub - Mecanismo tipo catraca para K7
Corôa Narrow Wide - (para usar só uma corôa - Single)

O k7 XT os Sram são melhores pois cada engrenagem é independente
possibilitando alterações de modo mais simples

K7 11-40 já vem com super kog, ideal para Single

## kit de alen 15 em 1

    Kit de Ferramentas Jogo de Chaves para Bicicleta 15 em 1 - Gara

## Alihador de gancheira
+ https://www.youtube.com/watch?v=onSJHfvRVOw
+ https://pedaleria.com/fabricando-alinhador-de-gancheira/

## DIY eletric bicycle - Spanish version
+ https://www.youtube.com/watch?v=j83HuBskedc

## Baterias mais eficientes - LTO Cells

## Motor para bike elétrica de 500w
+ https://pt.aliexpress.com/item/4001026853453.html

## Alinhamento da corrente (chain line)
+ https://www.youtube.com/watch?v=lvoOqwJMBB0
+ https://www.youtube.com/watch?v=i8Yr-wOYDv4
Para fazer arruelas use eixo trazeiro para bicicleta com marchas

## Prolongamento de um câmbio shimano
+ https://www.youtube.com/watch?v=0H1YJQGdPPY

Substitua o goat link por um prolongado, o que permite que a corrente
abrace melhor os kogs menores

## Diferença entre 10 e 11 velocidades

## Catraca/k7 qual a diferença?
+ https://www.youtube.com/watch?v=LJOpj5yjRs0

## Transformando pedivela com 3 coroas em pedivela single Pedaleria
+ https://www.youtube.com/watch?v=gjgg955Ylgo

## Fita anti-furo caseira
+ https://www.youtube.com/watch?v=P7T5pQTicsY
+ EVA de 10mm
+ Garrafa Pet (policarbonato)
+ Fita Silver Tape
+ Cola instantânea

## Instalando pedivelas modernas em bikes antigas. Pedaleria
+ https://www.youtube.com/watch?v=3pI8ngeGVL8

## Vida nova ao quadro com a rosca do central espanada. Instale um salva quadro
+ https://www.youtube.com/watch?v=VSIImmKBGw4

## Relação de marchas ideal pra mim

Corôa única de 28 (narow wide) + super cog de 42

    28 x 42 ......... 0.666

    equivale a uma relação de corôa dupla 38 e 24 dentes na corôa
    e 36 no maior cog: 0.666

## O que é o BCD do pé de vela?
É a distância entre os pinos de fixação do pé de vela

Significa Bolt Circle Diameter - Bolt Circle Diameter or BCD is the diameter
of the circle that goes through the center of all of the bolts on your
chainring.  On bicycle chainring this dimension is usually measured in
millimeters.  It is critical to know the BCD of your crankset when you are
selecting a new chainring for your bike.  In many cases the BCD is printed
right on the chainring

