---
File: sxhkbd.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 12, 2020 - 16:31
tags: [tools]
---

#  sxhkbd.md intro:
+ https://www.youtube.com/watch?v=2ClckQzJTlk
+ https://wiki.archlinux.org/index.php/Sxhkd

Simple X HotKey Daemon

sxhkd defaults to `$XDG_CONFIG_HOME/sxhkd/sxhkdrc` for its configuration file.
An alternate configuration file can be specified with the -c option

## Configure middle mouse behavior:
+ https://unix.stackexchange.com/a/472464/3157

## Menu with associated keybindings

```sh
# ~/.config/sxhkd/sxhkd-help
awk '/^[a-z]/ && last {print $0,"\t",last} {last=""} /^#/{last=$0}' ~/.config/sxhkd/sxhkdrc | column -t -s $'\t' | rofi -dmenu -i -no-show-icons -width 1000
```

``` sh
# Toggle Telegram
super + alt + t
    sh ~/.config/sxhkd/scripts/bspwm-toggle-visibility TelegramDesktop telegram-desktop

# sxhkd help
super + slash
    ~/.config/sxhkd/scripts/sxhkd-help

# increase Brighness
super + F10
    xbacklight -inc 10

# decrease Brighness
super + F11
    xbacklight -dec 10
# increase Brighness
super + F10
    xbacklight -inc 10

# decrease/increase Brighness
super + {F10, F11}
    xbacklight {-dec 10, -inc 10}

# Volume Controls
super + {F3, F2}
    pactl -- set-sink-volume 0  {+5%, -5%}

# prev/next desktop
control + alt + {p,n}
	bspc desktop -f {prev.occupied,next.occupied}
```


