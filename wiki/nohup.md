---
file: nohup.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# nohup
+ https://devblog.drall.com.br/utilizando-o-comando-nohup-para-manter-programas-em-execucao-mesmo-com-a-queda-da-sessao-atual

Os processos no Unix serão terminados quando você fizer logout do sistema ou sair do shell atual, não importando se eles estão rodando em foreground ou em background.

Isto ocorre porque os programas recebem o sinal HUP (hangup), que é um aviso do terminal a seus processos dependentes de que ocorreu um logout.

Uma das formas de assegurar que o processo continue rodando sem ser finalizado é utilizando o comando nohup.

nohup é um comando do UNIX que permite executar programa “desconectado” da sessão atual. Desta forma, seria possível o usuário fazer logout e o comando continuar rodando em segundo plano.

O nohup ignora os sinais de interrupção durante a execução do comando especificado. Assim, é possível o comando continuar a executar mesmo depois que o usuário se desconectar do sistema.

A saída do programa em execução é enviado para o arquivo nohup.out.

Se a saída padrão é uma tty, esta saída e o erro padrão são redirecionados para o arquivo nohup.out (primeira opção) ou para o arquivo $HOME/nohup.out (segunda opção). Se nenhum destes arquivos puderem ser criados/alterados, o comando não será executado.

Abaixo a forma geral de uso do programa:

    nohup iceweasel &
