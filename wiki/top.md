---
file: top.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# top.md

Mostra o uso de memória

## Mostrar aplicativos que consomem mais memória no topo

Após abrir o programa digite

		Shift-m
