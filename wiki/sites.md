---
Filename: sites.md
Last Change: Sat, 13 Jul 2024 - 08:02:08
---
 vim:ft=markdown

## Good sites for

### Music

+ <https://audiomack.com/>
+ <https://howtodesk.net/pt-pt/7-melhores-alternativas-ao-soundcloud-para-fazer-upload-e-ouvir-musica>

## Documments

[Registro civil](https://registrocivil.org.br/)
