---
file: dicaspython.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## How To Update/Upgrade A Python Package with Pip?
+ https://www.poftut.com/how-to-update-upgrade-a-python-package-with-pip/
+ https://stackoverflow.com/questions/2720014/

Before updating or upgrading an installed Python package we will list already
installed packages. We will use the list command which will display the
complete name and the version of the installed packages.

    $ pip list

    $ pip2 list

    $ pip3 list

Upgrade/Update Python Package To The Latest Version

We will use the install command with the --upgrade option and also provide the
package name. In this example, we will update/upgrade the package named Django
to the latest version. We will also provide the --user option. Alternatively
to the --upgrade we can use -U which is the short form.

    $ pip install --user --upgrade django

    $ pip2 install --user --upgrade django

    $ pip3 install --user --upgrade django

    pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip install -U

    pip install -U `pip list --outdated | awk 'NR>2 {print $1}'`

### Using pure python (upgrade all python packages)

``` python
import pkg_resources
from subprocess import call

packages = [dist.project_name for dist in pkg_resources.working_set]
call("pip install --upgrade " + ' '.join(packages), shell=True)
```

``` python
python -c 'import pip, subprocess; [subprocess.call("pip install -U " +
d.project_name, shell=1) for d in pip.get_installed_distributions()]'
```

# Arquivo: dicas sobre python

Curso do professor Ronaldo Ramos no Youtube: http://goo.gl/n4NxP

## python keylogger
+ https://sourceforge.net/p/pyhook/wiki/PyHook_Tutorial/
+ http://www.thesnoopybub.com/blog/basic-keylogger-in-ubuntu/
+ https://lifestohack.com/how-to-make-very-simple-keylogger/
+ http://www.codelecture.com/wp/python-3-keylogger-w-subs/
+ https://github.com/theju/linux-keylogger/blob/master/keylogger.cpp
+ http://www.tecmint.com/how-to-monitor-keyboard-keystrokes-using-logkeys-in-linux/
+ http://porterhau5.com/blog/xkeyscan-parse-linux-keylogger/
+ https://github.com/porterhau5/xkeyscan
+ https://github.com/kernc/logkeys
+ http://www.securitybydefault.com/2013/09/listado-de-keyloggers-windows-linux.html
+ http://jeffhoogland.blogspot.com.br/2014/10/pyhook-for-linux-with-pyxhook.html

## Creting virtual environments
+ https://virtualenv.pypa.io/en/latest/userguide/

    pip3 install virtualenv
    virtualenv ENV   # ENV is a dir where the virtualenv is set
    source /path/to/ENV/bin/activate

OBS: In the case of instapy I had to install libressl{,-devel}

### How to install pyenv
+ https://github.com/pyenv/pyenv-installer

    curl https://pyenv.run | bash

## Error Handling
+ http://www.pythonforbeginners.com/error-handling/python-try-and-except/

    try:
        subprocess.Popen(['mpg123', '-q', file]).wait()
    except KeyboardInterrupt:
        print('You cancelled the operation.')

##  Using a credentials file in your python scripts
create a file called `credentials.py` or any name you want.
on the script you want to read these credentials make:

    --------------credentials.py------------------
    PASSWORD = 'yourpassord'
    USERNAME = 'superduperpassword'
    ----------------------------------------------

    import credentials

    user,password(settings.USER, settings.PASSWORD)

If our credentials are in a different folder we can do:

``` python
# some_file.py
import sys
# insert at 1, 0 is the script path (or '' in REPL)
sys.path.insert(0, '/the/folder/path/name-folder/')

import file
```

## python [selenium](selenium) - library to access websites

## Invertendo uma lista no python3
+ https://dbader.org/blog/python-reverse-list
Se tentar-mos usar "reversed" diretamente o python criará um objeto
como se vê no código a seguir...

    >>> reversed(mylist)
    <list_reverseiterator object at 0xb71a584c>

No entanto se chamar-mos a mesma função dentro de "list()"
obteremos o resultado desejado.

    >>> list(reversed(mylist))
    [5, 4, 3, 2, 1]

## Consertando legengas srt com python

source: http://tuxbalaji.wordpress.com/2013/10/05/how-to-fix-subtitles-delay-or-ealier-with-your-movies-by-python-code/

``` python
>>> subs.shift(seconds=-2) # Move all subs 2 seconds earlier
>>> subs.shift(minutes=1)  # Move all subs 1 minutes later
```

Passando parâmetro para o script

		import sys
		str(sys.argv[1])

``` python
##! usr/bin/python
import pysrt
subs=open("/home/bala/Pirates of the Caribbean -The Curse of the Black Pearl(2003).srt")
subs.shift(minutes=-2) # Move all subs 2 minutes earlier
subs.save('/home/bala/new.srt', encoding='utf-8')#saves file with new.srt in your home directory
```

## Instalando pacotes python

    sudo pip search package
    sudo pip install package
    sudo pip uninstall package

## baixando legendas com o subliminal

## no caso do ubuntu ou debian use...

    sudo apt-get install python-pip
    sudo pip install subliminal

    subliminal -l en -- The.Big.Bang.Theory.S05E18.HDTV.x264-LOL.mp4

