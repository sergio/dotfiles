---
Filename: luakit.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [browser, lua]
 vim:ft=markdown
---

## Shortcuts:
+ https://wiki.archlinux.org/title/Luakit#Browsing
+ https://luakit.github.io/docs/pages/02-faq.html

```markdown
    :help ................ help system
    o .................... pronpt to type url (mnemonic open)
    t .................... open url in new tab
    r .................... refresh current page
    Ctrl+tab ............. circle tabs
    Ctrl-w ............... close current tab
    Ctrl+shift+r ......... restart and reload config
    d .................... close current tab
    O .................... edit current url
    :open google foobar will search foobar on Google).
    H .................... get back
    L .................... go forward in history
    u .................... open last closed tab
    y .................... copy url to primary selection
    Y .................... copy url to the clipboard
    j .................... scroll down
    J .................... next tab
    K .................... previous tab
    k .................... scroll up
    Ctrl-d ............... scrool half page down
    Ctrl-u ............... scrool half page up
    Ctrl-o ............... go back in browser history
    Ctrl-i ............... go forward in browser history
    gh ................... open homepage
    gH ................... open homepage in a new tab
    ZZ ................... quit and save sesstion
    ZQ ................... quit and don't save session
    Ma ................... create quickmark 'a'
    goa .................. go to quickmark 'a'
    pt ................... primary selection in a new tab
    PT ................... clipboard in a new tab
    Ctrl+a ............... increment last number in a url
    F12 .................. opens web inspector
```

## Quick marks:

```markdown
    My ................... create mark y (youtube)
    gny .................. go to new tab using y mark
```



## config file:

```markdown
-- ~/.config/luakit/userconf.lua
-- ln -sfvn ~/.dotfiles/luakit/userconf.lua ~/.config/luakit/userconf.lua

settings.window.home_page = "www.google.com"
```

To see your settings type:

    luakit://settings/

## fonts:
+ https://github.com/luakit/luakit/issues/406

Create dir: .local/share/luakit/styles
Then create a css file - like forcefont.css

```css
 @-moz-document regexp(".*") {
   * {
     font-family: monospace !important;
     font-size: 16px !important;
     color: red !important;
   }
 }
```

## plugins:
+ https://github.com/luakit/luakit-plugins

# vim:cole=0:
