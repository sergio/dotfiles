---
file: zenity.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# zenity.md

Zenity – Creates Graphical (GTK+) Dialog Boxes in Command-line and Shell Scripts

I have this alias

    alias pbcopy='xclip -selection clipboard'
    zenity --entry | pbcopy

    zenity --calendar
