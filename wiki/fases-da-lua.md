---
File: ~/.dotfiles/wiki/fases-da-lua.md
Last Change: Wed, 22 Jan 2025 - 17:43:30
tags: [weather, clima, agricultura, natureza]
---

# Fases da Lua e Tipos de Plantas na Cultura Popular

De acordo com a cultura popular e práticas tradicionais de jardinagem lunar, as fases da lua são associadas a diferentes tipos de plantas e suas características de crescimento. Abaixo está um resumo das fases da lua e os tipos de plantas que se acredita serem mais adequadas para cada fase.

---

## 1. Lua Nova
- **Descrição**: A lua não é visível no céu, e a energia é considerada introspectiva, favorecendo o crescimento das raízes.
- **Plantas recomendadas**:
  - **Raízes e vegetais subterrâneos**: Cenoura, beterraba, cebola, batata, alho, rabanete e mandioca.
  - **Leguminosas**: Feijão, ervilha, lentilha e grão-de-bico.
- **Atividades recomendadas**: Preparação do solo, semeadura de plantas que crescem abaixo da terra e adubação.

---

## 2. Lua Crescente
- **Descrição**: A lua está aumentando em luminosidade, e a energia é favorável ao crescimento das partes aéreas das plantas (folhas e frutos).
- **Plantas recomendadas**:
  - **Folhas e vegetais acima do solo**: Alface, espinafre, couve, salsa, agrião, brócolis e repolho.
  - **Frutas e legumes**: Tomate, abóbora, melancia, pepino, pimentão, feijão e ervilha.
  - **Flores anuais**: Girassol, margarida, amor-perfeito e petúnia.
- **Atividades recomendadas**: Plantio, fertilização, irrigação frequente e desbaste de plantas.

---

## 3. Lua Cheia
- **Descrição**: A lua está totalmente iluminada, e a energia é considerada ideal para colheitas e maturação dos frutos.
- **Plantas recomendadas**:
  - **Colheita de frutos e vegetais maduros**: Tomate, abóbora, pimentão, melancia e uva.
  - **Flores e ervas**: Colheita de flores para arranjos e ervas para uso culinário ou medicinal.
- **Atividades recomendadas**: Colheita, preparação do solo para o próximo ciclo e adubação pós-colheita.

---

## 4. Lua Minguante
- **Descrição**: A lua está diminuindo em luminosidade, e a energia é voltada para o interior, favorecendo o crescimento das raízes e a manutenção do jardim.
- **Plantas recomendadas**:
  - **Raízes e vegetais subterrâneos**: Beterraba, cenoura, batata, cebola e rabanete.
  - **Plantas perenes e bulbos**: Morango, framboesa, lírio, tulipa e narciso.
- **Atividades recomendadas**: Poda, desbaste, transplante de plantas e controle de pragas.

---

## Resumo das Fases Lunares e Tipos de Plantas

| **Fase Lunar**   | **Tipo de Planta**                  | **Exemplos**                          |
|------------------|-------------------------------------|---------------------------------------|
| **Lua Nova**     | Raízes e vegetais subterrâneos      | Cenoura, batata, alho, beterraba      |
| **Lua Crescente**| Folhas, frutos e flores             | Alface, tomate, abóbora, girassol     |
| **Lua Cheia**    | Colheita de frutos maduros          | Tomate, melancia, uva                |
| **Lua Minguante**| Raízes, bulbos e manutenção         | Beterraba, lírio, tulipa              |

---

## Considerações Finais

A jardinagem lunar é uma prática ancestral que combina observação da natureza e tradição. Embora a ciência ainda não comprove de forma conclusiva a influência direta da lua no crescimento das plantas, muitos agricultores e jardineiros continuam a seguir esses ciclos, baseados em experiências práticas e sabedoria popular. Se você deseja experimentar, alinhar suas atividades de plantio e colheita com as fases da lua pode ser uma maneira interessante de se conectar com os ritmos naturais.

---

**Nota**: Para mais detalhes, você pode consultar fontes especializadas em jardinagem lunar ou práticas agrícolas tradicionais.
