---
file: weasyprint.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [html2pdf, htmltopdf, pdf, convert, tools, python3]
---

# weasyprint (python module)
+ https://weasyprint.org/

    sudo pip3 install easyprint

## How to use it?
+ https://weasyprint.readthedocs.io/en/stable/

    weasyprint https://weasyprint.org/ weasyprint.pdf

