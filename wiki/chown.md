---
file: chown.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução
Modifica o dono de arquivos e pastas (propriedade)

    chown -R user:user /home/user

    chown -v greys:greys try
    changed ownership of 'try' from root:root to greys:greys

