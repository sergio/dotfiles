---
File: ~/.dotfiles/wiki/phrases.md
Author: Sergio Araujo
Last Change: Tue, 27 Aug 2024 - 11:08:54
tags: [english, phrases]
---

# English phrases

The demure behavior of the actress in the interview surprised her fans.
O comportamento recatado da atriz na entrevista surpreendeu seus fãs.

After his birth, a seer had given his mother a cryptic prophecy
Após seu nascimento um vidente deu à sua mãe uma profecia enigmática

You've got a day or two days in the town, how do you get your bearings?
você tem um ou dois dias na cidade, como você se orienta?

The future seemed bleak after the company announced layoffs
O futuro parecia sombrio após a companhia anunciar demissões

The bear assumed that it's now-mauled prey was finally dead
O urso presumiu que sua agora atacada presa estava finalmente morta

The assertion that our civilization is contradictory sounds even trite.
A afirmação de que nossa civilização é contraditória parece até banal.

Bemused and a bit bewildered. That does
Confuso e um pouco perplexo. Isso faz

Starch molecules break down into glucose during digestion.
As moléculas de amido se decompõem em glicose durante a digestão.

After losing his job he had to scrimp on every little expense to make ends meet
Após perde o emprego ele teve que reduzir cada pequena  despesa para suas
contas fecharem

If they act too early, they may stifle innovation.
Se eles agirem muito cedo podem sufocar a inovação.

China accuses US of ‘slander and hype’ over air incident
China acusa EUA de "calúnia e exagero" sobre o incidente aéreo

Why don’t we do something together for a change?
Por que nós não fazemos alguma coisa juntos pra variar?

What’s the outlook for the 2022 global economy?
Quais as perspectivas para a economia global para 2022?

The civil rights movement marked a period of socila upheaval in the U.S.
O movimento civil por direitos marcou um período de convulsão social nos EUA

The 'shoot and scoot' tactics being used by Russian forces
As táticas de 'atirar e fugire' usadas pelos forças russas

I look forward to getting the jab soon, I want to feel safer against the virus
Estou ansioso para tomar a vacina logo, quero me sentir

We've got to talk a little bit about stalling.
Temos que falar um pouco sobre protelar.

I want you to learn how to write well. You have to know the nuts and bolts of writing.
Eu quero que você aprenda a escrever bem. Você tem de saber o básico da escrita.

Most people don’t think of Mike as a real actor.
He is pigeonholed as an action movie star.

The government has launched a polio vaccination drive to eradicate polio from India
O governo lançou uma campanha de vaciação para erradicar a poliomielite na Índia.

And so I found myself at this kind of really [snazzy] media party
E então e me ví nesse tipo de festa midiática muito [elegante]

At times this union with things divine can be carried to extremes, religious zealotry.
Áz vezes essa ligação com as coisas divinas é levada aos extremos, ao fanatismo religioso.

Interesting contrast between the slight trickle of a pond and the power of a water fall.
Interessante contraste entre um leve gotejar de um poço e o poder deu uma
cachoeira.

Answering the niggling little questions about the new consoles.
Respondendo às pequenas perguntas mesquinhas sobre o novo console.

You might have to go to the bank to ascertain if there is any money in your account.
Você talvez tenha que ir ao banco para veriricar se há algum diheiro em sua conta.

I feel bad for little mice. I don’t like to kill them.
Eu tenho dó de ratinhos. Eu não gosto de matalos.

We were hoping to finish by 5, but as it is, we’ll be lucky to finish by 8!
Esperamos terminar até as 4, mas do jeito que vai, teremos sorte se terminarmos até as 8

Pull firmly and steadily on the tick until it lets go, then swab[cotonete] the bite site with alcohol.
Puxe com firmeza e continuamente o carrapato até que ele se solte e, em seguida, limpe o local da picada com álcool.

After the shock, Alice wiped her tears and pulled herself together.
Após o choque, Alice enxugou as lágrimas e se recompôs.

Pull yoursel together, Annie. Don't get upset about such a silly thing.
Controle-se, Annie. Não fique chateada com uma coisa tão boba.

That’s been a crucial development, but it’s just a patchwork, and we’ll need to take much bigger steps in order to make sure this reaches everyone.
É um desenvolvimento crucial, mas é apenas uma couxa de retalhos, e precisamos de passos maiores a fim de nos certificar que alcance a todos.

If I could turn the clock back and do things differently, I would.
Se eu pudesse voltar atrás e fazer as coisas de um jeito diferente, eu faria.

In that first strum I'm just completely zoned into what I'm doing.
Nesse primeiro dedilhado eu estou completamente concentrado no que estou fazendo.

Your arm is not broken, just bruised.
Bem, seu braço não está quebrado, só machucado.

Fresh COVID-19 outbreaks force governments to backtrack on reopening
Novos surtos de COVID forçam governos a recuar abertura

I took my wife to the mall
Eu levei minha esposa para o shopping

Monk, the detective, really cracks me up.
Monk, o detetive, me racha de rir, de verdade.

Deadly Myanmar crackdown becomes test for the West
Mortal repressão em Myamar torna-se um teste para o ocidente

In so doing, he upset the status quo, enraged some, and transformed our world.
Ao fazer isso, ele chateou o status quo, enfureceu alguns e transformou o mundo

I don’t think we are going to be able to get past this check point.
Acho que nós não vamos conseguir passar dessa barreira de fiscalização.

This highway is a godsend to the local community
Esta estrada é uma dádiva de Deus para a comunidade local.

He is showing leadership through his actions and we commend him for doing so and are hopeful that his party heeds his message.
Ele está demonstrando liderança por meio de suas ações, e nós o elogiamos por isso e esperamos que seu partido ouça sua mensagem.

The wreckage of the vessel was found last week on the seabed over 130 feet below the waves.
Os destroços da embarcação foram encontrados semana passada no fundo do mar 130 pés abaixo das ondas.

My wife goes, “Let’s go out (a little);” and I go, “Where if everything is closed?”
Minha esposa disse, vamos sair; e eu disse, "pra onde se tudo está fechado?"

How do you two know each other?
Como vocês se conhecem?

Together we shall traverse this heavenly path to excellence.
Juntos devemos atravessar esse caminho celestial para a excelência

The first-class passengers had padded seats and a steward to serve them.
Os passageiros da primeira classe tinham assentos acolchoados e um comissário de bordo para atendê-los.

Is the New York Post a reliable news source?
O New York Post é uma fonte confiável de noticas?

I am halfway through the book "War and Peace".
Estou na metade do livro "Guerra e Paz"

Obviously, you'll only be able to invite a select number of guests but at least you won't have to worry about catering .
Obviamente você só poderá receber um número seleto deconvidados mas pelo menos você não tem que se preocupar com o atendimento

“It’s put us more on the map than we were already,” he muses.
"Isso nos colocou mais no mapa do que já estávamos," reflete ele.

Given the sea of features Vim offers, we always find something useful to share with our readership.
Dado o oceano de recursos que o vim oferece, nós sempre encontramos algo útil para compartilhar com nossos leitores.

A series of short, snappy sets, recreating the anarchic energy of a late night comedy gig.
Uma série rápida de sets, recriando a energia anárquica de um show de comédia noturno.

It must jarring to hear from me out of the blue.
Deve ser desagradável ter notícias minhas assim do nada.

He had a nightcap on his head and his feet were bare.
Ele usava uma touca e seus pés estavam descalços.

Being squashed into an aircraft like sardines is also a fairly recent phenomenon.
O fenómeno de as pessoas terem de sentar-se no avião como sardinhas em lata também é relativamente novo.

George has two strikes against him already. Everybody is against what he wants to do.
O George está em total desvantagem. Todo mundo é contra o que ele quer fazer.

Now that they have six children, their little house is bursting at the seams.
Agora que eles tê 6 filhos, a casinha deles está apertada.

The races are disputed over different courses, with upwind and downwind legs.
As corridas são disputadas em diferentes percursos, contra e a favor do vento.

Last year female jockeys outperformed their male counterparts.
No ano passado, as jóqueis do sexo feminino tiveram superaram seus
colegas homens.

I'm not suggesting we go and blatantly break some grey-area laws by stealing data
Não estou sugerindo que descaradamente quebremos algums áreas cinzentas roubando dados

I know people are worried about this new virus – and rightly so. But we’ve been through this before, and we’ll get through this together.
Eu sei que as pessoas estão preocupadas com esse novo vírus - e com razão. Mas já passamos por isso antes e vamos superar isso juntos.

Parents tell off their children very often for their bad behavior
Pais repreendem seus filhos muito frequentemente por mau comportamento

I had only been reading the textbook instead of going to classes, so a few of the questions on the final exam really threw me a curve ball.
Eu só andava lendo os livros ao invés de ir às aulas, então algumas perguntas na prova final me pegaram bem desprevenido.

They took high premiums to bolster up the funds.
Eles receberam altos prẽmios para reforçar os fundos.

European Central Bank pledges “no limits” to bolster economies
Banco Central Europeu promete "sem limites" para impulsionar a economia

The uncertainty wrought by the coronavirus pandemic
A incerteza provocada pela pandemia do coronavirus

'It was all absolute nonsense, complete poppycock ,' said Lennon.
'Foi tudo sem sentido, papo-furado total, disse Lennon'

For the first few months I had trouble finding my feet.
Nos primeiros meses, eu tive dificuldades em me adaptar.

Japanese politics has been reeling from crisis to crisis.
A política japonesa vem cambaleando de crise em crise.

The candidates will be nominated at the coming caucus.
Os candidatos serão nomeados na próxima convenção de partido.

I sat back down and resumed my editing, sticking the grape lollipop back in my mouth.
Recostei-me e retomei a edição, enfiando o pirulito de uva na boca.

There are still one million people working as manual scavengers all over India.
Ainda há um milhão de pessoas trabalhando como catadores manuais em toda a Índia.

He explained the rationale behind the change
Ele explicou a lógica por tráz da mudança

She refuses to take sides on the issue.
Ela se recusa a tomar partido nesse assunto.

Today’s challenge isn’t for the fainthearted.
O desafio de hoje não é para os fracos

She kept the employees under her thumb.
Ela mantinha os empregados sob total controle.

Once he had found his feet he was able to deal with any problem.
Assim que ele se adaptou, ele conseguia lidar com qualquer problema.

If they don’t like you, it stands to reason they won’t give you the job.
Se elas não gostarem de você, está na cara que não vão te dar o emprego.

If you didn’t sign a contract, you won’t have a leg to stand on.
Se você não assinasse o contrato, você não teria como provar.

For the umpteenth time, Anthony, knives and forks go in the middle drawer!
Pela enésima vez, Anthony, facas e garfos vão na gaveta do meio!

Would anyone happen to have room in your refrigerator for a large birthday cake?
Alguém teria espaço na sua geladeira para um grande bolo de aniversário?

Do you happen to have room in your refrigerator for a large cake?
Por acaso você tem espaço na geladeira para um bolo grande?

Bespoke programming can be carried out to an agreed customer specification
Programação sobe medida pode ser realizada para se adequar às especificações dos clientes

I just got home.
Acabei de chegar em casa.

By no stretch of the imagination does the factory operate efficiently.
A fábrica, nem em sonhos, opera de modo eficiente.

By no stretch of the imagination could he be seriously described as an artist. (Nem de longe ele poderia ser seriamente descrito como um artista.)

She had a slight cough and a sniffle
Ela tinha uma ligeira tosse e um fungado

I bought some sneakers online during the quarantine.
Eu comprei alguns tênis durante a quarentena.

It’s not a lot of money, but it’s nothing to sneeze at.
Não é muito dinheiro, mas não é de se jogar fora.

First principles thinking requires you to abandon your allegiance to previous forms
Princípios fundamentais de pensamento requerem que você abandone sua sumbição a formas prévias

He must’ve cooked up his scheme on the spur of the moment.
Ele deve ter bolado esse esquema no calor do momento.

the doctor had a serious heart-to-heart with Jane, and said: Jane, you need to lose some weight
"heart-to-heart - conversa franca/séria"

I want to get better at table tennis
Eu quero melhorar no tenis de mesa

They normally stay around 200ft in the air and sometimes fly in tandem with other drones.
Eles normalmente ficam cerca de 200 pés no ar e às vezes voam em conjunto com outros drones.

There's a big bottled beer stall of foreign booze
Há uma grande barraca de cerveja engarrafada de bebida estrangeira

Anyone who's been on a beach has encountered gulls, usually in large numbers
Qualquer um que já esteve em uma praia encontrou gaivotas, normalmente em grande número

There are moments of levity , but they don't last long.
Há momentos de leveza, mas eles não duram muito.

The Christmas store is closed, Michael. Now what?
As lojas de natal estão fechadas, Michael. E agora?

Pizza no onions
Pizza sem cebola

Pizza and onions
Pizza com cebola

Talk to you
Falar com você

Rice and beans
Arroz com feijão

Pay by check
Pagar com cheque

We reckon we have more scientific evidence than your guess at a figure.
Acreditamos que temos mais evidências científicas do que você imagina. (reckon
- contar considerar supor)

    The company constrained its production volume to reduce costs.
    A empresa restringiu seu volume de produção para reduzir custos

    She put the whammy on me.
    Ela colocou uma macumba em mim.

    I'm just gonna wrap up a few things, and then I’m going to head home.
    Vou terminar algumas coisas e depois vou para casa.

    We live in cities, in towns, and in rural communities, in houses instead of huts and caves.
    Vivemos nas cidades, nas cidades e nas comunidades rurais, nas casas em vez de cabanas e cavernas.

    She was well known in the estate for her love of life and her quirky sayings.
    Ela era bem conhecida na propriedade por seu amor à vida e por seus ditados peculiares.

    He started taking the mickey out of this poor man just because he is bald.
    Ele começou a tirar onda desse pobre coitado só porque ele é careca.

    I didn’t know whether Josh was taking the Mickey out of me or not.
    Eu não sabia se o Josh estava me zoando ou não.

    Our hearts must not harbour hate, anger, bigotry , violence or discrimination.
    Nossos corações não devem habitar ódio, raiva, intolerância/fanatismo, violência ou descriminação

    Learning to juggle is a neat trick for the brain as well as the hands.
    Aprender malabarismo é um truque legal para o cérebro assim como para as mãos.

    So, you take a deep breath, juggle all your bags and begin to weave across the puddles.
    Então, respire fundo, equilibre todos os sacos de compras e comece a tentar fugir das poças de água

    The hallway is wall-to-wall with Jimmy’s toys.
    O corredor está com brinquedos do Jimmy para todo lado.

    The center of the bullseye of our mission is to then educate people across this country
    O centro do alvo de nossa missão é, então, educar as pessoas em todo o país

    An economy left in tatters and questions still unanswered
    Uma economia deixada em frangalhos e perguntas ainda sem resposta

    Drugs that quell brain inflammation reverse dementia
    Drogas que debelam inflamação cerebral revertem demencia

    That chick has prefect tits, she's fucking gnarly.
    Aquela garota tem seios perfeitos, ela é o máximo. (gíria)

    She began to punch and kick the air in mock battle.
    Ele começou a socar e chutar o ar em uma batalha simulada.

    But would you make fun of her, laugh and mock at her?
    Mas você zombaria, riria e faria pouco caso dela?

    I must clear out my wardrobe and get rid of my old clothes
    Devo limpar meu guarda-roupa e me livrar de minhas roupas velhas

    I may stop by your house.
    De der eu dou uma passada na sua casa

    I just stopped by to check in on you and say hello.
    Eu só passei para ver como você está e dizer olá.

    It's funny, it's smart, it's endearing and it's a little sassy too.
    É engraçado, inteligente, cativante e também um pouco atrevido.

    The public adores her smart, sassy and succesfull presentations.
    O público se encanta com seuas inteligentes, atrevidas(petulantes) apresentações.

    I am not afraid of rolling up my sleeves and tackling these issues.
    Eu não tenho medo de arregaçar as mangas e lidar com esses assuntos.

    I don’t think I will ever live down the foolish way I behaved during dinner the other night.
    Eu acho que nunca vou esquecer o quão bobo eu fui outro dia no jantar.

    She may stop by the bank
    Se der ela passa no banco

    As long as you bought the car, could you give me a ride to the city center?
    Já que você comprou o carro, você poderia me dar uma carona para o centro?

    From now on I will not drink
    De hoje em diante eu não vou mais beber

    From now on my life will be different
    De hoje em diante minha vida será diferente

    I was going to your house, but I had a last minute thing
    Eu tava indo pra sua casa, mas apareceu algo

    The guerrillas called a three-day truce
    Os guerrilheiros pediram uma trégua de três dias

    It looks like we are in for rain.
    Parece que estamos prestes a ter uma chuva.

    If you don’t participate on our site, you are in for it.
    Se você não participar no nosso site, você vai ver só.

    It was Bob all along.
    Era o Bob o tempo todo.

    I knew it was him all along.
    Eu sabia que era ele o tempo todo.

    You are in for a surprise today.
    Vocẽ vai ter uma surpresa hoje.

    I hear Tony and Helen have shacked up together.
    Fiquei sabendo que Tony e Helen estão morando juntos.

    They should find a house and not a shack awaiting them.
    Eles deveriam encontrar uma case e não um barraco esperando por eles.

    If you find yourself at a loose end, you could always clean the bathroom.
    Se você estiver aí à toa, você pode limpar o banheiro.

    Please head over to the Podcasts app and leave a review for this podcast
    Por favor vá até o aplicativo de podcast e deixe um review para esse podcast.

    This is the first time that the union has has cast such a vote.
    Essa é a primeira vez que o sindicato vota desse modo.

    Who would have thought we’d still be quarantined in May?
    Quem diria que estariamos em quarentena em Maio?

    I was at loose ends after finishing school and not being able to find a job.
    Eu estava à toa depois que terminei a escola e não conseguia arranjar um emprego.

    Police departments have a lot of surveillance tools to identify protesters and looters
    Departamentos de polícia possuem muitas ferramentas de vigilância para identificar manifestantes e saqueadores.

    She told him he was a lazy good-for-nothing and should get a job.
    Ela disse que ele era preguiçoso, um à toa e deveria arrumar um emprego.

    Is the visa process deterring international students?
    O processo de visto está dissuadindo estudantes internacionais?

    It’s not for nothing that he lives next to his parents.
    Não é à toa que ele mora perto dos pais.

    He could not attend to the meeting, so he sent his surrogate.
    Ele não pôde comparecer à reunião, então enviou seu substituto.

    I tried getting out of walking today, but my excuse didn’t work.
    Eu tentei escapar da caminhada de hoje, mas não teve escapatória.

    That was a clear attempt to curry favor with the future emperor and his sister.
    Aquilo foi uma tentativa clara de ganhar favores com o futuro imperador e sua irmã.

    In doing so, he subjects central tenets of modern economics to trenchant criticism.
    Ao fazê-lo, ele submete os princípios centrais da economia moderna a
    críticas perspicazes.

    An unmarked young hog born in the woods, however, could be claimed by the first person to find it.
    Um jovem porco não marcado nascido na floresta, no entanto, poderia ser
    reivindicado pela primeira pessoa a encontrá-lo.

    You may be an importer who dreams of haggling with a feisty Moroccan carpet seller in perfect Arabic.
    Você pode ser um importador que sonha em pechinchar com um vendedor de tapetes marroquino mal-humorado em árabe perfeito.

    I am a little bewildered by the Commissioner's reply.
    Estou um pouco perplexo com a resposta do Senhor Comissário.

    Even Usain Bolt is a slowpoke next to a cheetah or a lion.
    Até mesmo Usain Bolt é lerdo comparado com um cheetah ou um leão.

    Society’s accelerating pace is shredding our patience (shred - retalhar)
    O ritmo acelerado da sociedade está destruindo nossa paciência

    And that is the real story of the tragic demise of Liam Lawlor.
    E esta é a história real da trágica morte de Liam Lawlor.

    He then looked over towards the tabby kitten and touched it.
    Ele então olhou para o gatinho malhado e o tocou.

    I did a bunch of terrible things in Milan and I always knew at one time I
    was gonna have to atone for it
    Eu fiz um monte de coisas terríveis em Milão e sempre soube que uma vez eu
    teria que expiar isso

    Luckily enough, this will suffice as our reference guide for both.
    Felizmente, isso será suficiente como nosso guia de referência para ambos.

    The Space Pirates are just too ruthless to get talked into repenting their ways.
    Os piratas espaciais são implacáveis demais para serem convencidos a se arrepender.

    After a few polite pleasantries he put the car into gear and drove off.
    Depois de algumas gentilezas educadas, ele engatou o carro e partiu.

    We’re really into reading.
    A gente curte muito ler.

    The online world brought us a bunch of fly-by-night professionals who do shoddy work.
    O mundo online nos trouxe um bando de profissionais oportunistas que fazer um trabalho duvidoso.

    He’s slightly aggressive, which a lot of people find off-putting when they first meet him.
    Ele é um pouco agressivo, o que faz com que muitas pessoas achem desagradável quando o conhecem.

    What I found off-putting was the amount of work that you were expected to do.
    O que achei desanimaro foi a quantidade de trabalho que se esperava de você.

    We want everyone to enjoy the giant pandas. Please don’t linger if other people are waiting.
    Queremos que todos aproveitem os pandas gigantes. Por favor não se demore se outras pessoas estiverem esperando.

    But despite official denials, the decree is sweeping in its scope.
    Mas, apesar das negações oficiais, o decreto é abrangente em seu escopo.

    That was not an accident! You did it out of spite.
    Isso não foi um acidente. Você fez isso só por vingança.

    Excuse me, have you got a moment?
    Com licença, você tem um momento?

    Sorry to bother you, but could I just ask you something? –
    Desculpa por te incomodar, mas eu poderia te perguntar algo?

    I may go
    Talvez eu vá

    It may rain today
    Talvez chova hoje

    I may stop by your house
    Eu talvez passe na sua casa

    Shortly after the moon landing, interest in the space program withered
    Logo após o pouso na lua, o interesse no programa espacial murcha

    I totally forgot that I have an exam tomorrow and I didn’t study. I think I’m screwed.
    I’m looking forward to the holidays. Não vejo a hora para entrar de férias.

    In fact, the next album looks set to dabble( a lot more in politics)
    De fato, o próximo álbum deve se envolver muito mais com política.

    The series, according to its producers, is avowedly apolitical.
    A série, de acordo com seus produtores, é declaradamente apolítica.

    The Fascist Party openly avowed its aversion to democracy and the liberal state.
    O Partido Fascista declarou abertamente sua aversão à democracia e ao Estado liberal.

    I’m going to take a few days off now at the end of the year.
    Vou tiar alguns dias de folga no final do ano.

    Drugs that quell brain inflammation reverse dementia.
    Drogas que suprimem inflamação cerebral revertem demencia.

    He wore a smock , gardening gloves, and a pair of half-moon glasses with a smudge of mud on them.
    Ele usava uma bata, luvas de jardinagem e um par de óculos de meia-lua com uma mancha de lama neles.

    I felt my lips tighten in a rictus that was closer to a grimace than a smile.
    Senti meus lábios se apertarem em um ricto que estava mais próximo de uma careta do que de um sorriso.

    Use your wits and ingenuity to solve over 100 puzzles and earn over 300 medals.
    Usa a tua (perspicácia|inteligência) e o sua engenhosidade para resolveres mais de 100 puzzles e ganhares mais de 300 medalhas.

    She is strolling on the beach
    Ela está caminhando na praia

    The second echelon was comprised of an armored division and a corps army aviation brigade.
    O segundo escalão era composto por uma divisão blindada e uma brigada de aviação do exército.

    Someone who is this unhinged sounds like the type who might turn on his benefactors.
    Alguém que é tão desequilibrado soa como o tipo que pode se voltar contra seus benfeitores.

    his body jerked with the recoil of the rifle
    Seu corpo foi empurrado pelo recudo do rifle

    After he had traveled a little way, he spied a dog lying by the roadside and panting as if he were tired.
    Depois de ter viajado um pouco, viu um cachorro deitado à beira da estrada e ofegando como se estivesse cansado.

    She has good manners, yet tends to be a bit of a tattletale at times.
    Ela tem boas maneiras, mas tende a ser um pouco linguaruda às vezes.

    The blurb on the back of the book says that it will touch your heart.
    A sinopse na parte de trás do livro diz que tocará seu coração.

    Something tells us that the remake is going to be a botch
    Algo nos diz que o remake vai ser um fiasco

    I have an employee who is always hitting me up for a raise.
    Eu tenho um empregado que está sempre me pedindo aumento.

    She taught me to let my imagination roam freely.
    Ela ensinou-me a deixar minha imagiação vaguear livremente.

    I had a splitting headache and was very parched.
    Eu tive uma dor de cabeça de rachar e estava com muita sede.

    Information pertaining to this configuration is available in instructions and manuals of the browser.
    As informações referentes a esta configuração estão disponíveis nas instruções e manuais do navegador.

    The Allied forces resumed their eastward progress and were poised to cross the Rhine
    As forças aliadas retomaram seu progresso para o leste e estavam prontas para atravessar o Reno

    She didn't linger at the back of the pharmacy.
    Ela não se demorou na parte de trás da farmácia.

    "Eu paguei o maior mico na frente de todo mundo!" = "I've made a real fool of myself in front of everyone!"
    "Você está pagando o maior mico fingindo saber dançar." = "You're making such a fool of yourself pretending to know how to dance."

    Take what she says with a pinch of salt.
    Tenha cautela com o que ela diz.

    The robbers got away with five hundred thousand dollars.
    Os ladrões se safaram com quinhentos mil de dólares.

    Don’t think you can get away with causing all this trouble.
    Não pense que você consegue se safar causando todo isse problema.

    Even while behind bars they seem capable of causing maximum mayhem.
    Mesmo atrás das grades, eles parecem capazes de causar o máximo de confusão.

    Earnings on this level fall to a *meagre* three cents a share.
    Os ganhos nesse nível caem para um *mesquinho/escasso* de três centavos por ação.

    Through the solid floor of the abode, the chill of winter seeped in, fettered little by the *meagre* warmth provided by the fire.
    Através do piso sólido da morada, o frio do inverno se infiltrou, encadearam-se pouco pelo *magro* calor fornecido pelo fogo.

    The situation is apparently under control, but for how much longer?
    A situação está aparentemente sob controle, mas até quando?

    These lopsided relations have the opposite effect politically speaking.
    Estas relações "distorcidas|assimétricas" geram o efeito político contrário.

    She never stops talking about dieting. She’s got a real bee in her bonnet about it.
    Ela nunca para de falar sobre dietas. Ela tem uma obsessão nisso.

    He’s got a bee in his bonnet about factory farming.
    Ele é obcecado por pecuária industrial.

    Unlocking a wealth of answers about human beings and their potentials, he did indeed resolve the long-standing riddle of the mind.
    Desbloqueando uma riqueza de respostas sobre os seres humanos e seus potenciais, ele realmente resolveu o antigo enigma da mente.

    Dogs are barred from many public places because they pose a serious hazard to health and can be a nuisance and danger.
    Os cães são barrados em muitos lugares públicos porque representam um sério risco para a saúde e podem ser um incômodo e perigo.

    And let's not forget a nimble mind's required to go along with the dexterity.
    E não vamos esquecer que uma mente ágil é necessária para acompanhar a destreza.

    The sage acts by doing nothing
    O Sábio age não fazendo nada

    As much as I hated to concede defeat, I opened the door and asked, ‘Anyone have any ideas?’
    Por mais que eu odiasse admitir a derrota, abri a porta e perguntei: "Alguém tem alguma ideia?"

    He took after his father.
    Ele puxou seu pai.

    I think she got lost. Why else would she be late?
    Eu acho que ela está atrasada. Por qual outra razão ela estaria atrasada?

    We were late for class because our car broke down.
    Nos atrazamos para a aula ontem porque o carro quebrou.

    You’ve made your bed, now lie in it.
    Ajoelhou, tem que rezar!

    If you’re in a plight, then you’re in a situation where you have a problem, or you’re in some danger, or you’re just in bad circumstances.
    Se você está em um "plight", então você está em uma situação em que tem umproblema.

    The intention is to treble the figure for appropriations currently being spent in the next seven years.
    A intenção é triplicar o valor das dotações a serem aplicadas nos próximos sete anos.

    What are a few things that you, your friends, or family members have a penchant for?
    Quais são algumas coisas pelas quais você, seus amigos ou familiares têm uma queda/inclinação?

    They have argued they were framed by a snitch who wanted a reduced prison term
    Eles argumentaram que foram incrimidados por um delator que queria redução de pena

    I can’t get enough of listening to this song. Play it again!
    Eu não me canso de ouvir essa música. Toca de novo!

    The scapegoat has done its duty, but it is in a wretched state.
    O bode expiatório cumpriu seu papel, mas está em um estado deplorável.

    ┌─────────────────────┬─────┬───────────────────────────┐
    │   colon             │  :  │   dois pontos             │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   semicolon         │  ;  │   ponto e vírgula         │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   period            │  .  │   ponto                   │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   coma              │  ,  │   vírgula                 │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   curly braces      │  {  │   chaves                  │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   brackets          │  [  │   colchetes               │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   quotation marks   │  "  │   aspas                   │
    ├─────────────────────┼─────┼───────────────────────────┤
    │   emdash            │  —  │   hifen longo             │
    └─────────────────────┴─────┴───────────────────────────┴

    Are we prepared to brace ourselves for this grim, albeit realistic, prospect?
    Estaremos dispostos a preparar-nos para essa perspectiva sombria, ambora realista?

    What made the Mogol army so dreadful and eficient?
    O que fez o exército Mogol tão terrível e eficiente?

    A much stronger nation can also turn a weaker one into a vassal state
    Uma nação mais forte pode também tornar uma nação mais fraca em um estado servil

    "What?" he says, spreading his hands in feigned innocence.
    "O quê?" diz ele, estendendo suas mãos em uma igorância figida.

    The guests began arriving in dribs and drabs.
    Os convidados chegaram de pouquinho em pouquinho.

    After that I made some sound that sounded like a grumble , but not quite.
    Depois disso fiz um som que soou como um resmungo, mas não exatamente.

    She looked up at him sadly, acknowledging his gesture with a half wag of her tail.
    Ela olhou para ele com tristeza, reconhecendo seu gesto com uma meia sacudida de seu rabo.

    Chad ran a hand through his dishevelled hair and let out a sigh.
    Chad passou a mão pelo cabelo desgrenhado e soltou um suspiro

    Some of the knit dresses showed off so much thigh that they looked like tops.
    Alguns dos vestidos de malha exibiam tanta coxa que pareciam topos.

    I put the laundry hamper up on the shelf, and got back into bed.
    Coloquei a roupa na prateleira e voltei para a cama. - hamper - dificultar / balaio

    She threw her dirty clothes in the laundry hamper and took a shower.
    Ela jogou suas roupas sujas no cesto de roupa suja e tomou um banho.

    Worse than the comedy, however, are the director's feeble attempts at character building.
    Pior que a comédia, no entanto, são as fracas tentativas do diretor em construir um personagem.

    I was in my sloppy clothes that I wear at home only.
    Eu estava com minhas roupas desleixadas que só uso em casa.

    The environment is gradually being ruined by sloppy and haphazard planning.
    O ambiente está sendo gradualmente arruinado pelo planejamento desleixado e aleatório.

    There is no question, we can topple any government we want to in the region.
    Não há dúvida de que podemos derrubar qualquer governo que quisermos na região.

    A flurry of editorials hostile to the administration
    Uma enxurrada de editoriais hostís à administração

    Indeed, by the end of 1981 inflation was rampant , reaching 14.1 percent.
    De fato, lá pelo final de 1981 a inflação estava desenfreada, alcançando 14,1 porcento.

    Even though I’ve been acting for years, I still get a thrill out of going on stage.
    Apesar de atuar por anos, ainda sinto um frio na barriga por subir no palco

    Barry’s boss said that if the economy doesn’t pick up they will be forced to fire all the employees and shut down the company.
    O chefe de Barry disse que, se a economia não melhorar, eles vão ser forçados a despedir todos os funcionários e fechar a empresa.

    Children pick up on our worries and anxieties, so we have to protect them.
    As crianças percebem nossas preocupações e anxiedades, então temos que protege-los

    He never did anything to thwart his father
    Ele nunca fez nada para furstrar seu pai

    A new level of fake news! That’s all we need!
    Um novo nível de fake news! Era só o que faltava!

    I personally think that watching films and TV shows is best way to pick up new vocabulary
    Pessoalmente, acho que assistir filmes e programas de TV é a melhor maneira de aprender novas palavras

    Whenever I speak to my Australian friend, I find it difficult to make out what he is saying
    Sempre que falo com meu amigo australiano, acho difícil entender o que ele está dizendo

    Unless you stop missing classes like this you will end up falling behind
    A menos que você pare de perder aulas desse jeito, vai acabar ficando para trás

    If you are struggling to keep up with your classmates, maybe you should consider switching groups
    Se você está lutando para acompanhar seus colegas de classe, talvez seja bom considerar mudar de turma

    “Once a month, they shut down the machines for maintenance”, Keith explained to us.
    “Uma vez por mês, eles desligam as máquinas para manutenção”, Keith nos explicou.

    John coasted for a few weeks before applying for a job.
    John ficou de boa por umas semanas antes de procurar emprego.

    The actor coasted to stardom on his father’s name.
    O ator se acomodou na fama de seu pai.

    Rita was coasting at school and I should have told her to work harder.
    A Rita estava se acomodando na escola e eu deveria ter dito a ela para ralar mais.

    All of us were a bit taken aback to learn that John was moving to England next month.
    Todos nós ficamos um pouco surpresos ao saber que John estava se mudando para a Inglaterra no próximo mês.

    I must admit that I was taken aback when I heard we weren't receiving our bonuses this year.
    Devo admitir que fiquei surpreso quando soube que não recebíamos nossos bônus este ano.

    She was completely taken aback by his anger.
    Ela ficou completamente surpresa pela raiva dele.

    Fortunately, they have backtracked and have now indicated that the Commission's proposal can count on their full support.
    Felizmente, recuaram, tendo declarado agora que a proposta da Comissão pode contar com o seu apoio.

    I always catch myself thinking of the worst.>>Eu sempre me pego pensando no pior....De repende percebo que estou pensando no pior.

    Your plan is very good! Too bad it will not work.
    Seu plano é muito bom! Que pena que não vai funcionar.

    The best time to prune is just after flowering has ended.
    A melhor época para a poda é logo após o fim da florada.

    Despite the government's policies, many more people are about to undertake these perilous voyages.
    A dispeito das politicas governamentais, muito mais pessoas estão prestes a empreender essas viagens perigosas.

    I don't want to hear any more of your sass.
    Eu não quero ouvir mais sua insolência.

    Nancy had plenty of spunk, but not much of a sense of humour.
    Nancy tinha muita coragem, mas não muito senso de humor.

    We are a nonprofit that serves homeless and runaway youth ages 12 to 25
    Somos uma organização sem fins lucrativos que atende jovens desabrigados e fugitivos com idades entre 12 e 25 anos

    Come to think of it, I know someone who can help up.
    Pensando bem, eu sei de alguém que pode ajudar a gente.

    I know I shouldn’t eat chocolate, but I can’t help it.
    Eu sei que eu não devia comer chocolate, mas eu nao posso evitar.

    Come to think of it, we better stay here.
    Pensando bem, é melhor a gente ficar aqui.

    Like a mirage in a desert, counterfeit love cannot quench your thirst.
    Como uma miragem no deserto, amor falsificado não pode saciar sua sede.

    I love getting her emails. Come to think of it, I haven’t had one for a while.
    Adoro receber os emails dela. Pensando bem, não tenho recebido nenhum já faz um tempinho.

    You might win first prize. - I should be so lucky!
    Você talvez ganhe o primeiro prêmio. - Quem me dera!

    You want three week holiday? You should be so lucky!
    Você quer 3 semanas de férias? Você está querendo demais!

    I didn't like studying at first, but it has grown on me.
    No princípio eu não gostava de estudar, mas agora estou começando a gostar.

    Scientists now report that 70 percent of the infrastructure in the Arctic is at risk due to thawing permafrost in the region.
    Cientistas agora relatam que 70% da infraestrutura no Ártico está em risco evido ao degelo do permafrost na região

    How often do you go bike riding?
    Com que frequência você sai pra pedalar?

    The city grew on me.
    Eu comecei a gostar da cidade.

    Don't answer the door until Agent Scully arrives.
    Não atenda a porta até a agente Scully chegar.

    They talked about bonuses, housing allowances and other perks.
    Eles conversaram sobre bônus, subsídios de habitação e outras mamatas

    There were rugs, hangings and paintings in profusion, many of them as yet unhung.
    Havia tapetes, tapeçarias e pinturas em profusão, muitos deles ainda não lançados.

    During most strokes, a blood vessel in the brain becomes clogged by a blood clot.
    Durante a maioria dos acidentes vasculares cerebrais, um vaso sanguíneo no cérebro fica entupido por um coágulo sanguíneo.

    I got the editor-tinkering itch recently, so I thought I’d write about the changes I’ve made.
    Eu fiquei com a coceira de ajustar o editor de textos recentemente, então eu pensei que escreveria sobre as mudanças que fiz.

    He would more or less single-handedly usher in the performance-art phase of the New York counterculture.
    Ele iria, mais ou menos sozinho, inaugurar a fase da arte performática da contracultura de Nova York.

    Scientists believe the beetles use this strength to plowarado through the woody debris that covers their tropical jungle habitat.
    Os cientistas acreditam que os besouros usam essa força para atravessar os destroços lenhosos que cobrem seu habitat na selva tropical.

    Let me anwer the door, I'll be right back!
    Deixa eu atender a porta, volto já!

    Actually, HTML is a veritable goldmine of repetition
    Na verdade, o HTML é uma verdadeira mina de repetição

    I still see a lot of people here using their phones normally here, but the fines are fairly stiff if you are caught.
    Eu ainda vejo muitas pessoas aqui usando seus telefones normalmente aqui, mas as multas são bastante rígidas se você for pego.

    It started under a mango three, in a hut made of straw.
    Começou debaixo de um pé de mangueira, numa cabaa de palha.

    She sings like nobody’s business.
    Ela canta muito bem. | Ela canta como ninguém.

    We get along like nobody’s business.
    A gente se dá muito bem. | A gente se entende como ninguém.

    Compared to today's Corolla, the design is just a little more crisp and modern. Toyota is promising better performance, driving feel and fuel economy in this new model.
    Comparado ao Corolla de hoje, o design é apenas um pouco mais nítido e moderno. A Toyota está prometendo melhor desempenho, sensação de direção e economia de combustível neste novo modelo.

    The stadium lights glare down coldly as you inhale a deep breath of cold, crisp air.
    As luzes do estádio brilham friamente enquanto você inspira uma respiração profunda de ar frio e fresco.

    Most of her war information is gleaned from her twice-weekly phone chats with her husband.
    A maior parte de suas informações de guerra é colhida de suas conversas telefônicas duas vezes por semana com o marido.

    We got there just in the nick of time.
    A gente chegou lá na hora H.

    A show that should entice a new audience into the theater
    Um show que deve atrair um novo público para o teatro

    Why don't women try to just grope men more?
    Porquê as mulhres não tentam apalpar mais os homens?

    As luck would have it, it rained the day of the picnic.
    Por azar, choveu no dia do piquenique.

    As luck would have it, there was one seat left.
    Por sorte, havia um assento livre.

    She watched the inauguration of her country’s new president.
    Ela assistiu à posse do novo presidente de seu país.

    The new president obviously enjoyed her inauguration.
    A nova presidente claramente adorou sua posse.

    To prevent a hangover in the first place, drink sensibly, which means don't have more than one drink an hour.
    Para evitar uma ressaca, beba de forma sensata, o que significa que você não beba mais do que uma bebida por hora.

    As a third term, the covenant engages aspects of the body politic as well as the modern contract.
    Como um terceiro termo, o pacto envolve aspectos do corpo político, bem como o contrato moderno.

    Their responsibility to uphold the covenant was both communal and individual.
    Sua responsabilidade de manter o pacto era tanto comunitária quanto individual.

    They also resolved that if he returned they would waylay him and give him a beating.
    Eles também resolveram que, se ele voltasse, eles o atacariam e lhe dariam uma surra.

    Bob’s tickets cost much less than ours. I think we’ve been ripped off.
    Os ingressos do Bob custaram menos do que os nossos. Acho que fomos enganados.

    The agency really ripped us off.
    A agência meteu a faca na gente.

    She called me names, but I took it in stride. She’s my sister and I have to be patient with her.
    Ela me xingou, mas eu encarei numa boa. Ela é minha irmã e eu tenho de ter paciência com ela.

    When you become a politician, you soon learn to take criticism in your stride.
    Quando você entra para a política, você rapidamente aprende a encarar as críticas com naturalidade.

    I think you’d better fill me in on what’s been happening.
    Eu acho bom vocês me colocarem a par do que está acontecendo.

    The new mayor is reaching out to the local community to involve them in his plans for the city.
    O novo prefeito está se aproximando da comunidade para envolvê-los em seus planos para a cidade.

    No matter how old you are, there is still time to mend your ways.
    Não importa a idade, sempre há tempo para criar vergonha na cara.

    I was getting really bad grades, but I promised my parents I would mend my ways and work harder.
    Eu estava tirando notas muito ruins, mas prometi aos meus pais que iria tomar jeito e me dedicar mais.

    She gave birth to twins last night.
    Ela deu à luz gêmeos esta manhã.

    She's just given birth to a baby girl.
    Ela acabou de dar à luz a uma garotinha.

    I'm starting to get the hang of how this computer works.
    Estou começando a pregar o jeito de como esse computador funciona.

    It's a bit tricky at first but you soon get the hang of it.
    É um pouquinho complicado no começo mas a gente logo pega o jeito.

    She kept trying to hold back her tears.
    Ela continuou tentando conter as lágrimas.

    She bit her lip to hold back the tears.
    Ela mordeu o lábio para conter as lágrimas.

    I held my tears back when I heard the bad news.
    Eu contive as lágrimas quando ouvi a má notícia.

    I racked my brains, trying to remember his name.
    Eu quebrei a cabeça tentando me lembrar do nome dele.

    She began to rack her brains to remember what had happened at the nursing home.
    Ela começou a quebrar a cabeça para se lembrar do que tinha acontecido na casa de repouso.

    Allen, supposedly the fifth richest man in the world, is the tycoon who bankrolled both museums.
    Allen, supostamente o quinto homem mais rico do mundo, é o magnata que financiou os dois museus.

    What happens when the government just hands people money?
    O que acontece quando o governo apenas entrega dinheiro às pessoas?

    So we’re still friends, right? No hard feelings?
    Então, ainda somos amigas, né? Sem ressentimentos?

    I wanted to show her there are no hard feelings.
    Eu queria mostrar a ela que não ficou nenhuma mágoa.

    It serves you right. You were not nice to him.
    Bem feito pra você! Você não foi gentil com ele.

    It serves you right. You got what you deserved.
    Acho é pouco! VoC6e teve o que mereceu.

    Did you see that kitten’s sweet little face? I can’t even!
    Você viu a carinha fofa daquele gatinho? Eu não aguento com tanta fofura

    Look at this mess. Ugh, I can’t even.
    Olha só pra essa bagunça. Aff, assim não dá!

    I've always thought that the sweet, mellow taste of traditional cream is a perfect match for warm apple desserts.
    Sempre achei que o sabor doce e suavemaduro do creme tradicional é uma combinação perfeita para sobremesas quentes de maçã.

    My dad has definitely mellowed out as he’s gotten older.
    Meu pai foi finalmente ficando de boa enquanto envelhecia.

    Wow, I can’t believe that she had the nerve to ask for a raise on her first day!
    Caraca, não consigo acreditar que ela teve a cara de pau de pedir um aumento já no primeiro dia dela!

    They’re banking on him to save the match.
    Eles estão confiando nele para salvar a partida.

    Can I bank on your support?
    Posso contar com seu apoio?

    Out of consideration for your past efforts, I will do what you ask.
    Por consideração aos seus esforços no passado, eu vou fazer o que está me pedindo.

    They let me do it out of consideration.
    Ele me deixaram fazer isso por consideração.

    You can’t put the decision off any longer.
    Você não continuar empurrando essa decisão com a barriga.

    He keeps asking me out, and I keep putting him off.
    Ele fica me chamando para sair e eu vou empurrando ele com a barriga.

    The result was a hexagonal shape plate, combined with an interior
    pentagon textures in bas-relief. O resultado foi uma placa em forma hexagonal, combinada com uma textura de pentágono interior em baixo-relevo.

    Feeling Sluggish? How to Express It in English slug = lesma
    Sentindo-se preguiçoso? Como expressar isso em Inglês

    I hadn’t seen her since childhood. Then, completely out of the blue, I received a letter from her.
    Eu não a tinha visto desde a infância. Aí, simplesmente do nada, eu recebi uma carta dela.

    They nodded at every platitude about making sacrifices today for a better tomorrow.
    Eles assentiram em cada platitudebanalidade sobre fazer sacrifícios hoje por um amanhã melhor.

    I’d better add more minutes to my cell phone.
    É bom eu colocar mais créditos no meu celular.

    We did our best to keep Sarah from finding out the party to no avail.
    Fizemos o nosso melhor para impedir que Sarah descobrisse a festa sem sucesso.

    When you’re a politician everyone is always watching in case you slip up.
    Quando se é um político todo mundo está sempre de olho em você caso você cometa um deslize.

    I slipped up by mentioning the surprise birthday party.
    Eu dei uma mancada ao mencionar a festa de aniversário surpresa.

    I’m sorry! I didn’t call you back sooner, it totally slipped my mind.
    Sinto muito! Eu não retornei a ligação mais cedo, eu me esqueci completamente.

    He said he would call you, but I guess it slipped his mind.
    Ele disse que iria te ligar, mas acredito que se esqueceu.

    We need a cut-and-dried decision by the end of the week.
    Nós precisamos de uma decisão definitiva até o fim da semana.

    I think it’s cut and dried already that they are going to close the place.
    Eu creio que já está certo/definido/decidido que eles vão fechar o local.

    If the noise level rises above that point, hearing protection, earplugs or a similar device, would be required
    Se o nível de ruído subir acima desse ponto, proteção auditiva, tampões auditivos ou um dispositivo similar, seriam necessários

    China shares surge on US trade deal optimism
    Ações da China aumentam no otimismo do acordo comercial dos EUA

    I suppose that, at a pinch, we could use my car.
    Acredito que, no caso de extrema necessidade, nós podemos usar meu carro.

    If you’re in a pinch, I’m sure they’d look after Jenny for a while.
    Se você estiver no desespero, tenho certeza que elas podem cuidar da Jenny por um tempinho.

    I knew you and Shirley would hit it off. You two have so much in common.
    Eu sabia que você a Shirley se dariam super bem. Vocês duas tem muito em comum.

    You can take charge of the project, just keep me in the loop.
    Você pode ficar responsável pela projeto, mas mantenha me por dentro.

    I’ve just eaten a huge slice of cake. So much for my diet!
    Eu acabei de comer um pedaço enorme de bolo. Minha dieta já era!

    The car won’t start. So much for our day trip to the beach!
    O carro não quer pegar. Nosso bate e volta até a praia já era!

    So much for polite introductions. It’s now time to get down to business.
    Chega de apresentações formais! Agora é hora de colocar a mão na massa.

    I didn’t really hit it off with his sister.
    Eu não me dei muito bem com sua irmã.

    He's not as evil as I thought he'd be - he's kind of a bonehead , but he's not an evil guy.
    Ele não é tão mal quanto eu pensei que ele seria - ele é meio idiota, mas ele não é um cara mau.

    Sure, the men behind the robbery looked pretty clever in the immediate aftermath of the heist.
    Claro, os homens por trás do roubo pareciam bem espertos logo após o assalto.

    The gun heist in France yesterday - which we condemn in the strongest possible terms
    O roubo de armas ocorrido ontem em França - que condenamos veementemente

    Bailing on plans with friends once in a while is unavoidable and perfectly understandable.
    Discutir planos com amigos de vez em quando é inevitável e perfeitamete compreensível.

    Julian Gutmanis had been called plenty of times before to help companies deal with the fallout from cyberattacks.
    Julian Gutmanis já havia sido chamado muitas vezes antes para ajudar as empresas a lidar com as consequências dos ataques cibernéticos.

    That one does not have a hangover the morning after drinking alcohol.
    Aquele não terá ressaca de manhã após beber alcool

    Well, you've succeeded in scaring me enough to listen to your last weaselly option.
    Conseguiste assustar-me para ouvir a tua última opção traiçoeira.

    The growth was funded at subpar levels, barely juicy enough to tempt the cash-starved universities.
    O crescimento foi financiado em níveis abaixo do normal, pouco lucrativos o suficiente para seduzir as universidades famintas de dinheiro.

    I can barely see the bear.
    Mal posso ver o urso.

    I can hardly see the bear.
    Mal posso ver o urso.

    I need to cut down on eating fried foods.
    Preciso comer menos comida frita.

    I can barely understand Japanese, let alone write in Japanese.
    Eu mal consigo entender Japonês, imagina escrever em Japonês.

    He doesn't know how to fix a leaky faucet, let alone renovate an entire bathroom
    Ele não sabe consertar uma torneira vazando, quanto mais reformar um banheiro inteiro

    Michele is hands down the most intelligent person I’ve met.
    Michele é sem dúvida a pessoa mais inteligente que já conheci.

    There’s nothing you can do but grin and bear it.
    Não há nada que você possa fazer a não ser aceitar sem reclamar.

    We had a barbecue yesterday.
    Fizemos um churrasco ontem.

    He said we could tag along if we were quiet.
    Ele disse que poderíamos ir junto, se ficássemos quietos.

    “Think about that for a moment. That is a staggering number.”
    Pense um momento sobre. Isso é um número impressionante.

    Study reports ‘staggering’ death rate in US among those infected who show
    symptoms.
    Estudo mostra taxas de mortalidade assustadoras nos EUA entre aqueles que
    apresentam sintomas.

    The oil price slump in 1985 created severe adjustment problems
    A queda dos preços do petróleo em 1995 criou severos problemas de ajuste

    We may have to go through many ‘lives’ before we achieve heavenly bliss.
    Podemos ter que passar por muitas "vidas" antes de alcançarmos a felicidade celestial.

    Someone was trying to smear her by faking letters
    Alguém estava tentando difamá-la com cartas falsas smear = nódoa

    My name was used to submit a fake comment to the FCC in favor of repealing net neutrality.
    Meu nome foi usado para enviar um comentário falso ao FCC em favor da revogação da neutralidade da rede.

    I wish here and now to express my deepest disgust.
    Eu gostaria aqui e agora de expressar meu profundo repúdio disgust - repugnância

    Missing the bus was a blessing in disguise. It caused me to meet my future wife.
    Perder o ônibus acabou sendo uma mau que veio para o bem. Me fez econtrar minha futura esposa.

    I think he’s still coming to terms with the death of his wife.
    Acho que ele ainda está aprendendo a lidar com a morte da esposa.

    The shimmer returned, and seconds later the man was gone.
    O brilho retornou, e segundos após o homem havia sumido.

    I assume you are going to take over the company, right?
    Eu presumo que você vai assumir a companhia, certo?

    This was done by shaving off part of one of the sides, and then shaving off some of the thinnest edge to make a flat plank .
    Isso foi feito raspando parte de um dos lados e, em seguida, raspando algumas das extremidades mais finas para fazer uma prancha plana.

    Executives led a new thrust in business development
    Executivos lideraram um novo impulso no desenvolvimento de negócios

    Management should ascertain whether adequate funding can be provided
    A administração deve verificar se o financiamento adequado pode ser

    Once there is more than one overt act, there is a range of possibilities for sentencing.
    Uma vez que haja mais de um ato evidente, existe uma gama de possibilidades de condenação.

    She honed her language skills by reading and writing every day.
    Ela aperfeiçoou as suas habilidades com o idioma lendo e escrevendo todos os dias.

    The crevasses make walking over the glacier hazardous.
    As fendas fazem com que caminhar sobre o glaciar seja perigoso.

    If plants start to wilt slightly water them right away.
    Se as plantas começarem a murchar rege-as imediatamente.

    We don't move on from grief. We move forward with it.
    Nós não saimos da tristeza(luto). Nós seguimos em frente com ela.

    I've written some very uplifting books.
    Escreví alguns livros muito inspiradores/edificantes.

    An uplifting story of triumph over adversity
    Uma história de triunfo sobre adversidade inspiradora

    "A mid-career switch can be challenging, especially if you've become pigeonholed in your current role."
    "Uma troca de meio de carreira pode ser desafiadora principalmente se você ficou bitolado no seu emprego atual"

    The Democrats are up in arms about what the attorney-general said.
    Os democratas estão revoltados com o que o procurador-geral disse.

    President Obama on Monday flew up to New York from Washington, rode in his motorcade to ABC's studios and sat down for an interview on "The View".
    Na segunda-feira, o presidente Obama viajou de Nova York para Nova York, acompanhou sua comitiva aos estúdios da ABC e sentou-se para uma entrevista em "The View".
    MOTORCADE: A procession of cars carrying VIPs, especially political figures.

    He flaunts his riches like everyone in the business.
    Ele ostenta suas riquezas como todos no negócio.

    Unbelievable you have some nerve showing up here just to schmooze with the tenure committee
    Inacreditável você tem coragem aparecendo aqui apenas para bajular o comitê de posse
    SCHMOOZE: bater papo, bajular

    During his tenure , the university experienced its most expansive period of growth.
    Durante seu mandato, a universidade experimentou seu período mais expansivo de crescimento.
    TENURE: posse, mandato, permanência.

    I didn’t see the car coming, that was a close call.
    Eu não vi o carro vindo, essa foi por pouco.

    People completed the course dressed in all kinds of weird and wacky outfits this year.
    As pessoas completaram o curso vestidas com todos os tipos de roupas estranhas e malucas/excentricas este ano.

    I'm a pushover when it comes to buying new kitchen gadgets.
    Eu sou facilmente persuadido quando se trata de comprar novos aparelhos de cozinha.

    I would be remiss if I did not do something about it.
    Eu seria negligente se eu não fizesse algo a respeito.

    Look, I know you guys don’t want to do this, but we have no choice.
    So, you can either "bitch and whine(reclamar)" or we can just "get it over with(acabar com isso)".

    I don’t agree with the boss either, but what he says goes.
    (Eu não concordo com a chefe, mas o que ela manda a gente faz.)

    His brother is a real dork!
    O irmão dele é muito panaca!

    I cringe at the sight of my dad dancing.
    Eu morro de vergonha vendo meu pai dançar.

    Well, it doesn’t hurt to ask, right?
    Bom, não custa nada perguntar, né?

    Even an otter picks up a rock when he wants a clam.
    Mesmo uma lontra pega uma pedra ela quer um molusco.

    The sleep group, they're going to get a full eight hours of slumber (sono)
    O grupo de "dormidores", eles vão receber uma noite completa de sono.

    *make heads or tails (out) of (someone or something)*
    To understand someone or something. This phrase is usually used in the negative to convey the opposite. After spending hours working with the new piece of software, I still could not make heads or tails out of it.

    I’m getting used to retirement, and loving it.
    Estou me acostumando com a aposentadoria, e amando.

    Remember for each kill you get, you receive slayer experience.
    Lembre-se, para cada morte você ganha experiência de matador

    You might drench yourself in bug spray to repel the mosquitoes that plague you when you go camping.

    This brave deed brings him many hardships: injuries, winter storms, and the loss of his companions.
    Este ato corajoso lhe traz muitas dificuldades: ferimentos, tempestades de inverno e a perda de seus companheiros.

    A midget submarine was then sent down, filming the bow, bridge and foredeck of the ship.
    Um submarino anão foi então enviado, filmando o arco, a ponte e a proa do navio.

    The momentum of all these events will be majorly enhanced as time progresses.
    A dinâmica de todos esses eventos será grandemente aumentada com o passar do tempo.

    In a passion of despair I struck with my fists at the water in the bottom of the boat, and kicked savagely at the gunwale.
    Em uma paixão de desespero, golpeei com meus punhos a água no fundo do barco e chutei selvagemente na amurada.

    He is mixed up in a $10 million insurance swindle
    Ele está envolvido em uma fraude de seguros de 10 milhões

    Don't mix me up with my brother
    Não me confunda com meu irmão

    I am honored to be with you today at your commencement from one of the finest universities in the world.
    É uma honra estar com vocês hoje na cerimônia de formatura de uma das melhores universidades do mundo.

    I was flicking through the channels on the TV
    Eu estava passeando pelos canais na TV

    It may sound like a dungeons and dragons type fantasy game.
    Pode parecer um jogo de fantasia tipo masmorras e dragões.

    We saw our friends off at the airport yestarday.
    Despedimo-nos de nossos amigos no aeroporto ontem.

    I will see you off at the station tomorrow.
    Eu vou dar tchau pra você na estação amanhã.

    Even at this late stage, it is possible that his candidacy will falter.
    Mesmo neste estágio tão avançado, é possível que sua candidatura vacile.

    Simplification seems to be the buzzword.
    Simplificar parece ser a palavra da moda.

    The bust is life size, unlike the peculiar concrete behemoth.
    O busto é em tamanho natural, diferente do gigante concreto peculiar.

    The Gates Foundation, which is the behemoth funder
    A fundação Gates, que o financiador gigante

    I don’t use any wig, that wig is hers.
    Eu não uso nenhuma peruca, aquela peruca é dela.

    Instead, we are bogged down in this interminable roundabout of discussion on procedure.
    Ao invés, vemo-nos enredados neste debate viciado sobre procedimentos. roundabout - rotatoria

    Both the structure and the facade are made from prefabricated concrete panels.
    Tanto a estrutura como a fachada foram feitas com painéis pré-fabricados de concreto.

    He was true horse that loved to frolic in the fields with or without a rider.
    Ele era um verdadeiro cavalo que adorava brincar nos campos com ou sem um cavaleiro.

    During the Vietnam War, the ace of spades was considered the card of death by the Viet Cong.
    Durante a guerra do Vietnan, o às de espadas era considerado a carta da morte pelos Viet Congs.

    The target of this foiled attack was police officers
    O alvo desse ataque frustrado eram os oficiais de polícia

    Personally, I'm hugely pessimistic about this, but I'm loath to spoil the mood.
    Pessoalmente, sou extremamente pessimista sobre isso, mas estou relutante em estragar o clima.

    My wife spoils me by bringing fruit to me every afternoon up here in my office.
    Minha esposa me deixa mal acostumado ao trazer frutas pra mim toda tarde aqui no meu escritório

    However I can guarantee there are some absolute rippers this year, but I don't want to spoil the proper announcements.

    My appreciation to your help yesterday was ace.
    Muito agradecido pela sua ajuda ontem foi um máximo.

    He had been wounded in the abdomen and in the crotch.
    Ele havia sido ferido no abdômen e na virilha.

    We are jigsaw pieces aligned on the perimeter edge
    Somos peças de quebra-cabeças alinhadas na borda do perímetro

    Both of them went to Seoul as stowaways on a train.
    Ambos foram para Seul como clandestinos em um trem.

    Since he cannot shovel snow because of his heart condition she ‘struggles to do this’.
    Uma vez que ele não pode escavar por cause de sua condição cardíaca ela 'luta para fazer isso'

    I turned quickly and glared at the offender, who was a hobo (no surprise there).
    Eu me virei rapidamente e olhei para o ofensor, que era um vagabundo (não é nenhuma surpresa).

    “I would rather live like a hobo here. I don’t see a future in South Korea.”
    "Eu prefereria viver como um vagabundo aqui. Não vejo futuro na Coreia do Sul"

    The electrical outlet isn’t working. We need to fix it.
    A tomada não está funcionando. Temos que conserta-la.

    Possibly he seems a little too meek, and should stand up for himself a bit more.
    Possivelmente ele parece um pouco manso demais, e deve se levantar um pouco mais.

    Well I was hoping you could run a plate for us. We are in a really big hurry!
    Bem Eu queria saber se você poderia verificar uma placa pra nós. Estamos realmente apressados!

    I've got three items in the docket
    Eu tenho três itens na pauta/boletim

    As well as giving an impressive blend of performance and economy, the new engine is also quiet .
    Além de oferecer uma mistura impressionante de desempenho e economia, o novo mecanismo também é silencioso.

    I have trouble waking up in the morning
    Tenho dificuldade de acordar de manhã

    I just want a hook up, not a relationship.
    Eu só quero uma ficante, não um relacionamento.

    How to turn a hook up into a relationship, as told by 10 people who have done it.
    Como fazer uma ficada virar um relacionamento sério, de acordo com 10 pessoas que conseguiram.

    I hope we'll be able to hook up for coffee or something while you're home for Christmas break.
    Espero que consigamos nos reunir para um café ou algo enquanto você está em casa para o Natal.

    At the party, Gloria made out with my brother.
    Na festa, Gloria ficou com meu irmão.

    Will be the attack on Huawei end up seeing as US blunder?
    O ataque a Huawai vai acabar sendo visto como um erro dos EUA?

    The adjective frisky means playful or lively.
    O adjetivo frisky significa brincalhão ou animado.

    “As far as I know, they have not set a date since my visit.”
    “Até onde eu sei, eles não marcaram uma data desde a minha visita.”

    You won’t believe who I ran into at the park today!
    Vocês não vão acreditar quem eu encontrei na praça hoje!

    He didn’t appear in a game in his last stint and his next game will mark his major league debut.
    Ele não apareceu em um jogo em sua última passagem e seu próximo jogo marcará sua estréia na liga principal

    If you’re fussy, you’re really picky about your needs and requirements.
    Se voçê é fussy, você é realmente exigente(chato) com relação às suas necessidades e requisitos

    Unfortunately the plan backfired.
    Infelizmente o tiro saiu pela culatra.

    The Community will have to continue its efforts to contribute to the eradication of this blight.
    A Comunidade deverá prolongar o seu esforço para contribuir para a erradicação deste flagelo.

    This makes our potatoes and winter cereal crops susceptible to weeds and disease such as blight.
    Isso torna nossas batatas e plantações de sereal de inverno suceptíveis a pragas e doenças como a ferrugem

    He was obviously upset, and muttered something about girlfriend trouble
    Ele estava obviamente chateado e resmungou alguma coisa sobre problemas com a namorada

    I have a weakness for candy bars, so Halloween is my favorite holiday.
    Eu tenho um fraco por doces, então Halloween é minha data festícia favorita.

    A glimmer is a tiny glint of light or the sliver of an idea. Either way, it's a sign of a lot more going on behind the scenes.
    Um vislumbre é um minúsculo reflexo de luz ou lasca de uma idéia, é um sinal de que muito mais esta acontecendo por traz das cenas.

    Finally, we will show you what to expect the day of your interview and during your oath ceremony
    Finalmente, vamos lhe mostrar o que esperar do dia de sua entrevista e cerimônia de juramento.

    As we watch his interview footage we will learn new English expressions
    Á medida que assistimos as cenas de sua entrevista nós iremos aprender novas expressões do Inglês.

    I'm usually ill at ease when addressing a large crowd of people.
    Eu geralmente me sinto desconfortável quando falo com uma grande multidão de pessoas.

    My forme boss was on the run from the law after the discovery he had embezzled 10 milion dollars from the company.
    Meu chefe estava fugindo da lei após a descoberta de que ele havia desviado 10 milhões de dólares da empresa.

    It's your space so you can decide what sort of lodger you want - young student, mature student, employee, male, female.
    É o seu espaço para que você possa decidir que tipo de locatário você quer - jovem estudante, estudante maduro, empregado, masculino, feminino.

    A brush should be used to scrub the instruments free of biological matter.
    Deve-se usar uma escova para esfregar os instrumentos e remover a matéria orgânica.

    When things get tough, don’t toss everything to the wind
    Quando as coisas ficam difíceis, não chute o pau da barraca.

    I'm 80% annoying and 20% boring
    Eu sou 80% irritante e 20% chato

    Come over tomorrow and we can catch up.
    Passa aqui amanhã e nós colocamos o papo em dia.

    Visiting Brazil is on our bucket list.
    Visitar o Brasil está na nossa lista de desejos.

    He very much resembles a friend of mine.
    Ele lembra muito um amigo meu.

    I know that is impolite, but I would ask you to understand and bear with me.
    Sei que é indelicado, mas agradeço a vossa compreensão e paciência.

    Some people in the crowd were sledding and snowboarding down the street.
    Algumas pessoas na multidão estavam andando de trenó e praticando snowboarding pela rua.

    These short pauses will pep you up in no time.
    Estas pequenas pausas irão revitalizá-lo num instante.

    Anyone who tries to grow potatoes in a home garden worries about leafhoppers.
    Qualquer um que tente cultivar batatas em uma horta preocupa-se com as cigarrinhas.

    Please, watch your mouth around here young lady
    Por favor, olha como fala por aqui moça

    I’m going to get some tomatoes at the store.
    Eu vou comprar alguns tomates no comércio.

    As you can hear, the remodeling is still going on.
    Como você pode ouvir, a reforma continua.

    I ran out of sugar.
    Fiquei sem açúcar.

    I rode my motorcycle to class today and the helmet messed up my hair.
    Eu fui de moto pra aula hoje e o capacete assanhou meu cabelo.

    Ashes to ashes, dust to dust, let me sink into oblivion.
    Cinzas às cinzas, pó ao pó, deixe-me afundar no esquecimento.

    Lots of people write to ask about detecting the subtle signs of a potentially dicey relationship.
    Muitas pessoas escrevem para perguntar sobre como detectar os sinais sutis de um relacionamento potencialmente arriscado.

    Something dicey is unpredictable - and it may even be risky or downright dangerous.
    Algo que é "dicey" é imprevisível - e pode até mesmo ser ariscado ou francamente perigoso.

    It forms a barrier, to protect baby form chafing.
    Forma uma barreira, protegendo o bebê das incômodas assaduras.

    It isn't coated, thus accordingly few scratches or chafing should be expected.
    Não é revestido, portanto devem-se esperar alguns arranhões ou escoriações.

    How do we retrace Peter steps?
    Como refazemos os passos do Peter?

    Q:When listening to an audio replay, the audio is all messed up with pops and glitches?
    Q:Ao ouvir a repetição de um audio, o audio é bagunçado com estalos e falhas?

    Let your arms swing as you walk
    Deixe seus braços balançarem à medida que anda

    Wait a few minutes to let the moisturizer sink in.
    Espere alguns minutos para que o hidratante penetre.

    It took a while for the reality of my situation to sink in.
    Demorou um tempo para cair a ficha sobre a realidade da minha situação.

    Are you brothers? I can tell!
    Vocẽs são irmãos? Dá pra perceber!

    I can tell you’re a little stressed out.
    Dá para notar que você está estressado.

    In the 1990s Python was still very much a programming language underdog.
    Na década de 1990, o Python ainda era uma linguagem de programação discriminada.

    He can always tell when someone is lying.
    Ele sempre percebe quando alguém está mentindo.

    Girl ignores signs, Honda gets towed.
    Garota ignora placas, Honda é rebocado.

    We were strolling in the beach
    Nós estávamos caminhando na práia

    The couple decided to move up the wedding date to avoid the rainy season
    O casal decidiu adiantar a data do casamento para evitar o período de chuvas

    Various technology companies have developed contraptions that can fly
    Várias companhias de tecnologia desenvolveram geringonças que podem voar

    Research is yet to fully unravel how such stimuli can be effectively deployed
    A pesquisa ainda está para desvendar completamente como tais estímulos podem ser efetivamente implantados

    You should use this option with care, and don't use it hastily as an alias.
    Você deve usar essa opção com cuidado, e não use-a precipitadamente com um alias.

    I'v heard you have a girlfriend, what is she like?
    Eu ouvi dizer que você tem uma namorada, como ela se parece?

    The U.S. housing market is out of whack
    O mercado imobiliário nos Estados Unidos está desorganizado

    After years of leniency, the school is finally cracking down on plagiarism.
    Depois de anos de clemência, a escola está finalmente reprimindo o plágio.

    You can beat the odds if you realize you aren't alone.
    Você pode superar as dificuldades se perceber que não está sozinho.

    There was hardly anyone there.
    Não tinha quase ninguém lá.

    She could create a system whereby she sets aside an hour a day for sales calls.
    Ela poderia criar um sistema no qual ela reserva uma hora por dia para telemarketing.

    Besides the park and daily walks, there are plenty of other ways to get your pooch to burn off some energy with his peers. pooch = vira-latas

    The besieged city
    A cidade sitiada

    Her hands trembled, a fine quiver that rippled through her body.
    Suas mãos tremiam, um belo tremor que percorreu seu corpo.

    Outcry after reports Brazil plans to investigate Glenn Greenwald
    Clamor após relatos que que o Brasil planeja investigar Glenn Greenwald

    The dress is cut low enough to reveal a lot of cleavage.
    O vestido tem um corte que acaba revelando bastante o decote.

    Clare was wearing a low-cut dress that showed off her cleavage.
    A Clare usava um vestido decotado que mostrava seu colo.

    I sat back down and resumed my editing, sticking the grape lollipop back in my mouth.
    Sentei-me novamente e retomei minha edição, colocando o pirulito de uva de volta na minha boca.

    You’re the one who made all the mess.
    Foi você que causou toda a confusão.

    He's not an alcoholic, but a borderline case
    Ele não alcólico, mas estão no limite.

    She was dressed in a really long light blue bathrobe.
    Ela estava vestida com um roupão muito longo azul claro.

    He was busted by US inspectors at the border.
    Ele foi preso por inspetores Norte Americanos na fronteira.

    He started singing a lullaby , and I stood in the doorway, watching them.
    Ele começou a cantar uma canção de ninar e eu fiquei na porta, observando-os.

    I can't tell what he wants. He keeps blowing hot and cold.
    Eu nao consigo entender o que ele quer. Tem horas que ele quer uma coisa e outras ele quer outra.

    If it were up to me, you wouldn’t have this.
    Se dependesse de mim, você não teria isso.

    If it were up to me, I would never let them do this again.
    Se dependesse de mim, eu nunca deixaria elas fazerem isso.

    Find out how your customers expect you to meet their needs.
    Descubra como os seus clientes esperam que você atenda às necessidades deles.

    You strive for perfection and worry when things don't turn out just so.
    Você luta pela perfeição e se preocupa quando as coisas não saem exatamente assim.

    We must strive to secure steady growth
    Devemos nos esforçar para um crescimento constante

    I've noticed that Tommy's getting in the bad habit of chewing with is mouth open. Let's nip that in the bud.
    Notei que Tommy está com o mau hábito de mastigar com a boca aberta. Vamos cortar isso pela raiz.

    Smiling in blissful content, she tried to remember the last time she had relaxed this much.
    Sorrindo feliz, ela tentou se lembrar da última vez que relaxara tanto.

    The storm's winds were strong enough to uproot trees and to knock people off their feet.
    Os ventos da tempestade eram fortes o suficiente para arrancar árvores e derrubar as pessoas.

    Simmer the sauce gently until thickened
    Ferva o molho suavemente até engrossar

    How about drinking something?
    Vamo tomar uma?

    How about going to the bank?
    Vamos lá no banco comigo?

    How about dancing with me?
    Vamos dançar?

    Little did I know it was Angelina Jolie’s apartment.
    Mal sabia eu que era o apartamento da Angelina Jolie.

    He gave her a month's respite, to think over and accede.
    Ele a deu um mês de descanso, para pensar e aderir.

    he was trying to spool his tapes back into the cassettes with a pencil eraser
    Ele estava tentando enrolar suas fitas de volta nos cassetes com uma borracha de lápis

    I've seen a reel smoke from the friction of the spinning spool.
    Eu ví um carretel de fumaça do atrito do carretel girando.

    I didn’t sleep a wink last night with all that noise.
    Eu não preguei o olho ontem à noite com todo aquele barulho.

    I guess you know I have a bone to pick with you, right?
    Eu acho que você sabe que eu preciso ter uma conversa séria com você, não é?

    This is simply intolerable, and I fully support the notion that this dictator should be ousted.
    É simplesmente intolerável e dou o meu apoio total à ideia de que este ditador deveria ser deposto.

    That’s right, Amazon, the behemoth of the online shopping world
    É isso mesmo, a Amazon, a gigante do mundo das compras online

    A seemingly impregnable and utterly ruthless regime fell to pieces in a few hours.
    Um regime aparentemente inexpugnável e totalmente implacável desmoronou em poucas horas.

    wasn’t until about 2008 that the notion of a plugin manager really came into vogue.
    Não foi até por voltaa do ano 2008 que a noção de gerenciador de plugins realmente entrou em voga.

    I can never understand the rationale behind some of the things that women do these days.
    Eu nunca consigo entender a lógica por trás de algumas das coisas que as mulheres fazem nos dias de hoje.

    That's the rationale for taking the tour, you got a guide
    Essa é a justificativa para você fazer o passeio, você tem um guia

    I was beside myself when I found out I’d been rejected from my first-choice school.
    Eu fiquei abalada quando soube que não tinha sido aceita para a primeira escola que escolhi.

    He was paid a piddling amount of money
    Ele foi pago com uma quantidade irisória de dinheiro

    China's power is haunting us
    O poder da China está nos assombrando

    Cheapskate is a stingey(mesquinho) person. Somebody who buys cheap in favour of higher quality or better stuff.

    The only sound that filled the room was a high-pitched throb.
    O único som que preencheu o quarto foi a palpitação aguda.

    I don’t sleep around anymore, not since I met Mary.
    Eu não sou mais promíscuo, não desde que casei com Maria.

    Has it sunk in that you’re going to be a father again?
    Já caiu a fich de que você será pai novamente?

    What I found off-putting was the amount of work that you were expected to do.
    O que eu achei desanimador foi a quantidade de trabalho que você tinha que fazer.

    I will have to go out of my way to give you a ride home.
    Vou fazer o possível para te dar uma carona pra casa.

    In the course of that bitter conflict, Lincoln had been reviled and attacked without mercy.
    Durante aquele amargo conflito, Lincon foi insultado e atacado sem misericórdia.

    I let out a deep gush of air.
    Soltei um profundo jorro de ar.

    But it will be harder for the ordinary punter to call everyday businesses to account.
    Mas será mais difícil para o apostador comum chamar as empresas comuns para prestar contas.

    I need a – whatchamacallit – you know… that thing that you scrape paint off with?
    Eu preiso - como é que eu digo - sabe... aquele treco, aquilo que você usa pra raspar a tinta?

    Knock out the orange pegs with some clever aiming and a bit of luck.
    Derrube os pinos laranja com pontaria e um pouco de sorte.

    He’s just a stooge for the oil industry.
    Ele é apenas um laranja da indústria petrolífera.

    The fraud depended on hundreds of bank accounts being opened on behalf of straw men.
    A fraude dependia de centenas de contas bancárias sendo abertas em nome de laranjas.

    I don’t think we need to add that chart to the report, considering all of the others we have. – Yeah, you’ve got a point there.
    Acho que não precisamos adicionar esse gráfico ao relatório, considerando todos os outros que temos. - Sim, você tem razão.

    Well, by the way he’d been living, a heart attack was a long time coming.
    Bom, pelo jeito como ele andava vivendo, um ataque cardíaco ia acontecer a qualquer hora.

    It’s been a long time coming, but it was worth the wait.
    Demorou, mas a espera valeu a pena.

    When looking you for an after-school job, you might have to make a trade-off: a lower hourly wage for a more convenient location
    Ao procurar um emprego depois da escola, talvez seja necessário fazer uma troca: um salário por hora mais baixo para um local mais conveniente

    I was on the swing and then all of a sudden my hair got gangled in the chain
    Eu estava no balanço e de repente meu cabelo se enroscou na corrente

    The teacher recommended downloading a dictionary app to look up new words
    O professor recomendou o download de um aplicativo de dicionário para pesquisar novas palavras

    She didn't linger at the back of the pharmacy.
    Ela não se demorou no fundo da farmácia.

    Such a lopsided result defies the laws of probability and suggests something sinister at work.
    Um resultado tão desigual desafia as leis da probabilidade e sugere algo sinistro no trabalho.

    I’m afraid you missed the point. Let me explain again.
    Receio que você não tenha me entendido direito. Vou explicar de novo.

    This unidentified being presents the status of a true spirit and has been received into our fellowship.
    Esse ser, não identificado, apresenta o status de um verdadeiro espírito e foi recebido na nossa fraternidade".

    As a safety precaution, if you have a seatbelt, strap yourself in.
    Como medida de segurança, se você tiver cinto de segurança, prenda-se.

    There is still one obstacle, the most perilous: the crossing of the Channel to arrive in England.
    Resta-lhes ainda um obstáculo, o mais perigoso: atravessar a Mancha para desembarcar em Inglaterra.

    We should treat this information with caution and fairness.
    Devíamos, não obstante, tratar esta informação com prudência e equidade.

    Science has the magic wand of being eternally new.
    A Ciência tem o condão mágico de ser eternamente nova.

    She gets pretty wound up before a game.
    Ela fica bastante nervosa antes de um jogo.

    So shawty understand it
    então, gata, entenda isso

    On the spiritual plane it is revealed in the experience of eternal bliss
    No plano espiritual é revelada a experiência da eterna felicidade

    Children in Africa are worse-off than children in the USA.
    As crianças na África estão piores que as crianças nos EUA.

    Religious shrines were important parts of the household structure.
    Santuários religiosos eram parte importante da estrutura familiar.

    In relation to the tendency presented in previous years, 2011 was outstanding.
    Em relação à tendência apresentada nos anos anteriores, 2011 foi um ponto fora da curva.

    She had a droll voice and a rather wicked sense of humor.
    Ela tinha uma voz divertida e um senso de humor bastante perverso.

    He also aptly captures the United States’ conflicted feelings about immigrants, a mix of resentment and need.
    Ele também captura apropriadamente os sentimentos conflitantes dos Estados Unidos sobre imigrantes, uma mistura de ressentimento e necessidade.

    But yeah, that's right, I seldom catch myself rambling on and on and on.
    Mas então, tá certo, eu raramente me pego resmungando por parágrafos e mais parágrafos.

    Our ideas ramble through centuries of history
    Nossas idéias vagueiam através de séculos de história

    The company's policies underwent a major overhaul.
    As políticas da empresa passaram por uma ampla revisão.

    We binge-watched the entire last season of Monk last weekend
    Nós assistimos a última temporada de Monk de uma vez só

    My dad’s such a cheapskate that he cuts his hair himself.
    Meu pai é tão mão de vaca que ele corta o próprio cabelo.

    There’s no point in asking Joe to pay for it – he’s a real tightwad.
    Não faz sentido pedir ao Joe para pagar por isso – ele é um verdadeiro mão de vaca.

    She has good manners, yet tends to be a bit of a tattletale at times.
    Ela tem boas maneiras, mas tende a ser um pouco fofoqueira às vezes.

    The blurb on the back of the book says that "it will touch your heart."
    A sinopse na parte de trás do livro diz que "tocará seu coração".

    Information pertaining to this configuration is available in instructions and manuals of the browser.
    As informações referentes a esta configuração estão disponíveis nas instruções e manuais do navegador.

    Education will bear the brunt of the cuts
    A educação irá suportar o impacto dos cortes

    When I retired in 1991, my retirement pension was a pittance.
    Quando me aposentei em 1991, minha pensão era uma ninharia.

    Remember how I pet-sat for a woman in Beverly Hills? It turns out her daughter works for Halle Berry
    Lembra de eu cuidei dos animais de uma mulher em Beverly Hills? Acontece que sua filha trabalha para Halle Berry,

    The pages were probably scanned in crooked, or skewed.
    As páginas provavelmente foram alimentadas inclinadas, ou distorcidas.

    Get Phone X, Dad. Don’t settle for the cheaper one.
    Pege o fone X pai, não aceite o mais barato.

    I think that was the crappiest movie I've ever seen.
    Eu acho que foi o pior filme que eu já ví.

    Such missions and envoys operate in many areas of the world as well as in the post-Soviet space.
    Tais missões e enviados operam em muitas áreas do mundo, bem como no espaço pós-soviético.

    Students are fed up with school, so they are taking a week off.
    Os estudantes estão de saco cheio com a escola, então eles vão tirar uma semana de folga.

    If he had known, he would have never dropped out of high school.
    Se ele soubesse nunca teria abandonado o Ensino Médio.

    The teacher asked her to hand out the worksheets.
    O professor lhe pediu para entregar as folhas de exercícios.

    the society is determined to delve deeper into the matter
    A sociedade está determinada a se aprofundar no assunto

    This is a picture of our children, nieces and nephews throwing a 1920s party.
    Esta é uma foto de nossos filhos, sobrinhas e sobrinhos dando uma festa dos anos 20.

    During the sandstorm, they hunkered down in a small hut.
    Durante a tempestade de areia, eles se abrigaram em uma pequena cabana.

    Rather than moving toward compromise, both sides continue to hunker down.
    Ao invés de chegarem a um consenso, os dois lados continuam não cedendo.

