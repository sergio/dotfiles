---
file: gpsd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown

# gpsd
+ http://www.linux-magazine.com/Issues/2018/210/Tutorial-gpsd

    Interface and daemon for GPS receivers

## for a voidlinux distro enables gpsd service

    sudo ln -s /etc/sv/gpsd /var/service

