---
File: ~/.dotfiles/wiki/politica.md
Last Change: Tue, 17 Sep 2024 - 08:00:46
tags: [politica, brasil, madalena]
---

# Robo no facebook

+ [como criar um robo no facebook](https://youtu.be/GPKlXUdzqio?si=HmYWOvSGOfJ2tGGE)
+ [chatfuel](https://chatfuel.com/)
+ [chave pra ativar no instagram](https://www.instagram.com/reel/C24sEkQrDQu/?igsh=MTBsNng4a3djbGxtNw==)
## Idéias

01. Barragens subterrâneas
02. Curso de torneiro mecânico (na capital) ou no município
03. Curso de marcenaria
04. Informática avançada e programação encubadora de Software
05. Palestra do Pierluigi Piazzi (Para a educação)
06. Rádios comunicadores na campanha
07. Inseminação artificial para melhoramento genético do gado.
08. Distribuição de fruteiras.  quintal produtivo e farmácia viva.
09. Distribuição de sementes de capim
10. Seleção de articuladores via redação (Quem sabe redigir convence)
11. Articulação com transportadores
12. Distribuição de palma direto nas propriedades (contrapartida)
13. Programa periódico de análise de água das cisternas visando a saúde
14. Programa dentista itinerante (gratuito)
15. Feira de troca de sementes e programa de aquisição de alimentos 
16. Sistema de câmeras na sede e Macaoca
17. Programa chinêz de implementos para assentamentos (Federal)
18. Follow up dos atendimentos (especialmente para quem busca o Prefeito)
19. Exame simplificado de vista no começo do ano letivo
20. Cooperativa dos comerciantes para salvar o comércio
21. Quadras e ginásios poliesportivos
22. Comunidades mais pobres os alunos ficarem o dia todo na escola
23. Focar na diminuição dos pequenos delitos para descaptalizar o crime
24. Cavalgada dominical
25. Campanha municipal de castração e amparo a animais abandonados
26. Calçadão na estrada de Quixeramobim para caminhadas
27. Projeto cabra leiteira (Sobral exemplo) animal chipado
28. Mutirão de plantio de árvores nativas
29. Programa de recuperação de nascentes
30. Combate a esgotos a céu aberto (Disque denúncia)
31. Carnaval com bandinha na rua (tradicional)
32. Festival de forró tradicional e vaquejada

No Follow Up funcionários da prefeitura visitam as pessoas que buscam a prefeitura, tipo: Consertando a cisterna, aferindo a qualidade da água que bebem, fazendo avaliação da visão, saúde bucal e questões de segurança alimentar (aconselhamento nutricional). A idéia é sentirem o quanto a prefeitura e o prefeito se preocupam com as pessoas. Deverá existir uma pasta dessas para cada localidade.

Censo da saúde: e criação de um banco de dados com histórico de saúde. Quando foi a última vez que a família tomou remédio pra verme?

Gilvan Sales de mãos dadas com o povo e a proteção do Divino mestre, com seu propósito sincero, seu jeito simples, sorriso largo e amigo. Sempre solícito e atento às causas populares há de receber de Deus bênção e inspiração pra junto conosco ganhar essa eleição.

Um tempo novo chegará em Madalena, cidade de povo simples e humilde mas que ao longo do tempo tem sido maltratada por filhos da terra sem coração, que se fingindo de boa gente tem enganado seguidamente essa gente simples. Abre o olho Madalena, porque passarão em tua porta esses mesmos oportunistas que só aparecem na eleição, com mentira e muitas promessas inventando coisas sobre o Gilvan Sales.

Desafiamos a todos, não terem tido um parente ou amigo a quem o Gilvan não tenha socorrido na hora da precisão. É chegada a hora de reconhecer o valor de quem de verdade o tem por isso meu povo amigo valorize esse homem do povo.

Texto do João

Vamos todos juntos
Pra ganhar essa eleição
Com o nosso Gilvan Sales
E apoio do povão
Com as bênçãos de Deus
É o momento da união...

Pela saúde, pela educação,
esporte e cultura
É com o Gilvan Sales
Que a nação está segura
Vamos todos colocar
O Gilvan na prefeitura...

É ele o candidato
Que preocupa com o povão
De mãos dadas sempre juntos
Ganharemos essa eleição
Salve o nosso Gilvan Sales
Fortaleza em união.

## Texto para visitas

Até antes da convenção do Gilvan, ainda era possível acreditar que o grupo da prefeita pudesse ter mais apelo popular, embora nós já desconfiássemos que essa força política do grupo dela não era tudo aquilo que eles repetiam com muita soberba, e veio a convenção, e os deixou atônitos, os poderosos ficaram desnorteados, surpresos. O que vai acontecer até as eleições é que eles, cheios de dinheiro, com as mesmas promessas vazias irão intensificar seu assédio ao povo. Mas o povo sabe quantas vezes deu com a cara na porta da prefeitura, quantas obras mal feitas tiveram que ser refeitas e apresentadas ao povo como se estivessem fazendo favores. O tempo e a sabedoria popular estão prestes a retirar o poder de quem em nenhum momento o fez por merecer. Clamamos aos madalenenses simples, juntense a nós para trazer o novo ao nossa amada madalena. Gilvan é o homem!

## Convencimento

A sabedoria popular tem um velho ditado que diz: "A verdade um dia aparece", Abraham Lincoln, o grande presidente dos Estado Unidos da América certa vez disse: "É possível enganar algumas pessoas todo o tempo; é também possível enganar todas as pessoas por algum tempo; o que não é possível é enganar todas as pessoas todo o tempo".

Após longos 8 anos da atual gestão, a realidade dos fatos está posta: Obras mal feitas e desrespeito à inteligência das pessoas. Eles tem a desfaçatez de apresentar-se ao povo prometendo uma gestão séria, ora meu povo, quem teve 8 anos pra fazer diferença real na vida da gente sofrida de nossa terra e zombou dela deve apresar-se em fazer as malas. Já dizia o Leu Pinéu: "Arruma a mala aê". Madalena ama quem de fato a ama, o filho ingrato deixa seu nome no lixo da história. Viremos e página dando lugar a um homem sério, reconhecido pelo povo simples. Gilvan Sales, o homem que reconhecidamente foi o melhor vereador que Madalena já teve e com o apoio de pessoas simples e a benção de Deus será nosso prefeito.

## o tempo não para

Como dizia Cazuza: "O tempo não para" e para alguns políticos de Madalena a afirmação de que "suas ideias não correspondem aos fatos" também é mais atual do que nunca. E já que o tempo não para, que temos que aprender com nossos erros, já que muitos de nós, ingenuamente confiamos no discurso de quem, sendo filha da nossa terra, prometera ser fiel a ela, deixando tão somente desapontamento e a certeza de que cometemos um erro. O tempo veio e essa árvore, a da confiança, definhou. Boa parte dos que se diziam liderança em Madalena, com sua subserviência à atual gestão, apenas dizem com seus atos, que de fato não são fiéis ao povo que eles dizem defender. Nos resta seguir em frente confiantes na sabedoria popular. O povo simples saberá dar a resposta devida no momento certo. Mas aqui estamos novamente para alertar a todos, eles, com discursos bonitos, baterão na tua porta mais uma vez, prometendo o que não foram capazes de cumprir em 8 anos, apresentando-se como "o novo", mas não se engane, é tudo maquiagem! Suas idéias não correspondem aos fatos!

## lideranças

Tem gente em Madalena que se auto intitula "Liderança", contudo o pleito que se aproxima trará algumas surpresas, pois o que caracteriza uma liderança legítima é sua capacidade de levar às comunidades benefícios perceptíveis. Se nas oportunidades que o camponês, o pequeno lavrador, homem humilde do campo, necessitou de um mísero comprimido no hospital e não foi atendido em sua necessidade, ou quando por um motivo qualquer buscou a prefeitura e recebeu alguma desculpa, o que o fez desanimar, fique certo, ele não esqueceu, digamos que ele anotou no seu caderninho. Essa conta não fecha. O povo às vezes custa a perceber mas essa percepção um dia chega, é como uma enchente muito grande, a força do povo não se pode deter. Quando a massa escolhe um lado não tem poderoso que a domine. Acreditamos firmemente que um dêsses momentos chegou. O povo de Madalena está de saco cheio de tanta propaganda de obras mal feitas e descaso com os reais problemas de nossa gente. Cantemos em alto e bom som: "Arruma a mala aê".

## Quem te visita?

Quem te visita na política? Essa pessoa aparece mesmo na hora de sua dificuldade? Quando faltou comprimido no hospital, qual foi a desculpa que ela deu? Se é que é possível acreditar em alguma desculpa nesse caso? Você acha mesmo que uma avenida de duas pistas estreitas no centro de Madalena pode ser considerado algo extraordinário? Poderia isso ser divulgado como sinal de uma gestão séria? Se essa gestão que se arrasta, e esse é o termo mais apropriado, por intermináveis 8 anos, com muita propaganda, obras de péssima qualidade, contas desaprovadas pelo tribunal de contas dos municípios, mas já que a maioria dos vereadores são submissos e se omitem em seu papel de fiscalizar, cabe a você cidadão, retirar o poder,  de quem ficou provado que está fazendo mau uso dos recursos escassos de Madalena, pois quando um município tem contas desaprovadas isso é indício claro de corrupção. A atual gestão não pode negar esse fato, e se fizer beicinho é porque está vestindo a carapuça. Errar uma vez é humano, persistir no erro é burrice. Vamos mandar essa gente arrumar as malas: "Arruma a mala aê!"

## Nepotismo

No setor público: O nepotismo é ilegal. A Súmula Vinculante nº 13 do Supremo Tribunal Federal (STF), publicada em 2008, proíbe a prática de nepotismo nos três poderes (Executivo, Legislativo e Judiciário),

Dentre as muitas ilegalidades praticadas pela atual gestão uma em especial campeia solta, é o nepotismo, e o que vem a ser esse bicho? Simples, empregar qualquer parente em cargo público. No setor público: O nepotismo é ilegal. A Súmula Vinculante nº 13 do Supremo Tribunal Federal (STF), publicada em 2008, proíbe a prática de nepotismo nos três poderes (Executivo, Legislativo e Judiciário).

Assim sendo, não só a prefeita comete crime como os vereadores que a apoiam, já que ficam bem caladinhos, e sim meu povo, como diz o ditado: "O rei está nú", a prefeita que alardeia uma gestão de sucesso, lança mão de práticas ilícitas e o VAR, que são os vereadores, ao invés de denunciarem, se calam, deixando o mal feito em baixo do tapete. O penalti não foi acusado pelo juiz, mas voçê eleitor humilde tem a força do seu voto. Se por acaso lhe oferecerem dinheiro, é sinal de que querem a continuidade dessa gestão e de seus desmandos.

Pra piorar a situação, depois de oito anos dessas práticas ela apoia um candidato que, esse sim é honesto, hora vejam quem a essa altura do campeonato tem a petulância de nos dizer que é bom pra Madalena. e em resposta a isso o povo tem um ditado que diz: Filho de peixe peixinho é. Se o bicho tem orelha de porco, pé de porco, focinho de porco e se apresenta como beija flor, aí eu digo: Cai nessa quem quer! Vai comendo raimundão!

## Dissimulação

A história que se repete:

Quando o ex prefeito Zarlú perdeu a eleição, muitos disseram que o seu maior adversário teria sido a Rejeição. No caso da atual gestão existe um elemento a mais a se considerar, o adversário tem muitos serviços prestados e já provou ser bom de voto, ou seja, desenha-se um duro embate, porque embora o adversário do Gilvan tenha até fingido um rompimento com a prefeita, percebe-se um envolvimento muito estreito entre ele e a prefeita, a intenção deles é convencer o povo de que  atual prefeita não interferiria na gestão de seu pupilo. Porem, é tarefa um tanto difícil dissociar a prefeita do seu candidato, dado o nível de engajamento dos funcionários, especialmente os comissionados, que perambulam o município exaltando qualidades na prefeita e no candidato dela que nós particularmente não enxergamos, nem com muita boa vontade ousamos supor que ela não teria influência na nova gestão. Se independente fosse, nosso adversário teria feito como o Gilvan, afastando-se da prefeita e de sua esfera de influência. Saiba pois o eleitor que: Votar em Crispiano é votar em Sônia e em tudo que isso representa. O aviso tá dado, cai na esparrela quem quer.

## Abandono

A melhor maneira de identificar se um político é mesmo sério é observando o modo como ele trata as pessoas mais simples, as comunidades mais distantes. Tomemos como base duas comunidades de nosso município: Poço doce e Torrões, para os moradores dessas localidades o acesso ao município vizinho é feito sobre uma passagem molhada, que como todos sabem, além de ser altamente insegura, em dados momentos inviabiliza totalmente o acesso entre Paracurú e Paraipaba. É algo impensável imaginar que um município como Paracurú dependa do nível do rio para acesso a um município vizinho, esse ano houve vários acidentes com motos sendo arrastadas pelo rio. A comunidade dos torrões tem há muitos anos a necessidade de iluminação decente, especialmente para o povo da Crôa. Torrões espera há muito tempo pelo cumprimento de promessas que até hoje nunca se tornaram realidade.

Há um ditado que diz: "É burrice continuar fazendo a mesma coisa e esperar algo diferente". Para sair das promessas e passar para a realidade vote diferente".

## Herança

Quando uma gestão pública deixa um legado negativo, fica a chamada herança maldita, quando o gestor incompetente consegue eleger candidatos que representam a continuidade de seu modelo nefasto, via de regra, os malfeitos são por assim dizer "jogados para debaixo do tapete". Houve rumores de um rompimento entre nosso adversário e a atual prefeita, ao que parece foi só um jogo de cena para acalmar os ânimos dos eleitores que rejeitam a atual gestão e seu modelo concentrador de poder. É preciso que o povo esteja ciente de que daquele mato não sai coelho, que Sônia e Crispiano são duas faces da mesma moeda, que com eles Madalena continua como na cantiga da perua "pior pior pior".

## O embate real

Iniciada oficialmente a campanha política o grupo do Gilvan Sales ganha mais força, já que o lado da prefeita tem em suas mãos a máquina municipal, agora é a hora da onça beber água e uma grande mudança está em curso, o Gilvan, homem do povão enfrenta o continuísmo disfarçado de novo. As comunidades distantes, a gente humilde de Madalena sabe quem realmente os representa, alguém que faz parte da história dessa gente sofrida, saído do meio do povo, mas que ao contrário de muitos e muitas não renega o povo e continua próximo a ele, não esqueceu sua origem simples, ele não apareceu de repente, é o mesmo Gilvan que está do lado do povo há muitos anos e agora merece nosso reconhecimento. Gilvan é o candidato do povo e os poderosos podem até querer negar isso, mas não obterão sucesso. Para os que querem a continuação dessa administração de fantasias nós dizemos: "Gilvan é o meu candidato e arruma a mala aê!".

## A hora da onça beber água

Na linguagem popular, o momento decisivo é descrito como "a hora da onça beber água". Pois bem, ao nos aproximarmos dessa hora decisiva é bom que o madalenense pense duas vezes antes de entregar novamente o poder ao grupo de Sônia, mesmo aqueles que trabalham na prefeitura, que supostamente tem seu emprego e salário garantidos, deve saber que apesar do silêncio da maioria dos vereadores, as contas do município de Madalena foram desaprovadas pelo tribunal de contas dos municípios, e o que isso significa? Significa que a destinação dos recursos que o município recebeu não foi devidamente comprovada. Tipo assim, o bicho tem focinho de porco, orelha de porco, pé de porco, mas eles querem dizer que o bicho é outro. Eles dizem "somos honestos!", e porquê os vereadores que apoiam Sônia não fizeram investigação nas contas do município? Quem é de fato honesto não teme investigação. Nós eleitores somos os fiscais dos vereadores e de prefeito, e se confiarmos quem teve contas desaprovadas estaremos assuinando um atestado de burrice.

## Raposas

Para o eleitor mais experiente de Madalena, ouvir falar da gestão de Sônia, de suas supostas realizações fantásticas e de um candidato seu que se apresenta como mensageiro do novo, tudo isso soa estranho, alguma coisa nessa estória tá mal contada. De onde já se viu? botar a raposa pra cuidar do galinheiro, a turma que os segue é a mesma de sempre, tem gente ali que se perder a boquinha da política fica sem chão, tem vereador do lado de lá que pula mais de galho do que rouxinol. Já passou da hora de Madalena deixar de brincar de política e assumir postura séria. Gilvan é um homem experimentado e o povo o conhece. Junte-se a nós nesse processo de mudança, urgente e necessário! O que não podemos é nos contentar com quem fez tão pouco por nossa gente!  Com Gilvan temos plena convicção que Madalena dará um salto para o progresso nunca visto em sua história! É Gilvan na cabeça e no coração!

## Tempo

Nada como o tempo pra separar a verdade da mentira, após muitos anos na política Gilvan praticamente não tem rejeição, já com relação à prefeita e seu candidato não se pode dizer o mesmo. Os menos ingênuos sabem que há uma panelinha se lambuzando com o poder, mas benefícios reais para a coletividade é tudo  miragem, uma fantasia que ganha forma na mente e na boca daqueles que equivocadamente, por privilégio, ingenuidade ou má fé divulgam supostas realizações da atual gestão. Quando se olha o quanto de dinheiro foi destinado a Madalena e o que se transformou em algo palpável fica absolutamente nítido, claro, cristalino, que em se tratando de gestão dona Sônia foi na verdade um desastre. As pessoas que amam Madalena de verdade tem que saber separar amizade de política. Não vote só porquê é amigo de fulano, primo de beltrano, porquê enquanto isso acontecer o fumo vai entrando, e depois que passar a eleição não adianta reclamar, são mais quatro anos de conversa mole. É só uma patrolzinha aqui, uma passagem molhada acolá, uma pintura nas escolas e Madalena padecendo nas mãos daqueles que só tem amor por ela nos lábios. Cante a música no Néo Pinéo e em 6 de Outubro vote Gilvan 10.

Quando alguém vende o voto está cometendo a maior burrice que pode existir, mas porquê? É que se pegarmos a quantidade de dinheiro que entra no município ao longo dos quatro anos e dividirmos pela população o valor que daria seria centenas de vezes o valor que se houve falar por aí. Ou seja quem vende o voto por, digamos, 100 Reais está dando acesso ao político ladrão a milhões de reais. Só pra voçês terem uma idéia de quanto dinheiro entra em Madalena, para aquela estrada da Cajazeiras vieram Um milhão e meio de reais.
R$ 1.500.000,00

A título de comparação vejamos:
Considerando, digamos o valor de 100 Reais, esse valor cabe 15.000 vezes em R$ 1.500.000,00. Ou seja, com o valor de uma obra apenas o político ladrão poderia comprar todo o eleitorado
de Madalena.

## O tribunal

Pra nós eleitores é de suma importância saber o que é o Tribunal de Contas dos Municípios. Esse órgão tem a função de verificar, se o dinheiro que vem pro município foi gasto de forma correta, se não houve desvio, o chamado roubo. Assim, o tribunal emite um relatório dizendo se tudo correu bem ou se houve irregularidades no município em questão. Quando o relatório do tribunal aponta irregularidades as contas do município são desaprovadas, aí deveria entrar em cena a figura do vereador, é ele que de posse desses indícios de irregularidades deveria abrir investigações contra o prefeito ou prefeita. No caso de nossa cidade as contas foram desaprovadas, e como diria o matuto, a maioria dos vereadores, que são ligados à prefeita preferiram ficar caladinhos. E onde entra voçê eleitor nessa história? Se o vereador que devia ser fiscal da prefeita não cumpriu seu papel, o nosso como eleitor é mandar ele arrumar a mala e votar em outro.

#  Acreditar

Se eu que acreditava em Sônia, que no passado a admirava e torcia por ela, me decepcionei amargamente, fico então a pensar no candidato dela, que tal qual a própria, posa de bom moço, mas acontece que eu tô ficando é velho, não é doido não. Eu quero é mudança, e no horizonte de nossa amada Madalena só tem uma mudança possível e ela atende pelo nome de Gilvan Sales, o resto é papo furado!

Não é vergonha nenhuma admitir um erro, agora insistir num caminho repleto de erros, ignorar que diante de tantas promessas, nem o básico nos foi proporcionado, pois à partir do governo de Sônia foi que Madalena passou a não ter mais um banco, uma delegacia, que os alunos das comunidades foram todos amontoados na sede. Foi comum nesse governo, se é que assim o podemos chamar, a falta de comprimidos no hospital. Foram oito anos de indiferença com o povo. E se alguém chegar na tua porta oferecendo facilidades, receba o que é seu e vote com consciência, e cremos que ela, a sua consciência não te deixará votar nesse atraso disfarçado de novo.

Oito anos se foram de uma gestão que não vai deixar saudades, claro que alguns poucos gatos pingados ariscam tecer elogios à mesma, mas a verdade é que Madalena estava sufocada e em 6 de outubro vai respirar a plenos pulmões e como na música de Chico Buarque diremos: "Apesar de você Amanhã há de ser outro dia, eu pergunto a você onde vai se esconder
Da enorme euforia". Enfim 

## Luva de pedreiro

Em época de eleição se alguém chegar pra voçê oferecendo dinheiro pelo seu voto, saiba que é o ladrão do dinheiro público que veio devolver o seu dinheiro. Ora meu amigo, dinheiro não nasce em árvore, em geral o dinheiro oferecido na compra de votos veio da obra mal feita, dos sacos de cimento a menos na escola do seu filho que não foram gastos, dos buracos da estrada que não foram consertados, era o dinheiro que deveria ter se transformado em seringa, agulha e comprimido que faltaram no hospital. Se algum político tem dinheiro pra comprar votos esse dinheiro nunca foi dele, é o seu dinheiro que ele veio devolver, faça como diz o Luva de Pedreiro "Receba", receba e na hora do voto vote num candidato que preste, pois ladrão que rouba ladrão tem cem anos de perdão.

## Está chegando a hora

Diz o ditado: "Quem parte leva saudade de alguém que fica chorando de dor", só
que não, a prefeita não vai deixar saudade alguma, talvez para um magote de
privilegiados, de cargo comicionado e bajuladores. A gente simples tá é com o
Gilvan, o desespero já campeia entre os que duvidavam da força política do grupo
do Gilvan. E de quebra essa política vai promover uma renovação também nas
lideranças, pois alguns que ainda se achavam liderança estão descobrindo que de
liderança só carregavam o nome. Essa gente sabida vai ter de cantar: "Ai, ai ai
ai, o dia já vem raiando meu bem eu tenho que ir embora".

No tempo do Vitor o professorado o rejeitou. Vitor não tinha poder, não tinha a
máquina, e por isso os professores, ao menos os oportunistas não o apoiaram,
agora a cantiga é outra. Mas em compensação eles não estão enfrentando uma
galinha morta não, a parada aqui é outra, o buraco é mais embaixo. Vai comendo
raimundão.

Eles querem nos convencer de que alguém saido das saias da prefeita não
continuaria com as panelinhas que campeiam nessa gestão, gestão que aliás é
muito boa em maquiagem, divulgando obras mal feitas como se estivessem fazendo
grande coisa. Cá pra nós, eles estão é se pelando de medo de perder a boquinha.
