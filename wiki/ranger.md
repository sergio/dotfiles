---
file: ranger.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [files, tools, desktop, vim, rename]
---

# ranger
+ https://do.co/2UjHyXd

A simple, vim-like file manager (file explorer)

## Shortcuts
+ https://github.com/ranger/ranger/wiki/Official-user-guide

    i ............... enter selected dir or preview file
    h ............... jumps to left panel
    l ............... jumps to right panel
    q ............... exit ranger
    j ............... selects file or dir below
    k ............... selects file or dir above
    cw .............. rename current file
    yy .............. copy file
    pp .............. paste
    dtt ............. (personal mapping to copy selected files to another tab gn)
    dff ............. (personal mapping to copy selected files from anoter tab)
    dd .............. prepares to deletion
    dD .............. Delete file
    :bulkreame ...... rename a bunch of files
    ? ............... show help, keybindings etc
    / ............... search
    f8 .............. console delete
    ctrl + h ........ toggle hidden files
    gn .............. tab new
    gc .............. close tab
    m [x] ........... bookmark the dir
    `x, 'x .......... jump to mark x
    `` .............. jump to previous dir
    I 	.............. edit name, cursor at front (I change it to i, and make Alt-i view in pager)
    A 	.............. edit name, cursor at end
    a 	.............. edit name, cursor inside extension
    zh ............... toggle show hidden files
    z ................ toggle settings
    F7 ............... create dir
    insert ........... touch (create) file
    +x ............... make file executable

    'v' selects all if nothing is selected, again and it deselects all, and if some are selected with Space then 'v' inverts selection, 'uv' deselects all if some are selected

## Reload ranger config file:

    :eval fm.source(fm.confpath('rc.conf'))

## copy or move selected files/direcoties:
+ https://superuser.com/a/1156817/45032

To move the selected file or directory to a given directory, e.g.:

    shell -f mv %f /foo/bar

where %f is the ranger macro for highlighted files, and %s would be for all the selected.
The option -f forks the operation so that the ranger session is not interrupted by a long file process.
You can summon the comand line with s, ! or @.

But that actually only saves you from hitting yy Tab dd Tab. To be honest I usually switch tab and dd.

I set up key-bindings in the rc.conf file like so,

    #sync sel'd into dir on next tab
    map ytt shell -f rsync -rult %s %D

    #sync sel'd from dir on next tab
    map yff shell -f rsync -rult %S %d

    #sync sel'd into dir on next tab  :incl referred files
    map ytr shell -f rsync -ruLt %s %D

    #sync sel'd from dir on next tab  :incl referred files
    map yfr shell -f rsync -ruLt %S %d

    #move sel'd into dir on next tab
    map dtt shell -f mv %s %D

    #move sel'd from dir on next tab
    map dff shell -f mv %S %d

Note that %S is the macro for the selected files on the next tab. %d is the macro for the directory path of current tab.

And some others,

    #copy sel'd into h'd dir
    map pc shell -f rsync -rut %s %d/%f/

    #move sel'd into h'd dir
    map pm shell -f mv -u %s %d/%f

    #copy buffer into h'd dir
    map pds shell -f rsync -rult %c %d/%f/

    #move buffer into h'd dir
    map pdm shell -f mv -u %c %d/%f

    #sync buffer'd files into curr dir :links only
    map ps shell -f rsync -rult %c %d

Where %c is the ranger macro for files held in the copy/cut buffer.

Files are easily added to the buffer with yy, to yank, or ya, to add to the copy buffer. Similarly, with dd, to cut, or da, to add to the cut buffer.

## how to use ranger bulkraname?

Select the files you want to raname with `v` command and keep
pressing `j` until you select in "yellow" all files. After that press...

    :bulkrename <Enter>

    Now you are on vim and you can use `Ctrl-v` to block select
    the patterns you want to change or use other vim resources
    to achieve your final result

For example, adding leading zeros:

    :let c=0 | g// let c+=1 | s//\=printf('%02d', submatch(0))/

## Ranger Open File and Folder Quicker with Fasd
Fasd is a program to open commonly used directory and files quicker.
Ranger is a great file manager, but now we can combine the two to be even more powerful.

* tutorial video: [Link](https://www.youtube.com/watch?v=V9T2G7eGzgc)
* offical website: [Link](https://github.com/clvv/fasd)

## install requirements

    ranger fasd fzf

## what is fasd?

https://www.youtube.com/watch?v=ur81Y-mV5Us

## keybinding

    ranger --copy-config=rc

    vim ~/.config/ranger/rc.conf

    map zz   fzf_fasd
    map zo   console fasd%space

## commands

    vim ~/.config/ranger/commands.py

    # fzf_fasd - Fasd + Fzf + Ranger (Interactive Style)
    class fzf_fasd(Command):
        """
        :fzf_fasd

        Jump to a file or folder using Fasd and fzf

        URL: https://github.com/clvv/fasd
        URL: https://github.com/junegunn/fzf
        """
        def execute(self):
            import subprocess
            if self.quantifier:
                command="fasd | fzf -e -i --tac --no-sort | awk '{print $2}'"
            else:
                command="fasd | fzf -e -i --tac --no-sort | awk '{print $2}'"
            fzf = self.fm.execute_command(command, stdout=subprocess.PIPE)
            stdout, stderr = fzf.communicate()
            if fzf.returncode == 0:
                fzf_file = os.path.abspath(stdout.decode('utf-8').rstrip('\n'))
                if os.path.isdir(fzf_file):
                    self.fm.cd(fzf_file)
                else:
                    self.fm.select_file(fzf_file)

    # Fasd with ranger (Command Line Style)
    # https://github.com/ranger/ranger/wiki/Commands
    class fasd(Command):
        """
        :fasd

        Jump to directory using fasd
        URL: https://github.com/clvv/fasd
        """
        def execute(self):
            import subprocess
            arg = self.rest(1)
            if arg:
                directory = subprocess.check_output(["fasd", "-d"]+arg.split(), universal_newlines=True).strip()
                self.fm.cd(directory)

## references
- https://www.youtube.com/watch?v=V9T2G7eGzgc
- fasd from cli: https://www.youtube.com/watch?v=ur81Y-mV5Us
- [fzf playlist](https://www.youtube.com/playlist?list=PLqv94xWU9zZ2fMsMMDF4PjtNHCeBFbggD)
- [ranger playlist](https://www.youtube.com/playlist?list=PLqv94xWU9zZ18QJz2Ev8mSeHlICJbejzK)
- https://github.com/ranger/ranger/wiki/Commands
- https://github.com/clvv/fasd
- https://github.com/junegunn/fzf

## contact

                 _   _     _      _
      __ _  ___ | |_| |__ | | ___| |_ _   _
     / _` |/ _ \| __| '_ \| |/ _ \ __| | | |
    | (_| | (_) | |_| |_) | |  __/ |_| |_| |
     \__, |\___/ \__|_.__/|_|\___|\__|\__,_|
     |___/

- http://www.youtube.com/user/gotbletu
- https://twitter.com/gotbletu
- https://plus.google.com/+gotbletu
- https://github.com/gotbletu
- gotbletu@gmail.com

