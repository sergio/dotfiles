---
file: radios.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Links of radios

The Brian Later Show
* http://fm939.wnyc.org/wnycfm

Most of my radios is on my aliases file

    apnews='mplayer http://apnews.streamguys1.com/apnews'
    cbanews='mplayer http://abclive.abcnews.com/i/abc_live2@136328/index_2500_av-b.m3u8'
    cbsnews='mplayer http://cbsnewshd-lh.akamaihd.net/i/CBSNHD_7@199302/index_2200_av-p.m3u8'
    news='mplayer -playlist http://minnesota.publicradio.org/tools/play/streams/news.pls'
    nprnews='mpv https://nprdmp-live01-mp3.akacast.akamaistream.net/7/998/364916/v1/npr.akacast.akamaistream.net/nprdmp_live01_mp3'
    rtnews='mplayer https://rt-eng-live-hls.secure.footprint.net/rt/eng/index1600.m3u8'
