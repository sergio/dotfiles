---
File: ~/.dotfiles/wiki/wezterm.md
Last Change: Sat, 09 Nov 2024 - 17:43:11
tags: [terminal, tools]
---

# intro

WezTerm is a modern terminal emulator and multiplexer with a focus on high performance, versatility, and customization. It was created by Wez Furlong and is known for supporting advanced features, making it popular among power users and developers. Here are some of the highlights of WezTerm:
Key Features

GPU Acceleration: WezTerm leverages the GPU to render text, making it fast and efficient, especially when working with high refresh rates or complex UIs.

Multiplexer Integration: WezTerm includes its own built-in multiplexer similar to tmux, allowing users to manage multiple terminal sessions within a single window. This lets you split panes, switch between them, and manage workspaces without needing an external multiplexer.

Cross-Platform Support: It’s available on Windows, macOS, and Linux, with configurations that work consistently across platforms.

Lua Configuration: WezTerm is configured using Lua, making it flexible and scriptable. You can define custom keybindings, color schemes, and even advanced behaviors with Lua scripts.

Font and Text Rendering: WezTerm includes excellent font support, including ligatures, emojis, and Unicode characters, and supports font fallbacks to improve text display.

Color and Theme Customization: WezTerm provides robust color scheme support, with popular themes like Solarized, Dracula, and many others. You can also define custom color schemes or use dynamic themes that change based on conditions, like time of day.

Tab and Pane Management: Users can create, rename, and close tabs and panes easily, allowing flexible workflow management. Each tab can also have its own configuration, such as a different font or color scheme.

SSH and Serial Port Support: It integrates SSH and serial port support, allowing users to connect directly to remote servers or devices from within WezTerm.

Wayland Support: WezTerm supports Wayland, the display server protocol that’s increasingly popular on Linux, in addition to X11.

Performance Optimizations: With features like scrollback, smooth scrolling, and precise rendering, WezTerm is optimized for both appearance and efficiency, handling large outputs smoothly.

[youtube](https://youtu.be/V1X4WQTaxrc)
[wezterm config](https://github.com/m1chaelwilliams/make-windows-pretty)
