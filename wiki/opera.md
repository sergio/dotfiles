---
file: opera.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Opera browser tips

## How to import bookmarks into opera:
+ https://www.lifewire.com/import-bookmarks-and-data-to-opera-4103700
+ https://www.lifewire.com/import-bookmarks-and-data-to-firefox-4103605

First, open your Opera browser. Enter the following text into the
browser's address/search bar and hit the Enter key:

```text
opera://settings/importData
```

## How to show your bookmarks on opera

```text
Ctrl + Shift + b
```


## To remove search bar/box from start page go to

How to hide search bar on speed dial

Settings (or press Alt+P) → click on white space

To access power user settings go as follow with the arrows on your keyboard:-

UP UP DOWN DOWN LEFT RIGHT LEFT RIGHT then type letter "B" and letter "A" (without quotes)

On the dialog click: I understand proceed

In settings fast search type SPEED

Check the box next to Hide search box in Speed Dial
