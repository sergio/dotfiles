---
File: ~/.dotfiles/wiki/cobreiro.md
Last Change: Mon, 20 May 2024 - 09:28:10
tags: [reza, oracao]
---

# Reza pra curar cobreiro

Usa três espetos, cada pedaço de mais ou menos um palmo
encosta o primeiro pauzinho encosta onde tá o cobreiro e fala "que corte",
ensina a pessôa a responder: Cobreiro brabo corta a cabeça, a pessôa repete
"corta a cabeça", corta o meio, a pessôa repete "corta o meio", corta o rabo
(repete), aí voçê responde sozinho: "assim mesmo eu corto" e vai quebrando o
pauzinho e reza um Pai Nosso, uma Ave Maria e um Glória ao Pai. Para cada
pauzinho. Em voz baixa (quebrando os pauzinhoso) 3x Pai Nosso, Ave Maria e
Glória ao Pai para cada pauzinho.

O ritual é o mesmo para os três pauzinhos

Corte: a pessoa responde Cobreiro Brabo e segue o ritual, pode encostar por fora
da roupa especialmente se for em parte íntima.

Se for uma criança ou pessôa encabulada pede um adulto pra responder pela
pessôa.

Ofereço esses nove Pai Nossos Nove Ave Marias e nove Glórias ao Pai à Nossa
Senhora da Piedade e à Santíssima Trindade, assim como Vós curastes vosso filho
amado curai o cobreiro de Fulano e manda pendurar os pauzinhos quebrados num
lugar pra secar ao sol ou joga em cima de um telhado. Ou pendura numa fumaça
pois assim que secar os pauzinhos o cobreiro morre.

- Na quebra nâo separa as partes dos pauzinhos
- Amarra o três junstos para dar o destino.
- Utilize uma faquinha pra fazer os cortes
