File: /home/sergio/.dotfiles/wiki/artix.md
Last Change: Fri, 24 Nov 2023 - 12:11:29
tags: [archlinux, aur]

[after install](https://www.rikerlinux.com.br/2022/04/o-que-fazer-apos-instalar-o-artix-veja.html)

## Add archlinux keys and archlinux repos

[add repo](https://dev.to/nabbisen/artix-linux-add-arch-linux-repos-extra-community-35ab)

```sh
sudo pacman -Sy artix-keyring
sudo pacman -Sy archlinux-keyring
sudo pacman -Syu artix-archlinux-support
sudo pacman-key --populate archlinux

sudo pacman --noconfirm -S archlinux-keyring
sudo pacman -S artix-archlinux-support

sudo nvim /etc/pacman.conf

[universe]
Server = https://universe.artixlinux.org/$arch
Server = https://mirror1.artixlinux.org/universe/$arch
Server = https://mirror.pascalpuffke.de/artix-universe/$arch
Server = https://artixlinux.qontinuum.space/artixlinux/universe/os/$arch
Server = https://mirror1.cl.netactuate.com/artix/universe/$arch
Server = https://ftp.crifo.org/artix-universe/$arch
Server = https://artix.sakamoto.pl/universe/$arch

sudo pacman -Sy
sudo pacman -Syu artix-archlinux-support
```

## some useful packages

``` sh
sudo pacman -S xdo python-pyyaml aria2 yt-dlp pamixer detox bat
doas pacman -S ytfzf curl jq ueberzug
```

## zip unzip packages

```sh
sudo pacman -S p7zip zip unzip
```

## lock screen

``` sh
doas pacman -S xautolock slock
```

## Acertar o locale

``` sh
ln -sf /usr/share/zoneinfo/America/Fortaleza /etc/localtime
sudo hwclock --systohc
```


## Iniciar o ntpd jervice para acertar o relógio

```sh
rc-service ntpd start
```

## Controling services

```sh
rc-service service start
```

## How to know my init system

```sh
ls -l /sbin/init
```

## How to install ssh openrc and cron

``` sh
doas pacman -S openssh-openrc cronie

sudo rc-update add sshd default
sudo rc-service sshd start

rc-update add cronie default
sudo rc-service cronie start
```


## install lua language server (nvim)

``` sh
sudo pacman -S neovim xclip clipmenu
sudo pacman -S lua-language-server
sudo pacman -S neovim-nvim-treesitter tree-sitter-cli
sudo pacman -S stylua
sudo pacman -S luarocks nodejs neovim-nvim-treesitter ripgrep
sudo pacman -S nodejs npm prettier stylua
```


## wallpaper changer

```sh
sudo pacman -S python-pywal
```

## para que o terminal scratchypad funcione no dwm instale o xdo

## suporte ao AUR

+ <https://www.edivaldobrito.com.br/aur-helper-yay-no-arch-linux/>

### Como instalar o AUR Helper YAY no Arch Linux e derivados

Para instalar o AUR Helper YAY no Arch Linux e derivados, você deve fazer o seguinte:

Passo 1. Abra um terminal;
Passo 2. Certifique-se de que seu sistema tem instalado o git e os pacotes da linguagem Go;

sudo pacman -S git go

Passo 3. Use o comando abaixo para baixar o código fonte do YAY;

git clone <https://aur.archlinux.org/yay.git>

Passo 4. Em seguida, acesse a pasta criada;

cd yay

Passo 5. Finalmente, use o comando abaixo para compilar e instalar o programa;

makepkg -si

Como usar o YAY?

O uso do YAY é semelhante a outros auxiliares e gerenciadores de pacotes do AUR.

Para instalar um pacote do utilitário YAY, execute o seguinte comando:

```sh
yay -s arch-wiki-man
```

Para procurar um pacote no AUR e no repositório oficial, execute o seguinte comando:

```sh
yay -Ss arch-wiki-cli
```

Para ver as informações detalhadas sobre um determinado pacote:

```sh
yay -Si arch-wiki-cli
```

Podemos pesquisar o pacote já instalado usando a seguinte opção:

```sh
yay -Qs arch-wiki-man
```

Para obter informações sobre o pacote instalado:

```sh
yay -Qi arch-wiki-man
```

Use o comando abaixo para atualizar qualquer pacote ou pacote no seu sistema:

```sh
yay -Syu
```

## Font iosevka

+ <https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases/download/v6.1.4/iosevka-mayukai-original-editor-v614.zip>
