Filename: inxi.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [hardware, tools, admin]

 vim:ft=markdown
inxi  - Command line system information script for console and IRC

    inxi

    inxi --full

Show battery:

    inxi -B

Cpu Info:

    inxi -C

System Info:

    inxi -S

Wheather Info:

    inxi -w

