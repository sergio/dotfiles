---
file: iso2utf.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução
iso2utf8 é um script que usa o comando [iconv](iconv.md) para fazer
conversões
