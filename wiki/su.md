---
file: su.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [admin, sudo]
---

# intro

to become admin you should use this command
+ https://www.tecmint.com/difference-between-su-and-su-commands-in-linux/

    sudo su -

user is provided his own default login environment,
including path to executable files;
he also lands into his default home directory.

To become another regular user

    su uer

## Force su to ask your password

    echo -n "Enter password for $USER:"
    su $USER -c true 2>/dev/null  && echo -e "\nsuccess" || echo -e "\nfailure"

