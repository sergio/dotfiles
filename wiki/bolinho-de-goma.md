---
File: bolinho-de-goma.md
Author: Sergio Araujo
Last Change: Sat, 27 Jul 2024 - 05:44:40
tags: [receitas, bolo, bolinho]
---

#  bolinho-de-goma da vovó:

+ Bate as claras em neve
+ Acrescenta as gemas e misturado
+ Acrescenta açúcar (uma colher para cada ovo)
+ Acrescenta a goma (um pouco mais que a quantidade de ovos)
+ Uma pitada de sal
+ Assar na chapa ou frigideira
+ Polvilhar açúcar e canela
