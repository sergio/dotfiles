---
## Recursos digitais de aprendizado

## Mapas mentais, o que são e como utilizar
 vim:ft=markdown

    + Como aprender mais rápido
    + Repetição espaçada (flashcards - Anki)
    + Sleep Learning for language acquisition
    + Sugestopedia
    + Lexical Approach (exemplos)

## Projeto Ãncora

+ Problemas do ensino tradicional

    + Sistema de notas e avaliação
         + O aluno ascende no curso sem haver completado 100% de aprendizado
         + O sistema de notas inibe a inovação
         + Na prova você começa com 100% e cada erro é punido
    + Falta de revisao para fixação do conteúdo na memória de longo prazo
        + Pirluigi Piazzi
        + como funciona o cérebro (como ele memoriza)
    + Treinando o cérebro para não gostar da aula
    + Valter Herman [idph](idph.md)

## Ferramentas web para alunos e professores

    + Favoritos online
        + google keep
        + pocket (extensão para o firefox)

    OBS: Ambas ferramentas tem aplicativos para celular que permitem
         a sincronização de conteúdos entre computador e celular

## Introdução ao Processamento de dados

    Qualifica o Professor e o Aluno no uso efetivo das ferramentas digitais,
    do computador, e da Internet de um modo geral:

        É comum nos dias atuais, jóvens que usam diariamente celulares mas
        que nunca tiveram contato com computadores, e mesmo tento contato com
        o mundo digital, desconhecerem conceitos básicos de informática.

        No quesito uso do computador um fator crítico no que se refere ao bom
        uso do mesmo é a digitação, a digitação de um simples ofício pode ser
        uma tarefa simples ou extremamente complexa, dependendo do quão hábil
        é o operador.

        **SEGURANÇA DIGITAL**
        *-------------------*
        Quando se trata de segurança digital o problema é ainda mais sério, a
        grande maioria das pessoas desconhece completamente os riscos a que
        pode estar exposto.

# Dicas firefox (Como ser mais produtivo usando o navegador)

    + [Simple-translate](https://addons.mozilla.org/vi/firefox/addon/simple-translate/)
    + Como usar tags nos favorits do firefox e como acessálas rapidamente
    + Como habilitar e personalizar a barra de favoritos do navegador
    + Bloqueadores de pop-up, o que são e como habilita-los.
    + Como usar o histórico do firefox para reabrir sites recem acessados
    + Como sincronizar seus favoritos no firefox
    + Como fixar abas do navegador para manter uma página aberta
    + Como compartilhar links com o celular e vice-versa
    + Como reabrir uma aba recem fechada
    + Como ampliar e reduzir a fonte
    + Como melhorar a legibilidade de um site
    + Extensão para bloquear pop-ups (ublock origin)
    + Como criar perfís de usuários distintos no firefox e como abri-los
    + Como habilitar navegação privada (ctrl + Shift + p)
    + Como colocar o cursor na barra de endereços (Ctrl + l)

## Como usar melhor o google

    + busca entre aspas
    + (buscar | procurar) +"conteúdo" -content
    + buscar num site
    + buscar no title
    + buscar por tipo de arquivo

    “CNN AROUND(2) Obama,” you get results where the two terms are written on the page in close proximity


## Busca avançada do gmail

    + buscar por remetente ou destinatário
    + buscar por data
    + buscar e-mails não lidos
    + buscar e-mails com anexos
    + criar filtros para limpar sua caixa postal
    + desinscrever de listas de e-mails
    + usar o gmail plus para se proteger de spam
    + criar apelidos para e-mails

## Encurtadores de url

O que são e como utilizalos

## Geradores e leitores de QRcode
Como usar QRcode

## Whatsapp web

## Ferramentas para baixar vídeos

+ No linux youtube-dl (baixa canais inteiros) + baixa também só áudio


