---
file: mapfile.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# bash (builtin) lê um arquivo e gera um vetor
+ http://www.dicas-l.com.br/cantinhodoshell/cantinhodoshell_20100121.php

``` bash
$ cat frutas
abacate
maçã
morango
pera
tangerina
uva

$ mapfile vet < frutas  # Mandando frutas para vetor vet
$ echo ${vet[@]}        # Listando todos elementos de vet
abacate maçã morango pera tangerina uva
```

Obteríamos resultado idêntico se fizéssemos:

``` bash
$ vet=($(cat frutas))
```
