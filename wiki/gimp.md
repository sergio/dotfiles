---
file: gimp.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## gimp.md

     yaourt -S gimp-plugin-lqr gimp-plugin-gmic

## Ferramenta Seleção de Frente:

    1 - Adiciona canal alpha para a camada
    2 - Contorna a área a ser selecionada
    3 - Pressiona Enter
    4 - Pinta com o Pincel a área interna da figura
    5 - Clica em Selecionar

## Atalhos do gimp:

    TAB .................. ocultar e exibir os paineis
    ] .................... aumentar pincel
    [ .................... diminuir pincel
    Ctrl-shif-j .......... ajustar imagem à janela

    Ctrl-i ............... inverter seleção

## Traçar uma linha reta:

    clique no ponto inicial
    Pressione Shift

    Use Ctrl ou Alt para fixar o final da linha e Enter

## GIMP keyboard shortcuts

    In this list you can see most important hotkeys for the GIMP under
    Linux (most of them work on Windows as well). All keys can be
    individually assigned: File / Preferences / Interface / Hotkeys.
                             Help
    Help                               F1
    Context Help                       Shift+F1
                            Toolbox
    Rect Select                        R
    Ellipse Select                     E
    Free Select                        F
    Fuzzy Select                       U
    Select By Color                    Shift+O
    Scissors                           I
    Paths                              B
    Color Picker                       O
    Move                               M
    Crop and Resize                    Shift+C
    Rotate                             Shift+R
    Scale                              Shift+T
    Shear                              Shift+S
    Perspective                        Shift+P
    Flip                               Shift+F
    Text                               T
    Bucket Fill                        Shift+B
    Blend                              L
    Pencil                             N
    Paintbrush                         P
    Eraser                             Shift+E
    Airbrush                           A
    Ink                                K
    Clone                              C
    Convolve                           V
    Smudge                             S
    Dodge/Burn                         Shift+D
    Swap Colors                        X
    Default Colors                     D
                             File
    New image                          Ctrl+N
    Open image                         Ctrl+O
    Open image as new layer            Ctrl+Alt+O
    Duplicate image                    Ctrl+D
    Open recent image 01               Ctrl+1
    Open recent image 02               Ctrl+2
    Open recent image 03               Ctrl+3
    Open recent image 04               Ctrl+4
    Open recent image 05               Ctrl+5
    Open recent image 06               Ctrl+6
    Open recent image 07               Ctrl+7
    Open recent image 08               Ctrl+8
    Open recent image 09               Ctrl+9
    Open recent image 10               Ctrl+0
    Save image                         Ctrl+S
    Save under a new name              Shift+Ctrl+S
    Quit                               Ctrl+Q
                            Dialogs
    Layers                             Ctrl+L
    Brushes                            Shift+Ctrl+B
    Patterns                           Shift+Ctrl+P
    Gradients                          Ctrl+G
    Tool-Options                       Shift+Ctrl+T
    Palettes                           Ctrl+P
    Info window                        Shift+Ctrl+I
    Navigation window                  Shift+Ctrl+N
    Close the window                   Alt+F4
    Jump to next widget                Tab
    Jump to previous widget            Shift+Tab
    Set the new value                  Enter
    Activate current button or list    Space
    In a multi-tab dialog, switch tabs Ctrl+Alt+PgUp
    Open Location                      Shift+L
    Up-Folder                          Alt+ up
    Down-Folder                        Alt+ down
    Home-Folder                        Alt+Home
    Close Dialog                       Esc
                             View
    Main Menu                          F10
    Drop-down Menu                     Shift+F10
    Toggle fullscreen                  F11
    Toggle quickmask                   Shift+Q
    Close document window              Ctrl+W
    Zoom in                            + or Ctrl+Mousewheel
    Zoom out                           - or Ctrl+Mousewheel
    Zoom 1:1                           1
    Shrink wrap                        Ctrl+E
    Fit image in window                Shift+Ctrl+E
    Toggle rulers                      Shift+Ctrl+R
    Toggle guides                      Shift+Ctrl+T
                             Edit
    Undo                               Ctrl+Z
    Redo                               Ctrl+Y
    Copy selection                     Ctrl+C
    Cut selection                      Ctrl+X
    Copy visible                       Ctrl+Shift+C
    Paste as new image                 Ctrl+Shift+V
    Paste clipboard                    Ctrl+V
    Clears selection                   Ctrl+K
    Named copy selection               Shift+Ctrl+C
    Named cut selection                Shift+Ctrl+X
    Named paste clipboard              Shift+Ctrl+V
    Fill with FG Color                 Ctrl+,
    Fill with BG Color                 Ctrl+.
    Fill with Pattern                  Ctrl+:
                            Layers
    Select the layer above             PgUp
    Select the layer below             PgDn
    Select the first layer             Home
    Select the last layer              End
    Merge visible layers               Ctrl+M
    Anchar layer                       Ctrl+H
                          Selections
    Toggle selections                  Ctrl+T
    Select all                         Ctrl+A
    Select none                        Shift+Ctrl+A
    Invert selection                   Ctrl+I
    Float selection                    Shift+Ctrl+L
    Path to selection                  Shift+V
                           Plug-ins
    Repeat last plug-in                Ctrl+F
    Reshow last plug-in                Shift+Ctrl+F

## Install gimp 2.10 flatpack

     flatpak install https://flathub.org/repo/appstream/org.gimp.GIMP.flatpakref


