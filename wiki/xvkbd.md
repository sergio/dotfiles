---
File: /home/sergio/.dotfiles/wiki/xvkbd.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [mouse, desktop, x11, tools, utils]
 vim:ft=markdown
---

## Intro:
+ https://www.reddit.com/r/linuxquestions/comments/q761a2/is_it_possible_to_change_primary_selection_paste/
+ http://xahlee.info/linux/linux_xvkbd_tutorial.html
+ https://medium.com/@Aenon/bind-mouse-buttons-to-keys-or-scripts-under-linux-with-xbindkeys-and-xvkbd-7e6e6fcf4cba

    xvkbd -xsendevent -text "$(xclip -o -selection primary)"

xvkbd - Virtual (graphical) keyboard program for X11
xvkbd is command line tool that lets you send a keyboard key signal or mouse click.

## xvkbd Command Example

    # this command sends the key control+c
    xvkbd -no-jump-pointer -xsendevent -text '\Cc'

    # this command sends mouse left click
    xvkbd -no-jump-pointer -xsendevent -text '\m1'

xvkbd key syntax

    \[F1] F1

    \[F2] F2

    \[Print] PrintScreen (Print Screen)

    \[Scroll_Lock] ScrollLock (Scroll Lock)

    \[Pause/Break] Pause (Pause/Break)

    \r Return

    \t Tab

    \e Escape (escape)

    \b Backspace ⌫

    \d Delete ⌦

    \S Shift (modify the next character)

    \C Ctrl (modify the next character)

    \A Alt (modify the next character)

    \M Meta (modify the next character)

    \[keysym] keysym is X11's key syntax. (For example, \[Left]) [see Linux: X11 Keyboard Key Names]

    \m1 Mouse left click

    \m2 Mouse middle click

    \m3 Mouse right click

    \mdigit Mouse click of the specified mouse button. [see Linux: X11 Mouse Button Numbering]



