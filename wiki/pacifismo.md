---
File: ~/.dotfiles/wiki/pacifismo.md
Last Change: Tue, 20 Feb 2024 - 08:09:44
tags: [geopolítica, brasil, mundo]
---

# Comentários sobre geopolítica

+ [vídeo sobre a vitoria russa](https://youtu.be/Z_Uv2ehKpSQ?si=T8SrRuVlGnKcaFXT)

Um rapazinho posta: Enquanto a Rússia forma engenheiros, aqui formamos
cientistas sociais preocupados com o amor.

Minha resposta:

Se a humanidade já tivesse aprendido a lição do Cristo não haveriam
atrocidades, mas em terras brazilis os ditos cristãos são armamentistas.
Mas porquê nosso exército tem um poderio bélico pífio? Porquê os países
europeus abriram mão de ter exércitos próprios fortes?

Ser inimigo da OTAN é problemático, mas ser aliado deles é terrível, veja
o caso alemão, pra não falar da própria Ucrânia, abriu mão do gás russo
barato pra comprar gás liquefeito dos EUA, diminuindo assim sua
competitividade no mercado global. A Europa toda está sendo penalizada
por esse conflito, já os magnatas da OTAN, incólumes, contabilizam o
lucro. Seja qual for o resultado, vitória ou derrota em solo europeu os
oligopólios Ianques ganham.

Por ventura já assistiu A CIA E OS NAZISTAS aqui no youtube? Talvez mude
a visão de muitos que insistem em viver no mundo de Alice.
