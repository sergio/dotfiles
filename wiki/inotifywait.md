---
File: inotifywait.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 21, 2020 - 20:48
tags: [tools]
---

#  inotifywait.md intro:

    inotifywait - wait for changes to files using inotify

## Installation on (voidlinux)

    sudo xbps-install -S inotify-tools

