---
file: tobeadopted.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


To be adopted

Teacher Debbie Moon's first graders were discussing a picture of a family. One little boy in the picture had a different hair color than the other members. One of her students suggested that he was adopted.

A little girl said, 'I know all about Adoption, I was adopted.'

'What does it mean to be adopted?', asked another child.

'It means', said the girl, 'that you grew in your mommy's heart instead of her tummy!'

grow (grow, grew, grown) - crescer
hair color - cor de cabelo
heart - coração
instead- ao invés de
little boy - garotinho
picture - foto, imagem
tummy - barriga

