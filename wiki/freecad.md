
# intro video

+ [svg to freecad](https://www.youtube.com/watch?v=6LedIN5S2so)
+ [Usar imagem como modelo](https://youtu.be/7Zhpc-3GtZg?t=99)
+ [Make sketching easier](https://youtube.com/shorts/JXO2XusXXEw)
+ [Drawing a flange](https://youtu.be/twRgek552j0?t=91)

## Shortcuts

```text
    Shift + RBM  ................. rotate object
    Ctrl + RBM ................... move
```
