Filename: fontpreview.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [fonts, preview, fzf]

 vim:ft=markdown
# intro:
+ https://github.com/OliverLew/fontpreview-ueberzug

fontpreview-ueberzug is a POSIX shell script to preview all fonts installed on system in fzf with ueberzug. It is inspired by fontpreview project while most of the code are completely rewritten here.

