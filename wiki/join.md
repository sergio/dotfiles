---
file: join.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# join.md intro

join lines of two files on a common field

				join -v 2 <(sort A.txt) <(sort B.txt)
