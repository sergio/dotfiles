---
file: nitrogen.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown

# Nitrogen

Nitrogen is a fast and lightweight desktop background browser and setter for X windows.

In my case I use it to set my [i3](i3.md) background
