---
file: w3m.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# w3m - navegador por linha de comando

    Shift-u ................... type new url
    Tab ....................... swich fields, buttons etc
    Enter ..................... in text field start search

    o ......................... display settings
    Shift-b ................... go back

## Dealing with urls
I have had some problems with downloading a bunch of files, one url per line,
the problem is that the output file had slashes "/", to fix this just use

    {/.}

    One example of how o solve this problem:

    echo "http://url"{01..10}.com | tr ' ' '\n' | parallel echo {} {/.}.txt

    parallel w3m -dump {.} -O -UTF-8 > {/.}.txt :::: urls


    # one file for each html
    parallel 'w3m -dump "{}" -O -UTF-8 "{.}.txt"' ::: *.html
    parallel 'w3m -dump "{}" > "{.}.txt"' ::: *.html
    parallel w3m -dump {} -O -UTF-8 '>' {.}.txt ::: *.html
