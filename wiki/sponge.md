---
file: sponge.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


* http://www.youtube.com/watch?v=ZeGzgVvBt3w&playnext_from=TL&videos=I2_3VXwmXiw

O comando sponge faz parte do pacote moreutils e não vem instalado por padrão

    column -t data.csv | sponge data.csv

