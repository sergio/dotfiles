---
File: /home/sergio/.dotfiles/wiki/gpm.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [mouse, utils]
 vim:ft=markdown
---

## Intro:
+ https://www.geeksforgeeks.org/gpm-command-in-linux-with-examples/

gpm - a cut and paste utility and mouse server for virtual
       consoles

With gpm we can probably solve the primary selection paste issue!

GPM (“General Purpose Mouse”) software provides support for mouse devices in Linux virtual consoles. This package provides a daemon that captures mouse events when the system console is active and delivers events to applications through a library. By default, the daemon provides a ‘selection’ mode, in order that cut-and-paste with the mouse works on the console even as it does under X. It is used to select the text, drag the mouse while holding the left button, or to paste text within the same or another console, press the center button. The right button is employed to increase the choice. Two-button mice use the right button to paste the text.
