---
file: ~/.dotfiles/wiki/split.md
author: Sergio Araujo
Last Change: Thu, 25 Jul 2024 - 16:33:17
---

# split - usado para dividir arquivos grandes

```sh
 Split a file, each split having 10 lines (except the last split)
 split -l 10 path/to/file
 Split a file into 5 files. File is split such that each split has same size )
 split -n 5 path/to/file
 Split a file with 512 bytes in each split (except the last split; use 512k f)
 split -b 512 path/to/file
 Split a file with at most 512 bytes in each split without breaking lines
 split -C 512 path/to/file


split -b 15M --additional-suffix=.mp3 big-file.mp3
``````
