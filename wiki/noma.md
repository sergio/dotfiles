
---
File: ~/.dotfiles/wiki/noma.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [notes, brain, learning]
---

By: [[NICK MILO]]

# O método NoMa
Ideias em minutos que duram uma vida inteira.

O método NoMa é sobre criação de notas. (NoMa = Note-Making). Criação de notas te inclina para frente, facilitando o engajamento com o [[mundo das ideias]].
Esse processo de engajar com as ideias afia nossos pensamentos e nos dá bens intelectuais.

## O que é Note-Making?

Basicamente, existem duas coisas:
1. Tomar anotações;
2. Criar anotações;

- Tomar anotações = Captura rápida das palavras de outra pessoa.
	Tomar anotações é um processo passivo, onde simplesmente são anotadas as palavras de outras pessoas. As coisas que vem fácil, também vão embora facilmente.
	- (FOMO-mentality = "Fear Of Missing Out")
	- (EIEO = "Easy in, Easy out").

- Criar anotações - Criadas através da ponderação, com suas próprias palavras.
	Criar anotações é um processo ativo. Se lê algo, se pensa e se escreve. Quando ponderamos sobre algo, a taxa de retenção sobre o tópico é muito maior.
	- (JOMO-mentality = "Joy Of Missing Out")
	- (MIMO = "Mindfully in, Magically Out").

## O que é o NoMa Method?

Uma maneira simples de criar ideias em minutos que duram uma vida inteira.

O método NoMa se baseia em 5 passos. São eles:
1. **Encontre coisas interessantes:**
		Toda vez que se deparar com algo interessante, pense: "Hmm, isso é interessante..."


2. **Pense em tópicos relacionados.**
	Sempre que se deparar no primeiro passo, pense em coisas relacionadas ao tópico de interesse.
	Assim, você vai estar ligando, criando "links", entre os conceitos.
	pense: "Isso me lembra de (alguma coisa)"


3. **Demonstre as semelhanças entre os dois tópicos.**
4. **Demonstre as diferenças entre os dois tópicos.**
	Os passos 3 e 4 nos permitem sermos mais ativos no nosso modo de pensar, criando justificativas para ligarmos ou não dois ou mais tópicos.


5. **Justifique seu interesse sobre o assunto**
	A pergunta "Isso é importante para mim pois..." vai te proporcionar um entendimento melhor sobre seus interesses e onde você deveria estar devotando seu foco e atenção.

> [!NOTE] Resumo das etapas
> *X* é interessante. De certa forma é parecido com *Y* por causa de *Relação* e diferente de *Z* por conta de *Diferença*. Me interesso por esse tópico por gostar de *Motivo*.
