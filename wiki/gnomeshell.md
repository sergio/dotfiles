---
file: gnomeshell.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Getting gnome-shell version

    gnome-shell --version

## news reader (RSS)
+ https://flathub.org/apps/details/org.gabmus.gfeeds

Gnome feeds

    flatpak install flathub org.gabmus.gfeeds

## Reset or disable tracker
+ https://stackoverflow.com/a/51645277/2571881
+ https://askubuntu.com/questions/346211/

Simply change this values using the gsettings witch will disable the constant indexing of Tracker:

    gsettings set org.freedesktop.Tracker.Miner.Files crawling-interval -2
    gsettings set org.freedesktop.Tracker.Miner.Files enable-monitors false

The values could be changed using dconf-editor by navigating through org > freedesktop > Tracker > Miner > Files:

After this changes, it is highly recommended to cleanup the database to reclaim some lost space on the disk:

    echo y | LANG=en tracker reset --hard

    tracker reset -r

## appfolders
+ https://wiki.gnome.org/HowDoI/AppFolders

## start gnome-shell from command line

   Add `/usr/bin/gnome-session`  to your `~/.xinitrc`
   then you can `startx`

## installing with speed limit

    sudo apt-get -o Acquire::http::Dl-Limit=20 install task-gnome-desktop task-brazilian-portuguese task-brazilian-portuguese-desktop

    sudo apt-get -o Acquire::http::Dl-Limit=20 install libgl1-mesa-dri x11-xserver-utils gnome-session gnome-shell gnome-themes gnome-terminal gnome-control-center nautilus gnome-icon-theme --no-install-recommends

    sudo apt-get -o Acquire::http::Dl-Limit=20 --no-install-recommends install gdm3
    aptitude install –without-recommends gdm3

## shortcuts
+ https://wiki.gnome.org/Gnome3CheatSheet
```
    minimize .................. super + h
    show ...................... super + s
    super (windows) + a ....... show applications
    super (windows) ........... alternate tasks (applications)
    Super+Shift+Page Up ....... move current windows to ...
    Super + h ................. hide window
    Super + right ............. split window
    Alternate apps ............ alt-tab
    Alternate apps (submenu) .. alt-'

	Maximize................... Super + Up
	Unmaximize ................ Super + Down
	Fit half the screen ....... Super + Left/Right
    zoon in and out ........... Alt + Super + 8

	Show messages ............. Super + M
	JavaScript console ........ type 'lg' in the Alt+F2 prompt (Esc) to exit
	Screen Recorder ........... Shift+Ctrl+Alt+r

	Taking a screenshot
	+----------------------------------------------------------------------------+
    | Take a screenshot                                      | PrintScr          |
    | Take a screenshot of a window                          | Alt+Print         |
    | Take a screenshot of a region                          | Shift+Print       |
    | Take a screenshot and paste in a clipboard             | Ctrl+Print        |
    | Take a screenshot of a window and paste in a clipboard | Ctrl+Alt+Print    |
    | Take a screenshot of a region and paste in a clipboard | Ctrl+Shift+Print  |
	+----------------------------------------------------------------------------+
	+----------------------------------------------------------------------------+
    | Past from primary selection                            | Shift + Insert    |
	+----------------------------------------------------------------------------+
```

## Am I using wayland or X11?
+ https://stackoverflow.com/a/45536186/2571881
+ https://askubuntu.com/a/929678/3798

    echo $XDG_SESSION_TYPE
    env | grep -i wayland

## gnome-terminal Error: Locale not supported by C library
+ You should enable the locale in /etc/locale.gen and run locale-gen.

    You should enable the locale in /etc/locale.gen and run locale-gen.

## Log off shortcut
You can define a new shortcut with

		gnome-session-quit --logout --no-prompt

## keep your screen alive
+ https://extensions.gnome.org/extension/517/caffeine/

    Disable the screensaver and auto suspend. If you’re sick of having to
    unlock your Gnome desktop after leaving for 10 minutes, this extension is
    for you.

## gnome-shell-extension-updater
+ https://github.com/eonpatapon/gnome-shell-extension-updater

## tiling windows on gnome-shell using popos extension:
+ https://github.com/pop-os/shell
+ https://www.youtube.com/watch?v=UUrvF0Y9AUg

- Dependencies:

    gnome-shell, gnu-make, typescript

    doas npm install --location=global typescript

Shotcuts:

    super + y ................ toggle tiling
    super + arrows ........... jump to other windows
    Super + o ................ change orientation
    Super + g ................ toggle float window
    Super + m ................ toggle maximize
    Super + q ................ close window


    super + enter ............ Enter window adjustment mode
    control + direction keys   swap while adjustment mode
    Esc ...................... cancel adjustment mode
    shift + arrow ............ resize
    arrow .................... move window

## primary selection and wayland

```sh
[[ ! -d ~/.config/gtk-3.0 ]] || mkdir ~/.config/gtk-3.0/
if [[ ! -f ~/.config/gtk-3.0/settings.ini ]]; then
cat <<-EOF > ~/.config/gtk-3.0/settings.ini
	[Settings]
	gtk-enable-primary-paste=true
EOF
```

## Fixing dir names
+ https://askubuntu.com/a/737236/3798

Set enabled=True in /etc/xdg/user-dirs.conf

    File: ~/.config/user-dirs.dirs

``` bash
setdirs () {
cd
for i in Desktop Downloads templates public docs music img videos; do
    mkdir $i
done

cat <<-EOF ~/.config/user-dirs.dirs
# File ~/.config/user-dirs.dirs
XDG_DESKTOP_DIR="$HOME/Desktop"
XDG_DOWNLOAD_DIR="$HOME/Downloads"
XDG_TEMPLATES_DIR="$HOME/templates"
XDG_DOCUMENTS_DIR="$HOME/docs"
XDG_PICTURES_DIR="$HOME/img"
XDG_VIDEOS_DIR="$HOME/videos"
XDG_PUBLICSHARE_DIR="$HOME/share"
XDG_MUSIC_DIR="$HOME/music"
EOF

xdg-user-dirs-update --set DESKTOP "$HOME/Desktop"
xdg-user-dirs-update --set DOWNLOAD "$HOME/Downloads"
xdg-user-dirs-update --set TEMPLATES "$HOME/templates"
xdg-user-dirs-update --set DOCUMENTS "$HOME/docs"
xdg-user-dirs-update --set PICTURES "$HOME/img"
xdg-user-dirs-update --set VIDEOS "$HOME/videos"
xdg-user-dirs-update --set PUBLICSHARE "$HOME/share"
xdg-user-dirs-update --set MUSIC "$HOME/music"

xdg-user-dirs-update
xdg-user-dirs-gtk-update
} && setdirs
```

## set system sound above 100%
+ https://superuser.com/a/1017972/45032
install pulseaudio

on debian like for example

    pactl -- set-sink-volume 0 150%

## Cleaning up Linux journal
+ https://cubethethird.wordpress.com/2016/05/10/cleaning-up-linux-journal/

While reading up on a few random things, I stumbled across info on the Arch
Wiki about how the system journal archives can get quite large, and may cause
slowdowns. This is caused by a lack of limit in place for the size of journals
stored. To my surprise, I had nearly 4GB of space taken up by system journals
in /var/log/journal. I ran the following command to clear that up:

    journalctl --vacuum-size=100M

This brought the size down to approximately 100MB. Along with this, I’ve set a
limit to the journals, by editing the /etc/systemd/journald.conf:

    SystemMaxUse=100M

## How do I know if I am using wayland or x11

The `XDG_SESSION_TYPE` environmental variable takes precedence. One of "
unspecified ", " tty ", " x11 ", " wayland " or " mir "

## disable lockdown screen
+ https://askubuntu.com/q/675256/3798

This option only prevents alowing user to lock screen manually

    gsettings set org.gnome.desktop.lockdown disable-lock-screen true

avoid lock on suspending desktop

    gsettings set org.gnome.desktop.screensaver ubuntu-lock-on-suspend false

## delay until screen goes black

    gsettings get org.gnome.desktop.session idle-delay
    gsettings set org.gnome.desktop.session idle-delay 600

## click to minimize

		gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'

## Night mode settings
+ http://www.omgubuntu.co.uk/2017/07/adjust-color-temperature-gnome-night-light

    gsettings set org.gnome.settings-daemon.plugins.color night-light-enabled true
    gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-from 17.00
    gsettings set org.gnome.settings-daemon.plugins.color night-light-schedule-to 7.50

## gsettings collection

    gsettings set org.gnome.nautilus.preferences sort-directories-first true

## Startap applications

rode o comando (Alt+F2) dialog run:

    gnome-session-properties

## How can I delay a specific program on startup?
+ https://askubuntu.com/a/195036/3798

    gedit ~/.config/autostart/application.desktop

Append the following line to the file:

    X-GNOME-Autostart-Delay=foo

where foo is the time in seconds you want to delay the application launch by, e.g.:

    X-GNOME-Autostart-Delay=60

    [Desktop Entry]
    Encoding=UTF-8
    Type=Application
    Name=Conky
    Name[en_US]=Conky
    Exec=/usr/bin/conky
    Comment[en_US]=
    StartupNotify=true
    X-GNOME-Autostart-Delay=60

## how can I create an autostart script on gnome-shell?
+ https://superuser.com/a/472399/45032

So basically, as nodiscc suggested, create a desktop launcher:
`~/.config/autostart/script.desktop` with the following contents:

``` markdown
cat <<-EOF > ~/.config/autostart/script.desktop
[Desktop Entry]
Type=Application
Name=Autostart Script
Exec=~/.dotfiles/bin/autostart
Icon=system-run
X-GNOME-Autostart-enabled=true
EOF
```

Then create the autostart script: `~/bin/autostart` with your bash contents:

    #!/bin/bash
    # Execute bash script below
    synclient TapButton1=1 TapButton2=3 TapButton3=2

Make sure that `~/bin/autostart` is executable

In my case I wanted to fix a touchpad feature called tap to click so I have
created the following shortcut:

cat <<-EOF > ~/.config/autostart/autostart.desktop
# ~/.config/autostart/autostart.desktop
[Desktop Entry]
Type=Application
Terminal=true
Name=fix-tap-to-click
Icon=/home/sergio/img/icons/terminal.svg
Exec=/home/sergio/.dotfiles/bin/autostart
EOF

## ocs-url
+ https://www.linux-apps.com/p/1175480/

It is a protocol similar to apt-url that allows us to
install new gnome-shell themes easily

## theme list

+ united theme: https://www.gnome-look.org/p/1174889/
+ zuki: https://github.com/lassekongo83/zuki-themes

## Backing up gnome-shell extensions

    rsync -av --update --delete ~/.local/share/gnome-shell/extensions/ ~/docs/conf/gnome-shell/extensions

## Enabbling extensions

    gnome-extensions enable material-shell@papyelgringo

## Enabling extensions through command line
+ dash to dock: https://extensions.gnome.org/extension/307/dash-to-dock/
+ blur : https://extensions.gnome.org/extension/1251/blyr/
+ workspaces indicator: https://extensions.gnome.org/extension/21/workspace-indicator/
+ top pannel workspace scroll: https://extensions.gnome.org/extension/701/top-panel-workspace-scroll/

Manual instalation of dash-to-dock:
+ https://micheleg.github.io/dash-to-dock/download.html#installfromsource

OBS: Don't forget to restart Alt-r r

Get your gnome-shell version

    gnome-shell --version

Dash-to-dock version:

    https://micheleg.github.io/dash-to-dock/releases.html

Download the extension from the site, like "dash-to-dock"
after that unzip you need to get the `uuid` in order to do that
unzip the file and run:

    EUUID=$(awk -F'"' '/uuid/ {print $4}' metadata.json)

Then use this result as the extension folder name

    FULLNAME="~/.local/share/gnome-shell/$(EUUID)"

    mkdir -p ${FULLNAME}

cd ~/dowload

    wget https://extensions.gnome.org/review/download/9456.shell-extension.zip

Now it is time to Unzip Gnome extension into previously created directory:

    unzip -q downloads/9456.shell-extensions.zip -d ${FULLNAME}

With "-d" you will disable any extension

Oh, and you can get the names of all your locally installed extensions by doing

    ls ~/.local/share/gnome-shell/extensions

org.gnome.shell enabled-extensions ['workspace-indicator@gnome-shell-extensions.gcampax.github.com', 'dash-to-dock@micxgx.gmail.com', 'gravatar@jr.rlabs.io', 'user-theme@gnome-shell-extensions.gcampax.github.com', 'activities-config@nls1729', 'CoverflowAltTab@palatis.blogspot.com', 'applications-overview-tooltip@RaphaelRochet', 'launch-new-instance@gnome-shell-extensions.gcampax.github.com', 'remove-contacts-search@busaster.gmail.com', 'impatience@gfxmonk.net', 'quicklists@damianbasic.gmail.com', 'backslide@codeisland.org', 'mediaplayer@patapon.info']

## save alsamixer settings
source: http://askubuntu.com/a/465641/3798

    alsactl --file ~/.config/asound.state store

    reloading:

    alsactl --file ~/.config/asound.state restore

    vim ~/.config/autostart/alsarestore.desktop

    Entries in ~/.config/autostart/ directory are used to autostart programs
    and services for specific user on startup/graphical login.

    The contents of the .desktop file should be the following:

    [Desktop Entry]
    Type=Application
    Terminal=false
    Name=alsarestore
    Exec=alsactl --file ~/.config/asound.state restore

    Among other things, you could store your config in /etc/asound.state and
    symlink it to /var/lib/alsa/asound.state, but this one is more of a suggestion
    rather than tested solution

    Another user of askubuntu posted this:
    Obs: The top answer didn't work for me but this did! I created a config
    file and placed it at /etc/asound.state and added the reload line to my
    /etc/rc.local

    The first sugestion I got was this:

    sudo alsactl store

    This should save alsamixer configurations to /etc/asound.state which gets
    loaded every startup.

    another attempt:

    After 2 months of trying to make "sudo alsactl store" to work, I finally
    managed to do it. Firstly type in terminal "alsamixer" to enter the
    alsamixer UI. Then make the configurations you need(e.g increase
    speakers/headphones level or unmute something pressing "m" on keyboard).
    Now the most important part. Before you exit alsamixer, open a new terminal
    and do : "sudo su" to get high privileges (Be very careful with commands
    you use in "sudo su" mode because you may destroy your system) and then do
    "alsactl store" to save alsa settings. Then close both terminals and
    restart your computer. This will do the job.

## how to set languages shortcuts from config files or command line
http://askubuntu.com/questions/604462/

    gsettings set org.gnome.desktop.interface clock-show-date true

   # do not show menu on terminal

   gsettings set org.gnome.Terminal.Legacy.Settings default-show-menubar false

   gsettings set org.gnome.desktop.datetime automatic-timezone true

## cleaning cached thumbnails

    find .cache/thumbnails -type f | xargs rm -f

## Increasing the sound through command line

    pactl set-sink-volume 0 +10%

    And to make the volume go down by the same amount, the command would be:

    pactl set-sink-volume 0 -- -10%

## touche to click on touchpad
+ https://askubuntu.com/a/700018/3798

    gsettings set org.gnome.desktop.peripherals.touchpad tap-to-click true

My new solution to this issue

    synclient TapButton1=1 TapButton2=3 TapButton3=2

## Set keyboard language

    gsettings set org.gnome.desktop.input-sources sources "[('xkb', 'br'), ('xkb', 'us')]"

## Disable update checking for gnome extensions

    gsettings set org.gnome.shell disable-extension-version-validation "true"

## change wallpaper every so many minutes
+ https://extensions.gnome.org/extension/543/backslide/

http://thornton2.com/How_To_Shuffle_Your_Desktop_Background_Every_So_Many_Minutes_in_GNOME_and_MATE

## Dash to pannel
+ https://extensions.gnome.org/extension/1160/dash-to-panel/

An icon taskbar for the Gnome Shell. This extension moves the dash into the
gnome main panel so that the application launchers and system tray are
combined into a single panel, similar to that found in KDE Plasma and Windows
7+. A separate dock is no longer needed for easy access to running and
favorited applications.

## Amazing site to get wallpapers
+ http://sf.co.ua/

##!/bin/sh
echo $DBUS_SESSION_BUS_ADDRESS > $HOME/.dbus_session_bus_address

(You can name the file in the echo command whatever you want, but it's a dot-file in order to keep it hidden from ordinary listings.)

Run the echo command from a shell (or from the Run prompt accessed by Alt+F2) so that you don't have to log out for the shuffling to start.  Otherwise, just log out and log in again after you follow all the rest of these instructions.

Change the file permissions to executable.  To do that in a shell, use the command:

chmod a+x ~/bin/login-script.sh

## dentro de .config/autostart coloque

    [Desktop Entry]
    Name=MyScript
    GenericName=A descriptive name
    Comment=Some description about your script
    Exec=/path/to/my/script.sh
    Terminal=false
    Type=Application
    X-GNOME-Autostart-enabled=true

    [Desktop Entry]
    Name=Conky
    GenericName=background monitor
    Comment=Keep a background eye on your system
    Exec=conky &
    Terminal=false
    Type=Application
    Icon=conky
    Categories=System;
    StartupNotify=false
    Name[en_US]=conky
    X-GNOME-Autostart-Delay=80

ou rode o comando (Alt+F2) dialog run:

    gnome-session-properties

## Screen zooming in
+ https://unix.stackexchange.com/a/192896/3157

    Super+Alt+8 : Toggle zoom enabled/disabled (when enabled, the next two keyboard shortcuts become active)
    Super+Alt+'+' : Zoom in (increases zoom factor by 1.0)
    Super+Alt+'-' : Zoom out (decreases zoom factor by 1.0, until it is 1.0)

## Set wallpaper mode
+ https://bbs.archlinux.org/viewtopic.php?id=152571
Possible values are "none", "wallpaper", "centered", "scaled", "stretched", "zoom", "spanned".

    gsettings set org.gnome.desktop.background picture-options 'zoom'

## set scalling - How do I change the font DPI settings? (zoom | scale)
+ https://askubuntu.com/questions/60044/

    gsettings set org.gnome.desktop.interface text-scaling-factor 0.9

## Show current wallpaper path

    gsettings get org.gnome.desktop.background picture-uri

    Script to change wallpaper automatically
    https://github.com/mattwilmott/Gnome3-Auto-Wallpaper-Changer

    Using notify send system:
    notify-send -i "terminal" "current-background" "$(gsettings get org.gnome.desktop.background picture-uri)"

Using notify-send to warn the user when a job is finished

    notify-send "Task Completed"

## Screenshot directory

    gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/img/Screenshots"

## setting nautilus auto mount

    gsettings set org.gnome.desktop.media-handling automount true

## Some nautilus shortcuts

    Ctrl+Enter ......... open in a new tab
    Ctrl+t ............. open a new tab
    Ctrl+Shift+t ....... restores tab
    Ctrl+PageUp ........ goes to previous tab
    Ctrl+h ............. show hidden files
    Ctrl+1 ............. List view
    Ctrl+Shift+i ....... invert selection
    Ctrl+s ............. selects pattern

## fixing audacious open folders error

I don't know how it happened but
when, eg, I plug in a USB drive or try to open a folder in a firefox
extension (or other programs that want to open a folder...), it always
wants to open with Audacious which is obnoxious.  Browsing through in
Nautilus works fine and normal, but I want to know if I can find out how
to get rid of the association with Audacious.

    I've tried deleting ~/.config and ~/.local to no help...

    Create the file

    ~/.local/share/applications/mimeapps.list

    and fill it with

    [Default Applications]
    inode/directory=nautilus.desktop;

    xdg-settings set default-web-browser firefox.desktop

``` sh
cat <<-EOF ~/.local/share/applications/mimeapps.list
[Default Applications]
inode/directory=nautilus.desktop;
text/html=firefox.desktop;
EOF
```

## setting gnome screenshot dir

  gsettings set org.gnome.gnome-screenshot auto-save-directory "file:///home/$USER/pictures/"

## setando o muse para ser destacado com o ctrl ou não

  gsettings set org.gnome.settings-daemon.peripherals.mouse locate-pointer false

## habilitando o click to tap no mouse

    gsettings set org.gnome.settings-daemon.peripherals.touchpad tap-to-click true
    gsettings set org.gnome.desktop.peripherals-touchpad disable-while-typing true
    gconftool-2 --toggle /desktop/gnome/peripherals/touchpad/touchpad-enabled
    habilitar a rolagem pela borda to touchpad

    gsettings set org.gnome.desktop.peripherals.touchpad scroll-method "edge-scrolling"

## Blinking mouse time
+ https://askubuntu.com/a/485489/3798

    gsettings set org.gnome.desktop.interface cursor-blink-time 800

## accessing ssh throug nautilus

    sftp://user@address/folder/
    ssh://admin:admin@192.168.1.3:2222

## Alterando o tempo de gravação no gnome shell

fonte: [ampliando o tempo de gravação no gnome-shell](http://www.talesam.org/blog/dica-ampliando-o-tempo-de-gravacao-do-desktop-no-gnome-shell/)
    gsettings set org.gnome.settings-daemon.plugins.media-keys max-screencast-length 1800

## Extensão que permite gravar com audio
+ https://extensions.gnome.org/extension/690/easyscreencast/

## Extensão pra liberar espaço na tela
+ https://extensions.gnome.org/extension/723/pixel-saver/

## Extensão que reproduz sons como chuva, água etc
+ https://extensions.gnome.org/extension/1224/focusli/

mprove focus and increase your productive by listening to different sounds

## Extension to set blur effect
+ https://extensions.gnome.org/extension/1251/blyr/
+ https://github.com/yozoon/gnome-shell-extension-blyr

    git clone git@github.com:yozoon/gnome-shell-extension-blyr.git
    cd gnome-shell-extension-blyr/
    cp -r blyr@yozoon.dev.gmail.com ~/.local/share/gnome-shell/extensions

## Media Player indicator extension
+ https://extensions.gnome.org/extension/55/media-player-indicator/

This extension is ideal to combine with Anoise program
+ http://anoise.tuxfamily.org/

## how install gnome shell in any distro with jhbuild
http://worldofgnome.org/how-to-easily-install-the-very-latest-gnome-in-any-distro-with-jhbuild/

## performance tweaks  * https://wiki.archlinux.org/index.php/Sysctl *
http://penguininside.blogspot.com.br/2009/08/top-10-gnome-performance-tweaks.html
https://alexcabal.com/disabling-gnomes-recently-used-file-list-the-better-way/

-------------------------------
 disable most recent files view --> recently-used-xbel
-------------------------------

Put the following in ~/.config/gtk-3.0/settings.ini (create the file if
it doesn’t exist):

        [Settings]
        gtk-recent-files-max-age=0
        gtk-recent-files-limit=0

    Then, remove the file holding recently-used data:

        rm ~/.local/share/recently-used.xbel

Disabling tracer system

    tracker-control

## other stuff

Put this in your /etc/environment

    BROWSER=/usr/bin/firefox
    CLUTTER_PAINT=disable-clipped-redraws:disable-culling
    export CLUTTER_VBLANK=none

Network performance

https://raw.githubusercontent.com/voyeg3r/dotfiles/master/bin/sysctrl.conf

If you had customized /etc/sysctl.conf, you need to rename it as
/etc/sysctl.d/99-sysctl.conf. If you had e.g. /etc/sysctl.d/foo, you
need to rename is to /etc/sysctl.d/foo.conf

## solving the problem of hidden files by default

 * source: http://ubuntuforums.org/archive/index.php/t-2133298.html

	open terminal and type:
	dconf-editor
	Enter and then:
	org->gtk->settings->file-chooser
	uncheck show-hidden

	Have a nice day!

## how make a screencast on gnome-shell

  Ctrl+Shift+Alt+r

## to reset default value to video record resource

    gsettings reset org.gnome.shell.recorder pipeline

## text expander for linux

https://bbs.archlinux.org/viewtopic.php?id=71938
https://github.com/Dieterbe/snip/
https://github.com/Dieterbe/snip/tree/master

## best addons to gnome-shell
+ https://linuxious.com/2016/01/10/my-recommended-gnome-extensions/

## Dinamic Panel Transparency addon
+ https://extensions.gnome.org/extension/1011/dynamic-panel-transparency/

## gmail notifications extension
+ https://extensions.gnome.org/extension/1230/gmail-message-tray/

## Replacement of Alt-Tab, iterates through windows in a cover-flow manner.
+ https://extensions.gnome.org/extension/97/coverflow-alt-tab/

## Extensão para trocar papel de parede automaticamente
+ https://extensions.gnome.org/extension/543/backslide/

## Readme sobre a extensão acima
+ https://bitbucket.org/LukasKnuth/backslide

## Set your gravatar icon
+ https://extensions.gnome.org/extension/1015/gravatar/

## Extensão para rolar workspaces automaticamente (scroll)
+ https://extensions.gnome.org/extension/701/top-panel-workspace-scroll/

## Extensão para adicionar transparência a barra do topo
+ https://extensions.gnome.org/extension/857/transparent-top-bar/
+ https://extensions.gnome.org/extension/885/dynamic-top-bar/

## Extensão que adiciona um menu tipo windows 10
+ https://extensions.gnome.org/extension/1228/arc-menu/

## Extensão para mostrar um tooltip sobre a aplicação
+ https://extensions.gnome.org/extension/1071/applications-overview-tooltip/

## Disable confirmation window when closing Terminal

The Terminal will always display a confirmation window when trying to close
the window while one is logged in as root. To avoid this, execute the
following:

    gsettings set org.gnome.Terminal.Legacy.Settings confirm-close false

## Drag window
+ https://superuser.com/a/297919/45032

    gsettings set org.gnome.desktop.wm.preferences mouse-button-modifier "'<Super>'"

## How many workspaces

    gsettings set org.gnome.shell.overrides dynamic-workspaces true
    gsettings set org.gnome.desktop.wm.preferences num-workspaces 4

## shortcut to change workspaces
+ http://jeffbastian.blogspot.com/2012/06/static-workspaces-and-keyboard.html

    gsettings set org.gnome.desktop.wm.keybindings switch-to-workspace-1
    "[\"<Control>F1\"]"

    setworkspaces (){
        for ((x=1 ; x <= 5 ; x++)) ; do
        echo "Setting hotkeys for workspace $x"
        gsettings set org.gnome.desktop.wm.keybindings \
            switch-to-workspace-$x "[\"<Control>F$x\"]"
        gsettings set org.gnome.desktop.wm.keybindings \
            move-to-workspace-$x "[\"<Control><Shift>F$x\"]"
        done
    }

    echo "Verify Settings:"
    gsettings list-recursively org.gnome.shell.overrides | grep dynamic-workspaces
    gsettings list-recursively org.gnome.desktop.wm.preferences | grep num-workspaces
    gsettings list-recursively org.gnome.desktop.wm.keybindings | grep to-workspace

## como instalar o gnome-shell no ubuntu 11.04

    sudo add-apt-repository ppa:gnome3-team/gnome3
    sudo apt-get update
    sudo apt-get dist-upgrade
    sudo apt-get install gnome-shell

## Como abrir o menu Ativities?

  Basta pressionar a tecla windows

## block adds on gnome web (ublock addblock)
+ https://askubuntu.com/questions/470040/

## best gnome shell addons
https://extensions.gnome.org/extension/549/web-search-dialog/
https://extensions.gnome.org/extension/277/impatience/
