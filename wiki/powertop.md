---
file: powertop.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


## Powertop
+ https://mythreed.com/posts/2017-10-26-tweaking-void-pt2/

Powertop is a utility made by Intel to help tweak your system to reduce power
consumption and increase battery life. It shows a lot of detailed stats and
helps you tweak your system to remove wasteful resources. While this can be
quite complicated Powertop can be fairly automated using the auto tune
parameter

    sudo powertop --auto-tune

However these values are unlikely to change, so we can have powertop output
these tweaks and load them into our rc.local.

    powertop --html=power.html
    awk -F '</?td ?>' '/tune/ { print $4 }' power.html > tweaks
    sudo su -c 'cat tweaks >> /etc/rc.local'

Our tweaked parameters should now load every boot without having to run powertop. Void Linux makes this very easy, as other systems requires us to create various udev rules.
