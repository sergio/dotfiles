---
file: gpasswd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## gpasswd -a seu_usuario nome_do_grupo
## EXEMPLO: #
gpasswd -a phillipe storage

