---
File: /home/sergio/.dotfiles/wiki/xprop.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [tools, desktop, x11]
 vim:ft=markdown
---

## xprop - property displayer for X

Get the app name:

The xprop utility is for displaying window and font properties
in an X server.  One window or font is selected using the
command line arguments or possibly in the case of a window, by
clicking on the desired window.  A list of properties is then
given, possibly with formatting information.

    xprop | awk '/WM_CLASS/ {print $4}'


    xprop | awk ' /^WM_CLASS/{sub(/.* =/, "instance:"); sub(/,/, "\nclass:"); print}
            /^WM_NAME/{sub(/.* =/, "title:"); print}'

