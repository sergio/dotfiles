---
file: pkexec.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# pkexec

pkexec - Execute a command as another user

    pkexec='pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY'


