Filename: ghostscript.md
Last Change: Thu, 24 Nov 2022 - 11:02:31
tags: [pdf, tools, inkscape, cmyk]

 vim:ft=markdown
## Converting PDF to CMYK
+ https://stackoverflow.com/a/8567524/2571881
+ https://www.devmedia.com.br/manipulando-pdf-com-ghostscript/34424

```sh
 gs \
   -o test-cmyk.pdf \
   -sDEVICE=pdfwrite \
   -dEncodeColorImages=false \
   -sProcessColorModel=DeviceCMYK \
   -sColorConversionStrategy=CMYK \
   -sColorConversionStrategyForImages=CMYK \
    test.pdf
```

```sh
gs -o outputfile-cmyk.pdf -sDEVICE=pdfwrite -sProcessColorModel=DeviceCMYK -sColorConversionStrategy=CMYK -sColorConversionStrategyForImages=CMYK -dOverrideICC=true inputfile-rbg.pdf
```

## Como criar um documento PDF/X-3 com o Ghostscript
+ https://meiradarocha.jor.br/news/2012/08/25/como-criar-um-documento-pdfx-3-com-o-ghostscript/

    gs -dPDFX -dBATCH -dNOPAUSE -dNOOUTERSAVE -dUseCIEColor -sDEVICE=pdfwrite -sOutputFile=out-x3.pdf PDFX_def.ps input.ps
