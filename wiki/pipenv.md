---
File: pipenv.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: abr 15, 2020 - 09:21
tags: [python, tools]
---

# pipenv.md intro

+ <https://www.youtube.com/watch?v=GBQAKldqgZs>

    sudo xbps-install -Sy python3-pip python-pip
    sudo pip install pipenv
    pipenv --help

# list all installed packages

```vim
pip freeze
```

or

```vim
pipenv graph
```

## list all dependencies from the current project

```vim
pipenv lock -r
```

## lock your pipenv files

Update your dependencies, package versions to the Pipenv.lock

```vim
pipenv lock
```

This updates your Pipefile.lock so you can use your lock file
to control packages versions and dependencies.

```vim
pipenve install --ignore-pipfile
```

This creates an invironment with you Pipfile.lock as a reference

## set specific environment variables to your virtualenv

    cat .env <<-EOF
    SECRET_KEY="MySuperSecretKey"
    EOF

Now we can run:

```vim
pipenv run python
```

And it will say:

    Loading .env environment variables

    import os
    os.environment['SECRET_KEY']
    MySuperSecretKey

## Create a virtual environmente

based on the current version of python on the current directory

    pipenv --three

## Enter in the just created virtual environmente

    pipenv shell

## Show the path of your virtual environment

    pipenv --venv

## Run a command from the virtual environment without entering it

    pipenv run django-admin

## Install a package into the new environmente

    pipenv install flask
    pipenv install "requests>=1.4"

## update libriries

    pipenv update

## Delete current virtual environment

    exit
    pipenv --rm

## install all dependencies

    pipenv install

If we run `pipenv install` and we still have the `Pipfile` the new
environment will instal its dependencies.

## removing an specifi package

    pipenv uninstall requests

## unistall all orphan packages

    pipenv clean

## Creating "requirements.txt"

    flashk==1.0.3
    requests==1.4

    pipenv install -r requirements.txt
