---
file: vim-unimpaired.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---


# https://github.com/tpope/vim-unimpaired

pairs of handy bracket mappings

## Nice shortcuts

    [<space> ....... add new line before
    ]<space> ....... add new line after
    [e ............. exchange with preceding line
    ]e ............. exchange with next line
    ]a ............. next arg

tags: vim, nvim
