File: /home/sergio/.dotfiles/wiki/sobrevivencialismo.md
Last Change: Fri, 06 Sep 2024 - 09:54:43

## Lanternas:

+ https://www.youtube.com/watch?v=2kTfIzWEh4E (essa lanterna atinge 800 metros)
usa led v3 - Cupom de desconto: MUNDOEDC
https://www.tiochicoshop.com.br/produtos/lanterna-tatica-jws-led-v3-militar-longo-alcance-800-metros/

## Impermeabilizar tecidos

[óleo mineral e parafina](https://youtu.be/HedRbIsM75M)
[Solvente + silicone](https://youtu.be/z_R0gEDZhAI)
[Outro bom exemplo](https://youtu.be/z_R0gEDZhAI)

No vídeo acima ele fala de um solvente "naphtha" (nafta) um litro e meio para um
bastão de silicone.

Para uma mochila o cara recomenda meio a meio ( solvente + silicone) e aplicar
com pincel.

Meio litro de óleo mineral por kilo de parafina

Para impermeabilizar tecidos, a proporção mais comum de solvente para silicone é de **1 parte de silicone para 10 partes de solvente**. Essa proporção pode variar dependendo da porosidade do tecido e do efeito de impermeabilização desejado.

### Tipo de Silicone Ideal

O silicone ideal para impermeabilização de tecidos é o **silicone líquido** ou **silicone em emulsão**. Esse tipo de silicone é fácil de misturar com o solvente e de aplicar no tecido. O **silicone à base de água** também pode ser utilizado, principalmente para tecidos mais delicados, pois ele tem menos impacto no toque e na cor do tecido.

### Solvente

O solvente mais comum para diluir o silicone é o **solvente mineral** (aguarrás ou white spirit). Ele evapora rapidamente, deixando apenas o silicone no tecido, o que resulta em uma camada uniforme e durável.

### Instruções Gerais:

1. **Mistura**: Misture o silicone com o solvente na proporção desejada, geralmente 1:10.
2. **Aplicação**: Aplique a mistura no tecido com um pincel, rolo ou pulverizador, garantindo uma cobertura uniforme.
3. **Secagem**: Deixe o tecido secar completamente, de preferência ao ar livre e em local bem ventilado.

Essa mistura cria uma barreira impermeável no tecido, protegendo contra água e outros líquidos.
