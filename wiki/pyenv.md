---
Filename: pyenv.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [python, dev, tools]
 vim:ft=markdown
---

## para o pyenv
+ https://realpython.com/lessons/installing-pyenv/
+ https://www.youtube.com/watch?v=31WU0Dhw4sk
+ https://king.host/wiki/artigo/como-utilizar-virtualenv-no-python/

    doas -Sy xbps-install base-devel bzip2-devel openssl openssl-devel readline
    readline-devel sqlite-devel xz zlib zlib-devel

Install pyenv:

    curl https://pyenv.run | bash
    curl https://pyenv.run | zsh

## Commands:

```python
pyenv
pyenv-virtualenv
pyenv-update
pyenv-doctor
pyenv-which-ext


pyenv install --list | grep -i '3\.[89]'

pyenv install -v 3.9.13
pyenv virtualenv 3.9.13 neovim
pyenv activate neovim

❯ pyenv versions
* system (set by /home/sergio/.pyenv/version)
 3.9.1

 # set a versrion for every new shell
 sergio@void 13:15 ~  pyenv global 3.9.1

pip install virtualenv --user
mkdir $HOME/.virtualenvs
virtualenv $HOME/.virtualenvs/neovim
source  $HOME/.virtualenvs/neovim/bin/activate
```

## Como sair do virtualenv?

    deactivate

Creiei um aliás para acessar o ambiente do neovim no ambiente virtual:

    alias myenv="source  $HOME/.virtualenvs/neovim/bin/activate; cd ~/.config/nvim"

Mas pode ser ainda mais fácil:

    pipx install virtualenvwrapper
    mkvirtualenv myenv
    workon myenv
    deactivate




