---
File: /home/sergio/.dotfiles/wiki/dmenu.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Intro:
+ https://adamsimpson.net/writing/getting-started-with-rofi

Dmenu is a fast and lightweight dynamic menu for X. It reads arbitrary text
from stdin, and creates a menu with one item for each line. The user can then
select an item, through the arrow keys or typing a part of the name, and the
line is printed to stdout.

## Rofi + dmenu

Rofi adds dmenu capability plus lots of other things like selecting open
windows, running ssh commands etc. To enable dmenu mode in Rofi pass it as an
option -dmenu. Let's port our previous example code snippet to Rofi: echo
"foo\nbar" | rofi -dmenu. Easy enough! With the dmenu option Rofi becomes just
as useful as Alfred and makes me think Alfred should gain a command line option
to pop open Alfred from within scripts like dmenu or Rofi.

    sxiv ~/img/$(echo {screenshots,backgrounds} | tr " " "\n"  | rofi -dmenu --window-title sxiv)

## dmenu clipboard:
+ https://github.com/cdown/clipmenu
