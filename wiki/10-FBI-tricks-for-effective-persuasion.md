---
File: ~/.dotfiles/wiki/10-FBI-tricks-for-effective-persuasion.md
Last Change: Mon, 16 Sep 2024 - 18:35:56
tags: [hack]
---

www.linkedin.com

10 FBI Tricks For Effective Persuasion
LaRae Quy

7 - 9 minutes

Some of my biggest laughs come from watching Hollywood movies and TV shows depicting FBI agents as indestructible bullies walking that fine line between good and evil. They are either taking the law into their own hands in their pursuit of justice or abusing their power to crush the little people that get in their way.

The silliness of it all is pure entertainment for someone like me, a former FBI agent. The danger that lurks, however, is that impressionable audiences actually start to believe all the crap they see, read, and hear.

Bullying, intimidation, and rudeness pump up hormones and get everyone’s juices flowing. The results are good ratings.

    The fact is, FBI agents use persuasion to get the job done in the majority of cases—not brute strength and ignorance.

As sales people, executives, and leaders, you deserve to know the truth—persuasion is a skill that is as instrumental to your success as it is to an FBI agent.

Persuasion, at it’s core, requires emotional intelligence because it is essential that you have enough awareness of emotions to develop rapport with another individual.

Emotional awareness is an essential component of mental toughness, because if you aren’t savvy enough to read other people, you will never be able to adapt your own approach to accommodate different personalities.

There are a few tricks of the trade, and here are 10 that will help you to get people to lean toward your way of thinking when it matters most:
1. LEAVE A STRONG FIRST IMPRESSION

There is a reason FBI agents wear suits and workout every day. They portray the image of someone who is both professional and capable of handling themselves in every situation.

    Most people make snap decisions within the first few seconds of meeting you. They then spend the rest of the conversation justifying their first impression.

Take advantage by paying attention to your appearance, posture, voice, and mannerisms. The secret weapon is likability and it can make a huge impact on your success.
2. GREET PEOPLE BY NAME

Dale Carnegie, the author of How to Win Friends and Influence People, believed that using someone’s name was very important. He said that a person’s name is the sweetest sound in any language for that person.

Research shows that people feel validated when they are referred to by their name—just don’t over do it!

Personalize your interactions with others by using their name. Not everyone is good at remembering names, so you may need to collect business cards and make notes on the back of them to help jog your recollection.
3. TILT YOUR HEAD

Common sense tells us that if you nod when you’re listening to someone, it indicates that you are in agreement.

But, when you tilt your head and nod, you are sending a more powerful non-verbal message that indicates you are listening, comfortable, and receptive.
4. LIMIT YOUR SPEECH

To be most effective, talk no longer than 30 seconds at a time in a given conversation.

    According to researcher Andrew Newberg, the human brain can really only hold on to four things at a time, so if you go on and on for five or 10 minutes trying to argue a point, the person will only remember a very small part of that.

Speak briefly, sticking to one or two sentences or around 30 seconds worth, because that’s really what the human brain can take in.
5. MIRROR THEIR BEHAVIOR

Mirroring is observing a person’s body posture and then subtly letting your body reflect their position.

Mirror neurons fire when you reflect an emotion you see in others. Researchers have discovered that those who had been mimicked were much more likely to act favorably toward the person who had copied them.

It’s an effective way to build rapport and increase a person’s comfort level when you need to use persuasion to get your point across.
6. PARAPHRASE AND REPEAT BACK

One of the most positive ways to persuade others is to show them that you truly understand how they feel—even if you disagree with them.

Studies have shown that when you listen to what someone has to say, and then rephrase it as a question to confirm that you understood it, they are going to be more comfortable talking with you.

    FBI agents use this to help them get confessions, but you can use this same trick because people are more likely to listen to what you have to say once you show them that you care about them.

7. SMILE, ALWAYS

It should be no surprise that a smile creates the highest positive emotion—but it has to be a real smile!

In a genuine smile, the lips are drawn toward the cheekbone, eyebrows rise, and pupils dilate to open up. There is no more more powerful persuasion tool in the world, or more disarming, than a genuine smile.
8. DON'T CORRECT PEOPLE WHEN THEY ARE WRONG

Dale Carnegie also pointed out in his famous book that telling someone they are wrong is usually unnecessary and is a catastrophic move if you want to persuade someone to do something for you because it’s an attack on their ego.

    This doesn’t mean you let people off the hook, but I’ve used something called the Ransberger Pivot many times and it’s an effective approach (unless you’re dealing with a nut or a radical in which case nothing will work):

This is the idea: instead of arguing, listen to what they have to say, and then seek to understand how they feel and why. Then you explain the common ground that you share with them, and use that as a starting point to explain your position. This makes them much more likely to listen to what you have to say, and allows you to correct them without them losing face.
9. SAY PLEASE AND THANK YOU

Researcher Robert Cialdini has shown that people respond to politeness! Treat people with respect. By simply adding the phrases “please” and “thank you” when making a request, compliance is much easier to achieve.
10. FLATTERY WORKS

This one may seem obvious at first, but there are some important caveats to it.

For starters, it must be sincere for it to be persuasive.

    Second, remember that we look for cognitive balance by keeping our thoughts and feelings organized in a similar way. So, if you flatter someone who has high self-esteem, they will like you more because you validated how they feel about themselves.

What other persuasion tricks would you add?

© 2015 LaRaeQuy. All rights reserved.

ABOUT THE AUTHOR

LaRae Quy was an FBI undercover and counterintelligence agent for 24 years. She exposed foreign spies and recruited them to work for the U.S. Government. As an FBI agent, she developed the mental toughness to survive in environments of risk, uncertainty, and deception. LaRae is the author of “Secrets of a Strong Mind” and “Mental Toughness for Women Leaders: 52 Tips To Recognize and Utilize Your Greatest Strengths.”

If you'd like to find out if you are mentally tough, get my FREE 45-Question Mental Toughness Assessment.

You can follow me on Twitter

