---
file: ultisnips.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

# intro
+ [github repo](https://github.com/SirVer/ultisnips)

## artigos a respeito:
+ [sharpen your vim with snippets](https://brigade.engineering/sharpen-your-vim-with-snippets-767b693886db#.qq0y0kthk)
+ [generic html snippets](https://medium.com/@shxfee/generic-html-snippets-with-vim-and-ultisnips-8cc369390cb9#.rheutidju)
+ [why snippets](http://fueledbylemons.com/blog/2011/07/27/why-ultisnips/)
+ [d-snippets](https://github.com/kiith-sa/DSnips)
+ https://noahfrederick.com/log/vim-templates-with-ultisnips-and-projectionist

## convert ultisnips to json format
+ https://github.com/erietz/ultisnips-vscode 

This is a simple tool to convert UltiSnips snippets to VsCode/json and conversely (json to Ultisnips)

    npm i -g convert-snippets

Usage

    convert-snippets source_file target_file

Automatically detects source_file format and generates target_file

## Snippets Priority
Sometimes we want to overwrite some snippets or even disable them. In order to
do that we have:

    clearsnippets

    'clearsnippets' removes all snippets with a priority lower than the current
    one. For example, the following cleares all snippets that have priority <= 1,
    even though the example snippet is defined after the 'clearsnippets'.

    ------------------- SNIP -------------------
    priority 1
    clearsnippets

    priority -1
    snippet example "Cleared example"
        This will never be expanded.
    endsnippet
    ------------------- SNAP -------------------

To clear one or more specific snippet, provide the triggers of the snippets as
arguments to the 'clearsnippets' command. The following example will clear the
snippets 'trigger1' and 'trigger2'.

## Usando seleção visual
Considerando que em meu caso eu uso <C-j> para disparar os snippets
eu posso selecionar um trecho, usar <C-j>, digitar `code` e novamente <C-j>

Quando usar o snippt ${VISUAL}

## Inserir texto com underline usando ultisnips

``` markdown
snippet '\bul(ine)?' "underline" r
${1:your text here}
`!p snip.rv = len(t[1])*'-'`
endsnippet
```

##  Para mostrar uma lista das complementações possíveis
pressione no modo insert

		Ctrl-Tab
        Ctrl-Space

## Para editar snippets use o comandos

    :UltisnipsEdit

## Para definir um snippet novo digite

    snip<tab>

## Para expandir um snip digite

    palavra<tab>

## Para pular para o próximo snippet

    Ctrl-j

A variável que define o gatilho é

    :let g:UltiSnipsExpandTrigger = "atalho"

## Para voltar para o snippet anterior

    Ctrl-k

## Snippets espelho (o mesmo nome digitado simultaneamente em dois lugares)

``` markdown
snippet req "require a module" b
let ${1:${VISUAL}} = require('${0:$1}');
endsnippet
```

## Conditional replacements

One feature that I didn't mention in the screencast are conditional
replacements. When you capture a group in the search part of a
replacement, you can check if it was matched in the replacement like
so: (?<number>:yes text:no text). yes text will be inserted if the
group was matched, otherwise no text will be inserted. A quick
example:

``` markdown
snippet cond
${1:some_text}${1/(o)|(t)|..*/(?1:ne)(?2:wo)/}
endsnippet
```

The transformation will match a o at the beginning into group 1 or a t
at the beginning in group 2. The rest of the search string just
matches everything of the first tabstop. The replacement will either
be ne when a o was matched, wo when a t was matched or an empty
string. That means that you can just type a t to quickly get two as
the complete content of the snippet or a o to get one. If you type
anything else, you only get the content of the place holder, i.e. the
verbatim of what you typed.

##  how could add literal string in vim snippet?
[source here:](http://stackoverflow.com/q/15017031/)

``` markdown
snippet code
    `repeat(nr2char(96),3)` ${1:markdown}

    `repeat(nr2char(96),3)`
```
## Use two line VISUAL selection and leave just the first word of each one
+ https://stackoverflow.com/questions/61091269

    snippet test "Description" w
    `!p
    import re
    snip.rv = re.sub("(^\w+).*", r"\1", snip.v.text, flags=re.MULTILINE)
    `
    endsnippet

## Function that was created colaborativelly to cacth a function name
+ https://vi.stackexchange.com/a/19347/7339

I was improving my init.vim by adding some 'guard' to some funtions

``` vim
snippet guard "add guard to functions" b
if !exists('*`!p
try:
    func_name = re.search('\S+\s+(\S+)\(', snip.v.text.splitlines()[0]).group(1)
except AttributeError:
    func_name = ''
snip.rv = func_name
`')
    ${VISUAL}
endif
${0:jump here <C-j>}
endsnippet
```

The explanation:

The `!p snip.rv = ...` part is a python interpolation. The evaluation of the expression to the right of the assignment operator after snip.rv replaces everything between the backticks.

Here the expression is:

    re.search('\S+\s+(\S+)\(', snip.v.text.splitlines()[0]).group(1)

    snip.v.text allows you to refer to the last visually-selected text in a python interpolation.
    snip.v.text.strip() --> use to remove a char

    .splitlines() allows you to split the lines of the visual selection in a list.

    snip.v.text.splitlines()[0] grabs the first line in the selection.
    re.search('\S+\s+(\S+)\(', ...).group(1) extracts the function name from this line.

If you make a mistake when selecting the function, the first line may not
match the regex, in which case an error will be raised. Inside the python
interpolation, you can use a try, except construct to catch the error.

