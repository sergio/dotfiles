---
file: pgrep.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
 vim:set ft=markdown nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
---

##  pgrep.md intro:

    pgrep -x mysqld >/dev/null && echo "Process found" || echo "Process not found"

    The `-x` flag makes `pgrep` show just the process ID "pid"

    pgrep -u $UID -f clipmenud

    -f, --full
        The pattern is normally only matched against the
        process name.  When -f is set, the full command line
        is used.

