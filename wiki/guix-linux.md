---
File: guix-linux.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [linux, distros]
---

# Intro
It is a non systemd linux distro

##  signature verification:

Make sure to download the associated .sig file and to verify the authenticity of the image against it, along these lines:

    $ wget https://ftp.gnu.org/gnu/guix/guix-system-install-1.3.0.x86_64-linux.iso.sig
    $ gpg --verify guix-system-install-1.3.0.x86_64-linux.iso.sig

If that command fails because you do not have the required public key, then run this command to import it:

    $ wget https://sv.gnu.org/people/viewgpg.php?user_id=127547 \
        -qO - | gpg --import -

and rerun the gpg --verify command.

Take note that a warning like “This key is not certified with a trusted signature!” is normal.

This image contains the tools necessary for an installation. It is meant to be
copied as is to a large-enough USB stick or DVD. 


## Copying to a USB Stick

Insert a USB stick of 1 GiB or more into your machine, and determine its device name. Assuming that the USB stick is known as /dev/sdX, copy the image with:

    dd if=guix-system-install-1.3.0.x86_64-linux.iso of=/dev/sdX status=progress sync

## Graphical installation
+ https://guix.gnu.org/manual/en/html_node/Guided-Graphical-Installation.html
