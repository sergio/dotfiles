---
file: xbps.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags [voidlinux, void, packages]
---

## Mudando o mirror:
+ https://www.youtube.com/watch?v=0E4-Lx1kML4

## fuzzy xbps:
+ https://github.com/zdykstra/fuzzypkg

## how many updates:

    xbps-install -nuM | wc -l
    xbps-install -nuM | awk {print $1}

## xbps - voidlinux package manager

+ http://bit.ly/2LtUJQt

    Config dir: /usr/share/xbps.d

## Advanced xbps tutorial:
+ https://www.youtube.com/watch?v=44I2i1QynbM

## summary:

    xbps-install -Su   .................... update system
    xbps-query -s  ........................ string busca local
    xbps-query -sR ........................ string busca nos reps
    xbps-query -l ......................... same as dpkg (list packages)
    xbps-query -Oo  ....................... gremove orphans

## Basic Usage

To install a single package or list of packages (including dependencies), issue the following command:

    sudo xbps-install -S package_name1 package_name2 ...

To search for a package:

    sudo xbps-query -Rs package_name
    xbps-query --regex -Rs '^linux[0-9.]+-[0-9._]+'

To get a list of all installed packages, without their version:

    xbps-query -l | awk '{ print $2 }' | xargs -n1 xbps-uhelper getpkgname

To remove a single package, leaving all of its dependencies installed:

    sudo xbps-remove package_name

To remove a single package and all of its dependencies that are not required by other packages:

    sudo xbps-remove -R package_name

To synchronize your repository databases and update your system to the most recent packages, including their dependencies:

    sudo xbps-install -Su


## Holding packages
+ https://docs.voidlinux.org/xbps/advanced-usage.html

To prevent a package from being updated during a system update, use xbps-pkgdb(1):

    xbps-pkgdb -m hold <package>

The hold can be removed with:

    xbps-pkgdb -m unhold <package>

## Which package contains XYZ?

    xbps-query -Ro '*XYZ*'

If you need this more often, xlocate from the xtools package provides a caching database:

    doas xbps-install -Su xtools
    xlocate -S
    xlocate XYZ

## xbps-remove

This utility can be used to remove installed packages and clean the cache directory.
Removing a single package

    sudo xbps-remove pkg

Removing a single package and recursively all packages that were installed as dependencies

    sudo xbps-remove -R pkg

## Cleaning up the cache directory

    sudo xbps-remove -O

## Removing all package orphans

    sudo xbps-remove -o

Note: here "orphan" means packages not installed manually and no packages
depended. In Debian, "orphan packages" refer to packages abandoned by their
package maintainer.

## Removing all package orphans and clean the cache directory

    sudo xbps-remove -Oo

## xbps-reconfigure

This utility can be used to configure or force reconfiguration of an installed package.

When xbps-install installs a package, it performs the task in two phases:
unpacking and configuration. The unpacking phase unpacks the package files of
the binary package into disk, and the configuration phase performs additional
steps necessary to execute the software.

Packages that were not configured can be listed with xbps-query -l if its
first two characters are uu. In that case, those packages should be
reconfigured:

    sudo xbps-reconfigure -a

## Configure a package that is in unpacked state

    sudo xbps-reconfigure pkg

## Configure all packages that are in unpacked state

    sudo xbps-reconfigure -a

## Force reconfiguration of a package (even if it was configured previously):

    sudo xbps-reconfigure -f pkg

    xbps-pkgdb

This utility can be used to report errors in installed packages, as well as changing some of its properties.
Checking for errors in an installed package

    sudo xbps-pkgdb pkg

If pkg does not have any error there won’t be any output and return value will be 0.
Checking for errors in all installed packages

    sudo xbps-pkgdb -a

## Changing properties of an installed package

An installed package can have different modes depending how it was installed.
If a package was explicitly installed by the administrator and not as a
dependency, its installation mode will be set to manual, otherwise auto.

Packages that were installed manually can be listed with:

    $ xbps-query -m

or per-package:

    $ xbps-query -p automatic-install pkg

    It’s possible to change this mode with xbps-pkgdb(1):

    sudo xbps-pkgdb -m auto pkg
    sudo xbps-pkgdb -m manual pkg

A package can also be put on hold mode to skip updates while performing a system update:

    sudo xbps-pkgdb -m hold pkg
    sudo xbps-pkgdb -m unhold pkg

A package can also be put in repository locked mode (only update from the same repository it installed):

    sudo xbps-pkgdb -m repolock pkg
    sudo xbps-pkgdb -m repounlock pkg

    xbps-rindex

This utility can be used to generate local repositories, remove obsolete binary packages stored in them, and to sign the packages with a cryptographic key.
Creating a local repository

    $ xbps-rindex -a /path/to/dir/*.xbps

    Once the command has run, a local repository is available at /path/to/dir and can be used as an argument to the --repository option or be declared in /etc/xbps.d/.
Adding a specific package to a repository

    $ xbps-rindex -a /path/to/dir/foo-1.0_1.x86_64.xbps

Force addition of a specific package to a repository

    $ xbps-rindex -f -a /path/to/dir/foo-1.0_1.x86_64.xbps

Cleaning a repository (removing stalled entries)

    $ xbps-rindex -c /path/to/dir

Removing obsolete packages in a repository

    $ xbps-rindex -r /path/to/dir

Signing a repository

Initialize the repository metadata with signing properties:

    $ xbps-rindex --sign --signedby "I'm Groot" /path/to/dir

Signs all binary packages stored in repository with your specified RSA key. If the --privkey argument is not set, it defaults to ~/.ssh/id_rsa.

    $ xbps-rindex --signedby "I'm Groot" --sign-pkg /path/to/dir/*.xbps

    xbps-alternatives

    The xbps-alternatives utility lists or sets the alternatives provided by installed packages. Alternatives are classified by groups, and a group contains a number of symbolic links which are applied when the group is set.
List all alternatives

    $ xbps-alternatives -l

List alternatives for a specific package

    $ xbps-alternatives -l foo

Set all alternative groups

    $ xbps-alternatives -s foo

Set specific alternative groups

    $ xbps-alternatives -g bar -s foo

