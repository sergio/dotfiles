---
title: Markdown Language Manual
author: Sérgio Araújo
Last Change: Mon, 19 Feb 2024 - 02:13:39
tags: [text, markdown]
---

# How to use markdown syntaxe

+ [reference](https://daringfireball.net/projects/markdown/syntax)
+ <https://blog.ghost.org/markdown/>

Veja como pular para um [link interno](#Links internos)

## Enable vim conceal links

```
syn region markdownLink matchgroup=markdownLinkDelimiter start="(" end=")" contains=markdownUrl keepend contained conceal
syn region markdownLinkText matchgroup=markdownLinkTextDelimiter start="!\=\[\%(\%(\_[^][]\|\[\_[^][]*\]\)*]\%( \=[[(]\)\)\@=" end="\]\%( \=[[(]\)\@=" nextgroup=markdownLink,markdownId skipwhite contains=@markdownInline,markdownLineStart concealends
```

## Presentation tool

``` sh
pipx isnstall lookatme
```

## Converting html to pdf

+ [web page](https://www.princexml.com/) [[prince.md]]

## Links internos

Para linkar para um título interno basta fazer [nome do link](# How to use markdown syntaxe)

## formating divs

+ [making tip boxes with bookdown](https://desiree.rbind.io/post/2019/making-tip-boxes-with-bookdown-and-rmarkdown/)

``` css
.tip {
    padding: 1em;
    margin: 1em 0;
    padding-left: 100px;
    background-size: 40px;
    background-repeat: no-repeat;
    background-position: 15px center;
    min-height: 80px;
    color: #000000;
    /*background-color: #bed3ec;*/
    border: solid 2px #606060;
    border-radius: 6px;
    background-image: url("img/tip.png");
}
```

In your markdown:

``` markdown
:::tip
My content goes in here!
:::
```

In your html it becomes

``` html
<div class="tip">
My content goes in here!
</div>
```

``` markdown
::: warning
*here be dragons*
:::
```

Will then render as:

``` html
<div class="warning">
<em>here be dragons</em>
</div>
```

## Line breaks

+ <https://slides.yihui.org/2019-dahshu-rmarkdown#18>

By default, simply a line break in source won't work (it will be ignored).
The answer is add two trailing spaces before you break a line in the source.

## Coverter markdown para html com css

Make sure that your template is either in your working directory or in
`~/.pandoc/templates` NOTE: You have to make this directory yourself. You can use
variables in the template such as fontfamily to style your own file. Example:

``` sh
pandoc sample.md -o sample.pdf --template=mytemplate.tex -V fontfamily=sans

pandoc -s -c buttondown.css --metadata pagetitle="Vim Markdown" -o
teste.html vim-markdown-preview.md
```

## Creating title and changing the font in pandoc transformation

yaml or frontmatter

``` yaml
---
title: Daily Vim Solutions
author: Sérgio Araújo -- Thiago Avelino
fontsize: 12pt
fontfamily: Libertine
toc: yes
number-sections: yes
cover-image: assets/cover.png
stylesheet: assets/epub.css
keywords: [vim, nvim, programming, regex]
---
```

### Generating new templates

```sh
pandoc -D latex > ~/.pandoc/default.latex && $EDITOR ~/.pandoc/default.\LaTeX\
```

## Online rendering markdown to html

+ <http://dillinger.io/>
+ <https://stackedit.io/>

## Inserting table of contents

"inserindo links locais no seu wiki/markdown"

```txt
## Table of Contents
1. [Example](#example)
2. [Example2](#example2)
3. [Third Example](#third-example)

## Example
## Example2
## Third Example
```

## tabelas (tables)

``` markdown
Table: Vim Modes

Mode    |How to Use It
:-------|:-------------
Normal  |<kbd>Esc</kbd>
Insert  |<kbd>i</kbd>,<kbd>I</kbd>,<kbd>a</kbd>,<kbd>A</kbd>
Visual  |<kbd>v</kbd>,<kbd>V</kbd>
Command |<kbd>Esc</kbd><kbd>:</kbd>
```

## Heading Numbers add-on for Google Docs

``` sh
1 - Head up to your Google Docs document.
2 - Click Add-ons > Get add-ons.
3 - Search for “Heading Numbers” in the G-suite market place.
4 - Once you found it, click on the add-on and install it.
```

## <i class="icon-file"></i> Create a document

## text bold, italic and links

It's very easy to make some words **bold** and other words *italic* with
Markdown. You can even [link to Google!](http://google.com)

```markdown
![alt text for screen readers](/path/to/image.png "Text to show on mouseover").
```

## This is an <h1> tag

## This is an <h2> tag

#### This is an <h6> tag

*This text will be italic*
*This will also be italic*

**This text will be bold**
**This will also be bold**

*You **can** combine them*

## Lists

+ Item 1
+ Item 2
+ Item 2a
+ Item 2b

## Cite inside Blockquotes

+ <https://stackoverflow.com/a/2002150/2571881>

``` markdown
    > Quote here.
    >
    > -- <cite>Benjamin Franklin</cite>
```

## Images

![GitHub Logo](/images/logo.png)
Format: ![Alt Text](url)

Links

<http://github.com> - automatic!
[GitHub](http://github.com)

Blockquotes

As Kanye West said:

> We're living the future so
> the present is our past.

Inline code

I think you should use an
`<addr>` element here instead.

## Syntax highlighting

Here’s an example of how you can use syntax highlighting with GitHub Flavored Markdown:

```javascript
function fancyAlert(arg) {
    if(arg) {
        $.facebox({div:'#foo'})
    }
}
```

You can also simply indent your code by four spaces:

    function fancyAlert(arg) {
        if(arg) {
            $.facebox({div:'#foo'})
        }
    }
