---
file: unaccent.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## veja também [[iconv]]

    echo ação | unaccent utf-8 | tr '[a-z]' '[A-Z]'

    unaccent UTF-8 "ação"
