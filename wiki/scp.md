---
file: scp.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# scp.md
O comando scp permite copiar arquivos remotamente
sua syntax é simples

	scp origem destino

veja um exemplo em que copiamos arquivos *.jpg para a pasta atual

	scp fulano@172.30.34.5:/home/fulano/*.jpg ./

	scp -p fulano@192.168.15.30:/home/fulano/*.jpg ./

	scp -rvz -e 'ssh -p 2222' --progress user@server:~/.vimrc .

    # copy to android through termux (-P indicates port)
    scp -P 2223 aliases u0_a103@192.168.15.30:~/storage/downloads
    scp -rv -P 8022 u0_a179@192.168.100.11:~/sdcard/Music ./

## copiando pastas remotamente
Neste caso temos que usar o parâmtro 'r' do scp

	scp -r fulano@ip:/pasta /home/backup/
	scp -P 2222 user@remote-host:/home/user/.dotfiles ./

## editando arquivos remotamente

	:e scp://username@someremotehost/./path/to/file

	gvim scp://manager@192.168.1.11/./bashrc

## Para arquivos grandes habilite a compressão

	scp -Cr origem destino

## limitando o consumo de banda

	scp -l10 pippo@serverciccio:/home/zutaniddu/* .

