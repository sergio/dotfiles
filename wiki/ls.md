---
file: ls.md
author: Sergio Araujo
Last Change: Tue, 28 May 2024 - 09:41:46
tags: [ls, utils, cargo]
---

Get back to [home](Home.md)

## ls with icons (you can also use exa)

`cargo install lsd` or install exa, both can show icons in your terminal

``` sh
(( $+commands[lsd] )) && alias ls='lsd --group-dirs=first -a'
```

The command exa also shows icons

## ignore pattern in the file name:

  ls -I "*.jpg"

## mostrar o nome entre aspas

```sh
use a opção -Q
quoted :)
```

## listar arquivos em full path

```sh
ls | sed s,^,$(pwd)/,
```

## listar em lista

```sh
ls -1
```

## Listar ocultos menos . e ..

```sh
ls -A
```

## Listar arquivos de hoje

```sh
ls -al | grep -v '^d' | grep `date +%Y-%m-%d`
```

## Listar arquivos e seu tamanho

```sh
ls -Ssh1
```

## exibe arquivos maiores que 50M com seu respectivo tamanho e localização

```sh
find -size +50M -print0 | xargs -0 ls -Ssh1
```

## Listar arquivos por data 'time'

```sh
ls -t
```

## Listar arquivo mais antigo

```sh
ls -t | tail -1
```

# pega somente arquivos

```sh
ls -lt | grep '^-' | awk '{print $8}' | tail -1
```

## listar arquivo recem modificado

```sh
ls -Alt|head -2
ls -Alt | head -2 | awk 'END {print $8}'
```

## Listar arquivos ocultos

```sh
ls -a
```

## Listar permissões de arquivo

```sh
ls -l
```

## Encontrar o diretório mais recentemente modificado

```sh
ls -d --sort=time */ | head -n 1
```

```sh
ls -lrt | awk '/^d/ { f=$NF }; END{ print f }'
```

## Combinando o ls com o comando shuf:

Alguns comandos não geral uma lista de arquivos, embora lidem com listas geradas por outros comandos, o shuf é um desses exemplos, se desejo pegar o nome de uma imagem aleatória no meu diretório de imagens eu faço:

```sh
ls -1A ~/img/backgrounds/* | shuf -n 1
```

O comando acima pode ser usado para criar uma variável a ser udada em outro comando, no caso um comando que além de setar o seu papel de parede, baseado nas cores do wallpaper, ele vai configurar as cores do seu terminal.

```sh
sudo pip3 install pipx
pipx install pywal
```

O comando pipx instala seus programas em um sandbox, evitando conflitos no namespace. Após instaldo o pywal você terá um comando `wal`, usando ele criei o seguinte script:

```sh
/home/sergio/.local/bin/wal -i "$(ls -1A ~/img/backgrounds/* | shuf -n 1)"
```

Altere o caminho de acordo com o seu sistema e usuários, após isso abra um novo terminal e rode esse comando:

```sh
wal --preview
```

Assim cada vez que você rodar o comando `wal -i "caminho-do-arquivo` você verá um preview de como ficará o tema do seu terminal.

No caso do meu "tiling window manager" o bspwm eu tenho um atalho no sxhkdrc:

```sh
# change background
super + b
    ~/.dotfiles/algorithm/shell/bash/waltheme.sh
```

também uma linha no início do bspwm:

```sh
/home/sergio/.local/bin/wal -i "$(ls -1A ~/img/backgrounds/* | shuf -n 1)" &
```

E finalmente eu tenho uma linha no meu crontab que roda esse mesmo script a cada cinco minutos:

```sh
# crontab -e
# vim:et:sw=4:ts=4:cms=#%s:
# >/dev/null 2>&1   <- used to preven sending e-mail's
# env | grep -i display  to get your DISPLAY variable
# otherwhise feh will not be abble to change your background
# source: https://superuser.com/a/1122245/45032
DISPLAY=':0'
SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/home/sergio/.dotfiles/algorithm/shell/bash/
XDG_PICTURES_DIR=/home/sergio/img
WALLPAPER_DIR=$XDG_PICTURES_DIR/backgrounds
WAL=/home/sergio/.local/bin/wal
MAILTO=""
*/5    *    *    *    *  /home/sergio/.dotfiles/algorithm/shell/bash/waltheme.sh
```



