---
File: /home/sergio/.dotfiles/wiki/mpc.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [mpd, music, mp3, songs, audio, midia]
 vim:ft=markdown
---

## populate your playlist:

    mpc ls | mpc add

### Function to add songs:
+ https://apple.stackexchange.com/a/171664/303151

```sh
mpcadd(){
    for arg in "$@"; do
    dir=$(cd -P "$(dirname "$arg")" && pwd)/ || continue
    /bin/mpc add "${dir#"$HOME/music/"}$(basename "$arg")"
    done
}
```

## show current playing song:

    mpc current

## mpc search and play
+ https://unix.stackexchange.com/a/87751/3157

mpc play expects a position as argument, not a filename. A position in a playlist to be precise. You first have to add the file to a playlist, and then tell mpc to play that song from the playlist. But add will add the new song at the end of the playlist. To play the newly added song you have to know how long the playlist is.

Here are some suggestions how you can add a song and start playing it:

    Clear the playlist first:

    mpc clear
    mpc search title "when I was your man" | mpc add
    mpc play

    Insert as next and play next:

    mpc search title "when I was your man" | mpc insert
    mpc next

    Determine length of playlist to play last:

    mpc search title "when I was your man" | mpc add
    mpc play $(mpc playlist | wc -l)

    Print the current playlist with numbers:
    mpc playlist | awk '{print $0, NR}'

## rofi + mpc:
https://github.com/Marco98/rofi-mpc

Fast graphical Rofi-Interface for contolling MPD

In my case I've added a keybinding to `ctrl + shift + m` to call the script:
~/.dotfiles/bin/rofi-mpc

Note: I have also edited the original script changing the music folder name to "music"

## Getting the song number in the playlist and playing it:

    mpc playlist | awk '{print $0, NR}' | grep -i chris
    Chris Fisher - Unfilter 370: New Lab Leak Evidence 5199
    Chris Fisher - Unfilter 376: America's War 5200

## listing my "choros" from the playlist:

    mpc playlist | awk '{print $0, NR}' | grep choro | awk -F/ '{print $3,$4}'

## playing a radio

    mpc insert http://relay3.slayradio.org:8000/
    mpc next

    # universitária FM (Fortaleza)
    mpc insert http://200.129.35.230:8081
    mpc next

    Common parameters of mpc : mpc add add songs to the playlist
    mpc listall|mpc add can add all songs to the current playlist
    mpc listall can list all songs
    mpc playlist view the current playlist
    mpc view the information of the currently playing song
    mpc play play
    mpc pause pause
    mpc stop stop
    mpc next play next
    mpc prev play previous
    mpc repeat on enable repeat play
    mpc random on enable random play
    mpc play 18 the 18th
    mpc search filename in the playlist You can search for
    mpc by file name You can find search artist by artist
    mpc search title Search by song name can
    adjust the volume:
    mpc volume +20
    mpc volume -20
    more options mpc mpc can see the help of
