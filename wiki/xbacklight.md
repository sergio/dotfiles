---
File: xbacklight.md
Author: Sergio Araujo
Last Change: Mon, 23 Dec 2024 - 10:13:21
tags: [tools, desktop]
---

#  xbacklight.md intro:

xbacklight - adjust backlight brightness using RandR extension

    xbacklight -dec 20

see also: xrandr
