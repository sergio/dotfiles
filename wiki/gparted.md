---
file: gparted.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown

# Introdução
Particionador gráfico, site oficial: http://gparted.sourceforge.net/

## Veja também
* Clonezilla: http://clonezilla.org/download/sourceforge/
