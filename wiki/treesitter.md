---
File: ~/.dotfiles/wiki/treesitter.md
Last Change: Sat, 06 Jan 2024 - 06:40:41
tags: [nvim, treesitter, programming]
---

## Article

+ [Code Folding in Neovim with Tree-sitter](https://www.jmaguire.tech/posts/treesitter_folding/)
+ [Treesitter & Text Objects](https://www.josean.com/posts/nvim-treesitter-and-textobjects)

Para inspecionar a arvore
```nvim
:InspectTree
```
