---
file: feedly.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [rss, news]
---

## feedly is an online css client
+ https://www.makeuseof.com/best-rss-feed-readers-for-linux/

## How to change my passowrd?
+ https://feedly.com/i/account/logins

## How to export feedly opml file?
+ https://zapier.com/blog/feedly-export-opml/

    prefrencies →  then click on the arrow

## feedly shortcuts

     ┌─────────────┬─────────────────────────────────┐
     │     j       │  Next article                   │
     ├─────────────┼─────────────────────────────────┤
     │     K       │  Previous article               │
     ├─────────────┼─────────────────────────────────┤
     │     v       │  View origial in a new tab      │
     ├─────────────┼─────────────────────────────────┤
     │     gl      │  Read later                     │
     ├─────────────┼─────────────────────────────────┤
     │     ?       │  Show help                      │
     └─────────────┴─────────────────────────────────┘

