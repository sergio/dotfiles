---
file: lshw.md
author: Sergio Araujo
---
 vim:ft=markdown

# lshw.md -
Last Change:  Tue, 26 Apr 2022 - 13:07:39

Este comando é muito útil, pois exibe informações detalhadas sobre o hardware,
como a lista pode ser muito longa use em combinação com o comando [[less]]

   lshw | less

## show memory

    sudo lshw -short -class memory

## show processor

    sudo lshw -short -class processor

## disc type and capacity

    sudo lshw -short -class disk

## which monitor do you have?

    sudo lshw -c display

## find your wirless network adaptor. You can do so by using

    lshw -C network

### para saber se o sistema é 32 ou 64 bits

    sudo lshw -C cpu | grep width

### informações sobre a placa de vídeo

    lshw -C display

### seu sistema é 32 ou 64 bits?

    sudo lshw -C cpu | grep width

* http://blog.cidandrade.pro.br/tecnologia/hardware-linux-ubuntu/
