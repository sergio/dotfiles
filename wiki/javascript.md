---
javascript.md
last change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

JSON ( JavaScript Object Notation, pronounced / ˈdʒeɪsən /; also / ˈdʒeɪˌsɒn /) is an open standard file format and data
interchange format that uses human-readable text to store and transmit data objects consisting of attribute–value pairs
and arrays (or other serializable values).

- https://docs.npmjs.com/creating-a-package-json-file
