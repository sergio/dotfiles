---
file: adduser.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
vim:ft=markdown
---

## adduser.md Introdução

Adiciona usuários ao sistema

    adduser nome

## usando uma pasta existente como home

    adduser fulano --home=/home/fulano
