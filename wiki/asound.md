## Alsa config file
# $HOME/.asoundrc

pcm.!default {
 vim:ft=markdown
    type file               # File PCM
    slave.pcm "hw:0,0"      # This should match the playback device at /proc/asound/devices
    file "|safe_fifo /tmp/audio" #safe_fifo will be in the cli-visualizer/bin/safe_fifo directory
    format raw              # File format ("raw" or "wav")
    perm 0666               # Output file permission (octal, def. 0600)
}

