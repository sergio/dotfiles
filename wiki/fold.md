---
file: fold.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 06:42:55
---

Utilizando o fold a saída será devidamente formatada para uma “tela” de, no
caso, 80 colunas de largura.

    fold --width=70 --spaces lorem_ipsum.txt

