---
file: curl.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [tools, network, download]
---

    curl transfers a url like wget

## References:
+ [Download por linha de comando com o curl](http://bemvindoaolinux.blogspot.com/2009/01/downloads-por-linha-de-comando-curl.html)
+ [IBM](http://www.ibm.com/developerworks/br/opensource/library/os-curl/index.html)
+ [manpage](https://curl.haxx.se/docs/manpage.html)

    curl -O url

A opção `-O` irá preservar o nome original do arquivo, caso queira altera-lo use`-o`

    curl -O http://cdn.designrulz.com/wp-content/uploads/2012/08/pallet-designrulz-[1-33].jpg

    the `-o` option allows you to chose a different name or location

## get current ip address (public address)

    curl ifconfig.me

## use quiet mode
Just use -s option

    curl -s

       If you prefer a progress "bar" instead of the regular meter, -#,
       --progress-bar is your friend. You can also disable the progress meter
       completely with the -s, --silent option.

## How do I determine if a web page exists with shell scripting?

    curl -s --head http://www.google.com/ | head
    awk '{print $2}' <(curl -s --head $site | head -n 1)

    status=$(curl -s --head -w %{http_code} http://www.google.com/ -o /dev/null)
    echo $status

	curl -s -o /dev/null -w "%{http_code}" www.google.com

## Downloading an audio stream:

    curl --max-time 7200 -sS -o stream.mp3 http://stm3.xcast.com.br:8560/stream

## Downloanding a range of files
You can specify multiple URLs or parts of URLs by writing part
sets within braces as in:

    http://site.{one,two,three}.com

Or you can get sequences of alphanumeric series by using [] as in:

    ftp://ftp.example.com/file[1-100].txt

    ftp://ftp.example.com/file[001-100].txt (with leading zeros)

    ftp://ftp.example.com/file[a-z].txt

Nested sequences are not supported, but you can use several ones next to each other:

    http://example.com/archive[1996-1999]/vol[1-4]/part{a,b,c}.html

You can specify any amount of URLs on the command line. They will
be fetched in a sequential manner in the specified order.

You can specify a step counter for the ranges to get every Nth number or letter:

    http://example.com/file[1-100:10].txt

    http://example.com/file[a-z:2].txt

    --create-dirs

    When used in conjunction with the -o option, curl will create
    the necessary local directory hierarchy as needed. This option
    creates the dirs mentioned with the -o option, nothing else.
    If the -o file name uses no dir or if the dirs it mentions
    already exist, no dir will be created.

### baixar todos os pacotes tar.gz

    curl http://www.phrack.org/archives/tgz/phrack[1-67].tar.gz -o phrack#1.tar.gz

    curl "http://forklift-photos.com.s3.amazonaws.com/[12-48].jpg" -o "#1.jpg"

    curl "http://www.ssb.no/english/subjects/06/05/nos_lonn_en/nos_d362_en/tab/tab-[001-131].html" -o "#1.html"

    curl -Z 'https://www.springfieldspringfield.co.uk/view_episode_scripts.php\?tv-show\=big-bang-theory\&episode\=s[01-12]e[01-24]' -o '#1_#2.txt'

# Downloading files from a list
+ https://stackoverflow.com/a/8841522/2571881

``` bash
xargs -n 1 curl -O -J -L < your_files.txt
xargs -n 1 curl -k -O -J -L < bigfile.txt
```

NOTE: the -J and -L options are used to get the original file name and follows
the link

## get your timezone

    curl -s worldtimeapi.org/api/ip.txt | sed -n 's/^timezone: //p'

## measuring a iso size before downloading it

    curl -sI https://static.apricityos.com/iso/apricity_os-09.2016-dev-gnome-i686.iso | grep Content-Length | awk '{print $2}'

## Filering some iptv channels

    curl -s https://www.iptvsource.com/dl/br_030119_iptvsource_com.m3u | sed -n '/CULTURA/{n;p}'

The curl's `s` option supresses download progress view and the sed options
show the next line after the pattern

## using curl with proxy
add this line in your ~/.bashrc

    alias curl='curl --proxy 192.168.1.11:8080'

    curl -x proxysever.example.com:PORT http://address-i-want-to.access

## followig redirections

    just use the "-L" option

    curl -L url

## salvando os comandos mais populares do site command-line-fu

    curl http://www.commandlinefu.com/commands/browse/sort-by-votes/plaintext/[0-2500:25] | grep -v _curl_ > comfu.txt

## get external ip

    curl ifconfig.me

## Save mp3 from stream

    curl -sS -o $outfile -m $showlengthinseconds $streamurl

## encurtador de urls do google
* http://vivaotux.blogspot.com/2010/03/encurtador-de-urls-do-google.html

    ``` bash
    curl -s 'http://ggl-shortener.appspot.com/?url='"$1" | sed -e 's/{"short_url":"//' -e 's/"}/\n/g'
    ```

## Exibir IP externo

    echo `curl -s sputnick-area.net/ip`

## ou

    curl icanhazip.com
    curl -s ip.appspot.com

## Dá pra twittar com o curl

    curl -u NOMEDEUSUARIO:SENHA -d status=”SUA MENSAGEM AQUI ENTRE AS ASPAS” http://www.twitter.com/statuses/update.xml
    curl -u user:pass -d status="Tweeting from the shell" http://twitter.com/statuses/update.xml

## ou

    curl -u twitter-username -d status="Hello World, Twitter!" -d source="cURL" http://twitter.com/statuses/update.xml

``` bash
## ou
##!/bin/bash
curl -u USER:PASS -d status="$*" http://twitter.com/statuses/update.xml > /dev/null
echo "Message sent!"
```

## upload via ftp

    curl -u user:passwd -T /home/dir/local_file_to_upload ftp://your_host.com/subdir/

## baixar a transcrição de uma temporada inteira do Two and A Half Men

		curl http://www.springfieldspringfield.co.uk/view_episode_scripts.php\?tv-show\=two-and-a-half-men\&episode\=s01e\[01-24\] -o "two-and-a-half-men_#01.txt"

## baixando lições de inglês audio e texto

    curl --limit-rate 15k http://learnrealenglish.com/AudioVideo/Tip%20[1-7].zip -o tip_#1.zip
    curl --limit-rate 15k http://learnrealenglish.com/AudioVideo/tips/Tip_[1-7].pdf -o tip_#1.pdf

## download all delicious bookmarks

    curl -u username -o bookmarks.xml https://api.del.icio.us/v1/posts/all

## extrair tarball remoto sem salvar localmente

    curl http://example.com/foo.tar.gz | tar zxvf -

## changing user-agent

    curl -silent -A "iMacAppStore/1.0.1 (Macintosh; U; Intel Mac OS X 10.6.7; en) AppleWebKit/533.20.25" http://ax.search.itunes.apple.com/

The option below sets curl as opera

    curl -silent -k -A "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36 OPR/58.0.3135.127"

    curl -A "Opera/9.80 (Macintosh; Intel Mac OS X; U; en) Presto/2.6.30 Version/10.63" -o file.zip url

Apearing like opera-developer

    curl -A "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3610.70 Safari/537.36 OPR/60.0.3259.32"

    https://addons.${navegador}.com/themes/download/${theme-name}/

chrome:

    curl -A "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/58.0.3029.110 Chrome/58.0.3029.110 Safari/537.36"

# Curl: disable certificate verification
+ https://serverfault.com/a/469825/92333

The error message: curl failed to verify the legitimacy of the server and therefore could not

The solution:

    -k, --insecure

(SSL) This option explicitly allows curl to perform "insecure" SSL connections
and transfers. All SSL connections are attempted to be made secure by using
the CA certificate bundle installed by default. This makes all connections
considered "insecure" fail unless -k, --insecure is used.

 vim:tw=78:ts=4:ft=markdown:norl:syntax=off:fdl=3
