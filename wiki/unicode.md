---
-- File: /home/sergio/.dotfiles/wiki/unicode.md
-- Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [utf-8, unicode, vim, nvim]
 vim:ft=markdown
---

	╭─────────────────────────────────────╮
	│                                     │
	│  One text box drowed using neovim   │
	│                                     │
	╰─────────────────────────────────────╯

 u256d --> ╭

 u2570 --> ╰

 u256e --> ╮

 u2500 --> ╯

 u2500 --> ─
