# zshell zmv module

Last Change: Mon, 23 Oct 2023 - 14:52:58
tags: [zsh, zmv, rename, tools]


Mass rename files with zmv. To install zmv, run autoload zmv. I downloaded a lot of images for a machine learning model and wanted to rename them to be more consistent (ie. epcot-1.jpg, epcot-2.jpg, epcot-3.jpg... instead of 1.jpg, 2.jpg, 3.jpg...) The command to do so would be

```sh
  zmv '(*).(jpg|jpeg)' 'epcot-$1.$2'
```

To check what would happen before running the command, you can add -n, instead running

```sh
  zmv -n '(*).(jpg|jpeg)' 'epcot-$1.$2'
```
