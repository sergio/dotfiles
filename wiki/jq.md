---
file: /home/sergio/.dotfiles/wiki/jq.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [utils, text, json]
---

#  jq.md intro:
+ https://linuxhint.com/bash_jq_command/
+ http://www.compciv.org/recipes/cli/jq-for-parsing-json/
+ https://blog.lazy-evaluation.net/posts/linux/jq-xq-yq.html

jq is a program described as "sed for JSON data":

You can use it to slice and filter and map and transform structured data with
the same ease that sed, awk, grep and friends let you play with text.

JSON data are used for various purposes. But JSON data can’t be read easily
from JSON file by using bash script like other normal files. jq tool is used
to solve this problem. jq command works like sed and awk command, and it uses
a domain specific language for working with JSON data. jq is not a built-in
command. So, you have to install this command for using it.

``` json
Students.json

[
    {
    "roll": 3,
    "name": "Micheal",
    "batch": 29,
    "department": "CSE"
    },
    {
    "roll": 55,
    "name": "Lisa",
    "batch": 34,
    "department": "BBA"
    },
    {
    "roll": 12,
    "name": "John",
    "batch": 22,
    "department": "English"
    }
]
```

Run the following command to read Students.json file.

    jq '.' Students.json

    jq '.[] | .name, .department' Students.json

## matching a phone number:
You can you the site: https://jqplay.org/ to test your jq command

```sh
termux-contact-list > contatos.txt

jq '.[] | select(.name == "Emanoel") . number' contatos.txt

jq -r '.[] | select(.name | startswith("Ana Rita")).number' contatos.txt

jq '.[] | select(.name | startswith("Fábio")) | .name + ":" + .number'
contatos.txt

 jq '.[] | select(.name | startswith("Ana")) | "contato: " + .name + "
 --> " + .number' contatos.txt
```

 # convert output to csv
 + https://stackoverflow.com/a/68664471/2571881

 I have my contact numbers as follows:

 ```json
[
  {
    "name": "88 99965-4727",
    "number": "88 99965-4727"
  },
  {
    "name": "ABELARDO LUPION DEM PR",
    "number": "32155515"
  }
]
 ```

 To filter for a given name and give me the output in csv format I use:

 ```sh
 jq -r '.[] | select(.name | startswith("Fábio")) | [.name,.number] | @csv ' contatos.txt
 ```

## compact output

    pbpaste | jq -c

## Filtering my google keep data

    jq '.annotations[]|.title,.url' *.json | vim

## Pretty format a json file in vim with jq
+ https://til.hashrocket.com/posts/dosh4hozhr-pretty-format-a-json-file-in-vim-with-jq

Viewing a minified or unformatted json in vim can be a headache. But luckily for us, that’s an easy fix if you have jq installed. The following command will pass the contents of your current buffer to the jq external command for formatting:

:!% jq .

https://stedolan.github.io/jq/
