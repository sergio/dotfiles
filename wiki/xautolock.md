Filename: xautolock.md
Last Change: Thu, 02 Nov 2023 - 05:40:57
tags: [screensaver, screen, x11, lock]

## Intro:

    xautolock -time 10 -locker "i3lock -i background_image.png"

    xautolock -time 5 -locker "~/bin/lockscreen.sh" -detectsleep -corners '++--' -cornerdelay 5 -notify 5 -notifier ~/bin/lockscreen-notify.sh

In my DWM autoload.sh I have this:

```sh
pgrep -x xautolock >/dev/null ||  xautolock -time 5 -locker slock -nowlocker slock -detectsleep -corners ++-- -cornerdelay 3 -notify 5 -notifier "/home/sergio/.dotfiles/bin/lockscreen-notify.sh" &
```

```sh
#!/bin/sh
set -e

notify-send --urgency=critical --icon preferences-desktop-screensaver --expire-time=5000 "about to lock screen ..." "move or use corners"
```

