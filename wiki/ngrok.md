---
Filename: ngrok.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [network, ssh]
 vim:ft=markdown
---

## Get ngrok token:

https://dashboard.ngrok.com/get-started/your-authtoken

ngrok config add-authtoken 1Q70Hkj9J2kJQW6X0dUxRWg_2zaT6JD1NiUzMTEt7sjoR

where the key must be different
``` sh
ngrok tcp 22

It will show something like:

Forwarding      tcp://0.tcp.sa.ngrok.io:19816 -> localhost:22

ssh -p 19816 user@0.tcp.sa.ngrok.io
```


