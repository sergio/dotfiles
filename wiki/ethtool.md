---
file: ethtool.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# ethtool

    Criado: Sun 18/Jan/2015 hs 09:50
    Last Change: mar 19 2019 11:59

## speed up the Ethernet

    sudo ethtool -s eth0 speed 100 duplex full

## como forçar a placa de rede para full duplex

## instale o programa ethtool

    iface eth0 inet static
    #pre-up /usr/sbin/ethtool -s $IFACE speed 1000 duplex full autoneg off
    pre-up /usr/sbin/ethtool -s $IFACE autoneg off 100 duplex full
    address 123.456.789.123
    netmask 255.255.255.0
    gateway 123.456.789.254

Para verificar o uso do modo full-duplex no Windows, acesse as propriedades da
conexão de rede e clicar em "configurar" para abrir a janela de opções da
placa de rede. Você encontrará a opção com o
modo de transferência da placa de rede na sessão "Avançado":
Fonte: [[@http://www.gdhpress.com.br/redes/leia/index.php?p=cap1-10]] [[image:http://www.gdhpress.com.br/redes/leia/cap1-10_html_78acedb5.png align="left"]]
