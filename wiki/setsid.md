---
File: /home/sergio/.dotfiles/wiki/setsid.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [utils, sysadmin, admin, tools]
 vim:ft=markdown
---

From: Linux 101 Hacks
Linux setsid Command Examples
by Ramesh

What is setsid?
2 setsid examples
Syntax and Options
Related Commands

## What is setsid?

setsid creates a new session, which can be used to execute a program.

## setsid Examples
1. Execute a program even after log out

Just like nohup and screen command, you can use setuid to execute a program even after you log-out as shown below. The following example will run the ./your-long-running-script.sh even after you log-out.

# setsid ./your-long-running-script.sh

2. Execute a program with an argument in setsid

You can also execute a program that requires an argument as shown below. The following example executes ./custom-script.sh with ‘sample-argument’ as its argument.

    # setsid ./custom-script.sh 'sample-argument'

Syntax and Options

    Syntax:

    setsid program [arg...]
