---
file: britisandamerican.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Arquivo: class notes

    beats me...     sei lá  (audio 1)
    named after ... com nome de..(audio 1)
    look after...   cuidar (audio 3)
    flat rate ...   valor fixo, montante fixo

Youtube video: possessive pronoums and determiners
http://va.mu/AdL98

If you wanna download one phrase in english you can do:

http://translate.google.com/translate_tts?tl=en&q=How+are+you+doing

------------------------------------------------
 Good places to practice your grammar knowledge
 ------------------------------------------------

http://web2.uvcs.uvic.ca/elc/studyzone/410/grammar/410-modals-of-necessity-prohibition-and-permission1.htm
