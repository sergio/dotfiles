---
File: buftabline.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [nvim, plugins]
---

#  buftabline.md intro:

    Plug 'ap/vim-buftabline'
    highlight BufTabLineCurrent ctermfg=109
    highlight BufTabLineCurrent ctermbg=green
    highlight BufTabLineActive ctermbg=white
    highlight BufTabLineHidden ctermbg=darkgrey
    highlight BufTabLineFill ctermbg=grey

