---
File: ~/.dotfiles/wiki/brightnessctl.md
Last Change: Mon, 23 Dec 2024 - 10:27:55
tags: [monitor, brightness, keyboard, desktop, tool, led]
---

# **`brightnessctl` Command Line Guide**

`brightnessctl` is a simple utility to adjust the brightness of screens and keyboards on Linux systems.

## Set keyboard leds on

```sh
sudo brightnessctl --device='input2::scrolllock' set 1
```

#### **Basic Syntax**
```bash
brightnessctl [options] [operation] [value]
```

#### **Options**
- `-d, --device=<device>`: Specify a device (e.g., `backlight` or `kbd_backlight`).
- `-l, --list`: List all brightness-controllable devices.
- `-m, --machine-readable`: Output machine-readable data.
- `-q, --quiet`: Suppress output.
- `-h, --help`: Show help and exit.

#### **Operations**
1. **Get current brightness**:
   ```bash
   brightnessctl get
   ```
2. **Set brightness to a percentage or value**:
   ```bash
   brightnessctl set <value>
   ```
   Examples:
   - `brightnessctl set 50%` – Set brightness to 50%.
   - `brightnessctl set 200` – Set brightness to absolute value 200.

3. **Increase/Decrease brightness by a percentage or value**:
   ```bash
   brightnessctl set +<value>
   brightnessctl set -<value>
   ```
   Examples:
   - `brightnessctl set +10%` – Increase brightness by 10%.
   - `brightnessctl set -10%` – Decrease brightness by 10%.

4. **Get maximum brightness**:
   ```bash
   brightnessctl max
   ```

#### **Examples**
- **List available devices**:
  ```bash
  brightnessctl -l
  ```


- **Set keyboard backlight to 30%**:
  ```bash
  brightnessctl -d kbd_backlight set 30%
  ```
- **Reduce screen brightness by 5% silently**:
  ```bash
  brightnessctl -q set -5%
  ```

For further details, use `man brightnessctl` or `brightnessctl --help`.

## add autostart file

```sh
[  ! -f  "$HOME/.config/autostart/turnonkeyboardled.desktop" ] &&
cat <<-EOF> "$HOME/.config/autostart/turnonkeyboardled.desktop"
[Desktop Entry]
Type=Application
Exec=sh -c "sleep 10 && $HOME/.dotfiles/bin/turnonkeyboardled.sh"
Hidden=false
NoDisplay=false
X-GNOME-Autostart-enabled=true
Name=Turn On Keyboard LED
Comment=Script to turn on the keyboard LED
EOF
```


