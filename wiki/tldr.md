---
File: tldr.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [tools, cli, man]
---

## Shorter and better man pages

    tldr find

## update tldr database

    tldr -u
