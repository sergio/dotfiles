---
title: Pandoc Document Converter
author: Sérgio Araújo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [pandoc]
---

## Synopsis
+ https://pandoc.org/MANUAL.html

    pandoc [*options*] [*input-file*]...

*Pandoc* can generate multiple outputs from markdown text
for example

    pandoc -s README -o example4.tex
    pandoc -s README -o example4.pdf

    pandoc MANUAL.txt --pdf-engine=pdflatex -o example13.pdf

    pandoc -f markdown Sample.md --pdf-engine=pdflatex -t latex -o Sample.md.pdf

    pandoc sample.md -o sample.pdf --template=mytemplate.tex -V fontfamily=sans

    pandoc -s nvim-made-easy.md --variable urlcolor=cyan --listings -H
    listings-setup.tex --top-level-division=chapter -o nvim-made-easy.pdf &&
    evince $_

    pandoc --toc -V documentclass=report -s nvim-made-easy.md --variable
    urlcolor=cyan --listings -H listings-setup.tex
    --top-level-division=chapter -o nvim-made-easy.pdf && evince $_

Instead of a file, an absolute URI may be given. In this case
pandoc will fetch the content using HTTP:

    pandoc -f html -t markdown http://www.fsf.org -o sfs-org.md

    curl --silent site | pandoc -f html -t markdown_strict -o output.md

## generating an epub

    pandoc -r markdown -o dist/book.epub metadata.txt chapters/00-Acknowledgements.md chapters/01-introduction.md --table-of-contents


## markdown to pdf

``` markdown
pandoc --from=markdown --output=my.pdf my.md                                   \
       --variable=geometry:"margin=0.5cm, paperheight=421pt, paperwidth=595pt" \
       --highlight-style=espresso

    # The variable geometry can also be done with
    pandoc input.md -V geometry:margin=1in -o output.pdf

pandoc -V geometry:"margin=5.0cm" -V fontsize=15pt --from=markdown
--output=livro.pdf livro.md
```

## Changing geometry of the output file
+ https://stackoverflow.com/a/34845685/2571881

For new users of pandoc (like myself), as an alternative to specifying
variables with the -V flag, you can add them to the YAML metadata block of
the markdown file. To change the fontsize, prepend following to your
markdown document.

``` markdown
---
documentclass: extarticle
fontsize: 12pt
classoption:
- twocolumn
---
```

## using here document to test pandoc
``` markdown
pandoc -o test.pdf --pdf-engine=lualatex << EOT

---
mainfont: Source Sans Pro
---

Lorem ipsum dolor sit amet

EOT
```

## Beamer presentations using pandoc
+ https://jeromyanglim.blogspot.com/2012/07/beamer-pandoc-markdown.html

    pandoc -t beamer my_source.md -o my_beamer.pdf
    pandoc -t beamer religiao.md -o Religião.pdf -V theme:Warsaw

## Change font on markdown
+ https://tex.stackexchange.com/questions/417572/pandoc-set-sans-serif-family
``` markdown
---
mainfont: Font-Regular.otf
mainfontoptions:
- BoldFont=Font-Bold.otf
- ItalicFont=Font-Italic.otf
- BoldItalicFont=Font-BoldItalic.otf
---
```
or
``` markdown
---
fontfamily: arev
---
```

Other option would be:

Get the default tex template from pandoc by running `pandoc -D latex` and then
add the font definitions you want to the document. save as my-font.latex. Then
run pandoc with the switch `--template=my-font`

## format source code
+ https://tex.stackexchange.com/a/179956/3528
``` markdown
% Contents of listings-setup.tex
% pandoc test.md --listings -H listings-setup.tex -o output.pdf
\usepackage{xcolor}

\lstset{
    basicstyle=\ttfamily,
    numbers=left,
    keywordstyle=\color[rgb]{0.13,0.29,0.53}\bfseries,
    stringstyle=\color[rgb]{0.31,0.60,0.02},
    commentstyle=\color[rgb]{0.56,0.35,0.01}\itshape,
    numberstyle=\footnotesize,
    stepnumber=1,
    numbersep=5pt,
    backgroundcolor=\color[RGB]{248,248,248},
    showspaces=false,
    showstringspaces=false,
    showtabs=false,
    tabsize=2,
    captionpos=b,
    breaklines=true,
    breakatwhitespace=true,
    breakautoindent=true,
    escapeinside={\%*}{*)},
    linewidth=\textwidth,
    basewidth=0.5em,
}
```

## Creating e-pub books
+ https://pandoc.org/epub.html

Compiling a markdown with titles and subtitles

    pandoc -s nvim-made-easy.md --top-level-division=chapter -o
    nvim-made-easy.pdf && evince $_

## Coverter markdown para html com css
+ https://thelocalyarn.com/article/making-a-book-making-machine

Make sure that your template is either in your working directory or in
`~/.pandoc/templates` NOTE: You have to make this directory yourself. You can use
variables in the template such as fontfamily to style your own file. Example:

    pandoc sample.md -o sample.pdf --template=mytemplate.tex -V fontfamily=sans

    pandoc -s -c buttondown.css --metadata pagetitle="Vim Markdown" -o
    teste.html vim-markdown-preview.md

The option `-s` stands for "standalone"

## How to set a css file on pandoc
Put your css styles in the folder

    ~/..pandoc/templates

NOTE: Do not forget to define a ***"header" section on your markdown file,
this is necessary because pandoc needs this section to set the html title.

    ---
    file: filenamd.md
    author: author_name
    date: mar 2019
    ---

## Another not yet tested pandoc template system
+ https://github.com/kjhealy/pandoc-templates

## Easy pandoc templates
+ https://github.com/ryangrose/easy-pandoc-templates

    curl 'https://raw.githubusercontent.com/ryangrose/easy-pandoc-templates/master/copy_templates.sh' | bash

The author says:

> motivation
>
> I was tired of how complicated making a nice-looking html page with pandoc
> was, so I decided to simplify it. CSS files were a good start, but to use
> anything more complicated (such as bootstrap or javascript files) I needed
> templates. The problem was that most templates used static references to local
> files, which required the file to be in the same directory as these files. In
> other words, you were forced to write your documents in the cloned repo
> directory.
>
> To solve this, I turned to the ubiquity of CDNs and github. I began by
> converting the static references to css, js, etc files to ones loaded via CDN
> in the html head block. This made the templates portable as well as always
> up-to-date since the CDN (rawgit) pulls from Github.

## Paragraphs

A paragraph is one or more lines of text followed by one or more blank line.
Newlines are treated as spaces, so you can reflow your paragraphs as you like.
If you need a hard line break, put two or more spaces at the end of a line.

## Footnotes

**Extension: `footnotes`**

Pandoc's markdown allows footnotes, using the following syntax:

    Here is a footnote reference,[^1] and another.[^longnote]

    [^1]: Here is the footnote.

    [^longnote]: Here's one with multiple blocks.

## Code syntax highlight

```python
import os
import sys

def application(env, start_response):
    start_response('200 OK', [('Content-Type','text/html')])
    return "Hello WSGI!!"
```

## Definition lists

``` markdown
Term 1

:   Definition 1

Term 2 with *inline markup*

:   Definition 2
```

## Simple lists
+ you can use plus `+`
+ also you can use `-`
+ and you can use `*`
