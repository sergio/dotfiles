---
file: csplit.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

csplit -z -f split teste '/evento/' '{*}'

csplit prog.c '/^}/+1' {100}

## splits prog.c into different
## routines where it is assumed
## that each routine ends with a
## '}' at the start of a line

