---
file: fuser.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução
Mata processos associados a um arquivo ou dispositivo

    fuser -k filename

	-k

Kill processes accessing the file. Unless changed with -signal,
SIGKILL is sent. An fuser process never kills itself, but may kill
other fuser processes. The effective user ID of the process executing fuser is
set to its real user ID before attempting to kill.

In order to kill a processes accessing a file or socket,
employ the -k or --kill option like so:

    sudo fuser -k .

To interactively kill a process, where you are that asked to
confirm your intention to kill the processes accessing a file
or socket, make use of -i or --interactive option:

    sudo fuser -ki .

matar processos associados a um diretório

    fuser -km /home

kills all processes accessing the file system /home in any way.

para casos em que você tenta desmontar um cdrom e o sistema
não permite o use do comando de desmontagem, ou seja, dá uma mensagem de erro use o comando fuser:

    fuser -mkv /media/cdrom0

    fuser -v -m .bashrc

The option, -m NAME or --mount NAME means name
all processes accessing the file NAME. In case you a
spell out directory as NAME, it is spontaneously changed to NAME/, to use any file system
that is possibly mounted on that directory.

## Referências
* http://debtux.co.cc/?p=7
