---
File: /home/sergio/.dotfiles/wiki/screenkey.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [screen, keyboard, tools, x11, video, tools]
 vim:ft=markdown
---

## How to kill screenkey?

+ https://superuser.com/a/1345082/45032

    pkill -f screenkey

