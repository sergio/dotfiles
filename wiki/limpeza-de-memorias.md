---
File: ~/.dotfiles/wiki/limpeza-de-memorias.md
Last Change: Thu, 26 Sep 2024 - 15:12:35
tags: [espiritism]
---

# oração

Hó Deus, bondade amor perfeição e misericórdia infinitos, venho a vós pedir humildemente que retireis de minhas memórias todos os traços de escolhas infelizes que tive em meu passado, para que sentimentos de culpa não atrapalhem meu caminho até vós, sustenta o meu propósito de manter-me no caminho reto. Desde agora vos agradeço por esta graça. Abençôa ó Pai cada passo meu, dando-me saúde properidade e vida longa, Amém!


