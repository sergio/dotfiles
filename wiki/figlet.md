---
file: figlet.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [asciart, ascii-art]
---

##  figlet.md intro:

display large characteres made up of ordinary screen characters

## On vim

    :r !figlet demo

        _
     __| | ___ _ __ ___   ___
    / _` |/ _ \ '_ ` _ \ / _ \
   | (_| |  __/ | | | | | (_) |
    \__,_|\___|_| |_| |_|\___/

To center the text use `-c` flag

tags: art, design, asciiart
