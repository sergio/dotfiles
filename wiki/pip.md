---
file: pip.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Gerenciador de pacotes python

    # install pip2
    python2.7 -m ensurepip --default-pip

    sudo pip install requests --upgrade
    sudo pip install beautifulsoup4

    sudo pip install package
    sudo pip uninstall package
    sudo pip install --upgrade package
    pip search package

## how to know which pip I am using

    pip --version

## Where does pip installs my packages
+ https://stackoverflow.com/a/45309460/2571881

    pip3 show package

## update all python packages
+ https://stackoverflow.com/a/16269635/2571881

    sudo pip install pip-review
    sudo pip-review --local --interactive

## force reinstall a python lib or package

    sudo pip install --upgrade --force-reinstall requests

## upgrade all pip packages
+ https://stackoverflow.com/a/27071962/2571881

    pip install -U `pip list --outdated | awk 'NR>2 {print $1}'`
    pip list -o --format json | jq '.[] | .name' | xargs pip install -U

tags: python, pip
