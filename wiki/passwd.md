---
file: passwd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Define senhas

Se o comando passwd for usado sem argumentos, ou seja, sem indicar
o usuário o sistema considera que se deseja modificar a senha do
usuário corrente
