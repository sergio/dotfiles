---
file: locate.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# localizar arquivos mp4

    locate .mp4

## como atualizar o banco de dados do locate?

    locatedb

No voidlinux

    sudo xbps-install -S mlocate
    sudo xbps-alternatives -s mlocate -g locate
