---
File: ttyrec.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [tty, recording, terminal, tools, utils]
 vim:ft=markdown
---

## Install on voidlinux:

    sudo xbps-install -Sy ttrec

Also install ttygif: https://github.com/icholy/ttygif

## How to use it:
+ https://www.thegeekdiary.com/ttyrec-record-terminal-session-in-linux/

    ttyrec

    ttyplay ttyrecord
