---
File: vimala.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: mai 31, 2021 - 20:28
tags: [spirituality, caligraphy]
---

#  vimala.md intro:
+ http://love-to-learn-homeschool.blogspot.com/2007/12/vimala-handwriting.html
+ http://archive.constantcontact.com/fs090/1107894919152/archive/1109181299979.html

Vimala Rodgers developed The Vimala Alphabet. Each letter shape is designed
specifically to bring your hidden talents and abilities to life. As you begin
to adopt self-affirming writing patterns, you naturally release those that are
self-limiting, and at the same time you reconfigure the neurological patterns
in your brain. This process is invaluable for people who are determined to let
go of the past and stride ahead fully expressed in life. In scientific circles
it is called neuroplasticity or cortical remapping.  All it takes is pen and
paper.

## The Vimala Alphabet

The way we write is a direct representation of what we are – how we feel and
what our values are.  The Vimala Alphabet is based on the research of Vimala
Rodgers, who discovered the meaning of each letter and what value it
represents. When you change your handwriting, you can actually change your
life.

Each time you write a particular letter, you reinforce a value in your life.
Sometimes, these letters might not represent your best self. By adopting The
Vimala Alphabet you can change what you are and how you see yourself.

How does it work? When you change your handwriting, you are changing the
pathways in your brain. This is called neuroplasticity. It is like rewiring
your brain. What we’re doing through changing our handwriting is actually
rewiring parts of our brain so that we can live up to our full potential.

The Vimala Alphabet is divided into families of letters that are taught in the
order in which they are shown below since they represent the ongoing process
of human maturity.

The families are:

A, O, D, G, Q and P - Communication

Y, U, W, V - Learning and Evaluating

M, N, H - Honoring and Expressing

L, E, I, J - Insight

F, R, S - Applied Creativity

T, K, B - Status

C, X - Trusting & Inner Authority

Z - Contentment

## How to use the Vimala Alphabet?

If you are among the courageous visionaries who want to transform their lives through the Vimala Alphabet technology, choose at least 3 letters:

The first letter is the initial of your first name - not nickname, but your real birth first name. If you adopted a new first name after your 35th birthday, use that as your first name. By writing this letter, you will experience profound personal changes.
The second letter is the Letter of the Week.
The third letter is one you have chosen in order to bring about a specific transformation in your life.  Choose a letter from the Vimala Alphabet Guidebook.
Follow these steps:

Write at least 3 lines of upper and lowercase letters.
Write on unlined paper in the landscape direction.
No pencils, please. Use a pen.
Begin by writing once, at the top of the page, the Declaration of Intent of the initial of your first name.
Write the first and third letters for at least 40 consecutive days. If you skip a day, start over. 40 is the number of transformation; it takes 40 days for the neurons in the brain to realign themselves, which is what the process of adopting self-affirming handwriting patterns does.
 Write the third letter for a single week.

## The letter A
+ http://archive.constantcontact.com/fs090/1107894919152/archive/1109770632995.html

SOUL QUALITY: Spiritual stardom ~ Transforming ego into Spirit

As the first letter of the Alphabet, the Letter A a represents our
entrance into the world as a soul encased in a personality. The
question the Letter Aa answers is "How does my self image affect my
presence in the world?

To round the uppercase A as we were taught in elementary school is to
emphasize the ego, the personality, and put Spirit on the shelf---to
have the outer appearance as our focus. From this vantage point we
become self-conscious with an overriding concern about what others
think of us. Ego rules. By inscribing the "Star A," the personality
becomes merely an interested observer as the soul makes itself known,
quietly takes center stage, and expresses itself from
there. Attentively, write a few lines of the "Star A" ---slowly at
first---to see how it makes you feel.

The lowercase a lives in the midzone---everyday life---and is a clean and clear gentle oval with no clutter inside.

DECLARATION OF INTENT: Today I will notice how my personality affects others, and grow from that knowledge.

GUARDIAN PROTECTOR: Gabriel (Gahb-ree-EL)---the angel of rebirth.

The guardian protector of the Letter Aa is Archangel Gabriel. In
spiritual traditions around the world, Gabriel is the one who brings
good news, who trumpets the onset of new life. Known as the angel of
rebirth, Gabriel urges us to slip out of the skin of ego and begin to
express ourselves from Spirit---that precious part of ourselves we
have set aside or denied for perhaps countless incarnations, yet in a
roundabout ways have been struggling to reclaim. If this is one of
your lifetime goals, begin a dialog with Gabriel today, asking him to
awaken you to who you really are, minus the mask of your personality.

## The letter B
+ https://www.youtube.com/watch?v=sLOP9BvshWI
+ https://changemindfully.wordpress.com/2015/05/15/how-to-manage-money-better/

B is a sheltering and protecting form

Whenever we tend to “leak” money it will show in our letter B-b... Letter B-b
shows many different attitudes, one of them is relates to how we manage our
finances & the choices we make…

Have you ever found yourself buying on impulse, regretting it afterwards,
knowing that you had neither need nor funds to spare for the purchase? “What
in the hell possessed me?”, “If only I could resist…” we often say…

It sometimes feels like somebody else is in charge not us…

## The letter C
+ https://www.youtube.com/watch?v=eaBJFbE0Oro

The letter C is the letter of pure trust, is the letter that invites
use to feel the embrace of the Earth

## The letter D

It has three important aspects

1 - Contens our hability to deal confrontation
2 - Our sensitivity to others
3 - It creates our sense of boundaries

the letter D has three really important qualities in it it contains our ability
to deal with confrontation it also has our sensitivity to others and it has our
creating our sense of boundaries which is so important when we're with other
people the letter D has a beautiful shape which says these exact qualities so
perfectly it starts with an IM stroke which starts at the very tip top of the
upper zone and comes down to the baseline as you can see right here and once it
hits the baseline what this says is I'm a spiritual being gathering gifts from
heaven and I'm bringing them down to earth which is represented by the baseline
I'm bringing them down to earth to use here on the earthly plane and then the
second stroke of the uppercase letter D is this giant backward C shape which is
actually quite wide and it comes it says I'm I'm trading a space for myself to
be I'm claiming the territory that I need setting my boundaries and then once
it hits out I am struggles forward with this stroke which is this blessing this
sharing this attitude of gratitude and opening to others the lowercase letter D
begins at the top of the middle zone and forms a very sweet oval shape once
it's done with that it goes shoots up into the upper zone to this very tip top
of the episode and then that loop is retraced back down hitting the baseline
and then reaching out with the sweet little garland to meet the next letter the
lowercase D it's very important that the stem has no loops in it the loops we
form in the upper zone are like an antenna our periscope looking around
gathering information about what other people think of us which really is none
of our business what other people think of us has more to do with who they are
and what they believe that it has to do with us so we're going to eliminate
that what other people think of us really has very little to do with ourselves
and the less we collect that data from other people the better off we are in
life it prevents us from being people-pleasing it insulates us against always
trying to worry what the public opinion about is is instead going inward for
the answers where the answers the true answers actually belong going inside so
this is in the the stem of the lowercase D so what we have here is this
beautiful picture the uppercase D being quite wide and giving us that room and
those boundaries we need the lowercase C being very clear and defined what we
find in our lives when we have a very healthily formed D is we find that when
confrontation comes up we stop viewing it as a personal attack on us and we're
curious well what does the other person actually trying to say what do they
mean what is the nature of their conflict with us rather than immediately and
instantly responding to it as if they said some form of truth about us it gives
us that space I like to liken it to a clock face and this impulse are this
attack or this criticism comes to us from fate from the direction of the number
three on the clock and it comes in and we immediately react no response is to
react back comes in three we reacted three now when we actually are able to
form the D in a very healthy way what we find is a whole repertoire of
responses increases so in that moment when that person says that thing which
feels like an attack or criticism we can stop and have that space in there and
then respond and we might respond on that clock face anywhere from 12 to 5 or
even more responses but we have that space in there which allows us to reflect
and respond rather than react also find that with a very healthy form D what we
have is we have really healthy boundaries now many people have said that
healthy boundaries are the very basis of a good relationship where we have
healthy boundaries the other person is able to relate to us and our job is to
teach them how to treat us and with healthy boundaries we can do that in a very
effective way so sensitivity to others boundaries and skill and handling
confrontation those are three qualities in the very broadest sense that help us
that are so that we find support us when we form a healthy letter D you

## The letter F
+ http://archive.constantcontact.com/fs090/1107894919152/archive/1109279787049.html

Letter of the Week - F f

When a first-time writer of The Vimala Alphabet decides to adopt the Letter
Ff, the response to writing the lowercase is often grumbling and
resistance--and even, tales be told---of throwing a pen across the room in
frustration! It isn't the two vertical loops that cause upset, it's the final
horizontal loop that is appropriately placed to the left of the initial
downstroke. that has the pen resist mightily. Why? Because, drawn in this
manner it states, "Nothing can stop me!" The subconscious mind, aka mover of
the pen,  declares "Oh yeah? Who do you think you are?!" ---and you submit by
placing that loop to the right---if you form it at all. The Letter Ff is a
member of the Alphabetical Trio of Grumbling and Resistance; the other two
members are the letters Mm and Cc. These three are the most challenging
letters for most people to change---and the most transformational, once
mastered.

Declaration of Intent:

Nothing will stop me from making a difference in the world. Nothing!

Guardian Protector: Jophiel (Jo-fee-EL) ---the angel of creative power

Everyone ever born has a uniqueness that is theirs alone. You do. Your mother
does. Your next door neighbors does. I do. Each is a little different from the
other, yet distinctly our own. The invitation Jophiel issues is clear and
simple: "As you practice your Letter Ff, invite me to guide the pen-to loosen
any knots of unexpressed creativity that live in your mind, and set you free.
It would give me great joy to accelerate your homeward journey."

If doubt and uncertainly plague you in regard to your ability to get out there
and create a program that would serve the needs of a particular group, begin
your daily writing practice with a prayer to Jophiel, asking her to keep your
mind and heart open to unusual and innovative ideas, and ways in which you can
implement them.  Throughout the day speak to her as though to a good
friend---for she will become that.

How to inscribe the letter Ff:

Uppercase: One "I am" stroke to the baseline, followed by two horizontal
strokes, tipped slightly upward.

Lowercase: From a gentle garland at the baseline, scoop up to the top of the
upperzone creating a loop filling the mid- and upperzone. Continue into the
lower zone and create a loop to the right of the downstroke. Come back to the
baseline and form a horizontal stroke to the left of the initial stroke,
thrusting it off to the right.

Caveat:

    Do not ever have a stick formation in either the upper or lower zone.
    Inscribing a loop to the left in the lower zone represents self sabotage. Don't do that!

Page 131: Your Handwriting Can Change Your Life

## The letter H
+ https://changemindfully.wordpress.com/2015/05/14/i-have-a-wonderful-job-with-letter-h/

It invites you to learn necessary lessons from the blind alleys you encounter
along the way and guides you through them with dynamic, not-forsissies energy.
If you want to put firm footing on your intended life path, this is the letter
to adopt. As a sidebar, I have practiced the Letter Hh daily for more than 30
years; not surprisingly, I experience its “Just go do it!” energy each
day—yes, each day. Miracles and open doors greet me everywhere I go.

There are so many people out there unhappy in their jobs, but not knowing what else to do…
This letter is perfect for all those situations when we don’t really know what do to professionally…
It is packed full of energy; courageous, energetic & fully alive!
Doing what you love and loving what you’re doing…

If you want find a job that makes the most of all of your talents & makes you
smile and get up happy in the morning – you need take a good look at this H
and start inscribing it in the way shown…

There is so much energy in this letter…just feel it…

And it looks just unstoppable with that loop thrust to the right (future)
which says exactly that: “Nothing will stop me!”

“Whatever I have in my soul, projects, desires, passions…. I am ready to
express them now, & nothing will stop me!”

If you have a vision you were too shy to share with the world… start working
on this letter and you will find the courage to offer it to the world – our
gifts are what the world needs… Don’t let them go to waste… bring them to life
– these talents have been  vested in you and you have the power and an
obligation to share them… & you’re gonna love doing it!

Make sure your strokes are firm and decisive – slow down if you need to and
inscribe the “H” with intention & energy that it deserves… Also make sure that
in the small “h” you pull it apart even more than in the picture – create a
narrow ‘v’ there at the baseline before you finish it… that will help you open
up to the new…

Miracles and open doors will greet you wherever you go… Trust me… I have been there …

Enjoy the ride!…

Declaration of Intent:  I live my life path with the zeal of an evangelist and the wide-eyed innocence of a child.

Alphabetical Family:  The Letter Hh is the youngest child in the Family of Honoring and Expressing. All the letters in this family—Mm, Nn, and Hh—convey one message “Risk! Be true to yourself! Get out there in your life and do it!”

Element: Fire. The fuel of the letter Hh is to blaze a path through life fully and with joy.

Gender: Masculine. The Letter Hh is full to bursting with generative energy.

Gemstone: Blue lace agate. If you have dreams of making the world a happier
place yet can’t quite organize them into a profession, purchase and wear the
blue lace agate. Find a good-size raw one and place it on your altar. If you
don’t have an altar, make one. It’s simply a way of acknowledging that someone
higher than you is running the show. By placing a blue lace agate on your
altar, you are saying, “We’re in this together. Inflame my life path with your
guidance, grace, and blessings.” I wear a blue lace agate every day and have
one on my altar as well.

Animal: Butterfly. The butterfly is the quintessential expression of freedom
from a confining self-image. It begins life as a creepy crawly little
caterpillar and then, when the time is right, it wraps a cocoon around itself.
After a gestation period the cocoon begins to crack, then open. Then, lo and
behold, out comes not a little worm-shaped creature, but a magnificent
creation of varied colors and designs—with wings! A butterfly! To see the
butterfly and the Alphabet in concert, visit Appendix C.

Guardian Protector: Cerviel (Sir-vee-EL)—the angel of courage. When President
Woodrow Wilson asked that Alice Paul be put in an insane asylum for insisting
that women have the right to vote, this was the reply of the M.D. who denied
his request:  “Courage in women is often mistaken for insanity.”

Cerviel fills you with irresolute courage when others may question the
validity of your lifework because it’s unfamiliar to them or, more likely,
because it’s never been done before. When you feel cornered by negative
judgments, strike up a conversation with Cerviel as you write your Letter Hh.
Trust me — she will be right there to keep your intentions alive and give you
what it takes to carry on. I speak from experience when I say that she will
remind you again and again that “This is your life — not anyone else’s. I am
right here to give you what you need: opportunities and the courage to embrace
them. Now go — Live your life path fully!”

## The letter I
+ https://myemail.constantcontact.com/Your-Weekly-Vimala-Alphabet---The-Letter-I-.html?soid=1107894919152&aid=Jmzltxu-FVA#LETTER.BLOCK11

The Letter of the Week - I i

The letter I i  insists that you view all circumstances and people from an
objective stance. In other words, no good or bad, no right or wrong, no
judgments. This power-packed letter invites you to gain a perspective that
sees all sides clearly and evaluate without a viewpoint.  It puts you firmly
in the present and has you ask, "Am I seeing clearly, or am I interpreting
what I see?" You will understand the power of this letter only after you have
begun writing it regularly. Keep your Miracle Journal handy!

Guardian Protector: Shekinah (Shek-EEE-nah)-the angel of unity

Shekinah is a mighty angel in the Hebrew tradition. Depending on whose
translation you read, her name means "The Feminine Presence of God," "The
Glory of God," or "Queen of Angels." Basically, Shekinah is the feminine
aspect of God: open, loving, forgiving, accepting, and all-embracing, with no
restrictive parameters. She is a radiant example of the Letter I i . In a
situation where you want to be in present time and see all sides of an issue,
invoke Shekinah's guidance to be impartial and clear-seeing with no
prejudicial inferences. If it's possible at the time, as you write lines of
the Letter I i , both upper- and lowercase, still your mind, sit quietly in
silence, tune into her consciousness and pray, "Beloved Friend...Guide me,
bless me, move my pen."

Declaration of Intent:

I see as God sees and love as God loves.

When I heard Ram Dass say this in the '70s, although it was the precise soul
energy of the Letter I i , I don't use anyone else's material and claim it as
my own, so I set it aside...although it remained powerfully in my mind as a
reflection of the letter I i . I continued to search my heart for the perfect
Declaration of Intent for the Letter I i  --but because "I see as God sees and
love as God loves" is the very soul of the letter I i , I adopted it as our
own. No other statement fits quite so precisely. Thank you, Ram Dass.

How to inscribe this letter:

Uppercase: Inscribe a firmly drawn "I am" stroke to the baseline. Pick up the
pen. Draw two horizontal crossbars, one at the top of the "I am" and one at
the bottom. Tilt the top one slightly upward.

Caveat: Do not let the "I am" stroke incise either crossbar.

Lowercase: Begin at the baseline extending the stroke to the top of the
midzone, retracing it as you descend to the baseline. Place a dot (only a dot)
directly above the stem.

Caveat: Do not ever let a loop occur in the stem. Not ever.

## The letter M
+ http://archive.constantcontact.com/fs090/1107894919152/archive/1108954009050.html

Letter of the Week - M

The flow of Divine Grace is not reserved for a special few; it is available to
everyone, no matter their belief system or lack of it. It indicates the degree
of control you attempt to exert in your relationships, whether personal or
business. It also reflects the habit you may have of trying to "change" or
"fix" people. The Letter MMm, well drawn,  has you realize that you can
transform any relationship, not by having the other person change, but by
changing your attitude toward them. The Letter MMm will do that for you. Easy?
Not for most of us. But worth every pen and bottle of ink you use to master
it.

Declaration of Intent: I give up the need to control; Divine Grace fuels my life.

Guardian Protector: Ananchel (ah-NAN-kel)-the angel of Divine Grace.

Ananchel is the angel who radiates from the heart of the Letter MMm. She has been assigned the joy-filled task of keeping an eye out for those times when only heavenly intervention can spin a negative situation around. If you find yourself wanting to restore broken relationships, and truly want to change your attitude by accepting the other persons as they are, create a dialog with Ananchel. If you are sincere, as you purposefully write your MMm's each day and invite  Ananchel into your life, be on the lookout for miracles! I have celebratory stories from many people who have done just this, and as a result, have seen their lives have open up in amazing, joy-filled ways.

How to Inscribe the Most Self-Affirming MMm:

Uppercase curved top M:

Begin with a gently curved stroke (the humor flourish) at the top of the upper zone, leading into two arcades written in a cascading fashion like a gentle waterfall, with the first one higher than the second.

Caveat: Pull the arcades apart at the baseline creating a v shape to separate them. End with a gentle upturned stroke at the baseline.

Uppercase pointed M-pointed:

Scoop up from the baseline to the top of the upperzone, then drop down to the baseline and form a soft, lean, narrow curve. Ascend upward again, forming a point not as high as the first one, then drop to the baseline, finishing with a gentle upturned stroke at the baseline.

Lowercase m - lowercase:

Beginning at the baseline, not at the top of the midzone, inscribe a miniature of the uppercase, forming three arcades, pulling them apart at the baseline, cascading downward, and occupying only the midzone.

Caveat: Do not ever begin this letter, upper- or lowercase, with an angular stroke.

Suggestion: If you decide to write this letter every day, you might want to write at least once---or say aloud---the Declaration of Intent. Just as there is power in each pen stroke, there is also power in words. Each is kinesiologically based.

## The letter N

learning to look at handwriting is actually
like opening a book into someone's soul
and with a little bit of training and it
actually doesn't take that much there's
a tremendous amount that you can see
about yourself although that's the
hardest part but about other people
because the way and being the letter of
friendship when you look at this and you
look at how someone forms their letter n
you can look at the quality of their
relationships one on one you can see how
they feel about themselves how they
where they place the relationship
between themselves and others all that
is actually very apparent in this letter
and so what we're teaching our children
when we're teaching them this these
forms is that in a one-on-one
relationship this is self and this is
other in a one-on-one relationship it's
very important that we place ourselves
first now that can sound selfish and
before you jump to that conclusion I
want to say this there's only one person
that you're going to have is a life
partner that's going to be with you your
entire life and that's you and if you
don't place yourself first and take care
of yourself and recognize your needs
and ask for those needs to be met and do
what's necessary to feed and clothe and
bathe yourself there isn't a single
person in the world that's going to do
that for your whole life it's just that
they're just not out there and so when
we relate to our to others from a place
of taking care of our own needs of being
responsible for that then we're able to
enter into a relationship if we're
entering into a relationship hoping and
wishing but there you can see that we
haven't XYZ tore we need to be eat or we
need whatever it's not going to work
it may work in the short term but it's
not going to work in the long term and
so this is what you can see in the
letter n is this relationship between
South and other and
parents I know I wanted to give
everything to my children and make them
feel loved and meet all their needs but
really the task of parents is to have
our children grow up to learn to take do
those things for themselves and this is
this is all you can see all of this in
the letter n you know how we print the
letter n where we well when you look at
this when people do this in cursive what
you see is what they've left out is this
first arcade which is theirselves and
you'll see I will get handwriting from
people who will but the whole thing will
be cursive except their ends
it's a giant flashing red light this
person does not know how to take care of
themselves you'll see often in the ends
and you see it I see it often in the
ammu's so in our task as parents and
teachers and we want to teach children
to take care of themselves to get their
needs Matt give them to put that RK that
first arcade in there very important
look at your own handwriting to you are
you putting that first arcade in there
when you write the letter n I mean these
are all things that you can you can work
with and this is just one small example
of one variation of one letter but it
serves as a an example of what
handwriting contains
you

## The letter O
+ https://www.youtube.com/watch?v=d6KqOvuL67Y

"Sensitive truth-telling"

The  letter O well drawn insists that you speak indwelling in another, the
inner voice for an adult, my words are always kind and helpfull. Sensitive
speaking honors the listener, conveis the truth and never judges for it speaks
to the indewelling spirit of the listener, this atitute is reinforced by the
habit of inscribing the letter 'o' in a clockwise direction, always in a
clockwise direction. The letter o drawn in a counter cost-wise direction tends
to encorage fat sharing with little concert for how the listener will
interpret it, it can be quite hartful.

## The letter S
+ http://archive.constantcontact.com/fs090/1107894919152/archive/1109384429675.html

The letter of balanace and equanimity

The Letter Ss looks so simple to form: Swirl, swirl, loop, done. Like
life! Almost like a dance---but as you and I know, sometimes we just
can't keep up when the tempo accelerates. Of all the letters, the
Letter Ssinsists that we s-l-o-w d-o-w-n, not only as we move the pen,
but as we move through life. Take deep breaths more often; stop and
enjoy your surroundings whether in the city or in a forest; take a
walk when possible; when you pass by a garden, be aware of the beauty;
drop into perfect silence when others are speaking; and never let
family and friends come last. Never. Instead have your hugs, smiles,
and laughter be an expression of your love for them.

Guardian Protector: Ariel (Ah-ree-EL)---the angel of nature.

Ariel represents Mother Nature at her balanced best. She oversees not
only all plants, animals, insects, and bugs, she also guides the
energy in our lives for we, too, are a part of nature. On those days
when it feels as though all the demands of life are crushing down on
you, take a break and ask for Ariel's help. Go for a walk, or if
that's not possible, excuse yourself from whatever you're doing and
find a place to be alone, a quiet place---no phones---no
computers---no one else---and sit where's there's a writing surface
handy. Close your eyes and invoke Ariel's presence and guidance. Take
a few deep breaths, then open your eyes, and slowly---the key is
slowly---write lines of the letter Ss and lighthearted words that
contain it. Invest at least 20 minutes of quality time writing. Her
help takes many forms---so don't be surprised when it comes. Be
grateful.

Declaration of Intent: There are no extremes in my life.

## The letter T

"Like everyone else on this planet, you were born with a driving interest of
some kind. If that fascination of yours is not at the very heart of your
career and you'd like it to be, cross all your T's on TOP. Yes, lowercase as
well...in fact, in particular! I have the handwriting of over 40
visionaries---and ALL their crossbars are inscribed determinedly right at the
top! To name a few: Albert Einstein; Maria Montessori; Abraham Lincoln, Nikola
Tesla, Rudolph Steiner, Annie S. Peck, Anne Morrow Lindbergh, Mother Teresa,
Annie Jump Cannon and, when he wrote in English: Mohandas Gandhi. Each of them
was only one person--and yet each one, solitarily, elevated the consciousness
of the world by activating their vision. You can do the same! So pick up your
pen and begin! We need our visionaries to step forward now--and you are one of
them!

Declaration of Intent: I grow in spiritual stature as my choices align with my vision.

Guardian Protector: Paschar (Poo-SHAR)---the angel of vision. Each angel in
the hierarchy of thousands has a specific assignment, a particular purpose to
aid us humans in being fully aware of who we are as children of God. Some
angels are sweet, some are protective, some are kindly. As Guardian Protector
of the Letter , Paschar is a dynamo. Her job is to enter your heart of hearts
and inflame you with your life purpose and support you in achieving it. As you
begin to place a firm crossbar on top of the letter , especially if you decide
to practice the "dancing ," invite Paschar to walk---or run---with you as the
seeds of your vision begin gently to split open, sprout, and take root. Create
a dialog with her; let her know "you mean business," and she will be right by
your side, guiding you step by step. She loves to support persons with drive
and spirit ! How to inscribe this letter: Uppercase: Inscribe an "I am" stroke
with a firmly drawn horizontal stroke at the very top, tilted slightly upward.
Don't exaggerate the length; that's an ego display. Draw it with firm intent;
that's a declaration of Spirit!

Lowercase: #1 - From the baseline, stretch to the top of the upperzone, then
RETRACE that stem back to the baseline (No loop in that stem please). Finish
with a firmly drawn crossbar placed right on top of the stem. #2 - Dancing .
Only lowercase. Scoop to the top of the upperzone, then drop straight to the
baseline. Keep pen on paper and stretch to the top of the upperzone. Once
there, swing to the left and inscribe a horizontal loop, finishing off to the
right.

Point of interest: Just as every stroke in handwriting makes a statement, this
horizontal loop declares: Nothing can stop me! It occurs in five other Vimala
letters: A, q, H, f and Th."

## The letter U
+ https://myemail.constantcontact.com/Your-Weekly-Vimala-Alphabet---The-Letter-Uu.html?soid=1107894919152&aid=Q7amWQsP9SE

If you'd like to give up the need to be the one who knows, and allow others
to tell you "how it is," you will grow in spirit as you master writing this
letter, and apply it in your day-to-day life.

Declaration of Intent: I listen to others as though I know nothing.

Guardian Protector: Metatron (MET-a-tron) -the angel of thought. In Judaic
lore, Metatron is considered to be larger than the earth in size and next to
God in importance. Twin brother of Sandalphon, who is Guardian Protector of
the Letter Xx, many traditions are wrapped around this mighty being.
Alphabetically, the Mighty Metatron is the angel who invites us to keep our
thoughts free from opinions, and to be open to Truth in its many guises. He
invites us to interact with others as though they have the answers we lack. He
invites us to be grateful for our own mind, and to use it with respect for
both others and ourselves. If Uu is the letter you have chosen to practice,
ask Metatron to guide your thought habits, freeing them from opinions and with
an open mind to the views of others. It can be quite an astounding journey.

How to inscribe the Letter  Uu:

Uppercase: Cup-shape filling mid- and upperzones with the right side slightly
shorter than the left, ending with a gentle up-curve at the baseline.

Lowercase: A miniature of the uppercase, occupying only the midzone.

Caveat: Keep the sides of the letter vertical, not curved; no loop on either side.
