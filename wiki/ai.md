Filename: ai.md
Last Change: Tue, 05 Nov 2024 - 05:22:26
tags: [ai, chatgpt]

# pc control via ai

[youtube](https://youtu.be/F0y6BF8ywc0)
https://youtu.be/0_zXeS240G8
[youtube](https://youtu.be/ucilhsaIzqk)

## midjourney course:
+ https://www.youtube.com/watch?v=xeEw-R35ocU&t=302s
+ https://www.youtube.com/watch?v=XTfpIorqDb4
+ https://github.com/anthropics/anthropic-quickstarts/tree/main/computer-use-demo

I am using an alternative bot called "bluewillow"

Prompts:

    /imagine a colorfull high resolution rain forest with rocks and a rainfall --ar 3:2
    /imagine expressionist style realistic landscape in a colorful sunset --v 4 --ar 16:9
    /imagine Detaild Colorfull Raphael Lacoste ladscape --ar 3:2

    For generating nice wallpapers (using some artists styles)

    /imagine sunset over the ocean with the style of Monet's Water Lilies --ar 16:9
    /imagine snow-covered mountains with the style of Caspar David Friedrich --ar 16:9
    /imagine a field of poppies with the style of Vincent van Gogh's Starry Night --ar 16:9
    /imagine a forest with the style of John Constable --ar 16:9
    /imagine a city skyline at night with the style of Edward Hopper --ar 16:9
    /imagine a desert with the style of Georgia O'Keeffe --ar 16:9
    /imagine a beach scene with the style of Winslow Homer --ar 16:9
    /imagine a waterfall with the style of J.M.W. Turner --ar 16:9
    /imagine a mountain range with the style of Albert Bierstadt --ar 16:9
    /imagine a countryside scene with the style of Thomas Kinkade --ar 16:9
    /imagine a seascape with the style of Winslow Homer --ar 16:9
    /imagine a garden with the style of Claude Monet's Garden at Giverny --ar
    16:9 --ar 16:9
    /imagine a rural landscape with the style of Andrew Wyeth --ar 16:9
    /imagine a coastal scene with the style of David Hockney --ar 16:9
    /imagine a canyon with the style of Ansel Adams --ar 16:9
    /imagine a forest with the style of Gustav Klimt --ar 16:9
    /imagine a cityscape with the style of Edward Burtynsky --ar 16:9
    /imagine a river scene with the style of Winslow Homer --ar 16:9
    /imagine a valley with the style of Albert Bierstadt --ar 16:9
    /imagine a beach with the style of Joaquín Sorolla --ar 16:9
    /imagine Detailed Akihiko Yoshida style medieval village --ar 16:9
    /imagine Detailed Brothers Hildebrandt style landscape --v 4 --ar 16:9
    /imagine panoramic view of a long street in new yourk city, dystopian theme, Marvel comic book style --ar 16:/imagine panoramic view of a long street in new yourk city, dystopian theme, Marvel comic book style --ar 16:99

    spaceship:
    /imagine Detailed Bob Layzell style futuristic spaceship --v 4 --ar 16:9

## seeds:
+ https://www.youtube.com/watch?v=41JjWy7P1i0

Under the hood Midjourney uses randomness to generate your images. Think of it like this: by default, Midjourney selects a random seed value in each generation process that defines the type of noise pattern Midjourney uses to generate the image you want. This seed value is simply a number used to introduce a random but consistent element of “noise” into the process.

The number itself is not of particular importance, since it is used to make the results random. However, it does mean that Midjourney will use a different number at each prompt, which will produce different results unless you define a specific seed value. This is where the “seed command” comes in.

By using the seed command, we can force Midjourney to use a specific starting point for our prompt, so that if we use the same seed with the same prompt, we will get the same results.

To set the seed value, we use the “seed” parameter and add a random number:

      /imagine a black cat on a sofa --seed 1234

     /imagine Greg Rutkowski forest landscape, intricate, sunset, clouds --ar 16:9 --seed 977072360

     /imagine green moonlit desert, surrealism, by Tyler Edlin and Jean Delville,
     by John Berkey and Filip Hodas, sci-fi concept art, Beeple, highly detailed,
     soft lighting, rendered in octane, by Roger Dean, by Dean Ellis,
     catholicpunk, german romanticism style, oil on canvas, cinematic lighting,
     vibrant, concept art, gothic, contrasting, nightscape --ar 16:9

## Aspect ratios:
+ https://docs.midjourney.com/docs/aspect-ratios

  --aspect 7:4 Close to HD TV screens and smartphone screens.

## add segment and negative prompt:

   add segment to your prompt --> :: add negative prompt ::-0.5

## Image generation ai:
+ https://youtu.be/qOcpUqeiNYs

Give me a list of the best resources to learn prompt engineering and all the links as a footnotes
-------------------------------------------------------------------------------------------------

Here are some of the best resources for learning prompt engineering, along with links as footnotes:

[OpenAI's GPT-3 Playground (1)](https://beta.openai.com/gpt-3-playground/) - This is an interactive platform that allows you to experiment with OpenAI's GPT-3 model and see how it generates text based on the prompt you provide.

[The AI Dungeon (2)](https://aidungeon.io/) - AI Dungeon is a text adventure game powered by OpenAI's GPT-3. It is a fun way to see how GPT-3 can generate text for different prompts.

[OpenAI's GPT-3 Documentation (3)](https://beta.openai.com/docs/models/gpt-3) - This is a comprehensive guide to OpenAI's GPT-3 model, including technical details, tutorials, and examples.

[AI Writing Prompts (4)](https://aiwritingprompts.com/) - This is a website that provides various prompts for AI writing, including fiction, poetry, and more.

[AI Dungeon Master (5)](https://aidungeon.github.io/AIDungeonMaster/) - This is a tool that allows you to create your own text adventure games using OpenAI's GPT-3 model.

AI Writing Workshops (6) - Some organizations and universities offer workshops and courses on AI writing and prompt engineering. These can be a great way to get hands-on experience and learn from experts in the field.

Footnotes:

    [1] - https://beta.openai.com/gpt-3-playground/
    [2] - https://aidungeon.io/
    [3] - https://beta.openai.com/docs/models/gpt-3
    [4] - https://aiwritingprompts.com/
    [5] - https://aidungeon.github.io/AIDungeonMaster/
    [6] - Search for "AI writing workshops" on your preferred search engine to find options in your area.


