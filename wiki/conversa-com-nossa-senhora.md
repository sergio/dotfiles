---
file: conversa-com-nossa-senhora.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

    Conversa com Nossa Senhora

    Mão Santíssima, Agradeço imensamente sua presença junto a mim
    A luz que emana de ti, me envolve me dá paz e tranquilidade
    Sinto as gotas de luz que  colocas na minha boca, elas se espalham por todo o meu corpo
    curando-me inteiramente.
    Sinto purificar-se a minha casa mental e por extensão o meu corpo físico.
    Todas as tensões e ansiedade se vão á medida que minha respiração se aprofunda
    e se acalma. O ar que respiro torna-se leve e agradável.

    Sinto o teu perfume suave como ervas do campo e belas imagens que projetas na minha mente.
    Uma sensação de gratidão profunda toma conta de mim. Obrigado mão santíssima
    por estares ao meu derredor, por ouvires o meu chamado.

    Mãe Santíssima estende essa sesação de paz e tranquilidade a todos que se
    encontrarem ansiosos e aflitos. Da-me a certeza de que cada vez que me sentir
    em desequilíbrio possa dirigir o pensamento a vós e receber o teu santo
    amparo. Obrigado Mãe do Céu, Amém!
