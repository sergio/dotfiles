---
File: vimplugins.md
Author: Sergio Araujo
Last Change: Sat, 20 Apr 2024 - 12:50:17
vim:ft=markdown:
tags: [nvim, vim, plugins]
---

## Intro

This file describes some useful vim plugins

+ [emmet vim](emmet-vim.md) working with html files
+ [vim-unimpaired](vim-unimpaired.md) pairs of handy bracket mappings
+ [comment.nvim](comment-nvim.md) comment stuff out (lua plugin)
+ [fzf.md](fzf.md) fuzzy finder for files, commands and a lot more
+ [vim sneak](vimsneak.md) Sneak - the missing motion for Vim
+ [Vim Markdown](vim-markdown.md) Um plugin para facilitar a edição de arquivos markdown
+ [vim fugitive](https://github.com/tpope/vim-fugitive) git melhorado para o vim
+ [diffchar.vim](https://github.com/rickhowe/diffchar.vim) a better diff on vim
+ [vim-smoothie](vim-smoothie.md) Nice scrolling
+ [nvim-tree](nvim-tree.md) nerd tree like plugin (but in lua)
+ [buftabline](buftabline.md) show your opened buffers
+ [prettier](prettier.md)
+ [vim-autoswap](https://github.com/gioele/vim-autoswap)
+ [vim-speedating](tpope/vim-speeddating)
+ [nvim-beacom](nvim-beacom.md) DanilaMihailov/beacon.nvim (flashes the cursorline)

## Nvim plugins:

+ [buffer manager](https://github.com/j-morano/buffer_manager.nvim)
+ [comment.nvim](comment-nvim.md)
+ [edluffy/specs.nvim](https://github.com/edluffy/specs.nvim) better than beacon becaus it is written in pure lua
+ [gitsigns-nvim](gitsigns-nvim.md) Signs for added, removed, and changed lines
+ [harpoon](harpoon.md) Getting you where you want with the fewest keystrokes.
+ [impatient](lewis6991/impatient.nvim) lazy load plugins
+ [lazynvim](lazynvim.md)
+ [live-command](https://github.com/smjonas/live-command.nvim) adds live preview for commands
+ [lsp-zero](https://github.com/VonHeikemen/lsp-zero.nvim) - easy lsp config
+ [lsp-zero](https://github.com/VonHeikemen/lsp-zero.nvim) Easier LSP config
+ [lspsaga](lspsaga.md) better LSP options
+ [luasnip](luasnip.md)
+ [mason.nvim](mason-nvim.md)
+ [matchparen-nvim](matchparen-nvim.md) - faster than matchparen-vim because it uses treesitter
+ [mkdnflow.nvim](https://github.com/jakewvincent/mkdnflow.nvim)
+ [neopm](https://github.com/ii14/neopm) new package manager for test
+ [neoscroll](neoscroll.md) Nice scrolling (lua based)
+ [nvim-cursorword](nvim-cursorword) hightlights word under the cursor
+ [nvim-markdown](nvim-markdown.md) Fork of vim-markdown with extra functionality.
+ [nvim-surround](nvim-surround.md)
+ [packer](packer.md) Plugin management
+ [pounce.nvim](pounce-nvim.md)
+ [readline.nvim](https://github.com/linty-org/readline.nvim) Use readline shortcuts easily
+ [SmoothCursor.nvim](https://github.com/gen740/SmoothCursor.nvim) smooth scrolling
+ [shade-nvim](shade-nvim.md) dims your inactive windows, making it easier to see the active window at a glance.
+ [shade.nvim](https://github.com/sunjon/Shade.nvim) dim inactive windows
+ [snippet-converter.nvim](https://github.com/smjonas/snippet-converter.nvim)
+ [snippy][snippy.md] snippets engine (have a look)
+ [specs.nvim][https://github.com/levouh/specs.nvim] Show where your cursor moves when jumping large distances
+ [substitute.nvim](https://github.com/gbprod/substitute.nvim) similar to vim-exchange but written in lua (sxiw)
+ [surround.nvim](surround-nvim.md)
+ [telescope](telescope.md)
+ [vim-diminactive](vim-diminactive) vim alternative to shade-nvim
+ [which-key](which-key.md) shows and runs your mappings

## nvim colorschemes:

[tokyonight](tokyonight.md)

<!- vim: set nospell  --->
