---
file: parted.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# parted.md
+ https://linuxadmin.io/using-parted-create-new-swap-disk/

GNU Parted - a partition manipulation program

## General syntaxe

		parted device


In command line mode, this is followed by one or more commands. For example:

		parted /dev/sda resize 1 52 104 mkfs 2 fat16


 If you don't give a parameter to a command, Parted will ask you for it. For example:

		(parted) resize 1
		Start? 0
		End? 400

## Formating to install gentoo

		parted -a optimal /dev/sda

		mklabel gpt .......... creates a gpt partition table
		unit mib  ............ megabits as unit
		mkpart primary 1 3
		name 1 grub
		set 1 grub_bios on
		print

		mkpart primary 3 131
		name 2 boot
