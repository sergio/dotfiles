---
File: urxvt.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [terminals, tools]
---

# rxvt-unicode

## how to copy & paste on urxvt?
+ https://unix.stackexchange.com/questions/212360

    copy              =    Ctrl - Alt - c
    paste             =    Ctrl - Alt - v 
    primary selection =    shift - insert
