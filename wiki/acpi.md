---
file: acpi.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [battery, hardware]
---

# acpi - verify battery status

    acpi
    Battery 0: Discharging, 85%, 00:32:10 remaining

## verifing your battery at each 5 seconds

    watch --interval=5 acpi -V

tag: hardware, admin, adm
