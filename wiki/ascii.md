---
file: ascii.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Art ascii e tabela ascii
É uma tabela que relaciona números binários/inteiros com
caracteres

## ascii art
são desenhos feitos com caracteres

    $ cowsay "Hi, How are you"

     _________________
    < Hi, How are you >
     -----------------
            \   ^__^
             \  (oo)\_______
                (__)\       )\/\
                    ||----w |
                    ||     ||
