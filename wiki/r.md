---
File: r.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: jan 12, 2021 - 16:30
tags: [tags]
---

#  R programming language
+ https://cloud.r-project.org/

## install devtools
+ https://www.rdocumentation.org/packages/devtools/versions/2.3.2
+ [Using another mirror](https://cran.r-project.org/mirrors.html)
+ [shine](https://github.com/rstudio/shiny)

    install.packages("devtools")
    install.packages("remotes"
    install.packages('tufte')
    install.package("bookdownplus")

After installing a new package restart your session with Ctrl+shift+F10

    if (!require("remotes"))
    install.packages("remotes")
    remotes::install_github("rstudio/shiny")

    install.packages('package-name',repos='http://cran.us.r-project.org')

NOTE: I choose another mirror instead of Brazil's

## installation on voidlinux

    sudo xbps-install -Sy R

## manual instalation
Download here: https://cloud.r-project.org/src/base/R-4/R-4.0.3.tar.gz

    sudo xbps-install -Sy gcc-fortran
    sudo xbps-install -Sy xorg-server-devel
    sudo xbps-install -Sy pcre2{,-devel}

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0:
-->
