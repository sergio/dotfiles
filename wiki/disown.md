---
file: disown.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
vim:ft=markdown
---

# disown.md

Make a linux process run in background independent of YOU.
For example, you can log in on a machine run a

 No bash, o disown builtin é usado para remover tarefas da tabela de processos,
 ou para para marcar tarefas de forma a que, quando receberem um sinal SIGHUP,
 este não seja recebido pelo processo quando a shell pai receber este sinal.
 Isto ocorre quando, por exemplo, um usuário encerra sua sessão.

Vejamos mais um exemplo. Você começou uma tarefa complexa em seu notebook, e
achou que esta tarefa se encerraria rapidamente. Mas isto não aconteceu e você
precisa ir embora. O problema é que, se você encerrar a sua conexão, muito
provavelmente a tarefa importante e complexa que você iniciou será também
encerrada, com consequencias imprevisíveis.

Com o comando disown você pode conseguir escapar deste problema.

A primeira coisa a fazer é colocar o processo para rodar em background. Isto se
faz pressionando as teclas <CTRL><z> e em seguida digitando bg. Pronto, o
processo está rodando em background, mas você ainda não pode encerrar a sua
sessão. Quando damos o comando bg o processo recebe um número, que deve ser
usado em seguida para despossuir (disown) o comando.

Reproduzo a seguir uma sequencia completa de comandos ilustrando o uso do
comando diswown:

```sh
% find . >1 /dev/null
^Z
[1]+  Stopped                 find . /dev/null > 1
% :[/media/usbdisk]bg
[1]+ find . /dev/null > 1 &
% jobs
[1]+  Running                 find . /dev/null > 1 &
% disown %1
% jobs
%
```

O comando find é deliberadamente longo, achar todos os arquivos a partir do
diretório corrente. Em seguida, o processamento é interrompido (<CTRL><z>) e o
processo passa a rodar em background (bg). Em seguida, executo o comando jobs
para ver quais processos estão rodando em segundo plano. Vemos que o processo
continua lá. Em seguida, executo o comando disown %1. Ao emitir o comando jobs
novamente, vemos que o processo não mais me pertence. Posso fechar tudo e ir
embora :-)

