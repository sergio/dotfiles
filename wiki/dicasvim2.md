---
file: ~/.dotfiles/wiki/dicasvim2.md
author: Sergio Araujo
Last Change: Tue, 21 May 2024 - 19:34:03
tags: [nvim, vim]
---

## Weekly vim tips

+ [1] https://www.reddit.com/r/vim/comments/4aab93
+ [2] https://www.reddit.com/r/vim/comments/4b9hg5/
+ [3] https://www.reddit.com/r/vim/comments/4c62l3/
+ [4] https://www.reddit.com/r/vim/comments/4d6q0s/
+ [5] https://www.reddit.com/r/vim/comments/4e6d0f/
+ [6] https://www.reddit.com/r/vim/comments/4f79fd/
+ [7] https://www.reddit.com/r/vim/comments/4g9i9g/
+ [8] https://www.reddit.com/r/vim/comments/4ha7u6/
+ [9] https://www.reddit.com/r/vim/comments/4ifnbo/
+ [10] https://www.reddit.com/r/vim/comments/4jhjhu/
+ [11] https://www.reddit.com/r/vim/comments/4kjgmz/
+ [12] https://www.reddit.com/r/vim/comments/4ll0z8/
+ [13] https://www.reddit.com/r/vim/comments/4mobsr/
+ [14] https://www.reddit.com/r/vim/comments/4nrhya/
+ [15] https://www.reddit.com/r/vim/comments/4ouh89/
+ [16] https://www.reddit.com/r/vim/comments/4pz3v6/
+ [17] https://www.reddit.com/r/vim/comments/4r3d3e/
+ [18] https://www.reddit.com/r/vim/comments/4s5z12/
+ [18] https://www.reddit.com/r/vim/comments/4s5z12/
+ [19] https://www.reddit.com/r/vim/comments/4tb61s/
+ [20] https://www.reddit.com/r/vim/comments/4ue0tf/

## statusline

``` vim
function! SetStatusLine()
  if mode() =~ 'i'
    let c = 1
    let mode_name = 'I'
  elseif mode() =~ 'n'
    let c = 2
    let mode_name = 'N'
  elseif mode() =~ 'R'
    let c = 3
    let mode_name = 'R'
  else
    let c = 4
    let mode_name = 'V'
  endif
  return '%' . c . '*[' . mode_name . ']%* %<%F%=%m%r %18([%{toupper(&ft)}][%l/%L]%)'
endfunction

hi User1 gui=bold guibg=red guifg=white
hi User2 gui=bold guibg=blue guifg=white
hi User3 gui=bold guibg=coral guifg=white
hi User4 gui=bold guibg=green guifg=black

set statusline=%!SetStatusLine()
```

## vim jump to mark (Can I start vim by a bookmark)

+ https://stackoverflow.com/a/62697744/2571881

    vim +normal\'A

## Jump to the beginning of a method

+ [jump to next method](https://vimtricks.substack.com/p/vimtrick-jump-to-next-method)

```viml
]m jump to the beginning of the next method
]M jump to the end of the next method
[m jump to the beginning of the previous method
[M jump to the end of the previous method
```

If you have the plugin vim-markdown installed you can do
In markdown files we can simply use ]] or [[

## fuzzy file search with fzy or FZF

    vim **<tab>

## Adding current location to the jump list

+ https://stackoverflow.com/a/54928351/2571881

    m'

## Creating a file from terminal

``` vimscript
filename="test.txt"
vim -c ":wq! $filename" - << EOF
This file was created automatically from
a shell script.
EOF
```

## ASCII art on vim (unicode)

+ https://vi.stackexchange.com/questions/10087/

``` markdown
        0 	1 	2 	3 	4 	5 	6 	7 	8 	9 	A 	B 	C 	D 	E 	F
U+250x 	─ 	━ 	│ 	┃ 	┄ 	┅ 	┆ 	┇ 	┈ 	┉ 	┊ 	┋ 	┌ 	┍ 	┎ 	┏
U+251x 	┐ 	┑ 	┒ 	┓ 	└ 	┕ 	┖ 	┗ 	┘ 	┙ 	┚ 	┛ 	├ 	┝ 	┞ 	┟
U+252x 	┠ 	┡ 	┢ 	┣ 	┤ 	┥ 	┦ 	┧ 	┨ 	┩ 	┪ 	┫ 	┬ 	┭ 	┮ 	┯

U+253x 	┰ 	┱ 	┲ 	┳ 	┴ 	┵ 	┶ 	┷ 	┸ 	┹ 	┺ 	┻ 	┼ 	┽ 	┾ 	┿

U+254x 	╀ 	╁ 	╂ 	╃ 	╄ 	╅ 	╆ 	╇ 	╈ 	╉ 	╊ 	╋ 	╌ 	╍ 	╎ 	╏
U+255x 	═ 	║ 	╒ 	╓ 	╔ 	╕ 	╖ 	╗ 	╘ 	╙ 	╚ 	╛ 	╜ 	╝ 	╞ 	╟
U+256x 	╠ 	╡ 	╢ 	╣ 	╤ 	╥ 	╦ 	╧ 	╨ 	╩ 	╪ 	╫ 	╬ 	╭ 	╮ 	╯
U+257x 	╰ 	╱ 	╲ 	╳ 	╴ 	╵ 	╶ 	╷ 	╸ 	╹ 	╺ 	╻ 	╼ 	╽ 	╾ 	╿

for example ┍ is inserted pressing <c-v> u250D
for example ┑ is inserted pressing <c-v> u2511
for example ━ <c-v> u2501
for example ┯ <c-v> u252F
for example ┷ <c-v> u2537
for example │ <c-v> u2502
for example ✓ <c-v> u2713
check box ☑  <c-v> u2611
non-break space <c-v> uA0
em dash —  <c-v> u2014
right arrow <c-v> u2192 →
```

These symbols appear when you type 'ga' after Hex

    For substitution we can use :s/+/\="\u250D"/g

    ex -sc '%s/ /\="\x19"/g|x' hello.txt

    Transform this --------- Into this —————————
    :.s/-\+/\=substitute(submatch(0), "-", "\u2014", "g")

    substitute x for n times x
    :%s/X/\=repeat(submatch(0), 10)/gc

## comment string

+ https://vi.stackexchange.com/a/5539/7339
Alterar a string de comentário para ocultar linhas comentadas

    setlocal commentstring=#%s

## Edit a file as root preserving your environment variable vim config etc

    sudoedit file

## How do you comment out all non-blank non-commented lines in a file?
+ https://stackoverflow.com/a/59805775/2571881

    :%s/#@!./# &

For more regex tricks see :h pattern-overview

```vim
 	        lookbehind 	lookahead
positive 	(atom)\@<= 	(atom)\@=
negative 	(atom)\@<! 	(atom)\@!

In very magic mode:
	       lookbehind 	lookahead
positive 	(atom)@<= 	(atom)@=
negative 	(atom)@<! 	(atom)@!
```

## Getting the filename and extension

You can use expand(), see :h expand()

In a script you could do this to get file-name:

``` vim
let file_name = expand('%:t:r')
inoreabbrev fname <C-R>=expand("%:t")<CR>
inoreabbrev Fname <C-R>=expand("%:p")<CR>
```

To get extension you could do:

    let extension = expand('%:e')

For just seeing the file name and its path press:

    1 Ctrl-g

## Using tab or spaces

``` vim
function! UseTabs()
    set tabstop=4     " Size of a hard tabstop (ts).
    set shiftwidth=4  " Size of an indentation (sw).
    set noexpandtab   " Always uses tabs instead of space characters (noet).
    set autoindent    " Copy indent from current line when starting a new line (ai).
endfunction

function! UseSpaces()
    set tabstop=2     " Size of a hard tabstop (ts).
    set shiftwidth=2  " Size of an indentation (sw).
    set expandtab     " Always uses spaces instead of tab characters (et).
    set softtabstop=0 " Number of spaces a <Tab> counts for. When 0, featuer is off (sts).
    set autoindent    " Copy indent from current line when starting a new line.
    set smarttab      " Inserts blanks on a <Tab> key (as per sw, ts and sts).
endfunction
```

Usage:

``` vim
:call UseTabs()
:call UseSpaces()
```

## Turn first letter into uppercase

Lot of vi/vim tips and tricks are available at thegeekstuff.com. reading
these articles will make you very productive. following activities can be done very easily using vim editor.

    a. source code walk through,
    b. record and play command executions,
    c. making the vim editor as ide for several languages,
    d. and several other @ vi/vim tips & tricks.

    :%s/\v\. \w/\=toupper(submatch(0))

We can also do:

    :%s/\v\. \w/\u&

## Substitution with condition
+ https://nikodoko.com/posts/vim-substitute-tricks/

Transform this:

    {a:a, b:b, c:c}

Into this:

    {
    a:a,
    b:b,
    c:c
    }

%s/\v(\w+:\w+%(,|\}))/\="\n " . substitute(submatch(1), "}",  "\n}", "")/g

## How to replace all the same text inside a blocked text in vim?
+ https://stackoverflow.com/a/62117471/2571881

Let's say you have this code:

``` markdown
dayHour: {
    sunday: {
      openHour: null,
      closeHour: null,
    },
    monday: {
      openHour: null,
      closeHour: null,
    },
    tuesday: {
      openHour: null,
      closeHour: null,
    },
    ...
  }
```
And you want something like this:

``` markdown
dayHour: {
    sunday: {
      openHour: null,
      closeHour: null,
      openMenu: null,
      closeMenu: null
    },
    monday: {
      openHour: null,
      closeHour: null,
      openMenu: null,
      closeMenu: null
    },
    tuesday: {
      openHour: null,
      closeHour: null,
      openMenu: null,
      closeMenu: null
    },
    wednesday: {
      openHour: null,
      closeHour: null,
      openMenu: null,
      closeMenu: null
    },
  }
```

Solution:

    If your replacement text is already yanked into a register (clipboard in
    our example), you can put it as well:

    :g/closeHour/normal! "+]p

We can also achieve this with:

    :g/closeHour/exec "normal!oopenMenu: null, \<Enter>closeMenu: null"

or:

    /openHour\_.\{-}clos.*

    :%s//\=submatch(0) . "\r          " . substitute(submatch(0), 'Hour', 'Menu', 'g')/g

## conceal chars (unicode symbols)

    " syntax match arrow "->" conceal cchar=→
    " syntax match rcomp ">>" conceal cchar=»
    " syntax match lcom "<<" conceal cchar=«

## Insert a range of variables (each per line) to a template
+ https://www.youtube.com/watch?v=3yN6Q8I5KJA

Let's say you want to substitue VPN on the bellow block for a list of names like as follows. Each block will receive on each VPN one of those names from the list. The block is on register 'a'. The list of variables in on separete file.

```vim
{ "VPN"; outras palavras, outros valores, coisas diferentes; um segundo endereço: "VPN"; mais valores, mais palavras; e finalmente um terceiro: "VPN"}

name1
name2
name3
name4

:%s/.*/\=substitute(@a, 'VPN', submatch(0), 'g')/g
```

In order to join all lines:

    :g/./-1 normal! J

Join lines with comma separated

Given:

    8
    9
    10
    32

I'd like to end up with:

    8, 9, 10, 32

or:

    8,9,10,32

Solution:

    :1,$-s/\n/,
    :%!paste -sd,

## substitute on the clipboard register

There is a part of an url I want to remove, from "weekly" until the end,
so:

    let @+=substitute(@+, 'weekly.*', '', 'g')

## place the cursor after pasted text
+ https://stackoverflow.com/a/13317615/2571881

    gp .............. cursor after pasted bellow
    gP .............. cursor after pasted above

## find the end of the file
+ https://stackoverflow.com/a/7496112/2571881

    /%$

One use of this is to remove consecutive blank lines at the end of the file

    :%s/\v($\n\s*)+%$/\r

    \v ............. very magic (avoid slashes)
    () ............. regex group
    +  ............. at least once
    %$ ............. end of the file

## function to list all functions
+ https://stackoverflow.com/a/60693509/2571881
+ https://stackoverflow.com/questions/20933836

``` vim
function! Matches(pat)
    let buffer=bufnr("") "current buffer number
    let b:lines=[]
    execute ":%g/" . a:pat . "/let b:lines+=[{'bufnr':" . 'buffer' . ", 'lnum':" . "line('.')" . ", 'text': escape(getline('.'),'\"')}]"
    call setloclist(0, [], ' ', {'items': b:lines})
    lopen
endfunction

call Matches('\v^(\s*)?fun(ction)?!+.*(.*)( abort)?$')

We can use   :ll 3   to jump to the third match
or :lne[xt] :lp[revious]  :lr[ewin] :lla[st]
:lli[st]  mostra somente a lista

To reopen the local list use:
:lf[file]
```

## Sum fields in a file (somar campos)

    Children's Toys 49.50
    Bread 3.75
    What I owe John 6.20

    :r!awk '{t+=$NF} END {print "Total: "t}' %

## Difference between soft and hard wrapping
+ https://bit.ly/3dC4fN4

A hard wrap inserts actual line breaks in the text at wrap points, with soft wrapping the actual text is still on the same line but looks like it's divided into several lines.

If you want hard wrapping, i.e. real reformatting of the text by inserting newline characters, the 'textwidth' setting (as outlined in suspectus's answer) is the key. You need to reformat existing text, e.g. via the gq command.

## Check mappings
+ https://stackoverflow.com/a/26307213

    :let foo = maparg('<F6>', 'n')

    if mapcheck('<F1>', 'n') is# ''
        nnoremap <F1> :call myplugin#action()<CR>
    endif

we can also check this way (you can :ctrl-r ctrl-l) to inset the line below on the command line.

    ec maparg('<Leader>c')

It shows mappings in all modes

And hasmapto() checks if the given commands appears in the right-hand-side of a mapping, allowing you to map an action only if the user didn’t map it to something else:

    :echo hasmapto(':set wrap!<CR>', 'n')
    1

    :echo hasmapto(':set cursorline!<CR>', 'n')
    0

    if !hasmapto('myplugin#action', 'n')
        nnoremap <Leader>d :call myplugin#action()<CR>
    endif

## Run current line as a command

    :<Ctrl-r> <Ctrl-l>

## Get nvim version
+ https://vi.stackexchange.com/a/9942/7339

    :let var = matchstr(execute('version'), 'NVIM v\zs[^\n]*')

## Converter para maiúsculas em marcadores markdown

    :%s/+ ./\U&

## Making vim default manpager

    export MANPAGER="vim -M +MANPAGER -"

OBS: I had some problems with this, so I don't recommend using it.

## Translating selection with "trans"
If you have the program trans you can run

    :'<,'>!trans -b

### Using [trans](https://github.com/soimort/translate-shell) to translate part of the line
+ https://stackoverflow.com/questions/61176237

``` python
some_variable = True #循迹红外传
```

``` vim
:1s/\v([^#]*&[^"']|^)\zs#.+\ze/\=system('trans -b :en "'. submatch(0) . '"')

/\v([^#]*&[^"']|^)\zs#.+\ze/

A better search for Chinese characters

/[^\x00-\xff]\+

:1s//\=system('trans -b :en "'. submatch(0) . '"')
```

## Help to add incrementing numbers
+ [Reddit question](http://bit.ly/2SDo7pz)

Let's say you have these lines bellow (and want to create a sequence after the id)

``` txt
- :id:
:name: Aathira
:location: Thampanoor
- :id:
:name: Ajantha
:location: Chalai
- :id:
:name: Anjaly
:location: Thampanoor
```

Solution:

``` vim
gg
:let @r=0 | g/:id:/ let @r+=1 | norm $"rp

:let c=0 | g/id:\zs/ let c+=1 | s//\=" " . c
```
## Adding numbers to html sections

16 is the chapter number, for each h2 start vim will use a counter
``` markdown
let c=0 | g/<h2[^>]*>\zs[^<]*\ze/ let c+=1 | s//\='16.' . c . ' - ' . submatch(0)
```

## Create directory on vim
+ https://stackoverflow.com/a/43880028/2571881

    if !isdirectory($HOME . "/.vim/backup")
        call mkdir($HOME . "/.vim/backup", "p", 0700)
    endif

    :nmap <silent> <Leader>md :!mkdir -p %:p:h<CR>

## another increasing number sequence

Let's say you have this (consider a Huge file with a pattern like this)

    required string query = 1;
    optional int32 pagenumber = 32;
    optional optional result_per_page = 93;
    optional Corpus corpus = 4;
    required string url = 5;

And you want something like this

    required string query = 1;
    optional int32 pagenumber = 2;
    optional optional result_per_page = 3;
    optional Corpus corpus = 4;
    required string url = 5;

The solution

    gg
    :let @r=0 | g/./ let @r+=1 | exec "norm t;ciw\<c-r>r"

This is a sorter version

    :let c=0 | g/\d\+\ze;/ let c+=1 | s//\=c

    ADVANTAGES ARE: Use of variable instead of register,
    use substitution instead of normal command

The vim wiki solution

    " Add argument (can be negative, default 1) to global variable i.
    " Return value of i before the change.
    function! Inc(...)
        let result = g:i
        let g:i += a:0 > 0 ? a:1 : 1
        return result
    endfunction

    :let i=1 | %s/\d\+\ze;$/\=Inc()

    let g:i += a:0 > 0 ? a:1 : 1

if the first argument is bigger than zero `a:0 > 0` then use the second one, otherwise use 1

A simpler function with only positive increasing

    let g:i = 1
    func! Inc()
        let result = g:i
        let g:i += 1
        return result
    endfun

## Another increasing numbers solution
+ https://vi.stackexchange.com/a/22516/7339

Transform this

    Irish Bands:
    1. U2
    2. The Cranberries
    3. The Dubliners
    English Bands:
    1. Queen
    2. Duran Duran
    3. The Beatles
    American Bands:
    1. Nirvana
    2. Blondie
    3. The Doors

Into this:

    Irish Bands:
    1. U2
    2. The Cranberries
    3. The Dubliners
    English Bands:
    4. Queen
    5. Duran Duran
    6. The Beatles
    American Bands:
    7. Nirvana
    8. Blondie
    9. The Doors

Solution:

    :let c=0 | g/^\d\+\ze\./ let c+=1 |s//\=c

Explanation:

    let c=0|g/^\d\+\ze\./let c+=1|s//\=c
    ├─────┘ ├───────────┘├──────┘ ├─┘├─┘
    │       │            │        │  └ with the current value of the counter (i.e. new number)
    │       │            │        └ replace last used pattern (i.e. old number)
    │       │            └ for every matched line, increment the counter
    │       └ iterate over the lines starting with a number followed by a dot
    └ initialize counter to 0

## Virtual replace mode

When you use 'gR' on vim you (normal mode) you are in virtual replace mode
and this helps because it does not interfere with the subsequent characters

##  How to copy multiple lines and paste multiple times under each line separately?
+ https://stackoverflow.com/questions/64241250

    :%!awk '{while(++i<257)print;i=0}'

## vim sessions
+ https://grantwinney.com/so-vault-what-are-your-favorite-vim-tricks/

Answer by Lucas Oman (Sep 18, 2008)

I have the following in my vimrc:

``` viml
nmap <F3> <ESC>:call LoadSession()<CR>
let s:sessionloaded = 0
function LoadSession()
  source Session.vim
  let s:sessionloaded = 1
endfunction
function SaveSession()
  if s:sessionloaded == 1
    mksession!
  end
endfunction
autocmd VimLeave * call SaveSession()
```

When I have all my tabs open for a project, I type :mksession. Then, whenever
I return to that dir, I just open vim and hit F3 to load my "workspace".

## mapping more than one mode at once:
+ https://stackoverflow.com/questions/61263501

In this answer, the author illustrates how to use a for loop to map to more than one mode at the same time:

``` vim
for map_command in ['nnoremap', 'inoremap']
    execute map_command . ' <silent> Ó <Esc>vb'
    execute map_command . ' <silent> Ò <Esc>vel'
    execute map_command . ' <silent> Ô <Esc>vj'
    execute map_command . ' <silent>  <Esc>vk'
endfor
```

And in this answer, a wrapper function is used to achieve the same:

``` vim
function! MapBoth(keys, rhs)
    execute 'nnoremap' a:keys a:rhs
    execute 'inoremap' a:keys a:rhs
endfunction

call MapBoth('Ó', '<Esc>lvb')
call MapBoth('Ò', '<Esc>lve')
call MapBoth('Ô', '<Esc>vj')
call MapBoth('', '<Esc>vk')
```

With these solutions, you will only have to define the mappings once.

Disclaimer: I have not tried the solutions above, but please let us know if they do not work for you.

## Display last command output

    g<

## Repeat last substitution
+ https://vimtricks.substack.com/p/vimtrick-repeat-the-last-substitution

```viml
    :& - Repeats last substitution but resets the flags. Also works with just :s.
    :&& - Repeat last substitution with the same flags.
    :%& - Repeat last substitution on entire file, reset flags.
    :%&& - Repeat last substitution on entire file, keep flags.
```

## speeding up macros

    :set lazyredraw

## Calling functions inside scripts
+ https://vim.fandom.com/wiki/How_to_write_a_plugin

``` vim
fun! NameOfScriptFile#FunctionName()
    code
endfun
```

Thus maps and commands can use the "NameOfScriptFile#FunctionName()" format
for their functions in the plugin portion; when the user invokes the command
or map, vim will only then load the autoload/NameOfScriptFile.vim script. I
heartily recommend that plugin writers avail themselves of this method!

## Remove a particular duplicated line

    :sort u

## Decrease numbers less than 100 - condicionally replace numbers
+ https://stackoverflow.com/questions/60199331

``` vim
:%s/-[0-9]\+/\=submatch(0) < -100 ? -98 : submatch(0)/g
```

In the above vim command we are using a ternary operator
to use either -98 in case it matches -100 or the submatch itself

Decrease numbers less or equal to 9

    :%s/\v\d+/\=submatch(0) <= 9 ? submatch(0)-5 : submatch(0)

## more submatch

If you have names ending like this you can generate something like s01e01

    https://transcripts.fandom.com/wiki/The_Cruciferous_Vegetable_Amplification

    let c=0 | 5,21g/wiki\/\zs/ let c+=1 | s//\=' s01e' . printf('%02d', c) . '-'

## Running two commands at once:

    :execute "normal! mz" | execute "g/cockatoo/m'z"

## Inverting two words all over the file
+ https://vi.stackexchange.com/a/9090/7339

    :%s/foo\|bar/\={'foo':'bar','bar':'foo'}[submatch(0)]/g

    :%s/foo\|bar/\=submatch(0) == "foo" ? "bar" : "foo"/g

## Substitute expression by its result
+ https://www.reddit.com/r/vim/comments/c15r94/

Let's say yo have

    [tejxt text text] (5-10)
    [text text text] (40-58)

And yo want:

    [text text text] (5)
    [text text text] (18)

:%s/\v(\d+)-(\d+)/\=submatch(2)-submatch(1)

## Substitute date with submatch
+ https://jeffkreeftmeijer.com/vim-reformat-dates/

Let's say you have dates like: `2020-10-17`
and you want something like: `october 17, 2020`

    :%s/....-..-..\ze</\=system('date -d "'.submatch(0).'" +"%B %d, %Y"')/g

Or if you have something like:

    Time in hours, minutes and seconds which I want to substrat 00:04:01
    Time in hours, minutes and seconds which I want to substrat 00:05:01
    Time in hours, minutes and seconds which I want to substrat 00:06:12
    Time in hours, minutes and seconds which I want to substrat 00:14:01
    Time in hours, minutes and seconds which I want to substrat 00:00:01

And you want to decrease 10 seconds in every match:

    :%s/00.*\ze$/\=system('date -u -d "'.submatch(0).' +0000 -10 sec" +"%H:%M:%S"')/g

## Change book index

    A......Page 23
    by John Smith
    B......Page 73
    by Jane Doe
    C......Page 131
    by Alice Grey

    :%s/Page \zs.*/\=submatch(0)-22

    A......Page 1
    by John Smith
    B......Page 51
    by Jane Doe
    C......Page 109
    by Alice Grey

Another option:

    :g/\d\+$/exec "normal $22\<c-x>"

## Increasing numbers only where a pattern appears

    :%g/hello/s/50/70

    :%g/hello/exec "norm 10\<C-a>"

## Echo with ternary operator

    :echo has('clipboard') == 1 ? "yes" : "no"

## how to make a search in a vimscript

    :execute "normal /pattern/e+1\<CR>"
    :exec "normal mz/01:07h00\<Enter>jVnkd`z"

In this example we are deleting lines between a pattern

    function RemoveInBetween()
        "marcar a posição do cursor para
        normal mz
        execute "normal /pattern/+1\<cr>Vnkkd"
        " Retornar à posição original
        normal `z
    endfun

## Insert a file in a specific place

    :g/^<inputs>/+,/^<\/inputs>/-d|-r input-file.txt
    :/<inputs>/+1,/<\/inputs>/-1!cat foo.txt

## copy pattern to register
+ http://bit.ly/2P3tBJf

    :let @a=''
    :[range]g/pattern/y A

But sometimes it is easier to copy the content to a file

    :[range]g/pattern/.w! filename

## Conditional mapping
+ https://www.reddit.com/r/vim/comments/5cz685/
+ https://stackoverflow.com/a/60304236/2571881

    inoremap <expr> <tab> (getline('.') =~ '^\s*$' ? '<tab>' : '<c-p>')

    inoremap <expr> <cr> getline(".")[col(".")-2:col(".")-1]=="{}" ? "<cr><esc>O" : "<cr>"

    inoremap <expr> <CR>
        \ col('.') ==# col('$')
        \ && getline('.')[col('.')-2] ==# '{'
        \ ? "\<CR>}\<C-O>O"
        \ : "\<CR>"

In the above mapping we are mapping `<CR>` only if the cursor it is at the end
of the line and we have brackets before that

## What are {lhs} and {rhs}
+ http://bit.ly/2V8CaWU

{lhs} left hand side - That’s where you define the shortcut or the key(s)
you’re gonna use. It can be a single key like , or a sequence of keys like
,<space>.

{rhs} right-hand-side — That's where you define what will your shortcut
replace/execute when you press the key(s) defined at {lhs}

## Excluding filetype from autocmd
+ https://stackoverflow.com/a/60338380/2571881

    autocmd WinEnter,BufEnter * if &ft != "md" | do things

## Translate word under cursor

    :execute "!trans -b " . expand("<cword>")
    command! Translate execute "!trans -b " . expand("<cword>")

    :'<,'>!trans -b :pt

## Using grep

    :silent grep! 'tags:.*vim'
    :command! -bar -nargs=1 Grep silent grep <q-args> | redraw! | cw

# vim: ff=unix ft=markdown ai et ts=4 syntax=off
