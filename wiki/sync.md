---
file: sync.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


Sincroniza discos, é usado geralmente antes de desmontar
um disco para segurança de dados.

também é usado após o uso do comando [[ComandoDd|dd]]
