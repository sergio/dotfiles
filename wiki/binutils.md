---
Filename: binutils.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [debian, metapackage, utils, linux]
 vim:ft=markdown
---

## Programs in binutils:

ar

## Meta-packages in a nutshell

A ‘meta-package’ is a convenient way to bulk-install groups of applications, their libraries and documentation. Many Linux distributions use them for a variety of purposes, from seeding disk images that will go on to become new releases, to creating software “bundles” that are easy for a user to install. A meta-package rarely contains anything other than a changelog and perhaps copyright information, it contains no applications or libraries within itself. The way they work is by having a list of “dependencies” that the package manager reads. The package manager then goes to the repositories to find the dependencies and installs them.

The programs in this package are used to assemble, link and manipulate binary
and object files. They may be used in conjunction with a compiler and various
libraries to build programs.
