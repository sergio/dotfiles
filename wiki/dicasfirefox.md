---
file: ~/.dotfiles/wiki/dicasfirefox.md
author: Sergio Araujo
Last Change: Sat, 18 Jan 2025 - 00:05:16
tags: [firefox]
---

## Alternatives for firefox:

+ https://rigorousthemes.com/blog/best-firefox-alternatives/
+ https://archive.mozilla.org/pub/seamonkey/releases/2.53.10.2/linux-i686/pt-BR/
+ https://ftp.gnu.org/gnu/gnuzilla/60.7.0/ (gnu icecat)
+ https://librewolf.net/

## Better font rendering for firefox

```sh
sudo ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/ 
sudo xbps-reconfigure -f fontconfig 
```

## Increase security using firejail

## Baixar áudio usando o web inspector:

Na aba Network clique em "media", atualize a página e abra o audio em uma nova
aba.

## DNS over https:

+ https://support.mozilla.org/en-US/kb/firefox-dns-over+https

## alternatives to google:

+ https://startpage.com/

## Remove some anoying urls from your search:

    trashurl !g -inurl:https://www.libhunt.com -inurl:https://githubmemory.com -inurl:https://awesomeopensource.com -inurl:https://issueexplorer.com -inurl:https://giters.com -inurl:https://reposhub.com -inurl:https://github-wiki-see.page

    awk '/pattern/ {$1=""; print}' ~/.dotfiles/wiki/dicasfirefox.md | pbcopy

## Firefox send (share files)

+ https://send.firefox.com/

Encrypt and send files with a link that automatically expires to ensure your important documents don’t stay online
forever.

## References

+ https://www.maketecheasier.com/speed-up-firefox/
+ https://www.askvg.com/ultimate-collection-of-best-aboutconfig-tweaks-for-mozilla-firefox-4-0/

## How to enable Firefox mobile view from desktop?

+ https://bit.ly/3bqjmHK

Launch Firefox browser on your Windows computer. Visit the website or web page you like to check. Click the Menu at the
top right, choose Web Developer from the drop-down menu, then select Responsive Design Mode from the submenu.

    Ctrl + Shift + M

## get hidden media from webpages

On the tab network of firefox inspector, play the media and copy the url

## firefox Couldn't load XPCOM. [solution] libvpx.so.5

+ https://www.reddit.com/r/voidlinux/comments/belxs8/firefox_couldnt_load_xpcom_solution/

Hi, if like me you experience this problem after update your system, here is the solution:

    sudo ln -s /usr/lib/libvpx.so.5.0.0 /usr/lib/libvpx.so.5

## Replace firefox-esr (extended release support)

+ https://linuxconfig.org/how-to-install-latest-firefox-browser-on-debian-9-stretch-linux

voidlinux users should run:

    sudo xbps-remove firefox-esr
    sudo xbps-install -S firefox

## Disable search suggestions

    browser.urlbar.searchSuggestionsChoice

## Configure Your Backspace Button

+ https://www.maketecheasier.com/28-coolest-firefox-aboutconfig-tricks/

In Firefox you can set your backspace by getting it to either go back to the previous page or scroll up a page if it’s a
scrolling site. Holding Shift as a modifier will go forward a page if the value is set to 0 and scroll down if the value
is set to 1.

    Config name: browser.backspace_action
    Default value: 0 – goes back a page
    Modified value: 1 – scrolls up a page

## Set firefor as browser

```markdown
xdg-mime default firefox.desktop x-scheme-handler/http xdg-mime default firefox.desktop x-scheme-handler/https
```

The above set will create an entry on:

    ~/.config/mimeapps.list

## To know your default broser just type:

    xdg-settings get default-web-browser

## How to access foruns with no login

+ http://www.kerkeberos.net/2009/12/10/visite-qualquer-site-ou-forum-sem-autenticacao/

## How to Make Separate User Profiles in Firefox Quantum

+ https://www.downloadsource.net/how-to-create-multiple-user-profiles-in-firefox-quantum/n/10916/

  about:profiles

## How to install Firefox quantum on debian

+ https://linuxconfig.org/how-to-install-firefox-quantum-on-debian-stretch-linux

Edit the file

    /etc/apt/preferences.d/my_preferences

There are three dependencies that you'll need to pin in addition to Firefox. Set each of them up too.

    Package: firefox
    Pin: release a=unstable
    Pin-Priority: 1001

    Package: libfontconfig1
    Pin: release a=unstable
    Pin-Priority: 1001

    Package: fontconfig-config
    Pin: release a=unstable
    Pin-Priority: 1001

    Package: libnss3
    Pin: release a=unstable
    Pin-Priority: 1001

## Install Firefox

Finally, you're ready to install Firefox. You just need to update Apt, and tell Apt to install Firefox from Sid.

    # apt update
    # apt install -t sid firefox

```sh
	cat <<-EOF> | sudo tee /usr/share/applications/firefox-quantum.desktop
	[Desktop Entry]
	Version=46.0.1
	Encoding=UTF-8
	Name=Mozilla Firefox
	Comment=Navegador Web
	Exec=/opt/firefox-quantum/firefox
	Icon=/opt/firefox-quantum/browser/icons/mozicon128.png
	Type=Application
	Categories=Network
	EOF
```

## Suavização de fontes

    gfx.use_text_smoothing_setting

## Disable icecat "Reveal hidden HTML"

GNU IceCat comes with a "Reveal hidden HTML" extension that will frequently show a pop-up claiming there is somehow
"Hidden HTML" on page which can be "revealed". There is typically nothing to "reveal". This extension will just annoy
you regularly and never do anything useful. It is just an huge annoyance.

## Enabling firefox multiprocessing on ubuntu

Disable ubuntu-modifications addon and ...

    browser.tabs.remote.autostart
    true

## Chat gpt addon:

+ https://addons.mozilla.org/en-US/firefox/addon/web-chatgpt/
+ https://addons.mozilla.org/en-US/firefox/addon/chatgpt-for-google/

## https://addons.mozilla.org/en-US/firefox/addon/firelux/

Reduces the color temperature of the browser by adding a filter on web pages.
This addon will protect your eyes against blue light.

Reduzir tempreratura de cor para progeger seus olhos

## Download firefox pt-br

+ https://superuser.com/a/322381

  br.mozdev.org

  sudo tar -jxvf firefox.xx.xx.tar.bz2 -C /opt

  sudo chown -R root:users /opt/firefox

  sudo chmod 750 /opt/firefox

  usermod -a -G users sergio

  sudo ln -sfvn /opt/Firefox/firefox /usr/bin/firefox

  touch /usr/share/applications/firefox.desktop

  touch /usr/share/applications/firefox.desktop

  vim /usr/share/applications/firefox.desktop

```markdown
[Desktop Entry] Encoding=UTF-8 Name=Mozilla Firefox Comment=Browse the World Wide Web Type=Application Terminal=false
Exec=/usr/bin/firefox %U Icon=/opt/firefox/icons/mozicon128.png StartupNotify=true Categories=Network;WebBrowser;
```

## Privacy Badger

+ https://www.eff.org/privacybadger

## Sponsor block

+ https://addons.mozilla.org/en-US/firefox/addon/sponsorblock/

## Dark reader (Dark mode for every website)

+ https://addons.mozilla.org/en-US/firefox/addon/darkreader/

## DownThemAll addon para baixar tudo

+ https://addons.mozilla.org/en-US/firefox/addon/downthemall/

## Simple translate, a cool addon for google translate

+ https://addons.mozilla.org/vi/firefox/addon/simple-translate/

By default use Ctrl + Space to show a popup with the translation

## Protect Eyes with Night Mode Eye Guard for Firefox addon

+ https://addons.mozilla.org/en-US/android/addon/eyeguard/

## performance addon (extension)

+ https://addons.mozilla.org/en-US/firefox/addon/speed-tweaks-webextension/
+ https://addons.mozilla.org/en-US/firefox/addon/auto-tab-discard/

## Extension to control screen brightness

+ https://www.thewindowsclub.com/night-mode-page-dim-chrome-firefox

## Firefox addon "extension" to control your browser with vim keybindings

    Tridactyl
    https://addons.mozilla.org/en-US/firefox/addon/tridactyl-vim/

    :tutor
    :set newtab https://duckduckgo.com/
    b brings up a list of your current tabs. Press Tab/Shift-Tab to cycle through them and enter to select.

## Language reactor (two subtitles at once)

+ https://www.languagereactor.com/
+ https://youtu.be/-67SsFntNLk

## Enhanced youtube addon:

+ https://www.mrfdev.com/how-to-use-enhancer-for-youtube

## Navegaçao privada - Private browsing mode

    Ctrl + Shift + p

## Firefox quantum performance tricks

+ https://www.maketecheasier.com/speed-up-firefox/

To turn off accessibility features in Firefox, go to “Settings -> Options -> Privacy & Security,” then tick the box
labelled “Prevent accessibility services from accessing your browser.”

## hide title bar

+ https://askubuntu.com/a/1068102/3798

  Customize > Hide Title Bar

## How to Customize Firefox’s User Interface With userChrome.css:

+ https://www.howtogeek.com/334716/how-to-customize-firefoxs-user-interface-with-userchrome.css/

Update: Since Firefox 69, you must go into about:config and set “toolkit.legacyUserProfileCustomizations.stylesheets” to “true” to enable these customizations. If you don’t, Firefox will ignore your userChrome.css and userContent.css files.

The userChrome.css file has to go within that <randomstring>.default folder

    userChrome.css

## Enable compact mode:

+ https://www.omgubuntu.co.uk/2021/06/dont-like-firefoxs-new-look-try-one-of-these-tweaks

If the size of Firefox’s new floating tabs is your biggest gripe there is a simple and effective “change” you can make : enable compact mode.

“Joey,” you say, with a huff of incredulity, “I looked for that option first, and it isn’t there!” — and you’re right; in Firefox 89 the the “compact” density option is missing in the Firefox Customize toolbar

But you can still enable it — though the the option is being removed in future builds. How?

Open about:config in a new tab and dismiss the notice that appears. Then search for:

    browser.uidensity

At the far end of the row double click the numerical value and replace it with ‘1‘.

## Change firefox user-agent

+ https://www.deviceinfo.me/

    about:config

Create a new string:

    general.useragent.override

The string below will set your firefox to appear like opera 46

    Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko)
    Chrome/59.0.3053.3 Safari/537.36 OPR/46.0.2573.0 (Edition developer)

Restore firefox 54 user-agent

    Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:54.0) Gecko/20100101 Firefox/54.0

Firefox 70 user-agent

    Mozilla/5.0 (X11; Linux AMD64; rv:70.0.0) Gecko/20100101 Firefox/70.0.0

Identify as chrome

    Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like Gecko)
    Ubuntu Chromium/58.0.3029.110 Chrome/58.0.3029.110 Safari/537.36

## Addon to avoid site tracking

+ https://addons.mozilla.org/en-US/firefox/addon/disconnect/

## New speeding tab feature

Tab Warming: How Firefox Will Improve Web Browsing Experience? How To Get It Now?

How to enable tab warming in Firefox?

As you know, tab warming is currently under development. So, you can test the same in Firefox Nightly builds.

Go to about:config. Accept the warning message if it appears. Search for a preference called:

    browser.tabs.remote.warmup.enabled

## Better privacy settings

+ https://nordvpn.com/pt-br/blog/o-que-e-a-webrtc/
+ https://restoreprivacy.com/firefox-privacy/
+ https://bit.ly/3kyQ5yC

  media.peerconnection.enabled "false" dom.event.clipboardevents.enabled = false geo.enabled = false media.eme.enabled =
  false media.navigator.enabled = false media.peerconnection.enabled = false network.dns.disablePrefetch = true
  network.http.sendRefererHeader = 0 network.prefetch-next = false privacy.firstparty.isolate = true
  privacy.resistFingerprinting = true privacy.trackingprotection.enabled = true webgl.disabled = true

## Disable the battery API

    dom.battery.enabled "false"

## Make firefox speak English

[us language pack](https://addons.mozilla.org/en-US/firefox/addon/english-us-language-pack/)

## Select All Text When You Click on the URL Bar

+ https://www.maketecheasier.com/28-coolest-firefox-aboutconfig-tricks/

  browser.urlbar.clickSelectsAll - true browser.urlbar.doubleClickSelectsAll - false

## clear firefox memory usage

source: http://www.online-tech-tips.com/computer-tips/speed-up-mozilla-firefox/

    about:memory
    about:support

## user.js [[FirefoxUserProfile]]

+ https://github.com/arkenfox/user.js

First discover yout default profile on firefox, just run -> about:profiles and search for default profile "yes"

search for "Root Directory", copy this path and go there:

Unpack the firefox user.js and copy everything to your Root Profile:

    cp *.* <your root profile>

After reloading your firefox access: https://www.deviceinfo.me/

    Change:
    security.ssl3.rsa_des_ede3_sha to false
    network.http.sendRefererHeader	  0

Fix: "Hmm. That address doesn’t look right."

    keyword.enabled  true

## fix youtube comments if you have third part cookies bocked

add exceptions for this url's

    plus.googleapis.com
    google.com
    plus.google.com
    apis.google.com
    books.googleusercontent.com

## ativar plugins sob demanda

dentro do about:config

    plugins.click_to_play

## Plugins para o firefox quantum

+ https://addons.mozilla.org/en-US/firefox/addon/multi-account-containers/

O plugin acima permite logar em duas contas gmail, por exemplo, ao mesmo tempo ou seja, muiltilogin

    curl -O

'疘疤疤疠番畟畟疓疔疞畞疜疕疟疘畞疙疟畟疙疝疑疗疕疣畟疒疑疓疛疥疠疣畟疒疑疣疕疏疪疕疢疟畕畢畠疓疟疠疩畟疙疝疑疗疕疏疌疋
畡畝畣畠畩疌疍畞疚疠疗'

## Developer toolbar

+ http://www.hongkiat.com/blog/firefox-graphical-command-line/

  shift + f2

## desabilitar a checagem de virus

http://en.kioskea.net/faq/1686-optimize-firefox-completely

Launch Firefox In the address bar, type about:config

Confirm if needed In the filter, type safe In the Settings list, double-click on the following parameters and set them
to false:

browser.safebrowsing.enabled

browser.safebrowsing.malware.enabled Close Firefox and wait several seconds

In your Firefox profile, delete the file urlclassifier3.sqlite This poses no long-term risk - Firefox can recreate it if
required Restart Firefox You should see an improvement

browser.download.manager.scanWhenDone

## Carregar abas sob demanda apenas

coloque a chave

    browser.sessionstore.restore_pinned_tabs_on_demand

    com valor true

## channels firefox

http://www.mozilla.org/en-US/firefox/channel/#beta

## Reduce the amount of RAM Firefox uses for it’s cache feature

source: http://gnoted.com/3-hacks-for-firefox-double-internet-browsing-speed/

Here’s how to do it:

1. Type “about:config” (no quotes) in the adress bar in the browser.
2. Find “browser.sessionhistory.max_total_viewer”
3. Set it’s value to “0“;(Zero)

## Speed up Firefox by browser history

    browser.sessionhistory.max_entries

You will see:

    status: default
    Type: Integer
    Value: 50

This means that the browser keeps the history up to 50 URLs. You have to change it to a minimum value. For this double
click on it. This will opens a new pop-up message with value 50, now change 50 to 10. This will reduce the load to
browser

## Double Your Internet Browsing Speed by Defining Small Amount of Memory to Firefox

    browser.cache.memory.capacity

    -------------------------------------------
     put the value according to following table:
    -------------------------------------------
     RAM 	VALUE
     128MB 	2048
     256MB 	4096
     512MB 	8192
     1GB+ 	12288
    -------------------------------------------

## Boost Firefox by enabling its new HTTP cache: Set the Cache Capacity

    browser.cache.disk.capacity

    You will notice that; the value: 102400 (or something like this)
    Change it to 4096.

## Disable animations

Search for "animate" on about:config and set options to false

Disable Unnecessary Animations

Animations in Firefox Quantum are not a bad thing, but if you have an old PC where every MB of RAM counts or simply
don’t need these animated flourishes, you can disable them by going to toolkit.cosmeticAnimations.enabled and setting
the value to “false.”

    Default value: true
    Modified value: false

## Como instalar o plugin flash via rede local

    scp root@192.168.1.74:/usr/lib/mozilla/plugins/libflashplayer.so /usr/lib/mozilla/plugins

Você pode baixar o plugin no site da adobe e colocar o arquivo libflashplayer.so na pasta:

      /usr/lib/share/mozilla/plugins/

## como impedir o plugin flash de capturar cookies

    for i in ~/.adobe ~/.macromedia ; do ( rm $i/ -rf ; ln -s /dev/null $i ) ; done

## Plugins que uso no firefox

Feedly ...... plugin que gera um novo layout para as notícias do google reader e twitter:
https://addons.mozilla.org/pt-BR/firefox/addon/feedly/

    Turn on your pdf reader ......... pdfjs.disabled

## firefox cache in RAM

criar a seguinte chave (string) através do about:config

    browser.cache.disk.parent_directory

coloque o valor para /var/shm/firefox-cache

## Disable geo location

    geo.enabled = FALSE (prevents sites from pulling your GPS location)
    network.IDN_show_punycode = TRUE (prevent phishing attacks)

    privacy.firstparty.isolate = true

    The feature restricts cookies, cache and other data access to the domain
    level so that only the domain that dropped the cookie or file on the user
    system can access it.

If you wanna see or change the default use thie url

    about:config?filter=privacy.firstparty.isolate

## Hide your real email address to help protect your identity

+ https://relay.firefox.com/
+ https://addons.mozilla.org/en-US/firefox/addon/private-relay/

  bd2790fcv relay.firefox.com

Use a fake e-mail on firefox

## How do I change priority of firefox address bar suggestions to tags?

+ https://superuser.com/a/414387/45032

Type one of theses symbols at address bar before anything else and vuala:

```markdown
    about:config  browser.urlbar. (pref name)
    ┌─────────────┬───────────────┬─────────────────────────────────┐
    │ Pref Name   │  Default Key  │   Action                        │
    ├─────────────┼───────────────┼─────────────────────────────────┤
    │ title       │      #        │   Match text in the title       │
    │ url         │      @        │   Match text in the url         │
    │ bookmark    │      *        │   Results that are in bookmarks │
    │ history     │      ^        │   Results from history          │
    │ tag         │      +        │   tagged results                │
    │ typed       │      ~        │   Results tha have been typed   │
    │ openpage    │      %        │   Returns only open tabs        │
    └─────────────┴───────────────┴─────────────────────────────────┘
```
+ tag1 tag2
just add one space between + and each tag name

For example:

    url:   https://stackoverflow.com/questions/64695959/   -> @
    title: Vim how to move csv column                      -> #

Another useful example: Let's say you want to save, and afterwords access a useful website for printing websites (with
no adds) as a pdf. --> https://www.printfriendly.com/

Typing this `+pdf print` in your address bar will bring all tagged "pdf" sites who have the word print on its name.

## http cache resource

The Mozilla team is working on a number of performance tweaks for upcoming versions of Firefox, one of which is the use
of a new HTTP cache that should fix some UI hang issues. It isn't enabled by default yet, but will be soon—and you can
enable it right now with an about:config tweak. To do so:

    browser.cache.use_new_backend

Change its value from 0 to 1 to enable the new cache.

    network.dnsCacheEntries;500

    Find `network.dnsCacheEntries` and change its value to from 20 to 512.
    Find `network.dnsCacheExpiration` and change its value from 60 to 3600.
    If your ISP doesnâ€™t support `IPv6`, you can disable it. To do this just
    find network.dns.disableIPv6 and change it default value to True.
    Find `network.http.max-connections` and increase maximum number of simultaneous connections to 48 or 96.
    Find `network.http.max-persistent-connections-per-server` and increase maximum persistent connections per server from default 6 to 12
    Find `network.http.max-connections-per-server` and increase maximum connections per server from default 15 to 24

## Fix https issue

A brecha que simula sites e dribla o HTTPS é bem perigosa – e consegue enganar o Firefox
+ http://gizmodo.uol.com.br/https-ataque-homografico/

Escreva `about:config` na barra de endereços e pressione Enter; Escreva `Punycode` na barra de pesquisa; As
configurações do navegador irão exibir o `parâmetro network.IDN_show_punycode`, dê dois cliques e selecione a chave que
muda o valor de `false` para `true`.

http://pt.wikihow.com/Fazer-o-Firefox-Carregar-as-P%C3%A1ginas-de-Forma-Mais-R%C3%A1pida

also enable this: https://raw.githubusercontent.com/voyeg3r/dotfiles/master/bin/sysctrl.conf

## New tab opening home page

+ [New tab homepage addon](https://addons.mozilla.org/en-US/firefox/addon/777)

## open searches in new tab

By default, Firefox has a search box in the upper-right hand corner from which you can Google things with ease. When you
type in a search term, it opens the results in your current tab, which is annoying if you want to keep your current tab
open.

To change this behavior, open about:config and search for:

browser.search.openintab

## Preview Tabs in the Ctrl+Tab Switcher

      browser.ctrlTab.previews

# vim: ft=markdown et sw=4 ts=4 cole=0 nospell:
