---
File: bookdown.md
Author: Sergio Araujo
Last Change: Sat, 20 Apr 2024 - 07:47:01
Date: jan 12, 2021 - 16:23
tags: [markdown, bookdown, tools, e-book, epub, html]
---

#  What is bookdown?

The bookdown package is an [open-source R
package](https://github.com/rstudio/bookdown) that facilitates writing books and
long-form articles/reports with R Markdown. Features include:

   * Generate printer-ready books and ebooks from R Markdown documents.
   * A markup language easier to learn than LaTeX, and to write elements such as section headers, lists, quotes, figures, tables, and citations.
   * Multiple choices of output formats: PDF, LaTeX, HTML, EPUB, and Word.
   * Possibility of including dynamic graphics and interactive applications (HTML widgets and Shiny apps).
   * Support a wide range of languages: R, C/C++, Python, Fortran, Julia, Shell scripts, and SQL, etc.
   * LaTeX equations, theorems, and proofs work for all output formats.
   * Can be published to GitHub, bookdown.org, and any web servers.
   * Integrated with the RStudio IDE.
   * One-click publishing to https://bookdown.org.

## changing the output_dir:

+ [Reference](https://www.curso-r.com/blog/2019-08-28-bookdown/)

In the file `_bookdown.yml` add this line:

    output_dir: docs

## How to install bookdown on voidlinux?

    sudo xbps-install -Sy R

Now open R as root and run:

    install.packages("bookdown")

You can follow [this
tutorial](http://seankross.com/2016/11/17/How-to-Start-a-Bookdown-Book.html)
(it has an example repo). To compile your book you have to:

1 - Open R in terminal
2 - Run --> `bookdown::render_book("index.Rmd")`

We can also run `bookdown::render_book("index.Rmd", "bookdown::epub_book")`

## Tables

``` markdown
Table: Vim Modes

Mode    |How to Use It
:-------|:-------------
Normal  |<kbd>Esc</kbd>
Insert  |<kbd>i</kbd>,<kbd>I</kbd>,<kbd>a</kbd>,<kbd>A</kbd>
Visual  |<kbd>v</kbd>,<kbd>V</kbd>
Command |<kbd>Esc</kbd><kbd>:</kbd>
```

NOTE: the `<kbd>` block has its special CSS formatting, see [css file](css.md)

## Custom Blocks

```{block, type='FOO'}
Some text for this block.
```

``` markdown
will be this:
<div class="FOO">
Some text for this block.
</div>
```

and the LaTeX output will be this:

``` tex
\begin{FOO}
Some text for this block.
\end{FOO}
```

<!--
 vim: ft=markdown et sw=4 ts=4 cole=0 ft=markdown:
-->
