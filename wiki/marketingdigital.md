---
File: ~/.dotfiles/wiki/marketingdigital.md
Author: Sergio Araujo
Last Change: Thu, 26 Sep 2024 - 15:35:43
tags: [selling, tools, gig, imports]
---

#  marketingdigital.md intro:

+ [cssbuy](https://m.youtube.com/watch?v=JHRWeTszM_M) https://cssbuy.com/
+ [xianyu](https://2.taobao.com/) usa o chrome
+ https://www.youtube.com/watch?v=VAcKBKRmOQY (siglas)

Site com siglas e seus significados
+ https://clictadigital.com/top-100-common-digital-advertising-acronyms-abbreviations/
+ https://www.traycorp.com.br/conteudo/metricas-ads/

## cssbuy (Redirecionadores) agente de compras

taobao 1688 xianyu zhuanzhuan

[Tutorial](https://youtube.com/playlist?list=PLrvUfhgBj1mzjQAzoUG1v9AJesx35EnYF&si=K0L8LkHCJj8qI0hc)
[Another video](https://youtu.be/CrIFpW0nXeA?si=MU-ziMrmIB0TkK12)

## Compra no aliexpress

[Addon](https://www.ali-assist.com/)

## Avaliar popularidade de sites:

+ https://socialblade.com/

Neste site você cola a string de um canal e ele avalia a penetração do mesmo

Termos usados em Marketing Digital:

## SEO - Search Engine Optimization (https://www.tecmundo.com.br/blog/2770-o-que-e-seo-.htm)
Um bom site para gerar longtail keywords: (otimização de buscas não pagas)
+ https://neilpatel.com/br/ubersuggest/

são técnicas utilizadas em sites para serem melhor ranqueados nos mecanismos
de buscas, como o Google. Ou seja, quanto melhor as estratégias adotadas, mais
chances de um site aparecer logo nas primeiras páginas da pesquisa - e também
maior a probabilidade de acessos.

Tem também o SEM (Search Engine Marketing) neste caso está relacionado com
buscas pagas.

## Pay-per-click PPC
+ https://www.wordstream.com/ppc

PPC stands for pay-per-click, a model of internet marketing in which
advertisers pay a fee each time one of their ads is clicked. Essentially, it’s
a way of buying visits to your site, rather than attempting to “earn” those
visits organically.

Search engine advertising is one of the most popular forms of PPC. It allows
advertisers to bid for ad placement in a search engine's sponsored links when
someone searches on a keyword that is related to their business offering. For
example, if we bid on the keyword “PPC software,” our ad might show up in the
very top spot on the Google results page.

## O que é CPC?

+ https://revpanda.com/what-is-cpc-and-cpm-in-digital-marketing/
+ https://www.wordstream.com/ppc

CPC and CPM are some of the most important terminologies of PPC marketing (pay-per-click).
Most of the ads you see on the internet are one of these two. Regardless of
the platform, you use, either social media or search engine platforms, CPC and
CPM are handy acronyms that can help you improve your online revenue.

As one of the most critical terminologies of PPC marketing, CPC is the
abbreviation of “cost per click”. It stands for the amount you are supposed to
pay per each click you get on your ads. In other words, it is the answer to
how much it costs you to acquire a click on a platform.

## What is CTR (click through rate)
 What Is the Click-Through Rate (CTR)?

In online advertising, the click-through rate (CTR) is the percentage of
individuals viewing a web page who view and then click on a specific
advertisement that appears on that page.

## ROI - Return On Investmet

What Is Return on Investment (ROI)?

Return on investment (ROI) is a performance measure used to evaluate the efficiency or profitability of an investment or compare the efficiency of a number of different investments. ROI tries to directly measure the amount of return on a particular investment, relative to the investment’s cost.

## What is Customer Acquisition Cost (CAC)?

Customer acquisition cost (CAC) is the cost related to acquiring a new customer. In other words, CAC refers to the resources and costs incurred to acquire an additional customer.

## What is Google Ads?

Google Ads is the single most popular PPC advertising system in the world. The
Ads platform enables businesses to create ads that appear on Google’s search
engine and other Google properties.

## CRM Customer Relationship Management
É um software de gerenciamento de clientes, com contados, vendas etc.
(Software de gestão de clientes)

## LTKW (Long Tail Keywords)
+ https://ads.google.com/home/tools/keyword-planner/

These are the keywords that have more than 3 words.

## Obter vídeos de graça para republicar no youtube
+ https://www.pexels.com/
+ https://invideo.io/
+ https://www.storyblocks.com/ (it also has a video maker)
    (a good tutorial: https://www.youtube.com/watch?v=2iR54sYiKPY)

OBS: When creating a new video use 1920 x 1080

No youtube, ativando o filtro "creative commons", dá pra achar vídeos que
podemos republicar sem problemas com licenças.

## Plataforma nacional para anúncios (monetizze)
+ https://app.monetizze.com.br/loja

## Biblioteca de músicas do youtube
+ https://www.youtube.com/c/AudioLibraryEN/featured

## Os 4 C`s

O uso de "4 Ps" e "4 Cs" no marketing evoluiu ao longo do tempo para refletir as mudanças no comportamento do consumidor e nas estratégias de marketing.

4 Ps:

    Produto: O produto ou serviço real oferecido.
    Preço: O custo para o cliente adquirir o produto.
    Praça: Os canais de distribuição usados para alcançar o mercado-alvo.
    Promoção: As comunicações de marketing usadas para promover o produto.

O framework 4 Ps foi desenvolvido na metade do século 20 e foi amplamente usado por muitos anos. Ele se concentrou no tradicional marketing mix, enfatizando o controle que as empresas tinham sobre esses elementos.

4 Cs:

    Cliente: As necessidades e desejos do mercado-alvo.
    Custo: O valor percebido do produto ou serviço pelo cliente.
    Conveniência: A facilidade com que os clientes podem encontrar, comprar e usar o produto.
    Comunicação: A comunicação bidirecional entre a empresa e o cliente.

O framework 4 Cs surgiu no final do século 20 e ganhou popularidade à medida que as empresas começaram a se concentrar mais na satisfação do cliente e no desenvolvimento de relacionamentos com seus clientes. Ele enfatiza a importância de entender as necessidades do cliente e adaptar os esforços de marketing para atender a essas necessidades.

Por que a mudança de 4 Ps para 4 Cs?

A mudança de 4 Ps para 4 Cs reflete uma série de fatores, incluindo:

    Aumento do poder do cliente: Os clientes têm mais escolhas e informações do que nunca, dando-lhes mais poder no mercado.
    Marketing digital: O surgimento do marketing digital tornou mais fácil para as empresas alcançar e envolver os clientes de novas maneiras.
    Ênfase na experiência do cliente: As empresas estão cada vez mais focadas em fornecer uma experiência positiva ao cliente, o que requer um profundo entendimento das necessidades e desejos do cliente.

O framework 4 Cs fornece uma abordagem mais centrada no cliente para o marketing e é mais adequado aos desafios e oportunidades do mercado atual. Embora os 4 Ps ainda sejam relevantes em alguns contextos, os 4 Cs oferecem um framework mais abrangente e eficaz para empresas que buscam sucesso no mundo moderno.*


