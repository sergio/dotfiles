---
file: Awesomesites.md
author: Sergio Araujo
Last Change: Sat, 13 Jul 2024 - 08:02:28
---

+ <https://Youtube.com/TV> Watch Youtube in a different way
+ <https://archive.org/> Internet archive
+ <http://youpronounce.it/> improve your English pronunciation
+ <https://www.lifewire.com/about-us> a cool tech site
+ <http://www.tutorialspoint.com/codingground.htm> code online
+ <https://www.tutorialspoint.com/execute_python3_online.php>

## Documments

[Registro civil](https://registrocivil.org.br/)
