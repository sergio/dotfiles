---
File: ~/.dotfiles/wiki/prettier.md
Last Change: Sun, 21 Apr 2024 - 11:42:34
tags: [format, code, programming]
---

# intro

Prettier is "an opinionated code formatter". Prettier will format the style of your code per your configurations.

- [Don’t just lint your code - fix it with Prettier](https://www.freecodecamp.org/news/dont-just-lint-your-code-fix-it-with-prettier/)

First, we will need to install the necessary packages. In some cases "prettier" does not appears
on the path, so I think it is better to install it globally.

```sh
#yarn add prettier prettier-eslint prettier-eslint-cli -D

doas npm install -g prettier
npm install --save-dev --save-exact prettier
```

```lua
use({
    "prettier/vim-prettier",
    run = "yarn install",
    ft = { "javascript", "typescript", "css", "less", "scss", "graphql", "markdown", "vue", "html" },
})
```

On termux I did:

``` sh
npm install --save-dev --save-exact prettier
node --eval "fs.writeFileSync('.prettierrc','{}\n')"
```

Next, create a .prettierignore file to let the Prettier CLI and editors know which files to not format. Here’s an example:

# Ignore artifacts

build
coverage
