---
file: ~/.dotfiles/wiki/zip.md
author: Sergio Araujo
Last Change: Thu, 18 Jul 2024 - 11:34:30
tags: [zip, tools, compress, uncompress]
---

## create zip files

    zip -r dotfiles.zip dotfiles/

## zip all txt files to destination.zip

+ [superuser](https://superuser.com/a/462796/45032)

    zip destination . *.txt

## zip folder

    zip -r archivename.zip directory_name

To just see the zip content:

    unzip -l file.zip

    unzip -l friends-transcripts.zip | awk '{print $NF}'

In order to read the content of the ziped files try:

    unzip -c friends-transcripts.zip '*' | less

### zip a folder with encription:

+ https://www.linuxfordevices.com/tutorials/linux/encrypt-and-decrypt-file-or-directory

    zip -r --encrypt encypted2.zip <directory>

## view content of a zip file

    zip -sf file.zip
    unzip -l file.zip

## Deleta an item form a zip folder:

    zip -d folder.zip target-file

## Creating Split Zip File

Imagine you want to store the Zip archive on a file hosting service that has a
file size upload limit of 1GB, and your Zip archive is 5GB.

You can create a new split Zip file using the -s option followed by a
specified size. The multiplier can be k (kilobytes), m (megabytes), g
(gigabytes), or t (terabytes).

    zip -s 1g -r archivename.zip directory_name

The command above will keep creating new archives in a set after it reaches the specified size limit.

    archivename.zip
    archivename.z01
    archivename.z02
    archivename.z03
    archivename.z04

## Adding unzipped files to a zipped folder

+ https://unix.stackexchange.com/a/272896/3157

    zip -ur existing.zip myFolder

This command will compress and add myFolder (and it's contents) to the existing.zip.

In my case I have made this:

    zip -ur ~/.dotfiles/fonts.zip code128.ttf

# vim: ft=markdown et sw=2 ts=2 cole=0
