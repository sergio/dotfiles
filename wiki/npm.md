---
file: npm.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [npm, nodejs]
---

## npm.md

npm is the default package manager for Node.js.

Check the npm package is really installed globally.

    npm list -g --depth=0

## npm definition in Portuguese

O npm é o Gerenciador de Pacotes do Node (Node Package Manager) que vem junto
com ele e que é muito útil no desenvolvimento Node. Por anos, o Node tem sido
amplamente usado por desenvolvedores JavaScript para compartilhar ferramentas,
instalar vários módulos e gerenciar suas dependências. Sabendo disso, é
realmente importante para pessoas que trabalham com Node.js entendam o que é
npm.

## latex like websites
+ https://latex.now.sh/#getting-started

    npm install latex.css

    doas npm install --location=global package

