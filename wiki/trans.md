---
file: trans.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: Explain briefly how to use trans
---

#  trans.md intro:
+ https://www.soimort.org/translate-shell/

    sudo xbps-install -S translate-shell

    trans dog

    trans -brief dog
    cão

    trans -b dog

    trans file:///home/sk/greet.txt

    trans :pt mouse -brief

    trans -shell -brief
    Translate Shell
    (:q to quit)
    &gt; dog
    cão

if you want to hear try

    trans -b -p :pt "just a single phrase"
    apenas uma única frase

In the example below you can listen in a brief mode using bing as engine

    trans -b -p -e bing :en "este é apenas um teste"
    This is just a test

    Gettting the result on vim:
    :echom system("trans -b -p :pt " . "thats my dog")

## play a random phrase
Actually I have a script to select a random phrase. So I use

    trans -b -p :en "$(randphrase.sh| awk 'NR==3 {print $0}')"

