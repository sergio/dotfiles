---
file: solocimento.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: Tips and tricks about ecological brick (tijolo ecológico)
---

#  solocimento.md intro:
+ [tudo sobre video 1](https://www.youtube.com/watch?v=lQq8byvsMWQ)
+ [tudo sobre video 2](https://www.youtube.com/watch?v=nJ2TWCMg88k)
+ [explicação do funcionamento incluindo regulagens](https://www.youtube.com/watch?v=cWQ5CAjY5Ns)
+ [custos](jhttps://www.youtube.com/watch?v=pWYn8SITwp8&t=259)
+ [assentamento dos tijoos](https://www.youtube.com/watch?v=3y6FPAj9mfI)
+ [será que vale a pena?](https://www.youtube.com/watch?v=6wRf_0iKgFc)
+ [Experiência em Aquiraz](https://www.youtube.com/watch?v=hz6s6FTUxTY)
+ [orçamento](https://www.youtube.com/watch?v=UqvBBrf9rKg)


Anotações e dicas coletadas sobre o modelo construtivo solo-cimento

## Números (Custo)
+ https://www.youtube.com/watch?v=pWYn8SITwp8

No cálculo acima ele usa acal hidratada o que adiciona 0,10 centavos por
tijolo e contabiliza 0.6 com a terra (que é trazida por caminhão), ou seja,
descnsiderando acal e a compra da terra o tijolo sai por 0,13 centavos.

## Dicas
+ https://youtu.be/neEW7KR3TjE

Usar somente o triturador é no máximo 2x Usar 100% do solo

### O comprimento da forma ideal é 24.8
+ https://www.youtube.com/watch?v=zs7Wm9y1fAY

### Terra com raiz é ácida
+ https://youtu.be/KfGq77Wdg7c

### Não usar massa cola entre tijolos
+ https://www.youtube.com/watch?v=3MI-Zn3qKY8

No vídeo acima o Sr Francico Aguilar além de explicar que não é necessária
massa cola entre tijolos, apresenta gabaritos (tarugos) para alinhamento dos
tijolos. [gabaritos de madeira](https://www.youtube.com/watch?v=6TrH8eCtk6k)

O diâmetro do ferro deve ser 1/8 do diâmetro do furo por causa da dilatação do
ferro. O resultado, caso necessite arredondamento, sempre deve ser pra menos.
Caso necessite mais resistência, diminua a distância entre as barras.

### Assentar o macho para baixo
+ https://www.youtube.com/watch?v=NFgS7SO1ZGc

### Como empilhar corretamente o tijolo ecológico
+ https://www.youtube.com/watch?v=RVO3d-mN5ec

## Quanto ao preparo do solo
Solo ideal com 30% de argila e 70% de areia. Faça um bolo com a massa
na mão, se não deixar umidade na mão e conseguir dividir em dois, significa
que a umidade está correta.

traço para os grauts

    3.0 brita fina (pedrisco)
    2.5 de areia
    1.0 de cimento

Um vídeo sobre os grautes
+ https://www.youtube.com/watch?v=bMG9NE82YOI

Os furos dos grautes são feitos no momento de demarcar a primeira fiada.
com uma furadeira faz-se furos de 15cm e coloca-se o ferro.

Existe no mercado um aditivo de aderência específico para passar nas pontas
das varas de ferro dos grautes. O preencimento dos grautes só se inicia aos
50cm, na primeira fiada de canaletas.

## Da cura

Se o tijolo for excessivamente prensado o processo de cura fica prejudicado
porque a água não penetra no tijolo com facilidade para proporcionar a cura do
mesmo

## Dicas adicionais e observações

A gaveta de reabastecimento deve conter mais terra do que o reservatório de
prensagen, isso garante que uma única passada da gaveta abastecerá o mesmo

Dicas de regulagem da máquina:
+ https://www.youtube.com/watch?v=7WRoeaRXnLk

## Acessórios
Tanto o triturador de terra quanto a peneira agilizam o processo de fabricaçoã
de tijolos. Abaixo modelos de máquinas artesanais para triturar e peneirar a
mistura.

+ [Peneira motorizada](https://www.youtube.com/watch?v=_89CZrkynk0)
+ [Triturador de terra](https://www.youtube.com/watch?v=d9QXGBuuti4)
+ [Triturador de terra 02](https://www.youtube.com/watch?v=Ni8MShrVI5g)
+ [Triturador de terra 03](https://www.youtube.com/watch?v=zw3d-lhkKSk&)

## Projeto de uma máquina à venda
+ http://tecnicoagropecuario9.blogspot.com/2011/09/projeto-maquina-tijolo-ecologico.html

APENAS R$ 20,00 CONTATO -VIVO-83 8123 8077-- TIM 83 9970 0698
EMAIL robertoumbu@hotmail.com robertoumbu@gmail.com

## Construindo com tijolos ecológicos - 05 - Assentamento
+ https://www.youtube.com/watch?v=IS4ymbw_y3I

## Tijolo Ecológico - ERROS DE MODULAÇÃO
+ https://www.youtube.com/watch?v=rJzrGuqUn7E
+ https://www.youtube.com/watch?v=awqpumisuu8

A engenheira explica no vídeo acima que muitos engenheiros, a maioria deles,
por desconhecer o modelo contrutrivo solocimento, cometem erros de projeto e
mostra como corrigir. (Oferece também uma apostila paga no valor de 37 Reais)

Conduite liso é melhor do que os copos descartáveis nas canaletas, pois tomam
menos espaço do cimento, permitindo mais concreto sobre o ferro.

### Resistencia do Tijolo Ecologico NBR 10834
+ https://www.youtube.com/watch?v=hjRAvxVStzg

O cimento adquire sua resitência máxima após 28 dias, obtendo maior
resistência à compressão.

## Máquina da Ecomáquinas (mais modularizada)
+ https://www.youtube.com/watch\?v\=WCIyNZsXYGU

## Como fazer uma máquina solocimento
+ [video 1/5](https://www.youtube.com/watch?v=K-4YODgbV3g)
+ [video 2/5](https://www.youtube.com/watch?v=VkWm0PrZvvQ)
+ [vide0 3/5](https://www.youtube.com/watch?v=mmkCLi1t4A0)
+ [video 4/5](https://www.youtube.com/watch?v=yuZvF1x9dB8)

+ [modelo explicado](https://www.youtube.com/watch?v=Lm_kezDbZ4M)

## Dosagem para o traço do rejunte

    12 litros de terra
    1  litro de solocimento
    1/2 litro de cola branca
    água

    fazer uma massa mole como massa de pão

### Outros projetos com medidas (esse achei mais legal)
+ [video 1](https://www.youtube.com/watch?v=XJ1ErrL2UAQ)
+ [video 2](https://www.youtube.com/watch?v=_ALCPgDLdWQ)

``` txt
Medidas para o projeto acima:
+ Medida das barras ........ 04.0 cm
+ Espessura das barras ..... 01.4 cm
+ Comprimento do corpo ..... 33.0 cm
+ Largura do corpo  ........ 22.0 cm
+ Altura da máquina ........ 79.0 cm
+ Distância barras guia .... 03.7 cm
+ Comprimento barras guia .. 24.0 cm
+ Comprimento total da mesa  84.5 cm
+ Base da gaveta alimentado  54.5 cm
+ Comprimento da tampa ..... 31.0 cm
+ Largura da tampa ......... 15.0 cm
+ Astes de esbarro da tampa  30.0 cm
+ Forma do tijolo comp  .... 25.0 cm
+ Forma do tijolo larg  .... 12.5 cm
+ Suport para rolamentos ... 05.0 cm
```

        Notas: A altura desconsidear a guia do alimentador
               a tampa usa dobradiças de portão
               o suporte dos rolamentos é fora de centro
               usa três rolamentos

               As barras que fazem os pés devem ter
               portanto 70 ou 71 centémetros, já que
               há duas barras de 4 centímetros acima dela

               A mesa na qual se encaixa a fêmea (parte de baixo)
               é menor que a forma.

+ [mecanism video 1](https://www.youtube.com/watch?v=d_ECRGGcv1o)
+ [mecanism video 2](https://www.youtube.com/watch?v=bHLbFVjK6io)
+ [mecanism video 3](https://www.youtube.com/watch?v=Wh8ir9XaWg4)

## laje eps (isopor)
+ https://www.youtube.com/watch?v=0xsRlzkB-eE
+ https://www.youtube.com/watch?v=EDo5n60V_JA

## caixa molde 30 x 15

+ https://www.youtube.com/watch?v=m2OMHBDV7dw

## Prensa que faz tijolos na transversal (emperra menos)
+ https://www.youtube.com/watch?v=TOVpChRFl20

## Tutorial de montagem de um modelo específico
+ https://www.youtube.com/watch?v=UncjgT0MHoo

## Da fundaçao ao piso - faça você mesmo
+ [episódio 001](https://www.youtube.com/watch?v=Jd4wAulb8aI)
+ [episódio 002](https://www.youtube.com/watch?v=EObbsLNpQ60)
+ [episódio 003](https://www.youtube.com/watch?v=uWVLmAR4mm0)
+ [episódio 004](https://www.youtube.com/watch?v=dFIb7q-20VE)

## Régua e espaçadores para assentamento
+ [Régua e espaçadores](https://www.youtube.com/watch?v=UuspWCv2mmo)

