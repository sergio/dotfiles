---
File: hiperadobe.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: mar 14, 2021 - 17:58
tags: [building, solocimento, diy]
---

#  hiperadobe.md intro:
+ https://www.youtube.com/watch?v=3OBnNxVNZ38
+ https://www.youtube.com/watch?v=2YvGN79604Q
+ https://www.youtube.com/watch?v=LrdJJTzHSZ0
+ https://www.youtube.com/watch?v=hx3OoFEX4uQ
+ https://www.youtube.com/watch?v=z7hqnmMiqeU

## O que é o hiperadobe
+ https://sustentarqui.com.br/hiperadobe-o-que-e-vantagens/

+ Economiza 60 a 70 porcento do custo da parede
+ Excelente isolamento térmico

