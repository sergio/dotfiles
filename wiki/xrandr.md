---
file: xrandr.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [desktop, tools, video, x11, wayland, screen, resolution, utils]
---

xrandr is an official configuration utility to the RandR (Resize and Rotate) X Window System extension. It can be used to set the size, orientation or reflection of the outputs for a screen. For configuring multiple monitors see the Multihead page.

## mostrar resolução de tela

    xrandr | awk -F"," '/^Screen 0/ {print $2}'

## alterar o brilho da tela (screen brightness)
+ https://unix.stackexchange.com/a/529164/3157

That will return all the connected monitors (like LVDS-1 or DVI-D-0 for instance)

    xrandr -q | grep ' connected' | head -n 1 | cut -d ' ' -f1

Now

    xrandr --output LVDS1 --brightness 0.7

see also: xbacklight

