---
file: xterm.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [terminal, cli]
---

## Config xterm

    alias xterm='xterm -fn 7x13 -fa "Liberation Mono:size=14:antialias=false"'

    xterm -fn 7x13 -fa "IBMPlexMono-Regular:size=12:antialias=true"

## xterm shortcuts

    ^H .............. erases til the begining of line
    ^w .............. erases one word

## Using xterm with fzf as a fuzzy app laucher

    xterm -name 'floating xterm' -e "fzf $* < /proc/$$/fd/0 > /proc/$$/fd/1"

