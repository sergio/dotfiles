---
file: telegram.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Telegram tips


## Keyboard Shortcuts

    + ------------------------------------+
    | Formatting Type 	Keyboard shortcut |
    + ------------------------------------+
    | Bold 	            CTRL + B          |
    | Italics 	        CTRL + I          |
    | Monospace 	    CTRL + SHIFT + M  |
    | Insert link 	    CTRL + K          |
    + ------------------------------------+

## Sending code via Telegram:
+ https://www.wikihow.com/Send-Code-on-Telegram

Just type
'``` your code here ```'

## Telegram bots
+ https://telegrambots.github.io/book/

    You can use the following keyboard shortcuts in Telegram:

     ---------------------------------------------------- -------------------------------
     Command                                              Shortcut
     ---------------------------------------------------- -------------------------------
     Move to next chat                                    Ctrl + Tab
     Move to next chat                                    Ctrl + PageDown
     Move to next chat                                    Alt + Arrow Down
     Move to previous chat                                Ctrl + Shift + Tab
     Move to previous chat                                Ctrl + PageUp
     Move to previous chat                                Alt + Arrow Up
     Go to Previous Folder                                Ctrl + Shift + Arrow Up
     Go to Next Folder                                    Ctrl + Shift + Arrow Down
     Search selected chat                                 Ctrl + F
     Exit selected chat and search Telegram               Esc
     Exit display of current chat/channel                 Esc
     Delete currently selected message                    Delete
     Quit Telegram                                        Ctrl + Q
     Lock Telegram (if Local Password is set)             Ctrl + L
     Iconify (Minimize) Telegram                          Ctrl + M
     Iconify (Minimize) Telegram to System Tray           Ctrl + W
     Edit Previous Message                                Arrow Up
     Start New Line in Input Area                         Ctrl + Enter or Shift + Enter
     Move Cursor to Start of Multi-line Message           Ctrl + Home
     Make Text Italic                                     Ctrl + I
     Make Text Bold                                       Ctrl + B
     Make Text Underline                                  Ctrl + U
     Make Text Striketrough                               Ctrl + Shift + X
     Make Text Monospace                                  Ctrl + Shift + M
     Remove Text Formatting (Make Selection Plain Text)   Ctrl + Shift + N
     Add URL to Selected Text (Make Link)                 Ctrl + K
     Send File                                            Ctrl + O
     Open Contacts                                        Ctrl + J

That\'s it. Please let me know if I forgot something.

