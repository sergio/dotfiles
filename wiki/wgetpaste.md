---
Filename: wgetpaste.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [tools, snippets]
 vim:ft=markdown
---

## paste snippets in various sites:
+ https://ubunlog.com/en/wgetpaste-fragmentos-codigo-terminal/#Carga_fragmentos_de_texto_con_Wgetpaste

## Discover services:

```sh
wgetpaste -S
```

## Paste command + command output
```sh
wgetpaste -c 'pwd'
```

## Set language:

```sh
wgetpaste -l Bash mi-texto.txt
```



