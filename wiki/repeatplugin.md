---
file: repeatplugin.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Para colocar uma serie de palavras entre aspas

   csw" ................ coloca palavra atual entre aspas
   . ................... repete a inserção das aspas

   ( necessita do plugin repeat instalado )


tags: vim, nvim
