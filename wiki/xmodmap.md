---
File: xmodmap.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: abr 01, 2021 - 19:06
tags: [tags]
---

#  xmodmap.md intro:

Utility for modifying keymaps

## turn your caps into esc

``` markdown
remove Lock = Caps_Lock
keysym Caps_Lock = Escape
```

Now run

``` markdown
xmodmap ~/.xmodmap
```

Or run

```markdown
dconf write /org/gnome/desktop/input-sources/xkb-options "['caps:escape']"
```


## swap control and caps Caps_Lock
+ https://www.emacswiki.org/emacs/MovingTheCtrlKey#h5o-2

``` markdown
!
! Swap Caps_Lock and Control_L
!
remove Lock = Caps_Lock
remove Control = Control_L
keysym Control_L = Caps_Lock
keysym Caps_Lock = Control_L
add Lock = Caps_Lock
add Control = Control_L
```

<--!
 vim: ft=markdown et sw=4 ts=4 cole=0
-->
