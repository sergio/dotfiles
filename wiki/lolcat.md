---
File: lolcat.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: nov 22, 2020 - 05:53
tags: [tags]
---

#  lolcat.md intro:
What is lolcat?

Lolcat is an utility for Linux, BSD and OSX which concatenates like similar to
cat command and adds rainbow coloring to it. Lolcat is primarily used for
rainbow coloring of text in Linux Terminal.

lolcat is based on ruby, so:

    gem install lolcat

## How to use it?

    ps aux | lolcat

