---
-- File: /home/sergio/.dotfiles/wiki/gitsigns-nvim.md
-- Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## Intro:

- https://github.com/lewis6991/gitsigns.nvim

## Features

- Signs for added, removed, and changed lines
- Asynchronous using luv
- Navigation between hunks
- Stage hunks (with undo)
- Preview diffs of hunks (with word diff)
- Customisable (signs, highlights, mappings, etc)
- Status bar integration
- Git blame a specific line using virtual text.
- Hunk text object
- Automatically follow files moved in the index.
- Live intra-line word diff
- Support for yadm
