---
file: instagram.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: colect tips about instagram
---

#  instagram.md intro:
+ [Increase your Instagram followers with a simple Python bot](http://bit.ly/2FuZ2XB)
+ [How to get instagram followers/likes using python](https://www.youtube.com/watch?v=BGU2X5lrz9M)
+ [Like instagram pictures using Selenium | Python](http://bit.ly/2uvy7c1)
+ [Building an instagram bot](http://bit.ly/2QXWs1W)
+ [instapy quickstart](https://github.com/InstaPy/instapy-quickstart)
+ [instapy repo](https://github.com/timgrossmann/InstaPy)

## Installing geckodriver on voidlinux
+ [instapy video](https://www.youtube.com/watch?v=bwB5mR5PpTg&feature=youtu.be)

    pip install --user selenium
    pip install --user instapy
    sudo xbps-install -S geckodriver libressl{,-devel}

## instapy login error
+ https://github.com/timgrossmann/InstaPy/issues/4962#issuecomment-527870345

Sometimes the site checking does not work and then instapy does not loads
and if you want to bypass this error while the site is down, what I did was
just go to : instapy/login_util.py and comment out the Instagram Server Status
try/except step. That fixes it!

## A concept prof script using selenium to make a google search
+ https://stackoverflow.com/a/24599184/2571881

``` python
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys

browser = webdriver.Firefox()
browser.get('http://www.google.com')

search = browser.find_element_by_name('q')
search.send_keys("google search through python")
search.send_keys(Keys.RETURN) # hit return after you enter search text
time.sleep(5) # sleep for 5 seconds so you can see the results
browser.quit()
```

