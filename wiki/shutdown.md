---
file: shutdown.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [admin]
---

## Introdução

## reiniciar agora

    shutdown -r 0

##  desligar as 17:00

    shutdown -h 17:00

## desligar em 30 segundos

    shutdown -h +30

## Desligar máquina windows remotamente

```bash
net rpc shutdown -I ipAddressOfWindowsPC -U username%password
net rpc shutdown -r : reboot the Windows machine
net rpc abortshutdown : abort shutdown of the Windows machine
```

