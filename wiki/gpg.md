---
file: gpg.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown

# gpg.md

## References
+ https://fedoraproject.org/wiki/Creating_GPG_Keys#ExportGNOME
+ https://www.cyberciti.biz/tips/linux-how-to-encrypt-and-decrypt-files-with-a-password.html
+ https://superuser.com/a/249516/45032

Encripts and decript files and folders

## add public key

    gpg --recv-keys 5CC908FDB71E12C2

##  gnerating your keys
+ https://www.finux.org/generating-a-new-openpgp-key-with-gnupg/

``` sh
 gpg2 --full-gen-key
gpg (GnuPG) 2.2.23; Copyright (C) 2020 Free Software Foundation, Inc.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

gpg: directory '/home/sergio/.gnupg' created
gpg: keybox '/home/sergio/.gnupg/pubring.kbx' created
Por favor selecione o tipo de chave desejado:
   (1) RSA and RSA (default)
   (2) DSA and Elgamal
   (3) DSA (apenas assinatura)
   (4) RSA (apenas assinatura)
  (14) Existing key from card
Opção?
```

### listing your key

    gpg2 --list-key voyeg3r@gmail.com

### compress your settings

    tar cvfz gnupg.tar.gz .gnupg/

Now

    gpg2 -c gnupg.tar.gz

## importing a public key

    gpg --recv-key 1EB2638FF56C0C53

## Generating keys
Creating GPG Keys Using the Command Line

Use the following shell command:

    gpg2 --full-gen-key

A series of prompts directs you through the process. Press the Enter key to
assign a default value if desired. The first prompt asks you to select what
kind of key you prefer:

## Listing avaiable keys

    gpg --list-keys
    gpg -k

To list the secret keys you type:

    gpg --list-secret-keys

## Encripting files and folders
For files(outputs filename.gpg):

    gpg -c filename

For dirs:

    gpg-zip -c -o file.gpg dirname

## Decryption

For files(outputs filename.gpg):

    gpg filename.gpg

For dirs:

    gpg-zip -d file.gpg

## Exporting your public key
This will send your key to the gnupg default key server (keys.gnupg.net), if
you prefer another one use :

    gpg2 --keyserver hkp://pgp.mit.edu --send-key KEYNAME

Copying a Public Key Manually

If you want to give or send a file copy of your key to someone, use this command to write it to an ASCII text file:

    gpg2 --export --armor jqdoe@example.com > jqdoe-pubkey.asc

tags: hack, encryption, security
