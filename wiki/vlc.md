---
file: vlc.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [iptv, movies, player, tv, midia]
---

## dicas e atalhos sobre o vlc

## Reprodução

    Shift-left ............. atrazo muito pequeno
    Alt-left ............... pequeno atrazo
    Ctrl-left .............. retrocesso médio
    Ctrl-Alt-left .......... retrocesso longo
    slower ................. -
    faster ................. +
    slower by 10% each time  [
    faster by 10% each time  ]
    <space> ................ pause/play
    Swich audio track ...... b

    Shift-v ................ alternate subtitles
    Shift-r ................ record video
    Shift-s ................ capture image

    Ctrl-j ................. informações sobre o codificador (download url)
    Ctrl-n ................. abrir fluxo de rede

## Volume

    Ctrl-up ................ increase volume
    Ctrl-down .............. decrease volume

    f ...................... full screen
    show playlist .......... <ctrl>l

## IPTV daily playlist
+ https://www.dailyiptvlist.com/
+ https://www.iptvsource.com/dl/br_030119_iptvsource_com.m3u (brasil)

Tip: If Stream Stops Every 20-30 Seconds, please click on "Loop Play Button" :

## Consertando o download de legendas no Vlc

On windows edit:

    c:\windows\System32\drivers\etc\hosts

and add this line:

    92.240.234.122 api.opensubtitles.org

## ajustando o brilho

    Ctrl-e ............. agre janela de ajustes

## Advanced subtitle syncronization

When subtitles are late compared to the audio, and only in this case, you can use the advance synchronisation functionality of VLC :

    Step 1 : Detect (“hear and see”) that subtitles are out of sync
    Step 2 : Press Shift H when you hear a sentence that you will be able to easily recognize
    Step 3 : Press Shift J when you read the same sentence in the subtitle
    Step 4 : Press Shift K to correct the sync

## Record online radio station:
+ https://www.vlchelp.com/record-online-radio-streams-mp3/

Ceará Rural: http://stm3.xcast.com.br:8560/stream



# vim: cole=0:
