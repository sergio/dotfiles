---
file: pdftotext.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


## First of all you have to install
+ https://www.howtogeek.com/228531/

    sudo xbps-install -S poppler-utils

## converting pdf to text
+ https://askubuntu.com/a/307789/3798

    pdftotext -layout input.pdf output.txt
