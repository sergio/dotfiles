---
file: wingchun.md
Last Change: Thu, 19 Sep 2024 - 19:39:51
---
 vim:ft=markdown

## Materiais:
+ Quadro de acrílico
+ Ficha do aluno

## Formas:
+ https://www.youtube.com/watch?v=y7CQtgvH2kM

## The Three Treasures of Shaolin
+ http://hungfakwoon.com/Article5.htm

## Sam Dim Yat Sin - Three Points One Line
+ https://www.youtube.com/watch?app=desktop&v=8yrwoLJGO1w&feature=youtu.be

## Skills:
+ Desenvolver ambidestralidade
+ Learn English
+ Test any concept or technic agains the Fundamentals

## Gary Lam:
+ https://youtu.be/X7TQTX7GP5M "Foundation Fortress" - Trailer 2

## Jung Chi (aderência ao centro)
Utilizando o man sau (a mão que pergunta)
+ https://www.youtube.com/watch?v=sMnJUJ7srtM&t=29s

# Notes about wing chun

Wing Chun has Shuai(wrestling), Na(seizing & locking),
Ti(kicking), and Da(striking), making it a complete system.

ng cho kun

## aplicações

https://youtube.com/shorts/E6zrRIu6QI8
https://youtube.com/shorts/GV2mgZspuVY?si=3cFXN38-SEXCN7vf

## Fundamentals

+ Principle
+ Eficiency
It has principles that give origin to concepts and create tecnics

    CONCEPTS
    --------
    Ocupy space - center line - portais - superior position
    Man sao (mão que pergunta)
    Two lines of defense - espada escudo
    Jung chi - energia a frente
    San mo kiu - (Wen kiu - san kiu - faw kiu)
    Chi sao
    Loi lau roi sum

## Chi Sao
+ https://www.youtube.com/watch?v=N57Y1cMaCDc
+ https://youtube.com/shorts/8H3iD-c3VUM

## Teoria dos portais: (luk Mun Ji Sik)

## Tin Yan Dei MMA
+ https://www.youtube.com/watch?v=HyUh-p2dkWs
+ https://www.youtube.com/watch?v=XzMRJSQD88Y
+ https://www.youtube.com/watch?v=1I-yE16OCvg

## Siu nim tau (SBA)
+ https://www.youtube.com/watch?v=vI8ypZQ7ZAk
+ https://www.youtube.com/watch?v=sb4CMJM9BPE&t=252s

Ataque e defesa silmultâneo:
+ https://www.youtube.com/watch?v=nEgb58cN4EE

Teoria Lok Jong: https://www.youtube.com/watch?app=desktop&v=cihPFwIR8fw&feature=youtu.be

## Wooden Dummie sections
+ https://www.youtube.com/watch?v=9dY4NmNtthw
+ https://www.youtube.com/watch?v=MsoasIG6SUs
+ https://www.youtube.com/watch?v=z_dU5vBQ2M8
+ https://www.youtube.com/watch?v=25HFxOVH3p8
+ https://www.youtube.com/watch?v=rHdO5zsk9JE
+ https://www.youtube.com/watch?v=4CmLQ5DAKrM
+ https://www.youtube.com/watch?v=qscMYVwLlsY
+ https://www.youtube.com/watch?v=eFUUQXLy68E

## Saam jim bou
+ https://www.youtube.com/watch?v=lnwSFGrDyFM

## Movie
+ https://www.youtube.com/watch?v=IF_kY7VUBFg

## Wing Chun University:
+ https://www.wingchununiversity.com/
+ Wing Chun University - Tyler Rea's Jook Wan Huen - Foundations 01 - Ring Sets (SfGlCv26UG0)
+ Wing Chun University - Tyler Rea's Jook Wan Huen - Foundations 02 - (5d_C2GiVPYg)
+ Wing Chun University - Tyler Rea's Jook Wan Huen - Foundations 03 - (5d_C2GiVPYg)
+ Wing Chun University - Tyler Rea's Jook Wan Huen - Foundations 04 - (drX42Os8ll0)

+ https://www.youtube.com/watch?v=4hCsJcQajwg

## Benny Meng:
+ https://www.youtube.com/watch?app=desktop&v=Is1jQGtAFYU&feature=youtu.be Wing Chun Part 2 with Benny Meng
+ https://www.youtube.com/watch?app=desktop&v=RQdh7JwcdbE&feature=youtu.be Wing Chun Part 4 with Benny Meng

## Wu Lap Da - E porque muito Wing Chun não funciona na prática

+ https://www.youtube.com/watch?v=Q3i8BuvbtKg&t=220s

## Flow drills
+ https://www.youtube.com/watch?v=1REzmmc134E
+ https://www.youtube.com/watch?v=N4y50xN9l9k
+ https://www.youtube.com/watch?v=XqGKFMNFAOU

Lap da using elbol and pivoting your body, taken your partner off his or her balance

O tan tow lida jiao lao
