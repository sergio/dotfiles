---
file: mercurial.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# intro

Mercurial is a modern, open source, distributed version control system, and a
compelling upgrade from older systems like Subversion.
