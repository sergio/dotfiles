---
File: story.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: jul 15, 2020 - 19:20
tags: [tags]
---

#  story.md intro:

Here I am, at the top of my 52, flying over my home country, as if I would
have flight abroad before, thinking about life, and how amazing it is.
Sometimes or invariably big changes on our invironment change ourselves, and
definitily this flyght is more then ever changing my internal world, zilions
of times more than my external one. First of all, my safety concerns and on
top of it, the new life, wherever it will be, in Minas Gerais or in a more
subtle state. Many dreams, plans, passions, I think the turbulances are mixing
up my ideas and letters, but they are still here, and you dear reader, who
laugth at me, because you are walking on earth, maybe will take a great lesson
of my experience. Don't ever let the fear take you up, use it, at least to
write a good story.
