---
file: less.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# less

    /  .......... faz buscas
    n ........... repete busca para frente
    gg .......... vai para a linha 1
    G ........... vai para a última linha
    q ........... sair
    Ctrl-f ...... desce uma página
    Ctrl-b ...... volta uma página

## Search a pattern on a manpage

    LESS=+/EXAMPLE: man parallel

    LESS="$LESS+/^ *zmv *\\[" man zshcontrib

## exibindo um arquivo com números de linha

    less -N file

## changing the search keybinding

    # generates a binary configuration file for less
    lesskey ~/.dotfiles/lesskey
