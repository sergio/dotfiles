
Pai-Mãe, respiração da Vida,
Fonte do som, Ação sem palavras, Criador do Cosmos!
Faça a sua Luz brilhar dentro de nós, entre nós e fora de nós para que possamos torná-la útil.
Ajude-nos a seguir o nosso caminho, respirando apenas o sentimento que emana do Senhor.

Nosso EU, no mesmo passo, possa estar com o Seu, para que caminhemos como Reis e Rainhas com todas as outras criaturas.
Que o Seu e o nosso desejo, sejam um só,
em toda a Luz, assim como em todas as formas, em toda a existência individual, assim como em todas as comunidades.

Faça-nos sentir a alma da Terra dentro de nós, pois, assim, sentiremos a Sabedoria que existe em tudo. Não permita que a superficialidade e a aparência das coisas do mundo nos iluda, e nos liberte de tudo aquilo que impede nosso crescimento.

Não nos deixe ser tomados pelo esquecimento de que o Senhor é o Poder e a Glória do mundo, a Canção que se renova de tempos em tempos e que a tudo embeleza.
Possa o Seu amor ser o solo onde crescem as nossas ações.

Assim seja, Assim É.

