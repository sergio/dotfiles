---
file: chmod.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# chmod - muda permissões de arquivos e diretórios

## fixing folders and files permission

    find . -type d -print0 | xargs -0 chmod 0775
    find /home/user -type f -print0 | xargs -0 chmod 0664

find can do the trick alone with -exec:

    find /home/user -type f -exec chmod 0664 {} \;
    find /home/user -type d -exec chmod 0775 {} \;

to prevent find from spawning a chmod for each entry:

    find /home/user -type f -exec chmod 0664 {} +
    find /home/user -type d -exec chmod 0775 {} +

(this effectively calls chmod once with the list of all files as parameters rather than one chmod per file)
