---
file: dosfsck.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# corrigir pendrive=

sudo /sbin/dosfsck -altrvV /dev/sdb1
