---
file: audacity.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [audio, tools, mp3]
---
## O Audacity e um poderoso editor de audios
Agora a comunidade de software livre passa a usar dois forks
[tencity](https://sr.ht/~tenacity/tenacity/) acesse a página local no [tenacity](tenacity.md)

O repositório oficial é:
https://github.com/tenacityteam/tenacity

## How to easily convert any music to 432hz
+ https://bit.ly/2yrFJi3

The "Edit chains" was renamed to "Macros"
and the "Time Scale" was renamed to "Sliding Stretch"

## How to apply macros (formelly "Edit Chains")
+ https://www.youtube.com/watch?v=_DZeio_ansE

## Audacity alternative:

ardour --> Professional-grade digital audio workstation
