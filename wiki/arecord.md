---
File: /home/sergio/.dotfiles/wiki/arecord.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## arecord
+ https://medium.com/@YihuiXiong/record-play-audio-on-linux-8f59adf93710

arecord, aplay - command-line sound recorder and player for ALSA soundcard driver

For raw audio or wav audio, we can just aplay and arecord. Actually, they are the same program with different names. You can run ls -la /usr/bin/arecord to find it out. aplay and arecord have a bunch of parameters, but it is worth to learn these parameters

	aplay -h

To record a 10 second, 48000Hz, 2 channels, S16_LE format audio:

	arecord -d 10 -r 48000 -c 2 -f S16_LE audio.wav   # or arecord -d 10 -f dat audio.wav

