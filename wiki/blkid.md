---
file: blkid.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [uuid, tools, admin, stick]
---

Also see: bashmount

## Este comando tem que ser executado como root
Find UUID of your device

    blkid - command-line utility to locate/print block device attributes

    blkid
    /dev/sda1: UUID="8f2e84fe-fc1f-4147-b09b-d0e1d773a5db" TYPE="ext4"
    /dev/sda5: UUID="826eed8e-0a01-4649-9893-c76e9bd9f111" TYPE="swap

    sudo blkid /dev/sda1

Another way of doing that

``` bash
ls -l /dev/disk/by-uuid
total 0
lrwxrwxrwx 1 root root 10 ago 18 20:17 3639-3930 -> ../../sdb1
lrwxrwxrwx 1 root root 10 ago 18 18:28 4c7ad644-f0c0-4d7a-bea1-a9811a4a2241 -> ../../sda1
```

## Create symlink on your [termux] to access your sdcard:
First figure out your sdcard identifier, using a sdcard adapter, mount your
sdcard in your computer and run one of these two commands:

    awk -F'[ =]' '/.dev.sdb/ {print $3}' <<<$(sudo blkid)
    grep -Po '(?<= UUID=")[^"]*' <<<$(sudo blkid /dev/sdb1)
    awk '/sdb1/ {print $11}' <<<$(ls -l /dev/disk/by-uuid)

Identificador do meu sdcard atual: 7E03-1FE0

Veja também [[vol_id]]
