---
file: killall.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [admin, tools, kill, ps]
---

# Mata todos os processos

    killall -9 /usr/local/netscape/mozilla-bin
