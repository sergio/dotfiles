---
file: parabola.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---


# Parabola is a gnu archlinux

website: https://www.parabola.nu/

Get parabola: https://wiki.parabola.nu/Get_Parabola_(Portugu%C3%AAs)

## how to migrate any archlinux to parabola?
The complete migration process is described [here](https://wiki.parabola.nu/Migration_from_Arch#Complete_migration)
[here](https://wiki.parabola.nu/Migration_from_Arch_(Portugu%C3%AAs)) you have the Portuguese version of it.

Disable signature verification manually by modifying the line in `/etc/pacman.conf`:

    RemoteFileSigLevel = Never

The upgrade to gnupg-2.1 ported the pacman keyring to a new upstream format
but in the process rendered the local master key unable to sign other keys.
This is only an issue if you ever intend to customize your pacman keyring. We
nevertheless recommend all users fix this by generating a fresh keyring.

In addition, we recommend installing haveged, a daemon that generates system
entropy; this speeds up critical operations in cryptographic programs such as
gnupg (including the generation of new keyrings).

To do all the above, run as root:

    pacman -Syu haveged
    systemctl start haveged
    systemctl enable haveged

    rm -fr /etc/pacman.d/gnupg
    pacman-key --init
    pacman-key --populate archlinux
    pacman-key --populate parabola

Install the keyring and mirror list for free repositories:

    # pacman -U --noconfirm --force https://www.parabola.nu/packages/libre/any/parabola-keyring/download https://www.parabola.nu/packages/libre/any/pacman-mirrorlist/download

Note: If this doesn't work, use any other available package mirror with each package. For example:

    # pacman -U --noconfirm --force https://repo.parabola.nu/pool/parabola/parabola-keyring-20180126-1-any.pkg.tar.xz https://repo.parabola.nu/pool/parabola/pacman-mirrorlist-20171221-1.parabola1-any.pkg.tar.xz

Reenable signature verification in `/etc/pacman.conf`:

    RemoteFileSigLevel = Required

Rename mirrorlist.pacnew as mirrorlist:

    # cp -vr /etc/pacman.d/mirrorlist.pacnew /etc/pacman.d/mirrorlist

1.2 Replace non-free packages with Parabola liberated versions

    Add the [libre] repo above [core] in /etc/pacman.conf:

    ...
    [libre]
    Include = /etc/pacman.d/mirrorlist
    ...
    [core]
    ...

Add optional repos above [libre] in /etc/pacman.conf (see Repositories):

    ...
    [pcr]
    Include = /etc/pacman.d/mirrorlist
    [kernels]
    Include = /etc/pacman.d/mirrorlist
    [nonprism]
    Include = /etc/pacman.d/mirrorlist
    ...
    [libre]
    ...

Clean the pacman cache:

    # pacman -Scc

Force the database refresh:

    # pacman -Syy

Update the keyring:

    # pacman-key --refresh

Update to libre packages and install your-freedom to remove nonfree packages that do not have libre replacements:

    # pacman -Suu pacman your-freedom

Install the optional your-privacy package to block privacy disrespecting network protocols (see Nonprism):

    # pacman -Suu pacman your-privacy

     Note: If some of your packages have higher version than in Parabola repos
     they will be downgraded to lower version of libre packages to eliminate
     packages that are nonfree Warning: Beware any unofficial nonfree package
     that might still be lurking in your system (pacman -Q --foreign). Parabola
     doesn't support AUR helpers or any other third-party software. It is up to
     you to avoid installing nonfree software from AUR or from any other
     source.

1.3 Update bootloader configuration

Please update either GRUB or Syslinux, but not both, as it would cause a kernel panic.
1.3.1 GRUB

For grub regenerate your grub.cfg file running:

    # grub-mkconfig -o /boot/grub/grub.cfg

Tip: Check https://wiki.archlinux.org/index.php/GRUB for more information

1.3.2 Syslinux

Manually change following lines in /boot/syslinux/syslinux.cfg to reflect change:

    For generic kernel,

    LABEL parabola
    MENU LABEL Parabola GNU/Linux-libre, with generic kernel
    LINUX ../vmlinuz-linux-libre
    ...
    INITRD ../initramfs-linux-libre.img
    LABEL parabolafallback
    MENU LABEL Parabola GNU/Linux-libre fallback, with generic kernel
    LINUX ../vmlinuz-linux-libre
    ...
    INITRD ../initramfs-linux-libre-fallback.img
