---
File: ~/.dotfiles/wiki/telescope.md
Last Change: Sat, 20 Apr 2024 - 12:53:22
tags: [nvim, plugins]
---

# telescope neovim plugin

fixing fzf native:
source: https://chat.deepseek.com/a/chat/s/23c03042-cd38-4565-ad12-3c7c685f786f

```sh
cd ~/.local/share/nvim/lazy/telescope-fzf-native.nvim
make
```

## reverse path display

Long paths make it sometimes difficult to see the file name. You can create a custom formatter, but I thought it would be great if we can all have a format similar to Intelij, where the file name is in front.

```lua
path_display = {
    filename_first = {
        reverse_directories = false
    }
},
```

##  Fix fzf-native issue

```sh
rm -rf ~/.local/share/nvim/lazy/telescope-fzf-native.nvim/build
:Lazy sync
```

Manually build

```sh
cd ~/.local/share/nvim/lazy/telescope-fzf-native.nvim
make
```

```vim
:Telescope fzf
```

