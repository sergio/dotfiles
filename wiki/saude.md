---
file: saude.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown:nospell:
---

# Arquivo: dicas sobre saude

## Porquê não podemos deixar a cozinha suja:

De acordo com o Feng Shui, a cozinha é considerada um ambiente sagrado. Na sabedoria oriental, a cozinha alimenta a força energética que ativa a riqueza e a prosperidade de um lar.
.
Em resumo, a cozinha representa nossa riqueza, nossa saúde e o bem estar de nossa família. Sempre limpe e cuide muito bem sua cozinha e de tudo que nela se encontra. Mantenha esse local com uma energia fresca, saudável e próspera.⁣ ⁣ Na cozinha armazenamos e preparamos os alimentos que serão responsáveis por nos nutrir e nos proporcionar saúde, disposição e bem estar. Na Cabala da Casa a pia, por ser fonte do elemento água, representa os aspectos emocionais.
.
Quando está sempre suja, cheia de louças e restos de alimentos aponta instabilidade emocional com muita briga, nervosismo e mágoas. Observe também se há vazamentos. Os vazamentos em geral provocam perdas financeiras.
.
A geladeira também é uma peça importente e tem relação com os sentimentos do inconsciente, simbolizando o depósito de emoções. Muitas pessoas que passaram por dificuldade emocional não conseguem organizar sua geladeira. Geralmente perdem dinheiro e tempo, deixando que produtos estraguem por não os 'enxergarem' guardados ali. Busque manter a geladeira limpa e organizada.⁣ ⁣ O fogão é a representação do elemento fogo que está ligado a prosperidade da casa. Graças a ele preparamos os alimentos e construímos nele energia do nosso lar. Se ele estiver entupido, constantemente sujo ou entulhado, há uma sinalização de interrupção das fontes de renda, força e vitalidade. Portanto, é fundamental manter o fogão limpo e desobstruído em sua cozinha. O fogão é o símbolo da fartura e prosperidade dentro de casa.
.
Dentro do Feng Shui, a limpeza de um ambiente é essencial, uma vez que está diretamente associada à fluidez das boas energias. Por isso, a cozinha deve estar limpa e organizada. A técnica de Feng Shui se baseia em garantir um bom fluxo de boas energias circulando de maneira abundante em seus espaços. Aplicar Feng Shui é fazer a vida fluir em todos os aspectos Com amor

## O'ring test
+ https://www.youtube.com/watch?v=A9Ir4Exh6uk

O'ring test: conheci o O'ring test em l995, através do sr. Kimio Yamamoto,
pelo qual agradeço com profundo respeito.  É uma metodologia desenvolvida e
patenteada pelo médico professor dr.Yoshiaki Omura, nos  EE.UU. em 1976. deixo
uma fonte de pesquisa no YouTube para pesquisa:BDORT, comprovação clínica no
auxílio do diagnóstico. com a dra. Lucy Harasawa

faz um anel numa mão e segura ou aponta para um alimento ou medicamento,
se for bom para a pessoa você não consegue abrir os dedos, se for ruim você
consegue abrir os dedos.

## Livro: A saúde brota da Natureza

É um livro digital com mais de 500 páginas sobre tratamentos naturais

## como evitar [[ataquecardiaco]]

## cerveja e agua [[cervejaeagua]]

## Curas naturais para gastrite e úlcera

Babosa com água em jejum
Leite de pinhão brabo:
    começa com uma gota por dia até nove
    depois volta a diminuir

## cura natural para pedra nos rins

    água da raiz da malícia
    a folha da malícia é um poderoso anti-inflamatório

## 5 componentes essenciais para a saude
http://www.youtube.com/watch?v=MFp5QLR6xpE&feature=watch-vrec

   Vitamina D  (evita câncer) tomar sol
   Iodo
   Magnésio
   Omega 3 (fonte: berduégua ou belduega)

## Barro para tratamento de picadas peçonhentas

Aplique barro molhado sobre o local, pode ser misturado
com carvão em pó, o que potencializa o seu efeito

## Como provocar febre atificialmente
Coloque um dente de alho embaixo das axilas

A febre mata bactérias no corpo

## Exercícios para a visão

http://www.youtube.com/watch?v=S2Vjib_tGj4&feature=endscreen&NR=1

     Entendimento de que a lente do olho
     é movida por músculos (em geral cansados)

     O primeiro pilar da boa visão é aprender a focar
     O segundo pilar da boa visão é movimentar os olhos
     O terceiro pilar da boa visão é o piscar

     Piscar os olhos naturalmente (5 ou 10 minutos)
     Espalmação (não deixar entrar luz nos olhos por 5 ou 10 minutos)
     Olhar solar (ficar de frente para o sol com os olhos fechados)
     Relaxamento da ioga balançando os braços
     Leitura de um livro de cabeça para baixo (focar a visão)

## agua alcalina

A água alcalina não é absorvida no estado ALCALINO pelo organismo, pois quando
ela atinge o estômago, que é um ambiente ácido, ela se acidifica. Mas
é justamente na ACIDIFICAÇÃO da água com PH elevado que ocorre a grande
contribuição de se ingerir água alcalina. Toda vez que o estômago é atingido,
digamos, pela água com pH alto, o pH do estômago vai naturalmente se elevar num
primeiro momento. O corpo vai imediatamente responder ao “erro” produzindo
ácido clorídrico para corrigir o pH do estômago. Acontece que por um mecanismo
de compensação, o sistema que produz ácido clorídrico dispara um gatilho que
faz com que o pâncreas produza bicarbonato de sódio, cujo pH é acima de oito,
super alcalino, e esse bicarbonato entra direto no plasma sanguíneo,
ALCALINIZANDO todo o sistema. Como água não produz nenhum tipo de resíduo, e,
diga-se de passagem, todo resíduo tem no final uma natureza acídica quando
entra na corrente sanguínea, essa produção digamos extra de bicarbonato de
sódio fica a crédito em um sistema que após os 45 anos mais ou menos do
indivíduo, começa a diminuir, aumentando a velocidade do envelhecimento.

