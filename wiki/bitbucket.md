---
File: ~/.dotfiles/wiki/bitbucket.md
Author: Sergio Araujo
Last Change: Thu, 25 Apr 2024 - 08:05:39
tags: [git, tools, bitbucket]
---

#  bitbucket.md intro:

## my snippets

+ https://bitbucket.org/dashboard/snippets

## Downloading raw files from bitbucket

    wget -c https://bitbucket.org/sergio/dotfaster/raw/master/inputrc -O ~/.inputrc
    wget -c https://bitbucket.org/sergio/dotfaster/raw/master/algorithm/shell/bash/settermux.sh -O settermux.sh

    # the pattern is:
    https://bitbucket.org/<username>/<repo-name>/raw/<branch>/<file-name>

## snippets creation from command line

    curl -X POST https://api.bitbucket.org/2.0/snippets/sergio -u sergio -F file=@bitbucket.md

    https://bitbucket.org/sergio/workspace/snippets/eqLnaA/themelua
