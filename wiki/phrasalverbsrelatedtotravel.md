---
file: phrasalverbsrelatedtotravel.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

Eventually he gets promoted up to being a lieutenant and becomes the right-hand man to the family boss.
Finalmente, ele é promovido a tenente e se torna o braço direito do chefe da
família.

We didn't want to miss the flight, so we decide to set off [começar]
really early. We got into the taxi at 6.am. But unfortunately the
taxi broke down [pifou] on the way to the airport and we had to put
back [largar] with our luggae. Luckily, a bus came along [apareceu]
and we got on it.

By the time we got off [saimos] the bus at the airport and we went to
check in [embarcamos] we were 20 minutes late. But by another stroke
of luck the departure been called off [adiada] by one hour; the plane
toke off [decolou] 70 minutes late, but there was a following Wind so
it touch down [pousou] with only a 50 minutes delay.
