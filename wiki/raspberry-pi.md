---
file: raspberry-pi.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: gather infromatin about raspberry pi
---
#  raspberry-pi.md intro:


## Re-use your, old laptop screen to build a cool desktop with Raspberry pi.
+ http://bit.ly/333uJAF
+ http://bit.ly/2ObzS5L
+ http://bit.ly/2ObzS5L

## Acessórios essenciais
+ https://www.youtube.com/watch?v=MDrRNXPhr-c

1 - Adaptador micro HDMI
2 - Power Suply USB-C (5V 3A - 100 ~ 240v)
3 - Cabo micro HDMI to HDMI
4 - USB-C to HDMI - alternative power supply

## Passive cooling raspberry pi - heatsink
+ [Flirc Raspberry Pi 4 Case](https://flirc.tv/more/raspberry-pi-4-case)
+ [another video](https://youtu.be/4Yq6vfEOtcM)

## O potencial educativo do raspberry pi
+ https://www.edgarcosta.net/raspedu/
+ http://bit.ly/2OeYaf1

## voidlinux over raspberry pi 4
+ http://bit.ly/35s7jXj

## Windows 10 raspberrypi download
+ https://www.youtube.com/watch?v=3rjVrEQDjxA
