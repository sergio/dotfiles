---
File: ~/.dotfiles/wiki/wifi.md
Last Change: Fri, Dec 2024/12/06 - 14:18:10
tags: [wifi, network, voidlinux]
---

#  Configurar a rede no voidlinux

Configuração de Rede via WiFi no Void Linux (Versão Base)

Este guia fornece os passos necessários para configurar a rede via WiFi no Void Linux usando ferramentas de linha de comando.

## 1. Instalar as Ferramentas Necessárias

Primeiro, instale as ferramentas necessárias para gerenciar a rede. O pacote `wpa_supplicant` é comumente usado para conectar-se a redes WiFi.

```bash
sudo xbps-install -S wpa_supplicant
```

## 2. Criar um Arquivo de Configuração do `wpa_supplicant`

O `wpa_supplicant` usa um arquivo de configuração para definir as redes WiFi às quais você deseja se conectar. Crie ou edite o arquivo de configuração em `/etc/wpa_supplicant/wpa_supplicant.conf`.

```sh
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```

Adicione as seguintes linhas ao arquivo, substituindo `"SSID"` pelo nome da sua rede WiFi e `"senha"` pela senha da sua rede:

```plaintext
network={
    ssid="SSID"
    psk="senha"
}
```

Se a sua rede WiFi usa uma senha WPA-PSK, você pode gerar o hash da senha usando o comando `wpa_passphrase`:

```bash
wpa_passphrase "SSID" "senha" | sudo tee -a /etc/wpa_supplicant/wpa_supplicant.conf
```

## 3. Iniciar o `wpa_supplicant`

Agora, você precisa iniciar o `wpa_supplicant` e associá-lo à interface de rede WiFi. Primeiro, encontre o nome da sua interface WiFi (geralmente algo como `wlan0` ou `wlp2s0`):

```bash
ip link
```

Em seguida, inicie o `wpa_supplicant` e associe-o à interface WiFi:

```bash
sudo wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
```

## 4. Obter um Endereço IP

Depois de conectar-se à rede WiFi, você precisa obter um endereço IP. Isso pode ser feito usando o `dhcpcd`:

```bash
sudo xbps-install -S dhcpcd
sudo dhcpcd wlan0
```

## 5. Verificar a Conexão

Para verificar se você está conectado à internet, use o comando `ping`:

```bash
ping -c 4 google.com
```

Se tudo estiver configurado corretamente, você deve ver pacotes sendo enviados e recebidos.

## 6. Automatizar a Inicialização (Opcional)

Se você quiser que a conexão WiFi seja iniciada automaticamente na inicialização, você pode criar um serviço para o `wpa_supplicant` e `dhcpcd`.

### Para `runit` (padrão no Void Linux):

1. Crie um script de inicialização para o `wpa_supplicant`:

```bash
sudo nano /etc/sv/wpa_supplicant/run
```

Adicione o seguinte conteúdo:

```bash
#!/bin/sh
exec wpa_supplicant -B -i wlan0 -c /etc/wpa_supplicant/wpa_supplicant.conf
```

2. Torne o script executável:

```bash
sudo chmod +x /etc/sv/wpa_supplicant/run
```

3. Crie um script de inicialização para o `dhcpcd`:

```bash
sudo nano /etc/sv/dhcpcd/run
```

Adicione o seguinte conteúdo:

```bash
#!/bin/sh
exec dhcpcd wlan0
```

4. Torne o script executável:

```bash
sudo chmod +x /etc/sv/dhcpcd/run
```

5. Habilite os serviços:

```bash
sudo ln -s /etc/sv/wpa_supplicant /var/service/
sudo ln -s /etc/sv/dhcpcd /var/service/
```

Agora, a conexão WiFi deve ser configurada automaticamente na inicialização.

### Conclusão

Esses passos devem permitir que você configure a rede via WiFi no Void Linux na versão base. Se você encontrar algum problema, verifique os logs para obter mais informações sobre o que pode estar errado.
```

Você pode copiar todo o conteúdo acima e colá-lo em um arquivo `.md` ou em qualquer editor de texto que suporte Markdown.

