---
file: uniq.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
 vim:set ft=markdown nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:
---

## Retira duplicados e mostra contagem do que estava duplicado

    sort namesd.txt | uniq –c

## Exibir somente as entradas duplicadas

    sort namesd.txt | uniq –cd

