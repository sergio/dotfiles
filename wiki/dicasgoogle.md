---
file: dicasgoogle.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [google, tools, youtube]
---

## google services tips
+ https://gbhackers.com/latest-google-dorks-list/ (google dorks)

## google keep via command line (cli):
+ https://docs.nekmo.org/gkeep/usage.html

    pipx install gkeep

## alternatives to google:
+ https://startpage.com/

## Lista de todos os recursos do google
+ http://www.google.com/help/features.html
+ http://lmgtfy.com/

## google em inglês
+ http://www.google.com/webhp?hl=options

## Google's useful links
+ https://www.labnol.org/internet/important-google-urls/28428/

## Como encontrar fontes no google

    intitle "index of" (ttf | otf) meridean -html -htm -php -asp -cf -jsp

## find passwords in log files

    allintext:password filetype:log after:2018

## Busca por proximidade - AROUND(n)

A search query like "CNN Obama" will mostly show CNN pages that are
related to Obama. However, if we modify the query to look like “CNN
AROUND(2) Obama,” you get results where the two terms are written on the
page in close proximity

OBS: Make sure to put AROUND in caps else it wont work

## Link direto pra baixar filmes

    To find direct download link for movies, search “[movie name]

    -inurl:(htm|html|php|pls|txt) intitle:index.of "last modified" (mkv|mp4|avi)”.

## Como buscar fontes, livros e mp3 no google?

    -inurl:htm -inurl:html intitle:"index of" +("/ebooks""/book")
    -inurl:htm -inurl:html intitle:"index of" "Last modified" mp3

    +("index of") +("/ebooks"|"/ebook") +(chm|pdf|zip|rar) +health
    +("index of") +("/ebooks"|"/ebook") +(chm|pdf|zip|rar) +"Radiology Tips"

## Achar câmeras online

    inurl:"view/index.shtml"
    http://members.upc.nl/a.horlings/doc-google.html

## tempo em fortaleza

    wheater fortaleza

## fuso horario

   time in fortaleza

##  tamanho das imagens

    nordeste imagesize:1600x1200

## optional search

    "jonny quest" (mediafire | 4shared)

## search withing websites

    vivaotux.blogspot.com bash

## time frame

    scientific discoveries 2018...2018

## youtube shortcuts

# tip: use gr on vim to add content on cells
+ https://www.hongkiat.com/blog/useful-youtube-keyboard-shortcuts-to-know/

``` markdown
┌─────────────────┬──────────────────────────────────────────────────────┐
│  Keyword        │  what it does                                        │
├─────────────────┼──────────────────────────────────────────────────────┤
│     K           │  Play / pause  - Holding down this key (slow motion) │
│     M           │  Mute                                                │
│     F           │  Full screen                                         │
│     C           │  Toggle closed captions                              │
│     B           │  Change CC background color                          │
│     j           │  Go back 10 seconds                                  │
│     L           │  Go forward 10 secods                                │
│     ←           │  Go back 5 seconds                                   │
│     →           │  Go forward 5 seconds                                │
│     ↑           │  Increase volume                                     │
│     ↓           │  Decrease volume                                     │
│     Ctrl + →    │  Move to next video (only in playlist)               │
│     >           │  Increase player speed                               │
│     <           │  Decrease player speed                               │
│     Shift + P   │  Play previous played video                          │
│     Shift + N   │  Play next played video in recommendations playlist  │
└─────────────────┴──────────────────────────────────────────────────────┘
```

# contas google

    quanto de espaço estou ocupando na minha conta google?
    https://www.google.com/settings/storage
