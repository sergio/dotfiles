---
Filename: xmenu.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## xmenu

Add An Applications Menu To Any Window Manager:
+ https://www.youtube.com/watch?v=wMrdCbrQjnQ
+ https://github.com/phillbush/xmenu


## Easily create your menu:
+ https://github.com/OliverLew/xdg-xmenu

é um pequeno script de shell para gerar o arquivo de menu da área de trabalho xdg para o xmenu , um utilitário de menu x11 simples.

Este script é apenas um projeto de brinquedo e não o testei fora de minha própria máquina, portanto, erros são esperados e fique à vontade para relatá-los aqui.
Requerimento

O script irá procurar por ícones de acordo com as especificações do freedesktop . Se os ícones encontrados forem imagens svg, o que é muito possível, você precisará de qualquer uma das seguintes ferramentas de conversão (classificadas por prioridade):

    rsvg-convert

    : fornecido por

    librsvg

    pacote, é o mais rápido entre os três.

    convert

    : fornecido pelo

    imagemagick

    pacote.

    inkscape

    : isso é quase tão rápido (ou lento) quanto

    convert

Os ícones svg serão convertidos em imagens png e armazenados em cache na

~/.cache/xdg-xmenu/icons
