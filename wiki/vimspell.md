---
Filename: vimspell.md
Last Change: Thu, 21 Mar 2024 - 19:11:30
tags: [vim, nvim, spell]
---

## How to change the highlight style in Vim spellcheck?

+ [Change highlight in spell](https://stackoverflow.com/questions/6008921/)

```vim
hi clear SpellBad
hi SpellBad cterm=underline
```

## How to use the vim spell feature:

						*]s*
]s			Move to next misspelled word after the cursor.
			A count before the command can be used to repeat.
			'wrapscan' applies.

							*[s*
[s			Like "]s" but search backwards, find the misspelled
			word before the cursor.  Doesn't recognize words
			split over two lines, thus may stop at words that are
			not highlighted as bad.  Does not stop at word with
			missing capital at the start of a line.

							*]S*
]S			Like "]s" but only stop at bad words, not at rare
			words or words for another region.

							*[S*
[S			Like "]S" but search backwards.

To add words to your own word list:

							*zg*
zg			Add word under the cursor as a good word to the first
			name in 'spellfile'.  A count may precede the command
			to indicate the entry in 'spellfile' to be used.  A
			count of two uses the second entry.

			In Visual mode the selected characters are added as a
			word (including white space!).
			When the cursor is on text that is marked as badly
			spelled then the marked text is used.
			Otherwise the word under the cursor, separated by
			non-word characters, is used.

			If the word is explicitly marked as bad word in
			another spell file the result is unpredictable.

							*zG*
zG			Like "zg" but add the word to the internal word list
			|internal-wordlist|.

							*zw*
zw			Like "zg" but mark the word as a wrong (bad) word.
			If the word already appears in 'spellfile' it is
			turned into a comment line.  See |spellfile-cleanup|
			for getting rid of those.

							*zW*
zW			Like "zw" but add the word to the internal word list
			|internal-wordlist|.

zuw							*zug* *zuw*
zug			Undo |zw| and |zg|, remove the word from the entry in
			'spellfile'.  Count used as with |zg|.

zuW							*zuG* *zuW*
zuG			Undo |zW| and |zG|, remove the word from the internal
			word list.  Count used as with |zg|.

z=			For the word under/after the cursor suggest correctly
			spelled words.  This also works to find alternatives
			for a word that is not highlighted as a bad word

