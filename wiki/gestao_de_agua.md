# Gestão de água

---
#!/bin/bash

# Regras para tarifas

Gestão de água
Requisitos:
O valor do metro cúbico de água é 0.74
Caso o consumo ultrapasse os 20m³ são adicionados mais 1.50
Até 29m³, (para cada 10m³) são adicionados mais 0.50 centavos
Faixas de consumo para água
m³ 0.74
de 20m³ a 30m³ Soma 0.74 + 1.50 por cada metro
De 30 a 40m³ Soma 0.74 + 2.00 por cada metro
De 40 a 50m³ Soma 0.74 + 2.50 por cada metro
Exemplos:
Consumo de 8m³ 8 x 0.74
Consumo de 22m³ 22 x 0.74 + 3.00
Consumo de 34m³ 34 x 0.74 + (15reais dos 20 aos 30) + (8 reais dos 30 aos 34)
Consumo de 46m³ 46 x 0.74 + (15 reais do 20 aos 30) + (20 reais dos 30 aos 40) + (15
reais dos 40 aos 46)
Custo do operador:
1.50 por cada metro cúbico até 10m³
A cada m³ acima dos 10m³ adiciona mais 0.15 centavos por cada metro cúbico
Custo da energia:
0.40 centavos por cada m³
Multa por pagamento em atraso: 10% sobre o total
E será aplicada se houver um atraso de 20 dias à partir da data de vencimento do cliente.
OBS: a tabela unidade_consumidora deverá ter um campo com a data de vencimento da
mesma que servirá como referência para aplicação das multas.
Na fatura deve haver um espaço para comunicação tipo:
Valor de emissão de segunda via,
Valor da Substituição de hidrômetro,
Valor da ligação nova (incluindo hidrômetro em parcelas)

# Crie o banco de dados
mysql -u root -p < create_database.sql

# Crie o usuário e a senha
mysql -u root -p -e "GRANT ALL ON gestao_de_agua.* TO sergio@localhost IDENTIFIED BY '123'"
```

```sql
CREATE TABLE unidades_consumidoras (
  id INT AUTO_INCREMENT PRIMARY KEY,
  codigo_hidrometro VARCHAR(10) NOT NULL UNIQUE,
  endereco VARCHAR(255) NOT NULL,
  numero VARCHAR(10) NOT NULL,
  responsavel VARCHAR(255) NOT NULL,
  cpf VARCHAR(11) NOT NULL,
  telefone VARCHAR(15) NOT NULL,
  email VARCHAR(255) NOT NULL
);

CREATE TABLE consumos (
  id INT AUTO_INCREMENT PRIMARY KEY,
  id_unidade_consumidora INT NOT NULL,
  leitura_anterior INT NOT NULL,
  leitura_atual INT NOT NULL,
  data_leitura DATE NOT NULL,
  valor_total DECIMAL(10,2) NOT NULL
);
```

```php
<?php

// Configurações do banco de dados
define("DB_HOST", "localhost");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_DATABASE", "gestao_de_agua");

// Valores das tarifas
define("VALOR_METRO_CUBICO", 0.74);
define("VALOR_ENERGIA", 0.05);
define("VALOR_OPERADOR", 0.02);

// Configuração do email
define("EMAIL_HOST", "smtp.gmail.com");
define("EMAIL_PORT", 587);
define("EMAIL_USERNAME", "seu_email@gmail.com");
define("EMAIL_PASSWORD", "sua_senha");

// Configuração da segurança
define("APP_KEY", "sua_chave_secreta");

// Configuração do ambiente
define("APP_ENV", "local");
```

```php
<?php


// script para gerar a fatura
require_once "config.php";

// Obtém os dados da unidade consumidora
$id_unidade_consumidora = $_GET["id_unidade_consumidora"];
$unidade_consumidora = $pdo->query("SELECT * FROM unidades_consumidoras WHERE id = $id_unidade_consumidora")->fetch(PDO::FETCH_ASSOC);

// Obtém os dados do consumo
$consumo = $pdo->query("SELECT * FROM consumos WHERE id_unidade_consumidora = $id_unidade_consumidora")->fetch(PDO::FETCH_ASSOC);

// Calcula o consumo
$consumo_total = $consumo["leitura_atual"] - $consumo["leitura_anterior"];

// Calcula o valor da fatura
$valor_da_agua = $consumo_total * $config["valor_metro_cubico"];
$valor_da_energia = $consumo_total * $config["valor_energia"];
$valor_do_operador = $consumo_total * $config["valor_operador"];
$valor_total = $valor_da_agua + $valor_da_energia + $valor_do_operador;

// Aplica os descontos
if ($consumo_total > 20 && $consumo_total <= 30) {
  $valor_total += $consumo_total * 0.15;
} else if ($consumo_total > 30) {
  $valor_total += $consumo_total * 0.20;
  for ($i = 30; $i <= $consumo_total; $i += 10) {
    $valor_total += $i * 0.50;
  }
}

// Adiciona as taxas e multas
$valor_total += $consumo["taxas_multas"];

// Gera a fatura
$html = <<<HTML
<html>
<head>
  <title>Fatura de água</title>
</head>
<body>
  <h1>Fatura de água</h1>
  <p>
    <b>Consumo:</b> <span class="math-inline">consumo\_total m³
</p\>
<p\>
<b\>Valor da água\:</b\> R</span> <span class="math-inline">valor\_da\_agua
</p\>
<p\>
<b\>Valor da energia\:</b\> R</span> <span class="math-inline">valor\_da\_energia
</p\>
<p\>
<b\>Valor do operador\:</b\> R</span> <span class="math-inline">valor\_do\_operador
</p\>
<p\>
<b\>Taxas e multas\:</b\> R</span> <span class="math-inline">consumo\["taxas\_multas"\]
</p\>
<p\>
<b\>Valor total\:</b\> R</span> $valor_total
  </p>
  <p>
    <b>Pagamento até:</b> 30/09/2023
  </p>
</body>
</
```

```php
//script gerador de consumos
<html>
<head>
  <title>Gerar CSV de consumos</title>
</head>
<body>
  <h1>Gerar CSV de consumos</h1>

  <form action="gerar_csv.php" method="post">
    <div>
      <label for="id_unidade_consumidora">ID da unidade consumidora</label>
      <input type="number" name="id_unidade_consumidora" id="id_unidade_consumidora" readonly>
    </div>
    <div>
      <label for="responsavel">Responsável</label>
      <input type="text" name="responsavel" id="responsavel" readonly>
    </div>
    <div>
      <label for="leitura_anterior">Leitura anterior</label>
      <input type="number" name="leitura_anterior" id="leitura_anterior">
    </div>
    <div>
      <label for="leitura_atual">Leitura atual</label>
      <input type="number" name="leitura_atual" id="leitura_atual">
    </div>
    <button type="submit">Adicionar consumo</button>
  </form>
</body>
</html>
```

```html

<html>
<head>
  <title>Cadastro de unidades consumidoras</title>
</head>
<body>
  <h1>Cadastro de unidades consumidoras</h1>

  <form action="unidades_consumidoras.php" method="post" onsubmit="return validar()">
    <div>
      <label for="codigo_hidrometro">Código do hidrômetro</label>
      <input type="number" name="codigo_hidrometro" id="codigo_hidrometro" required>
    </div>
    <div>
      <label for="endereco">Endereço</label>
      <input type="text" name="endereco" id="endereco" required>
    </div>
    <div>
      <label for="numero">Número</label>
      <input type="number" name="numero" id="numero" required>
    </div>
    <div>
      <label for="responsavel">Responsável</label>
      <input type="text" name="responsavel" id="responsavel" required>
    </div>
    <div>
      <label for="cpf">CPF</label>
      <input type="text" name="cpf" id="cpf" required>
    </div>
    <div>
      <label for="telefone">Telefone</label>
      <input type="text" name="telefone" id="telefone" required>
    </div>
    <div>
      <label for="email">E-mail</label>
      <input type="email" name="email" id="email" required>
    </div>
    <button type="submit">Cadastrar</button>
  </form>

  <script>
    function validar() {
      // Verifica se todos os campos obrigatórios foram preenchidos
      var camposObrigatorios = ["codigo_hidrometro", "endereco", "numero", "responsavel", "cpf", "telefone", "email"];
      for (var i = 0; i < camposObrigatorios.length; i++) {
        var campo = document.getElementById(camposObrigatorios[i]);
        if (campo.value === "") {
          alert("O campo " + campo.name + " é obrigatório!");
          return false;
        }
      }

      // Verifica se o CPF é válido
      var cpf = document.getElementById("cpf").value;
      if (!validarCPF(cpf)) {
        alert("O CPF informado é inválido!");
        return false;
      }

      // Verifica se o e-mail é válido
      var email = document.getElementById("email").value;
      if (!validarEmail(email)) {
        alert("O e-mail informado é inválido!");
        return false;
      }

      return true;
    }

    function validarCPF(cpf) {
      // Valida o CPF de acordo com a regra da Receita Federal
      var soma = 0;
      var multiplicador = 10;
      for (var i = 0; i < 10; i++) {
        soma += multiplicador * cpf[i];
        multiplicador = (multiplicador === 10) ? 11 : multiplicador - 1;
      }

      var resto = 11 - (soma % 11);
      if (resto === 10 || resto === 11) {
        resto = 0;
      }

      var digito1 = cpf[9];
      var digito2 = cpf[10];

      if (digito1 !== resto || digito2 !== (11 - (resto * 10)) % 11) {
        return false;
      } else {
        return true;
      }
    }

    function validarEmail(email) {
      // Valida o e-mail de acordo com o padrão RFC 5322
      var regex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;
      return regex.test(email);
    }
  </script
</html>
```



```html
<html>
<head>
  <title>Consumos individuais</title>
</head>
<body>
  <h1>Consumos individuais</h1>

  <form action="gerar_csv.php" method="post" onsubmit="return validar()">
    <div>
      <label for="id_unidade_consumidora">ID da unidade consumidora</label>
      <input type="number" name="id_unidade_consumidora" id="id_unidade_consumidora" readonly>
    </div>
    <div>
      <label for="leitura_anterior">Leitura anterior</label>
      <input type="number" name="leitura_anterior" id="leitura_anterior" required>
    </div>
    <div>
      <label for="leitura_atual">Leitura atual</label>
      <input type="number" name="leitura_atual" id="leitura_atual" required>
    </div>
    <button type="submit">Adicionar consumo</button>
  </form>

  <script>
    function validar() {
      // Verifica se todos os campos obrigatórios foram preenchidos
      var camposObrigatorios = ["leitura_anterior", "leitura_atual"];
      for (var i = 0; i < camposObrigatorios.length; i++) {
        var campo = document.getElementById(camposObrigatorios[i]);
        if (campo.value === "") {
          alert("O campo " + campo.name + " é obrigatório!");
          return false;
        }
      }

      // Verifica se a leitura atual é maior ou igual à leitura anterior
      var leitura_anterior = document.getElementById("leitura_anterior").value;
      var leitura_atual = document.getElementById("leitura_atual").value;
      if (leitura_atual < leitura_anterior) {
        alert("A leitura atual deve ser maior ou igual à leitura anterior!");
        return false;
      }

      return true;
    }
  </script>
</body>
</html>
```
