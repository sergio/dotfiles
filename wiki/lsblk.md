---
file: lsblk.md
author: Sergio Araujo
Last Change: Wed, 18 Dec 2024 - 10:10:10
 vim:ft=markdown
tags: [usb, stick, livecd, tools, mount]
---

## How do I know the device path to an USB-stick?

+ https://askubuntu.com/a/311775/3798

  lsblk -l

Mostra a identificação única de cada dispositivo de tal modo
que se por exemplo você montar um pendrive pela sua identificação
única UUID, independente da entrada ele será identificado
igualmente.

outro comando relacionado é o "blkid"

    mkdir -p /mnt/pendrive

    mount /dev/sdX1 /mnt/pendrive
