---
File: xwallpaper.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [tools, wallpaper, desktop]
---

#  xwallpaper.md intro:
wallpaper setting utility for x

    xwallpaper --zoom path/to/your/wallpaper.jpg

