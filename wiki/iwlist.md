---
file: iwlist.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
vim:ft=markdown
tags: [wifi, network, admin, sysadmin]
---

## procurar redes wireless disponíveis

    iwlist device scanning
    iwlist wlan0 scanning

como pegar informações das redes sem fio
iwlist wlan0 scan

    iwconfig wlan0 essid 'nomedaredesemfio'

## Get your wifi name:
+ https://askubuntu.com/questions/117065

How do I find out the name of the SSID I'm connected to from the command line?

    iwgetid -r

## Referencias
+ va.mu/bRcZ
