---
file: head.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

O comando head por padrão exibe as 10 primeiras linhas de um arquivo
caso queira exibir apenas as 5 primeiras linhas faça:

head -5 /etc/passwd

