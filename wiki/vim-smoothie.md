---
File: vim-smoothie.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
Date: ago 14, 2021
tags: [vim, neovim, plugis, nvim]
---

#  vim-smoothie.md intro:

    use 'psliwka/vim-smoothie'       -- nice scrolling

    -- scrolling keybinds
    -- https://pastebin.com/7FKAH6ah
    map('n', '<CR>', ':<C-U>call smoothie#downwards()<CR>')
    map('n', '<BS>', ':<C-U>call smoothie#upwards()<CR>')
