---
file: playonline.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## PlayOnLinux é incapaz de encontrar as bibliotecas OpenGL 32bits
+ https://ubuntuforum-br.org/index.php?topic=117284.0

Esse problema ocorre porque o PlayOnLinux depende da versão 32 bits do Wine para funcionar.
Mesmo que você tenha um sistema 64 bits, você deverá ter instalado em conjunto o Wine 32 bits.

    sudo apt-get install wine32

Ou:

    sudo apt-get install wine:i386
