---
file: aspell.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

#  aspell.md intro:

To fix spell mistakes on \LaTeX\ documents just type

    aspell -t book.tex
