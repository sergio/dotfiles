---
file: qutebrowser.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: teach some tricks about qutebrowser
---
#  qutebrowser.md intro:
+ https://jamesbvaughan.com/qutebrowser/

    :home ........... abre a p�gina inicial (duckduckgo)
    :help ........... quick guide
    :history ........ see history
    :open url ....... open url
    m ............... set current page as quickmark
    O ............... Open list of history/quickmarks
    o ............... :open
    J ............... next tab
    K ............... previous tab
    d ............... delete (close) tab
    co .............. close other tabs
    H ............... back
    L ............... forwar
    M ............... bookmark current page
    yy .............. copy current url
    yY .............. copy current url to primary selection
    pp .............. paste clipboard as url
    f ............... show links
    ss .............. set something
    :clone .......... clona aba
    Ctrl-w .......... close tab
    :history ........ show history
    :history clear .. clear history
    :set ............ settings
    :bind ........... hotkeys (keybindings)

## quickmarks



## viewing bookmarks

    :bind <F2> bookmark-list

On download page use Alt+y to copy url

## addblock-update
+ http://www.ii.com/qutebrowser-tips/

Built-in add-on, just type:

    :adblobck-update

