---
file: startx.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [x11, desktop]
---

# Abrir outra seção gráfica
Para abrir outro modo gráfico

    startx -- :1

inicia no display 1 (ctrl+alt+F8)
no meu caso usei (ctrl+alt+F9)

Iniciando uma segunda seção do dwm:

    Ctrl+Alt+F2
    cd ~/.dotfiles/dwm
    startx ./dwm

Usando o método acima eu posso testar configurações do dwm em uma
segunda seção gráfica

