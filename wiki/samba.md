---
Filename: samba.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

---
    doas ln -s /etc/sv/smbd /var/service/

    sudo groupadd --system smbgroup
    sudo useradd --system --no-create-home --group smbgroup -s /bin/false smbuser

    sudo chown -R smbuser:smbgroup /home/share
    sudo chmod -R g+w /home/share
```


```conf
[global]
	workgroup = WORKGROUP
	netbios name = void-linux
    log file = /var/log/samba/%m
    log level = 1
    server role = standalone server
	wins support = yes
    name resolve order = bcast wins lmhosts

[share]
    # This share requires authentication to access
    path = /home/share
    read only = no
    inherit permissions = yes
```


