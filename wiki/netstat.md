---
file: netstat.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução
exibe a lista de IP de quem está conectado na maquina.

netstat -na

Para saber as rotas existentes na sua maquina use esse comando.

netstat -r

## out which TCP ports are listening and opened by which process in verbose
fonte: command-line-fu

netstat -tlvp
