Filename: rss.md
Last Change: Thu, 10 Oct 2024 - 00:16:22
tags: [rss]

## Some linux rss readers

+ <https://www.makeuseof.com/best-rss-feed-readers-for-linux/>
+ <https://gitlab.com/news-flash/news_flash_gtk>
+ [Furando a bolha com rss](https://dev.to/computandoarte/utilizando-o-rss-para-furar-a-bolha-54ai)

    flatpak install flathub org.gnome.FeedReader

## my feeds

```txt
https://sidneyliebrand.io/feed.xml
https://meleu.sh/index.xml
https://www.linuxfordevices.com/feed linux
https://rss.art19.com/think-fast-talk-smart
https://opensource.com/feed
https://www.linuxshelltips.com/feed
https://feeds.simplecast.com/Y8lFbOT4 podcasts freakonomics
https://lukesmith.xyz/rss.xml "linux" "suckless"
https://vkc.sh/feed/
http://feeds.feedburner.com/neuroscience-rss-feeds-neuroscience-news science mind
http://www.inovacaotecnologica.com.br/boletim/rss.xml science tech
https://reddit.com/r/voidlinux.rss "voidlinux" "~Reddit: r/voidlinux"
https://reddit.com/r/dwm.rss "voidlinux" "~Reddit: r/dwm"
https://reddit.com/r/linux.rss "linux" "~Reddit: r/linux"
https://reddit.com/r/commandline.rss "commandline" "linux"
https://landchad.net/rss.xml
https://based.cooking/rss.xml
https://artixlinux.org/feed.php "tech"
https://itsfoss.com/feed linux
https://www.youtube.com/feeds/videos.xml?channel_id=UCMiyV_Ib77XLpzHPQH_q0qQ "~Veronica Explains (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCfhSB16X9MXhzSFe_H7XbHg "~Bryan Jenks (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCVls1GmFKf6WlTraIb_IaJg "~Distrotube (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UC2eYFnH61tmytImy1mTYvhA "~Luke Smith (YouTube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UC3pasXoRUTKPpQaKtlJz62g "~The Linux Cast (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UC3yaWWA9FF9OBog5U9ml68A "~SavvyNik (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCWwr1v9WbZ53tCJBW3i7JFg "~Gabriel (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UC1yGcBvdPGxRIMT1yo_bKIQ "~Jake@linux (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCXuqSBlHAE6Xw-yeJA0Tunw "~Linus Tech Tips (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCF7psNFg-WKwuUuT2f1PZQQ "~O Pinguin Criativo (youtube)" youtube
https://www.youtube.com/feeds/videos.xml?channel_id=UCCIHOP7e271SIumQgyl6XBQ "~OldTechBloke (youtube)" youtube
```
