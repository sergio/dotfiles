---
Filename: command.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [bash, shell, cmd, builtin]
 vim:ft=markdown
---

## Test if a command exists:
+ https://stackoverflow.com/a/677212/2571881

```sh
if ! command -v <the_command> &> /dev/null
then
    echo "<the_command> could not be found"
    exit
fi
```


