---
File: /home/sergio/.dotfiles/wiki/cmus.md
Last Change: Fri, 28 Jun 2024 - 10:27:47
tags: [music, mp3, sound, cli]
---

man cmus-tutorial

    7 ................  helper menu
    5 ................. navigate
    a ................. add folder or song
    b ................. play next
    c ................. play/pause
    m ................. toggles (artist/albun/all)
    q ................. quit
    s ................. toggle shuffle
    t ................. toggle remaining time
