Filename: ~/.dotfiles/wiki/shfmt.md
Last Change: Thu, 18 Apr 2024 - 12:07:58
tags: [bash, zsh, format, tools, shell]

# Shell parser and formatter

+ [Artigo do Meleu](https://meleu.sh/shfmt/)

    shfmt -d -i 2 hello.sh

The above code shows a diff with 2 spaces indentation

    -ci, --ci-indent: para adicionar indentação em cada opção dentro de um case

    -sr, --space-redirects: operadores de redirecionamento serão seguidos
    por um

    Para fazer o shfmt alterar o arquivo diretamente, use a opção -w, --write
