---
file: nl.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# Introdução

Show file with line numbers

    nl /etc/passwd | less

