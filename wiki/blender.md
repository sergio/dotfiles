---
file: blender.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# blender
+ https://docs.blender.org/manual/pt/2.79/index.html
+ https://www.blendernation.com/

## Mini Curso para iniciantes (as aulas do blender são de 5 a 10)
+ https://youtu.be/GB83p4ZAFvk
+ https://youtu.be/VihZVF9HOsM
+ https://youtu.be/CoBWOHQ5NU0
+ https://youtu.be/Vj-tMhc-U0s
+ https://youtu.be/nwkru6vedeY
+ https://youtu.be/jdW_GwC51F4

## blender 2.8
+ [modelando uma mesa](https://youtu.be/xMonTaOp9cg)
+ [intersect](https://youtu.be/kp2rIepnueU)
+ [mirror modifier](https://youtu.be/i1GrQJ1u6IE)
+ [chanfer](https://youtu.be/DO1SiUMTRHc)
+ [another bevel](https://youtu.be/2jTLbjbN1DY)
+ [Desenhando o escudo do capitão américa](youtu.be/QT-idgkq_-w)
+ [100 blender 2.8 tips](https://youtu.be/_9dEqM3H31g)
+ [curso blender 2.8](https://www.youtube.com/watch?v=EVUlCHVySgc&list=PLrYLf1JihKtb5pbeR6fX1bMoREsjJn0Q_)

## svg export

+ https://youtu.be/XfmdUaTPySc

## Daily Blender Secrets - The Nuts and Bolts (and screws) of Transformation
Constraints
+ https://youtu.be/saqbK-Iexfo
+ (portuguese) https://youtu.be/kMKiD6MICSI

## Onde encontrar modelos 3D para o Blender? (bonus - sites para texturas)
+ https://youtu.be/TaxIaQ8d3Tg

+ https://www.blendswap.com/
+ https://imeshh.com/
+ https://store.chocofur.com/
+ https://www.blenderkit.com/
+ https://www.cgtrader.com/
+ https://www.poliigon.com/
+ https://cc0textures.com/
+ https://hdrihaven.com/hdris/

## Modifiers - modificadores
+ https://youtu.be/QS4vJoDwKco

### Mirror modifier
+ https://youtu.be/PjorPrvrWGA

It only works on edit mode

x-mirror is under "Edit Mode" > Tool > Mirror
or even better, if you look at the top of the window the mirror icon appears
on the in, and it also shows "auto merge" option.

## Join and Separate

    ctrl + j ............. join two objects
    p .................... separate (on edit mode)

## Exportar do Revit para o Blender
+ https://youtu.be/PEbb4_RJtzk

Addon para importar IFC: http://ifcopenshell.org/
procurar o IfcBlender: http://ifcopenshell.org/ifcblender

Escolhar a versão correta para sua plataforma, instalar e ativar o addon

## How to hide relationship lines?
+ https://blender.stackexchange.com/a/126727/69021

You used to be able to hide the dotted lines of a Parent Relationship by going
to the Display tab and disabling the checkbox of "Relationship Lines". That
option cannot be found there anymore. Where is it now?

In 2.80 all things that displays on top of the renderable objects calls
overlays. You can edit what do you want to display in viewport in this
Overlays popover:

## how to draw a house in blender
+ https://youtu.be/hDwzEzTEVJU
+ https://youtu.be/ITfkvQdWaRA

## how to draw a hammer in blender
+ https://youtu.be/elUJCEC06r8

## load factory settings

    file >> load factory settings

### set scale to milimiters (units) (precision editing)
+ https://blender.stackexchange.com/a/173140/69021
+ https://youtu.be/ZI49VIcISaw
+ https://youtu.be/7hlP4-4j1do
+ https://youtu.be/8a4mm-zb3nk (precise edition)
+ https://daler.github.io/blender-for-3d-printing/printing/export-stl.html
+ https://youtu.be/8uRIL1LCmgU "Usando o Blender 2.8 Como AUTOCAD"

    1 - scene properties >
            unit scale: 0.001
                lenght: milimiters

    2 - overlays
            scale: 0.001

    3 - Increase the clipping point adding another zero

        Properties N menu > View group:
        Add a zero on the "End" option

    4 - Select the camera
                         lens > clip start/end

Another good option is to show lenght of the faces.

    Edit mode > viewport overlays > edge lenght

## Controlling the zoom
+ https://youtu.be/vfHtoBZ-Mv4

    Edit > Preferences > Navegation
    Check "Auto Depth" checkbox

### Precision modelling - CAD like approach - Blender and 3D Printing
+ https://youtu.be/fmHXqLffPkk

## show measurements (mostrar medidas)

    n → mesh display → lenght

See more at [plugins](#plugins) section

## zoom to mouse position

    Edit > Preferences > Navigation > Zoom to mouse position

### Fixing the camera - How do I increase the render distance?
+ https://blender.stackexchange.com/a/5202/69021
+ https://youtu.be/7DT0dtarYv4

The camera can only see the geometry that is within a range defined by the
clipping distance.

    camera > viewport display
    Enable the checkbox limits and change:
    "Clip start" and "end" options

Anything that that closer of further away from that distance range will be ignored.

With the camera object selected, increase its End clipping distance in Properties > Camera settings > Lens:

## select objects

    right click

## Nomeclatura

    Envelopar -> ShrinkWrap
    Chanfro   -> Bevel

## shortcuts

    NOTE: Enable middle mouse emulation on:
    Edit > Preferences > Input > Emulate 3 buttom mouse
    source: https://bit.ly/3dpQDE6

    OBS: In order to select loops of edges, when the emulation is enabled
    just hit double click + https://blender.stackexchange.com/questions/5722/

    After some time I defined the OSkey to enable the 3 button emulation
    wich means the normal "alt + clic" to select a line of vertices is not
    affected.

    F2 ................. rename object
    F4 ................. file context menu
    F4 + p ............. open preferences
    F9  ................ brings back object creation properties
    1 .................. (edit mode) vertex select
    2 .................. (mode mode) edge select
    3 .................. (mode mode) face select
    fn .  .............. Frame selected
    Ctrl + numpad + .... expand selection
    Ctrl + numpad - .... decrease selection
    Ctrl + Shift + o ... open recent
    Ctrl + Shift + r ... Offset Edge Slide (proporcional edges outward)
    Ctrl + a ........... scale
    Ctrl+Alt+Spacebar .. toggles full screen mode
    Ctrl + Alt + 0  .... set camera to the current viewport
    Ctrl + page down/up  change workspace
    Ctrl + (0 1 2 3 ..). quick subdivide
    Ctrl + fn + "+" .... expand selection
    Ctrl + <space bar> . toggles full window
    Ctrl + click ....... extend selection
    Ctrl + n ........... (edit mode) recalc normals
             shift+n ... fix normals
    Ctrl + a ........... apply menu
    Ctrl + E ........... Edges options/Specials
    Ctrl + f ........... opens face menu
    Ctrl + i ........... invert selection
    Ctrl + l ........... select all vertices
    Ctrl + b ........... add bevel (after hit Enter)
            Is there a dialog where we can increase the precision of the bevel
    Ctrl + shift + b  .. bevel a vertex
    shift + e  ......... edge crease
    shift + c .......... cursor 3d to origin
    shift + h .......... hide all objects but selected
    shift + fn + 7 ..... align to active
    shift + right click  set 3d cursor position
    shift + r .......... repeat last action
    Shift+Z: ........... Turn the "wireframe" mode on or off
    Alt + g ............ move object to the 3d cursor
    Alt + h ............ show hidden objects
    Alt + r ............ reset rotation
    Alt + m ............ merge tool
    Alt + p ............ clear parents
    right click ........ select object
    Alt+Z .............. Activate or deactivate the x-ray mode
    / .................. hide other objects and zoom in
    Alt + right click .. select all edges of a face
    A .................. add a new form (shift a) (opens mesh menu)
    D .................. duplicate (shift d) shift + d + y
    i .................. inset (using o it creates an offset)
    m .................. merge (at center / last/ by distance)
    alt + d ............ instanciate copy
    tab ................ edit mode
    Ctrl + r ........... loop subdivide (loop cut and slide) (take a look a double g)
                         OBS: if the tool was not selected by mouse you can
                         use the scroll wheel to increase or decrese the
                         number of cuts
    Ctrl + lmc + drag .. lasso selection
    a .................. select/deselect all
    b + left click ..... rectangle selection add [b -> block]
    b + middle click ... rectangle selection remove
    c + left click ..... circle selection add [c -> circle]
    c + middle click ... circle selection remove
    e .................. extrude (free extrude e + z)
    Alt + e ............ extrude with options
    F (edit mode) ...... Create face/edge
    j .................. connect vertex path
    m .................. move to collection
    n .................. toggle transform (properties)
    o .................. toggle proportional editing
    k .................. knife
    K .................. knife (cut all selected objects)
    p .................. separate menu
    s .................. change selection pointer (editing mode)
    s .................. scale (resize object)
    t .................. toolbox (toggle)
    g .................. move selected object
    i .................. select face and ...
    a .................. select all
    scroll ............. zoon in/out
    ctrl + i ........... invert selection
    r .................. rotate
    t .................. toggle tools pannel
    x .................. delete selected object
    rx90<Enter> ........ rotate x 90 degres
    h .................. hide object
    H .................. hide all but selected (shift h) local view
    fn / ............... local/global view (numpad /) zoom in selected
    alt + h ............ show object
    alt + r ............ reset rotation
    alt + g ............ reset position
    alt + s ............ reset scalling
    w .................. specials
    hover + backspace .. reset values like position rotation
    hover + Ctrl + backspace ... reset only one value

    Pivot point

    Ctrl + Shift + Alt + c ............ Geometry to origin
    S ................................. snap (Shift + s)

    Navigation (OBS: for Emulated middle mouse it is different)
    -----------------------------------------------------------
    middle click ............ orbit
    shift + middle click .... pan
    ctrl + +  ............... zoon
    scroll .................. zoon

    scroll menu icons

    Ctrl + alt + drag (ctrl-alt-arrastar)

## Bridge faces (conectar duas faces opostas)
Select both faces, right click > bridge faces

## Blender: Subdivide After Boolean | Blender Quick Tips #1
+ https://youtu.be/C7ib_LMvk-c

    Select > Select linked > Linked flat faces

## addons
+ [descrição de vários addons](https://youtu.be/r5GxVeSHe3U?t=44m17s)
+ [destructive extrude](https://gumroad.com/l/cQjUX)
  a video demonstrating its use: https://youtu.be/XhN1FYtuR6o
  + download http://www.mediafire.com/file/lyfqyftu4d0vbip/export_svg_280.py/file

+ [scater objects addon]
+ https://blender-addons.org/object-scatter-addon/

The Object Scatter addon for Blender 2.8 allows you to scatter a mesh on a
surface. It doesn’t have to be a plane you want to scatter on, it can even be
a sphere. The add-on is already build in Blender and it makes the scene in a
stroke and few clicks way more interesting. So why we don’t use it a bit more?

+ [3d print toolbox] https://youtu.be/r5GxVeSHe3U?t=2938
+ [Bolt factory] Add bolts - it cames with blender by default
+ [Archipack](https://youtu.be/qHzNDR31QIw)

Neste vídeo eu demonstro pra vocês as fincionalidades do addon archipack, um
addon excelente que eu uso bastante em meu workflow. Este addon serve para
facilitar o processo de modelagem arquitetônica no Blender. Ele possui uma
versão gratuita que vem instalada no Blender e uma versão paga a qual você
pode achar no link abaixo.

    https://www.bensound.com

+ [Add mesh extra objects] vem por default é só habilitar

+ [offset edges](https://youtu.be/ylSoMm4VDS4)
+ https://gist.github.com/Bombaba/7e5d1343678542d44215346768c7d839

+ [bool tool] enable this addon to cut objects with Ctrl + - (numpad)
  Eu atribuí o atalho Ctrl-y para a opção circle to bool tools (uma mão na roda)

+ [mesh LoopTools] Enable at the addons (it stays at edit menu)
+ https://youtu.be/lhH4NDna7no loop tools

+ Bezier utils: https://github.com/Shriinivas/blenderbezierutils
+ https://youtu.be/enQHGmluQIw
+ how to bend and object: https://youtu.be/oHkzyH9dpv4

+ [Snap Utilities Line](https://blenderartists.org/t/cad-snap-utilities/635301)
    + https://github.com/Mano-Wii/Addon-Snap-Utilities-Line/wiki

### CAD Style Movement and Transformations...IN BLENDER?
+ https://youtu.be/TTKvxrgbFWU
+ https://gumroad.com/l/nqvcs
+ https://youtu.be/vhm_b-YVdK4

    Moviments

    G  (chose from element) V (vertex) E (edge)
    Then move to the target

    an extension that significantly upgrades Blender’s move functions with
    snapping, constraints, and much more!

    Using shift it scales 1, 1.5, 2,
    Using Alt + Shift     1, 1.10, 1.20

## Cutting a mesh in half
+ https://blender.stackexchange.com/a/28415/69021
+ https://youtu.be/gALWWh7lxn8

If you want to create two identical halves from a symmetrical mesh, all you
need to do is to tab into Edit Mode and press Numpad 5 then Numpad 1. Now you
can press B for Box Select and drag over the vertices to one side of the
center line, then delete those vertices.

## Pie menu
There is a special type of menu in blender called "Pie Menu" it appears after
pressing a hot key like "S" and it shows some options:

    Right click ............... cancel pie menu
    Hold the hot key .......... hover the mouse over one option and relase to apply
    Release the hot key ....... got to any option an click on it

    Another option is to hit the hot key and type the number associated with
    the option you want.

    pie menu for pivot point --> .
    pie menu for orientation --> fn ,

## How to extrude a rectangle proportionally?

Take a look at the object scale, if the values are different type
Ctrl-a before extruding, after that use Alt-e and choose:

    extrude faces (along normals)

## Align new object to view

For example, when adding a circle mark "align to view" on the circle menu

## Align two objects
+ https://blender.stackexchange.com/a/2292/69021

You can use Align Objects available in the 3d view header under Object >
Transform > Align Objects.

## Blender: How to Align Vertices, Edges or Faces - Revised
+ https://youtu.be/1c-imsuEVYE

    Move the 3d cursor to the aligment point
    change "transform pivot point" to 3d cursor
    now you can use for example:

    s > x > 0

    This will move all selected vertices to the 3d cursor position

## loop cut demonstration
+ https://youtu.be/-pCf3DjsEBg

## Create a plane with Rounded Edges in Blender 2.80
+ https://youtu.be/Y9XsEIWUDm4
+ https://youtu.be/JBJ5dYjPieI

First subdivide, after that select the vertex you want to Rounded
after that press:  Ctrl + Shift + b

## Display just an object as wire:

Object properties >> Viewport display >> Display as "wire"

## Append file
+ https://youtu.be/ln4yIEQPlO8

In order to import blender files we use append and hit enter on the target
file to open the folder objects (choose wich objects you want).

## preferences

    Ctrl + alt + u

## Enabling transparency

On preferences → system

    Region overlap

## How to divide an object with Blender
+ https://blender.stackexchange.com/questions/131126/

    On edit mode press `Ctrl + r`
    normal click and then right click

    Now press `P` and separate by selection

## Emulating num pad on laptops

Como configurar o blender para usar no laptop

Under "input" tab you can find: "Emulate numpad"
in order to zoon in and zoom out "Emulate 3 button"  LMB + drag
you can also enable "select with" left

    orbit = Alt + left
    pan   = shift + alt + drag
    zoon  = LMB + drag

## Engrave text in object
+ https://youtu.be/-itW6ukT_1M

    Alt + c  → mesh from curve (the second option)
    on gear  → add modifier (boolean)

## Scale vertices by precise percentage

    s 1.2  ............. increases 20%
    sx ................. scale x
    sz ................. scale z

### view
+ https://blender.stackexchange.com/questions/3690/toggle-wireframe-mode-with-hotkey#3691

    z ....................... toggle wireframe mode
    Shirt + z ............... toggle between rendered and solid
    Ctrl + b ................ select render area
    Shirt + c ............... centre 3d cursor
    Shift + right click ..... change 3d cursor position
    Alt + z ................. toggle textured and solid
    Alt + f ................. view to pointer
    Alt + Shift + s ......... to sphere
    0 ....................... toggle camera view (emulated numpad)
    Ctrl + fn + alt + numpad0 camera to view
    1 ....................... front view (Ctrl 1 back view)
    3 ....................... right view (Ctrl 3 left view)
    7 ....................... top view
    2 ....................... rotate view up
    8 ....................... rotate view down
    5 ....................... toggles perspective and orthographic view
    9 ....................... oposite view (180º degress rotate)
    fn 1 .................... front orthographic view (same as numpad)
    fn 3 .................... right orthographic [view](view) (same as numpad)
    fn 7 .................... top orthographic view (same as numpad)
    Ctrl + Alt + q .......... quad view
    n ....................... 3d view properties (painel de propriedades)

    OBS: if you use Ctrl you can view the oposite side 1, Ctrl-1

## how to put an object on the ground?
+ https://blender.stackexchange.com/questions/22888

## videos

+ Blender - Realistic Coins Modelling and Animation Tutorial
  https://youtu.be/-5vnH1_Bpew
+ 51 shortcuts you should know
  https://youtu.be/CWgKrFk5gU4
+ wireframe (aramado) https://youtu.be/kAaToapgBxQ
+ texture https://youtu.be/Oi7744vshEs
+ texture II https://youtu.be/V5Ils5a8yM8
+ [texturas no blender](http://www.lapix.ufsc.br/ensino/computacao-grafica/computacao-graficapaginas-de-suporte-e-tutoriais/computacao-graficaaplicando-texturas-em-blender)

## Draw a coin on blender

    x ...................... delete default object
    A ...................... mesh > circle
    fill type .............. triangle fan (add cilinder)
    vertices ............... 45
    tab .................... start editing mode
    e ...................... extrude (tip: control + right click)
    tab + e ................ extrude menu

Another way to draw a coin on blender could be:

    A ........................ add new mesh
    cilinder ................. basic shape
    s and z .................. scale z axis

## Applying texture
+ https://all3dp.com/2/blender-how-to-add-a-texture/

In my case I am using a cilinder, so i've done:

    x .................... delete oringinal form
    A > cilinder ......... add a new cilinder
    s z .................. define a new dimention
    Ctrl + ............... zoom in
    tab .................. enter edit mode
    a .................... remove all selection
    c .................... cilinder selection
    u .................... unwrap
    add new material
    add new texture
    Ctrl z ............... view rendered

## plugins (addons)
+ [measurement](https://www.blendernation.com/2015/05/29/take-measurements-in-the-blender-viewport-with-measureit/)
+ [pie menu](https://blender.stackexchange.com/questions/21044/) a kind of floating menu

+ [power snapping pies](https://github.com/mx1001/power_snapping_pies)
    This plugins allows you to set the pivot quickly with the Shift-s pie menu

+ [fast carv](https://github.com/jayanam/fast-carve)
  In blender 2.8 it changed  JmeshTool
  +  jayanam/jmesh-tools -> https://github.com/jayanam/jmesh-tools

    Open Blender >= 2.8x
    Go to Edit -> Preferecnes -> Click Install button and navigate to the zip file you downloaded
    JMesh Tools appears in the addon list -> Activate it (Checkbox)
    JMesh panel is now visible in the side bar (N key to open it)

 vim: ft=markdown et sw=4 ts=4 syntax=off
