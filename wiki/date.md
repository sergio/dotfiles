---
file: date.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [date, admin, data, time]
---

## subtract seconds from hour:
+ https://www.unix.com/shell-programming-and-scripting/246390-date-command-subtract-given-time.html

    date -u -d"12:13:00 +0000 -02 sec" '+%H:%M:%S'
    12:12:58

    iterar sobre um conjunto de linhas e diminuir os segundos

    for i in $(awk 'NR>1 {print $NF}' test.txt); do
        date -u -d"$i +0000 -02 sec" "+%H:%M:%S"
    done

### Script to subtract seconds:
The script uses the above solution, making easier to subtract dates on vim

    ~/.dotfiles/bin/less2sec
    :let @a='12:00:00'
    :let @a=system("/home/sergio/.dotfiles/bin/less2sec " . @a)

    :call setreg("a", system("/home/sergio/.dotfiles/bin/less2sec " . @a), "v")

## converter unix timestamp para date
+ https://stackoverflow.com/a/55758856/2571881
+ https://timestamp.online/

``` sh
# seconds since 1970 (timestamp)
date +%s
date +"%s" -d "nov 22, 2020"
date +"%s" -d "2021-08-23 16:40:35"

date -d @1267619929
LC_ALL=en_US; date +'%a, %b %d %Y' -d "@1267619929"
LC_ALL=en_US; date +"%a, %d %b %Y" -d @$(date +"%s" -d "nov 22, 2020")
```

## Data de hoje mais 21 dias:

    TZ='America/Sao_Paulo' date -d "2020-07-29+21 days"

## Para configurar a data faça

    date  mês-dia-hora-minuto[[seculo[ano]]]

O formato deve ser definido assim: MMDDHHMM[[CC[YY]]
O que está entre colchetes é opcional

## syncing hardware clock with system clock

    doas hwclock --set --date="08/21/2021 20:15:00"
    doas hwclock --hctosys

## Calculos com datas
+ http://www.dotsharp.com.br/linux/como-fazer-calculos-e-formatacao-de-data-no-linux.html
+ https://www.cyberciti.biz/faq/how-to-add-days-to-date-and-get-new-date-on-linux/

``` sh
date --date='1 year ago'
Thu Apr 29 21:24:37 BRT 2010

date --date='14 day ago' +%d/%m/%Y
```

I brought eggs from the market, and it says best before 20 days from the date
of packing. Say my egg box was packed on 29/July/2020. How do I add days to
date using Linux CLI? I want to find out date 29/July/2020 + 20days using the
CLI, and how do I do it?

    Let us see the date of the day four months and two day hence:
    date --date='4 months 2 day'

    TZ='America/Fortaleza' date -d "2020-08-18+20 days"
    date -d "2020-08-18+20 days"

So the solution will be something like:

    days="20 days"
    echo "Eggs expiry date $(date -d "${egg}+${days}")"
    Eggs expiry date seg set  7 15:08:02 -03 2020

## Changing the date language

    mes=$(LC_ALL=en_US; date +%b | lower)
    mes=$(LC_ALL=en_US; date +%b | tr '[:upper:]' '[:lower:]')

OBS: lower is a python script I have to convert sctrings to lowercase
+ https://bitbucket.org/sergio/dotfaster/src/master/bin/lower

## um dia antes

    date -d "2016-05-01 - 1 day"

    date -d "2016-05-01 - 1 day" +%d-%m-%Y

## data futura

    date --date='1 month'
    Sun May 29 21:26:04 BRT 2011

## qual o número da semana atual?

    date +"%V"

## quantos dias faltam para o fim do ano?
+ http://www.commandlinefu.com/commands/view/4804/how-many-days-until-the-end-of-the-year?
How many days until the end of the year

    echo "There are $(($(date +%j -d "Dec 31, $(date +%Y)")-$(date +%j))) left in year $(date +%Y)."

## ano bissexto

    leapyear() { [ $(date -d "Dec 31, $1" +%j) == 366 ] && echo leap || echo not leap; }

## adiantar o relógio do sistema em 30 minutos

    date --set='+30 minutes'

## sincronizando a data de um servidor com outro

		sudo date -s "$(ssh user@server.com "date -u")"

## Verificar a data de modificação de um arquivo
+ https://linoxide.com/get-last-modified-date-of-file-in-linux/

    date -r foo "+%a, %b %d %Y - %H:%M:%S"
    data=$(date -r foo "+%a, %b %d %Y - %H:%M:%S")

    awk '{$5=$6="";print $1,$3,$2,$7,$4}' <(date -r stat.md)

## Para exibir a data no padrão americano

    date -I

## Configurando a data com base em uma string

    date -s "01/31/2009 22:19:53"
    date -s "31 JAN 2009 22:19:53"

## configurando apenas o horário

    date +%T -s "22:19:53"

## data de amanhã

    date -d 'tomorrow' +%d-%m-%Y
    date --date='2 year ago'  # past
    date --date='3 years'     # go into future
    date --date='2 days'      # future
    date --date="next day"    # future
    date --date="1 days ago"  # past
    date --date='1 month ago' # past
    date --date='2 months'    # future
    date -d yesterday +%d-%m-%Y
    date --date='6 months 15 day'
    date --date='2 months 5 day ago'

## 8 months earlier

    date -d "-8 month -$(date +%d) days" +%B

## que dia (por extenso) será depois de amanhã?

    date -d '2 days' +%A

ou que dia foi há dois dias atráz

``` sh
date -d '-2 days' +%A

# Outro exemplo
LC_ALL=en_US; date -d '04/23/2021' "+%a, %d %b %Y"
```

## como saber que dia da semana foi 01 de outubro de 2012?

	date -d "10/1/2012" +%A

	gerando a lista de dias da semana de outubro de 2012

	for ((i=1;i<=31;i++)){
		diasemana=`echo date -d "10/${i}/2012" +%A | sh`
		numdiasemana=`echo date -d "10/${i}/2012" +%d | sh`

		echo "$numdiasemana - $diasemana"
	}

## que dia vai ser domingo

    date -d 'this sunday' +%d-%m-%Y

## mês passado

    date -d "last month" +%B

## em formato numérico

    date -d "last month" +%m

## data de ontem

    date --date="yesterday"

    exemplo:
    Para configurar a data em 24/03/2008 13:48h

    date 032413482008
    Seg Mar 24 13:48:00 BRT 2008

    First use Month
    after that use Month's day
    after that hour and minute
    and finally use Year

Você pode omitir século e ano

   date 03241348

Para mais informações acesse pelo terminal:

   man date

## O comando date pode também retornar strings

    FBKPNEW=$(date +"backup-%Y-%m-%d")

## testar se é fim de mês

    [ `date --date='next day' +'%B'` == `date +'%B'` ] || echo 'end of month'

Como no linux temos muitos comandos nem sempre o comando que você
necessita, ou que seria melhor para aquela situação, é o que
lhe vem primeiro à mente, veja um exemplo da lista shell script:

``` markdown
> Montei um script hoje para manter armazenado apenas os arquivos que possuem
> a data de hoje ou de ontem. Todos os demais arquivos daquele diretório serão
> apagados. O script está 100% funcional porém acredito que consigo deixá-lo
> mais conciso e abordar de outra maneira como eu busco o dia de ontem.
>
> Lembrando que eu não tenho a opção -d ou --date no comando date, segue o
> script.
>
> #! /usr/bin/bash
> SPATH="xxx"
> DAY=`date +%d`
> LDAY=`perl -e 'print localtime(time() - 86400) . "\n" ' | cut -c 9-10`
> for FILES in `ls $SPATH/Logs* | egrep -v $(date +%Y%m)"($DAY|$LDAY)"`; do rm
> $SPATH/$FILES;done
>
> Alguma sugestão?
> --
> Obrigado,
> Christian Silva
>
> [As partes desta mensagem que não continham texto foram removidas]
>

 find /diretorio_ a_excluir -type f -mtime +2 | xargs rm

```
