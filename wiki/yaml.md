---
file: yaml.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: explain something about YAML
---

#  yaml.md intro:
YAML is a human-readable data-serialization language. It is commonly used for
configuration files and in applications where data is being stored or
transmitted

## current date (on the yaml part)

    date: "`r format(Sys.time(), '%d %B, %Y')`"

## Getting settings from a yaml file

``` python
Get all the critical variables from .yml file:

import yaml
conf = yaml.load(open('conf/application.yml'))
email = conf['user']['email']
pwd = conf['user']['password']

server = smtplib.SMTP( "smtp.gmail.com", 587 ) # add these 2 to .yml as well
server.starttls()
server.login(email, pwd)

The application.yml will look similar to this:

user:
    email: example@mail.com
    password: yourpassword
```

