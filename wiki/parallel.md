---
file: parallel.md
Last Change: Tue, 20 Feb 2024 - 15:18:59
author: Sergio Araujo
 vim:ft=markdown
---

site: http://vivaotux.blogspot.com
licence: GPL (see http://www.gnu.org/licenses/gpl.txt)
e-mail: <voyeg3r ✉ gmail.com>
```
# parallel.md

+ free e-book: https://zenodo.org/record/1146014
+ https://www.gnu.org/software/parallel/

NOTE: Academic tradition requires you to cite works you base your article on.
If you use programs that use GNU Parallel to process data for an article in a
scientific publication, please cite:

    O. Tange (2018): GNU Parallel 2018, Mar 2018, ISBN 9781387509881,
    DOI https://doi.org/10.5281/zenodo.1146014

    This helps funding further development; AND IT WON'T COST YOU A CENT.
    If you pay 10000 EUR you should feel free to use GNU Parallel without
    citing.

    More about funding GNU Parallel and the citation notice:
    https://www.gnu.org/software/parallel/parallel_design.html#Citation-notice

    To silence this citation notice: run 'parallel --citation' once.

## References:
+ https://www.msi.umn.edu/support/faq/how-can-i-use-gnu-parallel-run-lot-commands-parallel

build and execute shell command lines from standard input in parallel

Parallel is a shell parallelization tool. Parallel is a relatively new addition
for GNU, and has many of xargs functionality built in. But the main feature is
getting a job/task/program we want to run in parallel,

If you use xargs and tee today you will find GNU parallel very easy to use as
GNU parallel is written to have the same options as xargs. If you write loops
in shell, you will find GNU parallel may be able to replace most of the loops
and make them run faster by running several jobs in parallel.

## Copiando o mesmo arquivo para três pastas diferentes
+ https://www.linuxfordevices.com/tutorials/linux/copy-file-to-multiple-directories-with-one-command

For example, if you wanted to copy the file /tmp/file.txt to the directories /home/user1 and /home/user2, you would use the following command:

    parallel cp /tmp/file.txt ::: /home/user1 /home/user2

## To see many parallel examples open a terminal and type

    LESS=+/EXAMPLE: man parallel

You can use 'n' as you do in vim to go to the next example

## getting a list of files with a pattern and use on vim

    parallel "awk '/Last Change/ && NR > 7 {print FILENAME,NR}' {}" ::: *.md
    vim $(parallel "awk '/Last Change/ && NR > 7 {print FILENAME}' {}" ::: *.md)

## Generate thumbnails fast:

    mkdir thumbs
    fd -d1 -tf | parallel mogrify -format jpg -path thumbs -thumbnail 200x200 {}

## Remove a lot of files at once

    parallel -j20 rm -rf {} ::: ls *

## copy or files
+ https://stackoverflow.com/questions/57289018/

    find -iname "*.mp3" | parallel 'cp {} ~/tmp/{/.}_`stat -c%i {}`.mp3'

    find ./images -name *150x150.jpg | \
    parallel 'mv {} ./another-directory/{/.}_`stat -c%i {}`'

OBS: In order to test the command one can use --dry-run option of parallel

## parallel tutorial

    man parallel_tutorial

    seq 4500 | parallel -j20 wget -c https://speechling.com/static/audio/male/english/english_{}.mp3
    seq 4500 | parallel -j6 curl -O https://speechling.com/static/audio/female/english/english_{}.mp3

    parallel -j20 wget -c https://speechling.com/static/audio/male/english/english_{}.mp3 ::: {1..4500}

    parallel -j20 wget -c https://basicenglishspeaking.com/wp-content/uploads/audio/{}.mp3 ::: {001..100}

NOTE: Sometimes sites do not allow downloading using other stuff than
browsers, so, alias curl or wget as if it were firefox:

    curl -A "Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.36 (KHTML, like
    Gecko) Chrome/47.0.2526.73 Safari/537.36 OPR/61.0.2036.

## Parallel can read the input of the command line instead of stdin

Reading arguments from command line

    GNU parallel can take the arguments from command line instead of stdin
    (standard input). To compress all html files in the current dir using
    gzip run:

    parallel gzip --best ::: *.html

## Convert wav to mp3 (also how to remove extension)

    parallel lame {} -o {.}.mp3 ::: *.wav
    parallel ffmpeg -i {} {.}.flac ::: *.wav

    parallel ffmpeg -v 16 -i {} -ab 320k {.}.mp3 ::: *.wav

    find . -type f -iname "*.m4a" | parallel -j8 ffmpeg -i {} -acodec mp3 -ac 2 -ab 192k {.}.mp3

NOTE: the dot '.' inside curly braces removes the file extension

Here another example where a "file.tar.bz" can be shown without its ".tar.bz"

    echo "file.tar.bz" | parallel --plus echo {..}

If you have directory with tar.gz files and want these extracted in the
corresponding dir (e.g foo.tar.gz will be extracted in the dir foo) you can
do:

    parallel --plus 'mkdir {..}; tar -C {..} -xf {}' ::: *.tar.gz

## Splitting command line args with GNU parallel
+ https://stackoverflow.com/questions/6255286

I have a program that takes two arguments, e.g.

    $ ./prog file1 file2
    $ ./prog file2 file3
    ...
    $ ./prog file23456 file23457

I'm using a script that generates the file name pairs, however this poses a
problem because the result of the script is a single string - not a pair.
like:

    $ ./prog "file1 file2"

GNU parallel seems to have a slew of tricks up its sleeves, I wonder if
there's one for splitting text around separators:

### Solution

    generate_file_pairs | parallel --colsep ' ' ./prog {1} {2}

    parallel --colsep '-' echo {4} {3} {2} {1} ::: A-B-C-D

 --colsep regexp
    -C regexp

Column separator. The input will be treated as a table with regexp separating
the columns. The n'th column can be accessed using {n} or {n.}. E.g. {3} is
the 3rd column.

If there are more input sources, each input source will be separated, but the
columns from each input source will be linked

## Dealing with urls
I have had some problems with downloading a bunch of files, one url per line,
the problem is that the output file had slashes "/", to fix this just use

    {/.}

    One example of how o solve this problem:

    echo "http://url"{01..10}.com | tr ' ' '\n' | parallel echo {} {/.}.txt

    # one single output file
    parallel w3m -dump {.} -O -UTF-8 > {/.}.txt :::: urls

    # one file for each html
    parallel 'w3m -dump "{}" -O -UTF-8 "{.}.txt"' ::: *.html
    parallel 'w3m -dump "{}" > "{.}.txt"' ::: *.html
    parallel w3m -dump {} -O -UTF-8 '>' {.}.txt ::: *.html

## Run a script in parallel
+ https://bit.ly/2O00BkO

Let's say you have
``` bash
# convert.sh
ffmpeg -i audio1.wav audio1.flac
ffmpeg -i audio2.wav audio2.flac
ffmpeg -i audio3.wav audio3.flac
ffmpeg -i audio4.wav audio4.flac
ffmpeg -i audio5.wav audio5.flac
type here
```

To run the above script in parallel just type:

    parallel -a ./convert.sh

## Download friends transcripts
+ https://dev.to/voyeg3r/runing-linux-commands-in-parallel-4ff8

    url='https://www.springfieldspringfield.co.uk/view_episode_scripts.php?tv-show=friends&episode='

    parallel lynx --dump $url{} '>' friends-transcript-{}.txt ::: s{01..10}e{01..24}

    parallel --verbose -j20 lynx --dump $url{} '>' friends-transcript-{}.txt ::: s{01..10}e{01..24}

    parallel --verbose -j20 lynx --dump $url{} '>' friends-transcript-{}.txt ::: s{01..09}e{01..24} s10e{01..18}

After downloading the files we can remove some lines using vim this way:

    :silent [argdo](argdo) normal! gg18dd
    :silent argdo normal! G{kdG
    :wall

Using sed

    sed -i '1,18d' *.txt
    sed -i '/References/,$d' *.txt

## Using a file as input:
Let's say you have a file with the urls you want to use, and the given url's have equal sign as a separator, something like this:

    https://your-domain/something-else=more-data=s01e01

And you want to get the final part (s01e01), in this case the parallel option:

    -C --colsep regexp

To indicate the file wich has on each line a given url we use the option:

    --arg-file -a file

    parallel -a transcripts.txt -C '=' --verbose -j20 lynx --dump {1}={2}={3} '>' 'two-and-a-half-men-{3}.txt'

NOTE: In my case the site was: https://www.springfieldspringfield.co.uk/episode_scripts.php?tv-show=two-and-a-half-men for the two and a half men transcripts

## Convert images jpg to png and vice-versa
+ https://www.tecmint.com/linux-image-conversion-tools/

```bash
# ---------- Convert PNG to JPG -----------
parallel convert '{}' '{.}.jpg' ::: *.png

# ---------- Convert JPG to PNG -----------
parallel convert '{}' '{.}.png' ::: *.jpg

Where,

    {} – input line which is a replacement string substituted by a complete line read from the input source.
    {.} – input line minus extension.
    ::: – specifies input source, that is the command line for the example above where *png or *jpg is the argument.
```

## Convert images jpg to png and vice-versa
+ https://www.tecmint.com/linux-image-conversion-tools/

```bash
## ---------- Convert PNG to JPG -----------
parallel convert '{}' '{.}.jpg' ::: *.png

## ---------- Convert JPG to PNG -----------
parallel convert '{}' '{.}.png' ::: *.jpg

Where,

    {} â€“ input line which is a replacement string substituted by a complete line read from the input source.
    {.} â€“ input line minus extension.
    ::: â€“ specifies input source, that is the command line for the example above where *png or *jpg is the argument.

```

## renaming to lowercase

    ls | parallel 'mv {} "$(echo {} | tr "[:upper:]" "[:lower:]")"'

In my particular case I have a python script called `lower` :

```python
import sys

argument = sys.argv[1:] if len(sys.argv) > 1 else sys.stdin
print(' '.join(argument).lower())
```

So I can do things like:

    ls | parallel 'mv {} "$(echo {} | lower)"'

## Find the files in a list that do not exist

    cat file_list | parallel 'if [ ! -e {} ] ; then echo {}; fi'

## Removing file extension when processing files

When processing files removing the file extension using {.} is often useful.
Create a directory for each zip-file and unzip it in that dir:

    parallel 'mkdir {.}; cd {.}; unzip ../{}' ::: *.zip

