---
File: ~/.dotfiles/wiki/luasnip.md
Last Change: Mon, 19 Feb 2024 - 20:41:47
tags: [plugin, neovim, regex, addon]
---

# snippets engine written in pure lua

+ [Ultisnips to luasnip](https://cj.rs/blog/ultisnips-to-luasnip/)
+ [Luasnip repo](https://github.com/L3MON4D3/LuaSnip)
+ [Config example](https://raw.githubusercontent.com/sbulav/dotfiles/master/nvim/lua/config/snippets.lua)
+ [Vim snippets](https://github.com/honza/vim-snippets.git)
+ [luasnip guides](https://evesdropper.dev/files/luasnip/)
+ [tjdevris luasnip guide](https://www.youtube.com/watch?v=KtQZRAkgLqo)
+ [article about luasnips](https://www.ejmastnak.com/tutorials/vim-latex/luasnip/)
+ [topic on reddit](https://www.reddit.com/r/neovim/comments/strtsg/share_your_most_advanced_luasnippets/)

## regex support:

```sh
sudo xbps-install lua-devel
sudo luarocks install jsregexp
```

All code snippets in this help assume the following:

``` lua
local ls = require("luasnip")
local s = ls.snippet
local sn = ls.snippet_node
local isn = ls.indent_snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local r = ls.restore_node
local events = require("luasnip.util.events")
local ai = require("luasnip.nodes.absolute_indexer")
local extras = require("luasnip.extras")
local l = extras.lambda
local rep = extras.rep
local p = extras.partial
local m = extras.match
local n = extras.nonempty
local dl = extras.dynamic_lambda
local fmt = require("luasnip.extras.fmt").fmt
local fmta = require("luasnip.extras.fmt").fmta
local conds = require("luasnip.extras.expand_conditions")
local postfix = require("luasnip.extras.postfix").postfix
local types = require("luasnip.util.types")
local parse = require("luasnip.util.parser").parse_snippet
local ms = ls.multi_snippet
local k = require("luasnip.nodes.key_indexer").new_key
```

By default, the names from luasnip.config.snip_env will be used, but it's possible to customize them by setting snip_env in setup.

``` lua
s("trigger", {
    t({"After expanding, the cursor is here ->"}), i(1),
    t({"", "After jumping forward once, cursor is here ->"}), i(2),
    t({"", "After jumping once more, the snippet is exited there ->"}), i(0),
})
```

## local require snippet

``` lua
s("lreq", fmt("local {} = require('{})", { i(1, "module_name"), rep(1)} ) ),
```

## Defining variables to use in isnippets

+ <https://sbulav.github.io/vim/neovim-setting-up-luasnip/>

``` lua
local date = function() return {os.date('%Y-%m-%d')} end

-- full date --> Wed, 09 Feb 2022 11:10
s('fdate', f(bash, {}, 'date +"%a, %d %b %Y %H:%M"')),

s({ -- see local date defiition
    trig = "date",
    namr = "Date",
    dscr = "Date in the form of Wed, 09 Feb 2022 11:37",
}, {
    f(date, {}),
}),
```

## Test if luasnip is loaded

```lua
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

if packer_plugins['LuaSnip'] and packer_plugins['LuaSnip'].loaded then
    print('LuaSnip is loaded')
end
```

## function to expand snippet

``` lua
local function snip_expand(snippet, opts)
```
