---
File: ~/.dotfiles/wiki/st.md
Author: Sergio Araujo
Last Change: Fri, 17 May 2024 - 07:11:58
Abstrac: Explain how to compile and use simple-terminal
tags: [termial, tools, cli]
---

## st manpage:

+ https://manpages.org/st

##  simple terminal intro:

+ [How to build and install st](https://brianbuccola.com/how-to-build-and-install-st-suckless-simple-terminal-from-source-on-arch-linux/)
+ https://unix.stackexchange.com/questions/479685/
+ [how to build and install](https://brianbuccola.com/how-to-build-and-install-st-uckless-simple-terminal-from-source-on-arch-linux/)
+ https://bandithijo.com/blog/st-simple-terminal-dari-suckless
+ [another good one](https://github.com/rudyghill/st)
+ [Another build recipe](https://brianbuccola.com/how-to-build-and-install-st-suckless-simple-terminal-from-source-on-arch-linux/)
+ https://martijnvos.dev/setting-up-the-st-terminal/
+ https://github.com/juliusHuelsmann/st (I using this patched version now)
+ https://github.com/sugud0r/st-config
+ https://gitlab.com/feloniousbonk/st-bonk.git (current one)

    git clone https://github.com/juliusHuelsmann/st
    cd st

## rudyghill keybindings

Clipboard

    Alt+Shift+c to copy
    Alt+Shift+v to paste
    Shift+Insert to paste

Scrollback

    Alt-k and Alt-j scroll back/forward in history one line at a time
    Alt-u and Alt-d scroll back/forward in history a page at a time
    Shift+PageUp/PageDown
    Shift+Mouse wheel

Increase/Decrease font

    Ctrl++/- to increase/decrease font size
    Ctrl+0 to reset font size
    Alt+Shift+PageUp/PageDown to increase/decrease font size
    Alt+Shift+Home to reset font size

Pipe URLs to dmenu

    Alt+l for link export

## key tips

The MODKEY is typically set to the Super key (also known as the Windows key) or
the Alt key. The TERMMOD key is typically set to the Shift key or the Control
key.

## Fix Keyboard Input on Terminals - Please

+ https://www.leonerd.org.uk/hacks/fixterms/

## oficial st repo

    git clone git://git.suckless.org/st

I am using this repo to compile my build:

    https://github.com/LukeSmithxyz/st

    git clone https://github.com/LukeSmithxyz/st.git

You can download the README.md file in order to get the keybindings

    wget -c https://raw.githubusercontent.com/LukeSmithxyz/st/master/README.md

LukeSmith rice features:

Unique features (using dmenu)

    follow urls by pressing alt-l
    copy urls in the same way with alt-y
    copy the output of commands with alt-o

Bindings for

    scrollback with alt-↑/↓ or alt-pageup/down or shift while scrolling the mouse.
    OR vim-bindings: scroll up/down in history with alt-k and alt-j. Faster with alt-u/alt-d.
    zoom/change font size: same bindings as above, but holding down shift as well. alt-home returns to default
    copy text with alt-c, paste is alt-v or shift-insert

This other one has a scrollback patch

    https://github.com/ceemos/st-scrollback

I normally set the font a little bigger, changing the config.h file

    vim config.h

    function ChangeFont()
        normal ma
        let @y='Iosevka Mayukai Original:size=15:antialias=true:autohint=true'
        %s/^static char \*font = "\zs.*\ze"/\=@y
        g/float alpha/s/= \zs.*\ze;/0.82
        normal `a
    endfunction
    " copy with yip
    " :@0 | call ChangeFont()

    :@+

Then

    make && sudo make install

Via command line we can use `Alt + Shift + K` to increase font
and `Alt + Shift + j` to decrease font size

## distrotube st:
+ https://gitlab.com/dwt1/st-distrotube

    git clone https://gitlab.com/dwt1/st-distrotube.git
    cd st-distrotube
    sudo make clean install

    shortcuts:

    Alt  + Shift + k ................... zoom int
    Alt  + Shift + j ................... zoom out
    Alt  + Shift + v ................... paste clipboard
    Alt  + Shift + c ................... copy into clipboard
    Alt  + Shift + y ................... selpaste
    Shift + pageup ..................... scrollup
    Shift + pagedown ................... scrolldown

A cofiguração para tanto vem dessas linhas:

```c
{ MODKEY|ShiftMask,     XK_C,           clipcopy,       {.i =  0} },
{ MODKEY|ShiftMask,     XK_V,           clippaste,      {.i =  0} },
{ MODKEY|ShiftMask,     XK_K,           zoom,           {.f = +1} },
{ MODKEY|ShiftMask,     XK_J,           zoom,           {.f = -1} },
```

## another rice:

    https://github.com/yac-d/st

    patches:

    alpha
    blinking cursor
    xresources
    scrollback
    defaultfontsize -> https://st.suckless.org/patches/defaultfontsize/

    zoom ->  Ctrl +shifit + page up
    paste -> Ctrl +shifit + v

## on debian buster
dependencies

    libX11-devel: Xlib header fils
    libXft-devel: freetype-based font drawing
    libXext-devel: X11 miscellaneous extensions

    doas xbps-install -S cmake base-devel {libX11,libXft,libXext}-devel pkg-config

## Undestangind the key mappings on the file config.h:
These notes apply to this repo: https://github.com/juliusHuelsmann/st

    MODKEY  ->  Alt
    TERMODE ->  look at "#define TERMMODE"
    TERMODE ->  my current st uses Ctrl + Shift

    Alt + j   ->  zoom out
    Alt + k   ->  zoom in
    Alt + Shift + v  -> clipaste
    Alt + Shift + c  -> clipcopy
    Ctrl + Shift + y  -> selpaste

## installing a patch
+ https://st.suckless.org/patches/blinking_cursor/
+ https://st.suckless.org/patches/alpha_focus_highlight/
+ https://st.suckless.org/patches/scrollback/st-scrollback-20210507-4536f46.diff
+ https://st.suckless.org/patches/scrollback/st-scrollback-mouse-20220127-2c5edf2.diff
+ https://st.suckless.org/patches/xresources/st-xresources-20200604-9ba7ecf.diff

    git clone https://git.suckless.org/st
    cd st

    make clean
    patch < patches/transparency.diff
    it will ask for target file: config.h
    make && sudo make install

I patched the blink cursor: https://st.suckless.org/patches/blinking_cursor/
It asks which file, I indicated "config.h"

## Changing the font

Edit the following line as you prefer (on the config.h):

```c
/*static char *font = "Liberation Mono:pixelsize=12:antialias=false:autohint=false";*/
static char *font = "Consolas:style=Regular:size=15:antialias=true:autohint=true";
static char *font = "JetBrains Mono Variable:style=Variable,Regular:size=14:antialias=true:autohint=true";
static char *font = "Iosevka Mayukai Original:size=15:antialias=true:autohint=true";
static char *font = "iosevka-regular:size=15:antialias=true:autohint=true";
static char *font = "IBMPlexMono-Regular:size=13:antialias=true:autohint=true";
static char *font = "Fantasque Sans Mono:style=Regular:size=15:antialias=true:autohint=true";
static char *font = "FuraMono-Regular:size=16:antialias=true:autohint=true";
static char *font = "Fira Mono for Powerline:size=14:antialias=true:autohint=true";
```

You can also pass the value of the font in the command line:

    st -f "Liberation Mono:size=12"
    st -f 'Liberation Mono-12'
    st -f 'iosevka-regular:size=15'
    st -g 90x20 -f 'iosevka-regular:size=15:antialias=true:autohint=true'
    st -g 90x20 -f 'Iosevka Mayukai Original:size=15:antialias=true:autohint=true'
    st -f "Consolas:pixelsize=19"
    st -f 'Input Mono Compressed:size=13'
    st -f 'Hack-Regular:size=15'
    st -f 'IBM Plex Mono Light:size=14'
    st -f "JetBrains Mono Variable:style=Variable,Regular:size=14:alpha=0.80"

Font names can be found with `fc-list` combined with grep, like this:

    fc-list | grep -i hack

## Setting alpha (transparency)
Instead of woring about patching st just set [picom](picom.md) (compositor)

On picom.conf set:

```sh
opacity-rule = [
    # "80:class_g = 'Polybar'",
    "92:class_g = 'URxvt' && focused",
    "75:class_g = 'URxvt' && !focused",
    "92:class_g = 'st' && focused",
    "75:class_g = 'st' && !focused",
    "92:class_g = 'St' && focused",
    "75:class_g = 'St' && !focused",
    "92:class_g = 'mpv' && focused",
    "75:class_g = 'mpv' && !focused",
    "92:class_g = 'st-256color' && focused",
    "75:class_g = 'st-256color' && !focused",
    "92:class_g = 'Nemo' && focused",
    "75:class_g = 'Nemo' && !focused"
];
```

## Installation
Edit config.mk to match your local setup (st is installed into
the /usr/local namespace by default).

Afterwards enter the following command to build and install st (if
necessary as root):

    make clean install

## Installing Iosevka font
+ http://tinyurl.com/y6r25car

## Fixing picom issue:
+ https://wiki.archlinux.org/title/Xcompmgr#Window_transparency

Install xcompmgr

Xcompmgr is a simple composite manager capable of rendering drop shadows and, with the use of the transset utility, primitive window transparency. Designed solely as a proof-of-concept, Xcompmgr is a lightweight alternative to Compiz and similar composite managers.

Because it does not replace any existing window manager, it is an ideal solution for users of lightweight window managers, seeking a more elegant desktop.

## Creatig a system shortcut
+ https://www.reddit.com/r/bspwm/comments/f52u5q/comment/fhxb6zy

``` sh
cat <<EOF > ~/.local/share/applications/st.desktop
[Desktop Entry]
Name=Neovim
GenericName=Text Editor
GenericName[pt]=Text Editor
Comment=Edit text files
TryExec=nvim
Exec=st -n nvim nvim %F
Terminal=false
Type=Application
Encoding=UTF-8
Icon=nvim
Categories=Utility;TextEditor;
StartupNotify=false
Keywords=Text;editor;
MimeType=text/english;text/plain;text/x-makefile;text/x-c++hdr;text/x-c++src;text/x-chdr;text/x-csrc;text/x-java;text/x-moc;text/x-pascal;text/x-tcl;text/x-tex;application/x-shellscript;text/x-c;text/x-c++;
EOF
```

## Defining geometry

    When creating a gnome-shell shortcut <Ctrl><Alt>shortcuts

    st -g=90x20 -f "iosevka-regular:size=14:antialias=true:autohint=true"'

## keybidings shortcuts

    Ctrl + Shift + C ............... copy
    Ctrl + Shift + V ............... paste
    Ctrl + Shift + PageUp .......... zoom in
    Ctrl + Shift + PageDown ........ zoom out

    Ctrl + Shift + Home ............ default zoom
    Alt + Shift + Scroll ........... zoom in / zoom out

    No arquivo de configuação:

    XK_c = c
    MODKEY = Alt
    ShiftMask = Shift
