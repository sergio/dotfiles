---
File: ~/.dotfiles/wiki/lazygit.md
Author: Sergio Araujo
Tags: [tools, git, programming]
---

# [Article about lazygit](https://medium.com/@cahakgeorge/git-the-lazy-way-simple-script-to-add-commit-and-push-files-fb87b1a226d4)

## tool to make easy use git

``` sh
q        .......................  quit
p        .......................  pull
shift+P: .......................  push

space:    toggle staged
c:        commit changes
shift+C: commit using git editor
shift+S: stash files
t:        add patched (i.e. pick chunks of a file to add)
o:        open
e:        edit
s:        open in sublime (requires 'subl' command)
v:        open in vscode (requires 'code' command)
i:        add to .gitignore
d:        delete if untracked checkout if tracked (aka go away)
shift+R: refresh files
```

## custom commands

+ [Read here about custom commands](https://github.com/jesseduffield/lazygit/blob/master/docs/Custom_Command_Keybindings.md)

You can add custom command keybindings in your config.yml (accessible by pressing 'o' on the status panel from within lazygit) like so:

I want to do:

    git commit -am "$1"
    git pull --quiet
    git push
