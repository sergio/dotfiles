---
File: ~/.dotfiles/wiki/motocicleta.md
Last Change: Mon, 28 Oct 2024 - 17:55:18
tags: [moto]
---

# Dicas para motos

[Bloqueador por presença serruns](https://bit.ly/44LtaZR)

## Lubrificar e remover ferrugem

Pulverol

## Troca do fluido de freio

[Youtube](https://youtu.be/YaVZp9pL7s5)

## moto desligando ao acelerar

[Youtube](https://youtu.be/iz0jKvQndLE?si=iGReo1O8g3PIA_z1)


## Relação de marchas

Pinhão maior – moto perde torque, perde na arrancada, porém ganha em alta na ultima marcha, se for descida. Para cada dente a mais no pinhão ganha em média 5 a 10 km a mais em velocidade final.
Pinhão maior é indicado para quem anda sem garupa e pega muita rodovia, o motor fica mais solto com o giro mais baixo. Para cada dente maior do pinhão reduz em média 500 giros na rotação por marcha e por consequência deixa a moto um pouco mais econômica.

Pinhão menor – moto ganha mais torque, ganha mais arrancada, porém as marchas ficam mais curtas. Para cada dente a menos no pinhão a moto também perde em média de 5 a 10 km na velocidade final.  Pinhão menor é indicado para quem anda muito com garupa, quase não pega estrada ou que trabalha por exemplo com entrega de gás e o uso dela faz a moto beber muito porque o motor trabalha o tempo todo em alta rotação podendo assim também ocorrer desgastes pre maturo do motor e aquecimento.
