---
file: amixer.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# amixer is a command-line mixer for ALSA soundcard driver.

Increase volume by 10%

    amixer set 'Master' 10%+

## Decrease volume by 10%

    amixer set 'Master' 10%-

## Set volume to 10%

    amixer set 'Master' 10%

## Set volume to 80%

    amixer set 'Master' 80%

## Swich microphone on/off:
+ https://nikhilwanpal.in/blog/mute-mic-with-keyboard-shortcut-on-ubuntu-or-linux-mint/

    amixer set Capture toggle | gawk 'match($0, /Front Left.*\[(.*)\]/, a)
    {print a[1]}' | xargs notify-send --hint=int:transient:1 -i "mic-on"
    "Mic switched: $1"

## Shows a complete list of simple mixer controls

    amixer scontrols

amixer is a command-line mixer for ALSA soundcard driver.

Set volume to 80%

    amixer set 'Master' 80%

Shows a complete list of simple mixer controls

    amixer scontrols
