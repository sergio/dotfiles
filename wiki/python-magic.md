---
File: python-magic.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
Date: abr 30, 2020 - 16:20
tags: [python, libraries]
---

#  python-magic.md intro:

Show mimetypes

## how to install it?

    pip3 install --user python-magic

## how to use install?

``` python

import magic
from pathlib import Path

target = Path.home() / 'docs' / 'e-books' / 'python'

def file_mime_type(filename):
    return magic.from_file(str(f), mime=True)

for f in target.iterdir():
    if f.is_file():
            print(f"{f.name} {file_mime_type(f)}")

```
