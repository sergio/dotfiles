---
file: dig.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# dig - fornece informações de DNS

## External ip (wanip)

    alias wanip='dig @resolver1.opendns.com ANY myip.opendns.com +short'
