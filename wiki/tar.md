---
file: tar.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:41:52
 vim:ft=markdown
---

# tar.md

compactador e descompactador do linux

Also have a look at pigz (parallel implementation of gzip)

## Descompactar durante um download

    wget http://example.com/archive.tar.bz2 -O - | tar -xj

    curl http://example.com/archive.tar.bz2 | tar -xj

    sudo wget -c http://www.site.com -O - | sudo tar -xz -C /place/you/want/

## compactar:

    tar -cvjSf pacote.tar.bz2 folder

    -j .............. bz2

## Descompactar removendo a estrutura de pastas

  `--strip-components=number'

    Strip given number of leading components from file names before extraction.

For example, suppose you have archived whole `/usr' hierarchy to a tar archive named `usr.tar'. Among other files, this archive contains `usr/include/stdlib.h', which you wish to extract to the current working directory. To do so, you type:

    $ tar -xf usr.tar --strip=2 usr/include/stdlib.h

The option `--strip=2' instructs tar to strip the two leading components (`usr/' and `include/') off the file name.

## Descompactando tar.bz2

    tar jxvf pacote.tar.bz2

Tem uma variação legal que é inciar o local onde você quer descompactar (você
tem que ter permissões de escrita no diretório de destino)

    tar jxvf pacote.tar.bz2 -C $HOME/tmp

## Deixando o tar descobrir como descompactar

Em geral usamos "tar zxvf" para tar.bz
e "tar jxvf" para tar.bz2, mas se usarmos a opção "tar axvf"
o tar irá detectar pela extensão qual programa usar:

    tar axvf somefile.tar.bz?

## Descompactar xz

    unxz archive.xz
    tar --xz -xf archive.txz

## apagando arquivos descompactados indevidamente numa pasta
Fonte: dicas-l http://va.mu/A6Erx

    rm -rf $(tar ztf arquivo.tar.gz)

## descompactando e descartando a pasta pai

    tar jxvf pacote.tar.bz2 --strip 1

veja também: http://ur1.ca/0253w from stack overflow
## criando um pacote tar.bz2

    tar -cjvf nome.tar.bz2 ./pasta
    tar -cvfj target-name.tar.bz2 folder

## listando o conteúdo de um pacote tar
+ https://www.commandlinefu.com/commands/view/2573:wq

    tar -tf conf-sys.21-08-2009-113639.tar.bz2  | awk '{print $6}'

Once we can list the content of a "tar" package we can also delete the
uncompressed files:

Remove all files previously extracted from a tar(.gz) file.

    tar -tf <file.tar.gz> | xargs rm -r

## Empacotar arquivos locais em um host remoto

    tar -czf - * | ssh example.com "cat > files.tar.gz"

    || criar pacote tar.gz || tar czvf nomedopacote.tar.gz /pasta ||
    || extrair pacote tar.gz || tar zxvf pacote.tar.gz [ -C /caminho/opcional/para/extracao/ ] ||
    || criar pacote tar.bz2 || tar cjvf nomedopacote.tar.bz /pasta ||
    || extrair pacote tar.bz2 || tar jxvf pacote.tar.bz2 -C /pasta/ ||

## Compactando tudo menos pastas

    tar -cvzf arch.tgz $(find /path/dir -not -type d)

    tar -cvf /path/dir.tar /path/dir* --exclude "/path/dir/name" --exclude "/path/dir/opt"

## como construir um pacote tar do stdin?
Qualquer comando que produza uma lista de arquivos pode ser usado

    tar cvzf archive.tgz `ls -1 *`

## criando pacotes tar com 7z

    tar cf - /path/to/data | 7z a -si archivename.tar.7z

Using 7z to create archives is OK, but when you use tar, you preserve all
file-specific information such as ownership, perms, etc. If that's important to
you, this is a better way to do it.

## para instalar o 7zip no ubuntu faça

    sudo apt-get install p7zip 7zip-full p7zip-rar lzma lzma-dev

## Compress two folders to a single tar.bz2

    tar cjvf package.tar.bz2 folder1 folder2

## How to compress and protect a folder in Linux using a password

Use the tar command to compress the whole folder named dir1 in the current working directory:

    tar -cz dir1 | openssl enc -aes-256-cbc -e > dir1.tar.gz.enc

Hide it:

    mv -v dir1.tar.gz.enc .dir1.tar.gz.enc

Delete the original directory in Linux using the rm command:

    rm -rf dir1

To decrypt, run:

    openssl enc -aes-256-cbc -d -in dir1.tar.gz.enc | tar xz
