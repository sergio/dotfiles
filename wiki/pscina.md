Filename: pscina.md
Last Change: Tue, 13 Dec 2022 - 17:09:03
tags: [trabalho, pscina]

## Decantar sugeira:

1 - Usar Sulfato de Alumínio (vai baixar o PH)
2 - Usar Barrilha para elevar o PH

## Quando usar o sulfato de alumínio?

A administração de sulfato de alumínio não é indicada em piscinas com filtros de areia. Isso porque sua função aglutinadora acabará prejudicando a eficiência dos filtros ao formar pequenos blocos de areia em seu interior.

Portanto, o ideal é usar esse tipo de aglutinador para piscinas que são limpas somente pelo processo de decantação e aspiração. O sulfato de alumínio é uma boa opção para piscinas pequenas e sem filtro, desde que a sua dosagem seja exata.

NOTA: após uma hora verificar o PH e aplicar Barrilha para normalizar a água

## Vantagens do clarificante em oposição ao Sulfato de Alumínio:

Não altera o PH, não prejudica o filtro de areia

## Manual do filtro da pscina:
+ https://www.sodramar.com.br/manual/filtros/filtros-fm-100.pdf

## Pastilha de cloro para evitar algas (manutenção):
+ https://www.youtube.com/watch?v=aVdo22AsIZw

Pastilha de cloro (barata) usada para prevenção no aparecimento de algas.
uma pedra para cada 30 mil litros

Modo de aplicação: (usar dentro de fluturadores)


## Produtos para a manuteção da pscina:

  OBS: O volume da pscina nova é aproximadamente o triplo da pscina antiga
  OBS: A primeira água (quase toda de pipa) provavelmente já tinha microalgas o que aumentou a demanda
       pelos produtos de limpeza. Tendo a pcina ficado por volta de um mês sem manutenção preventiva
       até que o sistema de drenagem estivesse operante.

  ┌─────────────┬───────────────────────────────────┬────────────────────────────────┐
  │ Quantidade  │   produto                         │  Descrição                     │
  ├─────────────┼───────────────────────────────────┼────────────────────────────────┤
  │   02        │   Algicida                        │  Combate as microalgas         │
  │   03        │   Clarificante                    │  Ação coagulante (decantação)  │
  │   01        │   Cloro granulado (balde)         │  Bactericida e algicida        │
  │   10        │   Cloro em pastilha               │  Economizar o cloro granulado  │
  │   01        │   Adaptador para dreno (depósito) │  Aspiração da pscina pequena   │
  │             │                                   │                                │
  └─────────────┴───────────────────────────────────┴────────────────────────────────┘

