---
Filename: ventoy.md
Last Change: Mon, 19 Feb 2024 - 20:50:57
tags: [usb, stick, iso, tools, liveusb]
---

# site

+ [Oficial site](https://www.ventoy.net/en/index.html)
+ [Ventoy pendrive multi boot](https://www.youtube.com/watch?v=NOZmSTT7jKM)
+ [video do diolinux](https://youtu.be/11CkqZQ3scE)
+ [Como usar o Ventoy](https://www.hardware.com.br/comunidade/v-t/1538171/)

Download ventoy [here](https://github.com/ventoy/Ventoy/releases)
then unpack the file, enter the folder and run:

    sudo Ventoy2Disk.sh { -i | -I | -u } /dev/sdX

If you do not know your usb device address run:

    lsblk
    sudo lsblk -o model,name,fstype,size,label,mountpoint

or

    doas blkid

## Vídeo do Pinguin Criativo sobre isos persistentes com o ventoy

+ [Isos persistentes com ventoy](https://www.youtube.com/watch?v=QLtZ2w2twg4)

## Personalizando o grub do ventoy

+ <https://www.youtube.com/watch?v=s2KHtIlbzqc>
+ <https://www.youtube.com/watch?v=HsKXXm2U12c>
