---
file: ipd.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: Introdução Ao Processamento de Dados
---

#  ipd.md intro:

INFORMÁTICACiência
do  tratamento  racional  da  informação  por  via  douso de máquinas automáticas.

Informação + Automática = Informática

Neologismo criado por Philippe Dreyfus em 1962 com oobjectivo  de  designar  as
disciplinas  que  permitem  otratamento automático de informação com a
finalidade degarantir a sua preservação e comunicação

“PARA GOSTAR É PRECISO CONHECER!”
+ http://homepage.ufp.pt/lmbg/textos/intro_inf.pdf

Assim, todos os indivíduos com responsabilidades edevidamente
educados para enfrentar a sociedade devemter conhecimentos em quatro
áreas relacionadas com astecnologias de informação:

- desenvolvimentos do processamento de dados einformação

- conceitos básicos de hardware e software (materiale  lógica)  e  dos  ambientes  específicos  que  estesgeram,

- impacto social resultante do uso de computadores, (eleição de 2018)
  Como a informática interfere na cultura e como pode nos auxiliar?

- modos  de  utilização  dos  computadores  emdiferentes áreas do saber.

Discussão do impacto da informática e dos redes colaborativas
Exemplo do projeto Genoma e do projeto GNU/Linux

## Hardware e Software

## Digital / Analógico

    O que é digitalização ou cópia digital
    O que é cópia analógica (analogia)

## Bit Byte (tamanhos dos arquivos)

    Como mensurar um arquivo e/ou o espaço disponível no seu
    sistema

    Byte, Kilobyte ou KB
    MegaByte ou MB
    Gigabyte ou GB
    Terabyte ou TB

Como descobrir o tamanho de um arquivo

    BIOS - Sistema Básico de Entrada e Saída
    Memória do computador

        Memória ROM
        Memória RAM

        Disco Rígido ou Hard Disk
        Memória Flash
        Discos em estado sólido SSD (novos)

Um pouco da história do computador
filme o jogo da imitação: http://bit.ly/2u6O0WJ

Teorias da conspiração sobre o surgimento do Microchip

## Dado e Informação
+ https://www.gigaconteudo.com/diferenca-entre-dados-e-informacao
+ http://bit.ly/2SijDVi

    Informações são compostas por dados

De maneira geral, é o conteúdo quantificável e que por si só não transmite
nenhuma mensagem que possibilite o entendimento sobre determinada situação.

    Ex de dado: Rua Coronel Feliciano, 312

É o resultado do processamento dos dados. Ou seja, os dados foram analisados
e interpretados sob determinada ótica, e a partir dessa análise se torna
possível qualificar esses dados.

    Exe de Informação: José Claudio Lima reside na Rua
    Coronel Feliciano, 312, Bairro Água Fria, Fortaleza-CE.

A  palavra  COMPUTADOR  tem  a  sua  origem  do  latimcomputare que significa “contar”, “calcular” ou “avaliar”.

    Entrada  --> Processamento --> Saída
    dados                          Informação

Esquema Geral da Informáticao ilustra o contexto em que o computador é útil!

    - O computador realiza o processamento
    - Existe uma entrada de dados - input
    - Existe uma saída de dados - output
    - O processamento de dados é controlado através deum programa

    ## Como gerenciar informaçoes de forma efetiva
    Não misturar informações para facilitar a pesquisa
    Por Exemplo: Nunca coloque o nome da rua junto com o número
                 e nem com o bairro juntos

                 Quando construir uma base de dados evite ascentos
                 para evitar ambiguidade

## Segurança da Informação
+ http://bit.ly/39ig4p5

 Para uma informação ser considerada segura ela precisa atender a esses
 requisitos

 Confidencialidade, Integridade e Disponibilidade (CID)

## Algoritmo (Receita de bolo) ou código fonte

Software Livre e Software Proprietário

   Linguagens de programação
   Linguágem natural (permite ambiguidade)

   Variáveis - espaços de memória onde testamos condições
   Repetições (for) por tantas vezes faça
   Condições (if) se tal condição existir faça tal coisa

    https://www.onlinegdb.com/online_python_interpreter

    Exemplo de código em python

    for i in range(1,11):
        if (i % 2 == 0):
            print("O número ", i, " é par")

## Dispositivos de entrada e saída (isso cai em concurso)

    Periféricos - CPU, Gabinete
    identificando a natureza do dispositvo

    Entrada
    Saída
    Entrada e Saída

## Programa gratuito para aprender a digitar

    Saber digitar é algo essencial
    https://klavaro.sourceforge.io/pt/

