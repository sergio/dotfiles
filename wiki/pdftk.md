---
file: pdftk.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

# Arquivo: manual do comando pdftk

## Pdftk - manipulando pdfs por linha de comando

* source: [Manipulando arquivos PDF com pdftk](http://www.vivaolinux.com.br/dica/Manipulando-arquivos-PDF-com-pdftk)

Uma alternativa mais leve é o [stapler](stapler)

## Concatenando dois arquivos em um

veja também o comando [convert](convert.md) e o comando [gs](gs)

    pdftk Arquivo1.pdf Arquivo2.pdf cat output Concatenado1e2.pdf

    gs -q -dNOPAUSE -dBATCH -sDEVICE=pdfwrite -sOutputFile=bla.pdf -f foo1.pdf foo2.pdf

## Removin password from a pdf file (remover senha de arquivo pdf)

* [Removing password from pdf on linux](https://www.cyberciti.biz/faq/removing-password-from-pdf-on-linux/)

   pdftk input.pdf output output.pdf user_pw YOURPASSWORD-HERE

I have removed a pdf password using qpdf:

  qpdf --password=YOURPASSWORD-HERE --decrypt input.pdf output.pdf

## Dividindo arquivos

    pdftk Arquivo.pdf burst

Desta forma cada página do arquivo será separada em um arquivo independente e
nomeados como pg_0001.pdf, pg_0002.pdf e assim por diante.

## Selecionando páginas a serem separadas

    pdftk arquivo.pdf cat 1-5 10-12 output paginasselecionadas.pdf

* o arquivo paginasselecionadas.pdf será criado com as páginas de 1 a 5 e de 10 a 12 do arquivo arquivo.pdf.

Pode-se remover uma determinada página repetindo o comando anterior alterando
apenas os parâmetros. Por exemplo, remover a página 5 de um arquivo:

    pdftk arquivo.pdf cat 1-4 6-end output novoarquivo.pdf

## convertendo formato cbr para pdf

+ source: [how-to-convert-cbr-to-pdf](http://askubuntu.com/questions/207172/how-to-convert-cbr-to-pdf)

  O formato cbr é usado em comics (cartoons)
  Você deve ter instalados os programas: pdftk sam2p unrar unzip

O comando de conversão é:

    cbr2pdf file.cbr
