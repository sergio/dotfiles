---
file: vis-editor.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: Introduce vis editor
---
#  vis-editor.md intro:
+ https://github.com/martanne/vis


vis is a vi-like editor based on Plan 9’s structural regular expressions.

## structural regular expressions
+ http://doc.cat-v.org/bell_labs/structural_regexps/se.pdf
