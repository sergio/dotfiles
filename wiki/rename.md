---
file: rename.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
---

## Instalando o perl-rename

Busque o pacote linux-util em sua distribuição
mas em geral há uma confusão entre o comando rename e o comando perl-rename,
sendo este último mais poderoso, portanto vejamos como instalar o perl-rename

## installing real [perl-rename](perl-rename.md)

A lot of manuals cite rename as perl-renam but are differences
to get real rename follow this instructions:

    cpan
    cpan1> install File::Rename

    sudo ln -s /usr/local/bin/rename /usr/bin/rename.pl


Obs: o comando [perl rename](perl-rename.md) é mais eficiente e no meu caso
criei um aliás para ele

    (( $+commands[perl-rename] )) && alias prename='perl-rename'
    (( $+commands[perl-rename] )) && alias rename='perl-rename'

## Adicionando zeros no começo dos nomes:
+ https://dquinton.github.io/debian-install/config/ranger.html#yk

    leadingzeros(){
        i=1; for file in $@; do mv "$file" $(printf "${file%.*}_%0.3d.${file##*.}" $i); i=$((i+1)); done
    }

Using the "ranger" bulkrename we can do:

    let c=0 | g// let c+=1 | s//\=printf('%02d', submatch(0))/

# removendo espaços no nome

    rename 'y/ /_/' *

``` sh
for i in *mp3; do
    mv "$i" `echo $i | sed 's/ //g'`
done
```

## modificando a extensão

``` sh
rename 's/\.html$/.htm/' *.html
rename .oldextension .newextension *.oldextension
```

## Se tirver hifem no começo pode dar erro
Esta solução para arquivos com hifen no começo

    find . -print0 | xargs -0 rename 's/-//g'

## removendo espaços no nome

    rename "s/ *//g" *.jpg
    rename 's/ /_/g' *.htm

## Adicionando extensão

    rename 's/$/.txt/g' *

usando o find e o xargs

    find . -iname \*.html -print0 | xargs -0 rename 's/\.html$/.htm/'
    find . -depth | xargs -n 1 rename -v 's/(.*)\/([^\/]*)/$1\/\L$2/' {} \;

Usando o find podemos fazer um comando recursivo, contudo
há um problema com arquivos que tenham "espaços no nome" isso
causaria um erro, por isso usamos '-print0' para imprimir um
delimitador para cada arquivo, o xargs por sua vez tem uma opção
que pega o delimitador enviado pelo find e entrega o resultado
para o rename.

O rename usa expressões regulares '\.' é um ponto literal
e '$' é final de linha.

## batch renaming with ranger file manager

Open ranger file manager at the directory you want, select the lines with
<shift> + v and type:

    :bulkrename

Use vim to edit the names, save the file and exit (that's it)

## adding leading zeros in the filenames
+ https://unix.stackexchange.com/a/347072/3157

(Remove flag -n to actually do the renaming.)

``` sh
rename -n -e 's/_(\d\.)/_0$1/g' -- *.jpg
'File_Ex_1.jpg' would be renamed to 'File_Ex_01.jpg'
'File_Ex_2.jpg' would be renamed to 'File_Ex_02.jpg'
'File_Ex_3.jpg' would be renamed to 'File_Ex_03.jpg'
'File_Ex_4.jpg' would be renamed to 'File_Ex_04.jpg'
'File_Ex_5.jpg' would be renamed to 'File_Ex_05.jpg'
'File_Ex_6.jpg' would be renamed to 'File_Ex_06.jpg'
'File_Ex_7.jpg' would be renamed to 'File_Ex_07.jpg'
'File_Ex_8.jpg' would be renamed to 'File_Ex_08.jpg'
'File_Ex_9.jpg' would be renamed to 'File_Ex_09.jpg'
```

