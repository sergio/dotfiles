---
File: ~/.dotfiles/wiki/excel.md
Last Change: Sun, 17 Nov 2024 - 09:33:19
tags: [excel, windows, vb]
---

# Dicas excel e visual basic

## Extensões para automação

+ automa
+ [Extrair links](https://youtu.be/iyab0aTK7mM)
+ [Conectar automa ao sheets](https://youtu.be/XZCOeHDZgKA)

## Formulas do excel

No meu caso tenho numeros assim

```txt
001-4
004-8
```

e quero remover do traço em diante. Posso fazer assim:

```txt
=ESQUERDA(A1;3)
```

## Calcular a quantidade de dias do viveiro

Detalhe: A data a ser considerada é a quantidade de dias do viveiro hoje somado
com a quantidade de dias até a próxima segunda-feira


```txt
=SE(VALOR(SUBSTITUIR(N7; "/0"; "")) = 0; "Vazio"; VALOR(SUBSTITUIR(N7; "/0"; "")) + MOD(2 - DIA.DA.SEMANA(HOJE(); 2); 6))
```

Na formula acima o número que pego tem um “/0” no final (são número de dias de um viveiro). Preciso acrescer a quantidade de dias até a próxima segunda feira,

Calcular sobrevivência

```txt
=SE(B1="Vazio";"";SE(E(B1>=1;B1<=30);"80%";SE(E(B1>=31;B1<=45);"66%";SE(E(B1>=46;B1<=65);"55%";SE(E(B1>=66;B1<=80);"50%";SE(E(B1>=81;B1<=95);"40%";SE(B1>=96;"35%";"Valor Inválido")))))))
```

A célula pode conter “Vazio”, neste caso exibirá nada ““, caso contrário calcula a sobrevivência baseada no número de dias

## Formatação condicional

Essa fórmula uso para formatação condicional uma linha cinza e outra branca

```txt
=MOD(LIN(A1);2)=0
```

## Validação de dados

Na validação de dados você pode colar os ítens separados por ponto e vírgula
para poder colar de outro programa

```txt
SM LINE 400 #2; SM LINE 400 #2 (TESTE); SM LINE 400 #1; SM LINE 350 LS; SM LINE 350 LS (TESTE);
```

## Filtrar uma lista por outra

Na coluna A tenho números de viveiros em ordem crescente
na coluna B tenho o peso do camarão
na coluna C o consumo de ração do viveiro
na coluna D tenho a lista de viveiros do Técnico

o CONT.SE(CE:CE;CB7)>0 testa se o viveiro do técnico está na coluna "A"
SE(CC7>6) testa se o peso do camarão está acima de 6 gramas

               teste
       ┌────────────────────┐                 se faso
       │                    │                  ┌──┐
    =SE(CONT.SE(CE:CE;CB7)>0;SE(CC7>6;"sim";"");"")
                             └────────────────┘
                                 se sim

CE:CE corresponde à lista de viveiros do técnico
CB7 é o primeiro item da lista de viveiros do relatório
CC7 é o peso do camarão

## dica do filtro

[Youtube](https://youtube.com/shorts/DIgRE0uu0GA)

## pular campo com Enter

+ [Formulario no excel](https://youtu.be/cKYHnO99xis)
```vbscript
Private Sub Worksheet_Change(ByVal Target As Range)
    Application.EnableEvents = False
    If Not Intersect(Target, Range("d6")) Is Nothing Then
        Range("d8").Select
    End If
    If Not Intersect(Target, Range("d8")) Is Nothing Then
        Range("g6").Select
    End If
    If Not Intersect(Target, Range("g6")) Is Nothing Then
        Range("g8").Select
    End If
    If Not Intersect(Target, Range("g8")) Is Nothing Then
        Range("d6").Select
        Call Cadastro
    End If
    Application.EnableEvents = True
End Sub
```

# formula switch

+ [video sobre](https://youtu.be/7jF62HvHG3Q)

```excel
=SWITCH(B1, 101, "Camiseta", 102, "Calça", 103, "Sapato", "Produto não encontrado")
```


## Filtrar uma listagem menor por uma lsitagem maior

Na listagem maior tenho   numero do viveiro, peso do camarão e ração acumulada

ao lado  na coluna  E  tenho a listagem de todos os viveiros do técnico em
questão

```txt
=SE(CONT.SE(E:E; A1)>0; "Sim"; "Não")
```

## Tornar referências abosolutas

```vbscript
' Transforma referências em absolutas
' Não necessita seleção

Sub TransformarReferenciasRelativasParaAbsolutas()
    Dim regex As Object
    Dim cel As Range
    Dim inputText As String
    Dim outputText As String
    Dim matches As Object
    Dim match As Object
    Dim changes As Integer
    Dim ws As Worksheet

    changes = 0  ' Contador para verificar se houve alterações

    ' Define o objeto Regex para identificar referências relativas
    ' Captura: Nome da planilha (opcional), Coluna relativa ([A-Z]), Linha relativa (\d+), mas exclui referências absolutas ($)
    Set regex = CreateObject("VBScript.RegExp")
    regex.Pattern = "(?:([\w\s]+)!)?([A-Z])(\d+)"  ' Regex genérico para identificar planilhas e referências relativas
    regex.Global = True

    ' Define a planilha ativa
    Set ws = ActiveSheet

    ' Ativa o tratamento de erros
    On Error Resume Next

    ' Loop através de todas as células usadas na planilha ativa
    For Each cel In ws.UsedRange
        ' Verifica se a célula contém uma fórmula
        If cel.HasFormula Then
            inputText = cel.Formula  ' Obtém a fórmula da célula
            outputText = inputText   ' Prepara outputText como uma cópia inicial

            ' Encontra todas as correspondências com a expressão regular
            If regex.Test(inputText) Then
                Set matches = regex.Execute(inputText)

                ' Loop por cada correspondência encontrada
                For Each match In matches
                    ' Ignora se a referência já é absoluta
                    If Not InStr(match.Value, "$") > 0 Then
                        Dim planilha As String
                        Dim coluna As String
                        Dim linha As String

                        ' Extrai os grupos capturados pelo regex
                        planilha = match.SubMatches(0) ' Nome da planilha (pode ser vazio)
                        coluna = match.SubMatches(1)   ' Coluna relativa
                        linha = match.SubMatches(2)    ' Linha relativa

                        ' Adiciona o nome da planilha se necessário
                        If planilha = "" Then
                            planilha = ws.Name & "!"  ' Nome da planilha atual
                        Else
                            planilha = planilha & "!"
                        End If

                        ' Constrói a referência absoluta
                        outputText = Replace(outputText, match.Value, planilha & "$" & coluna & "$" & linha)
                    End If
                Next match

                ' Se o outputText foi modificado, atualiza a célula
                If outputText <> inputText Then
                    cel.Formula = outputText
                    changes = changes + 1  ' Incrementa o contador de alterações
                End If
            End If
        End If
    Next cel

    ' Desativa o tratamento de erros
    On Error GoTo 0

     Exibe uma mensagem com o número de alterações feitas
    If changes > 0 Then
        MsgBox changes & " referências relativas foram convertidas para absolutas.", vbInformation
    Else
        MsgBox "Nenhuma referência relativa foi encontrada nas células usadas.", vbInformation
    End If
End Sub
```

## incrementa fórmulas

```vbscript
' Gera uma cópia das fórmulas incrementadas na nova posição (cinco colunas à direita).
' A fórmula original na célula selecionada permanece inalterada. Vamos esclarecer isso:

Sub IncrementarFomulasCincoCelulasParaDireitaRelativas()
    Dim cel As Range
    Dim inputText As String
    Dim newFormula As String
    Dim newRowNumber As Long
    Dim newColNumber As Long

    ' Loop através de cada célula na seleção
    For Each cel In Selection
        If cel.HasFormula Then
            inputText = cel.formula
            newColNumber = cel.Column + 5 ' Calcula nova coluna, 5 à direita

            ' Captura a letra da coluna e o número da linha
            Dim columnLetter As String
            Dim rowNumber As Long

            ' Usar expressão regular para capturar referências
            Dim regex As Object
            Set regex = CreateObject("VBScript.RegExp")
            regex.Global = True
            regex.IgnoreCase = True
            regex.Pattern = "([A-Z]+)(\d+)" ' Padrão para capturar referências de células sem $

            If regex.Test(inputText) Then
                Dim matches As Object
                Set matches = regex.Execute(inputText)

                For Each match In matches
                    columnLetter = match.SubMatches(0) ' Letra da coluna
                    rowNumber = CLng(match.SubMatches(1)) ' Número da linha

                    ' Incrementa o número da linha
                    newRowNumber = rowNumber + 1

                    ' Atualiza a nova fórmula
                    newFormula = Replace(inputText, match.Value, columnLetter & newRowNumber)
                Next match
            End If

            ' Insere a nova fórmula na célula apropriada
            Cells(cel.row, newColNumber).formula = newFormula
        End If
    Next cel
End Sub
```

### outra formula para incremento

```vbscritp
' copia a fórmula com incremento 5 células para a direita

Sub IncrementSel()
    Dim cel As Range
    Dim newFormula As String
    Dim newCol As Integer
    Dim regex As Object
    Dim matches As Object
    Dim match As Object
    Dim colLetter As String
    Dim rowNum As Long

    ' Loop através de cada célula na seleção
    For Each cel In Selection
        If cel.HasFormula Then
            ' Captura a fórmula da célula atual
            newFormula = cel.formula

            ' Define a nova coluna (5 células para a direita)
            newCol = cel.Column + 5

            ' Cria um objeto RegExp
            Set regex = CreateObject("VBScript.RegExp")
            regex.Global = True
            regex.IgnoreCase = True
            regex.Pattern = "(\$?[A-Z]+)(\$?(\d+))" ' Captura referências com ou sem $

            ' Verifica se a fórmula tem referências
            If regex.Test(newFormula) Then
                ' Aumenta o número da linha em 1 para cada referência encontrada
                Set matches = regex.Execute(newFormula)

                ' Incrementa as linhas nas referências
                For Each match In matches
                    colLetter = match.SubMatches(0)   ' Captura a letra da coluna
                    rowNum = CLng(match.SubMatches(2)) + 1 ' Incrementa a linha
                    ' Substitui a referência original pela nova referência com a linha incrementada
                    newFormula = Replace(newFormula, match.Value, colLetter & rowNum)
                Next match

                ' Insere a nova fórmula na célula 5 colunas à direita
                Cells(cel.row, newCol).formula = newFormula
            End If
        End If
    Next cel

    'MsgBox "Fórmulas incrementadas com sucesso!", vbInformation
    Application.StatusBar = "Fórmulas incrementadas com sucesso!"
End Sub

```

## inserir fórmula com teste condicional

Essa fórmula é bem complicada, por isso criei esse VBscript

```vbscript
Sub InserirFormula()
    Dim celulaReferencia As Range
    Dim formula As String

    ' Define a célula de referência como 4 linhas acima e 2 colunas à esquerda da célula ativa
    Set celulaReferencia = ActiveCell.Offset(-4, -2)

    ' Cria a fórmula com a célula de referência ajustada
    formula = "=IF(" & celulaReferencia.Address & "=""Vazio"",""""," & _
              "IF(AND(" & celulaReferencia.Address & ">=1," & celulaReferencia.Address & "<=30),""80%""," & _
              "IF(AND(" & celulaReferencia.Address & ">=31," & celulaReferencia.Address & "<=45),""66%""," & _
              "IF(AND(" & celulaReferencia.Address & ">=46," & celulaReferencia.Address & "<=65),""55%""," & _
              "IF(AND(" & celulaReferencia.Address & ">=66," & celulaReferencia.Address & "<=80),""50%""," & _
              "IF(AND(" & celulaReferencia.Address & ">=81," & celulaReferencia.Address & "<=95),""40%""," & _
              "IF(" & celulaReferencia.Address & ">=96,""35%"","""")))))))"

    ' Insere a fórmula na célula ativa
    ActiveCell.formula = formula
End Sub
```

