---
File: /home/sergio/.dotfiles/wiki/firejail.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [security, hack, tools]
 vim:ft=markdown
 ```

## what is firejail?
+ https://www.youtube.com/watch?v=MVLrclfbS4U

Firejail is an easy to use SUID sandbox program that reduces the risk of security breaches by restricting the running environment of untrusted applications using Linux namespaces, seccomp-bpf and Linux capabilities.

## Using Firejail by default

To use Firejail by default for all applications for which it has profiles, run the firecfg tool with sudo:

    $ sudo firecfg
