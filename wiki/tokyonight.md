---
Filename: tokyonight.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
---
 vim:ft=markdown

## nice colorscheme written in pure lua

Changing the split window border:

    vim.g.tokyonight_colors = { border = "orange" }
