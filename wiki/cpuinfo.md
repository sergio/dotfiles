---
file: cpuinfo.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# quantos núcleos tem o meu processador?

    grep -c ^processor /proc/cpuinfo

## vendor

    cat /proc/cpuinfo | grep vendor | uniq
    vendor_id       : GenuineIntel

Its an Intel processor. Next find the model name that can be used to lookup
the exact specifications online on Intel's website.

    cat /proc/cpuinfo | grep 'model name' | uniq
    model name      : Intel(R) Core(TM) i3-2328M CPU @ 2.20GHz

