---
file: paste.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

## Introdução

    paste pares.txt impares.txt

## Transformar 3 linhas em uma

    2,5,1,1
    3
    5
    3,7,1,1
    2
    8

    ## resulta em

    paste -d"," - - - < file
    2,5,1,1,3,5
    3,7,1,1,2,8

## Colar dois trechos do mesmo arquivo lado a lado

    joao
    jose
    felipe

    um
    dois
    tres

### Solução

    paste -d' ' <(sed '/^$/,$d' lixo) <(sed '1,/^$/d' lixo)

Na solução acima pegamos de uma linha vazia '^$' até o final $
e da linha '1' até uma linha vazia '^$'

## juntar linhas de 5 em 5

    ## Pessoal,

tenho uma comando que gera a seguinte saida, onde a unica padronagem é uma
sequencia de 5 linhas:

    0
    r5
    751625160
    601300096
    391584768
    1
    r5
    1401393800
    1121115008
    621056
    12
    r5
    1401393800
    1121115008
    68344320
    45
    r5
    1401393800
    1121115008
    1235456
    223
    r5
    2522508840
    2242230016
    388346880

## Eu precisava gerar a saida neste formato, em 5 colunas:

    0 r5 751625160 601300096 391584768
    1 r5 1401393800 1121115008 621056
    12 r5 1401393800 1121115008 68344320
    45 r5 1401393800 1121115008 1235456
    223 r5 2522508840 2242230016 388346880
    Como poderei fazer?
    Obrigado,
    Alessandro Almeida.

## solução

    paste -d" " - - - - - < teste.txt

## Turning multiple lines into one line with comma separated
+ https://stackoverflow.com/questions/15758814/

I have the following data in multiple lines:

    foo
    bar
    qux
    zuu
    sdf
    sdfasdf

What I want to do is to convert them with one line and comma separated:

    foo,bar,qux,zuu,sdf,sdfasdf

solution

    paste -d, -s file

You can also use vim (if your file is already open)

    :1,$- s/\n/,
    ::%!paste -sd,

    Just joining lines
    :silent g/./-1 normal gJ

The paste command merges the lines of two or more files or a file and standard in if a second file is not specified or a “-” is used in place of the second file.  Consider the following two files.  The first file, test1.txt contains the following lines:

    a
    one
    three
    cat
    good

The second file, test2.txt contains the following lines:

    tuna
    blue finch
    dogs
    fish
    eats

The paste command can be used to paste these two files like so:

    paste test1.txt test2.txt

producing the following output:

    a       tuna
    one     blue finch
    three   dogs
    cat     fish
    good    eats

Each line in test1.txt has been “pasted” to the corresponding line in test2.txt.  The default delimiter to “paste” each line together with is TAB.  “The delimiter can be specified with the -d, or –delimiters flag:

    paste –delimiters=, test1.txt test2.txt

Produces:

    a,tuna
    one,blue finch
    three,dogs
    cat,fish
    good,eats
