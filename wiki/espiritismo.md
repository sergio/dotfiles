---
file: espiritismo.md
author: Sergio Araujo
Last Change: Sat, 21 Sep 2024 - 20:37:37
tags: [espiritismo]
---

# Chama violeta

Fonte: Saulo Couto
[site](https://mentor.etc.br/meditacao-mais-poderosa-chama-violeta-saint-germain-decretos-e-afirmacoes-de-poder-o-eu-sou/)

Meditação Mais Poderosa – Chama Violeta – SAINT GERMAIN – Decretos e Afirmações de Poder – O EU SOU

Mantra dos decretos: Afirmações de Poder – O EU SOU

O meu coração é uma luz de fogo da chama violeta
o meu coração é a pureza que Deus deseja. (3x)

EU SOU um Ser de Fogo Violeta
EU SOU a Pureza que Deus Deseja. (3x)

Amada Presença de Deus eu sou em mim,
Cobre e protege a mim e à minha família com Seu Cone de Luz Branca,
Liberta-nos de todo o mal físico, mental, emocional e espiritual,
Purificando-nos com a Chama Violeta e
Libertando nosso templo, para a vinda do Senhor. (3x)

“EU SOU” é a atividade da vida, a plena atividade de Deus. “EU SOU” é a fonte ativa, o Deus dentro de nós.

Para começar, imagine um ponto de luz, vindo do alto de sua cabeça, como sendo o “EU SOU”.

Diga como quiser, em alto e bom tom ou mentalmente, como seu coração intuir:

“EU SOU A PORTA ABERTA QUE NINGUÉM PODE FECHAR!”

“EU SOU” luz!

“EU SOU” amor!

“EU SOU” saúde!

“EU SOU” prosperidade!

“EU SOU” abundância!

“EU SOU” fartura

“EU SOU” cura!

“EU SOU”… Diga o que você mais quer que aconteça contigo… Manifestados agora!

Assim é! Assim é! Assim é!

Tente fazer por 21 dias consecutivos na hora que mais desejar, é aconselhável fazer quando acordar ou quando for dormir. Um tratamento terapêutico magnífico que se creres conseguirá alcançar seu objetivo. E todas as vezes que lembrares pronuncia esta frase:

“EU SOU” o “EU SOU!” “O Deus em ação em mim!”

Esta é uma meditação muito poderosa para transformar positivamente nossa vida.
Para que não conhece a Chama Violeta é um dos Raios Divinos e nos ajuda transmutando tudo aquilo que não queremos em nossa vida.
Estamos falando , de vícios, sofrimento, relações com pessoas ou lugares, crenças limitantes, medos, pensamentos e atitudes negativas, enfim, cada um sabe onde está o foco de sua dor, com essa meditação e uma vontade verdadeira de mudar irá conseguir transformar…
Além disso a chama violeta tem a capacidade de limpar não só nosso corpo físico mas equilibrar também os nossos 7 corpos espirituais, realinhar os chakras, e harmonizar e curar nossa aura.
Mestre Saint Germain nos deu um dos mais importantes instrumentos de cura a Chama Violeta, os mestres nos dizem que toda dor e miséria surgem dos nossos pensamentos, sentimentos que acabam chegando ao mundo físico através de nossas atitudes baseadas nesses medos, esse Fogo Sagrado, não apenas limpa como transmuta o negativo em positivo.
Aqui nesse plano nada desaparece ele se transforma.
Faça Essa meditação para dormir ou antes de dormir todos os dias e veja sua vida se transformando a olhos vistos…

A Chama Violeta ativa uma limpeza cármica profunda, assim como a limpeza e transmutação de registros negativos e aspectos nocivos do ego. Ela liberta a pessoa de energias intrusas e externas, e ativa a capacidade de autoproteção desse tipo de energias.
A Chama Violeta apaga e dissolve os erros cometidos, transmuta estados mentais depressivos em um estado mental sereno e em paz, alivia a dor física, emocional e mental. A Chama Violeta é como uma poderosa transmuta o lixo de nossas vidas passadas e presente, de nossas cidades, de nossa nação e do nosso planeta também. Queimando este lixo e transmutando-o em Luz; Luz que volta para o nosso Corpo Causal, aumentando, assim, a nossa riqueza espiritual.
Quando não sabemos qual é a causa de um problema, invocar o poder do Fogo Sagrado Transmutador para dissolver o núcleo e a causa do problema.
Todo carma negativo é criado com nossos pensamentos, sentimentos, palavras e atos negativos.
Quando transformamos nossa energia em rancor, ressentimento, ódio, tristeza, medo, crítica, condenação ou julgamento, ela fica muito pesada para subir de volta a Fonte Original.

Então ela segue unindo -se a outras energias iguais a ela.
Assim, ela volta multiplicada, acumula-se.
Com o acúmulo de negatividade em nosso ser, acabamos por atrair mais e mais negatividade, problemas, escassez, doenças, acidentes, brigas … A Chama
Violeta dissolve os véus de ilusão e ignorância, trazendo, à Luz, a Verdade divina.

Nesse caso o ideal é que seja feito pelo menos 21 dias, mas os resultados costumam aparecer bem antes.

## O desligamento do véu

[youtube](https://youtu.be/3TZEyWyWVyk)

## Mantras

[Prosperidade](https://youtu.be/THAHDnhY6g4?si=A-ADpVfBcSG79tfs)

----------------------------------

Mantenha o seu voto de silêncio

Atenha-se exclusivamente às suas tarefas para fazê-las bem feito. Eu só vou ficar aquí o tempo que a espiritualidade achar conveninente, eu não fiz planos.

As tarefas que a espiritualidade determinar eu tentarei dentro das minhas limitações executar.

Tipo: Você está morando aqui? Não: Não exatamente morando, estou aqui pelo tempo que a espiritualidade determinar para executar algumas tarefas que precisam de cuidados diários, serviços como cuidar do galinheiro, fazer a coleta do lixo do entorno etc. Obviamente que se a coordenação determinar outros afazeres me sentirei feliz em poder contribuir de algum modo.

----------------------------------

Mohib Ibn Árabe, formulador do islã místico, conhecido como mestre dos mestres, foi, talvez, quem melhor traduziu a religião do amor quando dizia, há cerca de 700 anos:

“Meu coração se tornou capaz de todas as coisas: é pastagem para as gazelas, convento para os monges e templo para os ídolos; é a Caaba para os peregrinos, a tábua das leis e o livro do Corão. Minha religião é a religião do amor. Seja qual for o caminho que os camelos tomem, esta é minha religião e minha fé”.

fonte: http://www.smashwords.com/extreader/read/11419/9/terra-a-ilha-da-fantasia

[previna se](previna-se.md)
[Prece da Gratidão](prece-da-gratidao.md)

## Numerologia

Códigos que você deve usar

Ansiedade ............................ 51949131948
Harmonizar o presente ................ 71042
Dinheiro Inesperado .................. 520
Tudo é Possível ...................... 5197148
Tudo Ficará 100% ..................... 918197185
Solução Imediata ..................... 741
Prosperidade ......................... 318798

Lembre-se de recitar os números um por um
Ex: Sete, quatro, um

## Maual de viagem astral (viagemastral.com)

+ (capítulo 1) https://www.youtube.com/watch?v=sG2p2FpmRNU
+ (capítulo 2) https://www.youtube.com/watch?v=NTKu-gaD9Hk

## Palestras Espíritas

+ A mente extrafísica - https://www.youtube.com/watch?v=s7eG0t408s4
+ Tudo é pensameto - https://www.youtube.com/watch?v=R4G9DWwIn9E
+ O espírito me disse - https://www.youtube.com/watch?v=4O0KSCoSjyU

Avé Maria, cheia de graça, o Senhor é convosco, bendita sois vós entre as mulheres e bendito é o fruto do vosso ventre, Jesus. 
Santa Maria mãe de Cristo, rogai por nós filhas e filhos de Deus, agora e na hora de nossa vitória sobre o pecado a doença e morte! Amém

## Consagração do Sal

A consagração do sal é um ritual comum em várias tradições espirituais, incluindo práticas cristãs e esotéricas. Ele é utilizado como um símbolo de purificação e proteção, além de ser um elemento de bênção. Aqui está uma versão tradicional da consagração do sal:

1. **Prepare o sal**: Pegue uma pequena quantidade de sal, geralmente sal grosso, em um recipiente adequado.
   
2. **Oração de Consagração**:
   - Faça o sinal da cruz sobre o sal.
   - Recite uma oração para abençoar o sal. Um exemplo é:
     ```
     Deus Todo-Poderoso, abençoe este sal,
     que ele se torne um símbolo de purificação e proteção.
     Que por ele, sejamos guardados de todo mal.
     Em nome do Pai, do Filho e do Espírito Santo. Amém.
     ```

3. **Intenção**: Após a oração, declare sua intenção de uso, como purificação do ambiente, proteção ou bênção pessoal.

4. **Uso do sal**: Após consagrado, o sal pode ser espalhado em lugares que você deseja purificar, como cantos da casa, entradas, ou pode ser usado em rituais de banimento ou proteção.

Esse rito pode variar dependendo da crença ou tradição que você segue, mas o princípio geral é o mesmo: o sal, abençoado ou consagrado, é um elemento poderoso de proteção espiritual.
