Filename: sddm.md
Last Change: Thu, 24 Nov 2022 - 11:02:31

## Beautiful login manager:
 vim:ft=markdown
+ https://www.youtube.com/watch?v=2p7FINJSlAk

    # Note pi is an alias for doas xbps-install -Sy (opendoas)
    # download sugar-dark theme:
    # https://www.opendesktop.org/p/1272122 (sugar-dark)
    # https://store.kde.org/p/1312658/ (sugar-candy)

    doas tar -xzvf ${XDG_DOWNLOAD_DIR}/sugar-dark.tar.gz -C /usr/share/sddm/themes

    [ -f /etc/sddm.conf.d/sddm.conf ] || doas mkdir -p /etc/sddm.conf.d
    doas wget -c https://pastebin.com/raw/weygFJjX -O /etc/sddm.conf.d/sddm.conf

    pi sddm qt5{,-quickcontrols2,-svg,-graphicaleffects}
    rm -f /var/service/lxdm
    doas ln -s /etc/sv/sddm /var/service
