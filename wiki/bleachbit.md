---
file: bleachbit.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
---

# bleachbit.md
It is a graphical tool to delete unused files a packages on linux
