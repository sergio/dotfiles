---
Filename: nvim-frameworks.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [nvim, frameworks]
 vim:ft=markdown
---
## NvChad
+ [nvchad](https://github.com/NvChad)

## Neovim Essentials
+ [NeoVim_Essentials](https://github.com/Blovio/NeoVim_Essentials/)

A nice video about nvim configuration: https://www.youtube.com/watch?v=ajmK0ZNcM4Q

