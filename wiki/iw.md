---
file: iw.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
abstrac: This aims...
---

#  iw.md intro:

Show / Manipulate wireless devices and their configuration
+ https://wiki.voidlinux.org/Post_Installation

     sudo iw dev wlp7s0 scan | awk '/SSID/ {print $2}'

     iw dev | awk '$1=Interface {print $2}'

     ls /sys/class/ieee80211/*/device/net/
