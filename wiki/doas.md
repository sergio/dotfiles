---
File: doas.md
Author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [sudo, tools, admin]
---

# OpenDoas: a portable version of OpenBSD's doas command

script para configurar: /home/sergio/.dotfiles/bin/setdoas.sh 

doas is a minimal replacement for the venerable sudo. It was initially written by Ted Unangst of the OpenBSD project to provide 95% of the features of sudo with a fraction of the codebase.

# voidlinux install

    sudo xbps-install -Sy opendoas

#  doas.md intro:
+ https://www.reddit.com/r/linuxquestions/comments/lgfz2o/comment/gmr9beb/

Your last sudo use could be:

    sudo vim /etc/doas.conf

and add these lines:

    permit setenv { XAUTHORITY LANG LC_ALL } :wheel
    permit setenv { XAUTHORITY LANG LC_ALL } nopass sergio

+ https://flak.tedunangst.com/post/doas
Doas is much simpler and not configurable, and is generally good for
single-user boxes. Sudo is for boxes where not every user is root. Really,
there are two use-cases:

    some people like to use sudo to build elaborate sysadmin infrastructures
    with highly refined sets of permissions and checks and balances.

    some people like to use sudo to get a root shell without remembering two passwords

To get to an interactive shell with root prompt:

    $ doas -s

The owner and group for /etc/doas.conf should both be 0, file permissions should be set to 0400:

    # chown -c root:root /etc/doas.conf
    # chmod -c 0400 /etc/doas.conf

To check /etc/doas.conf for syntax errors, run:

    # doas -C /etc/doas.conf && echo "config ok" || echo "config error"

