---
File: ~/.dotfiles/wiki/systemd.md
Last Change: Tue, 09 Jul 2024 - 08:52:33
tags: [systemd]
---

# The the real motivation behind systemd

[Original link](https://unixdigest.com/articles/the-real-motivation-behind-systemd.html)

Published on 2018-05-01. Modified on 2023-10-28.

In this article we're going to take a close look at the real motivation behind the development of systemd, and we're going to take a look at some of the future perspectives for GNU/Linux as an operating system.

Update 2022-10-31: Things are not getting better, unsurprisingly. With Microsoft now at the leading role of the development of systemd and their brave new trusted boot world, combined with their takeover of so much open source infrastructure, this is slowly turning into the "covert take over" of the Linux world that no one ever wanted. It's seriously time to get back into community-driven development.
Table of Contents

    Introduction
    Fact 1: systemd is from Red Hat
    Fact 2: The primary reason for developing systemd is Red Hat's business interests in embedded devices
    Fact 3: No, it's not a myth, systemd is truly a huge monolith
    Fact 4: Privacy concerns
    Fact 5: Red Hat wants to become the next Microsoft Windows
    Fact 6: Red Hat needs other major Linux distributions to cooperate
    Consequences
    Final comments

Introduction

Personally I didn't have a problem with systemd in the beginning, when it was just a new init system. However, my problem with systemd today is that it has turned into a kind of Trojan horse. It is an attempt by Red Hat (IBM) to change the world of Linux in order to better serve their corporate interests.

While the Linux kernel, the GNU tools and the different major independent Linus distributions all started out as community driven projects, most of the current development in Linux world today is motivated by corporate interests, driven by developers sitting in different key positions in different companies, such as Intel, Microsoft, Red Hat, Google, Facebook and several others.

The Linux Foundation currently sponsors Linus Torvalds so he can work full-time on the Linux kernel, and he is still sitting at the wheel, but The Linux Foundation is slowly becoming "a shadow of Microsoft", just like The Open Source Initiative, where most of the money comes from Microsoft.

After Microsoft ran its "we love Linux" campaign, one board member from Microsoft was given a seat at The Linux Foundation. Then it was two board members and now three. It looks like a slow covert takeover in which the foundation no longer have any true community members left, it's mainly corporate employees. Furthermore, Microsoft also has a major influence at the "Technical Advisory Board of the Linux Foundation" where its current board members are trying to have Linux outsourced to Microsoft's GitHub.

Red Hat first released systemd an alternative init system. Then it suddenly turned into "a suite of software that provides fundamental building blocks for a Linux operating system". Red Hat then launched a campaign in order to influence all the other major Linux distributions and pressure them to adopt systemd.

The method deployed was that the systemd developers addressed several third party projects and tried to convince them to make their projects depend upon systemd, such as the attempts made by Lennart Poettering on the GNOME mailing list, and the attempt made by Red Hat developer "keszybz" on the tmux project. Most of these attempts were addressed as technical issues, however the long email correspondence on the GNOME mailing list (and elsewhere) shows that this was not the case.

In this thread it is discussed whether GNOME should be a Linux based OS only. Followed up by some significant objections in response.

Other tactics deployed by Red Hat was to hire developers from GNOME and other Linux distributions, such as Debian, and then have these people promote systemd.

One of the results of all of this was a huge uproar in the open source Linux community in which Debian Developer Joey Hess, Debian Technical Committee members Russ Allbery and Ian Jackson, and systemd package-maintainer Tollef Fog Heen resigned from their positions. All four justified their decision on the public Debian mailing list and in personal blogs with their exposure to extraordinary stress-levels related to ongoing disputes on systemd integration within the Debian and open source community that rendered regular maintenance virtually impossible.

In December 2014 a group calling themselves the "Veteran Unix Admins" announced a fork of Debian called Devuan that intends to provide a Debian variant without systemd. Devuan 1.0.0 was released on May 26, 2017.

    We believe this situation is also the result of a longer process leading to the take-over of Debian by the GNOME project agenda. Considering how far this has propagated today and the importance of Debian as a universal OS and base system in the distribution panorama, what is at stake is the future of GNU/Linux in a scenario of complete homogeneization and lock-in of all base distributions.

The latest invention by Lennart Poettering called systemd-homed is presented as a new way to handle home directories, whereas it really just is a way to get one step closer to eliminating /etc, which is something Red Hat has dreamed about for a long time.

Watch the FOSSDEM 2020 video where Poettering presents systemd-homed and notice how he criticizes the way full disk encryption is handled from the point of view that Linux is a multi-user system, yet at the same time he rejects at least five different challenges systemd-homed presents as irrelevant because, well, the laptop is really only used by one single person.

The fact is that the main development in Linux world, even in the kernel, has been almost completely hijacked by major companies. It is no longer mainly community driven development. Linux has become really big bucks for many corporations and they really want to control as much of the development as possible.

Let's take a look at some indisputable facts.
Fact 1: systemd is from Red Hat

Lennart Poettering and Kay Sievers who started the systemd project in 2010 where both Red Hat employees. Initially systemd was released as a new init system, but it has slowly grown into what Poettering describes as "a suite of software that provides fundamental building blocks for a Linux operating system". This is by design, not by coincidence.

The official reason for the development of systemd was described as:

    They wanted to improve the software framework for expressing dependencies, to allow more processing to be done concurrently or in parallel during system booting, and to reduce the computational overhead of the shell.

Fact 2: The primary reason for developing systemd is Red Hat's business interests in embedded devices

Red Hats primary business is in embedded devices, and the primary concerns addressed by systemd by design is embedded devices, such as the work towards removing /etc.

In an interview with Red Had CEO Jim Whitehurst he states:

    We partner with the largest embedded vendors in the world, particularly in the telecom and automotive industries where stability and reliability is the number one concern. They easily adapted to systemd.

Mentor Automotive has released their slides from a 2015 event. In these slides the many benefits provided by systemd to the embedded automotive market is fairly well explained. The reason why they "easily adapted to systemd" is because systemd is specifically designed to suit their needs.

The U.S. Military has been Red Hats biggest customer since 2002 and they have been a major source of motivation behind many of Red Hats decisions too.

In 2012 Lennart Poettering changed the systemd license from GPL to LGPL in order to better suit the embedded market.
Fact 3: No, it's not a myth, systemd is truly a huge monolith

In his blog post The Biggest Myths (can also be downloaded as PDF), from January 2013, Lennart Poettering argue against calling systemd a "monolith", which is what many people consider it to be. Lennart says:

    A package involving 69 individual binaries can hardly be called monolithic. What is different from prior solutions however, is that we ship more components in a single tarball, and maintain them upstream in a single repository with a unified release cycle.

The fact is however, that many of these so-called individual binaries has functionality that simply will not work without other systemd components. If we take a look at the man page for systemd-networkd it clearly states that if you set the option UseDNS as true the DNS servers received from the DHCP server will be used and take precedence over any statically configured ones. This corresponds to the nameserver option in /etc/resolv.conf. What the man page neglects to mention is that this setting (and multiple other settings) does not work without systemd-resolved. Other components of systemd are even more tightly integrated.
Fact 4: Privacy concerns

systemd-resolved has hard coded fallback DNS servers to Cloudflare, Quad9 and Google. Even if you turn these off, a bug might cause these to be used anyway (which actually happened at one point).
Fact 5: Red Hat wants to become the next Microsoft Windows

This is another major motivation at Red Hat and this is illustrated by Lennart Poetterings slides from FUDCON + GNOME Asia Beijing 2014. Go to page 15 and scroll slowly forward to page 19. Eventually you end up with the project objectives:

    Turn Linux from a bag of bits into a competitive General Purpose Operating System.
    Build the Internet's Next Generation OS.
    Unifying pointless differences between distributions.
    Bringing innovation back to the core OS.

Combined with the next set of slides that display the market Red Hat want to target:

    Desktop
    Server
    Container
    Embedded
    Mobile
    Cloud
    Cluster

Most of the added functionality that the different systemd modules provide is only added to make desktop systems like GNOME function like Microsoft Windows.
Fact 6: Red Hat needs other major Linux distributions to cooperate

If Red Hat was ever going to succeed in their long term plans for developing the "Internet's Next Generation OS" they knew they needed to somehow influence the other major Linux distributions. The reason for this is that if major Linux distribution like Debian, Ubuntu, Arch Linux, and others, was going to reject systemd, Red Hat wouldn't be able to proceed with their plans because too many third party projects simply wouldn't care about how Red Hat would like things to function.

This is very important to understand because many open source projects used to develop software with POSIX compatibility in mind. As such they try to make sure that their project compiles and works on several UNIX-like operating systems. This is something that isn't in the interests of Red Hats. As long as you have to consider other operating systems such as Solaris, FreeBSD, OpenBSD, NetBSD, etc., Linux is "held back" when compared to functionality in Microsoft Windows. Functionality such as automatic mounting and unmounting, simple privilege escalation, etc.

Another problem for Red Hat was that if the other major Linux distributions had rejected systemd, it would have become much more difficult for them to get systemd relevant changes and code pushed into the kernel. But when the other major distributions also adopted systemd, it became a lot easier.
Consequences

From a political point of view, one of the main problems with systemd is that its continued development is motivated by company interests and not the interests of the free and open source Linux community.

From a technical point of view, systemd has reached its tentacles deeply into many open source projects which slowly has made systemd a hard dependency (disregarding the existence of other open source operating systems). The result has been that many applications simply cannot run without systemd. This not only makes it very difficult to get such software running on an operating system like FreeBSD, but it also greatly effects other Linux distributions who maintain "init freedom", which is about restoring a sane approach to PID1 that respects portability, diversity and freedom of choice.

As a result of all of this, systemd breaks portability, ignores backwards compatibility, and replaces existing services. Many Linux distributions have given up on their preferred init system and adopted systemd - just so that they have a functioning OS. systemd has over 1.3 Million lines of code which might translate into a lot of security issues and/or bugs. For comparison, sinit has less than 200 lines of code and it does one job, and it does it well.

Another major problem is the previously mentioned hard coded DNS servers in systemd-resolved.

Lennart Poettering explained that the hard coded values should be there in case of catastrophic failure of configuration files, and a lack of DHCP on the network (the DNS fallback is changeable but requires a recompile). However, that's the "embedded developer" speaking. If a bug is found in the application that makes these DNS servers run even though you have disabled them, or if a race issue bug is found, you could be facing a serious privacy issue. Furthermore running with Cloudflare, Quad9, and Google DNS servers hard coded into the systemd code is deeply problematic as these companies are not only known for violating peoples privacy, but also because NSA has previously infiltrated Googles data centers, something revealed by the Snowden documents. Such settings should never be opt-out (where you have to remember to remove them), they should be opt-in, and definitely not the default options.

The way these issues are dealt with generally, and the extremely arrogant attitude of Lennart Poettering shows a complete disregard for user privacy and for the interests of the open source Linux community.
Final comments

It has surprised me that the initial discussion on the Debian mailing list somehow managed to only address SysVinit, Upstart and systemd. Nobody took a serious look at runit or s6. Not only are these systems much more aligned with the Unix philosophy, but they are also much more secure and easy to understand.

Casper Ti. Vectors post on the Gentoo forum, s6/s6-rc vs systemd, or why you probably do not need systemd, also shows that s6 is a better and in many ways superior solution to systemd.

Many people mistakenly think that each and every systemd component is independent, but that's just not true. Take a look at the code and the documentation and see the tight integration between many of these so-called modules.

Corporate politics, maneuvers and manipulations should have no place in the Linux world. While companies can be allowed to both use open source code, contribute code and also provide financial support from their earnings to projects, they should never be allowed such massive control as Red Hat and others now have in the Linux world.

Thank God for truly independent community driven projects because without those we would be left with rubbish like Microsoft Windows. On that note, take a look at:

    Open letter to the Linux World (by Christopher Barry). Also available here (TXT).
    OpenBSD is fantastic
    FreeBSD is an amazing operating system
    Technical reasons to choose FreeBSD over GNU/Linux
    Some of the great GNU/Linux distributions
    Lennart Poettering: BSD Isn't Relevant Anymore. Also available here (PDF).

If you have any comments or corrections please feel free to email them to me. Also, if you found any of the content on this website useful consider supporting me on Patreon ;)

##  outros links:

+ [Reddit](https://www.reddit.com/r/DistroHopping/comments/1ce6a0x/suggest_me_some_good_distros_without_the_evil/?tl=pt-br)
