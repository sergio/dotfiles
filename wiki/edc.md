---
File: ~/.dotfiles/wiki/edc.md
Last Change: Wed, 05 Jun 2024 - 07:54:30
tags: [tools]
---

# Every da carry

Everyday carry (EDC) or every-day carry is a collection of useful items that are consistently carried on person every day.

There are endless situations where you can have power shortage, floadings, snow
storms and so on.

1 - City edc
2 - Camping edc
3 - Surviving edc

[EDC kit](https://youtu.be/y4EhwA6ZHMQ?si=0YMazLBXXAfY4bll)
[More complete edc](https://youtu.be/bbciUxNPz1s)

01 - Lanterna
02 - Cortador de unha
03 - Linha de seda e agulha
04 - Filtro de água (portátil)
05 - Paracord (para amarrar coisas)
06 - Massarico ou pederneira (gerar fogo)
07 - Linha de pesca e anzol
08 - Esparadrapo e água oxigenada
09 - Canivete ou estilete
10 - Canivete suiço (contém ferramentas)
