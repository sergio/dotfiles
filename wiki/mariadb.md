# mariadb

https://sempreupdate.com.br/como-criar-um-novo-usuario-e-conceder-permissoes-no-mariadb-e-mysql/

---
CREATE USER 'sergio'@'localhost' IDENTIFIED BY '123';
GRANT ALL PRIVILEGES ON * . * TO 'sergio'@'localhost';
FLUSH PRIVILEGES;
```

## Pra fazer o login:

    sudo mysql --user=root mysql

     delete from mysql.db where user = 'jack' and then create a user

     minha senha está 123

# Criar banco de dados:

    create database nome;
    use nome_do_banco;

     create table usuarios {
      id
}

script para criar um banco de dados
de gestão de água:

```sh
```

## datagrip

    flatpak install flathub com.jetbrains.DataGrip

## Comandos

    show databases;
    create database [nomeDB];
    drop database [dbname];


    use [nomedb];

Mostrar todas as tabelas da respectiva base de dados:

    show tables;

Mostrar o formatos dos campos da tabela de uma base de dados:

    describe [nomeTabela];

Apagar uma tabela:

    drop table [nomeTabela];

Para sair da interface de administração do MariaDB:

  exit

# vim:set ft=markdown:tabstop=2:
