---
File: dunst.md
Author: Sergio Araujo
Last Change: Thu, 24 Nov 2022 - 11:02:31
 vim:ft=markdown
tags: [tools, notification]
---

# Instalation from github

    https://github.com/dunst-project/dunst

Then you must read the README file (install all dependencies)
run:

    make
    sudo make install

Lightweight and customizable notification daemon

    ln -sfvn /home/sergio/.dotfiles/dunst /home/sergio/.config

    dunstify -r 1234 "This is message 1"
    dunstify -r 1234 "This is message 2 replacing message 1"

## Control dunst notifications:

    dunstctl close ................. close the topmost notification
    dunstctl close-all
