---
File: ~/.dotfiles/wiki/contrato-de-locacao-comercial.md
Last Change: Mon, Jan 2025/01/27 - 17:24:53
tags: [contrato, aluguel]
---

# CONTRATO DE LOCAÇÃO COMERCIAL

**Entre**:
**[NOME COMPLETO DO LOCADOR]**, [nacionalidade], [estado civil], [profissão], portador do documento de identidade nº [XX], inscrito no CPF/MF sob o nº [XXX], residente e domiciliado em [endereço completo], doravante denominado **LOCADOR**;

**E**:
**[NOME COMPLETO DO LOCATÁRIO]**, [nacionalidade], [estado civil], [profissão], portador do documento de identidade nº [XX], inscrito no CPF/MF sob o nº [XXX], residente e domiciliado em [endereço completo], doravante denominado **LOCATÁRIO**.

As partes acima celebram o presente contrato, regido pelas cláusulas e legislação vigente.

---

## **CLÁUSULA 1ª – DO OBJETO**
O LOCADOR cede em locação ao LOCATÁRIO o imóvel comercial situado em **[endereço completo]**, matriculado sob nº **[número]** no Registro de Imóveis de **[cidade/estado]**, com área útil de **[XX m²]**, destinado exclusivamente para **[descrição da atividade comercial]**.

---

## **CLÁUSULA 2ª – DO PRAZO**
- Prazo de vigência: **[XX meses/anos]**, iniciando em **[data de início]** e terminando em **[data de término]**.
- Renovação: Mediante acordo prévio, comunicado por escrito com **[XX dias]** de antecedência.

---

## **CLÁUSULA 3ª – DO VALOR E REAJUSTE**
1. **Aluguel mensal**: R$ **[valor]** (por extenso), pago até o **[dia]** de cada mês via **[forma de pagamento]**.
2. **Reajuste**: Anual, baseado no **[índice (ex.: IGP-M)]** ou acordo entre as partes.

---

## **CLÁUSULA 4ª – DO DEPÓSITO CAUÇÃO**
- Valor do depósito: **[XX meses de aluguel]**, totalizando **R$ [valor]**.
- Restituição: Ao final do contrato, deduzidos débitos ou danos.

---

## **CLÁUSULA 5ª – DAS TAXAS E ENCARGOS**
1. **Condomínio**: Responsabilidade do LOCATÁRIO.
2. **IPTU**: Responsabilidade do LOCADOR.
3. **Consumos (água, luz, etc.)**: Responsabilidade do LOCATÁRIO.

---

## **CLÁUSULA 6ª – DAS OBRIGAÇÕES DO LOCATÁRIO**
1. Utilizar o imóvel apenas para a finalidade contratada;
2. Realizar pequenos reparos (lâmpadas, vazamentos);
3. Não sublocar sem autorização escrita;
4. Devolver o imóvel em bom estado de conservação.

---

## **CLÁUSULA 7ª – DAS OBRIGAÇÕES DO LOCADOR**
1. Entregar o imóvel em condições adequadas;
2. Responsabilizar-se por reparos estruturais;
3. Cumprir prazos e termos de renovação.

---

## **CLÁUSULA 8ª – DA RESCISÃO**
1. **Pelo LOCADOR**: Em caso de atraso superior a **[XX dias]** ou descumprimento contratual;
2. **Pelo LOCATÁRIO**: Se o imóvel tornar-se inabitável por culpa do LOCADOR;
3. **Rescisão amigável**: Mediante aviso de **[XX dias]** e multa de **[XX%]**.

---

## **CLÁUSULA 9ª – DISPOSIÇÕES FINAIS**
1. **Vistoria**: LOCADOR pode visitar o imóvel com aviso de **[XX dias]**;
2. **Alterações**: Apenas por escrito e com acordo mútuo;
3. **Foro**: Eleito o Foro de **[cidade/estado]** para questões judiciais.

---

**Local e Data**: **[Cidade]**, **[dia/mês/ano]**.

**Assinaturas**:
_____________________________
**[Nome do Locador]**
**LOCADOR**

_____________________________
**[Nome do Locatário]**
**LOCATÁRIO**

---

> **Observações**:
> - Anexar: Laudo de Vistoria, Comprovante de Depósito, etc.
> - Recomenda-se registro em Cartório de Notas.
