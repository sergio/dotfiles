---
File: /home/sergio/.dotfiles/wiki/surf.md
Last Change: Mon, 19 Feb 2024 - 14:05:58
tags: [browser, web, suckless]
 vim:ft=markdown
---

    surf url .................... open url
    ctrl + g .................... dmenu for opening urls
    ctrl + p .................... Ctrl+p: Go to URL in cut and paste clipboard.
    Ctrl+y: ..................... Put current website URL into cut and paste clipboard.

## opening suf with tabs:

    tabbed surf -pe

