---
file: openssl.md
author: Sergio Araujo
Last Change: Mon, 19 Feb 2024 - 14:05:58
 vim:ft=markdown
tags: [password, hack, security]
---

## References:
+ https://opensource.com/article/21/4/encryption-decryption-openssl

## Generate you key phrase:

    openssl genrsa -aes128 -out sergio_private.pem 2048

Extract your public key:

    openssl rsa -in sergio_private.pem -pubout > sergio_public.pem

## How to compress and protect a folder in Linux using a password

Use the tar command to compress the whole folder named dir1 in the current working directory:

    tar -cz dir1 | openssl enc -aes-256-cbc -e > dir1.tar.gz.enc

Hide it:

    mv -v dir1.tar.gz.enc .dir1.tar.gz.enc

Delete the original directory in Linux using the rm command:

    rm -rf dir1

To decrypt, run:

    openssl enc -aes-256-cbc -d -in dir1.tar.gz.enc | tar xz

## Encrypt a file:

    openssl rsautl -encrypt -inkey sergio_public.pem -pubin -in bit-ly.token -out bit.ly-token.enc

