---
File: ~/.dotfiles/wiki/hyprland.md
Last Change: Sun, 29 Dec 2024 - 09:22:28
tags: [linux, wm, desktop, compositor]
---

# intro

+ [youtube](https://youtu.be/2CP_9-jCV6A)
+ [Another video](https://youtu.be/-tCdeY1Jqk0)
+ [On github](https://github.com/Makrennel/hyprland-void.git)
+ [Rice](https://youtu.be/VP8oyN3Rkbs)
+ [Another typecract manuak](https://youtu.be/KA1jv40q9lQ)
+ [typecraft hyprland dotfiles](https://github.com/typecraft-dev/dotfiles)
+ [Another typecract manuak ep03](https://youtu.be/omhJMH9lPPc)
+ [Nice config](https://youtu.be/EujO_5KvCCo)
+ [hyprland void dots](https://github.com/void-land/hyprland-void-dots)
+ [Another rive](https://youtu.be/XMXdQCWmQn4)
+ [The ultimate hyprland guide](https://youtu.be/6La1VEWH-fw)
+ [codeberg repo](https://codeberg.org/Oglo12/hyprland-rice )
+ [Repo with configuration for archlinux](https://github.com/mylinuxforwork/dotfiles/wiki)

## Shortcuts

```text
super + m .......... exit
super + q .......... kitty
```

## config file

```sh
~/.config/hypr/hyprland.conf
```

## Discover you monitor

```sh
hyprctl monitors  all
hyprctl monitors | grep Monitor | awk '{print $2}'
```

## Install hyprland on voidlinux

+ [youtube video](https://www.youtube.com/watch?v=-tCdeY1Jqk0)
+ [Reddit thead](https://forum.puppylinux.com/viewtopic.php?t=11844)
+ [hyprland void dots](https://github.com/void-land/hyprland-void-dots)

```sh
#!/bin/bash

# Exit on errors
set -e

log_info() {
    echo -e "\e[32m[INFO]\e[0m $1"
}

# Step 1: Add Hyprland mirror
log_info "Adding Hyprland GitHub mirror..."
cat <<'SETMIRROR' > /etc/xbps.d/hyprland_mirror.conf
repository=https://raw.githubusercontent.com/Makrennel/hyprland-void/repository-x86_64-glibc
SETMIRROR

# Step 2: Update and sync package database
log_info "Updating package database..."
sudo xbps-install -Suy

# Step 3: Install Hyprland and dependencies
log_info "Installing Hyprland packages..."
xargs -n1 xbps-install -Suy <<< "hyprwayland-scanner tomlplusplus sdbus-cpp hyprutils hyprlang hyprcursor hyprlock hypridle hyprpaper hyprland-protocols hyprland-devel hyprland xdg-desktop-portal-hyprland"

# Step 4: Verify installation
log_info "Verifying installed packages..."
sudo xbps-query -Rs hypr

# Step 5: Add user to _seatd group
if ! groups | grep -q "_seatd"; then
    log_info "Adding user to _seatd group..."
    sudo usermod -aG _seatd $USER
    log_info "You will need to log out and log back in for the group change to take effect."
else
    log_info "User is already in the _seatd group."
fi

# Step 6: Enable required services
log_info "Enabling elogind and seatd services..."
sudo ln -s /etc/sv/elogind /var/service || true
sudo ln -s /etc/sv/seatd /var/service || true

log_info "Starting elogind and seatd services..."
sudo sv start elogind
sudo sv start seatd

# Step 7: Create a minimal Hyprland config
HYPR_DIR="$HOME/.config/hypr"
if [ ! -d "$HYPR_DIR" ]; then
    log_info "Creating minimal Hyprland config in $HYPR_DIR..."
    mkdir -p "$HYPR_DIR"
    cat <<EOF > "$HYPR_DIR/hyprland.conf"
# Example Hyprland configuration
monitor=,1920x1080@60,0x0,1
exec=hyprpaper
exec=wofi
exec=mako
EOF
else
    log_info "Hyprland configuration already exists. Skipping."
fi

log_info "Setup complete. Log out and log back in to start using Hyprland."
```

## keybindings

```sh
bind=$mainMod,mouse:272,exec,amongus
will bind it to SUPER + LMB.

bind=$mainMod,mouse_down,workspace,e-1
will jump to the previous workspace
```

## clipboard manager

    wl-clipboard

## keyboard  configuration

```sh
# https://wiki.hyprland.org/Configuring/Variables/#input
input {
    kb_layout = br
    kb_variant =
    kb_model = abnt2
    kb_options =
    kb_rules =

    follow_mouse = 1

    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.

    touchpad {
        natural_scroll = false
    }
}
```

