#!/bin/bash
#
# open-in-firefox.sh - open URL from Termux command line in Firefox Android browser
#
# Works with file:// URLs too, unlike with termux-open{-url}.
#

exec am start --user 0 -a android.intent.action.VIEW -n org.mozilla.firefox/.App -d "$1" >/dev/null

