```txt
Filename: README.md
Last Change: Tue, 23 Jan 2024 - 05:24:59
```

## Termux settings and tips:

**Needed programs**

    iproute2 openssh termux-services curl wget mpv mpg123
    cronie termux-api neovim python3 zsh zip unzip file fasd
    trash-cli x11-repo xorg-xauth termux-api xclip fd git
    tree fzf nodejs termimage silversearcher-ag lua53 luarocks perl zsh termux-exec
    jq

    pip3 install pipx
    pipx install yt-dlp oscclip

## keybindings (teclas de atalho:)

    Volume Down  -->  Ctrl
    Volume Up    -->  Esc

### font:
+ https://github.com/Iosevka-Mayukai/Iosevka-Mayukai/releases
+ https://www.reddit.com/r/termux/comments/k93jly/comment/gf1rua4/

Para setar a fonte tem que ter o nome "fonte" mesmo, essa que está na pasta
é a fonte Iosevka-Mayukai, que é muito bonita.

    cp font.ttf ~/.termux
    termux-reload-settings

## The HOME on termux:

    /data/data/com.termux/files/home --> $PREFIX

    If you want to edit your sshd_config type:
    $PREFIX/etc/ssh/sshd_config

## The scripts folder "bin":

Take a look at your ~/.bashrc if the bin folder appears in the path environment
variable:

There are some useful shorcuts in your ~/.inputrc

### Accessing your termux via nautilus:

    sftp://u0_a225@phone/data/data/com.termux/files/home
    sftp://u0_a225@192.168.0.100:8022/data/data/com.termux/files/home

## ssh:
Enable X11Forwarding on your $PREFIX/etc/ssh/sshd_config

    ssh -Y -p8022 192.168.0.46

## Enable some shortcuts:
+ https://wiki.termux.com/wiki/Terminal_Settings

Edit the file ~/.termux/termux-properties

```conf
# Open a new terminal with ctrl + t (volume down + t)
shortcut.create-session=ctrl + t

# Go one session down with (for example) ctrl + 2
shortcut.next-session=ctrl + 2

# Go one session up with (for example) ctrl + 1
shortcut.previous-session=ctrl + 1

# Rename a session with (for example) ctrl + n
shortcut.rename-session=ctrl + n
```

## Changing interface:

1 - On the scree, Long press on ...
2 - then press more
3 - style

