#!/bin/bash
# File: ~/.dotfiles/algorithm/shell/bash/clonerepo.sh
# Last Change: Sun, 19 Jan 2025 - 17:43:12
# Author: Sergio Araujo

# curl -sL https://bitbucket.org/sergio/dotfiles/raw/main/algorithm/shell/bash/clonerepo.sh | bash

# Configuração do log
LOG_DIR="${HOME}/.logs"
mkdir -p "$LOG_DIR"
LOG_FILE="${LOG_DIR}/clonerepo_$(date '+%Y%m%d_%H%M%S').log"
exec > >(tee -a "$LOG_FILE") 2>&1 || {
  echo "[ERROR] Unable to write to log file at $LOG_FILE. Check permissions."
  exit 1
}

echo "Iniciando script em $(date)"

# log_error() {
#   local message="$1"
#   echo "[ERRO] $(date '+%Y-%m-%d %H:%M:%S') - $message" >> "$LOG_FILE"
#   echo "[ERRO] $message"  # Exibe o erro no terminal também
# }

log_message() {
  local level="${1:-INFO}" # Default log level to INFO
  local message="$2"
  local formatted="[$level] $(date '+%Y-%m-%d %H:%M:%S') - $message"
  echo "$formatted" >> "$LOG_FILE" # Log to file
  echo "$formatted"               # Display in terminal
}

log_error() {
  log_message "ERROR" "$1"
}

if ! command -v xbps-install &> /dev/null; then
  log_error "Este script é destinado ao Void Linux."
  exit 1
fi

setdoas() {
  if ! command -v doas &> /dev/null; then
    echo "Installing opendoas..."
    sudo xbps-install -Sy opendoas || {
      log_error "Failed to install opendoas."
      return 1
    }
  fi

  if [ ! -f /etc/doas.conf ]; then
    echo "Configuring /etc/doas.conf..."
    sudo tee /etc/doas.conf > /dev/null <<-EOF
      permit setenv { XAUTHORITY LANG LC_ALL } :wheel
      permit setenv { XAUTHORITY LANG LC_ALL } nopass sergio
EOF
    [ $? -eq 0 ] || log_error "Failed to configure /etc/doas.conf."
  fi

  sudo chown root:root /etc/doas.conf && sudo chmod 0400 /etc/doas.conf || {
    log_error "Failed to set permissions for /etc/doas.conf."
    return 1
  }

  sudo doas -C /etc/doas.conf || log_error "Invalid /etc/doas.conf syntax."
  echo "opendoas setup complete!"
}


# Verificação de chaves SSH
[ ! -d ~/.ssh ] && {
  log_error "Diretório ~/.ssh não encontrado. Configure suas chaves SSH primeiro."
  exit 1
}

command -v doas && [ -f /etc/doas.conf ] && ELEVATE="doas" || ELEVATE="sudo"

pi() {
for pkg in "$@"; do
  if ! xbps-query -Rs "^$pkg\$" &>/dev/null; then
    echo "Installing $pkg..."
    $ELEVATE xbps-install -Sy "$pkg" || {
      log_error "Failed to install $pkg."
      return 1
    }
  else
    echo "$pkg is already installed."
  fi
done
}

installessential() {
  pi void-repo{,-nonfree} || {
    log_error "Falha ao configurar repositórios."
    return 1
  }
  $ELEVATE xbps-install -Suv || {
    log_error "Falha ao atualizar o sistema."
    return 1
  }
  pi zip git curl wget fzf gcc make fd the_silver_searcher xclip neovim xprop || {
    log_error "Falha ao instalar pacotes essenciais."
    return 1
  }
}

setarteclado() {
# Verifica se não está no WSL/Microsoft ou Wayland
if ! grep -qEi "(microsoft|wsl)" /proc/version && [ "$XDG_SESSION_TYPE" != "wayland" ]; then
  if xprop -root _XKB_RULES_NAMES | grep -qi 'abnt2'; then
    echo "O layout abnt2 já está ativo no ambiente gráfico."
  else
    echo "O layout abnt2 não está ativo. Configurando e adicionando ao arquivo de configuração do shell..."

    # Configura o layout do teclado
    setxkbmap -model abnt2 -layout br -option caps:escape || {
      log_error "Falha ao configurar o layout do teclado."
      return 1
    }

    # Determina o shell em uso e adiciona a configuração ao arquivo correto
    local shell_config
    case "$SHELL" in
      */bash)
        shell_config=~/.bashrc
        ;;
      */zsh)
        shell_config=~/.zshrc
        ;;
      *)
        log_error "Shell não suportado: $SHELL"
        return 1
        ;;
    esac

    # Adiciona a configuração ao arquivo do shell
    echo "setxkbmap -model abnt2 -layout br -option caps:escape" >> "$shell_config" || {
      log_error "Falha ao adicionar configuração ao $shell_config."
      return 1
    }

    echo "Configuração adicionada ao $shell_config."
  fi
else
  echo "Ambiente gráfico não suportado ou não detectado. Ignorando configuração do teclado."
fi
}

clonerepositorio() {
  if [ ! -d ~/.dotfiles ]; then
    git clone --jobs=8 git@bitbucket.org:sergio/dotfiles.git ~/.dotfiles || {
      log_error "Falha ao clonar o repositório."
      return 1
    }
    cd ~/.dotfiles && git fetch --recurse-submodules --jobs=8 || {
      log_error "Falha ao buscar submodules."
      return 1
    }
  fi
}

fixfirefoxfonts() {
  $ELEVATE ln -s /usr/share/fontconfig/conf.avail/70-no-bitmaps.conf /etc/fonts/conf.d/ || {
    log_error "Falha ao criar link simbólico para fontes do Firefox."
    return 1
  }
  $ELEVATE xbps-reconfigure -f fontconfig || {
    log_error "Falha ao reconfigurar fontconfig."
    return 1
  }
}

gitandsymlinks() {
  ssh -T git@bitbucket.org || {
    log_error "Falha ao conectar ao Bitbucket via SSH."
    return 1
  }
  var=$(echo 3gmail.com@ | sed 's,\(^3\)\(gmail\.com\)\(\@\),voyeg\1r\3\2,g')
  git config --global fetch.parallel 8
  git config --global user.name voyeg3r
  git config --global merge.tool vimdiff
  git config --global user.email "${var}"
  git config --global alias.last 'log -1 HEAD'
  git config --global credential.helper 'cache --timeout=3600'
  git config --global push.default simple
  git config --global alias.hist 'log --pretty=format:"%h %ad | %s%d [%an]" --graph --date=short'
  git config --global core.autocrlf input
  git config --global core.eol lf
  git config --global core.pager less -FRSX
  git config --global init.defaultbranch main
  git config --global color.branch auto
  git config --global color.diff auto
  git config --global color.interactive auto
  git config --global color.status auto
  git config --global color.ui auto

  ln -sfvn ~/.dotfiles/zsh ~/.config/
  ln -sfvn ~/.dotfiles/zsh/zshenv ~/.zshenv
  ln -sfvn ~/.dotfiles/bash/bashrc ~/.bashrc
  ln -sfvn ~/.dotfiles/bash/inputrc ~/.inputrc
  ln -sfvn ~/.dotfiles/bash/bash_profile ~/.bash_profile
  ln -sfvn ~/.dotfiles/xinitrc ~/.xinitrc
  ln -sfvn ~/.dotfiles/rofi ~/.config
  ln -sfvn ~/.dotfiles/polybar ~/.config
  ln -sfvn ~/.dotfiles/bspwm ~/.config
  ln -sfvn ~/.dotfiles/sxhkd ~/.config
  ln -sfvn ~/.dotfiles/Xresources ~/.Xresources
  ln -sfvn ~/.dotfiles/detox/detoxrc ~/.detoxrc
  ln -sfvn /home/sergio/.dotfiles/mpv/mpv.conf ~/.config/mpv/mpv.conf
  ln -sfvn ~/.dotfiles/agignore ~/.agignore
  # ln -sfvn ~/.dotfiles/ignore ~/.ignore
  ln -sfvn ~/.dotfiles/gemrc ~/.gemrc
  ln -sfvn ~/.dotfiles/curl/curlrc ~/.curlrc
  ln -sfvn ~/.dotfiles/fontconfig ~/.config/fontconfig
  ln -sfvn ~/.dotfiles/bcrc ~/.config/bcrc
  ln -sfvn ~/.dotfiles/aria2 ~/.config/aria2
  [ -d ~/.config/youtube-dl ] || mkdir -p ~/.config/youtube-dl
  ln -svfn ~/.dotfiles/youtube-dl.config ~/.config/youtube-dl/config
  ln -sfvn ~/.dotfiles/yt-dlp ~/.config
  ln -sfvn ~/.dotfiles/backgrounds "${XDG_PICTURES_DIR:-$HOME/img}"/backgrounds
  ln -sfvn ~/.dotfiles/awesome ~/.config
  ln -sfvn ~/.dotfiles/hidden ~/.hidden
  ln -sfvn ~/.dotfiles/elinks ~/.config
  ln -sfvn ~/.dotfiles/profile ~/.profile
  ln -sfvn ~/.dotfiles/asound.state ~/.config/asound.state
  ln -sfvn ~/.dotfiles/inkscape/templates ~/.config/inkscape/templates
  ln -sfvn ~/.dotfiles/dradio ~/.config
  ln -sfvn ~/.dotfiles/sxiv ~/.config
  ln -sfvn ~/.dotfiles/zsh ~/.config
  ln -sfvn ~/.dotfiles/starship/starship.toml ~/.config/starship.toml
  ln -sfvn ~/.dotfiles/redshift ~/.config
}

installfonts(){
  curl -fsSL https://raw.githubusercontent.com/getnf/getnf/main/install.sh | bash
  "$HOME/.local/bin/getnf" -i IosevkaTerm ProggyClean
}

configgtk() {
  [[ -d ~/.config/gtk-3.0 ]] && (
    mv ~/.config/gtk-3.0{,-backup} || {
      log_error "Falha ao fazer backup do diretório gtk-3.0."
      return 1
    }
    ln -sfvn ~/.dotfiles/gtk-3.0 ~/.config || {
      log_error "Falha ao criar link simbólico para gtk-3.0."
      return 1
    }
  )
}

linkluachek() {
  [[ ! -d "$XDG_CONFIG_HOME/luacheck/" ]] && {
    ln -sfvn ~/.dotfiles/luacheck "$XDG_CONFIG_HOME" || {
      log_error "Falha ao criar link simbólico para luacheck."
      return 1
    }
  }
}

linktermux() {
  if [[ $(uname -o) == "Android" ]]; then
    if [[ $(readlink ~/.termux) != "$HOME/.dotfiles/termux" ]]; then
      ln -sfvn ~/.dotfiles/termux ~/.termux || {
        log_error "Falha ao criar link simbólico para termux."
        return 1
      }
    fi
  fi
}

dwmautostart() {
  DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}"
  [ ! -d "$DATA_HOME/dwm" ] && (
    mkdir -p "$DATA_HOME/dwm" || {
      log_error "Falha ao criar diretório dwm."
      return 1
    }
    ln -sfvn "$HOME/.dotfiles/dwm/autostart.sh" "$DATA_HOME/dwm/autostart.sh" || {
      log_error "Falha ao criar link simbólico para autostart.sh."
      return 1
    }
  )
}

redshiftset() {
  if ! command -v redshift &> /dev/null; then
    echo "Redshift não está instalado. Instalando..."
    pi redshift || {
      log_error "Falha ao instalar o Redshift."
      return 1
    }
  fi

  [ ! -d "$HOME/.config/autostart" ] && mkdir -p "$HOME/.config/autostart"
  ln -sfvn ~/.dotfiles/redshift/redshift.desktop ~/.config/autostart/redshift.desktop || {
    log_error "Falha ao criar link simbólico para redshift.desktop."
    return 1
  }
}

enableprecommit() {
  cat <<- 'EOF' > ~/.dotfiles/.git/hooks/pre-commit
  #!/bin/sh
  # Filename: ~/.dotfiles/.git/hooks/pre-commit
  # Last Change: Thu, 10 Nov 2022 - 12:16
  # vim:nolist conceallevel=0 softtabstop=4 shiftwidth=4 tabstop=4 expandtab ft=sh:
  # References:
  # https://toroid.org/git-last-modified
  # https://mademistakes.com/notes/adding-last-modified-timestamps-with-git/

      # NOTE:
      # This script does not have any guarantee that it
      # will work correctly, make sure
      # you do all the necessary tests and
      # backups to avoid any hassle

      modified=$(git diff --cached --name-only --diff-filter=ACMR)

      [ -z "$modified" ] && exit 0

      for f in $modified; do
        # Get the last modification date of the file
        data=$(date -r "$f" "+%a, %d %b %Y - %T")

        # Adjust the date format for the touch command
        ttime=$(date -r "$f" "+%Y%m%d%H%M.%S")

        # Update the "Last Change" line in the file
        sed -ri "1,7s/^(\S*\s*Last (Change|Modified):).*/\1 $data/gi" "$f"

        # Preserve the original modification timestamp
        touch -t "$ttime" "$f"

        # Re-add the file to the staging area
        git add "$f"
      done
      EOF

  chmod +x ~/.dotfiles/.git/hooks/pre-commit || {
    log_error "Falha ao tornar o hook pre-commit executável."
    return 1
  }
}

main() {
  [ ! -d ~/.ssh ] && {
    log_error "Diretório ~/.ssh não encontrado. Configure suas chaves SSH primeiro."
    exit 1
  }

  setdoas || log_error "Erro ao configurar o doas."
  installessential || log_error "Erro durante a instalação de pacotes essenciais."
  clonerepositorio || log_error "Erro ao clonar o repositório."
  gitandsymlinks || log_error "Erro ao configurar Git e links simbólicos."
  setarteclado || log_error "Erro ao configurar o teclado."
  fixfirefoxfonts || log_error "Erro ao corrigir fontes do Firefox."
  installfonts || log_error "Erro ao instalar fontes."
  configgtk || log_error "Erro ao configurar GTK."
  linkluachek || log_error "Erro ao configurar luacheck."
  linktermux || log_error "Erro ao configurar Termux."
  dwmautostart || log_error "Erro ao configurar autostart do DWM."
  redshiftset || log_error "Erro ao configurar Redshift."
  enableprecommit || log_error "Erro ao configurar pre-commit hook."

  echo "Script concluído com sucesso em $(date)" >> "$LOG_FILE"
}

main "$@"

