#!/usr/bin/bash
# Last Change: Sun, 15 Oct 2023 - 14:26:34
# tags: [rofi, script, dmenu]

# Define o diretório de imagens
# Se $XDG_PICTURES_DIR estiver definido, usa ele. Caso contrário, usa ~/img.
PICTURES_DIR="${XDG_PICTURES_DIR:-$HOME/img}"

# Verifica se o diretório existe
if [ ! -d "$PICTURES_DIR" ]; then
  echo "Diretório não encontrado: $PICTURES_DIR"
  exit 1
fi

cd "$PICTURES_DIR" || exit 1

# Lista os arquivos e usa o rofi para selecionar um
chosen=$(ls | rofi -dmenu)

# Se o usuário cancelar a seleção (por exemplo, pressionar Esc), encerra o script
if [ $? -ne 0 ]; then
  exit 1
fi

# Abre a imagem selecionada com o sxiv
sxiv "$chosen"
