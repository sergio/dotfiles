-- File: ~/.dotfiles/luacheck/.luacheckrc
-- Last Change: Mon, 12 Feb 2024 - 21:36:06

-- You can get luacheck to recognize the vim global by putting this
-- configuration in ~/.luacheckrc (or $XDG_CONFIG_HOME/luacheck/.luacheckrc):

globals = {
  {"vim", "api" },
}
