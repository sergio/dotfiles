## script for listening radio stations

It uses dmenu or fzf for playing radio stations

URL: https://bitbucket.org/sergio/dradio/src/master/

    ln -sfvn ~/.dotfiles/dradio ~/.config

It reads stations from ~/.config/dradio/stations.txt
