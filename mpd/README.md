---
File: README.md
Lang: pt-BR
Author: Sergio Araujo
Last Change: Thu, 23 Sep 2021 - 21:39
Created: Thu, 23 Sep 2021 - 21:39
Tags: [mpd, music]
---

## mpd settings

    $XDG_CONFIG_HOME/mpd/mpd.conf

