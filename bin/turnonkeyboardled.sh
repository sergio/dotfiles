#!/bin/bash
# File: ~/.dotfiles/bin/turnonkeyboardled.sh
# Last Change: Sun, 29 Dec 2024 - 10:08:50
# Author: Sergio Araujo
# tags: [keyboard, led, script, bright]

# Define full paths to commands
BRIGHTNESSCTL="/usr/bin/brightnessctl"
DOAS="/usr/bin/doas"

# Check if the user wants to list devices
if [ "$1" == "--list" ]; then
  echo "Available devices:"
  $BRIGHTNESSCTL -l
  exit 0
fi

# Find the device number for the scroll lock LED
DEVICE=$($BRIGHTNESSCTL -l | grep -Po '(?<=input)[0-9]+(?=::scrolllock)')

# Check if the device was found
if [ -z "$DEVICE" ]; then
  echo "Scroll lock LED device not found!"
  echo "Install brightnessctl"
  exit 1
fi

# Set the scroll lock LED to 1
$DOAS $BRIGHTNESSCTL --device="input${DEVICE}::scrolllock" set 1

echo "Scroll lock LED set on device $DEVICE"

