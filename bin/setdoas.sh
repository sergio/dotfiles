#!/bin/bash
# File: /home/sergio/.dotfiles/bin/setdoas.sh 
# configurar opendoas doas
# tags: [opendoas, doas, sudo]

# Verifica se o opendoas está instalado
if ! command -v doas &> /dev/null; then
  echo "Instalando opendoas..."
  sudo xbps-install -Sy opendoas
fi

# Verifica se o arquivo /etc/doas.conf existe
if [ ! -f /etc/doas.conf ]; then
  echo "Configurando /etc/doas.conf..."
  sudo tee /etc/doas.conf > /dev/null <<-EOF
  permit setenv { XAUTHORITY LANG LC_ALL } :wheel
  permit setenv { XAUTHORITY LANG LC_ALL } nopass sergio
EOF
fi

# Define o proprietário e o grupo do arquivo /etc/doas.conf como root
sudo chown -c root:root /etc/doas.conf

# Define as permissões do arquivo /etc/doas.conf para 0400
sudo chmod -c 0400 /etc/doas.conf

# Verifica a sintaxe do arquivo /etc/doas.conf
if sudo doas -C /etc/doas.conf; then
  echo "Configuração do doas está OK."
else
  echo "Erro na configuração do doas."
  exit 1
fi

# Informa ao usuário que a configuração foi concluída
echo "Configuração do opendoas concluída com sucesso!"
