#!/bin/sh
# File: /home/sergio/.dotfiles/bin/lockscreen.sh
# Last Change: Wed, 05 Oct 2022 19:23:23
# Reference: https://dev.to/ccoveille/xautolock-corners-5161
# tags: [lock, lockscreen, x11, screen, screensaver, i3lock, dusnt]

set -e

# enable the lock screen
i3lock -c 272d2d -i "$XDG_PICTURES_DIR/backgrounds/YkVkRGU.jpg"

