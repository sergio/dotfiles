#!/bin/bash
# File: ~/.dotfiles/bin/hyprland-change-wallpaper.sh
# Last Change: Sun, 29 Dec 2024 - 09:30:48
# Author: Sergio Araujo
# source: https://github.com/hyprwm/hyprpaper/issues/108

directory=$XDG_PICTURES_DIR/wallpapers
monitor=`hyprctl monitors | grep Monitor | awk '{print $2}'`

if [ -d "$directory" ]; then
  random_background=$(ls $directory/* | shuf -n 1)

  hyprctl hyprpaper unload all
  hyprctl hyprpaper preload $random_background
  hyprctl hyprpaper wallpaper "$monitor, $random_background"
fi

