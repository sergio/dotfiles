#!/usr/bin/bash
# File: ~/.dotfiles/bin/432.sh
# Last Change: Tue, 11 Jun 2024 - 12:59:16

# Batch convert all .mp3 files in the current directory to 432Hz with ffmpeg

# Options
suffix="false" # Append the -432Hz suffix or not

oldIFS=$IFS
IFS=$'\n'
set -f # Deal with blanks and special characters in file names of the file command and in for loop

found=($(find . -name "*.mp3")) # Find the files in the current directory

IFS=$oldIFS
set +f
filename=""

for file in "${found[@]}"; do # Iterate the found files 
  # Math: 
  # We want to convert from 441Hz to 432Hz, the difference is 9Hz which are 2,040816327 % of 441Hz.
  # Parameters:
  # asetrate contains the desired pitch (frequency) but changes the playback speed
  # atempo ajusts the resulting playback speed, which should remain the same
  # aresample keeps the pitch change by correct rate of (re)sampling
  mv "$file" "$file.tmp"

  if $suffix == "true"; then
    filename="${file%.mp3}-432Hz.mp3"
  else
    filename="$file"
  fi

  ffmpeg -loglevel 8 -i "$file.tmp" -af asetrate=43200,atempo=1.00,aresample=43200 "$filename"
  rm "$file.tmp"
  echo "Pitched $filename"
done
