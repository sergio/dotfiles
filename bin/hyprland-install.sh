# File: ~/tmp/hyprland-install.sh
# Last Change: Wed, Jan 2025/01/01 - 09:30:18
# Author: Sergio Araujo
#

imissing() {
  # Install packages from a list (only ones missing)
  for dep in "$@"; do
    if ! xbps-query -l | grep -q "^ii ${dep}-[0-9]"; then
      echo "${dep} is not installed. Installing..."
      sudo xbps-install -Sy "${dep}"
    else
      echo "${dep} is already installed."
    fi
  done
}

imissing hyprland{,-scanner,-devel} hyprlang hyprutils hyprcursor hypridlea hyprpaper hyprlock sdbus-cpp xdg-desktop-portal-hyprland hyprland-protocols dbus seatd polkit elogind mesa-dri wlogout xdg-utils wl-clipboard

sudo ln -s  /etc/sv/polkitd /var/service
sudo ln -s  /etc/sv/seatd /var/service
sudo ln -s  /etc/sv/dbus /var/service

sudo usermod -aG _seadd sergio

