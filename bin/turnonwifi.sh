#!/bin/bash
# File: ~/.dotfiles/bin/turnonwifi.sh
# Last Change: Mon, Jan 2025/01/06 - 18:40:16
# Author: Sergio Araujo

# Esse script foi criardo com a ajuda da IA chinesa deepseek

NEEDED_COMMANDS="dunst nmcli"
missing_counter=0
for needed_command in $NEEDED_COMMANDS; do
  if ! hash "$needed_command" >/dev/null 2>&1; then
    printf "Command not found in PATH: %s\n" "$needed_command" >&2
    ((missing_counter++))
  fi
done
if ((missing_counter > 0)); then
  printf "Minimum %d commands are missing in PATH, aborting" "$missing_counter" >&2
  exit 1
fi

# Função para enviar notificações
notify() {
  notify-send "Wi-Fi Manager" "$1"
}

# Verifica se a conexão "Sergionet" está ativa
if nmcli connection show --active | grep -q "Sergionet"; then
  notify "Sergionet já está ativa."
else
  notify "Conectando à Sergionet..."
  if nmcli connection up Sergionet; then
    notify "Conectado à Sergionet com sucesso."
  else
    notify "Falha ao conectar à Sergionet. Tentando conectar à brisa-2800536..."
    if nmcli connection up brisa-2800536; then
      notify "Conectado à brisa-2800536 com sucesso."
    else
      notify "Falha ao conectar à brisa-2800536."
    fi
  fi
fi
