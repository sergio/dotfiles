#!/bin/bash
# Last Change: Mon, 17 Oct 2022 19:19:25
# vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab ft=sh:
#
# source: https://www.youtube.com/watch?v=XWlbaERuDP4
# https://github.com/ericmurphyxyz/dotfiles/blob/master/.local/bin/changevolume

# Configuração de variáveis de ambiente
export PATH=/usr/bin:/bin:/usr/local/bin
export DISPLAY=:0
export XAUTHORITY=$HOME/.Xauthority

function send_notification() {
  volume=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print int($2 * 100)}')
  dunstify -a "changevolume" -u low -r "9993" -h int:value:"$volume" -i "volume-$1" "Volume: ${volume}%" -t 2000
}

function adjust_volume() {
  local direction="$1"
  local step=5%

  # Unmute before adjusting volume
  wpctl set-mute @DEFAULT_AUDIO_SINK@ 0

  # Adjust volume
  if [[ $direction == "up" ]]; then
    wpctl set-volume @DEFAULT_AUDIO_SINK@ ${step}+
  elif [[ $direction == "down" ]]; then
    wpctl set-volume @DEFAULT_AUDIO_SINK@ ${step}-
  else
    echo "Invalid direction: $direction"
    exit 1
  fi

  # Get current volume
  volume=$(wpctl get-volume @DEFAULT_AUDIO_SINK@ | awk '{print int($2 * 100)}')

  # Ensure volume is not below 0%
  if (( volume < 0 )); then
    wpctl set-volume @DEFAULT_AUDIO_SINK@ 0%
    volume=0
  fi

  send_notification $direction
}

case $1 in
  up)
    adjust_volume "up"
    ;;
  down)
    adjust_volume "down"
    ;;
  mute)
    wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
    if wpctl get-volume @DEFAULT_AUDIO_SINK@ | grep -q "MUTED"; then
      dunstify -i volume-mute -a "changevolume" -t 2000 -r 9993 -u low "Muted"
    else
      send_notification up
    fi
    ;;
  *)
    echo "Usage: $0 {up|down|mute}"
    exit 1
    ;;
esac
