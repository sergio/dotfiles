#!/usr/bin/env bash
# shortener.sh - encurtador de URLs
# source: https://meleu.sh/url-shortener/

# TODO: o token abaixo é inválido! use o seu token!
readonly BITLY_TOKEN=$(< ~/docs/bitly/bitly-token.txt)
readonly BITLY_ENDPOINT='https://api-ssl.bitly.com/v4/shorten'

shortener() {
  local long_url="$1"
  while [[ -z "$long_url" ]]; do
    read -r -p "Digite a url: " long_url
  done
  curl -s \
    --header "Authorization: Bearer ${BITLY_TOKEN}" \
    --header "Content-Type: application/json" \
    --data "{\"long_url\":\"${long_url}\"}" \
    "${BITLY_ENDPOINT}" \
    | jq -r 'if .link == null then .description else .link end'
}

shortener "$@"

