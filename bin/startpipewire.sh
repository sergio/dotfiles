#!/bin/bash
# File: ~/.dotfiles/bin/startpipewire.sh
# Last Change: Sun, Jan 2025/01/05 - 10:52:43
# Author: Sergio Araujo

# Script para iniciar o PipeWire corretamente
# Autor: Sua Assistência de IA :)
# source: https://chat.deepseek.com/a/chat/s/58b89f1e-4254-451f-a567-f76585bcea9d 

# Definir o diretório de runtime do usuário
export XDG_RUNTIME_DIR=/run/user/$(id -u)
export PIPEWIRE_RUNTIME_DIR="$XDG_RUNTIME_DIR/pipewire"

# Função para parar um processo se ele estiver em execução
stop_process() {
  local process_name=$1
  if pgrep -x "$process_name" > /dev/null; then
    echo "Parando $process_name..."
    pkill -x "$process_name"
    sleep 1  # Dar um tempo para o processo parar completamente
  else
    echo "$process_name não está em execução."
  fi
}

# Criar o diretório de runtime do PipeWire, se não existir
if [ ! -d "$PIPEWIRE_RUNTIME_DIR" ]; then
  echo "Criando diretório de runtime do PipeWire em $PIPEWIRE_RUNTIME_DIR..."
  mkdir -p "$PIPEWIRE_RUNTIME_DIR"
  chmod 700 "$PIPEWIRE_RUNTIME_DIR"
  chown "$(id -u):$(id -g)" "$PIPEWIRE_RUNTIME_DIR"
else
  echo "Diretório de runtime do PipeWire já existe: $PIPEWIRE_RUNTIME_DIR"
fi

# Parar o PipeWire se já estiver em execução
stop_process "pipewire"

# Iniciar o PipeWire
echo "Iniciando o PipeWire..."
pipewire &
sleep 1

# Parar o pipewire-pulse se já estiver em execução
stop_process "pipewire-pulse"

# Iniciar o pipewire-pulse
echo "Iniciando o pipewire-pulse..."
pipewire-pulse &
sleep 1

# Verificar se o PipeWire está em execução
if pactl info >/dev/null 2>&1; then
  echo "PipeWire iniciado com sucesso!"
  echo "Detalhes do servidor:"
  pactl info
else
  echo "Erro: PipeWire não foi iniciado corretamente."
  exit 1
fi
