Filename: README.md
Last Change: Wed, 05 Oct 2022 13:23:40
tags: [icons, dunst, svg, inkscape, notifications]

## Dunst accepts svg icons

After some research I have reached a nice dunst configuration,
you can use these files under the GNU3 licence terms.

Contact:

    voyeg3r at gmail
    https://dev.to/voyeg3r

