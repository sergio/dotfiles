Filename: README.md
Last Change: Mon, 23 Jan 2023 - 16:40:26
tags: [tint2, openbox]

# tint2-themes
My Favorite Tint2 Themes for Openbox


Other resources:
+ https://terminalroot.com.br/2021/12/os-12-melhores-temas-para-seu-tint2.html
---

# English
To see the image/screenshot of each theme, go to:
### [The 12 Best Themes for your Tint2](https://en.terminalroot.com.br/12-best-themes-for-your-tint2/)

---

# Português
Para ver a imagem/captura de tela de cada um dos temas, acesse:
### [Os 12 Melhores Temas para seu Tint2](https://terminalroot.com.br/2021/12/os-12-melhores-temas-para-seu-tint2.html)

---

## COMO INSTALAR OS TEMAS

Logicamente você precisará possuir o Tint2 instalado, use o gerenciador de pacotes da da sua distro, exemplo para sistemas que utilizam APT:

    sudo apt install tint2

Em seguida crie um diretório dentro de ~/.config na sua pasta pessoal:

    mkdir -p ~/.config/tint2

Depois é só clonar o repositório, que separei todos previamente, e mover para ~/.config/tint2/:

    git clone https://github.com/terroo/tint2-themes
    cd tint2-themes
    mv * ~/.config/tint2/

Para testar use o comando tint2 com o parâmetro -c e o caminho do arquivo de configuração do Tint2(tint2rc), exemplo:

    tint2 -c ~/.config/tint2/livia/livia.tint2rc

Se quiser habilitar(facilita sua vida!) para sempre que iniciar a sessão no Openbox, adicione ao seu ~/.config/openbox/autostart a linha:

    tint2 -c ~/.config/tint2/livia/livia.tint2rc &

