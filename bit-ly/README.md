# File: /home/sergio/.dotfiles/bit-ly/README.md
  Last Change: Sun, 16 Oct 2022 13:42:26

You need a bit.ly token in order to use
the bit.ly url shortner.

In our case we have a script called "short"
that reads your bit.ly token using openssl,
because our bit.ly token is encripted.

The script is:

    ~/.dotfiles/bin/short

