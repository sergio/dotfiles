# File: /home/sergio/.dotfiles/zsh/zlogin
# Last Change: Wed, 12 Jan 2022 12:30
#
# ln -sfvn ~/.dotfiles/zsh/zlogin ~/.zlogin
#
# source: https://github.com/Eriner/zim
# startup file read in interactive login shells
#
# The following code helps us by optimizing the existing framework.
# This includes zcompile, zcompdump, etc.
#

(
# Function to determine the need of a zcompile. If the .zwc file
# does not exist, or the base file is newer, we need to compile.
# These jobs are asynchronous, and will not impact the interactive shell
zcompare() {
    if [[ -s ${1} && ( ! -s ${1}.zwc || ${1} -nt ${1}.zwc) ]]; then
        zcompile ${1}
    fi
}

autoloaded=${ZDOTDIR:-${HOME}}/autoloaded
setopt EXTENDED_GLOB

  # zcompile the completion cache; siginificant speedup.
  for file in ${ZDOTDIR:-${HOME}}/.zcomp^(*.zwc)(.); do
      zcompare ${file}
  done

  # zcompile .zshrc
  zcompare ${ZDOTDIR:-${HOME}}/.zshrc

  # # zcompile some light module init scripts
  # zcompare ${zim_mods}/git/init.zsh
  # zcompare ${zim_mods}/utility/init.zsh
  # zcompare ${zim_mods}/pacman/init.zsh
  # zcompare ${zim_mods}/spectrum/init.zsh
  # zcompare ${zim_mods}/completion/init.zsh
  # zcompare ${zim_mods}/fasd/init.zsh

  # zcompile all .zsh files for all autoloaded functions
  for file in ${autoloaded}/**/^(README.md|*.zwc)(.); do
      zcompare ${file}
  done

  for file in ${ZDOTDIR:-${HOME}}/plugins/**/*(.); do
      if [[ $file =~ ^(README.md|.*\.zwc)$ ]]; then
          zcompare "$file"
      fi
  done

  ) &!
