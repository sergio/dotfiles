
# Created by `pipx` on 2023-09-09 19:20:40
export PATH="$PATH:/home/sergio/.local/bin"

if [ -z $XDG_RUNTIME_DIR ]; then
  XDG_RUNTIME_DIR="/tmp/$(id -u)-runtime-dir"
  mkdir -pm 0700 $XDG_RUNTIME_DIR
  export XDG_RUNTIME_DIR
fi
