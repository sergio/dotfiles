# File: /home/sergio/.dotfiles/zsh/zsh-aliases
# Last Change: Sun, 02 Feb 2025 - 15:36:26

# (( $+commands[doas] )) && [ -f /etc/doas.conf ] && alias sudo="$(which doas)"
# estou usando a variável $ELEVATE mais adiante que é mais seguro por não mexer no sudo

# Check if the system is Void Linux
if [ -f /etc/os-release ] && grep -q 'void' /etc/os-release; then
  # Define a variável para o comando de elevação de privilégios

(( $+commands[doas] )) && [ -f /etc/doas.conf ] && ELEVATE="doas" || ELEVATE="sudo"

  # Basic xbps aliases
  alias xupdate="$ELEVATE xbps-install -Su"               # Update all installed packages
  alias xinstall="$ELEVATE xbps-install -y"               # Install a package
  alias xremove="$ELEVATE xbps-remove -y"                 # Remove a package
  alias xsearch='xbps-query -Rs'                          # Search for a package
  alias xlist='xbps-query -l'                             # List installed packages
  alias xinfo='xbps-query -p'                             # Show detailed package info

  alias xdeps='xbps-query -d'                             # Show dependencies of a package
  alias xrevdeps='xbps-query -r'                          # Show reverse dependencies

  # System maintenance commands
  alias xclean="$ELEVATE xbps-remove -O"                  # Clean the xbps cache
  alias xorphan='xbps-query -n'                           # List orphaned packages
  alias xcleanorphans="$ELEVATE xbps-remove -O"           # Remove orphaned packages
  alias xinstalled='xbps-query -l -v'                     # Detailed list of installed packages

  # Package management shortcuts
  alias xupgrade="$ELEVATE xbps-install -Su --update --upgrade"  # Upgrade a specific package
  alias xinstallall="$ELEVATE xbps-install -y"                   # Install package and dependencies
  alias xstatus='xbps-query -p'                                  # Check the status of a package installation
  alias xdowngrade="$ELEVATE xbps-install -d"                   # Downgrade a package

  # Custom convenience aliases
  alias xsinfo='neofetch'                                       # Show system info with neofetch
  alias xupclean="$ELEVATE xbps-install -Su && $ELEVATE xbps-remove -O"  # Update and clean package cache
  alias xinstallprompt="$ELEVATE xbps-install -y --ask"         # Prompt before installing package

  alias upgrade="(){ echo \"Running -> $ELEVATE xbps-install -Suv\"; $ELEVATE xbps-install -Suv; }"
  alias pu="(){ echo \"(update) Running -> $ELEVATE xbps-install -Suv\"; $ELEVATE xbps-install -Suv; }"
  # alias pi="$ELEVATE xbps-install -Sy"
  alias pq="xbps-query -Rs"
  alias pr="$ELEVATE xbps-remove -R"

  pi() {
    # Install packages from a list (only ones missing)
    for dep in "$@"; do
        if ! xbps-query -l | grep -q "^ii ${dep}-[0-9]"; then
            echo "${dep} is not installed. Installing..."
            $ELEVATE xbps-install -Sy "${dep}"
        else
            echo "${dep} is already installed."
        fi
    done
  }
fi

(( $+commands[xclip] && ! $+commands[termux-clipboard-get] )) && {
    alias pbpaste='xclip -i -selection clipboard -o'
    alias pbcopy='xclip -selection clipboard'
    alias selpaste='xclip -selection primary -o'
    alias selcopy='xclip -selection primary'
}

(( ! $+commands[xclip] && $+commands[termux-clipboard-get] )) && {
  alias pbpaste='termux-clipboard-get'
  alias pbcopy='termux-clipboard-set'
}

alias j='z'
alias f='zi'
alias g='lazygit'
alias zsh-update-plugins="find "$ZDOTDIR/plugins" -type d -exec test -e '{}/.git' ';' -print0 | xargs -I {} -0 git -C {} pull -q"

alias nb='newsboat'
alias lz="export NVIM_APPNAME=lazyvim; export MYVIMRC=~/.config/lazyvim/init.lua; nvim"
alias lazy='export NVIM_APPNAME=lazy; export MYVIMRC=~/.config/lazy/init.lua; nvim'
alias nv="export NVIM_APPNAME=nv; export MYVIMRC=~/.config/nv/init.lua; nvim"

alias vim=nvim
alias lv='(){(export NVIM_APPNAME=lv;export MYVIMRC=~/.config/lv/init.lua;nvim)}'
alias ne="nvim ~/.config/nvim/init.lua"

alias elinks='elinks --config-dir ~/.config/elinks/config'

# I had a problem on ~/.irgnore file
alias playmp3='fd -e mp3 -0 | xargs -0 mpv'

# alias python3.10='export PATH=$HOME/opt/python-3.10.1/bin:$PATH && python3.10'
# alias python='python3.10'

# Record audio
# alias recaudio="parec --monitor-stream="$(pacmd list-sink-inputs | awk '$1 == "index:" {print $2}')" | opusenc --raw - $(xdg-user-dir MUSIC)/recording-$(date +"%F_%H-%M-%S").opus"
#alias lunar='nvim -u ~/.local/share/lunarvim/lvim/init.lua --cmd "set runtimepath+=~/.local/share/lunarvim/lvim"'

# # get fastest mirrors
# alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
# alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
# alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
# alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Colorize grep output (good for log files)
alias grep='grep --color=auto'
alias egrep='grep -E --color=auto'
alias fgrep='fgrep --color=auto'
alias gh='history|grep -ri'
alias gril='grep -irl'
alias cpv='rsync -ah --info=progress2'

# easier to read disk
alias df='df -h'     # human-readable sizes
alias free='free -m' # show sizes in MB

# get top process eating memory
alias psmem='ps auxf | sort -nr -k 4 | head -5'

# get top process eating cpu ##
alias pscpu='ps auxf | sort -nr -k 3 | head -5'

# gpg encryption
# verify signature for isos
alias gpg-check="gpg2 --keyserver-options auto-key-retrieve --verify"
# receive the key of a developer
alias gpg-retrieve="gpg2 --keyserver-options auto-key-retrieve --receive-keys"

# # For when keys break
# alias archlinx-fix-keys="sudo pacman-key --init && sudo pacman-key --populate archlinux && sudo pacman-key --refresh-keys"

# alias mach_java_mode="export SDKMAN_DIR="$HOME/.sdkman" && [[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh""

alias ls='ls --color=auto'
alias sl=ls
#alias ls='ls -A'

# https://github.com/jarun/advcpmv
# see also on my wiki ~/.dotfiles/wiki/cpmv.md
# (( $+commands[advcp] )) && alias cp='/usr/local/bin/advcp -g'
# (( $+commands[advmv] )) && alias cp '/usr/local/bin/advmv -g'

# some aliases to fix 'command not found'
# alias ls='/bin/ls'
# alias mkdir='/bin/mkdir'
alias mv='mv -i'

# set abnt2 keymap
# alias abnt2='setxkbmap -model abnt2 -layout br'
# search radios: https://streamurl.link/
# in the command bellow -C -> cvs-exclude -u -> update
# alias git1='git clone --max-depth=1 '
# alias gitc='(){git clone --max-depth=1 $(xclip -selection clipboard);}'

alias git='LANG=en_US git'
# alias timer='(){for i in $(seq $((${1:-1} * 60)) -1 1) ; do echo -ne "\r$i " ; sleep 1 ; done}'

# alias gsend='(){ git commit -am "$1" && git pull --rebase --quiet      && git status | grep -q "Your branch is ahead" && git push; }'
alias gsend='(){ git commit -am "$1" && git pull --rebase --quiet && git push; }'

alias lcommit='(){git log -1 --pretty=%B 2>/dev/null || echo "You must be in git repo to run this command!";}'
alias lc='(){ git rev-parse HEAD 2>/dev/null && { tee >(xclip -i -selection clipboard); } || echo "Not in a git repository" ;}'

alias gl='git ls-tree --full-tree --name-only -r HEAD | tree --fromfile .'

# youtube-dl fork with some fixes
alias youtube-dl='yt-dlp'
alias yd='yt-dlp'
alias ya='(){yt-dlp -x --audio-format mp3 -o "%(title)s.%(ext)s" $(pbpaste);}'
alias updates='/home/sergio/.dotfiles/algorithm/shell/bash/xbps-updates'
# alias syncdot='rsync -auvzC --exclude="fonts.zip" sergio@void:~/.dotfiles ~/.dotfiles'
alias abnt2='setxkbmap -model abnt2 -layout br -variant abnt2'
alias dotfiles='cd ~/.dotfiles'
alias config='cd ~/.config'
alias wk='cd ~/.dotfiles/wiki'
alias bspcdesk='bspc query -D -d --names'
# alias st='/usr/local/bin/st -g=90x20 -f "Iosevka Mayukai Original:size=12:antialias=true:autohint=true"'
# alias st='/usr/local/bin/st -g 90x20 -f "iosevka:size=15:antialias=true:autohint=true:alpha=0.80"'
# alias st='/usr/local/bin/st'
# alias st='st -f SourceCodePro-Regular:size=12:antialias=true:autohint=true'
# alias st='st -f IBMPlexMono-Regular:size=13:antialias=true:autohint=true'
alias terminal="ps -o 'cmd=' -p $(ps -o 'ppid=' -p $$)"

# alias pip=pip3
alias size='du -h –max-depth=1'
#alias getwallpaper='gsettings get org.gnome.desktop.background picture-uri | sed "s,file://,,g"'
alias nettest='ping -c1 www.google.com  &> /dev/null && echo "Network ok" || doas sv t NetworkManager'
alias netrestart='ping -c1 www.google.com  &> /dev/null && echo "Network ok" || doas sv t NetworkManager'

# alias ipandroid="(){doas arp-scan --interface=wlp6s0 --localnet --plain | awk '/a0:ac:69:a9:5c:2a/ {print $1}';}"
alias ipandroid="nmap -sP 192.168.1.0/24 >/dev/null && arp -an | grep 'fe:5d:93:7a:6e:76'  | grep -Po '(?<=\D)192.168.1.\d+'"
#  wlp7s0

alias android="ssh -Y -p8022 android"
alias a32='ssh -p 8022 u0_a409@192.168.3.207'

# https://linuxdicasesuporte.blogspot.com.br/2018/03/alternativa-ao-gksu-no-debian-e.html?spref=fb
# alias pkexec='pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY'
(( $+commands[pkexec] )) && alias pkexec='pkexec env DISPLAY=$DISPLAY XAUTHORITY=$XAUTHORITY'

if (( $+commands[trash] )); then
  trash() { trash-put "$@"; }
elif (( $+commands[gio] )); then
  trash() { gio trash "$@"; }
else
  echo "Neither trash-cli nor gio is installed. Please install one of them."
fi

alias rm='echo use trash instead!'

# https://github.com/salman-abedin/devour
# (( $+commands[devour] )) && alias mpv="devour mpv"

alias mpv-cava='mkfifo /tmp/mpv-fifo 2>/dev/null; mpv --ao=pcm --ao-pcm-file=/tmp/mpv-fifo "$@" & cava -p /dev/stdin < /tmp/mpv-fifo; rm /tmp/mpv-fifo'

# https://askubuntu.com/a/63203/3798
# https://www.cnx-software.com/2016/12/16/compress-decompress-files-faster-with-lbzip2-multi-threaded-version-of-bzip2/
# (( $+commands[lbzip2] )) && alias tar='tar --use-compress-program=lbzip2'

# vim $(bat --config-file)
#(( $+commands[bat] )) && alias cat='bat'

# (( $+commands[vman] )) || vman() { man $* | col -b | vim -c 'set ft=man nomod nolist' -; } && alias man=vman

# simpler find command
# (( $+commands[fd] )) && alias fd=/bin/fd

# tmux aliases
alias tls='tmux ls'
alias t='tmux attach || tmux new-session'
alias tl='tmux list-sessions'
alias tman='tmux attach -t tmuxmanual || tmux new -s tmuxmanual'

alias pubkey="cat ~/.ssh/id_rsa.pub | pbcopy && echo '=> Pulblic key copied to clipboard'"

### Show hidden files ##
##alias l.='ls -d .* --color=auto'


# Check if the system is Artix Linux (Arch-based)
if [ -f /etc/os-release ] && grep -qi 'artix' /etc/os-release; then
  alias pu='sudo pacman -Syu'
  alias pi='sudo pacman -S'
  alias pr='sudo pacman -R'
  alias pq='pacman -Ss'
  alias plist='pacman -Q'
  alias pinfo='pacman -Qi'
  alias pdeps='pactree -u'
  alias pclean='sudo pacman -Sc'
  alias porphan='pacman -Qdtq'
  alias pcleanorphans='sudo pacman -Rns $(pacman -Qdtq)'
  alias pinstalled='pacman -Qe'
  alias pupgrade='sudo pacman -Syyu'
  alias psinfo='neofetch'
  imissing() {
    for dep in "$@"; do
      if ! pacman -Q "${dep}" &>/dev/null; then
        echo "${dep} is not installed. Installing..."
        sudo pacman -S --noconfirm "${dep}"
      else
        echo "${dep} is already installed."
      fi
    done
  }
fi

# alias lfm='(){awk "/^map/ {print \$0}" ~/.dotfiles/lf/lfrc  | rofi -dmenu }'
alias myenv="source  $HOME/.virtualenvs/neovim/bin/activate; cd ~/.config/nvim"

## https://askubuntu.com/a/590030/3798
# alias python='/usr/bin/python3'
# alias ae="$EDITOR $ZDOTDIR/aliases; source $ZDOTDIR/aliases"
# alias ae="nvim $ZDOTDIR/zsh-aliases; source $ZDOTDIR/zsh-aliases"
alias ae="nvim $ZDOTDIR/zsh-aliases; source $ZDOTDIR/zsh-aliases"
alias arch='uname -m'

# https://github.com/cirala/lfimg
alias lf="~/.dotfiles/bin/lfrun"

# Fuzzy (fzy) nvim edit file
# alias nfu='nvim $(find ~/.dotfiles -type f | fzy)'
# alias ff='vim $(find ~/.dotfiles -type f | fzy)'
# alias vimf='nvim $(find ~ -type f | fzy)'
alias vimscratch="vim -c 'setlocal bt=nofile bh=wipe nobl noswapfile nu'"

alias html2pdf='wkhtmltopdf'
# alias roteador='awk "/UG/ {print \$2}" <<(route -n)'
alias roteador='awk "/default/ {print \$3}" <<(ip route show)'
alias router='awk "/default/ {print \$3}" <<(ip route show)'

# source: https://www.reddit.com/r/linux/comments/3ly2zl/lpt_use_python_interpreter_as_an_advanced/
alias calc='python3 -ic "from math import *; import cmath"'

alias xterm='xterm -rv'
alias xterm='xterm -bg black -fg green'
alias xterm='xterm -fn 7x13 -fa "Liberation Mono:size=14:antialias=false"'
# alias calc='noglob calc'
alias shell='echo ${SHELL:t}'

alias ag='ag --path-to-ignore ~/.ignore'

# zsh dependent
alias -g ND='*(/om[1])' # newest directory
alias -g NF='*(.om[1])' # newest file
# alias -g NF{1,}='*(.om[1])'  # newest file
# alias -g NF{1,}A='*(.om[1]:a)'  # newest file with path

alias youindex="youtube-dl -o '%(playlist_index)02d-%(title)s.%(ext)s'"
alias youtube-srt='youtube-dl --write-srt --sub-lang --no-playlist en '
alias youtube-mp3='(){youtube-dl -x --audio-format mp3 "$1";}'
alias y3='(){yt-dlp --no-playlist -f "ba" -x --audio-format mp3 -o "%(title)s.%(ext)s" $(xclip -i -selection clipboard -o);}'

alias pip3upgrade='(){pip3 install --upgrade "$1"}'

# increase gnome-shell sound
alias upsnd='pactl set-sink-volume 0 +10%'
alias fullsnd='pactl set-sink-volume 0 +100%'
alias downsnd='pactl set-sink-volume 0 -10%'

# https://gitlab.com/linuxdabbler/dotfiles/.config/nvim
# ln -sfvn ~/.dotfiles/vinone ~/.config
if [[ -d "$HOME/.config/vinone" ]]; then
    [[ ! `readlink "$HOME/.config/vinone"` =~ '.*dotfiles.vinone$' ]] && ln -sfvn ~/.dotfiles/vinone ~/.config
	alias vinone='(){(export NVIM_APPNAME=vinone;export MYVIMRC=~/.config/vinone/init.lua;nvim "$@")}'
else
	alias vinone="nvim -u NONE -U NONE -N -i NONE -c 'set mouse=a| syntax on| set nu'"
fi

# alias vinone="vim -u NONE -N -c 'set nu'"
alias golf='vim -N -u ~/.dotfiles/vim/vimgolfrc'
alias znone='zsh -f'
# alias sensiblevim='vim -u ~/.dotfiles/nvim/sensible.vim'

alias battery='cat /sys/class/power_supply/BAT1/capacity'

alias mkdir='mkdir -pv'

## Colorize the grep command output for ease of use (good for log files)##
alias grep='grep --color=auto'
alias egrep='grep -E --color=auto'
alias fgrep='fgrep --color=auto'
alias hg='history | grep $1'
# alias mkcd='foo(){mkdir -p "$1"; cd "$1"}; foo'
alias mkcd='(){mkdir -p "$1"; cd "$1"}'
alias histg='(){ history -500 | grep "$1" ;}'
alias alive='echo 31999730615 | tee pbcopy'
alias ssh='ssh -Y'

# using trans (translator)
(( $+commands[trans] )) && {
    alias pt='(){trans -b en:pt "$1";}'
    alias en='(){trans -b pt:en "$1";}'
}

# alias lowercasename="zmv '*' '${(L)f}'"

# zsh profile editing
#alias zshrc='vi ~/.zshrc ; reload'
alias reload="exec -l $SHELL"
alias ze='export LC_ALL=en_US.UTF-8;vim $ZDOTDIR/.zshrc; echo "reloading zshrc" && source $ZDOTDIR/.zshrc'
# o alias abaixo gera conflito com o pyenv
#alias venv='export LC_ALL=en_US.UTF-8;vim ~/.zshenv'
alias zr="source $ZDOTDIR/.zshrc"
alias zenv="vim ~/.zshenv && source ~/.zshenv"
alias xterm='xterm -fn 7x13 -fa "Liberation Mono:size=14:antialias=false"'
alias zdot="cd $ZDOTDIR"

# pipe a download file to standard output on shell
alias wgetn='wget -O - -o /dev/null'
# to fix utf-8 filenames, see more here: https://stackoverflow.com/a/26941707/2571881
alias wget='wget --restrict-file-names=nocontrol'
#alias wget='wget --remote-encoding=UTF-8'
alias getsinglepage='wget -E -H -k -K -nd -N -p -P pagedir'
alias wgetfox='wget --user-agent="Mozilla/5.0 (X11; Linux i686; rv:78.0) Gecko/20100101 Firefox/78.0"'
# alias firefox='firefox --setHomePage "https://duckduckgo.com"'
alias fs='firefox -P sergio &'

#alias lynx='lynx -display_charset=utf-8'
alias lynx='lynx -display_charset=utf-8 -useragent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1"'
alias lynx='lynx -accept_all_cookies'

# alias vi='LC_ALL=en_US;/usr/bin/nvim'

# in zsh I have a binding ctrl + alt + l to call lvim
# bindkey -s '^[^L' 'lvim^M'
alias lvim='vim -c "normal '\''0"'
alias n='nvim -c "Telescope oldfiles"'
alias nhl='nvim --headless -c "Lazy sync" -c "quit"'

alias lunarvim="/home/sergio/.local/bin/lvim"
# edit the output of the last command
# alias vlast="(){fc -l -1 | awk '{$1="";print $0}'}"
# alias lvim='vim -c call setpos(".", getpos("'\""))'

#alias lvim='nvr -cc "normal '\''0" -s'
#alias lnvim="nvim -c':e#<1'"
alias vimessential='vim -u ~/.dotfiles/vim/essential.vim'
alias compilalivro='make clean; make && make show'
alias 4shared='cadaver http://webdav.4shared.com/'
alias trashurl="awk '/trashurl/ {\$1=\"\"; print}' ~/.dotfiles/wiki/dicasfirefox.md | pbcopy"

alias mpv='mpv --no-audio-display'

# mpv flatpak version
# alias mpv="flatpak run io.mpv.Mpv"

## optical character recognition OCR
(( $+commands[tesseract-ocr] )) && alias tesseract=tesseract-ocr

# buscar mais rádios:
# https://www.radios.com.br/busca/?q=fortaleza&qfilter=completo&pg=2
alias verdinha='mpv http://srv4.zoeweb.tv:1935/z18-live/stream/playlist.m3u8'
alias jovempan='mpv https://r10.ciclano.io:15037/stream/1/'
alias ceararural='mpv http://stm3.xcast.com.br:8560/stream'
alias bestbalads='mpv http://strm112.1.fm/onelive_mobile_mp3\?aw_0_req.gdpr\=true'
alias magic80='mpv http://strm112.1.fm/magic80_mobile_mp3\?aw_0_req.gdpr\=true'
alias adorejazz='mpv http://strm112.1.fm/ajazz_mobile_mp3\?aw_0_req.gdpr\=true'
alias birds='mpv http://strm112.1.fm/brazilianbirds_mobile_mp3\?aw_0_req.gdpr\=true'
alias vivaosamba='mpv http://74.63.237.84:8020/live'
alias foxnews='mpv https://streaming-ent.shoutcast.com/foxnews'
alias npr='mpv https://npr-ice.streamguys1.com/live.mp3'
alias uspfm='mpv http://143.107.8.106:8002/stream'
alias novabrasil='mpv http://playerservices.streamtheworld.com/api/livestream-redirect/NOVABRASIL_SPAAC_SC.aac'
alias esoterica='mpv http://192.99.247.236:8080/\;'
alias bossabrasil='mpv https://tunein.com/radio/Rdio-Bossa-Jazz-Brasil-s182075/'
alias tempofm='mpv http://r13.ciclano.io:8609/live'
alias chicago='mpv http://relay.broadcastify.com:80/il_chicago_police2\?xan\=7C80215A2A84B01D154525F05BD80095'
# Listen to Air Traffic Control, used to be scripts.
# alias miami='mpv https://www.broadcastify.com/scripts/playlists/1/3246/-6204126684.m3u'
alias miami='mpv https://broadcastify.cdnstream1.com/25051'
alias atlanticosul='mpv http://142.44.230.74:9026/live'
alias 80fm='mpv http://192.198.204.194:9020/listen.pls\?sid\=1\&t\=.pls'
alias all80s='mpv http://5.63.151.52:7194/listen.pls\?sid\=1\&t\=.pls'

alias itatiaia='mpv https://8903.brasilstream.com.br/stream'
alias radiodombosco='mpv http://stream.7br.com.br:10124/;'
alias flagradio='mpv http://ice9.securenetsystems.net/WZFG'
alias thepoint='mpv http://icy3.abacast.com/capitalcity-wqxlam-64'
alias adicted='mpv http://208.77.21.33:11730/;'
alias smolthjazz='mpv http://us3.internet-radio.com:8007/;stream'
alias megaton='mpv http://us2.internet-radio.com:8443/;stream'
alias infowars='mpv http://50.7.79.20/;'
alias scifioldrtime='mpv https://onlineradiobox.com/json/us/scifioldtime/play'
alias amazingtale='mpv http://rosetta.shoutca.st:8462/;stream1'
alias roswellufotalks='mpv http://streaming.shoutcast.com/TheUFOChannel'
alias cryptoradio='mpv http://station.kryptonradio.com:8000/stream'
alias ustoday='mpv http://tunein.streamguys1.com/usatoday'
alias calmradiobossa='mpv https://tunein.com/radio/Calm-Radio---Bossa-Nova-s192198/'
alias calmradiobarch='mpv https://tunein.com/radio/Calm-Radio---JS-Bach-s177684/'
alias sompb='mpv https://tunein.com/radio/Rdio-S-MPB-s280422/'
alias absolute80='mpv https://tunein.com/radio/Absolute-Radio-80s-Podcast-p484804/'
alias swissclassic='mpv https://stream.srg-ssr.ch/m/rsc_de/mp3_128'
alias inconfidenciafm='mpv https://tunein.com/radio/Rdio-Inconfidncia-AM-880-s99393/'
alias bluebossanova='mpv https://tunein.com/radio/Rdio-Blue-Bossa-Nova-s234676/'
alias culturafm='mpv https://tunein.com/radio/Rdio-Cultura-FM-1033-s2786/'
alias radiofraternidade='mpv https://tunein.com/radio/Web-Rdio-Fraternidade-s145235/'
# alias novabrasil='mpv https://opml.radiotime.com/Tune.ashx\?id\=s6996'
alias novabrasil='mpv https://playerservices.streamtheworld.com/api/livestream-redirect/NOVABRASIL_FORAAC.aac'

alias espiritismonet='mpv https://tunein.com/radio/Web-Rdio-Espiritismo-Net-s209070/'
alias radioespiritismo='mpv https://tunein.com/radio/Rdio-Espiritismo-s284524/'
alias radiodimensaoespirita='mpv https://tunein.com/radio/Rdio-Dimenso-Esprita-s195796/'
alias chorinho='mpv http://stm27.adcast.com.br:9380/;radiosu.aac'

alias gst='git status --short^M'
alias agf="(){git status --short | awk '$1 ~ /^\?\?/  {print \"git add \",$2}' | sh;}"
# files edited in the last two days
alias glast='git log --pretty=format: --name-only --since="2 days ago" | sed "/^$/d" | sort | uniq'
alias glog='git log --oneline -1'

# Miami International Airport (KMIA) Scanning Approach, Departure and Ground Control
alias miami='mpv http://relay.broadcastify.com/r5dywmx82pcg706.mp3'
alias miami2='mpv http://relay.broadcastify.com:80/970805198\?xan\=7C80215A2A84B01D154525F05BD80095'
# alias miami='mpv https://www.radios.com.br/players/listen-radio.m3u?radio=2758'
alias northLosAngelis='mpv http://relay.broadcastify.com:80/g35bnmpf8wc2\?xan\=7C80215A2A84B01D154525F05BD80095'
alias southLosAngelis='mpv http://relay.broadcastify.com:80/q1zxhgrdwt0p\?xan\=7C80215A2A84B01D154525F05BD80095'

alias classic='mpv http://80.237.154.83:8120'
# watch aljazeera
#
# alias alj='mpv https://english.streaming.aljazeera.net/aljazeera/english2/index101.m3u8'
alias alj='mpv https://english.streaming.aljazeera.net/aljazeera/english2/index4147.m3u8'

# search new radio stations
# https://www.internet-radio.com/
alias scifiold='mpv http://198.178.123.2:8652/listen.pls\?sid=1'
alias oldies104='mpv http://176.31.248.14:15696/listen.pls\?sid=1'
alias cbs='mpv https://cbsnhls-i.akamaihd.net/hls/live/264710-b/CBSN_mdialog/prodstream/master_360.m3u8'
# alias cbsnews='mpv http://cbsnewshd-lh.akamaihd.net/i/CBSNHD_7@199302/index_2200_av-p.m3u8'
alias cbsnews='mpv http://198.178.123.20:10746/listen.pls\?sid\=1'
alias apnews='mpv http://apnews.streamguys1.com/apnews'

alias dubaione='mpv http://dmi.mangomolo.com:1935/dubaione/smil:dubaione.smil/playlist.m3u8'
alias genesistv='mpv http://201.144.184.98:1935/genesis/smil:television.smil/chunklist_w1290068780_b410000_sleng.m3u8'
alias miami18='mpv http://mexicoserver.miamitvchannel.com/miamitv/smil:miamitvmexico/playlist.m3u8'
alias qvc='mpv http://qvclvp2.mmdlive.lldns.net/qvclvp2/9aa645c89c5447a8937537011e8f8d0d/manifest.m3u8'
alias rthdusa='mpv http://rt-usa-live.hls.adaptive.level3.net/rt/usa/index2500.m3u8'
alias tvaparecida='mpv http://caikron.com.br:1935/tvaparecida/tvaparecida.stream/playlist.m3u8'
alias cbs47='mpv http://cmghlslive-i.akamaihd.net/hls/live/224714/WJAX/master.m3u8'
alias discovery='mpv http://thepk.co:2095/live/denzuka/zukabumi13/255.ts'

# troque o c pelo b pra entender
alias hco='mpv http://freeeucache1.streamlive.to/live/qzbbu26gcvjxksz/chunks.m3u8\?nimblesessionid\=379679\&wmsAuthSign\=c2VydmVyX3RpbWU9Ny85LzIwMTcgNToxMDowMyBQTSZoYXNoX3ZhbHVlPWZyL1hXYjFqcUZ1WVN3YTAxVmxDcHc9PSZ2YWxpZG1pbnV0ZXM9MTAyMCZzdHJtX2xlbj0yMA\=\='

alias cmm='mpv http://37.48.70.138:8088/live/CNN_livestreaming2017/playlist.m3u8\?id\=71244\&wowzasessionid\=3312082838\&pk\=ec9ae024c0c1697ed8d3e762eef60770553be1a95f24b63f28ec1adea8d63340'
alias cnnnewsusa='streamlink -p mpv https://1861340594.rsc.cdn77.org/ls-54548-1/tracks-v1a1/mono.m3u8 worst'
alias bloonberg='streamlink https://www.bloomberg.com/live/us best'

alias fmovies1='mpv http://freeeucache1.streamlive.to/live/hc992q7x7cz86t9/chunks.m3u8\?nimblesessionid\=381576\&wmsAuthSign\=c2VydmVyX3RpbWU9Ny85LzIwMTcgNjowMzo0NSBQTSZoYXNoX3ZhbHVlPXlvM2JKSzBucmh0em93ZXFrekZ1WVE9PSZ2YWxpZG1pbnV0ZXM9MTAyMCZzdHJtX2xlbj0yMA\=\='

alias rtnews='mpv https://www.rt.com/on-air/'
alias rtnewsaudio='mpv --no-video https://www.rt.com/on-air/'
alias rtamerica='mpv https://rt-usa-live-hls.secure.footprint.net/rt/usa/index800.m3u8'

# some radios using mpv
alias bbc="mpv -playlist http://peace.str3am.com:6810/live-96k.mp3.m3u"
alias klara='mpv  http://mp3.streampower.be/klara-high.mp3'
alias radio4='mpv http://icecast.omroep.nl/radio4-bb-mp3'
#alias wnyc="mpv https://am820.wnyc.org/wnycam-web"
# alias wnyc="mpv http://fm939.wnyc.org/wnycfm-tunein.aac"
# alias wnyc="mpv http://wnycfm.streamguys.com/wnycfm.aac"
alias wnyc="mpv https://am820.wnyc.org/wnycam-web"
alias wfiu="mpv -playlist http://hannibal.ucs.indiana.edu:8000/wfiu_hi.m3u"
# alias ufm="mpv http://200.129.35.230:8081/;?d="
alias bandnewsfm='mpv http://evpp.mm.uol.com.br:1935/band/bandnewsfm_for/chunklist_w1799096750.m3u8'
alias ufm='mpv http://200.129.35.230:8081/ouvir'
alias 98bh='mpv https://9554.brasilstream.com.br/stream'
alias 98degrees='mpv https://www.internet-radio.com/servers/tools/playlistgenerator/?u=http://5.189.142.165:8108/listen.pls?sid=1&t=.m3u'
alias assuncao='mpv https://www.radios.com.br/play/playlist/23116/listen-radio.m3u'
alias lasmexicanas='mpv http://62.210.203.78:7064/stream'
alias varietylovesongs='mpv http://173.249.0.141:8024/stream'
alias romance='mpv http://146.71.124.10:8008/;'
alias actualradio='mpv http://uk3.internet-radio.com:8198/listen.pls&t=.pls'
alias 80soundtracks='mpv http://uk5.internet-radio.com:8256/listen.pls\&t\=.pls'
alias variadas='mpv http://45.43.200.34:14740/;'
alias assembleiafm='mpv http://srv6.zoeweb.tv:1935/z405-live/stream/playlist.m3u8'
alias tempofm='mpv http://r13.ciclano.io:8609/stream'
alias news="mpv http://minnesota.publicradio.org/tools/play/streams/news.pls" # MPR News
#alias npr='mpv http://www.npr.org/templates/dmg/dmg.php\?getProgramStream\=true\&NPRMediaPref\=WM'
alias current="mpv http://minnesota.publicradio.org/tools/play/streams/the_current.pls" # The Current
alias classical="mpv http://minnesota.publicradio.org/tools/play/streams/classical.pls" # Classical MPR
alias jazz='mpv http://streamerepsilon.jazz.fm:8000'
alias beatles='mpv http://uplink.duplexfx.com:8062'
alias localcurrent="mpv http://minnesota.publicradio.org/tools/play/streams/local.pls" # Local Current
alias heartland="mpv http://minnesota.publicradio.org/tools/play/streams/radio_heartland.pls" # MPR Radio Heartland
alias wonderground="mpv http://wondergroundstream2.publicradio.org/wonderground" # MPR Wonderground Windows Media
alias choral="mpv http://choralstream1.publicradio.org/choral.m3u" # Clasical MPR Choral
alias wefunk="mpv http://www.wefunkradio.com/play/shoutcast.pls" # WEFUNK Radio MP3 64K
alias sleepbot="mpv http://sleepbot.com/ambience/cgi/listen.cgi/listen.pls" # Sleepbot Environmental Broadcast 56K MP3
alias groovesalad="mpv http://somafm.com/groovesalad130.pls" # Soma FM Groove Salad iTunes AAC 128K
alias dronezone="mpv http://somafm.com/dronezone130.pls" # Soma FM Drone Zone iTunes AAC 128K
alias lush="mpv http://somafm.com/lush130.pls" # Soma FM Lush iTunes AAC 128K
alias sonicuniverse="mpv http://somafm.com/sonicuniverse.pls" # Soma FM Sonic Universe iTunes AAC 128K

# liberty statue live cam
alias libertystatue='mpv http://video3.earthcam.com/fecnetwork/libertyHD1.flv/chunklist_w1810184803.m3u8'

alias cbanews='mpv http://abclive.abcnews.com/i/abc_live2@136328/index_2500_av-b.m3u8'

alias vbros='mpv http://adultswim-vodlive.cdn.turner.com/the-venture-bros/stream_4.m3u8'
alias anoise='mpv --shuffle --volume=50 --no-video --title=anoise ~/music/anoise/*'

# external ip address
alias wanip='dig @resolver1.opendns.com ANY myip.opendns.com +short'
alias localip="ip addr | grep -Po '(?<=inet)\s(?!127)[^/]+'"
alias lp="ip addr | grep -Po '(?<=inet)\s(?!127)[^/]+'"
#alias curl='curl -A "Mozilla/5.0 (X11; Linux i686; rv:78.0) Gecko/20100101 Firefox/78.0"'
alias curl='curl -A "Mozilla/5.0 (X11; Linux i686; rv:78.0) Gecko/20100101 Firefox/78.0"'
alias myexternalip='curl ifconfig.me'
alias ipaddress='hostname -i'
alias iso2utf='iconv -f iso-8859-1 -t utf-8'
alias utf2iso='iconv -f utf-8 -t iso-8859-1'
alias fontreload='fc-cache -fv'
#alias timezsh='time zsh -ilc exit'
alias timezsh='for i in $(seq 1 10); do time zsh -i -c exit; done'

alias operacurl='curl -A "Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3610.70 Safari/537.36 OPR/60.0.3259.32"' # curl with opera user-agent
alias ffcurl='Mozilla/5.0 (X11; Linux i686; rv:78.0) Gecko/20100101 Firefox/78.0' # curl with firefox user-agent

alias wgetopera='wget --user-agent="Mozilla/5.0 (Windows NT 6.2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3610.70 Safari/537.36 OPR/60.0.3259.32"'

########### aliases #################
# aliases para fasd
# playing a sound
alias finished='paplay /usr/share/sounds/freedesktop/stereo/complete.oga'
alias errorsound='paplay /usr/share/sounds/freedesktop/stereo/suspend-error.oga'

# alias z='(){cd $(zoxide query $1)}'
# alias j='zshz 2>&1'

(( $+commands[zoxide] )) && {
  # alias j='zshz 2>&1'
  # alias cd=' z'
  # alias cd=' j'
  eval "$(zoxide init zsh)"
}

alias volumeup='pactl -- set-sink-volume 0 150%'
alias volumenormal='pactl -- set-sink-volume 0 100%'

(( $+commands[ddgr] )) && alias ddgr='export BROWSER="lynx";ddgr'

alias o='a -e xdg-open' # quick opening files with xdg-open
# alias _!='fc -e "sed -i -e \"s/^/sudo /\""' # sudo last command (now we have a widget)
# para abrir um arquivo recem editado com o vim faça: v nome

(( $+commands[devour])) && {
    (( $+commands[mupdf])) && alias mupdf="devour mupdf"
    # (( $+commands[mpv])) && alias mpv="devour mpv"
}

# http://bit.ly/2W4MdwU (exa a better ls)
(( $+commands[exa] )) && alias ls=exa

# pbpaste gets clipboard and redirects to a vim scratch buffer
alias vs="pbpaste | vimscratch"
lowercb='pbpaste | lower | pbcopy'

(( $+commands[xdg-open] )) && alias open="xdg-open"

#alias last2cb='fc -ln -1 | xclip -selection clipboard'
alias last2cb='fc -ln -1 | pbcopy'

alias http-server='python3 -m http.server'
alias pythonupdate="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1 | xargs -n1 sudo pip install -U"
# alias python='/usr/bin/python3'

# alias flashcard="pbpaste | sed 's, ,-,g' | sed 's/./\L&/g' | sed 's,$,.mp3,g' | sed 's/\x27\|,//g' | xclip -selection cliboard ; exit"

#copy output of last command to clipboard
alias cl="fc -e -| pbcopy"

# este alias � para editar no audacity um audio selecionado
# dentro do anki
# alias EW='z collect && audacity `xclip -selection clipboard -o`'

# to show aliases definitions simple do: which alias
alias newest='ls -lt  **/*(.om[1,20])' # list 20 newest files anywhere in directory hierarchy (very useful) *N*'
#alias lad='ls -d -- .*(/)'				# only show dot-directories
#alias lsa='ls -a -- .*(.)'				# only show dot-files
#alias lsd='ls -d -- *(/)'			        # only show directories

# alias ls='ls --color=auto --group-directories-first -F'
# alias ls='ls --color=auto'

# Next gen ls command with lots of pretty colors and awesome icons
# It is a cargo package
(( $+commands[lsd] )) && alias ls='lsd'

alias cd=' cd'
alias fixmp4='detox * && m4a2mp3 && prename 'tr/A-Z/a-z/' *'
alias -s {md,txt,text,lua}=vim
alias -s {wav,mp3,ogg}='background mpv'
alias -s pdf=evince
alias -g PP='|'
alias -g pp='xclip -selection clipboard -o' # pbpaste
alias -g pc='xclip -selection clipboard' # pbcopy
alias -g gp='| grep -i'
alias -g CB='| xclip -selection c'
alias -g C='| wc -l'
alias -g L='| less -r'
alias -g T='| tail'
alias -g H='| head'
alias -g G='| grep -i'
alias -g V='| gvim -'
#alias -g X='| xargs'
alias -g NM='**/*(.om[1])'       # newest modificated file
alias -g NC='**/*(.oc[1])'       # newest created file
alias -g ND='./**/*(/oc[1]:h)'   # newest directory --> print -l ND
alias -g latest='./**/*(.oc[1])' # newest file | also Ctrl-x Ctrl-m
alias -g NUL="> /dev/null 2>&1"
alias -g DN="> /dev/null 2>&1"
alias -g LC="| sed 's/./\L&/g'" # lowercase string (piped)
alias -g UL="| sed 's, ,_,g'"
alias -g ULC="| sed 's/./\L&/g' | sed 's, ,_,g'"
alias -g VS='| vimscratch -'
alias pnf='mpg123 ./**/*(.oc[1])'
#alias playrandom='mpg123 "$(shuf -n 1 -e **/*.mp3)"'
# alias playrandom='mpg123 -Z **/*.mp3'
alias playrandom='mpv --shuffle --no-video **/*.mp3'
