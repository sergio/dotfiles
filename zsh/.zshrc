# File: $ZDOTDIR/.zshrc
# Last Change: Sat, 18 Jan 2025 - 13:27:07
# vim:set ft=sh nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

# read also: ~/.zshenv

# disable control flow
stty -ixon

# automatically remove duplicates from these arrays
typeset -U path cdpath fpath manpath

zstyle ':completion::complete:*' use-cache on
zstyle ':completion::complete:*' cache-path ~/.zsh/cache/$HOST
# https://stackoverflow.com/a/24237590/2571881
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

# Use emacs key bindings
bindkey -e

#export ZDOTDIR=$HOME/.dotfiles/zsh
HISTFILE=~/.zsh_history
# setopt appendhistory
setopt inc_append_history
setopt share_history

export HISTSIZE=1000                   # Maximum events for internal history
export SAVEHIST=1000                   # Maximum events in history file

# History won't save duplicates.
setopt HIST_IGNORE_ALL_DUPS

# History won't show duplicates on search.
setopt HIST_FIND_NO_DUPS

# some useful options (man zshoptions)
setopt autocd extendedglob nomatch menucomplete
setopt interactive_comments
stty stop undef		# Disable ctrl-s to freeze terminal.
zle_highlight=('paste:none')

# beeping is annoying
unsetopt BEEP

# ~/.local/bin was duplicated
# path=( ~/.dotfiles/bin ~/.local/lib /home/sergio/.gem/ruby/2.7.0/bin /usr/bin/core_perl /usr/bin/python3 $path )
path=(
~/.dotfiles/bin                   # Diretório de scripts personalizados
~/.local/lib                      # Diretório local de bibliotecas
~/.gem/ruby/([0-9]+\.)*[0-9]+/bin(N-/)  # Diretório Ruby com versão dinâmica (números seguidos por ponto, último número sem ponto)
/usr/bin/core_perl(N-/)           # Diretório de binários Perl, se existir
/usr/bin/python3(N-/)             # Diretório de binários Python, se existir
$path                             # Manter o path existente
)

# add dwm scripts if we are in a dwm session
[[ "$DESKTOP_SESSION" == "Dwm" ]] && path=( ~/.dotfiles/dwm/scripts  $path)

path=( /bin/npm $path )

setopt auto_cd
# cdpath=($HOME/tmp $HOME/img $HOME/docs $HOME/music $HOME/build)
cdpath=(.. ../..)

MY_ZSH_FPATH=${ZDOTDIR}/autoloaded
fpath=( $MY_ZSH_FPATH $fpath )

if [[ -d "$MY_ZSH_FPATH" ]]; then
  for func in $MY_ZSH_FPATH/*; do
    autoload -Uz ${func:t}
  done
fi
unset MY_ZSH_FPATH

# completions
# Skip the not really helping Ubuntu global compinit
unsetopt GLOBAL_RCS
skip_global_compinit=1
setopt noglobalrcs

autoload -Uz zrecompile
autoload -Uz compinit
dump=$ZSH_COMPDUMP

autoload -U bashcompinit
bashcompinit

autoload zmv

# http://zsh.sourceforge.net/Doc/Release/Conditional-Expressions.html
if [[ -s $dump(#qN.mh+24) && (! -s "$dump.zwc" || "$dump" -nt "$dump.zwc") ]]; then
  compinit -i d $ZSH_COMPDUMP
  zrecompile $ZSH_COMPDUMP
fi
compinit -C

## complete cdpath
zstyle ':completion:*' group-name ''
zstyle ':completion:*:descriptions' format %d
zstyle ':completion:*:descriptions' format %B%d%b
zstyle ':completion:*:complete:(cd|pushd):*' tag-order \
  'local-directories named-directories'

# Fuzzy matching of completions
# https://grml.org/zsh/zsh-lovers.html
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only
zstyle -e ':completion:*:approximate:*' \
  max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3))numeric)'

# https://www.zsh.org/mla/users/2015/msg00467.html
zstyle -e ':completion:*:*:(ssh|mosh):*:my-accounts' users-hosts \
  '[[ -f ${HOME}/.ssh/config && $key = hosts ]] && key=my_hosts reply=()'

zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu select
# zstyle ':completion::complete:lsof:*' menu yes select
zmodload zsh/complist
# compinit

autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

#ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=cyan'
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=10'

# Colors
autoload -Uz colors && colors

# Useful Functions
source "$ZDOTDIR/zsh-functions"

# # # quote pasted URLs
# source "$ZDOTDIR/url-quote-magic"
# # it seems kind of uncompatible with substring-search
# autoload -Uz url-quote-magic
# zle -N self-insert url-quote-magic
# autoload -Uz bracketed-paste-magic
# zle -N bracketed-paste bracketed-paste-magic

# Normal files to source
zsh_add_file "zsh-exports"
# zsh_add_file "zsh-vim-mode"
zsh_add_file "zsh-aliases"
zsh_add_file "zsh-prompt"

# use starship or my old zsh-promp
# install starship: curl -sS https://starship.rs/install.sh | sh
# (( $+commands[starship] )) && eval "$(starship init zsh)" || zsh_add_file "zsh-prompt"

# Plugins
# https://forum.endeavouros.com/t/tip-better-url-pasting-in-zsh/6962
# Note -> zsh_add_plugin is a function defined at ~/.dotfiles/zsh/zsh-functions
zsh_add_plugin "zsh-users/zsh-autosuggestions"
ZSH_AUTOSUGGEST_BUFFER_MAX_SIZE=20

zsh_add_plugin 'zsh-users/zsh-syntax-highlighting'
zsh_add_plugin 'hlissner/zsh-autopair'
zsh_add_plugin 'le0me55i/zsh-extract'
zsh_add_plugin 'zsh-users/zsh-completions'

# zsh_add_plugin "agkozak/zsh-z"
# zsh_zdd_plugin "amaya382/zsh-fzf-widgets"
# zsh_add_plugin "oz/safe-paste"
#zsh_add_plugin "romkatv/powerlevel10k.git"
#zsh_add_completion "esc/conda-zsh-completion" false

# depens on zsh-fzf-widgets
# bindkey '^R' fzf-cdr
# bindkey '^H' fzf-history

[[ -d $ZDOTDIR/plugins/zsh-z ]]  && export ZSHZ_CASE=smart

# source: https://forum.endeavouros.com/t/tip-better-url-pasting-in-zsh/6962
autoload -U url-quote-magic bracketed-paste-magic
zle -N self-insert url-quote-magic
zle -N bracketed-paste bracketed-paste-magic

# Now the fix, setup these two hooks:
# fix url-quote-magic and autosuggestions lagging
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic
}
pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish

# and finally, make sure zsh-autosuggestions does not interfere with it:
ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(bracketed-paste)

#zsh_add_completion "marlonrichert/zsh-autocomplete"
# For more plugins: https://github.com/unixorn/awesome-zsh-plugins
# More completions https://github.com/zsh-users/zsh-completions

# Key-bindings
# bindkey -s '^o' 'ranger^M'
# bindkey -s '^f' 'zi^M'
# bindkey -s '^s' 'ncdu^M'
# bindkey -s '^n' 'nvim $(fzf)^M'
# bindkey -s '^v' 'nvim\n'
#bindkey -s '^z' 'zi^M'
bindkey '^[[P' delete-char
bindkey "\e[3~" delete-char
bindkey "^p" up-line-or-beginning-search # Up
bindkey "^n" down-line-or-beginning-search # Down
bindkey "^k" up-line-or-beginning-search # Up
bindkey "^j" down-line-or-beginning-search # Down
#bindkey -r "^u"
#bindkey -r "^d"

# In menu completion, the Return key will accept the current selected match
bindkey -M menuselect '^M' .accept-line
bindkey '^M' accept-line

# shift-tab: go backward in menu (invert of tab)
bindkey '^[[Z' reverse-menu-complete

# ctrl+b/f or ctrl+left/right: move word by word (backward/forward)
bindkey '^b' backward-word
bindkey '^f' forward-word
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

# fixing keybindings
bindkey '^A' beginning-of-line
bindkey '^E' end-of-line
bindkey '^K' kill-line

bindkey '^u' backward-kill-line
bindkey '^y' yank
#bindkey '^H' kill-word # deletes previous word (Ctrl+backspace)

# # https://unix.stackexchange.com/a/572440/3157
# # Make `Ctrl + W` behave like it does in Bash, deleting words separated by
# # spaces. We do this by redefining the `backward-delete-word` function and bind
# # that to `Ctrl + W`.
# SPACE_WORDCHARS='~!#$%^&*(){}[]<>?.+;-_/\|=@`'
# backward-delete-word() WORDCHARS=$SPACE_WORDCHARS zle .$WIDGET
# zle -N backward-delete-word
# bindkey "^W" backward-delete-word

# https://unix.stackexchange.com/a/319854/3157
# alt-backspace
backward-kill-dir () {
local WORDCHARS=${WORDCHARS/\/}
zle backward-kill-word
zle -f kill
}
zle -N backward-kill-dir
bindkey '^[^?' backward-kill-dir
# bindkey '^w' backward-kill-dir

# Ctrl+Backspace: kill the word backward
bindkey -M emacs '^H' backward-kill-word
bindkey -M viins '^H' backward-kill-word
bindkey -M vicmd '^H' backward-kill-word

# Ctrl+Delete: kill the word forward
bindkey -M emacs '^[[3;5~' kill-word
bindkey -M viins '^[[3;5~' kill-word
bindkey -M vicmd '^[[3;5~' kill-word

# in this case we need our vif() function (have a look at ~/.zsh/autoloaded)
# https://jdhao.github.io/2019/06/13/zsh_bind_keys/
bindkey -s '^o' 'vif^M'
bindkey -s '^f' 'nf^M'
bindkey -s '^g' '^Ugit status --short^M'
# the below binding key call an alias to the last edited file (Ctrl+Alt+l)
bindkey -s '^[^L' 'lvim^M'
bindkey -s '^[^O' 'old^M'
# bindkey -s '^D' 'cd ~/.dotfiles^M'
# bindkey -s '^[^O' 'nvim -c "Telescope oldfiles^M"'

# # Bind \eg to `git status`
# function _git-status {
#     zle kill-whole-line
#     zle -U "git status --short"
#     zle accept-line
# }
# zle -N _git-status
# bindkey '\eg' _git-status

bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search

# FZF
# TODO update for mac
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/doc/fzf/examples/completion.zsh ] && source /usr/share/doc/fzf/examples/completion.zsh
[ -f /usr/share/doc/fzf/examples/key-bindings.zsh ] && source /usr/share/doc/fzf/examples/key-bindings.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
#[ -f $ZDOTDIR/completion/_fnm ] && fpath+="$ZDOTDIR/completion/"
# export FZF_DEFAULT_COMMAND='rg --hidden -l ""'

compinit
_comp_options+=(globdots) # also show hidden files

# Edit line in vim with ctrl-e:
autoload -z edit-command-line; zle -N edit-command-line
bindkey "^X^E" edit-command-line
bindkey '^xe' edit-command-line
# bindkey '^e' edit-command-line

# alt+m: copy last word
# bindkey "^[m" copy-prev-shell-word

if ! grep -qEi "(microsoft|wsl)" /proc/version && [ "$XDG_SESSION_TYPE" != "wayland" ]; then
  setxkbmap -model abnt2 -layout br -option caps:escape
fi

# Speedy keys
# xset r rate 190 40

# Environment variables set everywhere
# set them in ~/.zshenv

# For QT Themes
#export QT_QPA_PLATFORMTHEME=qt5ct

# swap escape and caps
# setxkbmap -option caps:swapescape
# setxkbmap -option caps:escape
# swap escape and caps
# setxkbmap -option caps:swapescape

# I was getting and error on ctrl-d
# after exiting vim, I hope this work
# https://superuser.com/a/1452645/45032
exit_zsh() { exit }
zle -N exit_zsh
bindkey '^D' exit_zsh

zle -N copybuffer
# Ctrl-x Ctrl-o
bindkey -M emacs "^X^O" copybuffer
bindkey -M viins "^X^O" copybuffer
bindkey -M vicmd "^X^O" copybuffer

bindkey '^ ' autosuggest-accept

# backward-kill-word act on whole quoted string
# source: https://unix.stackexchange.com/a/721927/3157
autoload -U select-word-style
select-word-style normal
  zle -N backward-kill-shell-word backward-kill-word-match
  zstyle :zle:backward-kill-shell-word word-style shell
  bindkey '^w'  backward-kill-shell-word

# fix simple terminal del issue
function zle-line-init () { echoti smkx }
function zle-line-finish () { echoti rmkx }
zle -N zle-line-init
zle -N zle-line-finish

#  virtual machine management
nvm() {
  unfunction $0
  export NVM_DIR=~/.nvm
  [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
  nvm "$@"
}

# node() {
#     unfunction $0
#     export NVM_DIR=~/.nvm
#     [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
#     node "$@"
# }

# npm() {
#     unfunction $0
#     export NVM_DIR=~/.nvm
#     [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
#     npm "$@"
# }

## python virtual environment
#export PATH="$HOME/.pyenv/bin:$PATH"
#export PATH="$PYENV_ROOT/bin:$PATH"
#export PYENV_VIRTUALENV_DISABLE_PROMPT=1
#if command -v pyenv 1>/dev/null 2>&1; then
#  eval "$(pyenv init -)"
#fi

# source: https://github.com/cuducos/dotfiles/blob/main/bootstrap.sh
# setup neovim's python virtualenv
NEOVIM_PYTHON_VENV=$HOME/.virtualenvs/neovim
if [ ! -d $NEOVIM_PYTHON_VENV ]; then
  python3 -m venv $NEOVIM_PYTHON_VENV
  $NEOVIM_PYTHON_VENV/bin/pip install -U pip
  $NEOVIM_PYTHON_VENV/bin/pip install black neovim
fi

# alias luamake=/home/sergio/build/sumneko/3rd/luamake/luamake
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

# Created by `pipx` on 2023-09-09 19:20:40
export PATH="$PATH:/home/sergio/.local/bin"

if [ -d ~/perl5 ]; then
  PATH="/home/sergio/perl5/bin${PATH:+:${PATH}}"; export PATH;
  PERL5LIB="/home/sergio/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
  PERL_LOCAL_LIB_ROOT="/home/sergio/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
  PERL_MB_OPT="--install_base \"/home/sergio/perl5\""; export PERL_MB_OPT;
  PERL_MM_OPT="INSTALL_BASE=/home/sergio/perl5"; export PERL_MM_OPT;
fi

function exit {
  emulate -L zsh
  setopt extended_glob
  if [[ -n ${jobstates[(r)s*]} ]]; then
    echo "you have suspended jobs"
    return 1
  fi
  if [[ -n ${jobstates[(r)^done:*]} ]]; then
    echo "you have running jobs"
    return 1
  fi
  builtin exit "$@"
}

[[ -f /home/linuxbrew/.linuxbrew/bin/brew ]] && (
# https://brew.sh/
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
)
