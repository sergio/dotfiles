
[ ! -d  ~/.config/autostart ] && mkdir -p ~/.config/autostart
ln -sfvn ~/.dotfiles/redshift/redshift.desktop ~/.config/autostart/redshift.desktop
