-- Filename: /home/sergio/.config/nvim/after/ftplugin/help.lua
-- Last Change: Thu, 16 Nov 2023 - 06:11:03
-- vim:set softtabstop=2 shiftwidth=2 tabstop=2 expandtab ft=lua:

vim.keymap.set("n", "q", "<cmd>close!<cr>", { desc = "close help file using q"})
vim.keymap.set('', '<cr>', '<c-]>', { desc = "jump to tags using <cr>"})

