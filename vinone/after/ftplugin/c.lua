-- Filename: lua.lua
-- Last Change: Thu, 16 Nov 2023 - 06:11:03
-- reference: r/neovim/comments/pl0p5v/comment/hvn0kff/
-- vim:set ft=c nolist softtabstop=4 shiftwidth=4 tabstop=4 expandtab:

-- local opt_local = vim.opt_local
-- vim.opt_local.includeexpr , _ = vim.v.fname:gsub('%.', '/')
vim.opt_local.spell = false
vim.opt_local.list = false

vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.bo.softtabstop = 4
vim.bo.textwidth = 78
vim.bo.expandtab = true
vim.bo.autoindent = true
