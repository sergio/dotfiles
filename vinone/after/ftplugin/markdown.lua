-- Filename: markdown.lua
-- Last Change: Thu, 16 Nov 2023 - 06:11:03
-- vim:set ft=lua nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

vim.cmd([[syn region markdownLink matchgroup=markdownLinkDelimiter start="(" end=")" contains=markdownUrl keepend contained conceal]])
vim.cmd([[syn region markdownLinkText matchgroup=markdownLinkTextDelimiter start="!\=\[\%(\%(\_[^][]\|\[\_[^][]*\]\)*]\%( \=[[(]\)\)\@=" end="\]\%( \=[[(]\)\@=" nextgroup=markdownLink,markdownId skipwhite contains=@markdownInline,markdownLineStart concealends]])

vim.opt_local.number = false
vim.opt_local.relativenumber = false
vim.opt_local.list = false
vim.g['loaded_spellfile_plugin'] = 0
vim.opt_local.spell = true
vim.bo.spelllang = 'en_us'
vim.bo.textwidth = 80
-- vim.bo.filetype = 'markdown'
vim.opt_local.wrap = true
vim.opt_local.suffixesadd:prepend('.md')
vim.wo.conceallevel = 2


vim.cmd([[highlight MinhasNotas ctermbg=Yellow ctermfg=red guibg=Yellow guifg=red]])
vim.cmd([[match MinhasNotas /NOTE:/]])


vim.keymap.set('n', ']]', function()
    vim.fn.search("^#")
    vim.cmd('normal zt')
    require('core.utils').flash_cursorline()
end,
{ buffer = true, desc = 'Jump to the next Heading' })

vim.keymap.set('n', '[[', function()
    vim.fn.search("^#", "b")
    vim.cmd('normal zt')
    require('core.utils').flash_cursorline()
end,
{ buffer = true, desc = 'Jump to the previous Heading' })

-- TODO:
--vim.keymap.set('n', '+', function()
---- map to increase and decrease markdown headings
--end,
--)
