-- Filename: tex.lua
-- Last Change: Thu, 16 Nov 2023 - 06:11:03
-- vim:set ft=lua nolist softtabstop=2 shiftwidth=2 tabstop=2 expandtab:

vim.opt_local.wrap = true
