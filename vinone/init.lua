-- File: /home/sergio/.config/vinone/init.lua
-- Last Change: Mon, 10 Jun 2024 - 14:27:06
-- Compile lua to bytecode if the nvim version supports it.

local mapfile = " "
local Utils = require('utils')
local map = Utils.map

if vim.loader and vim.fn.has "nvim-0.9.1" == 1 then vim.loader.enable() end

vim.g.mapleader = " "					-- sets leader key
vim.g.netrw_banner = 0				-- gets rid of the annoying banner for netrw
vim.g.netrw_browse_split=4		-- open in prior window
vim.g.netrw_altv = 1					-- change from left splitting to right splitting
vim.g.netrw_liststyle=3				-- tree style view in netrw
--vim.cmd("let g:netrw_list_hide=netrw_gitignore#Hide()")
vim.opt.title = true					-- show title
vim.opt.winbar = '%=%m %F'
--vim.cmd('set path+=**')					-- search current directory recursively
vim.opt.path = vim.opt.path + "**"
vim.opt.syntax = "ON"
vim.opt.keywordprg = ':help'
vim.opt.autochdir = true
--vim.opt.keywordpgk = "help"  -- test
--vim.opt.backup = false
--vim.opt.compatible = false				-- turn off vi compatibility mode
vim.opt.number = true					-- turn on line numbers
vim.opt.relativenumber = true				-- turn on relative line numbers
vim.opt.mouse = 'a'						-- enable the mouse in all modes
vim.opt.ignorecase = true				-- enable case insensitive searching
vim.opt.smartcase = true				-- all searches are case insensitive unless there's a capital letter
vim.opt.hlsearch = false				-- disable all highlighted search results
vim.opt.incsearch = true				-- enable incremental searching
vim.opt.wrap = true						-- enable text wrapping
vim.opt.tabstop = 2						-- tabs=4spaces
vim.opt.shiftwidth = 2
vim.opt.fileencoding = "utf-8"				-- encoding set to utf-8
vim.opt.pumheight = 10					-- number of items in popup menu
vim.opt.showtabline = 2					-- always show the tab line
vim.opt.laststatus = 2					-- always show statusline
vim.opt.signcolumn = "auto"
vim.opt.expandtab = false				-- expand tab 
vim.opt.smartindent = true
vim.opt.showcmd = true
vim.opt.cmdheight = 2
vim.opt.showmode = true
vim.opt.scrolloff = 3					-- scroll page when cursor is 8 lines from top/bottom
vim.opt.sidescrolloff = 8				-- scroll page when cursor is 8 spaces from left/right
vim.opt.guifont = "monospace:h17"
--vim.opt.clipboard = unnamedplus
vim.opt.completeopt= { "menuone", "noselect" }
vim.opt.splitbelow = true				-- split go below
vim.opt.splitright = true				-- vertical split to the right
vim.opt.termguicolors = true			-- terminal gui colors
vim.cmd('colorscheme habamax')			-- set colorscheme
vim.cmd('let g:netrw_liststyle=3')
--vim.cmd('filetype plugin on')			-- set filetype
--vim.cmd('set wildmenu')					-- enable wildmenu
vim.cmd('inoreabbrev idate <c-r>=strftime("%a, %d %b %Y %T")<cr>')
vim.cmd('inoreabbrev Fname <c-r>=expand("%:p")<cr>')
vim.cmd('inoreabbrev Iname <c-r>=expand("%:p")<cr>')
vim.cmd('inoreabbrev fname <c-r>=expand("%:t")<cr>')
vim.cmd('inoreabbrev iname <c-r>=expand("%:t")<cr>')

--statusline
vim.cmd "highlight StatusType guibg=#b16286 guifg=#1d2021"
vim.cmd "highlight StatusFile guibg=#fabd2f guifg=#1d2021"
vim.cmd "highlight StatusModified guibg=#1d2021 guifg=#d3869b"
vim.cmd "highlight StatusBuffer guibg=#98971a guifg=#1d2021"
vim.cmd "highlight StatusLocation guibg=#458588 guifg=#1d2021"
vim.cmd "highlight StatusPercent guibg=#1d2021 guifg=#ebdbb2"
vim.cmd "highlight StatusNorm guibg=none guifg=white"
vim.o.statusline = " "
				.. ""
				.. " "
				.. "%l"
				.. " "
				.. " %#StatusType#"
				.. "<< "
				.. "%Y" 
				.. "  "
				.. " >>"
				.. "%#StatusFile#"
				.. "<< "
				.. "%f"
				.. " >>"
				.. "%#StatusModified#"
				.. " "
				.. "%m"
				.. " "
				.. "%#StatusNorm#"
				.. "%="
				.. "%#StatusBuffer#"
				.. "<< "
				.. "﬘ "
				.. "%n"
				.. " >>"
				.. "%#StatusLocation#"
				.. "<< "
				.. "燐 "
				.. "%l,%c"
				.. " >>"
				.. "%#StatusPercent#"
				.. "<< "
				.. "%p%%  "
				.. " >> "


-- smooth scroll with no plugin
-- https://stackoverflow.com/a/62826337/2571881
map('n', '<C-u>', [[repeat("\<C-y> :sleep 10m<CR>", winheight('%')/2)]], { expr = true})
map('n', '<C-d>', [[repeat("\<C-e> :sleep 10m<CR>", winheight('%')/2)]], { expr = true})

-- reload config
map("n", "<leader>r", ":source $MYVIMRC<CR>")	-- reload neovim config

map('n', '<leader>x', ':wsh | up | sil! bd<cr>', { silent = true })
map('n', '<leader>w', ':bw!<cr>', { silent = true })
map('n', 'gl', '`.', { desc = 'Jump to the last changepoint' })

map('c', '<C-a>', '<home>')
map('c', '<C-e>', '<end>')

map(
  'n',
  '<right><right>',
  function()
    vim.cmd.normal('}ztj')
    local current_line = vim.api.nvim_get_current_line()
    if current_line:match('^%s*$') ~= nil then
      vim.cmd.normal('"_dd')
    end
  end,
  {
    desc = 'Jump to the next block of code',
    silent = true,
  }
)

map(
  'n',
  '<left><left>',
  function()
    vim.cmd.normal('{{jzt')
  end,
  {
    desc = 'Jump to the previous block of code',
    silent = true,
  }
)

local open_command = 'xdg-open'
if vim.fn.has('mac') == 1 then
  open_command = 'open'
end

local function url_repo()
  local cursorword = vim.fn.expand('<cfile>')
  if string.find(cursorword, '^[a-zA-Z0-9-_.]*/[a-zA-Z0-9-_.]*$') then
    cursorword = 'https://github.com/' .. cursorword
  end
  return cursorword or ''
end

map('n', 'gx', function()
  vim.fn.jobstart({ open_command, url_repo() }, { detach = true })
end, { silent = true })

-- easy split generation
map("n", "<leader>v", ":drop $MYVIMRC<CR>")				-- space+v creates a veritcal split
map('n', '<Leader>z', '<cmd>drop ~/.zshrc<CR>')
map("n", "<leader>s", ":split")					-- space+s creates a horizontal split

map('i', '<C-r>+', '<C-r><C-o>+', { desc = 'Insert clipboard keeping indentation' })
map('i', '<C-r>*', '<C-r><C-o>*', { desc = 'Insert primary clipboard keeping indentation' })
map('i', '<S-Insert>', '<C-r><C-o>*', { desc = 'Insert primary clipboard keeping indentation' })
map('i', '<leader>+', '<C-r><C-o>+', { desc = 'Insert clipboard keeping indentation' })
map('i', '<S-Insert>', '<C-r><C-o>*', { desc = 'Insert clipboard keeping indentation' })

map('i', '.', '.<c-g>u')
map('i', '!', '!<c-g>u')
map('i', '?', '?<c-g>u')
--map('i', ';', ';<c-g>u')
--map('i', ':', ':<c-g>u')
--map('i', ']', ']<c-g>u')
--map('i', '}', '}<c-g>u')

map('n', '<M-p>', '"+p')
map('i', '<M-p>', '<C-r><C-o>+')
map('v', '<leader>y', '"+y')
map('n', '<c-m-f>', '<cmd>Format<cr>', { desc = 'Reformat keeping cursor position' })

map("n", "<F9>", "<cmd>up<cr>", { desc = "save file", silent = false})
map("n", "<c-s>", "<cmd>up<cr>", { desc = "save file", silent = false})

-- buffer navigation
map("n", "<leader>d", ":bd! <CR>")				-- Space+d delets current buffer

-- adjust split sizes easier
map("n", "<C-Left>", ":vertical resize +3<CR>")		-- Control+Left resizes vertical split +
map("n", "<C-Right>", ":vertical resize -3<CR>")	-- Control+Right resizes vertical split -

-- Open netrw in 25% split in tree view
map("n", "<leader>e", ":25Lex<CR>")			-- space+e toggles netrw tree view

-- Easy way to get back to normal mode from home row
map("i", "kj", "<Esc>")					-- kj simulates ESC
map("i", "jk", "<Esc>")					-- jk simulates ESC

-- Automatically close brackets, parethesis, and quotes
map("i", "'", "''<left>")
map("i", "\"", "\"\"<left>")
map("i", "(", "()<left>")
map("i", "[", "[]<left>")
map("i", "{", "{}<left>")
map("i", "{;", "{};<left><left>")
map("i", "/*", "/**/<left><left>")

-- Visual Maps
map("v", "<leader>r", "\"hy:%s/<C-r>h//g<left><left>")			    -- Replace all instances of highlighted words 
map("v", "J", ":m '>+1<CR>gv=gv")								-- Move current line down
map("v", "K", ":m '>-2<CR>gv=gv")								-- Move current line up 

-- map('n',  '>',  '>>', { desc = "faster indent unindent" } )
-- map('n',  '<',  '<<', { desc = "faster indent unindent" } )
map('v', '<', '<gv', { desc = 'Reselect after < on visual mode' })
map('v', '>', '>gv', { desc = 'Reselect after > on visual mode' })


map(
  'n',
  'dd',
  function()
    return Utils.is_empty_line() and '"_dd' or 'dd'
  end,
  {
    expr = true,
    desc = mapfile .. "Throw blank lines to black hole register",
  }
)

map(
  "n",
  "i",
  function()
    return Utils.is_empty_line() and 's' or 'i'
  end,
  {
    expr = true,
    desc = "properly indent on empty line when insert",
  }
)

map('c', 'bd', function()
  -- https://stackoverflow.com/a/47074633
  -- https://codereview.stackexchange.com/a/282183
  local results = {}
  local buffers = vim.api.nvim_list_bufs()
  for _, buffer in ipairs(buffers) do
    if vim.api.nvim_buf_is_loaded(buffer) then
      local filename = vim.api.nvim_buf_get_name(buffer)
      if filename ~= '' then
        table.insert(results, filename)
      end
    end
  end
  curr_buf = vim.api.nvim_buf_get_name(0)
  if #results > 1 or curr_buf == '' then
    vim.cmd('bd')
  else
    vim.cmd('quit')
  end
end, { silent = false, desc = 'bd or quit' })

-- line text-objects
-- https://vimrcfu.com/snippet/269
map('o', 'al', [[v:count==0 ? ":<c-u>normal! 0V$h<cr>" : ":<c-u>normal! V" . (v:count) . "jk<cr>" ]], { expr = true })
map('v', 'al', [[v:count==0 ? ":<c-u>normal! 0V$h<cr>" : ":<c-u>normal! V" . (v:count) . "jk<cr>" ]], { expr = true })
map('o', 'il', [[v:count==0 ? ":<c-u>normal! ^vg_<cr>" : ":<c-u>normal! ^v" . (v:count) . "jkg_<cr>"]], { expr = true })
map('v', 'il', [[v:count==0 ? ":<c-u>normal! ^vg_<cr>" : ":<c-u>normal! ^v" . (v:count) . "jkg_<cr>"]], { expr = true })

map('n', '<leader>*', '*<c-o>cgn')
map('n', '<leader>#', '#<c-o>cgn')

-- It adds motions like 25j and 30k to the jump list, so you can cycle
-- through them with control-o and control-i.
-- source: https://www.vi-improved.org/vim-tips/
map('n', 'j', [[v:count ? (v:count > 5 ? "m'" . v:count : '') . 'j' : 'gj']], { expr = true })
map('n', 'k', [[v:count ? (v:count > 5 ? "m'" . v:count : '') . 'k' : 'gk']], { expr = true })

-- automatically generate a template when a new bash script or markdown document is opened.
vim.cmd(':autocmd BufNewFile *.sh 0r ~/.config/nvim/skeleton.sh')
vim.cmd(':autocmd BufNewFile *.md 0r ~/.config/nvim/skeleton.md')

-- save as root, in my case I use the command 'doas'
vim.cmd([[cmap w!! w !doas tee % >/dev/null]])
vim.cmd([[command! SaveAsRoot w !doas tee %]])


map(
  'o',
  'il',
  ':normal vil<cr>',
  {
    desc = 'Inner line',
    silent = true,
  }
)

map(
  'x',
  'al',
  '$o_',
  {
    desc = 'Arrownd line',
    silent = true,
  }
)

map(
  'o',
  'al',
  ':normal val<cr>',
  {
    desc = 'Arrownd line',
    silent = true,
  }
)

-- AUTOCOMMANDS

-- Define local variables
local autocmd = vim.api.nvim_create_autocmd

-- @returns a "clear = true" augroup
local function augroup(name)
  return vim.api.nvim_create_augroup('vinone_' .. name, { clear = true })
end

-- Highlight text on yank
autocmd('TextYankPost', {
  group = augroup('YankHighlight'),
  callback = function()
    vim.highlight.on_yank { higroup = 'IncSearch', timeout = '700' }
  end,
  desc = 'Highlight yanked text',
})

autocmd('InsertLeave', {
  pattern = '*',
  group = augroup('MatchRedundantSpaces'),
  callback = function()
    vim.cmd([[highlight RedundantSpaces ctermbg=red guibg=red]])
    vim.cmd([[match RedundantSpaces /\s\+$/]])
  end,
  desc = 'Higlight extra spaces',
})

autocmd('InsertEnter', {
  pattern = '*',
  group = augroup('clear_matches'),
  callback = function()
    vim.cmd([[call clearmatches()]])
    vim.cmd([[highlight RedundantSpaces ctermbg=red guibg=red]])
    vim.cmd([[set nohls]])
  end,
  desc = 'Do not show extra spaces during typing',
})

autocmd({ 'VimEnter', 'CursorMoved' }, {
  group = augroup('Store_yank_pos'),
  pattern = '*',
  callback = function()
    cursor_pos = vim.fn.getpos('.')
  end,
  desc = 'Stores cursor position',
})

autocmd('TextYankPost', {
  pattern = '*',
  group = augroup('Restore_yank_pos'),
  callback = function()
    if vim.v.event.operator == 'y' then
      vim.fn.setpos('.', cursor_pos)
    end
  end,
})

autocmd("CmdwinEnter", {
  group = augroup('CmdWindowEnter'),
  desc = "Make q close command history (q: and q?)",
  command = "nnoremap <silent><buffer><nowait> q :close<CR>",
})

-- Close man and help with just <q>
autocmd('FileType', {
  pattern = {
    'help',
    'man',
    'lspinfo',
    'checkhealth',
  },
  callback = function(event)
    vim.bo[event.buf].buflisted = false
    vim.keymap.set('n', 'q', '<cmd>q<cr>', { buffer = event.buf, silent = true })
  end,
})

autocmd('BufReadPost', {
  group = augroup('Restore_cursor'),
  callback = function()
    local exclude = { 'gitcommit' }
    local buf = vim.api.nvim_get_current_buf()
    if vim.tbl_contains(exclude, vim.bo[buf].filetype) then
      return
    end
    local mark = vim.api.nvim_buf_get_mark(0, '"')
    local lcount = vim.api.nvim_buf_line_count(0)
    if mark[1] > 0 and mark[1] <= lcount then
      pcall(vim.api.nvim_win_set_cursor, 0, mark)
      vim.api.nvim_feedkeys('zz', 'n', true)
    end
  end,
  desc = 'Go to the last loc when opening a buffer',
})

autocmd("LspAttach", {
  group = augroup('autoformat'),
  callback = function(args)
    vim.api.nvim_create_autocmd("BufWritePre", {
      buffer = args.buf,
      callback = function()
        vim.lsp.buf.format { async = false, id = args.data.client_id }
      end,
    })
  end,
  desc = "Auto format on save",
})

autocmd({ 'FocusGained', 'TermClose', 'TermLeave' }, {
  group = augroup('userconf'),
  command = 'checktime',
  desc = "Check if the file needs to be reloaded when it's changed"
})


autocmd({ 'BufEnter', 'FocusGained', 'InsertLeave', 'CmdlineLeave', 'WinEnter' }, {
  pattern = '*',
  group = augroup('GainFocus'),
  callback = function()
    if vim.o.nu and vim.api.nvim_get_mode().mode ~= 'i' then
      vim.opt.relativenumber = true
    end
  end,
  desc = 'Toggle relative numbers based on certain events'
})

autocmd({ 'BufLeave', 'FocusLost', 'InsertEnter', 'CmdlineEnter', 'WinLeave' }, {
  pattern = '*',
  group = augroup('Lose_focus'),
  callback = function()
    if vim.o.nu then
      vim.opt.relativenumber = false
      vim.cmd('redraw')
    end
  end,
  desc = 'Toggle relative numbers based on certain events'
})

autocmd('BufWritePost', {
  group = augroup('make-executable'),
  pattern = { '*.sh', '*.zsh', '*.py' },
  callback = function()
    local file = vim.fn.expand("%p")
    local status = require('core.utils').is_executable()
    if status ~= true then
      vim.fn.setfperm(file, "rwxr-x---")
    end
  end,
  desc = 'Make files ended with *.sh, *.py executable',
})

autocmd({ 'InsertEnter', 'InsertLeave' }, {
  group = augroup("changeCursorline"),
  pattern = '*',
  callback = function()
    vim.wo.cursorline = not vim.wo.cursorline
  end,
  desc = 'Auto toggle cursorline'
})
