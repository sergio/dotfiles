#!/usr/bin/env bash
#     Filename: ~/.dotfiles/install.sh
#      Created: ago 31, 2021 - 10:53
#  Last Change: Wed, 22 Jan 2025 - 17:37:40
#        Autor: Sergio Araujo
#         site: https://dev.to/voyeg3r
#       github: @voyeg3r
#       e-mail: <voyeg3r ✉ gmail.com>
#      Licence: GPL (see http://www.gnu.org/licenses/gpl.txt)

# IF YOUR DO NOT HAVE PERMISSION TO CLONE TRY THE HTTP VERSION
# For those who have the access via ssh (you need my ssh keys in this case)

```sh
curl -sSL https://bitbucket.org/sergio/dotfiles/raw/main/algorithm/shell/bash/clonerepo.sh | bash
```

Visiting the script above you can copy and use some of my instalation ideas

