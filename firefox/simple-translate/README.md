---
File: README.md
Last Change: Tue, 16 Aug 2022 18:38:04
Created: Fri, 17 Sep 2021 - 08:02
Tags: [firefox, tools]
---

## A firefox translate addon
site: https://addons.mozilla.org/en-US/firefox/addon/simple-translate/

The json file in this folder has all simpletranslate settings backed up

## shotcuts:

    Ctrl-space ...................... show translation popup

