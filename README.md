---
Filename: /home/sergio/.dotfiles/README.md
Last Change: Fri, 17 Jan 2025 - 23:28:06
---

## Install this repo

git clone https://sergio@bitbucket.org/sergio/dotfiles.git ~/.dotfiles
cd ~/.dotfiles && git fetch --recurse-submodules --jobs=8

# For those who have the access via ssh (you need my ssh keys in this case)

```sh
curl -sSL https://bitbucket.org/sergio/dotfiles/raw/main/algorithm/shell/bash/clonerepo.sh | bash
```

## Install nerd fonts

- [A better way to install NerdFonts](https://github.com/ronniedroid/getnf)

```sh
git clone https://github.com/ronniedroid/getnf.git
cd getnf
./install.sh
```

Now you can run

``` sh
getnf
```

There is an amazing site where you can test some proramming fonts:

- [Programming fonts](https://www.programmingfonts.org/)

